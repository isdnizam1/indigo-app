import React, { Component } from 'react'
import { BackHandler, Platform, View } from 'react-native'
import { WebView } from 'react-native-webview'
import { connect } from 'react-redux'
import { isNil, noop } from 'lodash-es'
import PropTypes from 'prop-types'
import * as WebBrowser from 'expo-web-browser'
import Container from '../components/Container'
import HeaderNormal from '../components/HeaderNormal'
import { getYoutubeId } from '../utils/helper'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

class BrowserScreen extends Component {
  state = {
    webTitle: null,
    isLoading: true,
    canGoBack: false,
    isMidtrans: false,
    isMidtransCorrect: false,
  };

  componentDidMount() {
    const { handleBack, url, navigateTo, navigateBack } = this.props
    const youtubeId = getYoutubeId(url)
    if (youtubeId) {
      setTimeout(navigateBack, 250)
      setTimeout(() => {
        navigateTo('YoutubePreviewScreen', {
          youtubeId,
          onBackPress: noop,
        })
      }, 350)
    }

    if (handleBack && Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this._onBackPress)
    }
    this.timeout = setTimeout(this._checkMidtransIsCorrect, 4000) // wait for 4 seconds to check if midtrans works correctly
  }

  componentWillUnmount() {
    const { handleBack } = this.props
    if (handleBack && Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this._onBackPress)
    }
    clearTimeout(this.timeout)
  }

  _checkMidtransIsCorrect = () => {
    if (this.state.isMidtrans && !this.state.isMidtransCorrect) {
      this.props.navigateBack()
      WebBrowser.openBrowserAsync(this.props.url)
    }
  };

  _onBackPress = () => {
    const { navigateBack, handleBack, backHandler } = this.props
    const { canGoBack } = this.state
    if (backHandler) {
      backHandler()
      return
    } else if (handleBack && canGoBack && this.ref) {
      this.ref.goBack()
      return true
    } else {
      navigateBack()
      return
    }
  };

  _handleBack = (navState) => {
    this.props.onNavigationStateChange(navState)
    this.setState({ canGoBack: navState.canGoBack })
  };

  render() {
    const {
      url,
      title,
      forceTitle,
      onNavigationStateChange,
      onLoadStart,
      onLoadEnd,
      handleBack,
    } = this.props
    const { webTitle, isLoading } = this.state
    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._onBackPress}
            textType={'Circular'}
            textSize={'slight'}
            text={forceTitle ? forceTitle : title ? ` ${title || webTitle}` : null}
            centered
          />
        )}
      >
        <WebView
          source={{ uri: url }}
          useWebKit={true}
          ref={(webView) => {
            this.ref = webView
          }}
          onLoadStart={() => {
            this.setState({ isLoading: true })
            onLoadStart()
          }}
          onLoadEnd={() => {
            this.setState({ isLoading: false })
            onLoadEnd()
          }}
          onLoadProgress={() => {
            this.setState({ isLoading: false })
          }}
          onError={(err) => {
            this.props.navigateBack()
            WebBrowser.openBrowserAsync(url)
          }}
          startInLoadingState={true}
          renderLoading={() => <View />}
          onNavigationStateChange={(nav) => {
            (handleBack ? this._handleBack : onNavigationStateChange)(nav)
            this.setState(
              {
                isMidtrans: (nav.url || '').indexOf('/vtweb/') >= 0,
              },
              () => {
                if (this.state.isMidtrans && !this.state.isMidtransCorrect) {
                  this.setState({
                    isMidtransCorrect: !isNil(nav.url.match(/#\/\w+/g)),
                  })
                }
              },
            )
          }}
        />
      </Container>
    )
  }
}

BrowserScreen.propTypes = {
  title: PropTypes.string,
  forceTitle: PropTypes.forceTitle,
  url: PropTypes.string,
  onNavigationStateChange: PropTypes.func,
  handleBack: PropTypes.bool,
  onLoadStart: PropTypes.func,
  onLoadEnd: PropTypes.func,
}

BrowserScreen.defaultProps = {
  title: null,
  forceTitle: null,
  url: '',
  onNavigationStateChange: noop,
  handleBack: false,
  onLoadStart: noop,
  onLoadEnd: noop,
}

export default connect(mapStateToProps, {})(BrowserScreen)
