
import React from 'react'
import { View, StyleSheet } from 'react-native'
import moment from 'moment'
import SkeletonContent from 'react-native-skeleton-content'
import { upperFirst } from 'lodash-es'
import {
  WP1,
  WP105,
  WP2,
  WP35,
  WP6,
  WP3,
  WP80,
  WP4,
  WP100,
  WP12,
  HP1,
  WP30,
  WP50,
  WP5,
  WP15,
  WP40,
} from '../../constants/Sizes'
import Image from '../../components/Image'
import { SHADOW_STYLE } from '../../constants/Styles'
import Text from '../../components/Text'
import {
  GREY,
  TOMATO,
  WHITE,
  WHITE_MILK,
  GREY_WARM,
  TEAL,
  SILVER_CALMER,
  TOMATO_CALM50,
  TOMATO_CALM,
  GUN_METAL,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  PALE_GREY,
  REDDISH,
} from '../../constants/Colors'
import { speakerDisplayName } from '../../utils/transformation'
import { Icon } from '../../components'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingTop: WP4,
    borderBottomColor: PALE_GREY,
  },
  imageWrapper: {
    width: WP30,
    height: WP30,
    borderRadius: 6,
    ...SHADOW_STYLE['shadowThin'],
  },
  buttonActive: {
    backgroundColor: REDDISH,
    paddingHorizontal: WP4,
    paddingVertical: WP1,
    borderRadius: 6,
  },
  buttonInactive: {
    backgroundColor: PALE_GREY,
    paddingHorizontal: WP2,
    paddingVertical: WP1,
    borderRadius: 6,
  },
})

export const LearnVideoItem = ({ item, loading, line }) => {
  const title = loading ? 'Title' : item.title,
    createdBy = loading ? 'Pemilik Video' : item.created_by

  return (
    <View
      style={[
        styles.container,
        { paddingBottom: line ? WP4 : 0, borderBottomWidth: line ? 1 : 0 },
      ]}
    >
      <View style={{ marginRight: WP4 }}>
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[styles.imageWrapper]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <Image
            source={{ uri: item.image }}
            style={{
              borderRadius: 6,
              backgroundColor: WHITE,
              ...SHADOW_STYLE['shadowThin'],
            }}
            imageStyle={{ width: WP30, aspectRatio: 1, borderRadius: 6 }}
          />
        </SkeletonContent>
      </View>
      <SkeletonContent
        containerStyle={{ flex: 1 }}
        layout={[
          { width: WP50, height: WP5, marginBottom: WP105 },
          { width: WP35, height: WP5, marginBottom: WP3 },
          { width: WP40, height: WP4, marginBottom: WP3 },
          { width: WP15, height: WP6, borderRadius: 6 },
        ]}
        isLoading={loading}
        boneColor={SKELETON_COLOR}
        highlightColor={SKELETON_HIGHLIGHT}
      >
        <View >
          <Text
            numberOfLines={2}
            type='Circular'
            color={GUN_METAL}
            size='mini'
            weight={500}
            style={{ lineHeight: WP6, marginBottom: WP1 }}
          >
            {upperFirst(title)}
          </Text>
          <Text
            numberOfLines={1}
            type='Circular'
            color={SHIP_GREY_CALM}
            size='xmini'
            weight={300}
            style={{ marginBottom: WP2 }}
          >
            {upperFirst(createdBy)}
          </Text>
          <View style={{ flexWrap: 'wrap' }}>
            <View style={styles.buttonActive}>
              <Text type='Circular' color={WHITE} size='xmini' weight={400}>
                Watch
              </Text>
            </View>
          </View>
        </View>
      </SkeletonContent>
    </View>
  )
}