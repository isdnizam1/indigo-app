/* eslint-disable react/sort-comp */
import React from 'react'
import { connect } from 'react-redux'
import { concat, find, get, isEmpty, noop, toLower } from 'lodash-es'
import Constants from 'expo-constants'
import { TabBar, TabView } from 'react-native-tab-view'
import {
  ScrollView,
  BackHandler,
  Dimensions,
  Platform,
  TouchableOpacity,
  View,
} from 'react-native'
import {
  deleteJourney,
  getFeeds,
  getFeedsByTopic,
  getListTopics,
  getSubscriptionDetail,
  getTimelineAds,
  getTrending,
  postLogPremium,
} from 'sf-actions/api'
import { KEY_COACHMARK_LEVEL } from 'sf-constants/Storage'
import { HP100, WP10, WP100, WP162, WP4, WP80 } from 'sf-constants/Sizes'
import { GREY, WHITE } from 'sf-constants/Colors'
import {
  _enhancedNavigation,
  Container,
  Header,
  Icon,
  Image,
  Modal,
} from 'sf-components'
import { GET_MESSAGE_UNREAD } from 'sf-services/message/actionTypes'
import { NEED_RELOAD_FEEDS } from 'sf-services/timeline/actionTypes'
import { CARD_SONG_ID } from 'sf-services/player/actionTypes'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import ImageAuto from 'sf-components/ImageAuto'
import TimelineController from 'sf-components/TimelineController'
import { paymentDispatcher } from 'sf-services/payment'
import { SET_TIMELINE_SCROLL_DIRECTION } from 'sf-services/timeline/actionTypes'
import { NavigateToInternalBrowserNoTab } from 'sf-utils/helper'
import {
  getInternalLinkCategory,
  InternalLinkPath,
} from 'sf-components/notification/NotifAction'
import { setTopicId } from 'sf-services/timeline/actionDispatcher'
import TimelineTab from '../timeline/TimelineTab'
import { WP6 } from '../../constants/Sizes'

const mapStateToProps = ({
  auth,
  setting,
  message,
  timeline: { isNeedReload, scrollDirection, topicId },
  song: { playback, soundObject },
}) => ({
  isNeedReload,
  scrollDirection,
  userData: auth.user,
  coachmarkLevel: setting.coachmarkLevel,
  newMessageCount: message.newMessageCount,
  playback,
  soundObject,
  topicId,
})

const mapDispatchToProps = {
  setTopicId,
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
  setCoachmarkLevel: (response) => ({
    type: `${KEY_COACHMARK_LEVEL}_SETTER`,
    response,
  }),
  setUnreadMessage: (unreadMessageCount) => ({
    type: GET_MESSAGE_UNREAD,
    response: unreadMessageCount,
  }),
  setNeedReloadStatus: (response) => ({
    type: NEED_RELOAD_FEEDS,
    response,
  }),
  setSongId: (response) => ({
    type: CARD_SONG_ID,
    response,
  }),
  setTimelineScrollDirection: (response) => ({
    type: SET_TIMELINE_SCROLL_DIRECTION,
    response,
  }),
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  allowBack: getParam('allowBack', false),
  addPost: getParam('addPost', false),
  defaultTab: getParam('defaultTab', 0),
  topicId: getParam('topicId', 26),
  paramsTopicId: getParam('paramsTopicId', 26),
})

const tabIndex = {
  topics: 0,
  circle: 1,
}

const defaultFeeds = [
  { type: 'post' },
  { type: 'post' },
  { type: 'post' },
  { type: 'post' },
  { type: 'post' },
]

class Forum1000StartupScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      circleFeeds: defaultFeeds,
      topicsFeeds: defaultFeeds,
      deletededFeeds: [],
      index: tabIndex[this.props.defaultTab] || 0,
      routes: [
        { key: 'community', title: 'COMMUNITY' },
        { key: 'circle', title: 'CIRCLE' },
      ],
      isReady: false,
      topicId:
        this.props.paramsTopicId ||
        get(props, 'route.params.topicId') ||
        this.props.topicId,
      customFeed: 0,
      topics: [],
      trending: [],
      exitable: false,
      account_type: 'free',
      bannerPopup: null,
      scrollEnabled: false,
    }
    this.y = 0
    this.attachBackAction = this.attachBackAction.bind(this)
    this.releaseBackAction = this.releaseBackAction.bind(this)
    this._onAddPost = this._onAddPost.bind(this)
    this._onPressTopic = this._onPressTopic.bind(this)
    this.onTabPress = this.onTabPress.bind(this)
    this.onFeedScroll = this.onFeedScroll.bind(this)
    this._getCircleFeeds = this._getCircleFeeds.bind(this)
    this._getTopicsFeeds = this._getTopicsFeeds.bind(this)
    this._onDeletePost = this._onDeletePost.bind(this)
  }

  tabScrollView = React.createRef();

  _onPressTopic = (topic) => {
    this.setState(
      {
        topicId: topic.id_topic,
        isTabReady: false,
        topicsFeeds: defaultFeeds,
      },
      () => this._getTopicsFeeds({ start: 0, limit: 15 }, false, false),
    )
    // this.setState({ topicId: topic.id_topic })
  };

  onTabPress = (newIndex) => {
    const { width: screenWidth } = Dimensions.get('window')
    this.props.setTimelineScrollDirection('top')
    this.setState({ index: newIndex, scrollEnabled: true }, () => {
      typeof this.tabScrollView != 'undefined' &&
        typeof this.tabScrollView.scrollTo === 'function' &&
        this.tabScrollView.scrollTo({
          x: newIndex * screenWidth,
          y: 0,
          animated: true,
        })
      setTimeout(
        () =>
          this.setState({ scrollEnabled: false }, () => {
            if (newIndex === 0)
              this._getTopicsFeeds({ start: 0, limit: 15 }, null, false)
            else if (newIndex === 1)
              this._getCircleFeeds({ start: 0, limit: 15 }, null, false)
          }),
        500,
      )
    })
  };

  onFeedScroll = ({
    nativeEvent: {
      contentOffset: { y },
    },
  }) => {
    const scrollDirection = y < this.y || y < 48 ? 'top' : 'bottom'
    const scrollAmount = Math.abs(this.y - y)
    this.y = y
    if (
      scrollDirection != this.props.scrollDirection &&
      (scrollAmount >= 48 || y < 48)
    ) {
      this.props.setTimelineScrollDirection(scrollDirection)
    }
  };

  _onAddPost() {
    const { index, topicId } = this.state
    const { navigateTo } = this.props
    navigateTo('FeedPostScreen', {
      refreshFeeds: index === 1 ? this._getCircleFeeds : this._getTopicsFeeds,
      is1000Startup: true,
      topicIds:
        index === 1
          ? undefined
          : Number(topicId) !== 10 && Number(topicId)
          ? [Number(topicId)]
          : undefined,
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    const shouldUpdate = !!nextState.isReady || !!nextProps.isNeedReload
    return typeof shouldUpdate === 'undefined' ? true : shouldUpdate
  }

  async componentDidMount() {
    const { setTopicId, addPost } = this.props
    const { topicId } = this.state
    !!topicId && setTopicId(topicId)

    this.focusListener = this.props.navigation.addListener(
      'focus',
      this.attachBackAction,
    )
    this.blurListener = this.props.navigation.addListener(
      'blur',
      this.releaseBackAction,
    )
    await this._getInitialScreenData()
    // console.log({ addPost })
    addPost && this._onAddPost()
    // __DEV__ && console.log('componentDidMount')
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.topicId) {
      this._onPressTopic({ id_topic: this.props.topicId })
      this.props.setTopicId(null)
    }
  }

  componentWillUnmount() {
    typeof this.backHandler === 'object' &&
      typeof this.backHandler.remove === 'function' &&
      this.backHandler.remove()
  }

  attachBackAction() {
    const { isNeedReload, setNeedReloadStatus, navigateBack } = this.props
    if (isNeedReload) {
      setNeedReloadStatus(false)
      this._getTopicsFeeds()
    }
    const back = () => {
      navigateBack()
      return true
    }
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      back.bind(this),
    )
  }

  releaseBackAction() {
    const { soundObject, playback } = this.props
    playback.isLoaded && soundObject.pauseAsync()
    typeof this.backHandler !== 'undefined' && this.backHandler.remove()
  }

  _getInitialScreenData = async () => {
    const {
      userData: { id_user },
      dispatch,
    } = this.props
    const results = await Promise.all([
      dispatch(getSubscriptionDetail, { id_user }, noop, false, false),
      this._getTopics(),
      this._getTopicsFeeds({ start: 0, limit: 15 }, null, false),
      this._getCircleFeeds({ start: 0, limit: 15 }),
    ])

    const { account_type, package_type } = results[0]
    const accountType = account_type || package_type
    this.setState({
      account_type: accountType || 'free',
      isReady: true,
    })
  };

  _getPromotedFeed = async () => {
    const { dispatch } = this.props
    const dataResponse = await dispatch(getTimelineAds, {}, noop, true, false)
    let newPromotedFeed = {}
    if (!isEmpty(dataResponse) && dataResponse.code !== 500) {
      newPromotedFeed = dataResponse.result
      newPromotedFeed.type = 'ads'
    }
    return newPromotedFeed
  };

  _getCircleFeeds = async (params, loadMore = false, isLoading = false) => {
    const {
      userData: { id_user },
      dispatch,
    } = this.props
    if (!params) params = { start: 0, limit: 15 }

    if (!loadMore) {
      this.setState({
        isTabReady: false,
        circleFeeds: defaultFeeds,
      })
    }

    const dataResponse = await dispatch(
      getFeeds,
      {
        id_user,
        role: 'circle',
        platform: '1000startup',
        ...params,
      },
      noop,
      true,
      isLoading,
    )
    if (!loadMore) {
      const newFeeds = dataResponse.result || []
      this.setState({ circleFeeds: newFeeds, isTabReady: true })
    } else {
      this.setState((state) => ({
        circleFeeds: concat(state.circleFeeds, dataResponse.result || []),
        isTabReady: true,
      }))
    }
  };

  _getTopicsFeeds = async (params, loadMore = false, isLoading = false) => {
    const { userData, dispatch } = this.props
    const { topicId, customFeed, topics, trending } = this.state

    if (topicId == 12) {
      if (loadMore) return
      await this._getTrending()
      this.setState({ topicsFeeds: trending, isTabReady: true })
      return
    }

    if (!loadMore) {
      this.setState({
        isTabReady: false,
        topicsFeeds: defaultFeeds,
      })
    }

    const promotedFeed = await this._getPromotedFeed()
    if (!params) params = { start: 0, limit: 15 }
    if (loadMore) params.start = params.start - customFeed
    const dataResponse = await dispatch(
      getFeedsByTopic,
      {
        id_user: userData.id_user,
        id_topic: Number(topicId),
        role: 'community',
        platform: '1000startup',
        ...params,
      },
      noop,
      true,
      isLoading,
    )

    // console.log(
    //   { dataResponse },
    //   {
    //     id_user: userData.id_user,
    //     id_topic: Number(topicId),
    //     role: 'community',
    //     ...params,
    //   },
    // )

    if (this.isTrendingShown()) {
      await this._getTrending()
    }

    if (!loadMore) {
      const newFeeds = dataResponse.result || []
      let customItem = 0
      if (topicId == 3 && !isEmpty(promotedFeed)) {
        newFeeds.unshift(promotedFeed)
        customItem++
      }
      if (topicId == 2) {
        newFeeds.unshift({ type: 'bannerCollab' })
        customItem++
      }
      if (topicId == 11) {
        const itemTopics = topics.find((item) => item.id_topic == 11),
          imageChallenge = !isEmpty(itemTopics) ? itemTopics.image : null
        newFeeds.unshift({
          type: 'challenge',
          image: imageChallenge,
        })
        customItem++
      }
      this.setState({
        topicsFeeds: newFeeds,
        isTabReady: true,
        customFeed: customItem,
      })
    } else {
      this.setState((state) => ({
        topicsFeeds: concat(state.topicsFeeds, dataResponse.result || []),
        isTabReady: true,
      }))
    }
  };

  _getTopics = async (params, loadMore = false, isLoading = true) => {
    const { dispatch } = this.props

    const responseTopics = await dispatch(
      getListTopics,
      {
        ...params,
      },
      noop,
      true,
      isLoading,
    )
    this.setState({
      topics: responseTopics.result || [],
    })
  };

  _onClickBanner = async () => {
    const { bannerPopup, packages } = this.state
    const { navigateTo, dispatch, userData, paymentSourceSet } = this.props
    if (bannerPopup.bannerId === '1') {
      if (packages) {
        const currentPackage = find(
          packages,
          (packageDetail) => toLower(packageDetail.package_name) === 'maestro',
        )
        dispatch(
          postLogPremium,
          {
            id_user: userData.id_user,
            previous_screen: 'Popup Maestro',
          },
          noop,
          true,
          true,
        )
        paymentSourceSet('Popup Maestro')
        navigateTo('PromoteUser3', {
          selectedPackage: currentPackage,
        })
      }
    } else {
      if (!bannerPopup.internal_link) {
        NavigateToInternalBrowserNoTab({
          url: bannerPopup.link,
        })
      } else if (!isEmpty(bannerPopup.link)) {
        const splittedLink = split(bannerPopup.link, '/')
        const category = getInternalLinkCategory(splittedLink)
        const Link = InternalLinkPath[category]
        const payload = await Link.payload(splittedLink)
        navigateTo(Link.to, payload)
      }
    }
  };

  _onDeletePost = (idJourney) => {
    const { dispatch } = this.props
    let { deletededFeeds } = this.state
    deletededFeeds.push(parseInt(idJourney))
    this.setState({ deletededFeeds })
    dispatch(deleteJourney, idJourney)
  };

  isTrendingShown = () =>
    Number(this.state.topicId) === 10 || Number(this.state.topicId) === 12;

  _getTrending = async (params = {}, isLoading = false) => {
    const {
      userData: { id_user },
      dispatch,
    } = this.props

    const responseTrending = await dispatch(
      getTrending,
      {
        id_user,
        start: 0,
        limit: 5,
        ...params,
      },
      noop,
      true,
      isLoading,
    )
    this.setState({
      trending: responseTrending.result || [],
    })
    return responseTrending
  };

  render() {
    const { navigateTo, isLoading, newMessageCount, paymentSourceSet } = this.props
    const {
      topicsFeeds,
      circleFeeds,
      isTabReady,
      topics,
      topicId,
      index,
      trending,
      bannerPopup,
      scrollEnabled,
      account_type,
    } = this.state
    const { width: screenWidth } = Dimensions.get('window')
    return (
      <Container
        isTimeline
        hasBottomNavbar
        isLoading={isLoading}
        onBackOnline={this.componentDidMount}
        type='horizontal'
        headerBackground={WHITE}
        loadingContent={() => (
          <View
            style={{
              flex: 1,
              flexGrow: 1,
              width: WP100,
              height: HP100,
              marginTop: Platform.OS == 'android' ? -Constants.statusBarHeight : 0,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Image
              source={require('sf-assets/loading.gif')}
              size={WP162}
              aspectRatio={840 / 607}
            />
          </View>
        )}
        renderHeader={() => (
          <Header
            style={{
              paddingVertical: 0,
              width: WP100,
              paddingHorizontal: 0,
            }}
          >
            <TimelineController
              is1000Startup={true}
              onTabPress={this.onTabPress}
              onTopicPress={this._onPressTopic}
              topics={topics}
              topicId={topicId}
              index={index}
              accountType={account_type}
              paymentSourceSet={paymentSourceSet}
              navigateTo={navigateTo}
              navigateBack={this.props.navigateBack}
              newMessageCount={newMessageCount}
            />
          </Header>
        )}
        outsideContent={() => {
          return !isEmpty(bannerPopup) ? (
            <Modal
              isVisible={true}
              position='center'
              animationIn='fadeIn'
              animationOut='fadeOut'
              modalStyle={{ alignSelf: 'center', width: WP80 }}
              renderModalContent={({ toggleModal }) => (
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    onPress={() => {
                      this._onClickBanner()
                      toggleModal()
                    }}
                  >
                    <ImageAuto source={{ uri: bannerPopup.image }} width={WP80} />
                  </TouchableOpacity>
                  <View
                    style={{
                      position: 'absolute',
                      top: -WP4,
                      right: -WP4,
                    }}
                  >
                    <Icon
                      backgroundColor={WHITE}
                      style={{ borderRadius: WP100 }}
                      color={GREY}
                      size={WP10}
                      name='closecircle'
                      type='AntDesign'
                      onPress={() => toggleModal()}
                    />
                  </View>
                </View>
              )}
            >
              {(M) => <View>{M}</View>}
            </Modal>
          ) : null
        }}
      >
        <>
          <TabView
            navigationState={this.state}
            initialLayout={{ height: 0 }}
            swipeEnabled={false}
            onIndexChange={this.onTabPress}
            renderTabBar={(props) => <></>}
            renderScene={({ route }) => {
              switch (route.key) {
                case 'community':
                  return (
                    <View
                      style={{
                        width: screenWidth,
                        flex: 1,
                        height: '100%',
                      }}
                    >
                      <TimelineTab
                        is1000Startup={true}
                        onFeedScroll={this.onFeedScroll}
                        feeds={topicsFeeds}
                        deletededFeeds={this.state.deletededFeeds}
                        isActive={index == 0}
                        isTabReady={isTabReady}
                        getFeeds={this._getTopicsFeeds}
                        topics={topics}
                        selectedTopicId={Number(topicId)}
                        onPressTopic={this._onPressTopic}
                        onDeletePost={this._onDeletePost}
                        isTopics
                        isTrendingShown={this.isTrendingShown()}
                        trending={trending}
                        navigation={this.props.navigation}
                        type={'community'}
                      />
                    </View>
                  )
                case 'circle':
                  return (
                    <View
                      style={{
                        width: screenWidth,
                        flex: 1,
                        height: '100%',
                      }}
                    >
                      <TimelineTab
                        is1000Startup={true}
                        onFeedScroll={this.onFeedScroll}
                        feeds={circleFeeds}
                        deletededFeeds={this.state.deletededFeeds}
                        isActive={index == 1}
                        isTabReady={isTabReady}
                        getFeeds={this._getCircleFeeds}
                        onDeletePost={this._onDeletePost}
                        topFeedPost={true}
                        type={'circle'}
                      />
                    </View>
                  )
              }
            }}
          />

          {/* <View style={{ width: screenWidth * 2, height: '100%' }}>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              bounces={false}
              horizontal={true}
              ref={(ref) => {
                this.tabScrollView = ref
              }}
              scrollEnabled={scrollEnabled}
              style={{ flex: 1, backgroundColor: WHITE }}
            >
              <View style={{ width: screenWidth * 2, flexDirection: 'row' }}>
                <View
                  style={{
                    width: screenWidth,
                    flex: 1,
                    height: '100%',
                  }}
                >
                  <TimelineTab
                    is1000Startup={true}
                    onFeedScroll={this.onFeedScroll}
                    feeds={topicsFeeds}
                    deletededFeeds={this.state.deletededFeeds}
                    isActive={index == 0}
                    isTabReady={isTabReady}
                    getFeeds={this._getTopicsFeeds}
                    topics={topics}
                    selectedTopicId={Number(topicId)}
                    onPressTopic={this._onPressTopic}
                    onDeletePost={this._onDeletePost}
                    isTopics
                    isTrendingShown={this.isTrendingShown()}
                    trending={trending}
                    navigation={this.props.navigation}
                  />
                </View>
                <View
                  style={{
                    width: screenWidth,
                    flex: 1,
                    height: '100%',
                  }}
                >
                  <TimelineTab
                    is1000Startup={true}
                    onFeedScroll={this.onFeedScroll}
                    feeds={circleFeeds}
                    deletededFeeds={this.state.deletededFeeds}
                    isActive={index == 1}
                    isTabReady={isTabReady}
                    getFeeds={this._getCircleFeeds}
                    onDeletePost={this._onDeletePost}
                    topFeedPost={true}
                  />
                </View>
              </View>
            </ScrollView>
          </View> */}
          <TouchableOpacity
            style={{
              position: 'absolute',
              right: WP4,
              bottom: WP6,
            }}
            onPress={this._onAddPost}
          >
            <Image
              source={require('sf-assets/icons/newAddTomato.png')}
              aspectRatio={1}
              imageStyle={{
                width: 70,
                height: 70,
              }}
            />
          </TouchableOpacity>
        </>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(Forum1000StartupScreen),
  mapFromNavigationParam,
)
