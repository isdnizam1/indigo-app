/* eslint-disable no-constant-condition */
import React, { Component } from 'react'
import {
  Animated as RNAnimated,
  StatusBar,
  View,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  TextInput,
} from 'react-native'
import RNModal from 'react-native-modal'
import Modal from 'sf-components/Modal'
import PropTypes from 'prop-types'
import styleRegister from 'sf-styles/register'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import Animated from 'react-native-reanimated'
import { SHADOW_GRADIENT } from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import MessageBarClearBlue from 'sf-components/messagebar/MessageBarClearBlue'
import { registerDispatcher } from 'sf-services/register'

import { isEmpty, noop } from 'lodash-es'
import {
  getProfileDetailV3,
  getProfileProgressV2,
  getStartupTabMission,
  getStartupTabLearn,
  getDetailReferralCode,
  postSubmitReferralStartup,
  getAnnouncement
} from 'sf-actions/api'
import Carousel from 'react-native-snap-carousel'
import { WP205 } from 'sf-constants/Sizes'
import { ImageBackground } from 'react-native'
import {
  GREEN_30,
  GREEN_TOSCA,
  LIPSTICK_TWO,
  NAVY_DARK,
  NO_COLOR,
  PALE_BLUE,
  REDDISH,
  RED_GOOGLE,
  TRANSPARENT,
  WHITE,
  WHITE20,
} from '../../constants/Colors'
import {
  Avatar,
  Container,
  Icon,
  Image,
  Loader,
  ModalMessageView,
  Text,
} from '../../components'
import {
  WP10,
  WP100,
  WP105,
  WP3,
  WP4,
  WP6,
  WP1,
  WP80,
  WP2,
  WP5,
  WP25,
  WP20,
  WP375,
  WP05,
} from '../../constants/Sizes'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { MODAL_STYLE, SHADOW_STYLE } from '../../constants/Styles'
import { getBottomSpace, getStatusBarHeight } from '../../utils/iphoneXHelper'
import style from '../profile/v2/ProfileScreen/style'
import { getGeneralTabData, getStartupGeneral } from '../../actions/api'
import ButtonV2 from '../../components/ButtonV2'
import ImageAuto from '../../components/ImageAuto'
import {
  getShowModalOnboarding1000Startup,
  setShowModalOnboarding1000Startup,
} from '../../utils/storage'
import Spacer from '../../components/Spacer'
import GeneralTab from './GeneralTab'

const mapStateToProps = ({
  auth,
  // screen: {
  //   lv1: { luckyfren },
  // },
}) => ({
  userData: auth.user,
  // luckyfren,
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  category: getParam('category', ''),
  tabToShow: getParam('tabToShow', 0),
})

const mapDispatchToProps = {
  setReferralCode: (referral_code) => registerDispatcher.setData({ referral_code }),
}

class HomeStartupScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      showModalMaintenance: false,
      profile: {},
      profileProgress: {},
      generalData: null,
      data: {
        learn: {},
        mission: {},
      },
      index: this.props.tabToShow || 0,
      routes: [
        { key: 'general', title: 'GENERAL' },
        { key: 'learn', title: 'LEARN' },
        { key: 'mission', title: 'MISSION' },
      ],
      headerOpacity: new RNAnimated.Value(0),
      statusBarTheme: 'light',
      showBannerRegisterStartup: false,
      showModalReferralCode: false,
      referral: '',
      isValidReferral: null,
      isReferralButtonDisabled: true,
      showModalWelcome: false,
      currentIndex: 0,
      roadmap: [
        {
          image:
            'https://eventeer.id/assets/image/startup/banner-roadmap-disabled.png',
        },
        {
          image:
            'https://eventeer.id/assets/image/startup/banner-roadmap-disabled.png',
        },
        {
          image:
            'https://eventeer.id/assets/image/startup/banner-roadmap-disabled.png',
        },
        {
          image:
            'https://eventeer.id/assets/image/startup/banner-roadmap-disabled.png',
        },
      ],
      announcement: null
    }
  }

  componentDidMount() {
    this._getAllData()
  }

  _checkIsUserStartup() {
    const { profile } = this.state
    let showBannerRegisterStartup = false
    if (isEmpty(profile)) {
      showBannerRegisterStartup = true
    }
    this.setState({ showBannerRegisterStartup })
  }

  validateReferral() {
    const { setReferralCode } = this.props
    const { referral } = this.state

    !isEmpty(referral) &&
      getDetailReferralCode({ referral_code: referral })
        .then(({ data }) => {
          const { code } = data
          this.setState(
            { isValidReferral: code == 200, isReferralButtonDisabled: code != 200 },
            () => {
              code == 200 && setReferralCode(referral)
            },
          )
        })
        .catch((e) => {})
  }

  async _getAllData() {
    this.setState({ isLoading: true })
    await this._getUserProfile()
    await this._getProfileProgress()
    await this._getGeneralData()
    await this._getAnnouncement()
    // await this._getStartupTabMission()
    // await this._getStartupTabLearn()
    this._checkIsUserStartup()
    await this._getModalWelcome()
    this.setState({ isLoading: false })
  }
  async _getModalWelcome() {
    await getShowModalOnboarding1000Startup().then((value) => {
      value = JSON.parse(value)
      let showModalWelcome = false
      if (value === null || value === true) {
        showModalWelcome = true
      }
      this.setState({ showModalWelcome })
    })
  }

  async _setModalWelcome() {
    setShowModalOnboarding1000Startup(JSON.stringify(false)).then((response) => {
      this._getModalWelcome()
    })
  }

  _getProfileProgress = async () => {
    const { userData } = this.props
    // this.setState({ isLoading: true })
    const profileParams = { id_user: userData.id_user }
    return new Promise((resolve, reject) => {
      this.props
        .dispatch(getProfileProgressV2, profileParams)
        .then((result) => {
          this.setState({ profileProgress: result })
          resolve(result)
        })
        .catch((e) => {
          // this.setState({ isLoading: false })
        })
    })
  };

  _getStartupTabLearn = async () => {
    const { dispatch, userData } = this.props
    try {
      const data = await dispatch(getStartupTabLearn, noop, true, false)

      // this.setState({ isLoading: false })
      if (data) {
        this.setState((prevState) => ({
          data: {
            ...prevState.data,
            learn: data,
          },
        }))
      }
    } catch (e) {
      // this.setState({ isLoading: false })
    }
  };

  _getStartupTabMission = async () => {
    // this.setState({ isLoading: true })
    const { dispatch, userData } = this.props
    try {
      const data = await dispatch(
        getStartupTabMission,
        { id_user: userData.id_user, platform: '1000startup' },
        noop,
        true,
        false,
      )
      // this.setState({ isLoading: false })
      if (data.code === 200) {
        this.setState((prevState) => ({
          data: {
            ...prevState.data,
            mission: data.result,
          },
        }))
      }
    } catch (e) {
      // this.setState({ isLoading: false })
    }
  };

  _getGeneralData = async () => {
    getStartupGeneral({ id_user: this.props.userData.id_user }).then(
      ({ data: { result } }) => {
        this.setState({ generalData: result, roadmap: result.roadmap })
      },
    )
  };

  _getAnnouncement = async () => {
    const { dispatch, userData } = this.props
    try {
      const data = await dispatch(
        getAnnouncement,
        { id_user: userData.id_user },
        noop,
        true,
        false,
      )

      if (data.code === 200) {
        this.setState({ announcement: data.result })
      }
    } catch (e) {
      this.setState({ announcement: null })
    }
  };

  _onPullDownToRefresh = () => {
    this._getAllData()
  };
  _getUserProfile = async () => {
    const { userData, dispatch } = this.props
    // this.setState({ isLoading: true })
    const profileParams = { id_user: userData.id_user }
    try {
      const data = await dispatch(
        getProfileDetailV3,
        profileParams,
        noop,
        true,
        false,
      )
      if (data.code === 200) {
        this.setState({
          profile: data.result,
        })
      }
    } catch (e) {
      // this.setState({ isLoading: false })
    }
  };

  _actionSubmitReferral = async () => {
    const { userData, dispatch } = this.props
    const { referral } = this.state
    // this.setState({ isLoading: true })
    const body = { id_user: userData.id_user, referral_code: referral }
    try {
      const data = await dispatch(
        postSubmitReferralStartup,
        body,
        noop,
        true,
        false,
      )
      // eslint-disable-next-line no-empty
      if (data.code === 200) {
      }
    } catch (e) {
      // this.setState({ isLoading: false })
    }
    this._onPullDownToRefresh()
  };

  _headerSection = () => {
    const { roadmap, currentIndex } = this.state
    return (
      <View>
        <Image
          tint={'black'}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={1.095}
          source={require('../../assets/images/home1000startupbg.jpeg')}
        />
        <LinearGradient
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
          }}
          colors={[TRANSPARENT, TRANSPARENT, TRANSPARENT]}
        >
          <View
            style={{
              paddingTop: getStatusBarHeight(false) + WP105,
              paddingBottom: WP4,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                ...SHADOW_STYLE['shadowBold'],
                paddingHorizontal: WP4,
              }}
            >
              <View>
                <Icon
                  onPress={this.props.navigateBack}
                  background='dark-circle'
                  backgroundColor={WHITE20}
                  size='huge'
                  color={WHITE}
                  name='chevron-left'
                  type='Entypo'
                />
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View>
                  <Icon
                    onPress={() => {
                      this.props.navigateTo(
                        'Notification1000StartupScreen',
                        { isStartup: true },
                        'push',
                      )
                    }} // navigateTo('NotificationStack', {{type:'Other', platform: '1000startup'}} )
                    background='dark-circle'
                    size='massive'
                    color={WHITE}
                    name='notifications'
                    type='MaterialIcons'
                  />
                </View>
                <Spacer horizontal size={WP3} />
                <TouchableOpacity onPress={this._onEditProfile}>
                  <Avatar image={{ uri: this.props.userData.profile_picture }} />
                </TouchableOpacity>
              </View>
            </View>
            <Spacer size={WP5} />
            <View
              style={{
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
                paddingHorizontal: WP4,
              }}
            >
              <Image
                centered={false}
                aspectRatio={2.4}
                size={'extraMassive2'}
                source={require('../../assets/icons/welcomeFounders.png')}
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginVertical: WP4,
                }}
              >
                <Image
                  size={'tiny'}
                  source={require('../../assets/icons/clarityMapWhite.png')}
                />
                <Spacer size={WP2} horizontal />
                <Text color={WHITE}>Your startup roadmap</Text>
              </View>
            </View>
            <View>
              <View style={{ marginLeft: WP3 }}>
                <Carousel
                  data={roadmap}
                  onBeforeSnapToItem={(currentIndex) =>
                    this.setState({ currentIndex })
                  }
                  renderItem={({ item, index }) => {
                    const [startDate, endDate] = (
                      item.date || '1 Jan - 2 Jan'
                    ).match(/\d{1,2}\s[a-zA-Z]{1,3}/g)
                    const [phase] = (item.phase || 'Phase 1').match(/\d+/g)
                    return (
                      <View
                        key={`roadmap-${index}`}
                        style={{
                          width: WP375,
                          height: WP20 + WP2,
                          marginLeft: WP4,
                          elevation: 2,
                          marginBottom: 10,
                          borderRadius: WP205,
                          alignItems: 'flex-end',
                        }}
                      >
                        <ImageBackground
                          style={{
                            flex: 1,
                            paddingLeft: WP4,
                            width: WP375,
                            height: WP20 + WP2,
                            resizeMode: 'cover',
                            justifyContent: 'flex-start',
                            alignItems: 'flex-end',
                            paddingRight: WP3,
                            paddingTop: WP2 * 1.65,
                          }}
                          imageStyle={{ borderRadius: WP3 }}
                          source={{ uri: item.image }}
                        >
                          <Text
                            weight={700}
                            style={{
                              position: 'absolute',
                              left: WP2,
                              bottom: 0,
                              fontSize: WP20 + WP2,
                              lineHeight: WP25 - WP5,
                              color: !item.date
                                ? TRANSPARENT
                                : 'rgba(255,255,255,0.25)',
                            }}
                          >
                            {phase}
                          </Text>
                          <Text
                            size={'slight'}
                            weight={700}
                            color={!item.date ? TRANSPARENT : WHITE}
                          >
                            {item.title}
                          </Text>
                          <Spacer size={WP05} />
                          <Text
                            size={'tiny'}
                            color={!item.date ? TRANSPARENT : WHITE}
                          >
                            {startDate} - {endDate}
                          </Text>
                        </ImageBackground>
                      </View>
                    )
                  }}
                  sliderWidth={WP100}
                  itemWidth={WP375 + WP4}
                  inactiveSlideScale={1}
                  inactiveSlideOpacity={1}
                  activeSlideAlignment={'start'}
                  ref={(c) => {
                    this._carousel = c
                  }}
                />
              </View>
              {currentIndex !== roadmap.length - 1 && (
                <LinearGradient
                  colors={['rgba(33,43,54,.025)', 'rgba(33,43,54,1)']}
                  start={[0, 0]}
                  end={[1, 0]}
                  locations={[0, 0.6]}
                  style={{ position: 'absolute', height: WP20 + WP2, right: 0 }}
                >
                  {this._renderButtonPrevNext({
                    onPress: () => this._carousel.snapToNext(),
                  })}
                </LinearGradient>
              )}
              {currentIndex !== 0 && (
                <View
                  style={{
                    position: 'absolute',
                    height: WP20 + WP2,
                    left: 0,
                    transform: [{ rotate: '180deg' }],
                  }}
                >
                  {this._renderButtonPrevNext({
                    onPress: () => this._carousel.snapToPrev(),
                  })}
                </View>
              )}
            </View>
          </View>
        </LinearGradient>
      </View>
    )
  };

  _renderButtonPrevNext = ({ onPress }) => {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={{
          height: WP20 + WP2,
          alignItems: 'center',
          justifyContent: 'center',
          paddingHorizontal: WP3,
          left: 0,
        }}
      >
        <Image
          size={'mini'}
          source={require('../../assets/icons/arrowRightChevronTomato.png')}
        />
      </TouchableOpacity>
    )
  };

  _onChangeTab = async (index) => {
    this.setState({ index })
  };

  _outsideContent = () => {
    const { showModalReferralCode, showBannerRegisterStartup } = this.state
    const { navigateBack } = this.props
    return (
      <>
        <StatusBar
          animated={false}
          translucent={true}
          backgroundColor={'transparent'}
          barStyle={`${this.state.statusBarTheme}-content`}
        />
        {showBannerRegisterStartup && <Modal
          transparent
          isVisible={true}
          position='center'
          closeOnBackdrop={noop}
          onBackButtonPress={() => navigateBack()}
          animationIn='fadeIn'
          animationOut='fadeOut'
          backgroundColor={'transparent'}
          onBackDropPress={noop}
          // onModalHide={this._toggleModalReferral}
          modalStyle={{ alignSelf: 'center', width: WP80 }}
          renderModalContent={({ toggleModal }) => (
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <ImageAuto
                style={{ aspectRatio: 302 / 371 }}
                source={require('sf-assets/images/bannerKanal1000Startup.png')}
                width={WP80}
              />
              <ButtonV2
                onPress={() => {
                  toggleModal(), this._toggleModalReferral()
                }}
                style={{
                  height: true ? 50 : 38,
                  paddingHorizontal: WP6,
                  marginTop: WP4,
                }}
                textColor={WHITE}
                color={REDDISH}
                text={'Isi Referral Code'}
                disabled={false}
              />
              <View
                style={{
                  position: 'absolute',
                  top: 15,
                  left: 35,
                }}
              >
                <Icon
                  onPress={() => {
                    navigateBack(), toggleModal()
                  }}
                  background='dark-circle'
                  backgroundColor={WHITE20}
                  size='large'
                  color={WHITE}
                  name='chevron-left'
                  type='Entypo'
                />
              </View>
            </View>
          )}
        >
          {({ toggleModal }, M) => <View>{M}</View>}
        </Modal>}
        <RNModal
          onBackdropPress={noop}
          transparent
          isVisible={showModalReferralCode}
          style={[MODAL_STYLE['bottom']]}
          propagateSwipe={Platform.OS === 'ios'}
          avoidKeyboard={Platform.OS === 'ios'}
        >
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            style={{ flex: 1, justifyContent: 'flex-end' }}
          >
            <View
              behavior={Platform.OS === 'ios' ? 'padding' : null}
              style={{ flex: 1, justifyContent: 'flex-end' }}
            >
              <View style={styleRegister.referralBackground}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginBottom: WP1,
                    paddingLeft: WP6,
                    borderBottomWidth: 1,
                    borderTopWidth: 1,
                    borderColor: PALE_BLUE,
                  }}
                >
                  <Text
                    size={'slight'}
                    weight={600}
                    color={NAVY_DARK}
                    type={'Circular'}
                  >
                    Kode Referral
                  </Text>
                  <TouchableOpacity
                    style={{ paddingVertical: WP4, paddingHorizontal: WP6 }}
                    onPress={() => {
                      this._toggleModalReferral(), navigateBack()
                    }}
                  >
                    <Icon size={'large'} name={'close'} />
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    marginTop: WP4,
                    marginBottom: WP1,
                    paddingHorizontal: WP6,
                  }}
                >
                  <Text
                    style={styleRegister.label}
                    size={true ? 'tiny' : 'xmini'}
                    weight={400}
                    type={'Circular'}
                  >
                    Kode Referral
                  </Text>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flex: 1 }}>
                      <TextInput
                        autoCapitalize={'characters'}
                        onChangeText={(referral) =>
                          this.setState(
                            { referral, isValidReferral: null },
                            this.validateReferral,
                          )
                        }
                        autoCompleteType={'name'}
                        placeholder={'Tulis kode referral'}
                        style={styleRegister.input}
                      />
                      {!isEmpty(this.state.referral) &&
                        !!this.state.isValidReferral && (
                          <Text color={GREEN_30} size={'xmini'}>
                            Your referral code is valid
                          </Text>
                        )}
                      {!isEmpty(this.state.referral) && !this.state.isValidReferral && (
                        <Text color={RED_GOOGLE} size={'xmini'}>
                          Your referral code is invalid
                        </Text>
                      )}
                    </View>
                    <View style={{ paddingLeft: WP4 }}>
                      <ButtonV2
                        onPress={() => {
                          this._toggleModalReferral(), this._actionSubmitReferral()
                        }}
                        style={{ height: true ? 50 : 38, paddingHorizontal: WP6 }}
                        textColor={WHITE}
                        color={REDDISH}
                        text={'Pakai'}
                        disabled={this.state.isReferralButtonDisabled}
                      />
                    </View>
                  </View>
                </View>
                {true && (
                  <TouchableOpacity
                    // onPress={() => this.setState({ isValidReferral: null })}
                    style={{ paddingHorizontal: WP6 }}
                  >
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Text
                        color={true ? GREEN_TOSCA : LIPSTICK_TWO}
                        size={'tiny'}
                        weight={400}
                        type={'Circular'}
                      >
                        {/* {!isValidReferral
                        ? 'Kode tidak valid'
                        : 'Kode referal valid'} */}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </KeyboardAvoidingView>
        </RNModal>
      </>
    )
  };

  _onScroll = ({
    nativeEvent: {
      contentOffset: { y: scrollPosition },
    },
  }) => {
    const { statusBarTheme: theme, headerOpacity } = this.state
    if (scrollPosition > 15 && theme === 'light') {
      RNAnimated.timing(headerOpacity, {
        toValue: 1,
        useNativeDriver: true,
        duration: 250,
        delay: 0,
        isInteraction: false,
      }).start()
      this.setState({
        statusBarTheme: 'dark',
      })
    }
    if (scrollPosition <= 15 && theme === 'dark') {
      RNAnimated.timing(headerOpacity, {
        toValue: 0,
        useNativeDriver: true,
        duration: 250,
        delay: 0,
        isInteraction: false,
      }).start()
      this.setState({
        statusBarTheme: 'light',
      })
    }
  };
  _onEditProfile = () => {
    this.props.navigateTo('StartUpProfileScreen', {
      refreshProfile: this._getUserProfile,
    })
  };

  _toggleModalMaintenance = () => {
    this.setState({ showModalMaintenance: !this.state.showModalMaintenance })
  };

  _toggleModalReferral = () => {
    setTimeout(() => {
      this.setState({ showModalReferralCode: !this.state.showModalReferralCode })
    }, 500)
  };

  render() {
    const {
      navigateTo,
      // luckyfren: {
      //   result
      // },
    } = this.props
    const {
      isLoading,
      showModalWelcome,
      showBannerRegisterStartup,
      showModalReferralCode,
      generalData,
      showModalMaintenance,
    } = this.state
    return (
      <Container
        scrollBackgroundColor={WHITE}
        isReady={true}
        isLoading={false}
        noStatusBarPadding
        outsideContent={this._outsideContent}
        statusBarBackground={NO_COLOR}
      >
        <NavigationEvents onWillFocus={this._onPullDownToRefresh} />
        <Animated.ScrollView
          onScroll={this._onScroll}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        >
          <View style={{ flex: 1, paddingBottom: getBottomSpace() }}>
            {this._headerSection()}
            <View
              style={{
                flex: 1,
                marginTop: -WP10,
                backgroundColor: WHITE,
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
              }}
            >
              {generalData ? (
                <GeneralTab
                  announcement={this.state.announcement}
                  data={this.state.general}
                  generalData={generalData}
                  loading={isLoading}
                  navigateTo={navigateTo}
                  profileProgress={this.state.profileProgress}
                  profile={this.state.profile}
                  userData={this.props.userData}
                  onEditProfile={this._onEditProfile}
                  getData={this._onPullDownToRefresh}
                  showModalMaintenance={showModalMaintenance}
                  toggleModalMaintenance={this._toggleModalMaintenance}
                />
              ) : (
                <View>
                  <Loader isLoading={true} />
                </View>
              )}
            </View>
          </View>
        </Animated.ScrollView>
        <RNAnimated.View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            backgroundColor: WHITE,
            paddingTop: getStatusBarHeight(true),
            opacity: this.state.headerOpacity,
          }}
        >
          <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
          <View>
            <MessageBarClearBlue />
          </View>
        </RNAnimated.View>
        <ModalMessageView
          aspectRatio={124 / 26}
          fullImage={true}
          toggleModal={() => this.setState({ showModalWelcome: !showModalWelcome })}
          imageStyle={{ width: 150, marginTop: 33 }}
          isVisible={
            showModalWelcome && !showBannerRegisterStartup && !showModalReferralCode
          }
          image={require('sf-assets/images/sfHorizontalPrimaryBrandmark.png')}
          buttonPrimaryAction={() => {
            this._setModalWelcome()
          }}
          titleSize={'extraMassive'}
          buttonPrimaryTextWeight={700}
          buttonPrimaryTextSize={'small'}
          buttonPrimaryBgColor={REDDISH}
          buttonPrimaryText={'Let’s start!'}
          title={'Welcome,\nFounders!'}
          subtitle={
            'Mari kembangkan kapasitas diri dan\njuga pengetahuan mengenai Startup\nagar kamu bisa berkontribusi secara\nnyata melalui aplikasi digital!'
          }
        />
      </Container>
    )
  }
}

HomeStartupScreen.propTypes = {
  navigateTo: PropTypes.func,
}

HomeStartupScreen.defaultProps = {
  navigateTo: () => {},
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(HomeStartupScreen),
  mapFromNavigationParam,
)
