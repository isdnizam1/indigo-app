import React, { Component } from 'react'
import { View, SafeAreaView, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { LinearGradient } from 'expo-linear-gradient'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { noop } from 'lodash-es'
import { Alert } from 'react-native'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { Icon, Image, Text } from '../../components'

import {
	CLEAR_BLUE,
	SHIP_GREY_CALM,
	SKELETON_COLOR,
	SKELETON_HIGHLIGHT,
	WHITE,
} from '../../constants/Colors'
import {
	WP100,
	WP105,
	WP3,
	WP4,
	WP50,
	WP6,
	WP70,
	WP8,
	WP2,
	WP1,
} from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'
import { getStatusBarHeight } from '../../utils/iphoneXHelper'
import { getLearningList } from '../../actions/api'
import { restrictedAction } from '../../utils/helper'
import styles from './styles'

const mapStateToProps = ({ auth }) => ({
	userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
	//
})

class StartupLearnScreen extends Component {
	constructor(props) {
		super(props)

		this.state = {
			isLoading: false,
			data: null,
			expandStatus: null
		}
	}

	async componentDidMount() {
		await this._getData()
	}

	_getData = async () => {
		this.setState({ isLoading: true })
		const { dispatch, userData } = this.props
		const expandStatusTemp = this.state.expandStatus
		try {
			const data = await dispatch(
				getLearningList,
				{
					id_user: userData.id_user
				},
				noop,
				true,
				false,
			)
			this.setState({ isLoading: false })
			if (data.code === 200) {
				const expandStatus = []
				for (let index = 0; index < data.result.length; index++) {
					expandStatus.push(false)
				}
				this.setState({
					expandStatus: expandStatusTemp && expandStatus.length === expandStatusTemp.length ? expandStatusTemp : expandStatus,
					data: data.result //JSON.parse(data.result.value.replace(/\\"n/g, ' '))
				})
			}
		} catch (e) {
			Alert.alert('Error', e)
			this.setState({ isLoading: false })
		}
	}

	_onPullDownToRefresh = () => {
		this.setState({ isLoading: true }, () => {
			this._getData()
		})
	};

	_headerSection = () => {
		const image = this.state.data ? this.state.data.image : null

		return (
			<View>
				<Image
					tint={'black'}
					imageStyle={{ width: WP100, height: undefined }}
					aspectRatio={360 / 180}
					source={
						image ? { uri: image } : require('../../assets/images/bgStartupVideoPodcast.png')
					}
				/>
				<LinearGradient
					style={{
						position: 'absolute',
						left: 0,
						top: 0,
						right: 0,
						bottom: 0,
					}}
					colors={[
						'rgba(0, 0, 0, 0)',
						'rgba(0, 0, 0, 0.5)',
						'rgba(0, 0, 0, 1)',
					]}
				>
					<View
						style={{
							paddingTop: getStatusBarHeight(false) + WP3,
							paddingHorizontal: WP4,
							paddingBottom: WP4,
						}}
					>
						<SkeletonContent
							containerStyle={{
								alignItems: 'center',
								paddingHorizontal: WP8,
								paddingVertical: WP3,
							}}
							layout={[
								{ width: WP50, height: WP6, marginBottom: WP105 },
								{ width: WP70, height: WP4 },
							]}
							isLoading={this.state.isLoading}
							boneColor={SKELETON_COLOR}
							highlightColor={SKELETON_HIGHLIGHT}
						>
							<View style={{ ...SHADOW_STYLE['shadowBold'] }}>
								<View style={{ marginTop: WP2 }} />
								<Text
									style={{ marginBottom: WP2 }}
									type='Circular'
									size='medium'
									weight={700}
									color={WHITE}
									centered
								>
									Learn
								</Text>
								<Text
									style={{ width: 250 }}
									type='Circular'
									size={12}
									lineHeight={21}
									weight={400}
									color={WHITE}
									centered
								>
									Edukasi untuk kamu para Founders membangun Startup impian
								</Text>
							</View>
						</SkeletonContent>
					</View>
				</LinearGradient>
			</View>
		)
	};

	_renderHeader = () => {
		const { navigateTo } = this.props
		return (
			<View
				style={{
					flexDirection: 'row',
					justifyContent: 'space-between',
					paddingHorizontal: WP4,
					paddingVertical: WP2,
				}}
			>
				<Icon
					centered
					onPress={() => this.props.navigation.goBack()}
					background='dark-circle'
					size='large'
					color={SHIP_GREY_CALM}
					name='chevron-left'
					type='Entypo'
				/>

				<TouchableOpacity
					onPress={restrictedAction({
						action: () => navigateTo('StartupSearchScreen', { searchType: 'learn' }),
						userData: this.props.userData,
						navigation: this.props.navigation,
					})}
					activeOpacity={0.75}
					style={{
						flex: 1,
						backgroundColor: 'rgba(145, 158, 171, 0.1)',
						borderRadius: 6,
						flexDirection: 'row',
						alignItems: 'center',
						paddingHorizontal: WP2,
						paddingVertical: WP1,
						marginHorizontal: WP2,
					}}
				>
					<Icon
						centered
						background='dark-circle'
						size='large'
						color={SHIP_GREY_CALM}
						name='magnify'
						type='MaterialCommunityIcons'
						style={{ marginRight: WP1 }}
					/>
					<Text
						type='Circular'
						size='mini'
						weight={300}
						color={SHIP_GREY_CALM}
						centered
					>
						Coba cari Pembelajaran
					</Text>
				</TouchableOpacity>
			</View>
		)
	};

	_itemContent = (item, index) => {
		if (!item.is_published) {
			return null
		}
		return (
			<TouchableOpacity
				style={styles.itemContentContainer} onPress={() => {
					if (!item.is_locked) {
						if (item.type === 'video') {
							this.props.navigateTo('PremiumVideoTeaser', {
								id_ads: item.id_ads,
								isStartup: true,
								refreshListVideo: () => { this._getData() }
							})
						} else if (item.type === 'sp_album') {
							this.props.navigateTo('SoundplayDetail', {
								id_ads: item.id_ads,
								isStartup: true,
								refreshListLearn: () => { this._getData() }
							})
						} else if (item.type === 'news_update') {
							this.props.navigateTo('NewsDetailScreen', {
								id_ads: item.id_ads,
								is1000Startup: true,
								refreshListLearn: () => { this._getData() }
							})
						} else if (item.type === 'sc_session') {
							this.props.navigateTo('SessionDetail', {
								id_ads: item.id_ads,
								isStartup: true,
								refreshListLearn: () => { this._getData() }
							})
						}
					}
				}}
			>
				<View style={{ flexDirection: 'row', flex: 3, marginRight: WP3 }}>
					<Image
						key={Math.random()}
						imageStyle={{ width: 25, height: undefined, tintColor: item.status === 'continue' ?'#3287EC' : '' }}
						aspectRatio={1}
						source={
							item.is_locked ? require('../../assets/icons/ic_learn_locked.png') : item.status === 'completed' ? require('../../assets/icons/ic_learn_complete.png') :
								item.status === 'continue' || item.status === 'not-played' ? require('../../assets/icons/ic_learn_active.png') :
									require('../../assets/icons/ic_learn_locked.png')

						}
					/>
					<View style={{ ...styles.itemContentTextContainer, flex: 1 }}>
						<Text
							type='Circular'
							size={'slight'}
							numberOfLines={2}
							lineHeight={18}
							weight={400}
							color={item.is_locked ? '#C4C4C4' : '#454F5B'}
						>
							{item.title}
						</Text>
						<View style={styles.itemContentTypeContainer}>
							<Image
								imageStyle={{ width: 15, height: undefined, marginRight: 5, opacity: item.is_locked ? 0.5 : 1 }}
								aspectRatio={1}
								source={
									item.type === 'video' ? require('../../assets/icons/ic_list_video.png') :
										item.type === 'sp_album' ? require('../../assets/icons/ic_list_podcast.png') :
											item.type === 'news_update' ? require('../../assets/icons/ic_list_article.png') :
												item.type === 'sc_session' ? require('../../assets/icons/ic_list_webinar.png') :
													require('../../assets/icons/ic_list_video.png')
								}
							/>
							<Text
								type='Circular'
								size={'xmini'}
								lineHeight={14}
								weight={400}
								color={item.is_locked ? '#C4C4C4' : '#454F5B'}
							>
								{
									item.type === 'video' ? 'Video' :
										item.type === 'sp_album' ? 'Podcast' :
											item.type === 'news_update' ? 'Article' :
												item.type === 'sc_session' ? 'Webinar' :
													item.type
								}
							</Text>
						</View>
					</View>
				</View>
				<View style={{
					...styles.itemContentStatusContainer,
					backgroundColor:
						item.is_locked ? '#C4C4C4' :
							item.status === 'not-played' ? '#FF651F' :
								item.status === 'completed' ? '#42B70B' :
									item.status === 'continue' ? CLEAR_BLUE : '#C4C4C4',
					flex: 1,
				}}
				>
					<Text
						type='Circular'
						size={'xmini'}
						lineHeight={14}
						weight={500}
						color={'#FFFFFF'}
					>{
							item.is_locked ? 'Locked' :
								item.status === 'not-played' && item.type === 'video' ? 'Watch' :
									item.status === 'not-played' && item.type === 'sc_session' ? 'Join' :
										item.status === 'not-played' && item.type === 'sp_album' ? 'Listen' :
											item.status === 'not-played' && item.type === 'news_update' ? 'Read' :
												item.status === 'not-played' ? 'Play' :
													item.status === 'completed' ? 'Completed' :
														item.status === 'continue' ? 'Continue' :
															item.status === 'locked' ? 'Locked' :
																''
						}</Text>
				</View>
			</TouchableOpacity>
		)
	};

	_itemLearn = (item, index) => {
		const { data } = this.state
		const { expandStatus } = this.state

		return (
			<View>
				<TouchableOpacity
					style={{
						...styles.itemCategoryContainer,
						borderTopLeftRadius: index === 0 ? 10 : 0,
						borderTopRightRadius: index === 0 ? 10 : 0,
						borderBottomLeftRadius: index === data.length - 1 ? 10 : 0,
						borderBottomRightRadius: index === data.length - 1 ? 10 : 0
					}}
					onPress={() => {
						const d = expandStatus
						d[index] = !d[index]

						this.setState({
							expandStatus: d
						})
					}}
					key={index}
				>
					<View style={{ ...styles.itemCategoryContentContainer, justifyContent: 'space-between' }}>
						<View style={{ flexDirection: 'row' }} >
							{item.icon ? (
								<Image
									imageStyle={{ width: 48, height: undefined }}
									aspectRatio={1}
									source={
										item.icon ? { uri: item.icon } :
											require('../../assets/icons/ic_list_learn.png')
									}
								/>
							)
								:
								(
									<ImageBackground style={{ width: 48, height: 48, justifyContent: 'center' }} source={require('../../assets/icons/icon_category.png')}>
										<Image
											imageStyle={{ width: 38, height: undefined }}
											aspectRatio={1}
											source={
												item.icon ? { uri: item.icon } :
													require('../../assets/icons/ic_list_learn.png')
											}
										/>
									</ImageBackground>
								)
							}

							<View style={styles.itemCategoryTextContainer}>
								<Text
									type='Circular'
									size={'large'}
									numberOfLines={2}
									weight={500}
									color={'#454F5B'}
								>
									{item.title}
								</Text>
							</View>
						</View>

						<View style={{ ...styles.iconRightContainer, marginRight: WP2 }}>
							<Icon
								size={24}
								background={'none'}
								name={expandStatus[index] ? 'chevron-down' : 'chevron-up'}
								type={'Entypo'}
								centered
								color={'#FF651F'}
							/>
						</View>
					</View>
					{expandStatus !== null && expandStatus[index] && <View style={{ ...styles.itemSeparator, marginHorizontal: 0, marginVertical: WP3 }} />}
					{expandStatus !== null && expandStatus[index] && item.content.map(this._itemContent)}
				</TouchableOpacity>
			</View>
		)
	};

	render() {
		const { data } = this.state

		return (
			<SafeAreaView style={styles.container}>
				{/* Todo: Header Search */}
				{this._renderHeader()}
				<ScrollView style={{ flex: 1, backgroundColor: '#F9FAFB' }}>
					<View style={styles.contentContainer}>
						<View style={styles.contentHeaderContainer}>
							{this._headerSection()}
							<Text type={'Circular'} weight={700} style={styles.titleText}>{'Pilih Pembelajaran'}</Text>
							<View style={styles.separator} />
						</View>
						{data !== null && data.map(this._itemLearn)}
					</View>
				</ScrollView>
			</SafeAreaView>
		)
	}
}

StartupLearnScreen.propTypes = {
	navigateTo: PropTypes.func,
}

StartupLearnScreen.defaultProps = {
	navigateTo: () => { },
}

export default _enhancedNavigation(
	connect(mapStateToProps, {})(StartupLearnScreen),
	mapFromNavigationParam,
)