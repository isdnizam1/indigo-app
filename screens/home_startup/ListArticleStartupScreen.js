import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { LinearGradient } from 'expo-linear-gradient'
import { View } from 'react-native'
import { GREY_CALM_SEMI, SHADOW_GRADIENT } from '../../constants/Colors'
import { Container } from '../../components'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { HEADER } from '../../constants/Styles'
import HeaderNormal from '../../components/HeaderNormal'
import NewsTab from '../timeline/NewsTab'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

class ListArticleStartupScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
    }
  }

  async componentDidMount() {}

  render() {
    const { isLoading } = this.state
    const { navigateBack } = this.props
    return (
      <Container
        theme='dark'
        isLoading={isLoading}
        isReady={true}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={navigateBack}
              text={'Articles'}
              textSize={'slight'}
              centered
            />
            <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
          </View>
        )}
        outsideScrollContent={() => {}}
        scrollable
        scrollBackgroundColor={GREY_CALM_SEMI}
        scrollContentContainerStyle={{
          justifyContent: 'space-between',
        }}
      >
        <NewsTab navigation={this.props.navigation} is1000Startup={true} />
      </Container>
    )
  }
}

ListArticleStartupScreen.propTypes = {
  navigateTo: PropTypes.func,
}

ListArticleStartupScreen.defaultProps = {
  navigateTo: () => {},
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(ListArticleStartupScreen),
)
