import React, { Component } from 'react'
import { View, SafeAreaView, ScrollView } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { LinearGradient } from 'expo-linear-gradient'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { noop } from 'lodash-es'
import { Alert } from 'react-native'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { HeaderNormal, Image, Text } from '../../components'

import {
  GUN_METAL,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
} from '../../constants/Colors'
import {
  WP100,
  WP105,
  WP3,
  WP4,
  WP50,
  WP6,
  WP70,
  WP8,
  WP24,
  WP2,
  WP1,
} from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'
import { getStatusBarHeight } from '../../utils/iphoneXHelper'
import {
  getFaqData,
  getRewardData,
  getTermsData,
  getTimelineData,
} from '../../actions/api'
import styles from './styles'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  //
})

class DetailGeneralContentScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
      id: props.route.params.id,
    }
  }

  async componentDidMount() {
    await this._getData()
  }

  _getData = async () => {
    this.setState({ isLoading: true })
    const { dispatch } = this.props
    const { id } = this.state

    try {
      const data = await dispatch(
        id === 'faq'
          ? getFaqData
          : id === 'timeline'
          ? getTimelineData
          : id === 'terms'
          ? getTermsData
          : getRewardData,
        { platform: '1000startup' },
        noop,
        true,
        false,
      )
      this.setState({ isLoading: false })
      if (data.code === 200) {
        this.setState({
          data: data.result,
        })
      }
    } catch (e) {
      Alert.alert('Error', e)
      this.setState({ isLoading: false })
    }
  };

  _onPullDownToRefresh = () => {
    this.setState({ result: {}, isLoading: true }, () => {
      this._getData()
    })
  };

  _headerSection = () => {
    const image = this.state.data ? this.state.data.image : null
    const title = this.state.data ? this.state.data.title : null
    const subtitle = this.state.data ? this.state.data.subtitle : null

    return (
      <View>
        <Image
          tint={'black'}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 180}
          source={
            image
              ? { uri: image }
              : this.state.id === 'faq'
              ? require('../../assets/images/bgFaq.png')
              : this.state.id === 'timeline'
              ? require('../../assets/images/bgTimeline.png')
              : this.state.id === 'terms'
              ? require('../../assets/images/bgTerms.png')
              : require('../../assets/images/bgRewards.png')
          }
        />
        <LinearGradient
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
          }}
          colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0.5)', 'rgba(0, 0, 0, 1)']}
        >
          <View
            style={{
              paddingTop: getStatusBarHeight(false) + WP3,
              paddingHorizontal: WP4,
              paddingBottom: WP4,
            }}
          >
            <SkeletonContent
              containerStyle={{
                alignItems: 'center',
                paddingHorizontal: WP8,
                paddingVertical: WP3,
              }}
              layout={[
                { width: WP50, height: WP6, marginBottom: WP105 },
                { width: WP70, height: WP4 },
              ]}
              isLoading={this.state.isLoading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View style={{ ...SHADOW_STYLE['shadowBold'] }}>
                <Image
                  aspectRatio={160 / 108}
                  imageStyle={{ width: WP24, height: undefined }}
                  source={require('sf-assets/images/homeStartup.png')}
                />
                <View style={{ marginTop: WP2 }} />
                <Text
                  // style={{ marginTop: WP2 }}
                  type='Circular'
                  size='small'
                  weight={700}
                  color={WHITE}
                  centered
                >
                  {title
                    ? title
                    : this.state.id === 'faq'
                    ? 'Frequently Asked Question'
                    : this.state.id === 'timeline'
                    ? 'Timeline'
                    : this.state.id === 'terms'
                    ? 'Terms & Condition'
                    : 'Rewards'}
                </Text>
                <Text
                  // style={{ marginTop: WP2 }}
                  type='Circular'
                  size={12}
                  lineHeight={21}
                  weight={400}
                  color={WHITE}
                  centered
                >
                  {subtitle
                    ? subtitle
                    : this.state.id === 'faq'
                    ? 'Semua pertanyaan umum dari program 1000 Startup Indonesia Timur'
                    : this.state.id === 'timeline'
                    ? 'Alur waktu dari program 1000 Startup Indonesia Timur'
                    : this.state.id === 'terms'
                    ? 'Semua syarat dan ketentuan dalam program 1000 Startup Indonesia Timur'
                    : 'Keuntungan dan hadiah dari program 1000 Startup Indonesia Timur'}
                </Text>
              </View>
            </SkeletonContent>
          </View>
        </LinearGradient>
      </View>
    )
  };

  _itemFaqAnswer = (data, index) => {
    return (
      <View style={styles.itemFaqAnswerContainer} key={index}>
        <View style={styles.dotAnswer} />
        <Text
          type='Circular'
          color={SHIP_GREY_CALM}
          weight={400}
          size={12}
          lineHeight={20}
          style={styles.answerText}
        >
          {data}
        </Text>
      </View>
    )
  };

  _itemFaq = (data, index) => {
    return (
      <View style={styles.itemFaqContainer} key={index}>
        <Text
          type={'Circular'}
          color={SHIP_GREY}
          weight={500}
          size={12}
          lineHeight={24}
          style={styles.questionText}
        >
          {data.question}
        </Text>
        {data.answer.map(this._itemFaqAnswer)}
      </View>
    )
  };

  _faqSection = () => {
    const { data } = this.state

    if (data === undefined) {
      return null
    }

    const title =
      this.state.id === 'faq'
        ? 'Pertanyaan Umum dari 1000 Startup Indonesia Timur'
        : this.state.id === 'timeline'
        ? 'Timeline 1000 Startup Indonesia Timur'
        : this.state.id === 'terms'
        ? 'Syarat dan Ketentuan 1000 Startup Indonesia Timur'
        : this.state.id === 'rewards'
        ? 'Yang akan kamu dapatkan dari program 1000 Startup Indonesia Timur'
        : ''

    const content = data.description

    const question =
      this.state.id === 'terms'
        ? 'Berikut adalah syarat dan ketentuan untuk bisa mengikuti program “1000 Startup Indonesia Timur”:'
        : this.state.id === 'rewards'
        ? 'Dengan mengikuti program 1000 Startup Indonesia Timur, kamu akan mendapatkan keuntungan dan hadiah sebagai berikut:'
        : null

    return (
      <View style={{ paddingTop: WP1 * 1.25 }}>
        <Text type={'Circular'} weight={700} style={styles.titleText}>
          {title}
        </Text>
        <View style={styles.separator} />
        {this.state.id === 'faq' && content.map(this._itemFaq)}
        {this.state.id !== 'faq' && (
          <View style={styles.itemFaqContainer}>
            <Text
              type={'Circular'}
              color={SHIP_GREY}
              weight={500}
              size={12}
              lineHeight={16}
              style={styles.questionText}
            >
              {question}
            </Text>
            {content.map(this._itemFaqAnswer)}
          </View>
        )}
      </View>
    )
  };

  _timelineItem = (data, index) => {
    return (
      <View style={styles.timelineItemContainer} key={index}>
        <View>
          {/* {index === 0 ? (
						<View style={styles.activeDotContainer}>
							<View style={styles.activeDot}/>
						</View>
					):
					( */}
          <View style={styles.dotAnswer} />
          {/* )} */}
          <View style={styles.lineProgress} />
        </View>
        <View style={styles.timelineDataContainer}>
          <Text
            type={'Circular'}
            color={GUN_METAL}
            weight={400}
            size={12}
            lineHeight={15}
            style={styles.timelinePhaseText}
          >
            {data.phase}
          </Text>
          <Text
            type={'Circular'}
            color={GUN_METAL}
            weight={500}
            size={14}
            lineHeight={17}
            style={styles.timelineTitleText}
          >
            {data.title}
          </Text>
          <Text
            type={'Circular'}
            color={SHIP_GREY_CALM}
            weight={400}
            size={12}
            lineHeight={15}
            style={styles.timelineDateText}
          >
            {data.date}
          </Text>
        </View>
      </View>
    )
  };

  _timelineSection = () => {
    const { data } = this.state

    if (data === undefined) {
      return null
    }

    const content = data.description

    return (
      <View>
        <Text type={'Circular'} weight={700} style={styles.titleText}>
          {'Timeline 1000 Startup Indonesia Timur'}
        </Text>
        <View style={styles.separator} />
        <View style={styles.timelineContainer}>
          {content && content.map(this._timelineItem)}
        </View>
      </View>
    )
  };

  render() {
    const { id } = this.state
    const title =
      id === 'faq'
        ? 'FAQ'
        : id === 'timeline'
        ? 'Timeline'
        : id === 'terms'
        ? 'Terms & Condition'
        : id === 'rewards'
        ? 'Rewards'
        : ''

    return (
      <SafeAreaView style={styles.container}>
        <HeaderNormal
          centered={true}
          text={title}
          iconLeftOnPress={() => this.props.navigation.goBack()}
        />
        <ScrollView>
          <View style={styles.contentContainer}>
            {this._headerSection()}
            {id !== 'timeline' && this._faqSection()}
            {id === 'timeline' && this._timelineSection()}
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

DetailGeneralContentScreen.propTypes = {
  navigateTo: PropTypes.func,
}

DetailGeneralContentScreen.defaultProps = {
  navigateTo: () => {},
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(DetailGeneralContentScreen),
  mapFromNavigationParam,
)
