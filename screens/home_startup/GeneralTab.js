import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { PALE_BLUE } from 'sf-constants/Colors'
import { useRef } from 'react'
import { ImageBackground } from 'react-native'
import { AnimatedCircularProgress } from 'react-native-circular-progress'
import { Image, ModalMessageView, Text } from '../../components'
import {
  GREEN,
  NAVY_DARK,
  PALE_GREY_THREE,
  REDDISH,
  RED_20,
  RED_GOOGLE,
  SHIP_GREY,
  TRANSPARENT,
  WHITE,
} from '../../constants/Colors'
import {
  WP1,
  WP10,
  WP100,
  WP105,
  WP12,
  WP15,
  WP18,
  WP2,
  WP20,
  WP205,
  WP3,
  WP4,
  WP5,
  WP6,
  WP80,
} from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import GeneralTabItem from '../../components/home_startup/items/GeneralTabItem'
import Spacer from '../../components/Spacer'
import { GENERAL_TAB_ITEM, GENERAL_TAB_ITEM_LEADING } from './Constans'

const GeneralTab = ({
  loading,
  announcement,
  getData,
  navigateTo,
  generalData,
  onEditProfile,
  showModalMaintenance,
  toggleModalMaintenance,
}) => {
  const _renderGeneralItem = (item, index) => {
    const { id, title, bgItem, icon, gradient } = item
    return (
      <TouchableOpacity
        disabled={false}
        style={{ paddingVertical: WP2, flex: 1 }}
        key={`${index}${Math.random()}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={() => {}}
      >
        <GeneralTabItem
          gradient={gradient}
          title={title}
          icon={icon}
          bgItem={bgItem}
          id={id}
          navigateTo={navigateTo}
          toggleModalMaintenance={toggleModalMaintenance}
        />
      </TouchableOpacity>
    )
  }
  const Divider = () => {
    return (
      <View
        style={{
          height: 1.25,
          backgroundColor: PALE_BLUE,
          marginVertical: WP2,
          marginHorizontal: WP5 * 0.925,
        }}
      />
    )
  }
  const _onPressLearn = () => {
    navigateTo('StartupLearnScreen')
  }
  const _onPressMission = () => {
    navigateTo('StartupMissionHomeScreen')
  }
  return (
    <View
      isReady={!loading}
      isLoading={loading}
      theme='light'
      noStatusBarPadding
      scrollable
      scrollBackgroundColor={TRANSPARENT}
      onPullDownToRefresh={getData}
    >
      <Spacer size={WP2} />
      <Text
        type='Circular'
        size='medium'
        color={NAVY_DARK}
        weight={600}
        style={{ padding: WP6, paddingTop: WP3, paddingBottom: 0 }}
      >
        {'What\'s New'}
      </Text>
      <Text
        type='Circular'
        size='mini'
        color={SHIP_GREY}
        weight={300}
        style={{ padding: WP6, paddingTop: 0, paddingBottom: 0 }}
      >
        Check your new learn or mission
      </Text>
      <Spacer size={WP3} />
      <View style={{ paddingHorizontal: WP6, paddingBottom: WP4 }}>
        {announcement && announcement !== null && (
          <TouchableOpacity onPress={onEditProfile}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: 'rgba(255, 101, 31, 0.12)',
                padding: WP4,
                borderRadius: WP105,
                marginBottom: WP3,
              }}
            >
              <Image
                size={'mini'}
                source={require('../../assets/icons/infoCircleTomato.png')}
              />
              <View style={{ flex: 1, paddingLeft: WP3 }}>
                <Text color={'rgba(255, 101, 31, 1)'} weight={700} size={'slight'}>
                  {announcement.title}
                </Text>
                <Text color={'rgba(255, 156, 110, 1)'} size={'tiny'}>
                  {announcement.description}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
        <TouchableOpacity onPress={_onPressLearn}>
          <ImageBackground
            source={require('../../assets/images/group104.jpg')}
            style={{
              height: WP15 + WP3,
              width: '100%',
              resizeMode: 'cover',
              elevation: 2,
              marginBottom: WP3,
            }}
            imageStyle={{ borderRadius: WP3 }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                height: WP15 + WP3,
              }}
            >
              <View style={{ paddingHorizontal: WP3 }}>
                <AnimatedCircularProgress
                  size={WP12}
                  width={3.5}
                  fill={generalData.learn_progress_percentase}
                  tintColor={GREEN}
                  rotation={0}
                  backgroundColor={WHITE}
                >
                  {(fill) => (
                    <Text size={'xmini'} color={WHITE}>
                      {generalData.learn_progress_percentase}%
                    </Text>
                  )}
                </AnimatedCircularProgress>
              </View>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text color={WHITE} size={'medium'} weight={700}>
                  Learn
                </Text>
                <Text color={PALE_GREY_THREE} size={'xmini'}>
                  Mulai pembelajaran kamu sekarang
                </Text>
                <Spacer size={WP1} />
              </View>
              <View style={{ paddingHorizontal: WP4 }}>
                <Image
                  size={'xmini'}
                  source={require('../../assets/icons/arrowWhiteTransparent.png')}
                />
                <View
                  style={{
                    width: WP205,
                    height: WP205,
                    backgroundColor: '#FF1515',
                    borderRadius: WP105,
                    position: 'absolute',
                    right: WP3,
                    top: -WP1,
                  }}
                />
              </View>
            </View>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity onPress={_onPressMission}>
          <ImageBackground
            source={require('../../assets/images/group104.jpg')}
            style={{
              height: WP15 + WP3,
              width: '100%',
              resizeMode: 'cover',
              elevation: 2,
            }}
            imageStyle={{ borderRadius: WP3 }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                height: WP15 + WP3,
              }}
            >
              <View style={{ width: WP18, alignItems: 'center' }}>
                <Text color={WHITE} weight={500} size={'extraMassive'}>
                  {generalData.mission_progress.complete_mission}/
                  {generalData.mission_progress.total_mission}
                </Text>
              </View>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text color={WHITE} size={'medium'} weight={700}>
                  Mission
                </Text>
                <Text color={PALE_GREY_THREE} size={'xmini'}>
                  Kumpulkan poin rewardmu sekarang
                </Text>
                <Spacer size={WP1} />
              </View>
              <View style={{ paddingHorizontal: WP4 }}>
                <Image
                  size={'xmini'}
                  source={require('../../assets/icons/arrowWhiteTransparent.png')}
                />
              </View>
            </View>
          </ImageBackground>
        </TouchableOpacity>
      </View>
      <Spacer size={WP1} />
      <View style={{ paddingHorizontal: WP1 * 1.25 }}>
        <Divider />
      </View>
      <Text
        type='Circular'
        size='medium'
        color={NAVY_DARK}
        weight={600}
        style={{ padding: WP6, paddingTop: WP3, paddingBottom: 0 }}
      >
        What would you like to know?
      </Text>
      <Text
        type='Circular'
        size='mini'
        color={SHIP_GREY}
        weight={300}
        style={{ padding: WP6, paddingTop: 0, paddingBottom: 0 }}
      >
        {'Your Founder\'s directory menu'}
      </Text>
      <Spacer size={WP105} />
      {/* <>
        <View style={{ padding: 1, backgroundColor: '#F4F6F8' }} />
        {!isEmpty(profile) && (
          <CompleteProfileCard
            profileProgress={profileProgress?.profile_progress}
            profile={profile}
            editProfile={onEditProfile}
          />
        )}
      </> */}
      <View
        style={{
          flexDirection: 'row',
          width: WP100,
          paddingHorizontal: WP105 * 2.5,
        }}
      >
        {React.Children.toArray(GENERAL_TAB_ITEM_LEADING.map(_renderGeneralItem))}
      </View>
      <View style={{ paddingHorizontal: WP1 * 1.25 }}>
        <Divider />
      </View>
      <View
        style={{
          flexDirection: 'row',
          width: WP100,
          paddingHorizontal: WP105 * 2.5,
        }}
      >
        {React.Children.toArray(GENERAL_TAB_ITEM.map(_renderGeneralItem))}
      </View>
      <ModalMessageView
        aspectRatio={838 / 676}
        fullImage
        toggleModal={toggleModalMaintenance}
        imageStyle={{ width: WP80, marginTop: 33 }}
        isVisible={showModalMaintenance}
        image={require('sf-assets/images/bgUnderDevelopment.png')}
        buttonPrimaryAction={toggleModalMaintenance}
        titleSize={'massive'}
        buttonPrimaryTextWeight={500}
        buttonPrimaryTextSize={'small'}
        buttonPrimaryBgColor={REDDISH}
        buttonPrimaryText={'OK'}
        title={'Coming Soon'}
        subtitle={
          'This page is still under maintenance. We’ll inform you when the page is ready.'
        }
      />
    </View>
  )
}

export default GeneralTab
