import React, { Component, Fragment } from 'react'
import {
  Animated,
  FlatList,
  RefreshControl,
  StatusBar as StatusBarRN,
  View,
  Modal as RNModal,
  ScrollView,
} from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import { isEmpty, noop, get } from 'lodash-es'
import * as Animatable from 'react-native-animatable'
import moment from 'moment'
import Touchable from 'sf-components/Touchable'
import MenuOptions from 'sf-components/MenuOptions'
import { ORANGE_BRIGHT, GUN_METAL, PALE_BLUE_TWO } from 'sf-constants/Colors'
import { isIOS } from 'sf-utils/helper'
import { setData } from '../../services/screens/lv1/lucky-fren/actionDispatcher'
import { _enhancedNavigation, Container, Icon, Image, Text } from '../../components'
import {
  NAVY_DARK,
  NO_COLOR,
  PALE_BLUE,
  PALE_GREY_THREE,
  PALE_SALMON,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from '../../constants/Colors'
import {
  FONT_SIZE,
  HP10,
  WP1,
  WP100,
  WP105,
  WP15,
  WP2,
  WP20,
  WP3,
  WP305,
  WP4,
  WP5,
  WP50,
  WP6,
  HP40,
} from '../../constants/Sizes'
import {
  getStartupMissionHome,
  postCheckInTodayStartup,
  postLogActivity,
} from '../../actions/api'
import { getStatusBarHeight, getBottomSpace } from '../../utils/iphoneXHelper'
import { SHADOW_STYLE } from '../../constants/Styles'
import { FONTS } from '../../constants/Fonts'
import InteractionManager from '../../utils/InteractionManager'
import RankingItem from '../../components/lucky_fren/RankingItem'
import CheckInItem from '../../components/lucky_fren/CheckInItem'
import SliderMessage from '../../components/lucky_fren/SliderMessage'
import RewardItem from '../../components/lucky_fren/RewardItem'
import MissionItem from '../../components/lucky_fren/MissionItem'
import PopUpInfoTrigger from '../../components/popUp/PopUpInfoTrigger'
import Countdown from '../../components/lucky_fren/Countdown'

import { showReviewIfNotYet } from '../../utils/review'

const StatusBar = Animatable.createAnimatableComponent(StatusBarRN)
const modalConfig = {
  terms: {
    title: 'Syarat & Ketentuan: Hadiah & Peserta',
    items: [
      'Satu ID pengguna hanya dapat mengklaim satu hadiah.',
      'Jumlah hadiah terbatas atau selama persedian masih ada.',
      'Pengguna dapat mengklaim dan share hadiah jika ingin menukarkan star dengan hadiah yang ada.',
      'Pemenang yang berhasil mengklaim hadiah akan dihubungi oleh pihak Soundfren untuk mengkonfirmasi nama, alamat (digunakan sebagai tujuan pengiriman hadiah), nomor handphone dan alamat e-mail dalam waktu 3x24 jam.',
      'Nomor yang akan menghubungi adalah nomor official Soundfren dan email official Soundfren (contact center: 0813 1095 7673/contact service: hello@soundfren.id/).',
      'Apabila dalam 3 hari pemenang tidak dapat dihubungi, maka hadiah dianggap batal/hangus.',
      'Hadiah akan dikirimkan kepada pemenang dalam waktu maksimal 14 hari setelah melakukan konfirmasi data diri.',
      'Hadiah digital dikirim maksimal 3x24 jam.',
      'Program ini hanya bisa diikuti oleh pengguna yang telah terdaftar di aplikasi Soundfren.',
      'Pengguna punya kesempatan untuk mengumpulkan Star dengan mengikuti login harian, misi harian dan misi spesial.',
      'Peserta dapat mengklaim hadiah jika nominal Star telah mencukupi sesuai dengan ketentuan yang berlaku.',
      'Pemenang akan mendapatkan pesan pemberitahuan di aplikasi Soundfren ketika berhasil mengklaim hadiah yang tersedia.',
      'Jika menurut pertimbangan Soundfren terdapat indikasi/dugaan kecurangan atau kesalahan yang disengaja dalam pemberian informasi atau data yang diberikan oleh pemenang dalam mengikuti program maka pemenang didiskualifikasi sesuai pemberitahuan Soundfren, tanpa kompensasi apapun dari Soundfren kepada pemenang.',
      'Soundfren berhak mengubah syarat dan ketentuan yang berlaku, termasuk menghentikan/memperpanjang reward ini kapanpun tanpa pemberitahuan terlebih dahulu.',
      'Karyawan atau keluarga inti Karyawan tidak diperbolehkan untuk berpartisipasi dalam program yang diselenggarakan oleh Soundfren.',
      'Apabila karyawan atau Keluarga inti Karyawan secara sengaja memenangkan suatu program yang diselenggarakan oleh Soundfren maka hadiah tersebut akan dibatalkan.',
      'Dengan mengikuti kegiatan ini, pengguna dianggap mengerti dan menyetujui semua syarat dan ketentuan yang berlaku.',
    ],
  },
  about: {
    title: 'Tentang Mission',
    items: [
      'Lucky Fren merupakan program berhadiah dari Soundfren. Pengguna Soundfren memiliki kesempatan untuk mendapatkan hadiah dengan menyelesaikan misi yang tersedia dengan cara menukarkan point yang didapatkan dengan hadiah yang tentunya menarik untuk didapatkan.',
      'Cara mainnya gampang banget! kamu cuma perlu Check-In setiap hari untuk mendapatkan point harian. Tidak hanya itu, setiap minggunya akan ada kejutan hadiah menarik lainnya yang bisa kamu dapatkan dari lucky box hanya dengan melakukan check in harian.',
      'Ada berbagai hadiah menarik lainnya yang kamu bisa dapatkan lho!. Salah satunya Microphone Condenser yang bisa kamu pakai untuk keperluan home recording dan lain-lainnya.',
      'Kamu tidak perlu khawatir kekurangan point untuk mendapatkan hadiah yang kamu inginkan, di Lucky Fren kamu bisa tambah pointmu Untuk mendapatkan point lebih banyak dengan cara menyelesaikan misi harian dan misi spesial yang ada.',
    ],
  },
}
const luckyConfig = {
  reward: {
    title: 'Tukar Poinmu & Dapatkan Hadiahnya',
    subtitle: '',
    route: '',
    key_data: 'list_reward',
    line: false,
    horizontal: true,
    numColumns: undefined,
    empty_message: 'Maaf, untuk saat ini belum ada Hadiah yang tersedia',
  },
  mission: {
    title: 'Misi',
    subtitle: 'Ikuti misi & kumpulkan poinmu',
    route: '',
    key_data: 'mission_category',
    line: true,
    horizontal: true,
    numColumns: undefined,
    empty_message: 'Maaf, untuk saat ini belum ada Misi yang tersedia',
  },
  ranking: {
    title: 'Peringkat Sementara',
    subtitle: '',
    route: 'ListRankingScreen',
    routeParams: {
      is1000Startup: true,
    },
    key_data: 'leaderboard',
    line: true,
    horizontal: false,
    numColumns: undefined,
    empty_message: 'Maaf, untuk saat ini belum ada Daftar peringkat poin',
  },
}

const mapStateToProps = ({
  auth,
  screen: {
    lv1: { luckyfren },
  },
}) => ({
  userData: auth.user,
  luckyfren,
})

const mapDispatchToProps = {
  setData,
}

const mapFromNavigationParam = (getParam) => ({
  target: getParam('target', ''),
})

class StartupMissionHomeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
      scrollY: new Animated.Value(0),
      isLoading: false,
      templatePopUp: {},
      modalShown: null,
    }
    this.layout = {}
  }

  componentDidMount = () => {
    InteractionManager.runAfterInteractions(async () => {
      await this._getContent()
      if (this.props.target) {
        this.scrollViewRef.scrollTo({
          y: this.layout[this.props.target].y - HP10,
        })
      }
      postLogActivity({
        value: 'gamification',
        id_user: this.props.userData.id_user,
      }).then(() => {})
    })
  };

  _getContent = async () => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props
    // alert(id_user)
    let params = { id_user, platform: '1000startup' }
    const { result } = await dispatch(
      getStartupMissionHome,
      params,
      noop,
      true,
      false,
    )
    if (result) {
      // if(__DEV__) result.is_expired = __DEV__;
      this.props.setData({ result, isLoaded: true })
    }

    if (this.state.isLuckyBox) {
      setTimeout(() => showReviewIfNotYet(), 1000)
    }
  };

  _actionCheckin = async () => {
    const {
      dispatch,
      navigateTo,
      userData: { id_user },
    } = this.props
    try {
      let body = { id_user }
      const { code, result } = await dispatch(
        postCheckInTodayStartup,
        body,
        noop,
        true,
        false,
      )
      this.setState({ isLoading: false })
      if (code == 200) {
        const { lucky_box, point } = result?.checked_in
        const isLuckyBox = lucky_box == 1
        let templatePopUp = {
          closeColor: isLuckyBox ? SHIP_GREY : WHITE,
          image: isLuckyBox
            ? require('sf-assets/images/bgLuckyBox.png')
            : require('sf-assets/images/bgMultipleCoin.png'),
          aspectRatio: 296 / 184,
          title: isLuckyBox
            ? `Lucky Box & ${point} Poin`
            : `Mendapat ${point} Poin Tambahan`,
          multipleContent: true,
          content: [
            { style: 'normal', text: 'Selamat! kamu mendapatkan ' },
            {
              style: 'bold',
              text: `${point} Poin Tambahan ${isLuckyBox ? '& Lucky Box ' : ''}`,
            },
            { style: 'normal', text: 'dari ' },
            { style: 'bold', text: 'Check In hari ini. ' },
            {
              style: 'normal',
              text: isLuckyBox
                ? 'Ambil & buka hadiahnya!'
                : 'Ambil & kumpulkan pointnya!',
            },
          ],
          cta: isLuckyBox ? 'Ambil Poin & Hadiah' : 'Ambil Poin',
          onPress: isLuckyBox
            ? () => navigateTo('LuckyBoxScreen', { is1000Startup: true })
            : noop,
        }
        this.setState({ templatePopUp, isLuckyBox }, this.popUp._show())
        if (!isLuckyBox) setTimeout(() => showReviewIfNotYet(), 1000)
        this._getContent()
      }
    } catch (error) {
      this.setState({ isLoading: false })
    }
  };

  _headerSection = () => {
    // eslint-disable-next-line no-unused-vars
    const { navigateBack } = this.props
    const bgColor = this.state.scrollY.interpolate({
      inputRange: [-10, 0, WP5, WP15],
      outputRange: [WHITE, NO_COLOR, NO_COLOR, WHITE],
      extrapolate: 'clamp',
    })
    const iconColor = this.state.scrollY.interpolate({
      inputRange: [-10, 0, WP5, WP20],
      outputRange: [SHIP_GREY_CALM, WHITE, WHITE, SHIP_GREY_CALM],
      extrapolate: 'clamp',
    })
    return (
      <View style={{ width: WP100, position: 'absolute', zIndex: 999, top: 0 }}>
        <StatusBar
          animated
          translucent={true}
          backgroundColor={bgColor}
          barStyle={'dark-content'}
        />
        <Animated.View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: bgColor,
            paddingTop: getStatusBarHeight(true),
          }}
        >
          <Touchable
            onPress={() => navigateBack()}
            style={{ padding: WP2, paddingHorizontal: WP4 }}
          >
            <Icon
              centered
              background='dark-circle'
              backgroundColor='rgba(255,255,255, 0.16)'
              size='huge'
              color={iconColor}
              name='chevron-left'
              type='Entypo'
            />
          </Touchable>

          <Animated.View
            style={{
              flex: 1,
              borderRadius: 6,
              alignItems: 'center',
              padding: WP2,
              marginHorizontal: WP2,
            }}
          >
            <Animated.Text
              style={{
                color: iconColor,
                fontFamily: FONTS['Circular'][400],
                fontSize: FONT_SIZE['mini'],
                lineHeight: WP5,
              }}
              centered
            >
              Mission
            </Animated.Text>
          </Animated.View>
          <MenuOptions
            onClose={noop}
            options={[
              {
                type: 'menu',
                image: require('sf-assets/icons/badgeSoundfrenPremium.png'),
                imageStyle: {},
                title: 'Premium',
                imageSize: WP6,
                titleStyle: {
                  color: ORANGE_BRIGHT,
                  fontFamily: FONTS.Circular['500'],
                },
                onPress: () => {
                  this.props.navigateTo('MembershipScreen', {
                    paymentSource: '1000 Startup Gamification',
                  })
                },
                joinButton: false,
                isPremiumMenu: true,
              },
              {
                type: 'menu',
                image: require('sf-assets/icons/icAboutShipGrey.png'),
                imageStyle: {},
                imageSize: WP6,
                title: 'Tentang Mission',
                textStyle: {},
                onPress: () => {
                  setTimeout(
                    () => this.setState({ modalShown: 'about' }),
                    isIOS() ? 500 : 0,
                  )
                },
              },
              {
                type: 'menu',
                iconName: 'clipboard-text',
                iconSize: 'huge',
                title: 'Syarat & Ketentuan',
                textStyle: {},
                onPress: () => {
                  setTimeout(
                    () => this.setState({ modalShown: 'terms' }),
                    isIOS() ? 500 : 0,
                  )
                },
              },
              {
                type: 'menu',
                image: require('sf-assets/icons/mdi_chat.png'),
                imageSize: WP6,
                imageStyle: {},
                title: 'Feedback & Report',
                textStyle: {},
                onPress: () => {
                  this.props.navigateTo('FeedbackScreen')
                },
              },
            ]}
            triggerComponent={(toggleModal) => {
              this.toggleModal = toggleModal
              return (
                <Fragment>
                  <View style={{ padding: WP2, paddingHorizontal: WP4 }}>
                    <Icon
                      centered
                      background='dark-circle'
                      size='huge'
                      color={'transparent'}
                      name='dots-three-horizontal'
                      type='Entypo'
                    />
                  </View>
                </Fragment>
              )
            }}
          />
        </Animated.View>
      </View>
    )
  };

  _cardPointSection = (result) => {
    const { navigateTo } = this.props
    const availGift = result?.lucky_box
    return (
      <View>
        <Image
          tint={'black'}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 156}
          source={require('sf-assets/images/bgLuckyReddish.png')}
        />
        <View
          style={{
            width: '100%',
            paddingHorizontal: WP4,
            marginTop: -((WP100 * 156) / 360 / 2.25),
            marginBottom: WP6,
          }}
        >
          <View
            style={{
              borderRadius: 6,
              backgroundColor: WHITE,
              ...SHADOW_STYLE['shadowThin'],
            }}
          >
            <View style={{ width: '100%', overflow: 'hidden' }}>
              <RankingItem
                isStartup={true}
                loading={this.state.isRefreshing}
                image={result?.profile_picture}
                name={result?.full_name}
                position={result?.leaderboard_position}
                point={result?.total_point}
                isList={false}
              />
              <View
                style={{
                  width: '100%',
                  borderTopColor: PALE_GREY_THREE,
                  borderTopWidth: 1.5,
                }}
              >
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Touchable
                    style={{
                      flex: 1,
                      paddingVertical: WP305,
                      paddingHorizontal: WP4,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() =>
                      navigateTo('LuckyBoxScreen', { is1000Startup: true })
                    }
                  >
                    {availGift && (
                      <Icon
                        centered
                        type='MaterialCommunityIcons'
                        name='circle'
                        size={WP2}
                        color={REDDISH}
                        style={{ marginRight: WP1 }}
                      />
                    )}
                    <Image
                      style={{ marginRight: WP1 }}
                      size='xtiny'
                      source={require('../../assets/icons/icGift.png')}
                    />
                    <Text size='xmini' weight={400} color={SHIP_GREY} centered>
                      Kotak Hadiah
                    </Text>
                  </Touchable>
                  <View
                    style={{
                      height: '100%',
                      width: 1.5,
                      backgroundColor: PALE_GREY_THREE,
                    }}
                  />
                  <Touchable
                    style={{
                      flex: 1,
                      paddingVertical: WP305,
                      paddingHorizontal: WP4,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={() =>
                      navigateTo('MissionScreen', {
                        isExpired: this.props.luckyfren.result.is_expired,
                        is1000Startup: true,
                      })
                    }
                  >
                    <Image
                      style={{ marginRight: WP1 }}
                      size='xtiny'
                      source={require('../../assets/icons/icPoint.png')}
                    />
                    <Text size='xmini' weight={400} color={SHIP_GREY} centered>
                      Tambah Poin
                    </Text>
                  </Touchable>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  };

  _listCheckInSection = (result, isLoaded) => {
    const isCheckIn = result?.checked_in
    const dummyData = [1, 2, 3, 4, 5, 6, 7]
    let dataCheckIn = !isLoaded ? dummyData : result?.daily_checkin
    return (
      <View>
        <View style={{ paddingHorizontal: WP4 }}>
          <Text
            lineHeight={WP6}
            type='Circular'
            size='slight'
            color={NAVY_DARK}
            weight={600}
          >
            Check In Harian
          </Text>
          {!isCheckIn && (
            <View
              style={{ flexDirection: 'row', alignItems: 'center', marginTop: WP1 }}
            >
              <Text
                type='Circular'
                size='mini'
                color={SHIP_GREY}
                style={{ marginRight: WP2 }}
              >
                Check In sekarang
              </Text>
              <View style={{ backgroundColor: WHITE }}>
                <Countdown
                  onFinish={() => {
                    this.setState({ isRefreshing: true }, async () => {
                      await this._getContent()
                      this.setState({
                        isRefreshing: false,
                      })
                    })
                  }}
                />
              </View>
            </View>
          )}
        </View>
        {/* ITEM */}
        <FlatList
          bounces={false}
          bouncesZoom={false}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          style={{ flexGrow: 0 }}
          contentContainerStyle={{
            paddingVertical: WP4,
            paddingLeft: WP4,
          }}
          data={dataCheckIn.slice(0, 7)}
          extraData={result}
          renderItem={({ item, index }) => {
            return (
              <CheckInItem
                loading={!isLoaded}
                key={`${index}+${Math.random()}`}
                day={item.day}
                isChecked={item.checked_in}
                isLucky={item.lucky_box == 1}
                point={item.point}
              />
            )
          }}
        />
      </View>
    )
  };

  _checkInSection = (result, isLoaded) => {
    const { isRefreshing } = this.state
    const {
      luckyfren: {
        result: { is_expired: isExpired },
      },
    } = this.props
    const dateEnd = result?.endDate
    const diffDate = moment(dateEnd).diff(moment(), 'days')
    const isCheckIn = result?.checked_in
    const isStart = result?.is_started
    const notCheckin = result?.daily_checkin?.find(
      (item) => item.checked_in == false,
    )
    const title =
      isCheckIn && diffDate <= 0
        ? 'Event Lucky Fren telah berakhir '
        : isExpired
        ? 'Event Lucky Fren telah berakhir '
        : isCheckIn
        ? 'Check-In lagi besok untuk mendapat'
        : 'Check-In sekarang untuk mendapat'

    const subtitle =
      (isCheckIn && diffDate <= 0) || isExpired
        ? 'terima kasih atas partisipasinya Fren!'
        : `${
            notCheckin?.lucky_box == 1
              ? `${notCheckin?.point || 0} poin tambahan & Lucky Box.`
              : `${notCheckin?.point} poin.`
          }`
    return (
      <View
        style={{ width: '100%', padding: WP4, backgroundColor: PALE_GREY_THREE }}
      >
        <View
          style={{
            backgroundColor: WHITE,
            borderRadius: 6,
            ...SHADOW_STYLE['shadowThin'],
          }}
        >
          <View
            style={{
              padding: WP4,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              borderBottomWidth: 1.5,
              borderBottomColor: PALE_GREY_THREE,
            }}
          >
            {isLoaded ? (
              <View style={{ flex: 1 }}>
                <Text
                  lineHeight={WP5}
                  type='Circular'
                  size='xmini'
                  weight={300}
                  color={SHIP_GREY}
                >
                  {title}
                </Text>
                <Text
                  lineHeight={WP5}
                  type='Circular'
                  size='xmini'
                  weight={(isCheckIn && diffDate <= 0) || isExpired ? 300 : 500}
                  color={SHIP_GREY}
                >
                  {subtitle}
                </Text>
              </View>
            ) : (
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    width: WP50,
                    backgroundColor: PALE_GREY_THREE,
                    height: WP305,
                    marginBottom: WP105,
                    borderRadius: 2,
                  }}
                />
                <View
                  style={{
                    width: WP20,
                    backgroundColor: PALE_GREY_THREE,
                    height: WP305,
                    borderRadius: 2,
                  }}
                />
              </View>
            )}
            <Touchable
              disabled={isExpired || isRefreshing || isCheckIn || isStart == false}
              onPress={this._actionCheckin}
              style={{
                backgroundColor:
                  isExpired || isCheckIn || isStart == false ? PALE_SALMON : REDDISH,
                borderRadius: 6,
                paddingVertical: WP2,
                paddingHorizontal: WP4,
              }}
            >
              <Text type='Circular' size='xmini' weight={400} color={WHITE} centered>
                Check-In
              </Text>
            </Touchable>
          </View>
          <SliderMessage isExpired={isExpired} endDate={result?.endDate} />
        </View>
      </View>
    )
  };

  _section = (section) => {
    const {
      navigateTo,
      luckyfren: { result, isLoaded },
    } = this.props
    const config = luckyConfig[section]
    const dummyData = [1, 2, 3, 4, 5]
    const dataList = !isLoaded
      ? dummyData
      : !isEmpty(result)
      ? result[config.key_data]
      : []

    return (
      <View
        style={{
          borderTopColor: PALE_BLUE,
          borderTopWidth: config.line ? 1 : 0,
        }}
        onLayout={(event) => (this.layout[section] = event.nativeEvent.layout)}
      >
        <View style={{ paddingVertical: WP6 }}>
          {/* HEADER */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: WP4,
            }}
          >
            <Text
              lineHeight={WP6}
              type='Circular'
              size='slight'
              color={NAVY_DARK}
              weight={600}
            >
              {config.title}
            </Text>
            {!isEmpty(config.route) && (
              <Touchable
                activeOpacity={0.75}
                onPress={() => {
                  navigateTo(config.route, config.routeParams || {})
                }}
              >
                <Text type='Circular' size='xmini' color={REDDISH} weight={400}>
                  Lihat Semua
                </Text>
              </Touchable>
            )}
          </View>
          {!isEmpty(config.subtitle) && (
            <Text
              type='Circular'
              size='mini'
              color={SHIP_GREY}
              weight={300}
              style={{ paddingLeft: WP4 }}
            >
              {config.subtitle}
            </Text>
          )}

          {/* ITEM */}
          <FlatList
            bounces={false}
            bouncesZoom={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            horizontal={config.horizontal}
            numColumns={config.numColumns}
            style={{ flexGrow: 0 }}
            contentContainerStyle={{
              paddingLeft: section == 'ranking' ? 0 : WP4,
              paddingTop: section == 'ranking' ? WP3 : WP4,
            }}
            data={dataList}
            extraData={dataList}
            renderItem={({ item, index }) => this._adItem(item, index, section)}
            ListEmptyComponent={
              <View>
                <Text
                  type='Circular'
                  size='mini'
                  centered
                  weight={300}
                  color={SHIP_GREY_CALM}
                >
                  {config.empty_message}
                </Text>
              </View>
            }
          />
        </View>
      </View>
    )
  };

  _adItem = (item, i, section) => {
    const {
      luckyfren: {
        isLoaded,
        result: { is_expired: isExpired },
      },
      navigateTo,
    } = this.props
    const loading = !isLoaded
    return (
      <Touchable
        disabled={loading}
        style={{ paddingVertical: section == 'ranking' ? WP105 : 0 }}
        key={`${i}${Math.random()}`}
        activeOpacity={0.75}
        onPress={() => this._onPressItem(item, section)}
      >
        {section == 'reward' && (
          <RewardItem
            isExpired={isExpired}
            loading={loading}
            image={item.image}
            id_reward={item.id_reward}
            reward={item.reward_name}
            point={item.total_point}
            stock={item.available_stock}
            navigateTo={navigateTo}
            is1000Startup={true}
          />
        )}
        {section == 'mission' && (
          <MissionItem
            isExpired={get(this.props.luckyfren, 'result.is_expired')}
            loading={loading}
            image={item.image}
          />
        )}
        {section == 'ranking' && (
          <View style={{ width: '100%', paddingHorizontal: WP4 }}>
            <RankingItem
              isStartup={true}
              loading={loading}
              image={item?.profile_picture}
              name={item?.full_name}
              position={item?.position}
              point={item?.total_point}
              latest_position={item?.latest_position}
            />
          </View>
        )}
      </Touchable>
    )
  };

  _onPressItem = (item, section) => {
    const {
      navigateTo,
      luckyfren: {
        result: { is_expired: isExpired },
      },
    } = this.props
    if (section == 'reward') {
      navigateTo('DetailRewardScreen', {
        id: item.id_reward,
        isExpired,
        is1000Startup: true,
      })
    } else if (section == 'mission') {
      navigateTo('MissionScreen', {
        category: item.category,
        isExpired,
        is1000Startup: true,
      })
    } else if (section == 'ranking') {
      navigateTo('StartUpProfileScreen', { idUser: item.id_user, isExpired })
    }
  };

  cancelModal = () => {
    this.setState({ modalShown: null })
  };

  render() {
    const { isRefreshing, isLoading, templatePopUp, modalShown } = this.state
    const {
      luckyfren: { result, isLoaded },
    } = this.props
    return (
      <Container
        isLoading={isLoading}
        noStatusBarPadding
        statusBarBackground={NO_COLOR}
      >
        <View style={{ flex: 1, backgroundColor: WHITE }}>
          <NavigationEvents onDidFocus={this._getContent} />

          {this._headerSection()}
          <Animated.ScrollView
            ref={(ref) => (this.scrollViewRef = ref)}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={isRefreshing}
                onRefresh={() => {
                  this.setState({ isRefreshing: true }, async () => {
                    await this._getContent()
                    this.setState({
                      isRefreshing: false,
                    })
                  })
                }}
              />
            }
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: { contentOffset: { y: this.state.scrollY } },
                },
              ],
              { useNativeDriver: false },
            )}
          >
            {this._cardPointSection(result)}
            {this._listCheckInSection(result, isLoaded)}
            {this._checkInSection(result, isLoaded)}
            {this._section('mission')}
            {this._section('reward')}
            {this._section('ranking')}
            <PopUpInfoTrigger
              ref={(ref) => {
                this.popUp = ref
              }}
              template={templatePopUp}
            />
          </Animated.ScrollView>
        </View>
        <RNModal
          visible={!!modalShown}
          animationType={'none'}
          onRequestClose={this.cancelModal}
          transparent
        >
          <View
            style={{
              flex: 1,
              backgroundColor: 'rgba(0,0,0,0.5)',
              justifyContent: 'flex-end',
            }}
          >
            <Touchable onPress={this.cancelModal} style={{ flex: 1 }} />
            <View
              style={{
                backgroundColor: WHITE,
                minHeight: 100,
                paddingBottom: getBottomSpace(),
              }}
            >
              <View
                style={{
                  borderBottomWidth: 1,
                  paddingHorizontal: WP5,
                  paddingVertical: WP4,
                  borderBottomColor: PALE_BLUE,
                }}
              >
                <Text
                  type={'Circular'}
                  weight={600}
                  size={'slight'}
                  color={NAVY_DARK}
                >
                  {get(modalConfig[modalShown], 'title')}
                </Text>
              </View>
              <ScrollView style={{ maxHeight: HP40 }}>
                <View style={{ padding: WP5 }}>
                  {(get(modalConfig[modalShown], 'items') || []).map((item) => {
                    return (
                      <View
                        key={item}
                        style={{
                          flexDirection: 'row',
                          alignItems: 'flex-start',
                          marginBottom: modalShown === 'terms' ? WP1 : WP3,
                        }}
                      >
                        {modalShown === 'terms' && (
                          <Text
                            lineHeight={WP5}
                            type={'Circular'}
                            weight={300}
                            color={PALE_BLUE_TWO}
                            size={'petite'}
                          >
                            ●
                          </Text>
                        )}
                        <View
                          style={{
                            flex: 1,
                            paddingLeft: modalShown === 'terms' ? WP2 : 0,
                          }}
                        >
                          <Text
                            lineHeight={WP5}
                            type={'Circular'}
                            weight={300}
                            color={SHIP_GREY}
                            size={'mini'}
                          >
                            {item}
                          </Text>
                        </View>
                      </View>
                    )
                  })}
                </View>
              </ScrollView>
              <Touchable
                onPress={this.cancelModal}
                style={{
                  borderTopWidth: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: WP5,
                  paddingVertical: WP4,
                  borderTopColor: PALE_BLUE,
                }}
              >
                <View style={{ marginRight: WP4 }}>
                  <Icon name={'close'} />
                </View>
                <Text
                  type={'Circular'}
                  weight={400}
                  size={'slight'}
                  color={GUN_METAL}
                >
                  Tutup
                </Text>
              </Touchable>
            </View>
          </View>
        </RNModal>
      </Container>
    )
  }
}

StartupMissionHomeScreen.propTypes = {}

StartupMissionHomeScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(StartupMissionHomeScreen),
  mapFromNavigationParam,
)
