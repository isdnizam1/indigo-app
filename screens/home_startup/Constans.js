import { TRANSPARENT } from '../../constants/Colors'

export const LEARN_TAB_ITEM = [
  {
    title: 'Webinar',
    subtitle: 'Belajar mengenai dunia startup melalui Webinar',
  },
  {
    title: 'Video',
    subtitle: 'Belajar mengenai dunia Startup melalui Video',
  },
  {
    title: 'Podcast',
    subtitle: 'Belajar mengenai dunia startup melalui Podcast',
  },
  {
    title: 'Forum',
    subtitle: 'Belajar mengenai dunia startup melalui Forum',
  },
  {
    title: 'Consult with The Expert',
    subtitle: 'Konsultasi dengan para ahli dibidangnya',
  },
  {
    title: 'Articles',
    subtitle: 'Belajar mengenai dunia startup melalui Forum',
  },
]

export const GENERAL_TAB_ITEM_LEADING = [
  {
    title: 'Team',
    icon: require('../../assets/icons/peopleToolBoxWhite.png'),
    bgItem: require('../../assets/images/frame9210.png'),
    gradient: ['rgba(0,0,0,.7)', 'rgba(0,0,0,0.25)'],
    id: 'team',
  },
  {
    title: 'Mentor',
    icon: require('../../assets/icons/chalkBoardTeacherWhite.png'),
    bgItem: require('../../assets/images/frame9208.jpeg'),
    gradient: ['rgba(0,0,0,.7)', 'rgba(0,0,0,0.25)'],
    id: 'mentor',
  },
  {
    title: 'Forum',
    icon: require('../../assets/icons/fluentPeopleWhite.png'),
    bgItem: require('../../assets/images/frame9212.png'),
    gradient: ['rgba(0,0,0,.7)', 'rgba(0,0,0,0.25)'],
    id: 'forum',
  },
]

export const GENERAL_TAB_ITEM = [
  {
    title: 'Reward',
    subtitle: 'Informasi hadiah',
    icon: require('../../assets/icons/fluentRewardWhite.png'),
    bgItem: require('../../assets/images/bgTabGeneral.png'),
    id: 'rewards',
  },
  {
    title: 'T&C',
    subtitle: 'Semua ketentuan dari kompetisi',
    icon: require('../../assets/icons/dashIconWhite.png'),
    bgItem: require('../../assets/images/bgTabGeneral.png'),
    id: 'terms',
  },
  {
    title: 'FAQ',
    subtitle: 'Semua informasi program tentang',
    icon: require('../../assets/icons/faqWhite.png'),
    bgItem: require('../../assets/images/bgTabGeneral.png'),
    id: 'faq',
  },
]

export const MISSION_TAB_ITEM = [
  {
    id_mission_category: 1,
    isExpired: false,
    total: 20,
    subtitle: 'Misi kamu telah selesai',
    img: require('../../assets/images/bgMission1.png'),
  },
  {
    id_mission_category: 3,
    isExpired: true,
    total: 30,
    subtitle: 'Misi kamu telah selesai',
    img: require('../../assets/images/bgMission2.png'),
  },
]
