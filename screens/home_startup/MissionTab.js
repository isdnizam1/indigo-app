import React, { useState } from 'react'
import { FlatList, TouchableOpacity, View } from 'react-native'
import moment from 'moment'
import { postCheckInTodayStartup } from 'sf-actions/api'
import { Container, Text } from '../../components'
import {
  NAVY_DARK,
  PALE_GREY_THREE,
  PALE_SALMON,
  PALE_WHITE,
  REDDISH,
  WHITE,
} from '../../constants/Colors'
import { WP105, WP2, WP20, WP305, WP4, WP50, WP6 } from '../../constants/Sizes'
import Spacer from '../../components/Spacer'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import Touchable from '../../components/Touchable'
import ProfileCard1000StartUp from '../../components/1000_startup/ProfileCard1000StartUp'
import MissionTabItem from '../../components/home_startup/items/MissionTabItem'
import PopUpInfoTrigger from '../../components/popUp/PopUpInfoTrigger'
import { showReviewIfNotYet } from '../../utils/review'

moment.locale('id')

const MissionTab = ({ data, getData, navigateTo, userData, loading }) => {
  // missionData
  const [templatePopUp, setTemplatePopUp] = useState({})
  const [showPopUp, setShowPopUp] = useState(false)
  const _renderMissionItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        disabled={false}
        key={`${index}${Math.random()}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={() =>
          navigateTo('MissionCategoryDetailScreen', {
            category: item.category,
            idMission: item.id_mission_category,
            isExpired: data.is_expired,
          })
        }
      >
        <MissionTabItem
          category={item.category}
          total_mission={item.total_mission}
          complete_mission={item.complete_mission}
          subtitle={item.name}
          img={{ uri: item.banner }}
          index={index}
        />
      </TouchableOpacity>
    )
  }

  const _navigateDailyDetailScreen = () => {
    const missionCategory = data.mission_category
    for (let index in missionCategory) {
      if (missionCategory[index].category === 'daily') {
        navigateTo('MissionCategoryDetailScreen', {
          category: missionCategory[index].category,
          idMission: missionCategory[index].id_mission_category,
          isExpired: missionCategory[index].is_expired,
        })
      }
    }
  }

  const _actionCheckin = async () => {
    try {
      let body = { id_user: userData.id_user }
      const response = await postCheckInTodayStartup(body)
      const data = response.data
      const { result, code } = data
      if (code === 200) {
        const { lucky_box, point } = result?.checked_in
        const isLuckyBox = lucky_box == 1
        let templatePopUp = {
          closeColor: isLuckyBox ? SHIP_GREY : WHITE,
          image: isLuckyBox
            ? require('sf-assets/images/bgLuckyBox.png')
            : require('sf-assets/images/bgMultipleCoin.png'),
          aspectRatio: 296 / 184,
          title: isLuckyBox
            ? `Lucky Box & ${point} Poin`
            : `Mendapat ${point} Poin Tambahan`,
          multipleContent: true,
          content: [
            { style: 'normal', text: 'Selamat! kamu mendapatkan ' },
            {
              style: 'bold',
              text: `${point} Poin Tambahan ${isLuckyBox ? '& Lucky Box ' : ''}`,
            },
            { style: 'normal', text: 'dari ' },
            { style: 'bold', text: 'Check In hari ini. ' },
            {
              style: 'normal',
              text: isLuckyBox
                ? 'Ambil & buka hadiahnya!'
                : 'Ambil & kumpulkan pointnya!',
            },
          ],
          cta: isLuckyBox ? 'Ambil Poin & Hadiah' : 'Ambil Poin',
          onPress: isLuckyBox
            ? () => navigateTo('LuckyBoxScreen')
            : () => {
                setShowPopUp(false)
                _navigateDailyDetailScreen()
              },
        }

        setTemplatePopUp(templatePopUp)
        setShowPopUp(true)
        if (!isLuckyBox) setTimeout(() => showReviewIfNotYet(), 1000)
      }
    } catch (error) {}
    getData()
  }

  const _checkInSection = () => {
    let isLoaded = true
    return (
      <View style={{ width: '100%', marginTop: 12 }}>
        <View
          style={{
            backgroundColor: WHITE,
            borderRadius: 6,
            ...SHADOW_STYLE['shadowThin'],
          }}
        >
          <View
            style={{
              padding: WP4,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            {isLoaded ? (
              <View style={{ flex: 1 }}>
                {!data.is_expired && (
                  <Text type='Circular' color={'#637381'} style={{ fontSize: 11 }}>
                    Hari ini kamu
                    <Text
                      type='Circular'
                      color={'#637381'}
                      weight={700}
                      style={{ fontSize: 11 }}
                    >
                      {' '}
                      {data.checked_in ? 'Sudah' : 'Belum'} Check-In
                    </Text>
                    , silahkan{' '}
                    {data.checked_in ? 'ikuti' : 'Check-In untuk mengikuti'} misi
                    harian
                  </Text>
                )}
                {data.is_expired && (
                  <Text type='Circular' color={'#637381'} style={{ fontSize: 11 }}>
                    Event ini telah berakhir, terima kasih atas partisipasinya!
                  </Text>
                )}
              </View>
            ) : (
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    width: WP50,
                    backgroundColor: PALE_GREY_THREE,
                    height: WP305,
                    marginBottom: WP105,
                    borderRadius: 2,
                  }}
                />
                <View
                  style={{
                    width: WP20,
                    backgroundColor: PALE_GREY_THREE,
                    height: WP305,
                    borderRadius: 2,
                  }}
                />
              </View>
            )}

            <Touchable
              onPress={_actionCheckin}
              disabled={!!(data.checked_in || data.is_expired)}
              style={{
                backgroundColor:
                  data.checked_in || data.is_expired ? PALE_SALMON : REDDISH,
                borderRadius: 6,
                paddingVertical: 6,
                paddingHorizontal: 16,
              }}
            >
              <Text type='Circular' size='xmini' weight={400} color={WHITE} centered>
                Check-In
              </Text>
            </Touchable>
          </View>
          <View
            style={{ padding: 1, backgroundColor: '#F4F6F8', marginVertical: 8 }}
          />
          <Text
            type='Circular'
            color={'#919EAB'}
            weight={400}
            style={{ fontSize: 10, paddingBottom: 8, paddingLeft: 16 }}
          >
            Event ini berlangsung s/d {`${moment(data.endDate).format('DD MMMM Y')}`}{' '}
          </Text>
        </View>
      </View>
    )
  }
  return (
    <>
      <Container
        isReady={!loading}
        isLoading={loading}
        theme='light'
        noStatusBarPadding
        scrollable
        scrollBackgroundColor={PALE_WHITE}
        onPullDownToRefresh={() => getData()}
      >
        <View style={{ padding: WP4, paddingTop: WP6, paddingBottom: WP6 }}>
          <Text type='Circular' size='medium' color={NAVY_DARK} weight={600}>
            Your Mission
          </Text>
          <Spacer size={WP4} />
          <ProfileCard1000StartUp
            name={data.full_name}
            image={{ uri: data.profile_picture }}
            total_point={data.total_point}
            leaderboard_position={data.leaderboard_position}
            onPress={() => navigateTo('StartupMissionHomeScreen')}
          />
          {_checkInSection()}
          <Spacer size={WP105} />
          <FlatList
            bounces={false}
            bouncesZoom={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            horizontal={false}
            numColumns={2}
            style={{
              flex: 1,
              flexGrow: 0,
              minHeight: 100,
            }}
            contentContainerStyle={{
              paddingTop: WP2,
            }}
            data={data.mission_category}
            extraData={data.mission_category}
            renderItem={({ item, index }) => _renderMissionItem({ index, item })}
          />
        </View>
      </Container>
      <PopUpInfoTrigger visibility={showPopUp} template={templatePopUp} />
    </>
  )
}

export default MissionTab
