import React, { useState } from 'react'
import { FlatList, TouchableOpacity, View } from 'react-native'
import { Container, Text, ModalMessageView } from '../../components'
import { NAVY_DARK, PALE_WHITE, REDDISH } from '../../constants/Colors'
import { WP2, WP4, WP6, WP80 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import LearnTabItem from '../../components/home_startup/items/LearnTabItem'

const LearnTab = ({
  data,
  navigateTo,
  loading,
  getData,
  userData,
  modalIsVisible,
  toggleModal,
}) => {
  const [selectedItem, setSelectedItem] = useState()

  const _renderLearnItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        disabled={false}
        style={{ paddingVertical: WP2 }}
        key={`${index}${Math.random()}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={() => {
          _navigateToScreen({ item })
        }} //setSelectedItem(index);
      >
        <LearnTabItem title={item.title} subtitle={item.label} index={index} />
      </TouchableOpacity>
    )
  }

  const _navigateToScreen = ({ item }) => {
    const { title } = item
    if (title === 'Webinar') {
      navigateTo('SoundconnectScreen', { isStartup: true })
    } else if (title === 'Video') {
      navigateTo('VideoCategoryScreen')
    } else if (title === 'Podcast') {
      navigateTo('PodcastCategoryScreen')
    } else if (title === 'Forum') {
      navigateTo('Forum1000StartupScreen', { isStartup: true, tabToShow: 0 })
    } else if (title === 'Articles') {
      navigateTo('ListArticleStartupScreen', { isStartup: true, tabToShow: 0 })
    } else if (title === 'Consult with The Expert') {
      toggleModal()
    }
  }

  // const _renderItemComponent = ({ selectedItem }) => {
  //   if (selectedItem === 1) {
  //     return (
  //       <>
  //         <Text type='Circular' size='medium' color={NAVY_DARK} weight={600}>
  //           How do you want to watch today?
  //         </Text>

  //         {data.video.length > 0 ? (
  //           data.video.map((item, index) =>
  //           (<TouchableOpacity
  //             disabled={loading}
  //             style={{ paddingVertical: WP2 }}
  //             key={index}
  //             activeOpacity={TOUCH_OPACITY}
  //             onPress={() => { navigateTo('PremiumVideoTeaser', { id_ads: item.id_ads }) }}
  //           >
  //             {/* {LearnVideoItem({ item, index, loading }),} */}
  //             <LearnVideoItem item={item} index={index} loading={loading} />

  //           </TouchableOpacity>)
  //           )
  //         ) : (
  //           <View>
  //             <Text
  //               type='Circular'
  //               size='small'
  //               weight={400}
  //               color={SHIP_GREY_CALM}
  //             >
  //               {'Empty'}
  //             </Text>
  //           </View>
  //         )}
  //       </>
  //     )
  //   } else {
  //     return (
  //       <>
  //       </>
  //     )
  //   }
  // }

  return (
    <Container
      isReady={!loading}
      isLoading={loading}
      theme='light'
      noStatusBarPadding
      scrollable
      scrollBackgroundColor={PALE_WHITE}
      onPullDownToRefresh={() => getData()}
    >
      <View style={{ padding: WP4, paddingTop: WP6, paddingBottom: WP6 }}>
        {selectedItem === undefined && (
          <>
            <Text type='Circular' size='medium' color={NAVY_DARK} weight={600}>
              How do you want to learn today?
            </Text>
            <FlatList
              bounces={false}
              bouncesZoom={false}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              horizontal={false}
              numColumns={2}
              style={{
                flex: 1,
                flexGrow: 0,
                minHeight: 100,
              }}
              contentContainerStyle={{
                paddingTop: WP2,
              }}
              data={data}
              extraData={data}
              renderItem={({ item, index }) => _renderLearnItem({ index, item })}
            />
          </>
        )}
        {/* {_renderItemComponent({ selectedItem })} */}
      </View>
      <ModalMessageView
        aspectRatio={838 / 676}
        fullImage
        toggleModal={toggleModal}
        imageStyle={{ width: WP80, marginTop: 33 }}
        isVisible={modalIsVisible}
        image={require('sf-assets/images/bgUnderDevelopment.png')}
        buttonPrimaryAction={toggleModal}
        titleSize={'massive'}
        buttonPrimaryTextWeight={500}
        buttonPrimaryTextSize={'small'}
        buttonPrimaryBgColor={REDDISH}
        buttonPrimaryText={'OK'}
        title={'Coming Soon'}
        subtitle={
          'This page is still under maintenance. We’ll inform you when the page is ready.'
        }
      />
    </Container>
  )
}

export default LearnTab
