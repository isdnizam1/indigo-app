/* eslint-disable react/jsx-key */
import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { SHIP_GREY_CALM } from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import InteractionManager from 'sf-utils/InteractionManager'
import { Container, _enhancedNavigation } from 'sf-components'
import { SHADOW_GRADIENT } from 'sf-constants/Colors'
import { WP100, WP12, WP3 } from 'sf-constants/Sizes'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Icon } from 'sf-components'
import { noop } from 'lodash'
import { ScrollView } from 'react-native'
import { FlatList } from 'react-native'
import { Card } from 'react-native-shadow-cards'
import { Image, Text } from '../../../components'
import {
  HP15,
  HP20,
  WP05,
  WP1,
  WP10,
  WP2,
  WP205,
  WP305,
  WP4,
  WP5,
  WP50,
  WP6,
  WP80,
} from '../../../constants/Sizes'
import { PALE_BLUE, WHITE } from '../../../constants/Colors'
import Spacer from '../../../components/Spacer'
import { getStartupPodcastCategory } from '../../../actions/api'
import { restrictedAction } from '../../../utils/helper'

const style = StyleSheet.create({
  headerShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
})

class _PodcastCategoryScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      categories: [],
    }
    this._backHandler = this._backHandler.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
    this._getData = this._getData.bind(this)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._getData)
  }

  _getData = () => {
    getStartupPodcastCategory().then((response) => {
      this.setState({
        categories: response.data.result,
      })
    })
  };

  _backHandler = () => {
    this.props.navigateBack()
  };

  _renderHeader = () => {
    return (
      <View
        style={{
          paddingVertical: WP205,
          flexDirection: 'row',
          width: WP100,
        }}
      >
        <TouchableOpacity
          onPress={this.props.navigateBack}
          style={{
            width: WP12,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}
        >
          <Icon
            centered
            background='dark-circle'
            size='large'
            color={SHIP_GREY_CALM}
            name='chevron-left'
            type='Entypo'
            style={{ marginRight: WP1 }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={restrictedAction({
						action: () => this.props.navigateTo('StartupSearchScreen', {searchType: 'podcast'}),
						userData: this.props.userData,
						navigation: this.props.navigation,
					})}
          activeOpacity={0.75}
          style={{
            flex: 1,
            backgroundColor: 'rgba(145, 158, 171, 0.1)',
            borderRadius: 6,
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: WP2,
            paddingVertical: WP1 * 4,
            width: WP80 + WP2,
          }}
        >
          <Icon
            centered
            background='dark-circle'
            size='large'
            color={SHIP_GREY_CALM}
            name='magnify'
            type='MaterialCommunityIcons'
            style={{ marginRight: WP1 }}
          />
          <Text
            type='Circular'
            size='mini'
            weight={300}
            color={SHIP_GREY_CALM}
            centered
          >
            Coba cari Podcast
          </Text>
        </TouchableOpacity>
        <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
      </View>
    )
  };

  render() {
    const { categories } = this.state
    return (
      <Container renderHeader={this._renderHeader}>
        <View>
          <ScrollView>
            <View
              style={{
                width: WP100,
                height: WP50,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Image
                style={{ width: WP100, position: 'absolute' }}
                aspectRatio={2 / 1}
                size={'full'}
                source={require('sf-assets/images/podcastCategoryBackground.jpg')}
              />
              <Text color={WHITE} size={'huge'} type={'Circular'} weight={500}>
                Podcast
              </Text>
              <Spacer size={WP1} />
              <Text
                color={WHITE}
                size={'slight'}
                type={'Circular'}
                weight={300}
                centered
                style={{ lineHeight: WP6 }}
              >
                {'Edukasi untuk kamu melalui podcast\nkapan saja dimana saja'}
              </Text>
            </View>
            <View
              style={{
                backgroundColor: '#F9FAFB',
                paddingVertical: WP305,
                paddingHorizontal: WP5,
                borderBottomWidth: 1,
                borderBottomColor: PALE_BLUE,
              }}
            >
              <Text size={'medium'} type={'Circular'} weight={500}>
                Pilih Kategori
              </Text>
            </View>
            <Spacer size={WP4} />
            <View style={{ alignItems: 'center' }}>
              {React.Children.toArray(
                categories.map((item) => {
                  return (
                    <Card style={{ padding: 10, marginVertical: WP2 }}>
                      <View style={{}}>
                        <TouchableOpacity
                          onPress={() => {
                            this.props.navigateTo('PodcastModuleScreen', {
                              title: item.category_name,
                              modules: item.module,
                              category_id: item.id_sp_album_category,
                            })
                          }}
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}
                        >
                          <View>
                            <Image source={{ uri: item.bg_image }} />
                          </View>
                          <View style={{ paddingHorizontal: WP4, flex: 1 }}>
                            <Text size={'slight'} color={'#454F5B'} weight={400}>
                              {item.category_name}
                            </Text>
                            <Spacer size={WP05} />
                            <Text size={'mini'} color={'#919EAB'}>
                              {item.label ||
                                'Lorem ipsum dolor sit ammet'}
                            </Text>
                          </View>
                          <View>
                            <Icon
                              type={'Entypo'}
                              name={'chevron-right'}
                              size={'large'}
                              color={'#FF651F'}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                    </Card>
                  )
                }),
              )}
            </View>
            <View style={{ height: HP15 }} />
          </ScrollView>
        </View>
      </Container>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  package: getParam('package', null),
})

const PodcastCategoryScreen = _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(_PodcastCategoryScreen),
  mapFromNavigationParam,
)

export default PodcastCategoryScreen
