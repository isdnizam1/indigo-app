import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { SHIP_GREY, SHIP_GREY_CALM } from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import InteractionManager from 'sf-utils/InteractionManager'
import { Container, _enhancedNavigation } from 'sf-components'
import HeaderNormal from 'sf-components/HeaderNormal'
import { SHADOW_GRADIENT } from 'sf-constants/Colors'
import { WP100, WP12, WP3 } from 'sf-constants/Sizes'
import { FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Icon, Text } from '../../../components'
import { WP2, WP4, WP5 } from '../../../constants/Sizes'
import { PALE_GREY } from '../../../constants/Colors'

const style = StyleSheet.create({
  headerShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
})

class _PodcastModuleScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this._backHandler = this._backHandler.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
    this._getData = this._getData.bind(this)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._getData)
  }

  _getData = () => {};

  _backHandler = () => {
    this.props.navigateBack()
  };

  _renderHeader = () => {
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftOnPress={this._backHandler}
          centered
          textType={'Circular'}
          iconLeft={'chevron-left'}
          iconLeftSize={'large'}
          textSize={'slight'}
          iconLeftType={'Entypo'}
          text={this.props.title}
          textColor={SHIP_GREY}
          iconLeftColor={SHIP_GREY_CALM}
          rightComponent={<View style={{ width: WP12 }} />}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
      </View>
    )
  };

  render() {
    return (
      <Container renderHeader={this._renderHeader}>
        <View
          style={{
            paddingHorizontal: WP5,
            paddingTop: WP5 + WP2,
            paddingBottom: WP2,
          }}
        >
          <Text size={'medium'} type={'Circular'} weight={500}>
            Pilih Modul
          </Text>
        </View>
        <FlatList
          data={this.props.modules}
          keyExtractor={(item) => item}
          renderItem={({ item, index }) => {
            return (
              <View style={{ paddingHorizontal: WP5 }}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigateTo('PodcastByModuleScreen', {
                      module: item,
                      category_id: this.props.category_id,
                    })
                  }
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: PALE_GREY,
                    paddingVertical: WP4,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}
                >
                  <View style={{ flex: 1 }}>
                    <Text size={'slight'}>{item}</Text>
                  </View>
                  <View>
                    <Icon
                      type={'Entypo'}
                      name={'chevron-right'}
                      color={SHIP_GREY_CALM}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            )
          }}
        />
      </Container>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  title: getParam('title', null),
  category_id: getParam('category_id', null),
  modules: getParam('modules', []),
})

const PodcastModuleScreen = _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(_PodcastModuleScreen),
  mapFromNavigationParam,
)

export default PodcastModuleScreen
