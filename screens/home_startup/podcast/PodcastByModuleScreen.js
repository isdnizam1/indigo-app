/* eslint-disable react/jsx-key */
import React from 'react'
import { View, StyleSheet, Image as RNImage } from 'react-native'
import { connect } from 'react-redux'
import { LinearGradient } from 'expo-linear-gradient'
import { Container, _enhancedNavigation } from 'sf-components'
import HeaderNormal from 'sf-components/HeaderNormal'
import { SHADOW_GRADIENT } from 'sf-constants/Colors'
import { WP100, WP12, WP3 } from 'sf-constants/Sizes'
import { FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { get } from 'lodash-es'
import { SHIP_GREY_CALM } from 'sf-constants/Colors'
import { SHIP_GREY } from 'sf-constants/Colors'
import { NavigationEvents } from '@react-navigation/compat'
import { Text } from '../../../components'
import {
  WP05,
  WP1,
  WP10,
  WP105,
  WP18,
  WP2,
  WP205,
  WP25,
  WP4,
  WP5,
  WP505,
} from '../../../constants/Sizes'
import { PALE_GREY, REDDISH, WHITE } from '../../../constants/Colors'
import { getStartupPodcastByModule } from '../../../actions/api'
import Spacer from '../../../components/Spacer'
import ButtonV2 from '../../../components/ButtonV2'

const style = StyleSheet.create({
  headerShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
})

class _PodcastByModuleScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      learnings: {
        next_learning: [],
        start_learning: [],
      },
    }
    this._backHandler = this._backHandler.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
    this._getData = this._getData.bind(this)
  }

  _getData = () => {
    getStartupPodcastByModule({
      id_user: this.props.userData.id_user,
      platform: '1000startup',
      id_sp_album_category: this.props.category_id,
      module: this.props.module,
    }).then((response) => {
      this.setState({
        learnings: response.data.result,
      })
      // console.log(response.data.result)
    })
  };

  _backHandler = () => {
    this.props.navigateBack()
  };

  _renderHeader = () => {
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftOnPress={this._backHandler}
          centered
          textType={'Circular'}
          iconLeft={'chevron-left'}
          iconLeftSize={'large'}
          textSize={'slight'}
          iconLeftType={'Entypo'}
          text={this.props.module}
          textColor={SHIP_GREY}
          iconLeftColor={SHIP_GREY_CALM}
          rightComponent={<View style={{ width: WP12 }} />}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
      </View>
    )
  };

  render() {
    return (
      <Container scrollable renderHeader={this._renderHeader}>
        <NavigationEvents onWillFocus={this._getData} />
        {React.Children.toArray(
          ['start_learning', 'next_learning'].map((_, index) => {
            return (
              <View>
                <View
                  style={{
                    paddingHorizontal: WP5,
                    paddingTop: WP5 + WP2,
                    paddingBottom: WP2,
                  }}
                >
                  <Text size={'small'} type={'Circular'} weight={500}>
                    {['Mulai pembelajaran kamu', 'Pembelajaran selanjutnya'][index]}
                  </Text>
                </View>
                <FlatList
                  data={this.state.learnings[_]}
                  ListEmptyComponent={
                    index == 1 ? (
                      <View
                        style={{
                          paddingVertical: WP10,
                          paddingHorizontal: WP10,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      >
                        <RNImage
                          style={{ width: WP18, height: WP18 }}
                          source={require('sf-assets/icons/schoolHat.png')}
                        />
                        <Spacer size={WP4} />
                        <Text
                          style={{ lineHeight: WP505 }}
                          size={'mini'}
                          weight={400}
                        >
                          Materi sudah selesai
                        </Text>
                        <Spacer size={WP1} />
                        <Text
                          style={{ lineHeight: WP4 + WP05, textAlign: 'center' }}
                          size={'xmini'}
                          color={SHIP_GREY_CALM}
                          weight={300}
                        >
                          {
                            'Silahkan pelajari materi lain untuk menambah\npengetahuan kamu'
                          }
                        </Text>
                      </View>
                    ) : (
                      <View
                        style={{
                          paddingVertical: WP10,
                          paddingHorizontal: WP10,
                          alignItems: 'center',
                          justifyContent: 'center',
                        }}
                      >
                        <RNImage
                          style={{ width: WP18, height: WP18 }}
                          source={require('sf-assets/icons/schoolHat.png')}
                        />
                        <Spacer size={WP4} />
                        <Text
                          style={{ lineHeight: WP505 }}
                          size={'mini'}
                          weight={400}
                        >
                          Belum ada materi
                        </Text>
                        <Spacer size={WP1} />
                        <Text
                          style={{ lineHeight: WP4 + WP05, textAlign: 'center' }}
                          size={'xmini'}
                          color={SHIP_GREY_CALM}
                          weight={300}
                        >
                          {`Saat ini belum ada materi tersedia di modul : ${this.props.module}`}
                        </Text>
                      </View>
                    )
                  }
                  keyExtractor={(item) => item.id_ads}
                  renderItem={({ item }) => {
                    {
                      /* __DEV__ && console.log(item) */
                    }
                    return (
                      <View style={{ paddingHorizontal: WP5 }}>
                        <TouchableOpacity
                          onPress={() =>
                            index == 0 &&
                            this.props.navigateTo('SoundplayDetail', {
                              id_ads: item.id_ads,
                              isStartup: true,
                            })
                          }
                          style={{
                            borderBottomWidth: 1,
                            borderBottomColor: PALE_GREY,
                            paddingVertical: WP3,
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}
                        >
                          <RNImage
                            size={'huge'}
                            source={{ uri: item.image }}
                            style={{
                              borderRadius: WP105,
                              width: WP25,
                              height: WP25,
                              resizeMode: 'cover',
                            }}
                          />
                          <View style={{ paddingLeft: WP5, flex: 1 }}>
                            <Text
                              style={{ lineHeight: WP505 }}
                              size={'slight'}
                              color={SHIP_GREY}
                              weight={400}
                            >
                              {item.title}
                            </Text>
                            <Spacer size={WP1} />
                            <Text
                              style={{ lineHeight: WP505 }}
                              size={'xmini'}
                              color={SHIP_GREY_CALM}
                              weight={300}
                            >
                              {get(item, 'speaker.0.title')}
                            </Text>
                            <Spacer size={WP105} />
                            {index == 1 && (
                              <View
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                <ButtonV2
                                  style={{
                                    paddingVertical: WP1,
                                    paddingHorizontal: WP2,
                                  }}
                                  color={'#f6f6f6'}
                                  textColor={SHIP_GREY_CALM}
                                  text={'Terkunci'}
                                />
                              </View>
                            )}
                            {index == 0 && (
                              <View
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                }}
                              >
                                {item.played_status === 'played' && (
                                  <ButtonV2
                                    style={{
                                      paddingVertical: WP1,
                                      paddingHorizontal: WP2,
                                    }}
                                    color={WHITE}
                                    textColor={SHIP_GREY_CALM}
                                    borderColor={'#c0c0c0'}
                                    forceBorderColor
                                    text={'Putar lagi'}
                                  />
                                )}
                                {item.played_status === 'played' && (
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      alignItems: 'center',
                                      paddingLeft: WP205,
                                    }}
                                  >
                                    <RNImage
                                      style={{
                                        width: WP5,
                                        height: WP5,
                                        marginRight: WP1,
                                      }}
                                      source={require('sf-assets/icons/tickCircleGreen.png')}
                                    />
                                    <Text size={'xmini'} color={'#50B83C'}>
                                      Selesai
                                    </Text>
                                  </View>
                                )}
                                {item.played_status !== 'played' && (
                                  <ButtonV2
                                    style={{
                                      paddingVertical: WP1,
                                      paddingHorizontal: WP4,
                                    }}
                                    color={REDDISH}
                                    textColor={WHITE}
                                    text={'Putar'}
                                  />
                                )}
                                {item.played_status === 'playing' && (
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      alignItems: 'center',
                                      paddingLeft: WP205,
                                    }}
                                  >
                                    <RNImage
                                      style={{
                                        width: WP5,
                                        height: WP5,
                                        marginRight: WP1,
                                      }}
                                      source={require('sf-assets/icons/clockBlue.png')}
                                    />
                                    <Text size={'xmini'} color={'#3287EC'}>
                                      In Progress
                                    </Text>
                                  </View>
                                )}
                              </View>
                            )}
                          </View>
                        </TouchableOpacity>
                      </View>
                    )
                  }}
                />
              </View>
            )
          }),
        )}
      </Container>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  title: getParam('title', null),
  category_id: getParam('category_id', null),
  module: getParam('module', []),
})

const PodcastByModuleScreen = _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(_PodcastByModuleScreen),
  mapFromNavigationParam,
)

export default PodcastByModuleScreen
