export const AD_ROUTES = {
  announcement: { key: 'announcement', title: 'Announcement', category: 'announcement' },
  collaboration: { key: 'collaboration', title: 'Collaboration', category: 'collaboration' },
  band: { key: 'band', title: 'Artists Spotlight', category: 'band' },
  submission: { key: 'submission', title: 'Submission', category: 'submission' },
  event: { key: 'event', title: 'Event', category: 'event' },
  news_update: { key: 'news_update', title: 'News & Update', category: 'news_update' },
  venue: { key: 'venue', title: 'Venue Rental', category: 'venue' }
}
