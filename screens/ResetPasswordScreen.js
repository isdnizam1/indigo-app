import React from "react";
import {
  Animated,
  TextInput,
  View,
  TouchableOpacity,
  Modal,
  Dimensions,
  BackHandler,
  Keyboard,
} from "react-native";
import { isEmpty } from "lodash-es";
import { connect } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";
import { Text, _enhancedNavigation, Container, Icon } from "../components";
import Dialog from "../components/Dialog";
import {
  LIPSTICK_TWO,
  GREY,
  GREY_PLACEHOLDER,
  SHIP_GREY,
  NAVY_DARK,
  SHIP_GREY_CALM,
  SHADOW_GRADIENT,
  WHITE,
  REDDISH,
} from "../constants/Colors";
import { WP4, WP15, WP5, HP5, WP8, HP1 } from "../constants/Sizes";
import style from "../styles/register";
import { postForgotNewPassword } from "../actions/api";
import { isEmailValid } from "../utils/helper";
import { getLocalStorage } from "../utils/storage";
import { KEY_FORGOT_PASSWROD_EMAIL } from "../constants/Storage";
import { KEYBOARD_VISIBLE } from "../services/helper/actionTypes";
import HeaderNormal from "../components/HeaderNormal";
import PopUpInfo from "../components/popUp/PopUpInfo";
import { HEADER } from "../constants/Styles";
import Spacer from "../components/Spacer";
import ButtonV2 from "../components/ButtonV2";

const { height } = Dimensions.get("window");

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  keyboardVisible: (response) => ({
    type: KEYBOARD_VISIBLE,
    response,
  }),
};

class ResetPasswordScreen extends React.Component {
  fadeIn = new Animated.Value(0);
  recoveryInput = React.createRef();

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      errorMessage: null,
      password: "",
      showPassword: false,
      password_confirmation: "",
      showPasswordConfirmation: false,
      marginTop: 0,
      isLoading: false,
      isConfirmation: false,
      recovery_code: "",
      errorPassword: "",
      errorConfirmPassword: "",
      completed: false,
    };
    this.onChangeText = this.onChangeText.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onLayout = this.onLayout.bind(this);
    this.backHandler = this.backHandler.bind(this);
    this.loginNow = this.loginNow.bind(this);
  }

  loginNow() {
    const { navigateTo } = this.props;
    // supposedly navigateto LoginScreen
    Promise.resolve(navigateTo("LoginEmailScreen")).then(() => {
      navigateTo("LoginEmailScreen");
    });
  }

  backHandler() {
    return true;
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", () =>
      this.props.keyboardVisible(true)
    );
    this.keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", () =>
      this.props.keyboardVisible(false)
    );
    BackHandler.addEventListener("hardwareBackPress", this.backHandler);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    this.props.keyboardVisible(false);
    BackHandler.removeEventListener("hardwareBackPress", this.backHandler);
  }

  onLayout({ nativeEvent }) {
    const viewHeight = nativeEvent.layout.height;
    this.setState(
      {
        marginTop: height - viewHeight - viewHeight / 2,
      },
      () => {
        Animated.timing(this.fadeIn, {
          toValue: 1,
          duration: 50,
          useNativeDriver: false,
        }).start();
      }
    );
  }

  toggleModal() {
    this.setState({ modalVisible: !this.state.modalVisible });
  }

  async onSubmit() {
    const { password_confirmation, password } = this.state;
    const email = await getLocalStorage(KEY_FORGOT_PASSWROD_EMAIL);
    if (!isEmailValid(email)) {
      this.setState({
        modalVisible: true,
        errorMessage: "Incorrect email address",
        isConfirmation: false,
      });
    } else if (password.length < 6) {
      this.setState({ errorPassword: "Minimal 6 karakter" });
    } else if (password != password_confirmation) {
      this.setState({ errorConfirmPassword: "Password tidak sesuai" });
    } else {
      this.setState(
        {
          isLoading: true,
        },
        () => {
          postForgotNewPassword({
            email,
            password,
            confirm_password: password_confirmation,
          })
            .then(({ data }) => {
              const { code, message } = data;
              code == 200 && this.setState({ isSuccess: true });
              code == 500 &&
                this.setState({
                  modalVisible: true,
                  errorMessage: message,
                  isConfirmation: false,
                });
            })
            .catch((e) => {})
            .then(() => this.setState({ isLoading: false }));
        }
      );
    }
  }

  onChangeText(value, callback) {
    Promise.all([this.setState({ errorMessage: null }), callback(value)]).then(
      () => {
        const { email, setCtaEnabled } = this.props;
        setCtaEnabled(!isEmpty(email));
      }
    );
  }

  _isButtonChangePasswordDisabled = () => {
    const {
      completed,
      isLoading,
      errorConfirmPassword,
      errorPassword,
      password_confirmation,
      password,
    } = this.state;
    if (
      !completed &&
      !isLoading &&
      !isEmpty(password) &&
      !isEmpty(password_confirmation) &&
      isEmpty(errorConfirmPassword) &&
      isEmpty(errorPassword)
    )
      return false;
    return true;
  };

  render() {
    const { navigateBack, navigation, navigateTo } = this.props;
    const {
      modalVisible,
      errorMessage,
      password,
      password_confirmation,
      showPassword,
      showPasswordConfirmation,
      isLoading,
      isConfirmation,
      errorPassword,
      errorConfirmPassword,
      completed,
      isSuccess,
    } = this.state;
    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <View>
            <HeaderNormal
              style={style.headerNormal}
              iconLeftOnPress={navigateBack}
              textType="Circular"
              textColor={SHIP_GREY}
              textWeight={400}
              text={"Password Baru"}
              centered
            />
            <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
          </View>
        )}
      >
        <View style={{ paddingHorizontal: WP5 }}>
          {completed && (
            <View>
              <Text style={{ marginBottom: WP15 }}>
                {"You can Login now with new password"}
              </Text>
            </View>
          )}
          {!completed && (
            <View style={{ alignItems: "stretch" }}>
              <Text
                style={{ marginTop: HP5 }}
                color={NAVY_DARK}
                weight={600}
                size="large"
              >
                Buat password baru
              </Text>
              <Text style={{ marginBottom: HP5 }} color={SHIP_GREY} size="mini">
                Tuliskan password kamu minimal 6 karakter
              </Text>
              <Text
                textColor={SHIP_GREY}
                size={"mini"}
                weight={400}
                type={"Circular"}
              >
                Password Baru
              </Text>
              <View>
                <TextInput
                  value={password}
                  onChangeText={(password) => {
                    let errorPassword = "";
                    this.setState({ password });
                    if (password.length < 6) {
                      errorPassword = "Minimal 6 karakter";
                    }
                    this.setState({ errorPassword });
                  }}
                  secureTextEntry={!showPassword}
                  style={style.input}
                />
                <TouchableOpacity
                  onPress={() => this.setState({ showPassword: !showPassword })}
                  style={style.passwordToggler2}
                  size={"tiny"}
                  color={showPassword ? GREY : GREY_PLACEHOLDER}
                  weight={500}
                  type={"Circular"}
                >
                  <Icon
                    name={showPassword ? "eye-off" : "eye"}
                    type="MaterialCommunityIcons"
                    color={SHIP_GREY_CALM}
                    size="huge"
                  />
                </TouchableOpacity>
              </View>
              {!isEmpty(errorPassword) && (
                <Text size={"tiny"} color={LIPSTICK_TWO} type={"Circular"}>
                  {errorPassword}
                </Text>
              )}
              <Spacer size={HP1} />
              <View style={style.formGroup}>
                <Text
                  textColor={SHIP_GREY}
                  size={"mini"}
                  weight={400}
                  type={"Circular"}
                >
                  Konfirmasi Password
                </Text>
                <View>
                  <TextInput
                    value={password_confirmation}
                    onChangeText={(password_confirmation) => {
                      let errorConfirmPassword = "";
                      this.setState({ password_confirmation });
                      if (password !== password_confirmation) {
                        errorConfirmPassword = "Password tidak sesuai";
                      }
                      this.setState({ errorConfirmPassword });
                    }}
                    secureTextEntry={!showPasswordConfirmation}
                    style={style.input}
                  />
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({
                        showPasswordConfirmation: !showPasswordConfirmation,
                      })
                    }
                    style={style.passwordToggler2}
                    size={"tiny"}
                    weight={500}
                    type={"Circular"}
                  >
                    <Icon
                      name={showPasswordConfirmation ? "eye-off" : "eye"}
                      type="MaterialCommunityIcons"
                      color={SHIP_GREY_CALM}
                      size="huge"
                    />
                  </TouchableOpacity>
                </View>
                {!isEmpty(errorConfirmPassword) && (
                  <Text size={"tiny"} color={LIPSTICK_TWO} type={"Circular"}>
                    {errorConfirmPassword}
                  </Text>
                )}
              </View>
              <Spacer size={WP8} />
              <ButtonV2
                style={{ paddingVertical: WP4 }}
                textSize={"slight"}
                text="Ubah Password"
                onPress={() => this.onSubmit()}
                // disabled={!(!isEmpty(password) && !isEmpty(password_confirmation) && !isLoading && password?.length >= 6 && password_confirmation?.length >= 6) && !completed}
                disabled={this._isButtonChangePasswordDisabled()}
                textColor={WHITE}
                color={REDDISH}
              />
            </View>
          )}
        </View>
        {isSuccess && (
          <PopUpInfo
            closeOnBackdrop={true}
            isVisible={isSuccess}
            template={{
              title: "Password berhasil diubah",
              content:
                "Password baru telah aktif,\n" + "silahkan Log in kembali. ",
              image: require("../assets/icons/icSuccesLarge.png"),
            }}
            onPress={() => this.loginNow()}
          />
        )}
        <Modal
          transparent
          onRequestClose={this.toggleModal}
          visible={modalVisible}
        >
          <Dialog
            width={270}
            isConfirmation={isConfirmation}
            height={isConfirmation ? 128 : 120}
            cancelText={"Sign Up"}
            confirmText={"Try Again"}
            onCancel={() =>
              this.setState({ modalVisible: false }, () => {
                Promise.resolve(navigation.popToTop()).then(() => {
                  navigateTo("RegisterV3");
                });
              })
            }
            onConfirm={this.toggleModal}
          >
            <Text
              centered
              style={{
                marginTop: isConfirmation ? 0 : WP4,
                paddingHorizontal: WP4,
              }}
              size={"xmini"}
            >
              {errorMessage}
            </Text>
            {!isConfirmation && (
              <TouchableOpacity
                style={{ paddingVertical: WP4 }}
                onPress={this.toggleModal}
                activeOpacity={0.8}
              >
                <Text
                  centered
                  color={LIPSTICK_TWO}
                  weight={500}
                  type={"Circular"}
                  size={"xmini"}
                >
                  Try Again
                </Text>
              </TouchableOpacity>
            )}
          </Dialog>
        </Modal>
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ResetPasswordScreen),
  null
);
