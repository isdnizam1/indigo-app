import React from 'react'
import { isNil, isEqual, pick } from 'lodash-es'
import {
  View,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  Modal,
  SafeAreaView,
} from 'react-native'
import { connect } from 'react-redux'
import Constants from 'expo-constants'
import * as AppleAuthentication from 'expo-apple-authentication'
import { Text, _enhancedNavigation, Image, Icon } from 'sf-components'
import {
  RED_GOOGLE,
  WHITE,
  BLUE_FACEBOOK,
  GUN_METAL,
  BLACK,
  TRANSPARENT,
  NAVY_DARK,
  PALE_GREY_TWO,
  PALE_LIGHT_BLUE_TWO,
  PALE_BLUE,
} from 'sf-constants/Colors'
import {
  HP1,
  HP2,
  WP90,
  WP10,
  HP3,
  WP4,
  WP5,
  WP7,
  WP8,
  WP3,
  WP305,
} from 'sf-constants/Sizes'
import { setUserId } from 'sf-utils/storage'
import { convertBlobToBase64, fetchAsBlob, isIOS } from 'sf-utils/helper'
import { FacebookAuth, GoogleAuth, AppleAuth } from 'sf-actions/externalAuth'
import {
  getProfileDetail,
  getCity,
  postRegisterV3Facebook,
  postRegisterV3Google,
  postRegisterV3Apple,
} from 'sf-actions/api'
import { GET_USER_DETAIL } from 'sf-services/auth/actionTypes'
import { SET_REGISTRATION } from 'sf-services/register/actionTypes'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import { GET_NOTIFICATION } from 'sf-services/notification/actionTypes'
import Spacer from 'sf-components/Spacer'
import Loader from 'sf-components/Loader'
import { setLoading } from 'sf-services/app/actionDispatcher'

const borderRadius = 9

const mapStateToProps = ({ register }) => ({
  register,
})

const style = StyleSheet.create({
  sfLogo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: WP8,
  },
})

const mapDispatchToProps = {
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail,
  }),
  setNewNotification: (notification) => ({
    type: `${GET_NOTIFICATION}_PUSHER`,
    response: notification,
  }),
  setRegistration: (data) => ({
    type: SET_REGISTRATION,
    response: data,
  }),
  setLoading,
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', true),
})

class LoginScreen extends React.Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props)
    this.handleSocialSignin = this.handleSocialSignin.bind(this)
    this.onFacebookButtonLayout = this.onFacebookButtonLayout.bind(this)
  }

  onFacebookButtonLayout = ({ nativeEvent: { layout } }) => {
    this.setState({ buttonWidth: layout.width, buttonHeight: layout.height })
  };

  handleSocialSignin = (dataResponse) => {
    const {
      dispatch,
      register: { data, behaviour },
      setRegistration,
      navigateTo,
      setUserDetailDispatcher,
      setLoading,
    } = this.props
    dispatch(getProfileDetail, { id_user: dataResponse.id_user }).then(
      async (dataResponse) => {
        let city_name = null
        let {
          registration_step,
          id_user,
          email,
          id_city,
          full_name,
          profile_type,
          profile_picture,
          registration_version,
          registered_via,
          gender,
        } = dataResponse
        const isV3 = registration_version == '3.0'
        if (registration_step !== 'finish') {
          try {
            profile_picture = await this.getBase64(profile_picture)
          } catch (err) {
            // better keep silent
          } finally {
            const step = parseInt(registration_step)
            if (!isNil(id_city)) {
              const cities = await dispatch(getCity, { offset: 0 })
              const city = cities.filter(
                (city) => parseInt(city.id_city) == parseInt(id_city),
              )[0]
              city_name = city.city_name
            }
            Promise.resolve(
              setRegistration({
                behaviour: {
                  ...behaviour,
                  step: !isV3 ? 3 : step < 3 ? 3 : step + 1,
                },
                data: {
                  ...data,
                  id_user,
                  id_city,
                  city_name,
                  email,
                  profile_type,
                  profile_picture,
                  name: full_name,
                  registered_via,
                  gender,
                },
              }),
            ).then(() => {
              this.setState(
                {
                  disable: false,
                  isLoading: false,
                },
                () => {
                  navigateTo('RegisterV3')
                },
              )
            })
          }
        } else {
          await setUserId(id_user.toString())
          setUserDetailDispatcher(dataResponse)
          navigateTo('LoadingScreen', {}, 'replace')
        }
      },
    )
  };

  state = {
    disable: false,
    emailLogin: false,
    buttonWidth: 210,
    buttonHeight: 23,
    appleLoginAvailable: false,
  };

  attachBackAction() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this._backHandler,
    )
  }

  releaseBackAction() {
    try {
      this.backHandler.remove()
    } catch (err) {
      /*silent is gold*/
    }
  }

  async getBase64(url) {
    let result = await fetchAsBlob(url)
    let base64 = await convertBlobToBase64(result)
    return base64
  }

  componentDidMount() {
    const { register, navigateTo } = this.props
    this.focusListener = this.props.navigation.addListener(
      'focus',
      this.attachBackAction.bind(this),
    )
    this.blurListener = this.props.navigation.addListener(
      'blur',
      this.releaseBackAction.bind(this),
    )
    register.data.id_user && navigateTo('RegisterV3')
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(
      pick(nextState, ['disable', 'isLoading', 'buttonWidth', 'buttonHeight']),
      pick(this.state, ['disable', 'isLoading', 'buttonWidth', 'buttonHeight']),
    )
  }

  componentWillUnmount() {
    try {
      this.releaseBackAction()
      typeof this.focusListener !== 'undefined' && this.focusListener()
      typeof this.blurListener !== 'undefined' && this.blurListener()
    } catch (err) {
      /*silent is gold*/
    }
  }

  _backHandler = async () => {
    const { emailLogin } = this.state
    if (emailLogin) {
      this.setState({ emailLogin: false })
    } else {
      this.props.navigateBack()
    }
  };

  _onPressFacebookAuth = async () => {
    const { dispatch } = this.props
    await this.setState({ disable: true })
    const facebookUserDetail = await FacebookAuth()
    if (facebookUserDetail) {
      dispatch(postRegisterV3Facebook, {
        id: facebookUserDetail.id,
        email: facebookUserDetail.email,
        name: (facebookUserDetail.name || '').trim(),
        picture: facebookUserDetail.picture.data.url,
      }).then(this.handleSocialSignin)
    } else {
      await this.setState({ disable: false })
    }
  };

  _onPressGoogleAuth = async () => {
    const { dispatch } = this.props
    await this.setState({ disable: true })
    const googleUserDetail = await GoogleAuth()
    if (googleUserDetail) {
      dispatch(postRegisterV3Google, {
        id: googleUserDetail.id,
        email: googleUserDetail.email,
        name: `${googleUserDetail.givenName} ${googleUserDetail.familyName}`.trim(),
        picture: googleUserDetail.photoUrl,
      }).then(this.handleSocialSignin)
    } else {
      await this.setState({ disable: false })
    }
  };

  _onPressAppleAuth = async () => {
    const { dispatch } = this.props
    await this.setState({ disable: true })

    AppleAuthentication.isAvailableAsync().then(async () => {
      const appleUserDetail = await AppleAuth()
      if (appleUserDetail) {
        dispatch(postRegisterV3Apple, {
          id: appleUserDetail.user,
          email: appleUserDetail.email,
          name:
            appleUserDetail.fullName && appleUserDetail.fullName.givenName
              ? `${appleUserDetail.fullName.givenName} ${appleUserDetail.fullName.familyName}`.trim()
              : '',
        }).then(this.handleSocialSignin)
      } else {
        await this.setState({ disable: false })
      }
    })
  };

  render() {
    const { navigateTo } = this.props
    const { disable, emailLogin } = this.state

    const { width, height } = Dimensions.get('window')

    return (
      <View style={{ flex: 1, width, height, backgroundColor: BLACK }}>
        <ImageBackground
          style={{
            flex: 1,
            width,
            height,
            resizeMode: 'cover',
            justifyContent: 'space-between',
          }}
          source={require('sf-assets/images/v3/bgSignin.jpg')}
        >
          <SafeAreaView style={{ flex: 1 }}>
            <View style={style.sfLogo}>
              <Spacer size={WP7} />
              <Image
                centered={false}
                size={'extraMassive'}
                imageStyle={style.sfLogoImage}
                aspectRatio={248 / 52}
                source={require('sf-assets/images/v3/sf.png')}
              />
              <Spacer size={WP7} />
              <Text
                lineHeight={WP8}
                size={'huge'}
                weight={600}
                color={WHITE}
                type={'Circular'}
              >
                {'Connect\nInteract\nCollaborate'}
              </Text>
            </View>
            <StatusBar backgroundColor={'#000'} barStyle={'light-content'} />
            <View style={{ width, flex: 1, justifyContent: 'flex-end' }}>
              {emailLogin && (
                <Icon
                  style={{ marginTop: WP10, marginLeft: WP3 }}
                  onPress={() => this.setState({ emailLogin: false })}
                  size='massive'
                  color={WHITE}
                  name='left'
                />
              )}
              <View
                style={{
                  paddingTop: Constants.statusBarHeight,
                  justifyContent: 'center',
                }}
              >
                <View style={{ paddingHorizontal: WP10 }}>
                  <TouchableOpacity
                    style={{
                      alignSelf: 'center',
                      width: WP90 - WP3,
                      marginVertical: HP3,
                      paddingVertical: 12,
                      alignItems: 'center',
                      paddingHorizontal: WP5,
                      flexDirection: 'row',
                      backgroundColor: NAVY_DARK,
                      borderWidth: 1,
                      justifyContent: 'space-between',
                      borderRadius,
                    }}
                    onPress={() => navigateTo('LoginEmailScreen')}
                  >
                    <Icon
                      name={'email-outline'}
                      size={'large'}
                      type={'MaterialCommunityIcons'}
                      color={WHITE}
                    />
                    <Text
                      numberOfLines={1}
                      centered
                      size='mini'
                      weight={500}
                      color={WHITE}
                    >
                      Continue with Email
                    </Text>
                    <Icon
                      name={'email-outline'}
                      size={'large'}
                      type={'MaterialCommunityIcons'}
                      color={TRANSPARENT}
                    />
                  </TouchableOpacity>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      width: WP90 - WP3,
                      marginLeft: -WP305,
                    }}
                  >
                    <View
                      style={{
                        borderBottomWidth: 0.5,
                        borderBottomColor: GUN_METAL,
                        flex: 1,
                      }}
                    />
                    <Text
                      weight={500}
                      style={{ marginHorizontal: WP4 }}
                      size='xmini'
                      color={PALE_LIGHT_BLUE_TWO}
                    >
                      OR
                    </Text>
                    <View
                      style={{
                        borderBottomWidth: 0.5,
                        borderBottomColor: GUN_METAL,
                        flex: 1,
                      }}
                    />
                  </View>
                  <View style={{ marginTop: HP1 }}>
                    <View style={{ marginVertical: HP2, alignSelf: 'center' }}>
                      <TouchableOpacity
                        onPress={this._onPressGoogleAuth}
                        style={{
                          width: WP90 - WP3,
                          paddingVertical: HP2,
                          alignItems: 'center',
                          flexDirection: 'row',
                          backgroundColor: RED_GOOGLE,
                          paddingHorizontal: WP5,
                          justifyContent: 'space-between',
                          borderRadius,
                        }}
                      >
                        <Icon
                          color={WHITE}
                          size='large'
                          name='google'
                          type='AntDesign'
                        />
                        <Text
                          type={'Circular'}
                          size='slight'
                          weight={500}
                          numberOfLines={1}
                          color={WHITE}
                          style={{ marginLeft: WP4 }}
                        >
                          Continue with Google
                        </Text>
                        <Icon
                          color={TRANSPARENT}
                          size='large'
                          name='google'
                          type='AntDesign'
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={{
                          width: WP90 - WP3,
                          marginVertical: WP3,
                          paddingVertical: HP2,
                          alignItems: 'center',
                          flexDirection: 'row',
                          backgroundColor: BLUE_FACEBOOK,
                          paddingHorizontal: WP5,
                          justifyContent: 'space-between',
                          borderRadius,
                        }}
                        onPress={this._onPressFacebookAuth}
                      >
                        <Icon
                          color={WHITE}
                          size='large'
                          name='facebook'
                          type='Entypo'
                        />
                        <Text
                          type={'Circular'}
                          size='slight'
                          weight={500}
                          numberOfLines={1}
                          color={WHITE}
                          style={{ marginLeft: WP4 }}
                        >
                          Continue with Facebook
                        </Text>
                        <Icon
                          color={TRANSPARENT}
                          size='large'
                          name='facebook'
                          type='Entypo'
                        />
                      </TouchableOpacity>
                      {isIOS() && (
                        <TouchableOpacity
                          onPress={this._onPressAppleAuth}
                          style={{
                            width: WP90 - WP3,
                            paddingVertical: HP2,
                            alignItems: 'center',
                            flexDirection: 'row',
                            backgroundColor: PALE_BLUE,
                            paddingHorizontal: WP5,
                            justifyContent: 'space-between',
                            borderRadius,
                          }}
                        >
                          <Icon
                            color={BLACK}
                            size='large'
                            name='apple1'
                            type='AntDesign'
                          />
                          <Text
                            type={'Circular'}
                            size='slight'
                            weight={500}
                            numberOfLines={1}
                            color={BLACK}
                            style={{ marginLeft: WP4 }}
                          >
                            Continue with Apple
                          </Text>
                          <Icon
                            color={TRANSPARENT}
                            size='large'
                            name='apple1'
                            type='AntDesign'
                          />
                        </TouchableOpacity>
                      )}
                    </View>
                  </View>
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    onPress={() => navigateTo('RegisterV3')}
                    style={{
                      alignSelf: 'center',
                      paddingVertical: WP3,
                      width,
                      borderTopWidth: 1,
                      borderTopColor: GUN_METAL,
                    }}
                  >
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                      <Text type={'Circular'} size='mini' color={PALE_GREY_TWO}>
                        {'Belum punya akun? '}
                      </Text>
                      <Text
                        type={'Circular'}
                        size='mini'
                        weight={600}
                        color={PALE_GREY_TWO}
                      >
                        Daftar disini
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <Modal visible={disable} transparent={true}>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  backgroundColor: 'rgba(0,0,0,0.55)',
                }}
              >
                <Loader size={'slight'} isLoading />
              </View>
            </Modal>
          </SafeAreaView>
        </ImageBackground>
      </View>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(LoginScreen),
  mapFromNavigationParam,
)
1
