import React from 'react'
import { View, ScrollView, AsyncStorage, BackHandler } from 'react-native'
import { isEmpty } from 'lodash-es'
import { connect } from 'react-redux'
import { Container, Header, Text, InputText, Button, Form, Icon, _enhancedNavigation } from '../../components/index'
import { PINK_RED_DARK, ORANGE_BRIGHT_DARK, WHITE, WHITE20, NO_COLOR } from '../../constants/Colors'
import { HP5, WP5, HP4 } from '../../constants/Sizes'
import { KEY_USER_LAST_SKIPPED_STEP } from '../../constants/Storage'
import { alertMessage } from '../../utils/alert'
import { getCity, getCountry, postRegisterLocation } from '../../actions/api'
import InputModal from '../../components/InputModal'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class RegisterLocationScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _onRegisterLocation = ({ formData }) => {
    const {
      userData,
      dispatch,
      navigateTo
    } = this.props

    if (
      isEmpty(formData.country.id_country) ||
      isEmpty(formData.city.id_city)
    ) alertMessage(null, 'Please fill the form correctly')
    else {
      const bodyRequest = {
        id_country: formData.country.id_country,
        id_city: formData.city.id_city,
        id_user: userData.id_user
      }
      dispatch(postRegisterLocation, bodyRequest)
        .then((dataResponse) => {
          navigateTo('RegisterPhotoScreen')
        })
    }
  }

  _backHandler = async () => {
    if (!this.props.initialLoaded) this.props.navigateBack()
  }

  render() {
    const {
      initialLoaded,
      isLoading,
      navigateTo
    } = this.props
    return (
      <Container isLoading={isLoading} colors={[ORANGE_BRIGHT_DARK, PINK_RED_DARK]} theme='light'>
        <Header>
          <Icon
            onPress={this._backHandler} size='massive'
            color={initialLoaded ? NO_COLOR : WHITE} name='left'
          />
        </Header>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='handled'
        >
          <View style={{ marginHorizontal: WP5 }}>
            <Text centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>Location</Text>
            <Form onSubmit={this._onRegisterLocation}>
              {({ onChange, onSubmit, formData }) => (
                <View style={{ marginVertical: HP4 }}>
                  <InputModal
                    triggerComponent={(
                      <InputText
                        value={formData.country ? formData.country.country_name : ''}
                        label='Country'
                        editable={false}
                      />
                    )}
                    header='Select Country'
                    suggestion={getCountry}
                    suggestionKey='country_name'
                    suggestionPathResult='country_name'
                    suggestionPathValue='id_country'
                    onChange={onChange('country')}
                    selected={formData.country ? formData.country.country_name : ''}
                    createNew={false}
                    asObject={true}
                    placeholder='Search country here…'
                  />

                  <InputModal
                    triggerComponent={(
                      <InputText
                        value={formData.city ? formData.city.city_name : ''}
                        label='City'
                        editable={false}
                      />
                    )}
                    header='Select City'
                    suggestion={getCity}
                    suggestionKey='city_name'
                    suggestionPathResult='city_name'
                    suggestionPathValue='id_city'
                    onChange={onChange('city')}
                    selected={formData.city ? formData.city.city_name : ''}
                    createNew={false}
                    asObject={true}
                    placeholder='Search city here…'
                  />

                  <Button
                    onPress={onSubmit}
                    style={{ marginTop: HP5 }}
                    rounded
                    centered
                    shadow='none'
                    backgroundColor={WHITE20}
                    textColor={WHITE}
                    text='Next'
                  />

                  <Button
                    onPress={async () => {
                      await AsyncStorage.setItem(KEY_USER_LAST_SKIPPED_STEP, '3')
                      navigateTo('RegisterPhotoScreen', {})
                    }}
                    style={{ alignSelf: 'center', marginVertical: 0 }}
                    rounded
                    centered
                    shadow='none'
                    backgroundColor={NO_COLOR}
                    textColor={WHITE}
                    textSize='small'
                    textWeight={300}
                    text='skip'
                  />
                </View>
              )}
            </Form>
          </View>
        </ScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(RegisterLocationScreen),
  mapFromNavigationParam
)
