import React from 'react'
import { BackHandler, Image, TouchableOpacity, View } from 'react-native'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import { isEmpty } from 'lodash-es'
import { connect } from 'react-redux'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { Button } from '../../components'
import { _enhancedNavigation, Container, Header, Icon, ListItem, Modal, Text } from '../../components/index'
import { GREY_DARK, SILVER_WHITE, WHITE } from '../../constants/Colors'
import { HP105, HP2, HP5, WP10, WP100, WP2, WP3, WP5 } from '../../constants/Sizes'
import { postRegisterStep2_v2, postRegisterStep2Skip_v2 } from '../../actions/api'
import { authDispatcher } from '../../services/auth/index'
import { GET_USER_DETAIL } from '../../services/auth/actionTypes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { invalidButtonStyle, validButtonStyle } from '../../utils/helper'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
  authIsLoading: auth.isLoading
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class RegisterPhotoScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  state = {
    photoProfile: null
  }

  componentDidMount = async () => {
    const {
      userData,
      getUserDetailDispatcher
    } = this.props
    await getUserDetailDispatcher({ id_user: userData.id_user })
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _onRegisterPhoto = () => {
    const {
      userData,
      dispatch,
      navigateTo,
      setUserDetailDispatcher
    } = this.props
    const {
      photoProfile,
      photoProfileBase64
    } = this.state

    const formData = {
      id_user: userData.id_user,
      profile_picture: photoProfileBase64 || photoProfile || userData.profile_picture
    }
    dispatch(postRegisterStep2_v2, formData)
      .then((dataResponse) => {
        const updatedUserData = userData
        updatedUserData.profile_picture = dataResponse.profile_picture
        setUserDetailDispatcher(updatedUserData)
        navigateTo('RegisterGenreScreen')
      })
  }

  _onSkip = () => {
    const {
      userData,
      dispatch,
      navigateTo,
    } = this.props
    const formData = {
      id_user: userData.id_user,
    }
    dispatch(postRegisterStep2Skip_v2, formData)
      .then(() => {
        navigateTo('RegisterGenreScreen')
      })
  }

  _selectPhoto = async () => {
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCameraRoll === 'granted') {
      return await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
    }
  }

  _takePhoto = async () => {
    const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
      return await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
    }
  }

  _handlePhoto = async (result) => {
    if (!result) {
      return
    }

    if (!result.cancelled) this.setState({ photoProfile: result.uri, photoProfileBase64: result.base64 })
  }

  _backHandler = async () => {
    if (!this.props.initialLoaded) this.props.navigateBack()
  }

  _isValid = () => {
    const {
      photoProfile
    } = this.state

    const {
      userData
    } = this.props

    if (userData.registered_via !== 'email' && userData.registered_via !== null) {
      return validButtonStyle
    }

    if (
      isEmpty(photoProfile)
    ) return invalidButtonStyle
    return validButtonStyle
  }

  render() {
    const {
      isLoading,
      authIsLoading,
      userData
    } = this.props

    const {
      photoProfile
    } = this.state

    const isValid = this._isValid()
    return (
      <Container isLoading={isLoading || authIsLoading} colors={[SILVER_WHITE, SILVER_WHITE]} theme='dark'>
        <Header />
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: wp('100%'), paddingHorizontal: WP3 }}>
          <Text size='mini' color={GREY_DARK} style={{ flexGrow: 0 }}>Step 2 of 3</Text>
        </View>
        <View style={{ marginHorizontal: WP5, marginTop: WP3 }}>
          <View style={{ paddingHorizontal: WP5, marginBottom: WP10 }}>
            <Text centered color={GREY_DARK} type='SansPro' weight={500} size='massive'>Choose Your Profile Photo</Text>
            <Text centered color={GREY_DARK} type='OpenSans' weight={300} size='mini'>Choose your best selfie and upload
              it now!</Text>
          </View>
          {
            !isEmpty(userData) && (
              <View style={{ marginHorizontal: WP5 }}>
                <View style={{ marginVertical: HP2 }}>
                  <Modal
                    renderModalContent={({ toggleModal }) => (
                      <View>
                        <ListItem
                          inline onPress={() => {
                            this._selectPhoto().then((result) => {
                              toggleModal()
                              this._handlePhoto(result)
                            })
                          }}
                        >
                          <Icon type='MaterialIcons' size='large' name='photo-library'/>
                          <Text style={{ paddingLeft: WP2 }}>Select from library</Text>
                        </ListItem>
                        <ListItem
                          inline onPress={() => {
                            this._takePhoto().then((result) => {
                              toggleModal()
                              this._handlePhoto(result)
                            })
                          }}
                        >
                          <Icon size='large' name='camera'/>
                          <Text style={{ paddingLeft: WP2 }}>Take a picture</Text>
                        </ListItem>
                      </View>
                    )}
                  >
                    {({ toggleModal }, M) => (
                      <View>
                        <TouchableOpacity
                          activeOpacity={TOUCH_OPACITY}
                          onPress={toggleModal}
                          style={{
                            marginVertical: HP2, alignSelf: 'center',
                            borderRadius: WP100, borderWidth: 1, borderColor: WHITE
                          }}
                        >
                          {
                            isEmpty(userData.profile_picture) && isEmpty(photoProfile)
                              ? (
                                <View style={{
                                  width: 150,
                                  height: 150,
                                  backgroundColor: 'rgb(216,216,216)',
                                  borderRadius: WP100,
                                  justifyContent: 'center',
                                  alignItems: 'center'
                                }}
                                >
                                  <Image
                                    source={require('../../assets/icons/iconCamera.png')}
                                    style={{ height: 62, width: 75 }}
                                  />
                                </View>
                              )
                              : (
                                <View>
                                  <Image
                                    source={{ uri: photoProfile || userData.profile_picture }}
                                    style={{
                                      // width: ICON_SIZE['massive'] + (WP7 * 2),
                                      width: 150,
                                      height: undefined,
                                      aspectRatio: 1,
                                      borderRadius: 150 / 2
                                    }}
                                  />
                                  <Image
                                    source={require('../../assets/images/cameraChangePhoto.png')}
                                    style={{
                                      position: 'absolute',
                                      width: 150,
                                      height: undefined,
                                      aspectRatio: 1,
                                      borderRadius: 150 / 2
                                    }}
                                  />
                                </View>
                              )
                          }
                        </TouchableOpacity>
                        {M}
                      </View>
                    )}
                  </Modal>
                  <Text
                    style={{ marginVertical: HP105 }} centered size='small' type='SansPro' weight={500}
                    color={GREY_DARK}
                  >{userData.full_name}</Text>
                  <Text
                    centered color={GREY_DARK}
                    size='mini'
                  >{`${userData.job_title}, ${userData.location ? userData.location.city_name : 'Indonesia'}`}</Text>

                </View>
              </View>
            )
          }
          <View>
            <Button
              onPress={this._onRegisterPhoto}
              style={{ flexGrow: 0, marginTop: HP5 }}
              disable={isValid.disabled}
              colors={isValid.buttonColor}
              rounded
              centered
              text='Next'
              textColor={WHITE}
              textSize='small'
              textWeight={500}
              shadow='none'
            />
            <TouchableOpacity onPress={this._onSkip}>
              <Text centered size='mini' weight={500}>Skip</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(RegisterPhotoScreen),
  mapFromNavigationParam
)
