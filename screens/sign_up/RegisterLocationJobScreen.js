import React from 'react'
import { ScrollView, TouchableOpacity, View } from 'react-native'
import { cloneDeep, isEmpty, noop } from 'lodash-es'
import { connect } from 'react-redux'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { _enhancedNavigation, Container, Header, Icon, Text, Button } from '../../components/index'
import {
  GREY_DARK,
  GREY_LABEL,
  NO_COLOR,
  ORANGE_BRIGHT,
  RED_GOOGLE,
  SILVER_WHITE,
  WHITE,
  TEAL,
  LIGHT_TEAL,
  LIPSTICK,
  LIGHT_PINK
} from '../../constants/Colors'
import { HP2, HP5, HP90, WP1, WP10, WP100, WP2, WP3, WP4, HP1 } from '../../constants/Sizes'
import { getCity, getCompany, getJob, postRegisterStep1_v2, getDetailReferralCode } from '../../actions/api'
import InputModal from '../../components/InputModal'
import InputTextLight from '../../components/InputTextLight'
import { invalidButtonStyle, validButtonStyle } from '../../utils/helper'
import { authDispatcher } from '../../services/auth'
import { GET_USER_DETAIL } from '../../services/auth/actionTypes'
import { BORDER_COLOR, BORDER_WIDTH } from '../../constants/Styles'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class RegisterLocationJobScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  constructor(props) {
    super(props)
    this.state = {
      city: {},
      company_name: '',
      job_title: '',
      another_job_title: [],
      another_company_name: [],
      referral_code: '',
      isValidCode: ''
    }
  }

  _onRegisterJob = () => {
    const {
      userData,
      dispatch,
      navigateTo,
      setUserDetailDispatcher
    } = this.props

    const formData = {
      id_user: userData.id_user,
      id_city: this.state.city.id_city,
      company_name: this.state.company_name,
      job_title: this.state.job_title,
      another_company_name: this.state.another_company_name,
      another_job_title: this.state.another_job_title,
      referral_code: this.state.referral_code
    }
    dispatch(postRegisterStep1_v2, formData)
      .then((dataResponse) => {
        const user = { ...userData, ...this.state }
        user.location = { ...this.state.city }

        setUserDetailDispatcher(user)
        navigateTo('RegisterPhotoScreen')
      })
  }

  _isValid = () => {
    const {
      city, company_name, job_title
    } = this.state

    if (
      isEmpty(city) ||
      isEmpty(company_name) ||
      isEmpty(job_title)
    ) return invalidButtonStyle
    return validButtonStyle
  }

  _onChange = (key) => (value) => {
    this.setState({ [key]: value })
  }

  _onChangeMultiple = (key, index) => (value) => {
    const attribute = cloneDeep(this.state[key])
    attribute[index] = value
    this.setState({ [key]: attribute })
  }

  _onAddJob = () => {
    const another_company_name = cloneDeep(this.state.another_company_name)
    const another_job_title = cloneDeep(this.state.another_job_title)
    another_company_name.push('')
    another_job_title.push('')
    this.setState({ another_job_title, another_company_name })
  }

  _onRemoveJob = (index) => {
    const another_company_name = cloneDeep(this.state.another_company_name)
    const another_job_title = cloneDeep(this.state.another_job_title)
    another_job_title.splice(index, 1)
    another_company_name.splice(index, 1)
    this.setState({ another_job_title, another_company_name })
  }

  _checkReferralCode = async () => {
    const {
      referral_code
    } = this.state
    try {
      const dataResponse = await this.props.dispatch(getDetailReferralCode, { referral_code }, noop, true, true)
      this.setState({ isValidCode: dataResponse.code == 200 ? 'valid' : 'invalid' })
    } catch (e) {
      //
    }
  }

  render() {
    const {
      isLoading
    } = this.props

    const {
      city, company_name, job_title, another_company_name, another_job_title, referral_code, isValidCode
    } = this.state

    const isValid = this._isValid()

    return (
      <Container isLoading={isLoading} colors={[SILVER_WHITE, SILVER_WHITE]} theme='dark'>
        <View style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
          width: wp('100%'),
          paddingHorizontal: WP3,
          marginTop: WP4
        }}
        >
          <Text size='mini' color={GREY_DARK} style={{ flexGrow: 0 }}>Step 1 of 3</Text>
        </View>
        <Header style={{ marginTop: 0 }}>
          <Icon size='massive' color={NO_COLOR} name='left'/>
          <Text size='massive' type='SansPro' weight={500} color={GREY_DARK}>SIGN YOURSELF UP!</Text>
          <Icon size='massive' color={NO_COLOR} name='left'/>
        </Header>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='handled'
        >
          <View style={{ marginHorizontal: WP10, minHeight: HP90 }}>
            <View>
              <View style={{ marginTop: HP2 }}>
                <InputModal
                  triggerComponent={(
                    <InputTextLight
                      bold
                      color={GREY_LABEL}
                      labelColor={GREY_LABEL}
                      value={city.city_name}
                      label='Where do you live?'
                      placeholder='Ex: Jakarta'
                      editable={false}
                    />
                  )}
                  header='Select City'
                  suggestion={getCity}
                  suggestionKey='city_name'
                  suggestionPathResult='city_name'
                  suggestionPathValue='id_city'
                  onChange={this._onChange('city')}
                  suggestionCreateNewOnEmpty={false}
                  selected={city.city_name}
                  asObject={true}
                  createNew={false}
                  placeholder='Search city here...'
                />
              </View>

              <Text size='mini' color={GREY_LABEL} weight={500}>What are you doing in music industry?</Text>
              <Text size='mini' color={RED_GOOGLE} weight={500}>Headline</Text>
              <View style={{
                borderWidth: BORDER_WIDTH,
                borderColor: BORDER_COLOR,
                borderRadius: WP2,
                paddingHorizontal: WP4,
                paddingVertical: WP2
              }}
              >
                <InputModal
                  triggerComponent={(
                    <InputTextLight
                      bold
                      color={GREY_LABEL}
                      labelColor={GREY_LABEL}
                      value={job_title}
                      label='Job title'
                      placeholder='Ex: Vocalist'
                      editable={false}
                    />
                  )}
                  header='Select profession'
                  suggestion={getJob}
                  suggestionKey='job_title'
                  suggestionPathResult='job_title'
                  onChange={this._onChange('job_title')}
                  selected={job_title}
                  placeholder='Search job here...'
                />

                <InputModal
                  triggerComponent={(
                    <InputTextLight
                      bold
                      color={GREY_LABEL}
                      labelColor={GREY_LABEL}
                      value={company_name}
                      label='Company'
                      placeholder='Ex: Self Employed'
                      editable={false}
                    />
                  )}
                  header='Select Band or Company'
                  suggestion={getCompany}
                  suggestionKey='company_name'
                  suggestionPathResult='company_name'
                  onChange={this._onChange('company_name')}
                  selected={company_name}
                  placeholder='Search company here...'
                />
                <TouchableOpacity
                  onPress={this._onAddJob}
                  style={{ flexDirection: 'row', justifyContent: 'center' }}
                >
                  <Icon centered name='pluscircleo' color={ORANGE_BRIGHT} size='slight'/>
                  <Text style={{ flexGrow: 0, marginHorizontal: WP2 }} size='mini' weight={500} color={ORANGE_BRIGHT}>
                    Add another job
                  </Text>
                </TouchableOpacity>
              </View>

              {
                another_job_title.map((job, index) => (
                  <View
                    key={index}
                    style={{
                      borderWidth: BORDER_WIDTH,
                      borderColor: BORDER_COLOR,
                      borderRadius: WP2,
                      paddingHorizontal: WP4,
                      paddingVertical: WP2,
                      marginTop: WP1
                    }}
                  >
                    <TouchableOpacity
                      onPress={this._onRemoveJob}
                      style={{
                        backgroundColor: GREY_LABEL,
                        padding: 2,
                        flexGrow: 0,
                        position: 'absolute',
                        right: 4,
                        top: 5,
                        borderRadius: WP100
                      }}
                    >
                      <Icon centered name='close' color={WHITE} size='mini'/>
                    </TouchableOpacity>
                    <InputModal
                      triggerComponent={(
                        <InputTextLight
                          bold
                          color={GREY_LABEL}
                          labelColor={GREY_LABEL}
                          value={another_job_title[index]}
                          label='Job title'
                          placeholder='Ex: Vocalist'
                          editable={false}
                        />
                      )}
                      header='Select profession'
                      suggestion={getJob}
                      suggestionKey='job_title'
                      suggestionPathResult='job_title'
                      onChange={this._onChangeMultiple('another_job_title', index)}
                      selected={another_job_title[index]}
                      placeholder='Search job here...'
                    />

                    <InputModal
                      triggerComponent={(
                        <InputTextLight
                          bold
                          color={GREY_LABEL}
                          labelColor={GREY_LABEL}
                          value={another_company_name[index]}
                          label='Company'
                          placeholder='Ex: Self Employed'
                          editable={false}
                        />
                      )}
                      header='Select Band or Company'
                      suggestion={getCompany}
                      suggestionKey='company_name'
                      suggestionPathResult='company_name'
                      onChange={this._onChangeMultiple('another_company_name', index)}
                      selected={another_company_name[index]}
                      placeholder='Search company here...'
                    />
                  </View>
                ))
              }
              <View>
                <View style={{
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  alignItems: 'flex-end',
                  marginBottom: WP3
                }}
                >
                  <InputTextLight
                    autoCapitalize='none'
                    bold
                    label='Add referral code (optional)'
                    value={referral_code}
                    color={GREY_DARK}
                    onChangeText={this._onChange('referral_code')}
                    returnKeyType='done'
                  />
                  <Button
                    disable={!referral_code}
                    style={{ marginLeft: WP3, marginVertical: HP1 }}
                    onPress={this._checkReferralCode}
                    backgroundColor={TEAL}
                    centered
                    text='Check'
                    textColor={WHITE}
                    textSize='tiny'
                    textWeight={500}
                    shadow='none'
                    radius={6}
                  />
                </View>
                {isValidCode ? (
                  <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderRadius: 6,
                    paddingHorizontal: WP3,
                    paddingVertical: WP2,
                    backgroundColor: isValidCode == 'valid' ? LIGHT_TEAL : LIGHT_PINK
                  }}
                  >
                    <Text size='xtiny' color={isValidCode == 'valid' ? TEAL : LIPSTICK} weight={500}>{`Your code is ${isValidCode}`}</Text>
                    <Icon
                      type={isValidCode == 'valid' ? 'MaterialCommunityIcons' : 'Ionicons'}
                      name={isValidCode == 'valid' ? 'check-circle' : 'ios-close-circle-outline'}
                      color={isValidCode == 'valid' ? TEAL : LIPSTICK}
                      size='mini'
                    />
                  </View>
                ) : null}
              </View>
            </View>

            <Button
              onPress={this._onRegisterJob}
              style={{ flexGrow: 0, marginTop: HP5 }}
              disable={isValid.disabled}
              colors={isValid.buttonColor}
              rounded
              centered
              text='Next'
              textColor={WHITE}
              textSize='small'
              textWeight={500}
              shadow='none'
            />

          </View>
        </ScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(RegisterLocationJobScreen),
  mapFromNavigationParam
)
