import React from 'react'
import {
  BackHandler,
  ScrollView,
  View,
  Platform,
  KeyboardAvoidingView
} from 'react-native'
import { isEmpty } from 'lodash-es'
import { connect } from 'react-redux'
import { _enhancedNavigation, Container, Header, Text, Button } from '../../components/index'
import { BLUE_DARK, BLUE_DARK_LIGHT, GREY_SEMI, ORANGE_BRIGHT_DARK, PINK_RED_DARK, WHITE } from '../../constants/Colors'
import { HP5, WP1, WP10, WP100, WP15, WP2, WP20, WP3, WP5 } from '../../constants/Sizes'
import { getTopGenre, postRegisterStep3_v2 } from '../../actions/api'
import Image from '../../components/Image'
import { LinearGradient } from '../../components/'
import { LINEAR_TYPE } from '../../constants/Styles'
import { invalidButtonStyle, validButtonStyle } from '../../utils/helper'
import { authDispatcher } from '../../services/auth'
import { GET_USER_DETAIL } from '../../services/auth/actionTypes'
import Loader from '../../components/Loader'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class RegisterGenreScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  constructor(props) {
    super(props)
    this.state = {
      genres: [],
      interest_name: []
    }
  }

  async componentDidMount() {
    const {
      dispatch
    } = this.props

    const data = await dispatch(getTopGenre, { limit: 24 })
    this.setState({ genres: data || [] })

    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _onRegisterGenre = ({ formData }) => {
    const {
      userData,
      dispatch,
      navigateTo
    } = this.props

    const bodyRequest = {
      interest_name: this.state.interest_name,
      id_user: userData.id_user
    }
    dispatch(postRegisterStep3_v2, bodyRequest)
      .then((dataResponse) => {
        if (userData.registered_via !== 'email' && userData.registered_via !== null) {
          navigateTo('AppNavigator', { defaultTab: 'discover' })
        } else {
          navigateTo('RegisterVerificationScreen')
        }
      })
  }

  _backHandler = async () => {
    if (!this.props.initialLoaded) this.props.navigateBack()
  }

  _onClickGenre = async (genreName) => {
    let genres = [...this.state.interest_name]
    const index = genres.indexOf(genreName)
    if (index >= 0) {
      genres.splice(index, 1)
    } else {
      genres.push(genreName)
    }
    this.setState({ interest_name: genres })
  }

  _genreItem = (genre) => {
    const {
      interest_name
    } = this.state
    return (
      <LinearGradient
        colors={interest_name.includes(genre.genre_name) ? [PINK_RED_DARK, ORANGE_BRIGHT_DARK] : [GREY_SEMI, GREY_SEMI]}
        {...LINEAR_TYPE.horizontal}
        style={{ margin: WP3, width: WP20, height: WP20, justifyContent: 'center', alignItems: 'center' }}
        key={genre.genre_name}
      >
        <Image
          onPress={() => this._onClickGenre(genre.genre_name)}
          source={{ uri: genre.icon }}
          style={{ height: WP10 }}
          aspectRatio={1}
          imageStyle={{ height: WP10 }}
        />
        <Text color={WHITE} size='tiny' style={{ marginTop: WP2 }}>{genre.genre_name}</Text>
      </LinearGradient>
    )
  }

  _isValid = () => {
    const {
      interest_name
    } = this.state

    if (
      interest_name.length < 3
    ) return invalidButtonStyle
    return validButtonStyle
  }

  render() {
    const {
      isLoading
    } = this.props

    const {
      genres
    } = this.state

    const isValid = this._isValid()

    return (
      <Container isLoading={isLoading} colors={[BLUE_DARK, BLUE_DARK_LIGHT]} theme='light'>
        <Header/>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='handled'
        >
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null} style={{ marginHorizontal: WP5, flex: 1 }}
          >
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: WP100, paddingHorizontal: WP10 }}>
              <Text size='mini' color={WHITE} style={{ flexGrow: 0, marginBottom: WP1 }}>Step 3 of 3</Text>
            </View>
            <View style={{ marginHorizontal: WP5, marginBottom: WP3 }}>
              <Text centered color={WHITE} type='SansPro' weight={500} size='extraMassive' style={{ marginBottom: 10 }}>What
                Do You Like</Text>
              <Text centered color={WHITE} type='OpenSans' weight={300} size='mini'>Please share with us at least 3 of
                your favorite genres so we can display content cording on your preferences!</Text>
            </View>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
              {
                isEmpty(genres) && <Loader isLoading/>
              }
              {
                genres.map((item) => (
                  this._genreItem(item)
                ))
              }
            </View>
            <View style={{ marginBottom: WP15, marginTop: WP5, paddingHorizontal: WP5 }}>

              <Button
                onPress={this._onRegisterGenre}
                disable={isValid.disabled}
                style={{ flexGrow: 0, marginTop: HP5 }}
                colors={isValid.buttonColor}
                rounded
                centered
                text='Next'
                textColor={WHITE}
                textSize='small'
                textWeight={500}
                shadow='none'
              />
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(RegisterGenreScreen),
  mapFromNavigationParam
)
