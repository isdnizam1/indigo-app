import React from 'react'
import { View, ScrollView } from 'react-native'
import { isEmpty } from 'lodash-es'
import { connect } from 'react-redux'
import { Container, Header, Text, InputText, Button, Form, Icon, _enhancedNavigation } from '../../components/index'
import { PINK_RED_DARK, ORANGE_BRIGHT_DARK, WHITE, WHITE20, NO_COLOR } from '../../constants/Colors'
import { HP5, WP5, HP4 } from '../../constants/Sizes'
import { alertMessage } from '../../utils/alert'
import { getJob, getCompany, postRegisterMusicIndustry } from '../../actions/api'
import InputModal from '../../components/InputModal'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class RegisterJobScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false),
  })

  _onRegisterJob = ({ formData }) => {
    const {
      userData,
      dispatch,
      navigateTo,
    } = this.props

    if (
      isEmpty(formData.job_title)
    ) alertMessage(null, 'Please fill the form correctly')
    else {
      formData.id_user = userData.id_user
      dispatch(postRegisterMusicIndustry, formData)
        .then((dataResponse) => {
          navigateTo('RegisterLocationScreen')
        })
    }
  }

  render() {
    const {
      isLoading
    } = this.props

    return (
      <Container isLoading={isLoading} colors={[ORANGE_BRIGHT_DARK, PINK_RED_DARK]} theme='light'>
        <Header>
          <Icon onPress={() => {}} size='massive' color={NO_COLOR} name='left'/>
        </Header>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='handled'
        >
          <View style={{ marginHorizontal: WP5 }}>
            <Text centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>What are you doing in music industry?</Text>
            <Form onSubmit={this._onRegisterJob}>
              {({ onChange, onSubmit, formData }) => (
                <View style={{ marginVertical: HP4 }}>
                  <InputModal
                    triggerComponent={
                      <InputText
                        value={formData.job_title}
                        label='Job Title'
                        editable={false}
                      />
                    }
                    header='Select Job Title'
                    suggestion={getJob}
                    suggestionKey='job_title'
                    suggestionPathResult='job_title'
                    onChange={onChange('job_title')}
                    selected={formData.job_title}
                    placeholder='Search profession here…'
                  />
                  <InputModal
                    triggerComponent={
                      <InputText
                        value={formData.company_name}
                        label='Band/Company name (Optional)'
                        editable={false}
                      />
                    }
                    header='Select Band/Company'
                    suggestion={getCompany}
                    suggestionKey='company_name'
                    suggestionPathResult='company_name'
                    onChange={onChange('company_name')}
                    selected={formData.company_name}
                    placeholder='Search band or company here…'
                  />
                  <Button
                    onPress={onSubmit}
                    style={{ marginTop: HP5 }}
                    rounded
                    centered
                    shadow='none'
                    backgroundColor={WHITE20}
                    textColor={WHITE}
                    text='Next'
                  />
                </View>
              )}
            </Form>
          </View>
        </ScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(RegisterJobScreen),
  mapFromNavigationParam
)
