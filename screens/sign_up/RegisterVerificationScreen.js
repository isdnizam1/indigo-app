import React from 'react'
import { BackHandler, View } from 'react-native'
import { connect } from 'react-redux'
import { _enhancedNavigation, Container, Text, Button } from '../../components/index'
import { GREY_DARK, SILVER_WHITE, WHITE } from '../../constants/Colors'
import { HP5, WP10, WP5 } from '../../constants/Sizes'
import { getProfileDetail, postVerificationLink } from '../../actions/api'
import { invalidButtonStyle, validButtonStyle } from '../../utils/helper'
import { alertMessage } from '../../utils/alert'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

let backgroundIntervalEmailChecker = null
let backgroundIntervalCounter = null

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class RegisterVerificationScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  constructor(props) {
    super(props)
    this.state = {
      counter: 0
    }
  }

  componentDidMount = async () => {
    const {
      userData,
      navigateTo
    } = this.props

    backgroundIntervalEmailChecker = setInterval(() => {
      getProfileDetail({ id_user: userData.id_user })
        .then(async ({ data: { result: dataResponse } }) => {
          if (dataResponse.email_status === 'verified') {
            await clearInterval(backgroundIntervalEmailChecker)
            navigateTo('AppNavigator', { defaultTab: 'discover' })
          }
        })
        .catch((err) => err)
    }, 5000) // Change this to pusher

    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  async componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
    await clearInterval(backgroundIntervalEmailChecker)
  }

  _onSendVerification = () => {
    const {
      userData,
      dispatch,
    } = this.props

    const bodyRequest = {
      email: userData.email,
      verified_by: 'mobile'
    }

    this.setState({ counter: 10 })

    dispatch(postVerificationLink, bodyRequest)
      .then((dataResponse) => {
        alertMessage('Verify Your Email', 'Check your email inbox now')

        backgroundIntervalCounter = setInterval(async () => {
          if (this.state.counter === 0) await clearInterval(backgroundIntervalCounter)
          else this.setState({ counter: this.state.counter - 1 })
        }, 1000)
      })
  }

  _backHandler = async () => {
    if (!this.props.initialLoaded) this.props.navigateBack()
  }

  _isValid = () => {
    if (this.state.counter === 0) return validButtonStyle
    return invalidButtonStyle
  }

  render() {
    const {
      isLoading,
      userData
    } = this.props

    const buttonStyle = this._isValid()

    return (
      <Container isLoading={isLoading} colors={[SILVER_WHITE, SILVER_WHITE]} theme='dark'>
        <View style={{ marginHorizontal: WP5, justifyContent: 'space-between', flexGrow: 1, flex: 1 }}>
          <View style={{ paddingHorizontal: WP5, marginVertical: WP10 }}>
            <Text centered color={GREY_DARK} type='SansPro' weight={500} size='massive'>Verify Your Email</Text>
            <Text centered color={GREY_DARK} type='OpenSans' weight={300} size='mini'>You&apos;re almost done! Click the
              button bellow to send verification link to your email</Text>
          </View>
          <View>
            <Text centered color={GREY_DARK} type='OpenSans' weight={300} size='slight'>Your email is</Text>
            <Text centered color={GREY_DARK} type='OpenSans' weight={400} size='small'>{userData.email}</Text>
          </View>
          <View style={{ marginBottom: WP10, paddingHorizontal: WP5 }}>

            <Button
              onPress={this._onSendVerification}
              style={{ flexGrow: 0, marginTop: HP5 }}
              colors={buttonStyle.buttonColor}
              disable={buttonStyle.disabled}
              rounded
              centered
              text='Send verification link'
              textColor={WHITE}
              textSize='small'
              textWeight={500}
              shadow='none'
            />

            {
              this.state.counter !== 0 && (
                <Text
                  color={GREY_DARK} centered
                  size='slight'
                >{`You can send verification link on (${this.state.counter}s)`}</Text>
              )
            }
          </View>
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(RegisterVerificationScreen),
  mapFromNavigationParam
)
