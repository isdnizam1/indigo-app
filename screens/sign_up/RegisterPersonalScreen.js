import React from 'react'
import { View, Platform, KeyboardAvoidingView } from 'react-native'
import { isEmpty } from 'lodash-es'
import { connect } from 'react-redux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { _enhancedNavigation, Container, Header, Icon, Text, Button } from '../../components/index'
import { GREY_DARK, NO_COLOR, SILVER_WHITE, WHITE } from '../../constants/Colors'
import { HP4, HP5, WP5 } from '../../constants/Sizes'
import { setUserId } from '../../utils/storage'
import { postRegister_v2 } from '../../actions/api'
import { GET_USER_DETAIL } from '../../services/auth/actionTypes'
import { GET_NOTIFICATION } from '../../services/notification/actionTypes'
import { registerUser } from '../../utils/registerUser'
import InputTextLight from '../../components/InputTextLight'
import { invalidButtonStyle, validButtonStyle } from '../../utils/helper'

const mapDispatchToProps = {
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  }),
  setNewNotification: (notification) => ({
    type: `${GET_NOTIFICATION}_PUSHER`,
    response: notification
  })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class RegisterPersonalScreen extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  constructor(props) {
    super(props)
    this.state = {
      full_name: '',
      email: '',
      password: '',
      password_confirmation: ''
    }
  }

  _isValid = () => {
    const {
      full_name, email, password, password_confirmation
    } = this.state

    if (
      isEmpty(full_name) ||
      isEmpty(email) ||
      isEmpty(password) ||
      isEmpty(password_confirmation)
    ) return invalidButtonStyle
    return validButtonStyle
  }

  _onRegisterPersonal = () => {
    const {
      dispatch,
      navigateTo,
      setUserDetailDispatcher
    } = this.props

    const formData = {
      ...this.state
    }

    dispatch(postRegister_v2, formData, async (dataResponse) => {
      await setUserId(dataResponse.id_user.toString())
      await registerUser(dataResponse.id_user.toString(), this.props)
      setUserDetailDispatcher(dataResponse)
    })
      .then(async (dataResponse) => {
        navigateTo('RegisterLocationJobScreen', { initialState: true })
      })
  }

  _onChange = (key) => (value) => {
    this.setState({ [key]: value })
  }

  render() {
    const {
      isLoading,
      navigateBack
    } = this.props

    const {
      full_name, email, password, password_confirmation
    } = this.state

    const isValid = this._isValid()

    return (
      <Container isLoading={isLoading} colors={[SILVER_WHITE, SILVER_WHITE]} theme='dark'>
        <Header>
          <Icon
            onPress={() => {
              navigateBack()
            }} size='massive' color={GREY_DARK} name='left'
          />
          <Text size='massive' type='SansPro' weight={500} color={GREY_DARK}>SIGN YOURSELF UP!</Text>
          <Icon size='massive' color={NO_COLOR} name='left'/>
        </Header>
        <KeyboardAwareScrollView keyboardShouldPersistTaps='handled'>
          <View style={{ marginHorizontal: WP5, justifyContent: 'space-between', flexGrow: 1 }}>
            <KeyboardAvoidingView
              behavior={Platform.OS === 'ios' ? 'padding' : null}
              style={{ marginVertical: HP4 }}
            >
              <InputTextLight
                bold
                label='What is your name?'
                value={full_name}
                color={GREY_DARK}
                onChangeText={this._onChange('full_name')}
                placeholder='Johny Genjreng'
                returnKeyType='next'
              />
              <InputTextLight
                bold
                label='What is your E-mail address?'
                value={email}
                color={GREY_DARK}
                onChangeText={this._onChange('email')}
                placeholder='your@email.com'
                returnKeyType='next'
              />
              <InputTextLight
                bold
                label='Password'
                value={password}
                color={GREY_DARK}
                onChangeText={this._onChange('password')}
                returnKeyType='next'
                placeholder='Enter your password'
                secureTextEntry toggleSecureTextEntry
              />
              <InputTextLight
                bold
                label='Confirm Password'
                value={password_confirmation}
                color={GREY_DARK}
                onChangeText={this._onChange('password_confirmation')}
                returnKeyType='next'
                placeholder='Confirm Password'
                secureTextEntry toggleSecureTextEntry
              />
            </KeyboardAvoidingView>

            <Button
              onPress={this._onRegisterPersonal}
              style={{ flexGrow: 0, marginTop: HP5 }}
              colors={isValid.buttonColor}
              disable={isValid.disabled}
              rounded
              centered
              text='Next'
              textColor={WHITE}
              textSize='small'
              textWeight={500}
              shadow='none'
            />

          </View>
        </KeyboardAwareScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(null, mapDispatchToProps)(RegisterPersonalScreen),
  mapFromNavigationParam
)
