import React from 'react'
import { View, ScrollView, AsyncStorage, KeyboardAvoidingView, Platform } from 'react-native'
import { isEmpty } from 'lodash-es'
import { connect } from 'react-redux'
import { Container, Header, Text, InputText, Button, Form, Icon, _enhancedNavigation } from '../../components/index'
import { PINK_RED_DARK, ORANGE_BRIGHT_DARK, WHITE, WHITE20, NO_COLOR } from '../../constants/Colors'
import { HP5, WP5, HP2 } from '../../constants/Sizes'
import { KEY_USER_LAST_SKIPPED_STEP } from '../../constants/Storage'
import { alertMessage } from '../../utils/alert'
import { getGenreInterest, postInterest } from '../../actions/api'
import { getStringArrayFromObjectArray } from '../../utils/array'
import { authDispatcher } from '../../services/auth/index'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', true),
  refreshProfile: getParam('refreshProfile', () => {
  }),
  isEditing: getParam('isEditing', false)
})

class RegisterGenreInterestScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  state = {
    currentInterestData: getStringArrayFromObjectArray(this.props.userData.interest, 'interest_name')
  }

  _onRegisterGenreInterest = ({ formData }) => {
    const {
      dispatch,
      navigateTo,
      userData: { id_user },
      navigateBack,
      isEditing,
      refreshProfile,
      getUserDetailDispatcher
    } = this.props
    if (
      isEmpty(formData.interest_name)
    ) alertMessage(null, 'Please fill the form correctly')
    else {
      formData.id_user = id_user
      dispatch(postInterest, formData)
        .then((dataResponse) => {
          getUserDetailDispatcher({ id_user })
          refreshProfile()
          isEditing ? navigateBack() : navigateTo('AppNavigator')
        })
    }
  }

  render() {
    const {
      currentInterestData
    } = this.state
    const {
      initialLoaded,
      isLoading,
      navigateTo,
      navigateBack,
      isEditing
    } = this.props

    return (
      <Container isLoading={isLoading} colors={[ORANGE_BRIGHT_DARK, PINK_RED_DARK]} theme='light'>
        <Header>
          <Icon
            onPress={() => initialLoaded && !isEditing ? {} : navigateBack()} size='massive'
            color={initialLoaded && !isEditing ? NO_COLOR : WHITE} name={isEditing ? 'close' : 'left'}
          />
        </Header>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='handled'
          contentContainerStyle={{ flex: 1 }}
        >

          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null} style={{ marginHorizontal: WP5, flex: 1 }}
          >
            <Text centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>Genres & Interest</Text>
            <Text style={{ marginVertical: HP2 }} centered color={WHITE}>You almost done! Please select at least 3
              genres so we can display content according to your choice</Text>
            <Form initialValue={{ interest_name: currentInterestData }} onSubmit={this._onRegisterGenreInterest}>
              {({ onChange, onSubmit, formData, isDirty }) => (
                <View>
                  <InputText
                    selection
                    selectionData={formData.interest_name}
                    suggestion={getGenreInterest}
                    suggestionKey='interest_name'
                    suggestionPathResult='interest_name'
                    label={null}
                    onChangeText={onChange('interest_name', true)}
                    returnKeyType='next'
                    placeholder='Search genre here…'
                  />
                  <Button
                    onPress={onSubmit}
                    style={[{ marginTop: HP5 }]}
                    rounded
                    disable={!isDirty}
                    centered
                    shadow='none'
                    backgroundColor={WHITE20}
                    textColor={WHITE}
                    text={isEditing ? 'Save' : 'Next'}
                  />
                  {
                    !isEditing && (
                      <Button
                        onPress={async () => {
                          await AsyncStorage.setItem(KEY_USER_LAST_SKIPPED_STEP, 'finish')
                          navigateTo('AppNavigator')
                        }}
                        style={{ alignSelf: 'center', marginVertical: 0 }}
                        rounded
                        centered
                        shadow='none'
                        backgroundColor={NO_COLOR}
                        textColor={WHITE}
                        textSize='small'
                        textWeight={300}
                        text='skip'
                      />
                    )
                  }
                </View>
              )}
            </Form>
          </KeyboardAvoidingView>
        </ScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(RegisterGenreInterestScreen),
  mapFromNavigationParam
)
