import React, { createRef } from "react";
import { RefreshControl, View } from "react-native";
import { noop, get } from "lodash-es";
import { connect } from "react-redux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { _enhancedNavigation, Container, FeedItem } from "../components";
import { WHITE } from "../constants/Colors";
import { deleteJourney, getFeedDetail } from "../actions/api";
import HeaderNormal from "../components/HeaderNormal";
import CommentSection from "../components/comment/CommentSection";
import { WP5 } from "../constants/Sizes";

const mapStateToProps = ({ auth, song: { playback, soundObject } }) => ({
  userData: auth.user,
  playback,
  soundObject,
});

const mapFromNavigationParam = (getParam) => ({
  idTimeline: getParam("idTimeline", 0),
  onRefresh: getParam("onRefresh", () => {}),
  onUpdateFeed: getParam("onUpdateFeed", () => {}),
  onPressTopic: getParam("onPressTopic", () => {}),
  onCommentCount: getParam("onCommentCount", () => {}),
  is1000Startup: getParam("is1000Startup", false),
});

class FeedDetailScreen extends React.Component {
  static navigationOptions = () => ({
    gesturesEnabled: true,
  });

  state = {
    feed: {
      type: "post",
    },
    isReady: false,
    isRefreshing: false,
    bottomReached: false,
  };

  scrollRef = createRef();
  _commentOffset = 0;

  async componentDidMount() {
    await this._getInitialScreenData();
  }

  componentWillUnmount() {
    const { soundObject, playback } = this.props;
    playback.isLoaded && soundObject.pauseAsync();
  }

  _getInitialScreenData = async (...args) => {
    await this._getFeed(...args);
    await this.setState({
      isReady: true,
      isRefreshing: false,
    });
  };

  _getFeed = async () => {
    const {
      dispatch,
      idTimeline,
      userData: { id_user },
      onCommentCount,
    } = this.props;
    const dataResponse = await dispatch(
      getFeedDetail,
      { id_timeline: idTimeline, id_user },
      noop,
      false,
      false
    );
    dataResponse.id_timeline = idTimeline;
    onCommentCount(get(dataResponse, "total_comment"));
    await this.setState({ feed: dataResponse });
  };

  _onDeletePost = (idJourney) => {
    const { navigateBack, onRefresh, dispatch } = this.props;
    dispatch(deleteJourney, idJourney).then(() => {
      onRefresh();
      navigateBack();
      // navigateTo('TimelineScreen')
    });
  };

  _onSubmitComment = (isReply) => {
    !isReply &&
      setTimeout(() => this.scrollRef.scrollToEnd({ animated: true }), 100);
  };

  _onFocusCommentInput = () => {
    this.scrollRef.scrollTo({
      x: 0,
      y: this._commentOffset,
      animated: true,
    });
  };

  _headerTitle = () => {
    // const {
    //   userData: { id_user },
    // } = this.props
    const { feed } = this.state;
    if (feed.id_user) {
      return `${feed.full_name.trim()}'s Post`;
      // return feed.id_user == id_user ? 'My Post' : `${feed.full_name}'s Post`
    }
    return "Detail Post";
  };

  _onPressTopic = (topic) => {};

  render() {
    const {
      isLoading,
      userData,
      dispatch,
      navigateBack,
      idTimeline,
      is1000Startup,
    } = this.props;
    const { feed, comments, isReady, isRefreshing } = this.state;
    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={() => navigateBack()}
            text={this._headerTitle()}
            centered
          />
        )}
      >
        <View>
          <KeyboardAwareScrollView
            ref={(r) => {
              this.scrollRef = r;
            }}
            keyboardShouldPersistTaps="handled"
            showsVerticalScrollIndicator={false}
            // onScroll={(e) => {
            //   let paddingToBottom = 10
            //   paddingToBottom += e.nativeEvent.layoutMeasurement.height
            //   if (
            //     e.nativeEvent.contentOffset.y >=
            //     e.nativeEvent.contentSize.height - paddingToBottom
            //   ) {
            //     this.setState({ bottomReached: true })
            //   }
            // }}
            contentContainerStyle={{ backgroundColor: WHITE, flexGrow: 1 }}
            refreshControl={
              <RefreshControl
                refreshing={isRefreshing}
                onRefresh={() => {
                  this.setState({ isRefreshing: true }, () => {
                    this._getInitialScreenData(null, null, false);
                  });
                }}
              />
            }
          >
            <View style={{ paddingBottom: WP5 }}>
              <FeedItem
                {...this.props}
                onRefresh={this._getInitialScreenData}
                feed={feed}
                fullText
                navigation={this.props.navigation}
                onPressTopic={this._onPressTopic}
                isLoading={!isReady}
                isFromDetail={true}
                ownFeed={feed.id_user === userData.id_user}
                onDeleteFeed={this._onDeletePost}
              />
              <View
                onLayout={({ nativeEvent: { layout } }) =>
                  (this._commentOffset = layout.y)
                }
              >
                <CommentSection
                  bottomReached={this.state.bottomReached}
                  releaseBottomReachedState={() =>
                    this.setState({ bottomReached: false })
                  }
                  navigateTo={this.props.navigateTo}
                  userData={userData}
                  dispatch={dispatch}
                  comments={comments}
                  commentCount={feed.total_comment}
                  feedId={idTimeline}
                  onDeleteComment={this._getFeed}
                  onGetFeed={this._getFeed}
                  totalComment={get(feed, "total_comment") || 0}
                  // onFocusCommentInput={this._onFocusCommentInput}
                  onSubmitComment={this._onSubmitComment}
                />
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(FeedDetailScreen),
  mapFromNavigationParam
);
