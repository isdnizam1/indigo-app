import React from 'react'
import { View, StyleSheet } from 'react-native'
import moment from 'moment'
import SkeletonContent from 'react-native-skeleton-content'
import { upperFirst } from 'lodash-es'
import {
  WP1,
  WP105,
  WP2,
  WP35,
  WP6,
  WP3,
  WP80,
  WP4,
  WP100,
  WP12,
  HP1,
  WP30,
  WP50,
  WP5,
  WP15,
  WP40,
} from '../../constants/Sizes'
import Image from '../../components/Image'
import { SHADOW_STYLE } from '../../constants/Styles'
import Text from '../../components/Text'
import {
  GREY,
  TOMATO,
  WHITE,
  WHITE_MILK,
  GREY_WARM,
  TEAL,
  SILVER_CALMER,
  TOMATO_CALM50,
  TOMATO_CALM,
  GUN_METAL,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  PALE_GREY,
  REDDISH,
} from '../../constants/Colors'
import { speakerDisplayName } from '../../utils/transformation'
import { Icon } from '../../components'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingTop: WP4,
    borderBottomColor: PALE_GREY,
  },
  imageWrapper: {
    width: WP30,
    height: WP30,
    borderRadius: 6,
    ...SHADOW_STYLE['shadowThin'],
  },
  buttonActive: {
    backgroundColor: REDDISH,
    paddingHorizontal: WP4,
    paddingVertical: WP1,
    borderRadius: 6,
  },
  buttonInactive: {
    backgroundColor: PALE_GREY,
    paddingHorizontal: WP2,
    paddingVertical: WP1,
    borderRadius: 6,
  },
})

const SessionItemV2 = ({ ads, loading, line }) => {
  const title = loading ? 'Judul Soundfren Connect' : ads.title,
    speaker = loading ? 'Pembicara' : speakerDisplayName(ads.speaker),
    isActive = ads?.status == 'active',
    isJoined = ads?.user_joined,
    isAvailable = ads?.available_seats > 0,
    isPodcast = ads?.available_podcast

  return (
    <View
      style={[
        styles.container,
        { paddingBottom: line ? WP4 : 0, borderBottomWidth: line ? 1 : 0 },
      ]}
    >
      <View style={{ marginRight: WP4 }}>
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[styles.imageWrapper]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <Image
            source={{ uri: ads.image }}
            style={{
              borderRadius: 6,
              backgroundColor: WHITE,
              ...SHADOW_STYLE['shadowThin'],
            }}
            imageStyle={{ width: WP30, aspectRatio: 1, borderRadius: 6 }}
          />
        </SkeletonContent>
      </View>
      <SkeletonContent
        containerStyle={{ flex: 1 }}
        layout={[
          { width: WP50, height: WP5, marginBottom: WP105 },
          { width: WP35, height: WP5, marginBottom: WP3 },
          { width: WP40, height: WP4, marginBottom: WP3 },
          { width: WP15, height: WP6, borderRadius: 6 },
        ]}
        isLoading={loading}
        boneColor={SKELETON_COLOR}
        highlightColor={SKELETON_HIGHLIGHT}
      >
        <View>
          <Text
            numberOfLines={2}
            type='Circular'
            color={GUN_METAL}
            size='mini'
            weight={500}
            style={{ lineHeight: WP6, marginBottom: WP1 }}
          >
            {upperFirst(title)}
          </Text>
          <Text
            numberOfLines={1}
            type='Circular'
            color={SHIP_GREY_CALM}
            size='xmini'
            weight={300}
            style={{ marginBottom: WP2 }}
          >
            {upperFirst(speaker)}
          </Text>
          <View style={{ flexWrap: 'wrap' }}>
            {isActive ? (
              !isJoined && isAvailable ? (
                <View style={styles.buttonActive}>
                  <Text type='Circular' color={WHITE} size='xmini' weight={400}>
                    Join
                  </Text>
                </View>
              ) : !isJoined && !isAvailable ? (
                <View style={styles.buttonInactive}>
                  <Text
                    type='Circular'
                    color={SHIP_GREY_CALM}
                    size='xmini'
                    weight={400}
                  >
                    Sold Out
                  </Text>
                </View>
              ) : null
            ) : isPodcast ? (
              <View style={styles.buttonInactive}>
                <Text
                  type='Circular'
                  color={SHIP_GREY_CALM}
                  size='xmini'
                  weight={400}
                >
                  Podcast tersedia
                </Text>
              </View>
            ) : null}
          </View>
        </View>
      </SkeletonContent>
    </View>
  )
}

const SessionItem = ({
  ads,
  loading = true,
  horizontal = false,
  showButton = true,
  isSelected = false,
  new_version = false,
  line = false,
}) => {
  const title = loading ? 'Soundfren Connect Title' : ads.title,
    speaker = loading ? 'Speaker Name' : speakerDisplayName(ads.speaker),
    seats = loading ? '0 seats' : `${ads.available_seats} seats`
  if (new_version) return SessionItemV2({ ads, loading, line })
  return (
    <View
      style={{
        width: horizontal ? WP80 : WP100 - WP12,
        marginRight: horizontal ? WP6 : 0,
        marginLeft: 1,
        marginVertical: horizontal ? 0 : HP1,
        borderRadius: 12,
        backgroundColor: WHITE,
        ...SHADOW_STYLE.shadowThin,
      }}
    >
      <View
        style={{
          borderRadius: 12,
          overflow: 'hidden',
        }}
      >
        <View
          style={{
            backgroundColor: WHITE_MILK,
            height: WP35,
          }}
        >
          {!loading && (
            <View>
              <Image
                source={{ uri: ads.image }}
                imageStyle={{ height: WP35, aspectRatio: 2.5 }}
              />
              {moment().diff(moment(ads.updated_at), 'days') <= 3 && (
                <Image
                  style={{
                    height: WP2,
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    marginVertical: WP1,
                  }}
                  aspectRatio={48 / 16}
                  source={require('../../assets/images/labelNew3.png')}
                />
              )}
              {ads.available_seats == 0 && (
                <View
                  style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    paddingHorizontal: WP4,
                    paddingVertical: WP1,
                    borderTopRightRadius: WP100,
                    backgroundColor: TOMATO,
                  }}
                >
                  <Text
                    color={WHITE}
                    type='NeoSans'
                    ellipsizeMode='tail'
                    size='tiny'
                    weight={500}
                  >
                    Full booked
                  </Text>
                </View>
              )}
            </View>
          )}
        </View>
        <View
          style={{ paddingHorizontal: WP3, paddingTop: WP105, paddingBottom: WP3 }}
        >
          <Text
            type='NeoSans'
            color={GREY}
            numberOfLines={2}
            ellipsizeMode='tail'
            size='xmini'
            weight={500}
            style={{ marginBottom: WP1 }}
          >
            {title}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'flex-end',
            }}
          >
            <View>
              <Text
                color={GREY}
                numberOfLines={1}
                ellipsizeMode='tail'
                size='tiny'
                style={{ marginBottom: WP1 }}
              >
                {speaker}
              </Text>
              <Text
                color={ads.available_seats <= 3 || loading ? TOMATO : TEAL}
                numberOfLines={1}
                ellipsizeMode='tail'
                size='tiny'
              >
                <Text
                  color={ads.available_seats <= 3 || loading ? TOMATO : TEAL}
                  numberOfLines={1}
                  ellipsizeMode='tail'
                  size='tiny'
                  weight={500}
                >
                  {seats}
                </Text>{' '}
                remaining
              </Text>
            </View>
            {showButton && (
              <View
                style={{
                  backgroundColor: loading ? SILVER_CALMER : TOMATO,
                  paddingHorizontal: WP4,
                  paddingVertical: WP105,
                  borderRadius: 12,
                }}
              >
                <Text
                  type='NeoSans'
                  color={loading ? GREY_WARM : WHITE}
                  numberOfLines={2}
                  ellipsizeMode='tail'
                  size='tiny'
                  weight={500}
                >
                  Join
                </Text>
              </View>
            )}
          </View>
        </View>
      </View>
      {isSelected && (
        <View
          style={{
            flex: 1,
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            zIndex: 2,
            width: WP100 - WP12,
          }}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: TOMATO_CALM50,
              borderRadius: 12,
              alignItems: 'flex-end',
              paddingHorizontal: WP4,
              paddingVertical: WP3,
            }}
          >
            <View
              style={{
                width: WP6,
                height: WP6,
                borderRadius: WP6 / 2,
                backgroundColor: TOMATO_CALM,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Icon
                centered
                type='FontAwesome'
                name='check'
                color={WHITE}
                size='mini'
              />
            </View>
          </View>
        </View>
      )}
    </View>
  )
}

SessionItem.propTypes = {}

SessionItem.defaultProps = {}

export default SessionItem
