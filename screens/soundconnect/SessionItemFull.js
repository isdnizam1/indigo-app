import React from 'react'
import { View } from 'react-native'
import moment from 'moment'
import { isEmpty } from 'lodash-es'
import { HP2, WP100, WP2, WP20, WP4, WP6 } from '../../constants/Sizes'
import Image from '../../components/Image'
import Text from '../../components/Text'
import { NAVY_DARK, PALE_BLUE_TWO, PALE_GREY, REDDISH, SHIP_GREY, SHIP_GREY_CALM } from '../../constants/Colors'
import { speakerDisplayName } from '../../utils/transformation'
import { Icon } from '../../components'
import HorizontalLine from '../../components/HorizontalLine'
import { toPriceFormat } from '../../utils/helper'

const SessionItem = ({ ads, loading = true, voucher }) => {
  const title = loading ? 'Soundfren Connect Title' : ads.title,
    speaker = loading ? 'Speaker Name' : speakerDisplayName(ads.speaker)

  const isDiscount = ads.additional_data.price.discount !== '0%' || isEmpty(ads.additional_data.price.discount)
  let discountPrice = Number(ads.additional_data.price.discount_price)
  let voucherDiscount
  let isVoucher
  if (voucher) {
    isVoucher = true
    const discountRate = Number(voucher.amount_discount)
    discountPrice = (100 - discountRate) / 100 * discountPrice
    voucherDiscount = ads.additional_data.price.discount_price - discountPrice
  }
  const _roundDiscountValue = (value) => {
    if (value.length > 3) {
      return value.split('.')[0] + value.charAt(value.length - 1)
    }
    return value
  }

  let finalPrice = discountPrice === 0 ?
    'FREE ACCESS' :
    `${ads.additional_data.price.currency} ${(discountPrice).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}`

  return (
    <View>
      <View style={{
        flexDirection: 'row',
        width: WP100 - WP6
      }}
      >
        <Image
          source={{ uri: ads.image }}
          imageStyle={{ width: WP20, aspectRatio: 1, borderRadius: WP2 }}
        />
        <View style={{ flex: 1, paddingHorizontal: WP4, justifyContent: 'space-between' }}>
          <Text type='Circular' color={NAVY_DARK} numberOfLines={2} ellipsizeMode='tail' size='small' weight={500}>{title}</Text>
          <Text type='Circular' color={SHIP_GREY_CALM} numberOfLines={1} ellipsizeMode='tail' size='mini'>{speaker}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Icon name='calendar' color={SHIP_GREY_CALM} />
            <Text type='Circular' color={SHIP_GREY_CALM} size={'tiny'}> {moment(ads.additional_data.date.start).format('dddd, DD MMM YYYY | HH:mm')} WIB</Text>
          </View>
        </View>
      </View>
      <HorizontalLine color={PALE_GREY} style={{ marginVertical: HP2, alignSelf: 'stretch', width: WP100 - WP6 }} />
      <View>
        {
          isDiscount && (
            <View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text type='Circular' color={SHIP_GREY_CALM} numberOfLines={1} ellipsizeMode='tail' size='mini'>1 tiket (diskon {_roundDiscountValue(ads.additional_data.price.discount)})</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text type='Circular' weight={'400'} color={PALE_BLUE_TWO} style={{ textDecorationLine: 'line-through' }} size={'mini'}>
                    {ads.additional_data.price.currency} {(ads.additional_data.price.normal_price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                  </Text>
                  <Text type='Circular' color={SHIP_GREY_CALM} style={{ marginLeft: WP2 }} size={'mini'}>
                    {toPriceFormat(ads.additional_data.price.discount_price)}
                  </Text>
                </View>
              </View>
              <HorizontalLine color={PALE_GREY} style={{ marginVertical: HP2, alignSelf: 'stretch', width: WP100 - WP6 }} />
            </View>)
        }
      </View>
      <View>
        {
          isVoucher && (
            <View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text type='Circular' color={SHIP_GREY_CALM} numberOfLines={1} ellipsizeMode='tail' size='mini'>Voucher</Text>
                <Text type='Circular' color={SHIP_GREY_CALM} numberOfLines={1} ellipsizeMode='tail' size='mini'>-{voucherDiscount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')}</Text>
              </View>
              <HorizontalLine color={PALE_GREY} style={{ marginVertical: HP2, alignSelf: 'stretch', width: WP100 - WP6 }} />
            </View>)
        }
      </View>
      <View>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
          <Text type='Circular' color={SHIP_GREY} weight={500} numberOfLines={1} ellipsizeMode='tail' size='slight'>Total pembayaran</Text>
          <Text type='Circular' color={REDDISH} weight={600} numberOfLines={1} ellipsizeMode='tail' size='medium'>{finalPrice}</Text>
        </View>
      </View>
    </View>
  )
}

SessionItem.propTypes = {}

SessionItem.defaultProps = {}

export default SessionItem
