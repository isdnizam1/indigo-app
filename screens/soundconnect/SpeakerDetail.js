import React, { Component } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { View, Image, TouchableOpacity } from 'react-native'
import HTMLElement from 'react-native-render-html'
import { noop } from 'lodash-es'
import { ScrollView } from 'react-native-gesture-handler'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { getAdsDetail } from '../../actions/api'
import { getYoutbeId } from '../../utils/helper'
import {
  Container,
  Text,
  HeaderNormal
} from '../../components'
import {
  WP301,
  WP100,
  WP50,
  WP6,
  HP2
} from '../../constants/Sizes'
import {
  HP1 } from '../../constants/Sizes'
import {
  SHIP_GREY_CALM, SHIP_GREY, NAVY_DARK
} from '../../constants/Colors'
import HorizontalLine from '../../components/HorizontalLine'
import Video from '../../components/Video'
import SessionItem from './SessionItem'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam('id_ads', null)
})

class SpeakerDetail extends Component {

  state = {
    isLoading: true,
    isReady: false,
    speaker: null,
    activeSlider: 0
  }

  async componentDidMount() {
    let { id_ads, dispatch, userData: { id_user } } = this.props
    let { isLoading } = this.state
    let speaker = await dispatch(getAdsDetail, { id_ads, id_user }, noop, false, isLoading)
    speaker.additional_data = JSON.parse(speaker.additional_data)
    this.setState({ speaker, isLoading: false, isReady: true })
  }

  onLinkPress = (event, href) => this.props.navigateTo('BrowserScreenNoTab', { url: href })

  render() {
    const { navigation, navigateBack } = this.props
    const { isLoading, isReady, speaker } = this.state
    return (
      <Container
        isLoading={isLoading}
        isReady={isReady}
        scrollable
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text={'Profil Pembicara'}
            textType='Circular'
            textWeight={400}
            centered
          />
        )}
      >
        {speaker && (<View>
          <Image resizeMode={'contain'} style={{ width: WP100, height: WP50 }} source={{ uri: speaker.image }} />
          <View style={{ padding: WP6 }}>
            <Text textType='Circular' weight={'500'} size={'large'} color={NAVY_DARK}>{speaker.title}</Text>
            <Text textType='Circular' size={'tiny'} style={{ marginVertical: HP1 }} color={SHIP_GREY_CALM}>{speaker.additional_data.profession}</Text>
            <HorizontalLine style={{ marginLeft: -WP6, marginVertical: HP1 }} />
            {speaker.data_session && speaker.data_session.length > 0 ? <View style={{ marginTop: HP2 }}>
              <Text textType='Circular' style={{ marginBottom: HP1 }} weight={'500'} size={'tiny'} color={SHIP_GREY}>Sesi bersama {speaker.title}:</Text>
              {speaker.data_session.map((ads) => {
                return (<TouchableOpacity onPress={() => navigation.push('SessionDetail', { id_ads: ads.id_ads, key: Math.random() })} activeOpacity={0.8} style={{ marginBottom: HP1 }} key={Math.random()}>
                  <SessionItem new_version={true} ads={ads} loading={false} />
                </TouchableOpacity>)
              })}
            </View> : null}
            <HorizontalLine style={{ marginLeft: -WP6, marginVertical: HP1 }} />
            <View style={{ marginTop: HP2 }}>
              <Text textType='Circular' style={{ marginBottom: HP1 }} weight={'500'} size={'tiny'} color={SHIP_GREY}>Tentang {speaker.title}:</Text>
              <HTMLElement
                textSelectable
                html={speaker.description.trim().trim()}
                onLinkPress={this.onLinkPress.bind(this)}
                baseFontStyle={{ fontSize: WP301, fontFamily: 'CircularBook', color: SHIP_GREY_CALM }}
              />
            </View>
            <HorizontalLine style={{ marginLeft: -WP6, marginVertical: HP2 }} />
            {speaker.additional_data.works && speaker.additional_data.works.length > 0 ? <View style={{ marginTop: HP1 }}>
              <Text textType='Circular' style={{ marginBottom: HP1 }} weight={'500'} size={'tiny'} color={SHIP_GREY}>Karya {speaker.title}:</Text>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                <View style={{ flexDirection: 'row' }}>
                  {speaker.additional_data.works.map(({ link, title, created_at, total_view }, index) => {
                    let thumbnailUrl = `https://img.youtube.com/vi/${getYoutbeId(link)}/hqdefault.jpg`
                    return (<Video video={{ title, created_id: moment(created_at).format('DD MMM YYYY'), totalView: total_view, additional_data: { cover_image: thumbnailUrl, url_video: link } }} key={index} />)
                  })}
                </View>
              </ScrollView>
            </View> : null}
          </View>
        </View>)}
      </Container>
    )
  }

}

SpeakerDetail.propTypes = {}
SpeakerDetail.defaultProps = {}
export default _enhancedNavigation(
  connect(mapStateToProps, {})(SpeakerDetail),
  mapFromNavigationParam
)
