import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Image,
  Keyboard,
  View,
  TouchableOpacity,
  BackHandler,
  AppState,
} from "react-native";
import { noop, get, first, isEmpty } from "lodash-es";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import _enhancedNavigation from "sf-navigation/_enhancedNavigation";
import {
  getAdsDetail,
  getSubscriptionVoucher,
  postJoinSession,
  getVoucherUser,
} from "sf-actions/api";
import { isValidPhoneNumber, isEmailValid } from "sf-utils/helper";
import {
  BottomSheet,
  Button,
  Container,
  Icon,
  InputTextLight,
  Text,
  Modal,
  HeaderNormal,
  ModalMessageView,
} from "sf-components";
import {
  HP1,
  HP3,
  HP45,
  HP5,
  WP2,
  WP3,
  WP4,
  WP6,
  WP70,
  WP105,
  WP80,
  WP25,
  WP308,
  WP5,
  WP1,
  WP100,
  WP8,
  WP305,
  WP10,
} from "sf-constants/Sizes";
import {
  GREY,
  LIGHT_TEAL,
  TEAL,
  TOMATO,
  WHITE,
  LIGHT_PINK,
  RED_GOOGLE,
  TOMATO_SEMI,
  SHIP_GREY,
  SHIP_GREY_CALM,
  PALE_LIGHT_BLUE_TWO,
  SILVER_TWO,
  REDDISH,
  GUN_METAL,
  PALE_GREY,
  BLUE10,
} from "sf-constants/Colors";
import { voucherDispatcher } from "sf-services/voucher";
import { SHADOW_STYLE, TEXT_INPUT_STYLE } from "sf-constants/Styles";
import HorizontalLine from "sf-components/HorizontalLine";
import Spacer from "sf-components/Spacer";
import ButtonV2 from "sf-components/ButtonV2";
import moment from "moment";
import { showReviewIfNotYet } from "../../utils/review";
import { postScreenTimeAdsHelper } from "../../utils/helper";
import SessionItemFull from "./SessionItemFull";

const mapStateToProps = ({ auth, voucher }) => ({
  userData: auth.user,
  voucher: voucher.voucher,
});

const mapDispatchToProps = {
  voucherClear: voucherDispatcher.voucherClear,
};

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam("id_ads", null),
  isStartup: getParam("isStartup", false),
});

const dateFormat = "YYYY-MM-DD HH:mm:ss";

class SessionRegisterForm extends Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isReady: false,
      ads: null,
      activeSlider: 0,
      full_name: this.props.userData.full_name ?? "",
      whatsapp: "",
      email: this.props.userData.email ?? "",
      whatsapp_error: "",
      email_error: "",
      voucher_code: get(this.props.voucher, "voucher_code", ""),
      showVoucherStatus: false,
      isVoucherValid: false,
      voucher: this.props.voucher,
      showVoucherModal: false,
      backModal: false,
      buyModal: false,
      voucherData: [],
      viewFrom: null,
      appState: AppState.currentState,
    };
    this._didFocusSubscription = props.navigation.addListener("focus", () => {
      // console.log('isfocus')
      this.setState({ viewFrom: moment().format(dateFormat) });
      AppState.addEventListener("change", this._handleAppStateChange);
      BackHandler.addEventListener("hardwareBackPress", this._backHandler);
    });
  }

  async componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener(
      "blur",
      () => {
        // console.log('isBlur')
        this._postScreenTimeAds();
        AppState.removeEventListener("change", this._handleAppStateChange);
        BackHandler.removeEventListener("hardwareBackPress", this._backHandler);
      }
    );

    if (this.props.voucher) {
      this.setState({ showVoucherStatus: true, isVoucherValid: true });
    } else {
      this._getVouchers();
    }
    let {
      id_ads,
      dispatch,
      userData: { id_user },
    } = this.props;
    let { isLoading } = this.state;
    let ads = await dispatch(
      getAdsDetail,
      { id_ads, id_user },
      noop,
      false,
      isLoading
    );
    ads.additional_data = JSON.parse(ads.additional_data);
    this.setState({ ads, isLoading: false, isReady: true });
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription();
    this._willBlurSubscription && this._willBlurSubscription();
  }

  _handleAppStateChange = (nextAppState) => {
    // console.log(AppState.currentState)
    if (AppState.currentState === "background") {
      this._postScreenTimeAds();
    }
    if (
      (this.state.appState === "inactive" ||
        this.state.appState === "background") &&
      nextAppState === "active"
    ) {
      this.setState({ viewFrom: moment().format(dateFormat) });
      // console.log('back to foreground', this.state.viewFrom)
    }
    this.setState({ appState: nextAppState });
    // console.log({ appState: this.state.appState })
  };

  _postScreenTimeAds = () => {
    const { isLoading, viewFrom } = this.state;
    const {
      id_ads,
      userData: { id_user },
      isStartup,
      dispatch,
    } = this.props;
    // console.log({ id_ads, id_user, viewFrom })
    postScreenTimeAdsHelper({
      viewFrom,
      id_ads,
      id_user,
      dispatch,
      isLoading,
      isStartup,
    });
  };

  confirmAlert = React.createRef();
  inputWhatsApp = React.createRef();
  inputEmail = React.createRef();
  inputVoucher = React.createRef();

  _backHandler = async () => {
    const { backModal } = this.state;
    this.setState({ backModal: !backModal });
  };

  onWhatsappChange = (whatsapp) => {
    whatsapp && this.setState({ whatsapp: whatsapp.trim().replace(/\s/g, "") });
    if (whatsapp && isValidPhoneNumber(whatsapp))
      this.setState({ whatsapp_error: "" });
    else if (!whatsapp) this.setState({ whatsapp_error: "", whatsapp: "" });
    else this.setState({ whatsapp_error: "Enter valid whatsapp number" });
  };

  onEmailChange = (email) => {
    email && this.setState({ email: email.trim().replace(/\s/g, "") });
    if (email && isEmailValid(email)) this.setState({ email_error: "" });
    else if (!email) this.setState({ email_error: "", email: "" });
    else this.setState({ email_error: "Enter valid email address" });
  };

  joinSession() {
    let { whatsapp, whatsapp_error, email, email_error, ads } = this.state;
    if (whatsapp && !whatsapp_error && email && !email_error) {
      const onSubmit = this.onSubmitForm.bind(this);
      onSubmit();
    }
  }

  _roundDiscountValue(value) {
    if (value.length > 3) {
      return value.split(".")[0] + value.charAt(value.length - 1);
    }
    return value;
  }
  async onSubmitForm() {
    let {
      userData: { id_user },
      dispatch,
      navigation: { popToTop, navigate },
      voucherClear,
      navigateBack,
      navigateTo,
      isStartup,
    } = this.props;
    let { ads, whatsapp, voucher, email, full_name } = this.state;
    let discountPrice = Number(ads?.additional_data.price.discount_price);
    let id_ads = ads.id_ads;

    this.setState({ isLoading: true });
    try {
      let voucherUsed;
      if (voucher) {
        try {
          const { result } = await dispatch(
            getSubscriptionVoucher,
            {
              voucher_code: get(voucher, "voucher_code"),
              id_user,
              type: "session_ads",
            },
            noop,
            true
          );
          voucherUsed = result;
        } catch (e) {
          // keep silent
        }
      }
      const token_voucher = get(
        voucherUsed || voucher,
        "token_voucher",
        undefined
      );
      const feature = get(voucher, "feature", undefined);
      let body = {
        id_user,
        id_ads,
        whatsapp,
        token_voucher,
        full_name,
        feature,
        email,
      };
      if (
        discountPrice == 0 ||
        (voucher && voucherUsed?.amount_discount == 100)
      ) {
        // if (voucher && voucherUsed?.amount_discount == 100) {
        body = { ...body, payment_channel: null };
        let { payment_midtrans } = await dispatch(postJoinSession, body);
        if (!payment_midtrans) {
          voucherClear();
          this.setState({ isLoading: false });
          !isStartup &&
            Promise.all([
              popToTop(),
              navigate("NotificationStack", {
                screen: "NotificationScreen",
                params: { defaultTab: "activities", review: true },
              }),
            ]);
          isStartup &&
            Promise.all([
              navigateBack(),
              navigateBack(),
              navigateTo(
                "Notification1000StartupScreen",
                { isStartup: true },
                "push"
              ),
            ]);
        }
      } else {
        voucherClear();
        // console.log(ads,'xxxc')

        this.setState({ isLoading: false });
        this.props.navigateTo("PaymentMethodScreen", {
          payment_type: "join_session",
          body,
          isStartup,
        });
      }
    } catch (err) {
      this.setState({ isLoading: false });
    }
  }

  _onChangeVoucherCode = (value) => {
    this.setState({
      voucher_code: value,
      showVoucherStatus: false,
      voucher: undefined,
    });
  };

  _onChangeText = (key) => (value) => {
    this.setState({ [key]: value });
  };

  _onUseVoucher = async () => {
    const {
      userData: { id_user },
      dispatch,
    } = this.props;
    const { voucher_code } = this.state;
    Keyboard.dismiss();
    const body = {
      voucher_code,
      id_user,
      type: "session_ads",
    };
    if (voucher_code !== "") {
      try {
        const { result: voucher } = await dispatch(
          getSubscriptionVoucher,
          body,
          noop,
          true
        );

        if (voucher) {
          this.setState({
            showVoucherStatus: true,
            isVoucherValid: true,
            voucher,
          });
        } else {
          this.setState({
            showVoucherStatus: true,
            isVoucherValid: false,
            voucher: undefined,
          });
        }
      } catch (e) {
        this.setState({
          showVoucherStatus: true,
          isVoucherValid: false,
          voucher: undefined,
        });
      }
    }
  };

  _getVouchers = async () => {
    const { dispatch, userData } = this.props;

    try {
      const { result, code } = await dispatch(
        getVoucherUser,
        { id_user: userData.id_user },
        noop,
        true,
        true
      );
      if (code == 200) {
        this.setState({
          voucherData: result,
          showVoucherModal: true,
        });
      }
    } catch (error) {
      this._getVouchers();
    }
  };

  _onPressButtonModal = (type) => {
    if (type == "use") {
      this.setState(
        {
          voucher_code: first(this.state.voucherData).voucher_code,
          showVoucherModal: false,
        },
        () => {
          this._onUseVoucher();
          this.setState({
            voucherData: [],
            showVoucherModal: false,
          });
        }
      );
    } else {
      this.setState({ showVoucherModal: false, voucherData: [] });
    }
  };

  _customLabel = (title, subtitle, required = false) => {
    return (
      <Text weight={"400"} type="Circular" size={"mini"} color={SHIP_GREY}>
        {title}
        {subtitle && (
          <Text
            weight={"300"}
            type="Circular"
            size={"mini"}
            color={PALE_LIGHT_BLUE_TWO}
          >
            {" "}
            {subtitle}
          </Text>
        )}
        {required && (
          <Text weight={"500"} type="Circular" size={"mini"} color={TOMATO}>
            {" "}
            *
          </Text>
        )}
      </Text>
    );
  };

  render() {
    const { navigateBack } = this.props;
    const {
      ads,
      isLoading,
      isReady,
      whatsapp,
      whatsapp_error,
      email,
      email_error,
      voucher_code,
      showVoucherStatus,
      isVoucherValid,
      voucher,
      full_name,
      showVoucherModal,
      backModal,
      buyModal,
    } = this.state;
    let onSubmit = this.onSubmitForm.bind(this);

    // const isDiscount = ads?.additional_data.price.discount_price
    let discountPrice = Number(ads?.additional_data.price.discount_price);
    let normalPrice = Number(ads?.additional_data.price.normal_price);
    let discountPercent = ads?.additional_data.price.discount;
    if (voucher) {
      const discountRate = Number(voucher.amount_discount);
      discountPrice = ((100 - discountRate) / 100) * discountPrice;
      discountPercent = `${
        ((normalPrice - discountPrice) / normalPrice) * 100
      }%`;
    }

    return (
      <Container
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._backHandler}
            text={"Registrasi"}
            textType="Circular"
            textWeight={400}
            centered
          />
        )}
      >
        <View style={{ flex: 1, flexGrow: 1 }}>
          {ads && (
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
              <View style={{ paddingBottom: HP3 }}>
                <View
                  style={{
                    alignItems: "center",
                    marginVertical: HP3,
                    marginHorizontal: WP3,
                  }}
                >
                  <SessionItemFull
                    withPrice
                    voucher={voucher}
                    ads={ads}
                    loading={false}
                  />
                </View>

                <View style={{ paddingHorizontal: WP3 }}>
                  <HorizontalLine
                    style={{ marginLeft: -WP3, marginVertical: HP1 }}
                  />
                  <View style={{ marginTop: WP4 }}>
                    {this._customLabel("Nama Lengkap", "(untuk sertifikat)")}
                    <InputTextLight
                      editable={false}
                      bordered
                      size="mini"
                      maxLength={22}
                      returnKeyType={"next"}
                      placeholder="Tulis nama lengkapmu"
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      value={full_name}
                      wording=" "
                      onChangeText={this._onChangeText("full_name")}
                      textInputStyle={TEXT_INPUT_STYLE["inputV2"]}
                      onSubmit={() => this.inputWhatsApp.current.focus()}
                      lineHeight={1}
                      multiline
                      containerStyle={{
                        height: WP308 + WP2,
                      }}
                    />
                  </View>

                  <View style={{ marginTop: HP1 }}>
                    {this._customLabel("Nomor Whatsapp")}
                    <InputTextLight
                      innerRef={this.inputWhatsApp}
                      bordered
                      size="mini"
                      returnKeyType={"done"}
                      keyboardType={"numeric"}
                      placeholder="Tulis nomor Whatsapp"
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      value={whatsapp}
                      onChangeText={this.onWhatsappChange.bind(this)}
                      textInputStyle={TEXT_INPUT_STYLE["inputV2"]}
                      error={whatsapp_error}
                      onSubmit={() => this.inputVoucher.current.focus()}
                      containerStyle={{
                        height: WP308 + WP2,
                      }}
                    />
                  </View>

                  <View style={{ marginTop: HP1 }}>
                    {this._customLabel("Email")}
                    <InputTextLight
                      innerRef={this.inputEmail}
                      bordered
                      size="mini"
                      returnKeyType={"next"}
                      placeholder="Tulis emailmu"
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      value={email}
                      onChangeText={this.onEmailChange.bind(this)}
                      textInputStyle={TEXT_INPUT_STYLE["inputV2"]}
                      error={email_error}
                      onSubmit={() => this.inputEmail.current.focus()}
                      containerStyle={{
                        height: WP308 + WP2,
                      }}
                    />
                  </View>

                  <HorizontalLine
                    style={{ marginLeft: -WP3, marginVertical: HP1 }}
                  />

                  <View style={{ marginTop: WP4 }}>
                    {this._customLabel("Kode Voucher", "(optional)", false)}
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <InputTextLight
                        autoCapitalize={"characters"}
                        innerRef={this.inputVoucher}
                        bordered
                        size="mini"
                        placeholder="Tulis kode vouchermu"
                        placeholderTextColor={SILVER_TWO}
                        color={SHIP_GREY}
                        value={voucher_code}
                        onChangeText={this._onChangeVoucherCode}
                        textInputStyle={TEXT_INPUT_STYLE["inputV2"]}
                        iconRightCustom={
                          !voucher &&
                          showVoucherStatus && (
                            <Text
                              onPress={() => this._onChangeVoucherCode("")}
                              weight={"500"}
                              type="Circular"
                              size={"xmini"}
                            >
                              Remove
                            </Text>
                          )
                        }
                        containerStyle={{
                          height: WP308 + WP2,
                        }}
                        onSubmit={this._onUseVoucher}
                        disabled={!voucher}
                        editable={!voucher}
                      />
                      <View>
                        {!showVoucherStatus && (
                          <ButtonV2
                            disabled={!voucher_code}
                            onPress={this._onUseVoucher}
                            shadow="none"
                            backgroundColor={WHITE}
                            centered
                            radius={WP2}
                            textType="Circular"
                            textSize="mini"
                            textColor={REDDISH}
                            textWeight={500}
                            text="Pakai"
                            borderless
                            style={{
                              borderWidth: 1,
                              borderColor: REDDISH,
                              borderRadius: WP2,
                              height: WP10 - 1,
                              paddingVertical: 0,
                              marginLeft: WP2,
                              paddingHorizontal: WP5,
                            }}
                          />
                        )}
                      </View>
                    </View>
                    {showVoucherStatus && (
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                          backgroundColor: isVoucherValid
                            ? LIGHT_TEAL
                            : LIGHT_PINK,
                          borderRadius: 6,
                          paddingHorizontal: WP3,
                          paddingVertical: WP105,
                        }}
                      >
                        <Text
                          type="Circular"
                          weight={500}
                          size="tiny"
                          color={isVoucherValid ? TEAL : RED_GOOGLE}
                        >
                          {isVoucherValid
                            ? "Your voucher is valid"
                            : "Your voucher is invalid"}
                        </Text>
                        <Icon
                          size="mini"
                          name={isVoucherValid ? "checkcircle" : "closecircle"}
                          color={isVoucherValid ? TEAL : RED_GOOGLE}
                        />
                      </View>
                    )}
                  </View>
                </View>
              </View>
            </KeyboardAwareScrollView>
          )}
          {ads ? (
            <View
              style={[
                {
                  paddingHorizontal: WP6,
                  backgroundColor: WHITE,
                  paddingVertical: HP1,
                },
                !buyModal ? SHADOW_STYLE["shadowThin"] : null,
              ]}
            >
              <View style={{ paddingBottom: WP2, paddingTop: WP2 }}>
                <Button
                  colors={[REDDISH, REDDISH]}
                  compact="center"
                  radius={6}
                  textStyle={{
                    marginVertical: 10,
                    fontWeight: "500",
                    textTransform: "none",
                  }}
                  marginless
                  disable={!whatsapp || whatsapp_error || !full_name}
                  disableColor={TOMATO_SEMI}
                  toggle
                  autoFocus
                  toggleActive={false}
                  toggleInactiveTextColor={WHITE}
                  toggleInactiveColor={
                    whatsapp &&
                    !whatsapp_error &&
                    email &&
                    !email_error &&
                    full_name
                      ? REDDISH
                      : BLUE10
                  }
                  text={
                    this.props.voucher
                      ? "Ikuti Sesi"
                      : discountPercent && discountPercent == "100%"
                      ? "Daftar Sesi Ini"
                      : "Pilih Metode Pembayaran"
                  }
                  textSize="medium"
                  textType="Circular"
                  centered
                  onPress={this.joinSession.bind(this)}
                  shadow="none"
                />
                {!isEmpty(ads?.additional_data?.price?.normal_price) &&
                  ads?.additional_data?.price?.normal_price !=
                    discountPrice && (
                    <Text
                      centered
                      type="Circular"
                      size="tiny"
                      color={SHIP_GREY}
                      style={{ margin: WP2, marginTop: WP3 }}
                    >
                      Kamu hemat
                      <Text
                        type="Circular"
                        size="tiny"
                        color={SHIP_GREY}
                        weight={500}
                      >{` ${ads.additional_data.price.currency} ${(
                        ads.additional_data.price.normal_price - discountPrice
                      )
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}`}</Text>
                      {discountPercent
                        ? ` (${this._roundDiscountValue(discountPercent)})`
                        : ""}{" "}
                      dari pembelian ini
                    </Text>
                  )}
                {!!this.props.voucher && (
                  <View>
                    <Spacer size={WP2} />
                    <Text
                      color={SHIP_GREY}
                      centered
                      type={"Circular"}
                      size={"xmini"}
                    >
                      Voucher kamu telah aktif untuk pendaftaran sesi ini
                    </Text>
                  </View>
                )}
              </View>
            </View>
          ) : null}
          <ModalMessageView
            style={{ width: WP100 - WP8 }}
            contentStyle={{ paddingHorizontal: WP4 }}
            toggleModal={() => this.setState({ backModal: false })}
            isVisible={backModal}
            title={"Daftar Sesi"}
            titleType="Circular"
            titleSize={"small"}
            titleColor={GUN_METAL}
            titleStyle={{ marginBottom: WP4 }}
            subtitle={
              "Proses pendaftaran kamu belum selesai.\nLanjutkan pendaftaran?"
            }
            subtitleType="Circular"
            subtitleSize={"xmini"}
            subtitleWeight={400}
            subtitleColor={SHIP_GREY_CALM}
            subtitleStyle={{ marginBottom: WP3 }}
            image={null}
            buttonPrimaryText={"Lanjutkan"}
            buttonPrimaryContentStyle={{
              borderRadius: 8,
              paddingVertical: WP305,
            }}
            buttonPrimaryTextType="Circular"
            buttonPrimaryTextWeight={400}
            buttonPrimaryBgColor={REDDISH}
            buttonPrimaryAction={() => {
              this.setState({ backModal: false });
            }}
            buttonSecondaryText={"Batalkan"}
            buttonSecondaryStyle={{
              backgroundColor: PALE_GREY,
              marginTop: WP1,
              borderRadius: 8,
              paddingVertical: WP305,
            }}
            buttonSecondaryTextType="Circular"
            buttonSecondaryTextWeight={400}
            buttonSecondaryTextColor={SHIP_GREY_CALM}
            buttonSecondaryAction={() => navigateBack()}
          />
          <BottomSheet
            backdropClose
            innerRef={this.confirmAlert}
            snapPoints={[HP45, -HP45]}
            onOpenStart={() => {
              this.setState({ buyModal: true });
            }}
            onOpenEnd={() => {
              this.setState({ buyModal: false });
            }}
            renderContent={({ closeBottomSheet }) => (
              <View
                style={{
                  padding: WP6,
                  alignItems: "center",
                  justifyContent: "center",
                  flex: 1,
                }}
              >
                <Image
                  source={require("sf-assets/images/areYouSure.png")}
                  imageStyle={{ width: WP70 }}
                  aspectRatio={250 / 125}
                />
                <Text
                  style={{ marginVertical: HP5 }}
                  centered
                  type="Circular"
                  weight={600}
                  size="medium"
                >
                  Your registration is almost done, continue to payment?
                </Text>
                <View style={{ flexDirection: "row" }}>
                  <Button
                    style={{ flex: 1, marginRight: WP3 }}
                    onPress={() => {
                      // ugly but don't delete
                      closeBottomSheet();
                      closeBottomSheet();
                    }}
                    backgroundColor={TOMATO}
                    toggle
                    toggleActiveColor={WHITE}
                    toggleInactiveTextColor={TOMATO}
                    centered
                    bottomButton
                    marginless
                    radius={10}
                    shadow="none"
                    textType="Circular"
                    textSize="small"
                    textColor={WHITE}
                    textWeight={500}
                    text={"Check again"}
                  />
                  <Button
                    style={{ flex: 1, marginLeft: WP3 }}
                    onPress={() => {
                      // ugly but don't delete
                      closeBottomSheet();
                      closeBottomSheet();
                      onSubmit();
                    }}
                    backgroundColor={TOMATO}
                    centered
                    bottomButton
                    marginless
                    radius={10}
                    shadow="none"
                    textType="Circular"
                    textSize="small"
                    textColor={WHITE}
                    textWeight={500}
                    text="Yes"
                  />
                </View>
              </View>
            )}
          />
          <Modal
            unstyled
            swipeDirection={null}
            isVisible={showVoucherModal}
            position="center"
            animationIn="fadeIn"
            animationOut="fadeOut"
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
            renderModalContent={({ toggleModal }) => (
              <View
                style={{
                  alignItems: "center",
                  padding: WP6,
                  width: WP80,
                  justifyContent: "center",
                  backgroundColor: WHITE,
                  borderRadius: 12,
                }}
              >
                <Image
                  source={require("sf-assets/images/maestroShield.png")}
                  imageStyle={{ width: WP25 }}
                  aspectRatio={55 / 72}
                />
                <View
                  style={[
                    {
                      width: "100%",
                      marginVertical: WP4,
                      alignItems: "center",
                    },
                  ]}
                >
                  <Text size="small" weight={500} color={GREY}>
                    You have free voucher
                  </Text>
                  <Text size="xmini" color={GREY}>
                    Use it to claim your free ticket
                  </Text>
                </View>
                <Button
                  style={{ alignSelf: "center", ...SHADOW_STYLE["shadow"] }}
                  onPress={() => {
                    this._onPressButtonModal("use");
                    toggleModal();
                  }}
                  textColor={WHITE}
                  marginless
                  textSize="mini"
                  textWeight={500}
                  textType="Circular"
                  text={"Use Voucher"}
                  centered
                  radius={10}
                  backgroundColor={TEAL}
                  contentStyle={{
                    paddingVertical: WP3,
                    paddingHorizontal: WP3,
                  }}
                  width={"100%"}
                  shadow="none"
                />

                <TouchableOpacity
                  onPress={() => {
                    this._onPressButtonModal();
                    toggleModal();
                  }}
                >
                  <Text
                    centered
                    style={{ marginTop: WP4 }}
                    type="Circular"
                    weight={500}
                    size="mini"
                  >
                    No, thanks
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          >
            {({ toggleModal }, M) => <View>{M}</View>}
          </Modal>
        </View>
      </Container>
    );
  }
}

SessionRegisterForm.navigationOptions = () => ({
  gesturesEnabled: false,
});
SessionRegisterForm.propTypes = {};
SessionRegisterForm.defaultProps = {};
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SessionRegisterForm),
  mapFromNavigationParam
);
