import SpeakerItem from './SpeakerItem'
import SessionItem from './SessionItem'

export const soundconnectConfig = {
  speaker: {
    title: 'Connect with',
    listTitle: 'Speaker List',
    subtitle: '',
    listItem: SpeakerItem,
    column: 2
  },
  session: {
    title: 'Session',
    listTitle: 'Session List',
    subtitle: '',
    listItem: SessionItem
  },
}

export default {
  soundconnectConfig
}
