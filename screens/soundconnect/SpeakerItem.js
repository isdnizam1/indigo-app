import React from 'react'
import { View } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { upperFirst } from 'lodash-es'
import { WP05, WP100, WP2, WP35, WP6, WP40, WP5, WP1, WP4, WP3, WP50 } from '../../constants/Sizes'
import Image from '../../components/Image'
import { SHADOW_STYLE } from '../../constants/Styles'
import Text from '../../components/Text'
import { WHITE_MILK, GREY, GREY_WARM, GUN_METAL, SHIP_GREY_CALM, SKELETON_COLOR, SKELETON_HIGHLIGHT, PALE_GREY, CLEAR_BLUE, WHITE } from '../../constants/Colors'
import { Avatar } from '../../components'

const SpeakerItemV2 = ({ ads, loading, line }) => {
  const title = loading ? 'Nama Pembicara' : ads.title,
    subtitle = loading ? 'Profesi' : ads.profession,
    isAvailable = ads?.available_session

  return (
    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', paddingVertical: WP4, borderBottomColor: PALE_GREY, borderBottomWidth: line ? 1 : 0, }}>
      <View style={{ marginRight: WP3 }}>
        <Avatar isLoading={loading} size='small' image={ads.image} />
      </View>
      <SkeletonContent
        containerStyle={{ flex: 1 }}
        layout={[
          { width: WP50, height: WP5, marginBottom: WP2 },
          { width: WP35, height: WP3 },
        ]}
        isLoading={loading}
        boneColor={SKELETON_COLOR}
        highlightColor={SKELETON_HIGHLIGHT}
      >
        <View>
          <Text numberOfLines={1} type='Circular' color={GUN_METAL} size='mini' weight={500} style={{ marginBottom: WP05 }}>{upperFirst(title)}</Text>
          <Text numberOfLines={1} type='Circular' color={SHIP_GREY_CALM} size='xmini' weight={300}>{upperFirst(subtitle)}</Text>
        </View>
      </SkeletonContent>
      {
        isAvailable && (
          <View style={{ marginLeft: WP4, backgroundColor: CLEAR_BLUE, paddingHorizontal: WP2, paddingVertical: WP1, borderRadius: 6, }}>
            <Text type='Circular' color={WHITE} size='xmini' weight={400}>Sesi tersedia</Text>
          </View>
        )
      }
    </View>
  )
}

const SpeakerItemList = ({ ads, loading }) => {
  const title = loading ? 'Speaker Name' : ads.title,
    profession = loading ? 'Profession' : ads.profession

  return (
    <View style={{
      marginRight: WP5,
      marginLeft: WP5,
      width: WP40,
    }}
    >
      <View style={{ width: WP40 }}>
        {
          !loading && (
            <View>
              <View style={{
                borderRadius: WP5,
                overflow: 'hidden',
              }}
              >
                <Image
                  source={{ uri: ads.image }}
                  imageStyle={{ height: WP40, aspectRatio: 1 }}
                />
              </View>
            </View>
          )
        }
      </View>
      <View style={{ marginBottom: WP05, marginTop: WP2 }}>
        <Text color={GREY} numberOfLines={1} size='xmini' type='NeoSans' weight={500} centered>{title}</Text>
      </View>
      <Text numberOfLines={2} ellipsizeMode='tail' size='tiny' color={GREY_WARM} centered>{profession}</Text>
    </View>
  )
}

const SpeakerItem = ({ ads, loading = true, list = false, new_version = false, line = false }) => {
  const title = loading ? 'Speaker Name' : ads.title,
    profession = loading ? 'Profession' : ads.profession

  if (list) return SpeakerItemList({ ads, loading })
  if (new_version) return SpeakerItemV2({ ads, loading, line })
  return (
    <View style={{
      marginRight: WP6,
      marginLeft: 1,
      width: WP35,
    }}
    >
      <View style={{ height: WP35, width: WP35, backgroundColor: WHITE_MILK, borderRadius: WP100, ...SHADOW_STYLE['shadowThin'] }}>
        {
          !loading && (
            <View>
              <View style={{
                borderRadius: WP100,
                overflow: 'hidden',
              }}
              >
                <Image
                  source={{ uri: ads.image }}
                  imageStyle={{ height: WP35, aspectRatio: 1 }}
                />
              </View>
            </View>
          )
        }
      </View>
      <View style={{ marginBottom: WP05, marginTop: WP2 }}>
        <Text color={GREY} numberOfLines={1} size='xmini' type='NeoSans' weight={500} centered>{title+'dddd'}</Text>
      </View>
      <Text numberOfLines={2} ellipsizeMode='tail' size='tiny' color={GREY_WARM} centered>{profession}</Text>
    </View>
  )
}

SpeakerItem.propTypes = {}

SpeakerItem.defaultProps = {}

export default SpeakerItem
