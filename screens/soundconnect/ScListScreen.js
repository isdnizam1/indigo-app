import React, { Component } from 'react'
import { ImageBackground, TouchableOpacity, View, FlatList } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { noop } from 'lodash-es'
import { BLUE_LIGHT, GREY, LIGHT_PURPLE, NO_COLOR, WHITE, WHITE_MILK } from '../../constants/Colors'
import { Container, Header, Icon, Image, Text } from '../../components'
import { HP2, WP10, WP100, WP2, WP3, WP40, WP6, WP80 } from '../../constants/Sizes'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { getAds } from '../../actions/api'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { soundconnectConfig } from './SoundconnectConfig'

const mapStateToProps = ({ auth, message }) => ({
  userData: auth.user,
  newMessageCount: message.newMessageCount
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  category: getParam('category', ''),
})

class ScListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: true,
      scList: [],
      announcementList: [],
      announcementLoading: true,
    }
  }

  componentDidMount() {
    const {
      category
    } = this.props
    this._getAdList('announcementList', 'announcement')
    this._getAdList('scList', category)
  }

  _getAdList = async (listName, category) => {
    const {
      dispatch
    } = this.props
    const loading = `${category}Loading`,
      new_category = `sc_${category}`
    this.setState({ [loading]: true })
    try {
      const data = await dispatch(getAds, {
        status: 'active',
        category: new_category,
      }, noop, true, false)

      this.setState({ [loading]: false })
      if (data.code === 200) {
        this.setState({
          [listName]: data.result
        })
      }
    } catch (e) {
      this.setState({ [loading]: false })
    }
  }

  _onPullDownToRefresh = () => {
    this.setState({
      announcementList: [],
    }, this.componentDidMount)
  }
  _announcementList = (loading = true) => {
    const {
      navigateTo,
      navigateBack,
      category
    } = this.props

    const {
      announcementList,
      activeSlider
    } = this.state
    const dummyData = [1, 2, 3, 4],
      data = loading ? dummyData : announcementList

    return (
      <View style={{ marginBottom: WP6 }}>
        <Image
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={320 / 221}
          source={require('../../assets/images/bgSoundconnect.png')}
        />
        <View style={{ position: 'absolute', left: 0, top: 0, right: 0, bottom: 0 }}>
          <Header>
            <Icon
              onPress={() => navigateBack()} background='dark-circle' size='large' color={GREY}
              name='chevron-left' type='Entypo'
            />
            <Text size='small' type='NeoSans' weight={500} color={GREY}>{soundconnectConfig[category]?.listTitle}</Text>
            <Icon size='large' color={NO_COLOR} name='left' />
          </Header>

          <View style={{ paddingVertical: WP2 }}>
            <Carousel
              onSnapToItem={(i) => this.setState({ activeSlider: i })}
              data={data}
              firstItem={activeSlider}
              inactiveSlideScale={1}
              activeSlideAlignment='start'
              sliderWidth={WP100}
              itemWidth={WP80 + WP6}
              containerCustomStyle={{ paddingLeft: WP6 }}
              extraData={this.state}
              renderItem={({ item, index }) => (
                <View
                  style={{
                    width: WP80,
                    borderRadius: 12,
                    marginRight: WP6,
                    marginBottom: WP3,
                    height: WP40,
                    ...SHADOW_STYLE['shadowThin'],
                  }}
                  key={`${index}-announcement`}
                >
                  <View style={{
                    borderRadius: 12,
                    overflow: 'hidden',
                    backgroundColor: WHITE_MILK,
                    height: WP40,
                    width: '100%'
                  }}
                  >
                    {
                      !loading && (
                        <TouchableOpacity
                          activeOpacity={TOUCH_OPACITY}
                          onPress={() => navigateTo('AdsDetailScreenNoTab', { id_ads: item.id_ads })}
                        >
                          <ImageBackground
                            source={{ uri: item.image }}
                            style={{
                              width: WP80,
                              height: WP40,
                              aspectRatio: 2.5
                            }}
                          >
                            <View style={{ width: WP80, flex: 1, justifyContent: 'flex-end', padding: WP3 }}>
                              <View style={{
                                flexDirection: 'row',
                                alignItems: 'flex-end',
                              }}
                              >
                                <Text numberOfLines={2} size='xmini' type='NeoSans' weight={500} color={WHITE} style={{ flex: 1, marginRight: WP6, ...SHADOW_STYLE['shadowBold'] }}>{item.title}</Text>
                                <Image
                                  source={require('../../assets/images/icon.png')}
                                  aspectRatio={1}
                                  size={WP10}
                                  style={{ borderRadius: 8, ...SHADOW_STYLE['shadowThin'] }}
                                  imageStyle={{ borderRadius: 8 }}
                                />
                              </View>
                            </View>
                          </ImageBackground>

                        </TouchableOpacity>
                      )
                    }
                  </View>
                </View>
              )}
            />
            <Pagination
              dotsLength={data.length}
              activeDotIndex={activeSlider}
              dotColor={BLUE_LIGHT}
              inactiveDotColor={WHITE}
              inactiveDotScale={1}
              containerStyle={{
                justifyContent: 'center',
                paddingVertical: 0,
              }}
              dotContainerStyle={{ marginLeft: 1 }}
              dotStyle={{
                width: 8,
                height: 8,
                borderRadius: 4,
              }}
            />
          </View>
        </View>
      </View>
    )
  }

  render() {
    const {
      isLoading,
      category,
      navigateTo
    } = this.props
    const {
      isReady,
      announcementLoading,
      scList
    } = this.state

    const ScItem = soundconnectConfig[category].listItem ? soundconnectConfig[category].listItem : View
    const numColumns = soundconnectConfig[category].column ? soundconnectConfig[category].column : 1

    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={this._onPullDownToRefresh}
        isReady={isReady}
        isLoading={isLoading}
        colors={[LIGHT_PURPLE, LIGHT_PURPLE]}
      >
        <NavigationEvents
          onWillFocus={this._onPullDownToRefresh}
        />
        <View>
          {this._announcementList(announcementLoading)}

          <FlatList
            style={{ width: WP100 }}
            contentContainerStyle={{
              justifyContent: 'center',
              flexDirection: 'column',
              alignItems: 'flex-start',
              alignSelf: 'center',
              flex: 1
            }}
            data={scList}
            numColumns={numColumns}
            keyExtractor={(item, index) => `key${ index}`}
            renderItem={({ item, index }) => (
              <TouchableOpacity
                style={{ paddingVertical: WP2 }}
                key={`${index}${category}`}
                activeOpacity={TOUCH_OPACITY}
                onPress={() => {
                  const { id_ads, profession } = item
                  !profession && navigateTo('SessionDetail', { id_ads })
                  !!profession && navigateTo('SpeakerDetail', { id_ads })
                }}
              >
                <ScItem
                  ads={item}
                  loading={this.state[`${category}Loading`]}
                  navigateTo={navigateTo}
                  list
                />
              </TouchableOpacity>
            )}
          />
          <View style={{ height: HP2 }} />
        </View>
      </Container>
    )
  }
}

ScListScreen.propTypes = {
  navigateTo: PropTypes.func
}

ScListScreen.defaultProps = {
  navigateTo: () => {
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(ScListScreen),
  mapFromNavigationParam
)
