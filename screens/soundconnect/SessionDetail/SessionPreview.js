/* eslint-disable react/sort-comp */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { isUndefined, isNil, upperFirst, get, isObject, noop } from 'lodash-es'
import { isEqualObject, NavigateToInternalBrowserNoTab } from 'sf-utils/helper'
import Container from 'sf-components/Container'
import HeaderNormal from 'sf-components/HeaderNormal'
import Text from 'sf-components/Text'
import Icon from 'sf-components/Icon'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import Badge from 'sf-components/Badge'
import Avatar from 'sf-components/Avatar'
import Touchable from 'sf-components/Touchable'
import ButtonV2 from 'sf-components/ButtonV2'
import Modal from 'sf-components/Modal'
import HTMLElement from 'react-native-render-html'
import { connect } from 'react-redux'
import { postLogRequestSession, postLike } from 'sf-actions/api'
import { LinearGradient } from 'expo-linear-gradient'
import { HEADER } from 'sf-constants/Styles'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'
import {
  WP05,
  WP1,
  WP2,
  WP3,
  WP305,
  WP308,
  WP4,
  WP5,
  WP6,
  WP7,
  WP20,
  WP30,
  WP40,
  WP100,
} from 'sf-constants/Sizes'
import {
  WHITE,
  PALE_BLUE,
  PALE_BLUE_TWO,
  REDDISH,
  PALE_GREY,
  PALE_GREY_TWO,
  SHIP_GREY,
  SHIP_GREY_CALM,
  NAVY_DARK,
  CLEAR_BLUE,
  GUN_METAL,
  PALE_LIGHT_BLUE_TWO,
  SHADOW_GRADIENT,
} from 'sf-constants/Colors'
import {
  StyleSheet,
  View,
  Image,
  FlatList,
  ScrollView,
  Clipboard,
  Platform,
  ToastAndroid,
  ActivityIndicator,
  AppState,
  BackHandler,
} from 'react-native'
import moment from 'moment'
import SoundfrenExploreOptions from 'sf-components/explore/SoundfrenExploreOptions'
import SC_OPTIONS from '../SoundconnectOptions'
import { GREEN_30 } from '../../../constants/Colors'
import { postScreenTimeAdsHelper, toPriceFormat } from '../../../utils/helper'
import { HP5, HP100 } from '../../../constants/Sizes'
import CounterButton from '../../../components/feed/CounterButton'

const coverHeight = (WP100 * 315) / 930
const dateFormat = 'YYYY-MM-DD HH:mm:ss'

class SessionPreview extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showFullDescription: false,
      isActive: false,
      isJoined: false,
      isRequested: false,
      isRequesting: false,
      isSoldOut: false,
      copied: false,
      isModalVisible: false,
      viewFrom: null,
      appState: AppState.currentState,
    }

    this._didFocusSubscription = props.navigation.addListener('focus', () => {
      // console.log('isfocus')
      this.setState({ viewFrom: moment().format(dateFormat) })
      AppState.addEventListener('change', this._handleAppStateChange)
    }
    )

    this._joinNow = this._joinNow.bind(this)
    this.redirectLocation = this.redirectLocation.bind(this)
    this._onLinkPress = this._onLinkPress.bind(this)
    this._toggleFullDescription = this._toggleFullDescription.bind(this)
    this._copySessionLink = this._copySessionLink.bind(this)
    this._listenPodcast = this._listenPodcast.bind(this)
    this._onRequestSession = this._onRequestSession.bind(this)
  }
  
  static propTypes = {
    ads: PropTypes.object,
    navigateTo: PropTypes.func,
    navigateBack: PropTypes.func,
    onRefresh: PropTypes.func,
    isStartup: PropTypes.bool,
  };

  static defaultProps = {
    ads: {
      additional: {
        date: {},
      },
      speaker: [],
    },
  };

  componentDidMount() {
    if (!this.props.userData.id_user) {
      this.props.navigateBack()
      this.props.navigateTo('AuthNavigator')
    }
    this._willBlurSubscription = this.props.navigation.addListener('blur', () => {
      // console.log('isBlur')
      this._postScreenTimeAds()
      AppState.removeEventListener('change', this._handleAppStateChange)
    }
    )
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _didFocusSubscription;
  _willBlurSubscription;

  _handleAppStateChange = (nextAppState) => {
    // console.log(AppState.currentState)
    if (AppState.currentState === 'background') {
      // console.log('called app state')
      this._postScreenTimeAds()
    }
    if ((this.state.appState === 'inactive' || this.state.appState === 'background') && nextAppState === 'active') {
      this.setState({ viewFrom: moment().format(dateFormat) })
      // console.log('back to foreground', this.state.viewFrom)
    }
    this.setState({ appState: nextAppState })
    // console.log({ appState: this.state.appState })
  }

  _postScreenTimeAds = () => {
    const { isLoading, viewFrom } = this.state
    const { ads: { id_ads }, userData: { id_user }, isStartup } = this.props
    // console.log({ id_ads, id_user, viewFrom })
    postScreenTimeAdsHelper({ viewFrom, id_ads, id_user, dispatch: null, isLoading, isStartup })
  }

  _onLinkPress = (event, url) =>
    this.props.navigateTo('BrowserScreenNoTab', { url });

  _toggleFullDescription = () =>
    this.setState({
      showFullDescription: !this.state.showFullDescription,
    });

  _renderModalContent = ({ toggleModal }) => {
    return (
      <SoundfrenExploreOptions
        menuOptions={SC_OPTIONS}
        userData={this.props.userData}
        id={get(this.props.ads, 'id_ads')}
        navigateTo={this.props.navigateTo}
        onClose={toggleModal}
        onShare={() => {
          this.props.navigateTo('ShareScreen', {
            id: get(this.props.ads, 'id_ads'),
            type: 'explore',
            title: 'Bagikan Sesi',
          })
        }}
      />
    )
  };

  _menuOptions = ({ toggleModal }, M) => {
    const { isStartup } = this.props
    return (
      <Fragment>
        <Touchable
          style={style.dotsThreeContainer}
          disabled={!!isStartup}
          onPress={toggleModal}
        >
          {!isStartup && (
            <Icon
              centered
              background='dark-circle'
              color={SHIP_GREY_CALM}
              name='dots-three-horizontal'
              type='Entypo'
            />
          )}
        </Touchable>
        {M}
      </Fragment>
    )
  };

  shouldComponentUpdate(nextProps, nextState) {
    return (
      !isEqualObject(nextProps.ads, this.props.ads) ||
      !isEqualObject(nextState, this.state)
    )
  }

  static getDerivedStateFromProps(props, state) {
    const { ads } = props
    if (isObject(ads))
      return {
        isActive: ads.status === 'active',
        isJoined: ads.user_joined,
        isRequested: ads.is_session_request,
        isSoldOut: parseInt(get(ads, 'available_seats') || 0) <= 0,
        viewFrom: moment().format(dateFormat), // record time when ads successfully loaded
      }
    else return null
  }

  _onPressLike = () => {
    const {
      userData: { id_user },
      ads,
      isStartup,
      onRefresh
    } = this.props

    postLike({
      id_user,
      related_to: 'id_ads',
      id_related_to: ads.id_ads
    }).then(() => {
      onRefresh(() => {})
    })
  }

  _renderHeaderRightModal = () => {
    const {
      userData: { id_user },
      ads,
      isStartup,
    } = this.props
    if (isStartup) {
      return (
        <Touchable onPress={this._onPressLike}>
          <View style={{ padding: 15, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              source={ads.is_liked
                ? require('sf-assets/icons/active_heart.png')
                : require('sf-assets/icons/outlineHeartBlack.png')
              }
              style={{ width: 24, height: 24 }}
            />
          </View>
        </Touchable>
      )
    } else {
      return (
        <Modal
          position='bottom'
          swipeDirection={null}
          renderModalContent={this._renderModalContent}
        >
          {this._menuOptions}
        </Modal>
      )
    }
  }

  _renderHeader = () => {
    const { isActive, isJoined } = this.state
    return (
      <View>
        <HeaderNormal
          style={style.headerNormal}
          centered
          iconLeftOnPress={this.props.navigateBack}
          textType={'Circular'}
          textSize={'mini'}
          noRightPadding
          rightComponent={this._renderHeaderRightModal()}
          text={'Detail Sesi'}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
        {isActive && isJoined && (
          <View
            style={{
              zIndex: 9999,
              backgroundColor: GREEN_30,
              paddingVertical: WP4,
              paddingHorizontal: WP5,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Text type='Circular' color={WHITE} size='slight'>
              Cek emailmu untuk info lengkap sesi ini
            </Text>
            <Icon
              size={'huge'}
              color={WHITE}
              name='mail-outline'
              type='MaterialIcons'
            />
          </View>
        )}
      </View>
    )
  };

  _speakerKeyExtractor = (speaker) => `speaker-${speaker.id_ads}`;

  _renderSpeaker = ({ item: speaker, index }) => {
    return (
      <Touchable
        onPress={() =>
          this.props.navigateTo('SpeakerDetail', { id_ads: speaker.id_ads })
        }
        style={style.speaker}
      >
        <View>
          <Avatar shadow={false} image={speaker.image} />
        </View>
        <View style={style.speakerSummary}>
          <Text color={GUN_METAL} weight={500} size={'small'} type={'Circular'}>
            {upperFirst(speaker.title)}
          </Text>
          <Text color={SHIP_GREY_CALM} weight={300} size={'xmini'} type={'Circular'}>
            {upperFirst(speaker.job_title)}
          </Text>
        </View>
      </Touchable>
    )
  };

  _joinNow = () => {
    try {
      const {
        ads: {
          id_ads,
          additional: {
            redirect: { internal_link, link: url },
          },
        },
        navigateTo,
        isStartup,
      } = this.props
      if (internal_link) navigateTo('SessionRegisterForm', { id_ads, isStartup })
      else NavigateToInternalBrowserNoTab({ url })
    } catch (error) {
      const {
        ads: { id_ads },
        navigateTo,
      } = this.props
      navigateTo('SessionRegisterForm', { id_ads, isStartup })
    }
  };
  redirectLocation = () => {
    const { isLoading, ads } = this.props
      NavigateToInternalBrowserNoTab({ 'url' : this.props.ads.additional_data_arr.location_map });
  };

  _copySessionLink = async () => {
    const {
      ads: {
        status,
        additional: { url },
      },
    } = this.props
    if (!!url && status !== 'expired') {
      await Clipboard.setString(url)
      this.setState({ copied: true }, () => {
        setTimeout(() => this.setState({ copied: false }), 2500)
      })
      Platform.OS === 'android' &&
        ToastAndroid.show('Copied to clipboard', ToastAndroid.SHORT)
    }
  };

  _listenPodcast = () =>
    this.props.navigateTo('SoundplayDetail', get(this.props.ads, 'podcast'));

  _onRequestSession = () => {
    const {
      userData: { id_user },
      ads: { id_ads },
      onRefresh,
    } = this.props
    this.setState(
      {
        isRequesting: true,
      },
      () => {
        postLogRequestSession({ id_user, id_ads }).then(() => {
          onRefresh(() => {
            this.setState({
              isRequesting: false,
              isModalVisible: true,
            })
          })
        })
      },
    )
  };
  _roundDiscountValue(value) {
    if (value.length > 3) {
      return value.split('.')[0] + value.charAt(value.length - 1)
    }
    return value
  }

  _toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible });

  _renderCommentButton = () => {
    const { isLoading, ads } = this.props
    
    return (
      <Touchable
        onPress={this._onPressComment}
        style={{
          
          backgroundColor: WHITE,
          borderTopWidth: 1,
          borderTopColor: '#E4E6E7'
        }}>
        <View style={{
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
          padding: WP2,
          }}>
          <CounterButton
            onPress={this._onPressComment}
            images={[null, require('sf-assets/icons/icComment.png')]}
            value={counterLabel(
              ads?.total_comment,
              'Komentar',
              'Komentar',
            )}
            disable={isLoading}
          />
          <Icon
            centered
            name={'chevron-thin-right'}
            type='Entypo'
            color={SHIP_GREY_CALM}
          />
        </View>
      </Touchable>
    )
  };

  _onPressComment = () => {
    const { navigateTo, ads, onRefresh } = this.props
    navigateTo('StartUpCommentScreen',{
      id_ads: ads.id_ads,
      onRefresh: onRefresh
    })
  }

  render() {
    const {
      ads,
      ads: { additional, speaker: speakers },
      voucher,
    } = this.props
    const {
      showFullDescription,
      isActive,
      isJoined,
      isRequested,
      isRequesting,
      isSoldOut,
      copied,
    } = this.state
    let plainDescription = !isNil(ads)
      ? (ads.description || '')
          .replace(/<\/?[^>]+(>|$)/g, '\n')
          .replace(/ +/g, ' ')
          .trim()
      : ''
    return (
      <Container
        scrollable={false}
        renderHeader={this._renderHeader}
        isReady={!isUndefined(ads.id_ads)}
        isLoading={isUndefined(ads.id_ads)}
      >
        <ScrollView>
          <View style={style.header}>
            <Image
              resizeMode={'cover'}
              style={style.headerBackground}
              source={require('sf-assets/images/bgConnect.png')}
            />
            <View style={style.headerCover}>
              <Image
                resizeMode={'cover'}
                style={style.headerCoverImage}
                source={get(ads, 'image') ? { uri: ads.image } : null}
              />
            </View>
          </View>
          <View style={style.summary}>
            <Text
              color={NAVY_DARK}
              weight={600}
              centered
              size={'large'}
              type={'Circular'}
            >
              {upperFirst(ads.title)}
            </Text>
            <View style={style.spacer2x} />
            <Text
              color={SHIP_GREY}
              weight={300}
              centered
              size={'xmini'}
              type={'Circular'}
            >
              Promoted by  {upperFirst(ads.created_by)}

            </Text>
            <View style={style.spacer2x} />
            <View style={style.rowCentered}>
              <View>
                <Icon
                  size={'mini'}
                  color={SHIP_GREY_CALM}
                  name='calendar'
                  type='MaterialCommunityIcons'
                />
              </View>
              <Text
                color={SHIP_GREY_CALM}
                weight={300}
                centered
                size={'mini'}
                type={'Circular'}
              >
                {`  ${moment(get(additional, 'date.start')).format(
                  'ddd, DD MMM Y | hh:mm',
                )} WIB`}
              </Text>
            </View>
          </View>
          <View style={style.content}>
            {!isActive && (
              <View style={style.contentActionRow}>
                <ButtonV2
                  // disabled={!isRequested}
                  onPress={isRequested? noop : this._onRequestSession}
                  style={style.contentActionRowButton}
                  forceBorderColor
                  borderColor={PALE_BLUE_TWO}
                  color={isRequested ? PALE_GREY_TWO : WHITE}
                  textColor={SHIP_GREY}
                  textWeight={300}
                  icon={!isRequesting ? (isRequested ? 'check' : 'history') : null}
                  text={!isRequesting ? 'Request sesi ini' : null}
                >
                  {isRequesting && (
                    <View style={{ paddingVertical: WP2 + WP05 }}>
                      <ActivityIndicator size={'small'} color={REDDISH} />
                    </View>
                  )}
                </ButtonV2>
                {!!get(ads, 'podcast') && <View style={{ width: WP3 }} />}
                {!!get(ads, 'podcast') && (
                  <ButtonV2
                    onPress={this._listenPodcast}
                    style={style.contentActionRowButton}
                    iconColor={WHITE}
                    color={REDDISH}
                    textColor={WHITE}
                    textWeight={300}
                    icon={'play-circle'}
                    text={'Mainkan podcast'}
                  />
                )}
              </View>
            )}
            {isActive && isJoined && (
              <View style={style.contentLink}>
                <Text color={SHIP_GREY} weight={400} size={'mini'} type={'Circular'}>
                  Link Sesi Webinar
                </Text>
                <View style={style.contentLinkRow}>
                  <ButtonV2
                    onPress={this._copySessionLink}
                    style={style.contentLinkRowURL}
                    forceBorderColor
                    borderColor={PALE_BLUE_TWO}
                    color={PALE_GREY_TWO}
                    textColor={additional.url ? SHIP_GREY : SHIP_GREY_CALM}
                    textWeight={300}
                    text={additional.url ? additional.url : 'Belum tersedia'}
                  />
                  <View>
                    <ButtonV2
                      disabled={!additional.url || copied}
                      color={REDDISH}
                      onPress={this._copySessionLink}
                      textColor={WHITE}
                      style={style.contentLinkRowCopy}
                      text={copied ? 'Copied' : 'Copy'}
                    />
                  </View>
                </View>
                {!additional.url && (
                  <Text size={'tiny'} type={'Circular'} color={SHIP_GREY_CALM}>
                    Tersedia 1 jam sebelum sesi dimulai
                  </Text>
                )}
              </View>
            )}
            {isActive && !isJoined && (
              <View style={style.contentTicket}>
                <Text color={SHIP_GREY} weight={400} size={'mini'} type={'Circular'}>
                  Tiket:
                </Text>
                <View style={style.spacer2x} />
                <View style={style.row}>
                  {!isSoldOut ? (
                    <Badge textSize={'xmini'} textColor={WHITE} color={CLEAR_BLUE}>
                      {get(ads, 'available_seats') > 10
                        ? '10+'
                        : get(ads, 'available_seats')}{' '}
                      tiket tersedia
                    </Badge>
                  ) : (
                    <Badge
                      textSize={'xmini'}
                      textColor={SHIP_GREY_CALM}
                      color={PALE_GREY}
                    >
                      Sold Out
                    </Badge>
                  )}
                </View>
              </View>
            )}
            <View style={style.contentDescription}>
            <Text color={SHIP_GREY} weight={400} size={'mini'} type={'Circular'}>
                Lokasi :
              </Text>
              {ads.additional_data_arr ? (
            <View style={style.contentDescription}>
              <View style={style.spacer2x} />
              <Text color={SHIP_GREY} weight={300} size={'mini'} type={'Circular'}>
              {ads.additional_data_arr.location ? (ads.additional_data_arr.location  ) : ( 'Online' )}
              </Text>
                <View style={style.spacer2x} />
              {ads.additional_data_arr.location_map ? (
              <Text color={REDDISH} weight={300} size={'mini'} type={'Circular'} onPress={this.redirectLocation}>
                Klik untuk menuju ke lokasi
              </Text>
              ) : ( null )}
            </View>
              ) : (
                <View style={style.contentDescription}>

                <View style={style.spacer2x} />
                <Text color={SHIP_GREY} weight={300} size={'mini'} type={'Circular'}>
                  Online
                </Text>
              </View>
              )
          }
              <View style={style.spacer3x} />
              <View style={style.spacer3x} />
              <Text color={SHIP_GREY} weight={400} size={'mini'} type={'Circular'}>
                Deskripsi:
              </Text>
              <View style={style.spacer3x} />
              {showFullDescription ? (
                <View>
                  <HTMLElement
                    textSelectable
                    html={upperFirst(ads.description)
                      .trim()
                      .replace(/(\r\n|\n|\r|)/gm, '')}
                    onLinkPress={this._onLinkPress}
                    baseFontStyle={style.contentDescriptionText}
                  />
                  <View style={style.spacer1x} />
                  <Text
                    type={'Circular'}
                    size={'mini'}
                    onPress={this._toggleFullDescription}
                    color={REDDISH}
                  >
                    Read less...
                  </Text>
                </View>
              ) : (
                <View>
                  <HTMLElement
                    textSelectable
                    html={upperFirst(ads.description)
                      .trim()
                      .replace(/(\r\n|\n|\r|)/gm, '').substring(0, 200)}
                    onLinkPress={this._onLinkPress}
                    baseFontStyle={style.contentDescriptionText}
                  />
                  <Text style={style.contentDescriptionText}>
                    {plainDescription.length > 200 && '... '}
                  </Text>
                  {plainDescription.length > 200 && <View style={style.spacer1x} />}
                  {plainDescription.length > 200 && (
                    <Text
                      type={'Circular'}
                      size={'mini'}
                      onPress={this._toggleFullDescription}
                      color={REDDISH}
                    >
                      Read more...
                    </Text>
                  )}
                </View>
              )}
              <View style={style.spacer2x} />
            </View>
          </View>
          <View style={style.contentNoHorizontalPadding}>
            <View style={style.speakerHeader}>
              <Text color={SHIP_GREY} weight={400} size={'mini'} type={'Circular'}>
                Sesi bersama:
              </Text>
            </View>
            <View style={style.spacer2x} />
            <FlatList
              data={speakers}
              keyExtractor={this._speakerKeyExtractor}
              renderItem={this._renderSpeaker}
            />
          </View>
        </ScrollView>
        {this._renderCommentButton()}
        {!isJoined && isActive && !isSoldOut && (
          <View style={style.pricing}>
            {!voucher && (
              <View style={style.pricingSummary}>
                <Text
                  color={PALE_LIGHT_BLUE_TWO}
                  weight={300}
                  size={'tiny'}
                  type={'Circular'}
                >
                  1 tiket{' '}
                  {get(additional, 'price.discount')
                    ? `(diskon ${this._roundDiscountValue(
                        get(additional, 'price.discount'),
                      )})`
                    : null}
                </Text>
                <Text
                  color={SHIP_GREY}
                  weight={500}
                  size={'xmini'}
                  type={'Circular'}
                  style={style.pricingSummaryNormalPrice}
                >
                  {get(additional, 'price.currency')}{' '}
                  {toPriceFormat(get(additional, 'price.normal_price'))}
                </Text>
                <Text
                  color={SHIP_GREY}
                  weight={600}
                  size={'small'}
                  type={'Circular'}
                >
                  {get(additional, 'price.currency')}{' '}
                  {toPriceFormat(get(additional, 'price.discount_price'))}
                </Text>
              </View>
            )}
            <View style={[style.pricingAction, { paddingLeft: voucher ? 0 : WP3 }]}>
              <ButtonV2
                disabled={isSoldOut}
                onPress={this._joinNow}
                style={style.pricingActionButton}
                color={REDDISH}
                textColor={WHITE}
                textSize={'small'}
                text={voucher ? 'Pilih Sesi' : 'Daftar'}
              />
            </View>
          </View>
        )}

        <ModalMessageView
          toggleModal={this._toggleModal}
          isVisible={this.state.isModalVisible}
          title={'Request terkirim'}
          titleType='Circular'
          titleSize={'small'}
          titleColor={GUN_METAL}
          subtitle={
            'Terima kasih atas antusias kamu untuk sesi ini. Kamu bisa cek sesi lainnya lagi.'
          }
          subtitleType='Circular'
          subtitleSize={'xmini'}
          subtitleWeight={300}
          subtitleLineHeight={WP5}
          subtitleColor={SHIP_GREY_CALM}
          image={null}
          buttonPrimaryText={'Cari Sesi Lagi'}
          buttonPrimaryTextType='Circular'
          buttonPrimaryTextWeight={400}
          buttonPrimaryBgColor={REDDISH}
          buttonPrimaryAction={this.props.navigateBack}
        />
      </Container>
    )
  }
}

const counterLabel = (value, label, empty) => {
  let labelText = value < 1 ? empty : label
  let valueText = value < 1 ? '' : `${value} `
  return `${valueText}${labelText}`
}

const style = StyleSheet.create({
  headerNormal: {
    paddingVertical: 0,
    paddingRight: 0,
  },
  spacer1x: {
    height: WP1,
  },
  spacer2x: {
    height: WP2,
  },
  spacer3x: {
    height: WP3,
  },
  row: {
    flexDirection: 'row',
  },
  headerBackground: {
    width: WP100,
    height: coverHeight,
    marginBottom: WP20,
  },
  headerCover: {
    width: WP40,
    height: WP40,
    position: 'absolute',
    left: WP30,
    top: WP7,
    borderRadius: WP2,
    backgroundColor: WHITE,
    elevation: 10,
  },
  headerCoverImage: {
    width: WP40,
    height: WP40,
    borderRadius: WP2,
  },
  summary: {
    paddingHorizontal: WP5,
    paddingBottom: WP7,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  rowCentered: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    padding: WP5,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  contentNoHorizontalPadding: {
    paddingVertical: WP5,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  contentTicket: {
    paddingBottom: WP5,
    marginBottom: WP5,
    borderBottomWidth: 1,
    borderBottomColor: PALE_GREY,
  },
  contentLink: {
    paddingBottom: WP5,
    marginBottom: WP5,
    borderBottomWidth: 1,
    borderBottomColor: PALE_GREY,
  },
  contentDescriptionText: {
    fontSize: WP308,
    fontFamily: 'CircularBook',
    color: SHIP_GREY_CALM,
    lineHeight: WP6,
  },
  speakerHeader: {
    paddingHorizontal: WP5,
  },
  speaker: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: WP5,
    paddingVertical: WP2,
  },
  speakerSummary: {
    paddingLeft: WP5,
    flex: 1,
  },
  pricing: {
    backgroundColor: WHITE,
    elevation: 25,
    paddingHorizontal: WP5,
    paddingVertical: WP4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  pricingSummaryNormalPrice: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  pricingAction: {
    flex: 1,
    paddingLeft: WP5,
  },
  pricingActionButton: {
    paddingVertical: WP305,
  },
  contentActionRow: {
    flexDirection: 'row',
    paddingBottom: WP5,
    marginBottom: WP5,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  contentActionRowButton: {
    flex: 1,
    paddingVertical: 0,
    paddingHorizontal: WP2,
  },
  contentLinkRow: {
    flexDirection: 'row',
    marginTop: WP2,
    marginBottom: WP1 + WP05,
  },
  contentLinkRowURL: {
    justifyContent: 'flex-start',
    paddingHorizontal: WP3,
    flex: 1,
    marginRight: WP3,
  },
  contentLinkRowCopy: {
    paddingHorizontal: WP5,
    flex: 0,
  },
  dotsThreeContainer: {
    paddingHorizontal: WP4,
    paddingVertical: WP2 + WP05,
    ...HEADER.rightIcon,
  },
})

export default connect(
  ({ auth: { user: userData }, voucher: { voucher } }) => ({ userData, voucher }),
  null,
)(SessionPreview)

// export default _enhancedNavigation(
//   connect(({ auth: { user: userData }, voucher: { voucher } }) => ({ userData, voucher }), null)(SessionPreview),
//   null,
// )
