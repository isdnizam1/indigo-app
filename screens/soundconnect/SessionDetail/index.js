import React, { Component } from 'react'
import { connect } from 'react-redux'
import { noop, omit, isNil, isFunction, isUndefined } from 'lodash-es'
import { getAdsDetail } from 'sf-actions/api'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import { NavigationEvents } from '@react-navigation/compat'
import { StyleSheet, View } from 'react-native'
import SessionPreview from './SessionPreview'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam('id_ads', null),
  joined: getParam('joined', false),
  isStartup: getParam('isStartup', false),
  refreshListLearn: getParam('refreshListLearn', () => {})
})

class SessionDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ads: null,
    }
  }

  async componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('blur', () => {
      if (this.props.refreshListLearn) {
        this.props.refreshListLearn()
      }
    }
    )
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _didFocusSubscription;
  _willBlurSubscription;

  _getAds = (callback) => {
    let {
      id_ads,
      dispatch,
      userData: { id_user },
    } = this.props
    Promise.all([
      dispatch(getAdsDetail, { id_ads, id_user }, noop, false, false),
    ]).then((results) => {
      const [ads] = results
      this.setState(
        {
          ads: omit(
            {
              ...ads,
              additional: JSON.parse(ads.additional_data),
            },
            ['additional_data'],
          ),
        },
        () => {
          !isUndefined(callback) && isFunction(callback) && callback()
        },
      )
    })
  };

  render() {
    const { ads } = this.state
    const { navigateTo, navigateBack, isStartup, ...props } = this.props
    return (
      <View style={style.container}>
        {isNil(ads) ? (
          <SessionPreview navigateTo={navigateTo} navigateBack={navigateBack} isStartup={isStartup} {...props} />
        ) : (
          <SessionPreview
            onRefresh={this._getAds}
            navigateTo={navigateTo}
            navigateBack={navigateBack}
            ads={ads}
            isStartup={isStartup}
            {...props}
          />
        )}
        <NavigationEvents onDidFocus={this._getAds} />
      </View>
    )
  }
}

const style = StyleSheet.create({ container: { flex: 1 } })

export default _enhancedNavigation(
  connect(mapStateToProps, {})(SessionDetail),
  mapFromNavigationParam,
)
