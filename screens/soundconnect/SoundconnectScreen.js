import React, { Component, Fragment } from 'react'
import {
  View,
  TouchableOpacity,
  Animated as RNAnimated,
  StatusBar as StatusBarRN,
  ScrollView,
  Easing,
} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import SkeletonContent from 'react-native-skeleton-content'
import * as Animatable from 'react-native-animatable'
import { isEmpty, noop, get } from 'lodash-es'
import {
  NAVY_DARK,
  NO_COLOR,
  PALE_BLUE,
  PALE_BLUE_TWO,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
  SHADOW_GRADIENT,
} from 'sf-constants/Colors'
import { Container, Icon, Image, Text, Modal } from 'sf-components'
import { WP100, WP105, WP12, WP15, WP2, WP3, WP4, WP5, WP50, WP6, WP70, WP1, HP50, HP3, HP1 } from 'sf-constants/Sizes'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import { getAds, getSettingDetail } from 'sf-actions/api'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import SoundfrenExploreOptions from 'sf-components/explore/SoundfrenExploreOptions'
import { getStatusBarHeight } from 'sf-utils/iphoneXHelper'
import { FONTS } from 'sf-constants/Fonts'
import Animated from 'react-native-reanimated'

import HeaderNormal from 'sf-components/HeaderNormal'
import { LinearGradient } from 'expo-linear-gradient'
import MessageBarClearBlue from 'sf-components/messagebar/MessageBarClearBlue'
import style from '../profile/v2/ProfileScreen/style'
import { restrictedAction } from '../../utils/helper'
import SC_OPTIONS from './SoundconnectOptions'
import SessionItem from './SessionItem'
import SpeakerItem from './SpeakerItem'
import EmptyState from '../../components/EmptyState'

const HEADER_HEIGHT = WP12
const BANNER_SIZE = (WP100 * 168) / 360
const DATA_BAR = [{ title: 'SESI' }, { title: 'PEMBICARA' }]
const SC_CONFIG = {
  session_active: {
    title: "Sesi tersedia",
    key_data: "sessionActiveList",
    line: false,
    empty_message_title: "Belum ada sesi saat ini",
    empty_message_desc: "Tunggu webinar menarik untuk kamu !",
    type: "session",
  },
  session_expired: {
    title: "Sesi sebelumnya",
    key_data: "sessionExpiredList",
    line: true,
    empty_message_title: "Belum ada sesi saat ini",
    empty_message_desc: "Tunggu webinar menarik untuk kamu !",
    type: "session",
  },
  speaker: {
    title: "",
    key_data: "speakerList",
    line: false,
    empty_message_title: "Pembicara belum tersedia saat ini",
    empty_message_desc: "Akses profile pembicara kamu disini",
    type: "speaker",
  },
};

const mapStateToProps = ({ auth, message, voucher: { voucher } }) => ({
  userData: auth.user,
  newMessageCount: message.newMessageCount,
  voucher,
})
const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  isStartup: getParam('isStartup', false),
})

class SoundconnectScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      about: {},
      speakerList: [],
      sessionActiveList: [],
      sessionExpiredList: [],
      speakerLoading: true,
      sessionActiveLoading: true,
      sessionExpiredLoading: true,
      activeBar: 0,
      tabPosition: "bottom",
      headerOpacity: new RNAnimated.Value(0),
      statusBarTheme: "light",
    };
    this.y = 0;
  }

  async componentDidMount() {
    this._getAbout();
    this._getContent("sessionActiveList", "session", "active");
    this._getContent("sessionExpiredList", "session", "expired", {
      start: 0,
      limit: 5,
    });
    this._getContent("speakerList", "speaker", "active");
  }

  _getContent = async (
    listName,
    category,
    status,
    params = {},
    loadMore = false,
    isLoading = false
  ) => {
    const {
      dispatch,
      userData: { id_user },
      isStartup,
    } = this.props;
    const loading = `${listName.replace("List", "")}Loading`,
      new_category = `sc_${category}`;
    if (!loadMore) {
      this.setState({
        [loading]: true,
      });
    }

    try {
      let new_params = undefined;
      new_params =
        category == "session"
          ? { id_viewer: id_user, ...params }
          : { ...params };
      if (isStartup) {
        new_params =
          category == "session"
            ? { platform: "1000startup", id_viewer: id_user, ...params }
            : { platform: "1000startup", ...params };
      }
      const { result } = await dispatch(
        getAds,
        {
          status,
          category: new_category,
          start: 0,
          id_viewer: id_user,
          ...new_params,
        },
        noop,
        true,
        isLoading
      );

      if (!loadMore) {
        this.setState({
          [listName]: result,
          [loading]: false,
        });
      } else {
        const mergeData = [...this.state[listName], ...result];
        this.setState({
          [listName]: mergeData,
          [loading]: false,
        });
      }
    } catch (e) {
      this.setState({ [loading]: false });
    }
  };

  _getAbout = async () => {
    const { dispatch } = this.props;

    try {
      const response = await dispatch(
        getSettingDetail,
        {
          setting_name: "ads",
          key_name: "soundfren_connect",
        },
        noop,
        false,
        false
      );
      this.setState({ about: JSON.parse(response.value) });
    } catch (error) {
      //
    }
  };

  _onScrollHorizontal = ({ nativeEvent }) => {
    let x = nativeEvent.contentOffset.x;
    this.setState(
      {
        activeBar: Math.round(x / WP100),
      },
      () => {
        this.scrollVertical.scrollTo({ x: 0, y: 0, animated: true });
      }
    );
  };

  _onScroll = ({
    nativeEvent: {
      contentOffset: { y: scrollPosition },
    },
  }) => {
    const { statusBarTheme: theme, headerOpacity } = this.state;
    if (scrollPosition > 15 && theme === "light") {
      RNAnimated.timing(headerOpacity, {
        toValue: 1,
        useNativeDriver: true,
        duration: 250,
        delay: 0,
        isInteraction: false,
      }).start();
      this.setState({
        statusBarTheme: "dark",
      });
    }
    if (scrollPosition <= 15 && theme === "dark") {
      RNAnimated.timing(headerOpacity, {
        toValue: 0,
        useNativeDriver: true,
        duration: 250,
        delay: 0,
        isInteraction: false,
      }).start();
      this.setState({
        statusBarTheme: "light",
      });
    }
  };

  _headerSearch = () => {
    const { navigateBack, isStartup } = this.props;
    const iconColor = WHITE;
    return (
      <View
        style={{
          width: WP100,
          height: HEADER_HEIGHT,
          position: "absolute",
          zIndex: 999,
          top: 0,
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingLeft: WP4,
            paddingVertical: WP2,
            paddingTop: WP2 + getStatusBarHeight(true),
          }}
        >
          <Icon
            centered
            onPress={() => navigateBack()}
            background="dark-circle"
            backgroundColor="rgba(255,255,255, 0.16)"
            size="huge"
            color={iconColor || SHIP_GREY_CALM}
            name="chevron-left"
            type="Entypo"
          />
          {isStartup ? null : (
            <Icon
              centered
              background="dark-circle"
              size="huge"
              style={{ marginRight: WP5 }}
              color={iconColor || SHIP_GREY}
              name="dots-three-horizontal"
              type="Entypo"
            />
          )}
        </View>
      </View>
    );
  };

  _headerSection = () => {
    const { sessionActiveLoading, about } = this.state;
    let title = about?.title || "Agenda";
    let subtitle =
      about.subtitle || "Lihat agenda aktivitas event  \n kamu disini";
    if (this.props.isStartup) {
      title = "Agenda";
      subtitle = "Perluas wawasan dan pengetahuan tentang Startupmu disini!";
    }
    return (
      <View>
        <View style={{ height: BANNER_SIZE, width: WP100 }}>
          <Image
            tint={"black"}
            imageStyle={{ width: WP100, height: undefined }}
            aspectRatio={360 / 168}
            source={require("sf-assets/images/bgConnect.png")}
          />
          <View
            style={{
              position: "absolute",
              left: 0,
              top: getStatusBarHeight(true),
              right: 0,
              bottom: 0,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <SkeletonContent
              containerStyle={{ alignItems: "center", paddingHorizontal: WP12 }}
              layout={[
                { width: WP50, height: WP6, marginBottom: WP2 },
                { width: WP70, height: WP4, marginBottom: WP105 },
              ]}
              isLoading={sessionActiveLoading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View style={{ alignItems: "center" }}>
                <Text
                  type="Circular"
                  size="large"
                  weight={600}
                  color={WHITE}
                  centered
                  style={{ marginBottom: WP2 }}
                >
                  {title}
                </Text>
                <Text
                  type="Circular"
                  size="mini"
                  weight={300}
                  color={WHITE}
                  centered
                  style={{ lineHeight: WP6 }}
                >
                  {subtitle}
                </Text>
              </View>
            </SkeletonContent>
          </View>
        </View>
        {this._renderTabBar()}
      </View>
    );
  };

  _renderTabBar = () => {
    const { activeBar } = this.state;
    const onChangeTab = (index) =>
      this.scrollHorizontal.scrollTo({
        x: index * WP100,
        y: 0,
        animated: true,
      });
    return (
      <View>
        <View
          style={{
            width: WP100,
            height: HEADER_HEIGHT,
            backgroundColor: WHITE,
            flexDirection: "row",
            alignItems: "center",
            borderBottomWidth: 1,
            borderBottomColor: PALE_BLUE_TWO,
          }}
        >
          {DATA_BAR.map((item, index) => (
            <TouchableOpacity
              key={`${Math.random() + index}`}
              activeOpacity={TOUCH_OPACITY}
              onPress={() => onChangeTab(index)}
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                paddingVertical: WP2,
                paddingHorizontal: WP3,
              }}
            >
              <Text
                type="Circular"
                color={activeBar == index ? REDDISH : SHIP_GREY_CALM}
                size={"xmini"}
                weight={600}
                centered
              >
                {item.title}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={{ flex: 1, position: "absolute", bottom: 0 }}>
          <View
            style={{
              width: WP100 / DATA_BAR.length,
              height: 2,
              backgroundColor: REDDISH,
              transform: [
                { translateX: (WP100 / DATA_BAR.length) * activeBar },
              ],
            }}
          />
        </View>
      </View>
    );
  };

  _onPullDownToRefresh = () => {
    this.setState(
      {
        about: {},
        speakerList: [],
        sessionActiveList: [],
        sessionExpiredList: [],
      },
      this.componentDidMount
    );
  };

  _adItem = (ads, i, category, loading = true, totalData) => {
    let { navigateTo, voucher, isStartup } = this.props;
    const additional_data = JSON.parse(get(ads, "additional_data") || "{}");
    return (
      <TouchableOpacity
        disabled={loading}
        key={`${i}${new Date()}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={() => {
          const { userData, navigation } = this.props;
          const { id_ads } = ads;
          let action = noop;
          if (category === "session_active" || category === "session_expired") {
            action = () => navigateTo("SessionDetail", { id_ads, isStartup });
          } else if (category === "speaker") {
            action = () => navigateTo("SpeakerDetail", { id_ads });
          }
          restrictedAction({ action, userData, navigation })();
        }}
      >
        {category === "session_active" &&
          (!voucher || !!get(additional_data, "redirect.internal_link")) && (
            <SessionItem
              ads={ads}
              loading={loading}
              new_version={true}
              line={i < totalData - 1}
            />
          )}
        {category === "session_expired" && (
          <SessionItem
            ads={ads}
            loading={loading}
            new_version={true}
            line={i < totalData - 1}
          />
        )}
        {category === "speaker" && (
          <SpeakerItem
            ads={ads}
            loading={loading}
            new_version={true}
            line={i < totalData - 1}
          />
        )}
      </TouchableOpacity>
    );
  };

  // _section = (section) => {
  //   const config = SC_CONFIG[section];
  //   const dummyData = [1, 2, 3, 4, 5];
  //   const keyData = config.key_data;
  //   const isLoading = this.state[`${keyData.replace("List", "")}Loading`];
  //   const dataList = isLoading ? dummyData : this.state[keyData];
  //   return !isEmpty(this.state[keyData]) || isLoading ? (
  //     <View
  //       style={{
  //         borderTopColor: PALE_BLUE,
  //         borderTopWidth: config.line ? 1 : 0,
  //       }}
  //     >
  //       <View style={{ paddingVertical: section == "speaker" ? WP4 : WP6 }}>
  //         {/* HEADER */}
  //         {!isEmpty(config.title) && (
  //           <View
  //             style={{
  //               flexDirection: "row",
  //               justifyContent: "space-between",
  //               alignItems: "center",
  //               paddingHorizontal: WP4,
  //             }}
  //           >
  //             <Text type="Circular" size="small" color={NAVY_DARK} weight={600}>
  //               {config.title}
  //             </Text>
  //           </View>
  //         )}
  //         {/* ITEM */}
  //         <View style={{ paddingHorizontal: WP4 }}>
  //           {!isEmpty(dataList) ? (
  //             dataList.map((item, index) =>
  //               this._adItem(item, index, section, isLoading, dataList.length)
  //             )
  //           ) : (
  //             /* if data is empty */
  //             <View
  //               style={{
  //                 justifyContent: "center",
  //                 alignItems: "center",
  //                 height: config.type == "session" ? HP50 : HP50 * 1.25,
  //               }}
  //             >
  //               <EmptyState
  //                 image={
  //                   config.type == "session"
  //                     ? require(`sf-assets/icons/ic_webinar_session_emptystate.png`)
  //                     : require(`sf-assets/icons/ic_webinar_speaker_emptystate.png`)
  //                 }
  //                 title={config.empty_message_title}
  //                 message={config.empty_message_desc}
  //               />
  //             </View>
  //           )}
  //         </View>
  //       </View>
  //     </View>
  //   ) : (
  //     <View style={{ paddingHorizontal: WP4, paddingVertical: WP6 }}>
  //       <Text type="Circular" size="small" weight={400} color={SHIP_GREY_CALM}>
  //         Maaf, untuk saat ini belum tersedia
  //       </Text>
  //     </View>
  //   );
  // };

  _section = (section) => {
    const config = SC_CONFIG[section];
    const dummyData = [1, 2, 3, 4, 5];
    const keyData = config.key_data;
    const isLoading = this.state[`${keyData.replace("List", "")}Loading`];
    const dataList = isLoading ? dummyData : this.state[keyData];
    return (
      <View
        style={{
          borderTopColor: PALE_BLUE,
          borderTopWidth: config.line ? 1 : 0,
        }}
      >
        <View style={{ paddingVertical: section == "speaker" ? WP4 : WP6 }}>
          {/* HEADER */}
          {!isEmpty(config.title) && (
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                paddingHorizontal: WP4,
              }}
            >
              <Text type="Circular" size="small" color={NAVY_DARK} weight={600}>
                {config.title}
              </Text>
            </View>
          )}
          {/* ITEM */}
          <View style={{ paddingHorizontal: WP4 }}>
            {!isEmpty(dataList) || isLoading ? (
              dataList.map((item, index) =>
                this._adItem(item, index, section, isLoading, dataList.length)
              )
            ) : (
              /* if data is empty */
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  height: config.type == "session" ? HP50 : HP50 * 1.25,
                }}
              >
                <EmptyState
                  image={
                    config.type == "session"
                      ? require(`sf-assets/icons/ic_webinar_session_emptystate.png`)
                      : require(`sf-assets/icons/ic_webinar_speaker_emptystate.png`)
                  }
                  title={config.empty_message_title}
                  message={config.empty_message_desc}
                />
              </View>
            )}
          </View>
        </View>
      </View>
    );
  };

  render() {
    const {
      isReady,
      activeBar,
      // sessionExpiredList
    } = this.state;
    const { userData, navigation, isStartup } = this.props;
    return (
      <Container
        isReady={isReady}
        isLoading={false}
        noStatusBarPadding
        statusBarBackground={NO_COLOR}
      >
        <Animated.ScrollView
          onScroll={this._onScroll}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        >
          <View style={{ flex: 1 }}>
            <NavigationEvents onWillFocus={this._onPullDownToRefresh} />
            {this._headerSearch()}
            {this._headerSection()}
            <Container
              noStatusBarPadding
              scrollable
              scrollBackgroundColor={WHITE}
              onPullDownToRefresh={this._onPullDownToRefresh}
              onScrollViewRef={(ref) => (this.scrollVertical = ref)}
              onPullUpToLoad={
                /*activeBar == 0 ? async () => {
                await this._getContent('sessionExpiredList', 'session', 'expired', { start: sessionExpiredList.length, limit: 5 }, true, false)
              } : null*/ null
              }
            >
              <ScrollView
                ref={(ref) => {
                  this.scrollHorizontal = ref;
                }}
                horizontal={true}
                pagingEnabled
                bounces={false}
                showsHorizontalScrollIndicator={false}
                onScroll={this._onScrollHorizontal}
                scrollEventThrottle={1}
                style={{ flex: 1, backgroundColor: WHITE }}
              >
                <View key={`${Math.random() + 0}`} style={{ width: WP100 }}>
                  {activeBar == 0 && (
                    <View style={{ flex: 1 }}>
                      {this._section("session_active")}
                      {!this.props.voucher && this._section("session_expired")}
                    </View>
                  )}
                </View>
                <View key={`${Math.random() + 1}`} style={{ width: WP100 }}>
                  {activeBar == 1 && (
                    <View style={{ flex: 1 }}>{this._section("speaker")}</View>
                  )}
                </View>
              </ScrollView>
            </Container>
          </View>
        </Animated.ScrollView>
        <RNAnimated.View
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            backgroundColor: WHITE,
            paddingTop: getStatusBarHeight(true),
            opacity: this.state.headerOpacity,
            paddingHorizontal: WP1,
          }}
        >
          <HeaderNormal
            withExtraPadding
            iconLeftOnPress={this.props.navigateBack}
            centered
            textType={"Circular"}
            textSize={"slight"}
            text={"Webinar"}
            rightComponent={() => {
              return isStartup ? null : (
                <Modal
                  position="bottom"
                  swipeDirection={null}
                  renderModalContent={({ toggleModal }) => {
                    return (
                      <SoundfrenExploreOptions
                        menuOptions={SC_OPTIONS}
                        userData={this.props.userData}
                        navigateTo={this.props.navigateTo}
                        onClose={toggleModal}
                      />
                    );
                  }}
                >
                  {({ toggleModal }, M) => (
                    <Fragment>
                      <TouchableOpacity
                        onPress={restrictedAction({
                          action: toggleModal,
                          userData,
                          navigation,
                        })}
                        activeOpacity={TOUCH_OPACITY}
                        style={{
                          height: "100%",
                          width: WP15,
                          justifyContent: "center",
                          alignItems: "flex-end",
                          paddingRight: WP4,
                          paddingVertical: WP2,
                        }}
                      >
                        <View style={{ flex: 1 }}>
                          <Icon
                            centered
                            background="dark-circle"
                            size="huge"
                            color={SHIP_GREY}
                            name="dots-three-horizontal"
                            type="Entypo"
                          />
                        </View>
                      </TouchableOpacity>
                      {M}
                    </Fragment>
                  )}
                </Modal>
              );
            }}
          />
          <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
          <View>
            <MessageBarClearBlue />
          </View>
        </RNAnimated.View>
        <StatusBarRN
          animated
          translucent={true}
          barStyle={`${this.state.statusBarTheme}-content`}
        />
      </Container>
    );
  }
}

SoundconnectScreen.propTypes = {
  navigateTo: PropTypes.func,
}

SoundconnectScreen.defaultProps = {
  navigateTo: () => {},
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(SoundconnectScreen),
  mapFromNavigationParam,
)
