import { ORANGE_BRIGHT } from '../../constants/Colors'
import { FONTS } from '../../constants/Fonts'

export const SC_OPTIONS = (props) => {
  let menu = [
    // {
    //   type: 'menu',
    //   image: require('../../assets/icons/badgeSoundfrenPremium.png'),
    //   imageStyle: {},
    //   name: 'Premium',
    //   textStyle: { color: ORANGE_BRIGHT, fontFamily: FONTS.Circular['500'] },
    //   onPress: () => { },
    //   joinButton: false,
    //   isPremiumMenu: true
    // },
    {
      type: 'menu',
      image: require('../../assets/icons/icAboutShipGrey.png'),
      imageStyle: {},
      name: 'Tentang Webinar',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('AboutFeatureScreen', {
          content: {
            image: require('../../assets/images/bgAboutConnect.jpg'),
            imageRatio: 360 / 198,
            title: 'Agenda',
            subtitle: 'Lihat agenda aktivitas event kamu disini',
            header: 'Tentang Webinar',
            paragraph: [
              'Eventeer Webinar memungkinkan kamu dapat terhubung dan juga berinteraksi melalui sesi dengan profesional dari berbagai bidang di industri musik. Melalui Soundfren Connect kamu dapat memperluas pengetahuan dan networking kamu dengan pegiat di industri musik.',
              'Soundfren Connect saat ini berbentuk online webinar interaktif. Sesi Soundfren Connect telah menghadirkan pembicara yang kompeten di bidangnya. Nama-nama besar seperti Danilla, Marchello Tahitoe, Winky Wirawan, Kukuh Rizal, dan nama lainnya telah berbagi langsung di Soundfren Connect.',
              'Sesi yang tersedia di Soundfren Connect membahas topik-topik edukasi praktis tentang bermusik dan hal-hal lainnya yang dapat mendukung kemampuan dan wawasan kamu dalam bermusik.',
            ],
            route: 'SoundconnectScreen',
            buttonTitle: 'Jelajahi Webinar'
          }
        })
      }
    },
    {
      type: 'menu',
      image: require('../../assets/icons/mdi_chat.png'),
      imageStyle: {},
      name: 'Feedback & Report',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('FeedbackScreen')
      }
    },
    {
      type: 'separator'
    },
    {
      type: 'menu',
      image: require('../../assets/icons/close.png'),
      imageStyle: {},
      name: 'Tutup',
      textStyle: {},
      onPress: props.onClose
    },
  ]
  if (props.onShare) menu.splice(1, 0, {
    type: 'menu',
    image: require('../../assets/icons/mdi_share.png'),
    imageStyle: {},
    name: 'Bagikan',
    textStyle: {},
    onPress: () => {
      props.onClose()
      props.onShare()
    },
    joinButton: false,
    isPremiumMenu: false
  })
  return {
    withPromoteUser: true,
    menus: menu
  }
}

export default SC_OPTIONS
