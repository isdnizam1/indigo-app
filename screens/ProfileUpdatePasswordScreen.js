import React from 'react'
import { ScrollView, View, TouchableOpacity, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash-es'
import { setLoading } from 'sf-services/app/actionDispatcher'
import { _enhancedNavigation, Container, Header, Icon, Text, Form, InputTextLight } from '../components'
import { ORANGE_BRIGHT, WHITE } from '../constants/Colors'
import { WP4 } from '../constants/Sizes'
import { TOUCH_OPACITY } from '../constants/Styles'
import { alertMessage } from '../utils/alert'
import { postProfileUpdatePassword } from '../actions/api'
import { authDispatcher } from '../services/auth'
import { clearLocalStorage } from '../utils/storage'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setLoading
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', () => {
  })
})

class ProfileUpdatePasswordScreen extends React.Component {
  _onChangePassword = async ({ formData }) => {
    const {
      userData: { id_user },
      navigateTo,
      dispatch,
      setLoading
    } = this.props

    if (
      isEmpty(formData.currentPassword) ||
      isEmpty(formData.newPassword) ||
      isEmpty(formData.confirmNewPassword)
    ) alertMessage(null, 'Please fill the form correctly')
    else {
      await dispatch(postProfileUpdatePassword, {
        id_user,
        current_password: formData.currentPassword,
        new_password: formData.newPassword,
        confirm_new_password: formData.confirmNewPassword
      })
      await clearLocalStorage()
      navigateTo('LoadingScreen', {}, 'replace')
    }
  }

  render() {
    const {
      isLoading,
      navigateBack
    } = this.props

    return (
      <Container isLoading={isLoading} colors={[WHITE, WHITE]}>
        <Form onSubmit={this._onChangePassword}>
          {({ onChange, onSubmit, formData, isDirty }) => (
            <View>
              <Header>
                <Icon onPress={() => navigateBack()} size='massive' color={ORANGE_BRIGHT} name='close'/>
                <Text size='massive' type='SansPro' weight={500}>Change Password</Text>
                <TouchableOpacity
                  disabled={!isDirty} onPress={() => {
                    Keyboard.dismiss()
                    onSubmit()
                  }} activeOpacity={TOUCH_OPACITY}
                >
                  <Text size='large' color={ORANGE_BRIGHT}>Save</Text>
                </TouchableOpacity>
              </Header>
              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1, paddingHorizontal: WP4 }}
              >
                <InputTextLight
                  secureTextEntry
                  toggleSecureTextEntry
                  label='Current Password'
                  onChangeText={onChange('currentPassword')}
                  value={formData.currentPassword}
                />
                <InputTextLight
                  secureTextEntry
                  toggleSecureTextEntry
                  label='New Password'
                  onChangeText={onChange('newPassword')}
                  value={formData.newPassword}
                />
                <InputTextLight
                  secureTextEntry
                  toggleSecureTextEntry
                  label='Confirm New Password'
                  onChangeText={onChange('confirmNewPassword')}
                  value={formData.confirmNewPassword}
                />
              </ScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProfileUpdatePasswordScreen),
  mapFromNavigationParam
)
