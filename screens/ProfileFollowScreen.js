import React, { Fragment } from 'react'
import { BackHandler, View } from 'react-native'
import { noop, isEmpty, concat } from 'lodash-es'
import { connect } from 'react-redux'
import { TabBar, TabView } from 'react-native-tab-view'
import { NavigationEvents } from '@react-navigation/compat'
import { _enhancedNavigation, Container, Icon, Text, Modal } from '../components'
import { DEFAULT_PAGING, FOLLOW_PAGING } from '../constants/Routes'
import { PALE_BLUE, REDDISH, SHIP_GREY, SHIP_GREY_CALM, WHITE } from '../constants/Colors'
import {
  getProfileDetail,
  getProfileFollowers,
  getProfileFollowing,
  postProfileFollow,
  postProfileUnfollow
} from '../actions/api'
import { WP2, WP4, } from '../constants/Sizes'
import SoundfrenExploreOptions from '../components/explore/SoundfrenExploreOptions'
import ProfileFollowTab from './ProfileFollowTab'
import ArtistSpotlightMenus from './artist_spotlight/ArtistSpotlightMenus'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  onRefresh: getParam('onRefresh', () => {
  }),
  isMine: getParam('isMine', false),
  idUser: getParam('idUser', 0),
  initialTab: getParam('initialTab', 0)
})

class ProfileFollowScreen extends React.Component {
  state = {
    user: {},
    userFollowers: [],
    userFollowing: [],
    isReady: false,
    isRefreshing: false,
    index: this.props.initialTab,
    routes: [
      { key: 'followers', title: 'FOLLOWERS' },
      { key: 'following', title: 'FOLLOWING' }
    ]
  }

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  async componentDidMount() {
    await this._getInitialScreenData()
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _getInitialScreenData = async (...args) => {
    await this._getProfile(...args)
    await this._getFollowers(...args)
    await this._getFollowing(...args)
    this.setState({
      isReady: true,
      isRefreshing: false
    })
  }

  _refreshScreen = () => {
    this.setState({
      user: {},
      userFollowers: [],
      userFollowing: [],
      isReady: false,
    }, async () => {
      await this._getInitialScreenData()
    })
  }

  _getProfile = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const {
      idUser,
      dispatch
    } = this.props
    const dataResponse = await dispatch(getProfileDetail, { id_user: idUser, ...params }, noop, false, isLoading)
    await this.setState({ user: dataResponse })
  }

  _getFollowers = async (params = FOLLOW_PAGING, loadMore = false, isLoading = true) => {
    const {
      userData: { id_user },
      dispatch
    } = this.props
    const {
      user,
      userFollowers
    } = this.state

    if (!params) params = FOLLOW_PAGING

    const dataResponse = await dispatch(getProfileFollowers, {
      id_user: user.id_user,
      id_viewer: id_user, ...params
    }, noop, true, isLoading)

    let newFollower = loadMore ? userFollowers : []
    if (!isEmpty(dataResponse.result)) newFollower = concat(newFollower, dataResponse.result)

    await this.setState({
      userFollowers: newFollower
    })
  }

  _getFollowing = async (params = FOLLOW_PAGING, loadMore = false, isLoading = true) => {
    const {
      userData: { id_user },
      dispatch
    } = this.props
    const {
      user,
      userFollowing
    } = this.state

    if (!params) params = FOLLOW_PAGING

    const dataResponse = await dispatch(getProfileFollowing, {
      followed_by: user.id_user,
      id_viewer: id_user, ...params
    }, noop, true, isLoading)

    let newFollowing = loadMore ? userFollowing : []
    if (!isEmpty(dataResponse.result)) newFollowing = concat(newFollowing, dataResponse.result)

    await this.setState({
      userFollowing: newFollowing
    })
  }

  _onFollowed = async (follow, destinationId, isLoading = true) => {
    const {
      userData: { id_user },
      dispatch
    } = this.props
    const currentAction = follow ? postProfileFollow : postProfileUnfollow
    const currentPayload = follow ? { id_user: destinationId, followed_by: id_user } : {
      id_user,
      id_user_following: destinationId
    }
    await dispatch(currentAction, currentPayload, noop, true, isLoading)
    await this._getProfile(null, null, false)
    await this._getFollowing(null, null, false)
    await this._getFollowers(null, null, false)
  }

  _backHandler = async () => {
    this.props.onRefresh()
    this.props.navigateBack()
  }

  render() {
    const {
      userData,
      navigateTo,
      isLoading,
      isMine
    } = this.props
    const {
      user,
      userFollowing,
      userFollowers,
      isReady
    } = this.state
    return (
      <Container
        scrollable={false}
        onPullDownToRefresh={() => {
          this._getProfile()
          this._getFollowers(null, null, false)
        }}
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: WP4,
            paddingVertical: WP2,
            backgroundColor: WHITE,
            borderBottomWidth: 1,
            borderBottomColor: PALE_BLUE
          }}
          >
            <Icon
              onPress={() => this._backHandler()}
              background='dark-circle'
              size='large'
              color={SHIP_GREY_CALM}
              name='chevron-left'
              type='Entypo'
            />
            <Text type='Circular' size='mini' weight={400} color={SHIP_GREY} centered>{isMine ? 'Your Profile' : user.full_name}</Text>
            <Modal
              position='bottom'
              swipeDirection={null}
              renderModalContent={({ toggleModal }) => {
                return (
                  <SoundfrenExploreOptions
                    menuOptions={ArtistSpotlightMenus}
                    userData={userData}
                    navigateTo={navigateTo}
                    onClose={toggleModal}
                  />
                )
              }}
            >
              {
                ({ toggleModal }, M) => (
                  <Fragment>
                    <Icon
                      centered
                      onPress={toggleModal}
                      background='dark-circle'
                      size='large'
                      color={SHIP_GREY_CALM}
                      name='dots-three-horizontal'
                      type='Entypo'
                    />
                    {M}
                  </Fragment>
                )
              }
            </Modal>
          </View>
        )}
      >
        <NavigationEvents
          onWillFocus={() => this._refreshScreen()}
        />
        <TabView
          navigationState={this.state}
          renderScene={
            ({ route }) => {
              switch (route.key) {
              case 'followers':
                return (
                  <ProfileFollowTab
                    type='followers'
                    refreshProfile={this._getProfile}
                    refresh={this._getFollowers}
                    onFollow={this._onFollowed}
                    dataSource={userFollowers}
                    idViewer={userData.id_user} isMine={isMine} navigateTo={navigateTo} user={user}
                  />
                )
              case 'following':
                return (
                  <ProfileFollowTab
                    type='following'
                    refreshProfile={this._getProfile}
                    refresh={this._getFollowing}
                    onFollow={this._onFollowed}
                    dataSource={userFollowing}
                    idViewer={userData.id_user} isMine={isMine} navigateTo={navigateTo} user={user}
                    withEmptyScreen
                  />
                )
              }
            }
          }
          renderTabBar={(props) => (
            <TabBar
              {...props}
              style={{ backgroundColor: WHITE, elevation: 0, borderBottomWidth: 1, borderBottomColor: PALE_BLUE, }}
              indicatorStyle={{ backgroundColor: REDDISH, height: 2 }}
              renderLabel={({ route, focused, color }) => {
                return (
                  <Text type='Circular' centered color={focused ? REDDISH : SHIP_GREY_CALM} size={'xmini'} weight={600}>{`${route.key === 'followers' ? user.total_followers ? user.total_followers : '' : user.total_following ? user.total_following : ''} ${route.title}`}</Text>
                )
              }}
            />
          )}
          onIndexChange={(index) => this.setState({ index })}
        />
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(ProfileFollowScreen),
  mapFromNavigationParam
)
