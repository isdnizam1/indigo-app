import React from 'react'
import { BackHandler, Keyboard, Platform, ScrollView, StatusBar, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { forEach, isEmpty, noop } from 'lodash-es'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import * as yup from 'yup'
import { setClearBlueMessage } from 'sf-services/messagebar/actionDispatcher'
import { _enhancedNavigation, Container, Form, Icon, InputTextLight, ListItem, Modal, Text, Image, Card } from '../components'
import { ORANGE_BRIGHT, WHITE, SHIP_GREY, GUN_METAL, REDDISH, SHIP_GREY_CALM, SILVER_TWO, PALE_WHITE, PALE_BLUE, PALE_SALMON } from '../constants/Colors'
import { HP1, HP2, WP05, WP100, WP15, WP2, WP3, WP4, WP5, WP60, WP8, WP90 } from '../constants/Sizes'
import { HEADER, SHADOW_STYLE, TEXT_INPUT_STYLE, TOUCH_OPACITY } from '../constants/Styles'
import { postProfileAddArt, putProfileEditArt } from '../actions/api'
import { authDispatcher } from '../services/auth'
import { convertBlobToBase64, fetchAsBlob } from '../utils/helper'
import HeaderNormal from '../components/HeaderNormal'
import ButtonV2 from '../components/ButtonV2'
import { getFileSize } from '../utils/file'
import { formatBytes } from '../utils/upload'
import { setAddProfile } from '../utils/review'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setClearBlueMessage
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', noop),
  initialValues: getParam('initialValues', {})
})

export const FORM_VALIDATION = yup.object().shape({
  title: yup.string().required(),
  description: yup.string().required(),
})

class ProfileAddArtScreen extends React.Component {
  _didFocusSubscription
  _willBlurSubscription

  constructor(props) {
    super(props)

    this._backHandler = this._backHandler.bind(this)
    this._didFocusSubscription = props.navigation.addListener('focus', () =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler)
    )
  }

  state = {
    images: [],
    imagesName: [],
    imagesBase64: [],
    imageSize: 0,
    step: 1,
    isUpload: false,
    isUploadOver: false,
    isUploading: false,
  }

  async componentDidMount() {
    const {
      initialValues
    } = this.props
    if (!isEmpty(initialValues)) {
      const images = []
      const imagesBase64 = []
      const imagesName = []
      forEach(initialValues, async (art) => {
        const [name] = art.attachment_file.split('_').slice(-1)
        images.push(art.attachment_file)
        imagesName.push(name)
        imagesBase64.push(await fetchAsBlob(art.attachment_file).then(convertBlobToBase64))
      })
      this.setState({
        images,
        imagesName,
        imagesBase64
      })
    }

    this._willBlurSubscription = this.props.navigation.addListener('blur', () =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
    )
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _backHandler = async () => {
    const {
      step
    } = this.state
    if(step !== 1) {
      this.setState({ step: 1 })
    } else {
      const { navigateBack } = this.props
      navigateBack()
    }
  }

  _onAddArt = async ({ formData }) => {
    const {
      userData: { id_user },
      navigateBack,
      refreshProfile,
      dispatch,
      initialValues,
      getUserDetailDispatcher
    } = this.props
    const action = isEmpty(initialValues) ? postProfileAddArt : putProfileEditArt
    const {
      imagesName,
      imagesBase64
    } = this.state

    this.setState({ isUploading: true }, async () => {
      await dispatch(action, {
        id_user,
        id_journey: initialValues[0]?.id_journey,
        title: formData.title,
        description: formData.description,
        image: imagesBase64,
        image_name: imagesName
      })
      this.setState({ isUploading: false })
      getUserDetailDispatcher({ id_user })
      this.props.setClearBlueMessage('Photo berhasil ditambahkan')
      await setAddProfile()
      refreshProfile()
      navigateBack()
    })
  }

  _selectPhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCameraRoll === 'granted') {
      return await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
    }
  }

  _takePhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
      return await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
    }
  }

  _deleteImage = (index) => {
    const {
      images,
      imagesName,
      imagesBase64
    } = this.state
    imagesName.splice(index, 1)
    images.splice(index, 1)
    imagesBase64.splice(index, 1)
    this.setState({
      imagesName,
      images,
      imagesBase64
    })
  }

  _handlePhoto = async (result) => {
    if (Platform.OS === 'ios') StatusBar.setHidden(false)
    const {
      uri,
      base64,
      cancelled
    } = result

    if (!result || cancelled) {
      return
    }

    const fileSize = await getFileSize(uri)

    if (!cancelled) this.setState((state) => {
      let name = result.uri.split('/')
      name = name[name.length - 1]

      return ({
        images: [uri],
        imagesBase64: [base64],
        imagesName: [name],
        isUpload: true,
        imageSize: fileSize
      })
    })

    if(fileSize > 10485760) {
      this.setState({ isUploadOver: true, isUpload: false })
    } else {
      this.setState({ isUploadOver: false })
    }
  }

  render() {
    const {
      initialValues,
      isLoading,
    } = this.props

    const {
      images,
      step,
      isUpload,
      isUploadOver,
      isUploading,
      imagesName,
      imageSize
    } = this.state
    return (
      <Container isLoading={isLoading} colors={[WHITE, WHITE]}>
        <Form initialValue={initialValues[0]} onSubmit={this._onAddArt} validation={FORM_VALIDATION}>
          {({ onChange, onSubmit, formData, isValid }) => (
            <View>
              <HeaderNormal
                iconLeftOnPress={this._backHandler}
                text={`${isEmpty(initialValues) ? 'Upload' : 'Edit'} Photo`}
                textType='Circular'
                textColor={SHIP_GREY}
                textWeight={400}
                withDummyRightIcon
                rightComponent={(
                  <>
                  {step === 2 && !isUploading && <TouchableOpacity
                    onPress={() => {
                      Keyboard.dismiss()
                      onSubmit()
                    }}
                    activeOpacity={TOUCH_OPACITY}
                    disabled={!isValid}
                    style={HEADER.rightIcon}
                                                 >
                    <Text size='small' weight={400} color={isValid ? ORANGE_BRIGHT : PALE_SALMON}>Upload</Text>
                  </TouchableOpacity>}
                  {step === 2 && isUploading &&
                    <Image
                      spinning
                      style={HEADER.rightIcon}
                      source={require('../assets/icons/ic_loading.png')}
                      imageStyle={{ width: WP8, aspectRatio: 1 }}
                    />}
                </>
                )}
                centered
              />

              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1, paddingHorizontal: WP3, backgroundColor: PALE_WHITE }}
              >
                {step === 1 && <Modal
                  renderModalContent={({ toggleModal }) => (
                    <View>
                      <ListItem
                        inline onPress={() => {
                          this._selectPhoto().then((result) => {
                            toggleModal()
                            this._handlePhoto(result)
                          })
                        }}
                      >
                        <Icon type='MaterialIcons' size='large' name='photo-library'/>
                        <Text style={{ paddingLeft: WP2 }}>Select from library</Text>
                      </ListItem>
                      <ListItem
                        inline onPress={() => {
                          this._takePhoto().then((result) => {
                            toggleModal()
                            this._handlePhoto(result)
                          })
                        }}
                      >
                        <Icon size='large' name='camera'/>
                        <Text style={{ paddingLeft: WP2 }}>Take a picture</Text>
                      </ListItem>
                    </View>
                  )}
                               >
                  {({ toggleModal }, M) => (
                    <View style={{ paddingVertical: HP1, alignItems: 'center' }}>
                      <Image
                        onPress={() => {
                          if(!isEmpty(initialValues)) {
                            toggleModal()
                          }
                        }}
                        source={!isEmpty(images) ? { uri: images[0] } : require('../assets/images/uploadPhotoPlaceholder.png')}
                        imageStyle={{ width: WP90, aspectRatio: !isEmpty(images) ? 1 : 360/376,
                          borderRadius: 15, overflow: 'hidden' }}
                        style={{ width: WP90, marginVertical: HP2 }}
                      />
                      <Text type='Circular' size='large' weight={500} color={GUN_METAL}>Upload Photo / Artwork</Text>
                      <Text type='Circular' size='small' centered color={SHIP_GREY} style={{ margin: HP1 }}>Bagikan foto kegiatan bermusikmu atau artwork karyamu disini.</Text>
                      <ButtonV2
                        onPress={() => {
                          if(isUpload || !isEmpty(initialValues)) {
                            this.setState({ step: 2 })
                          } else {
                            toggleModal()
                          }
                        }}
                        color={REDDISH}
                        textColor={WHITE}
                        style={{ paddingHorizontal: WP5, width: WP60, marginVertical: HP2 }}
                        text={isUpload || !isEmpty(initialValues) ? 'Selanjutnya' : 'Upload Photo'}
                      />
                      {isUploadOver && <Text type='Circular' size='mini' centered color={SHIP_GREY_CALM}>Failed, too big image</Text>}
                      <Text type='Circular' size='mini' centered color={SHIP_GREY_CALM}>{isUpload ? 'Photo berhasil diupload' : 'Upload file max. 10 mb (jpg, jpeg, png)'}</Text>
                      {M}
                    </View>
                  )}
                </Modal>}
                {step === 2 && <View>
                  <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginVertical: HP2 }}>UPLOADED FILES</Text>
                  <Card style={[{ borderRadius: 5, marginVertical: HP2 }, SHADOW_STYLE['shadowThin']]}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        flex: 1,
                        justifyContent: 'space-between',
                      }}
                    >
                      <View
                        style={{
                          flexDirection: 'row',
                          flexShrink: 1,
                          alignItems: 'center',
                        }}
                      >
                        <View style={{ marginRight: WP4 }}>
                          <Image
                            source={!isEmpty(images) ? { uri: images[0] } : require('../assets/images/bgSoundsEdu.png')}
                            imageStyle={{ width: WP15, aspectRatio: 1,
                              borderRadius: 12, overflow: 'hidden' }}
                            style={{ width: WP15 }}
                          />
                        </View>
                        <View style={{ flexShrink: 1 }}>
                          <Text
                            style={{ marginBottom: WP05 }}
                            color={GUN_METAL}
                            weight={400}
                            size='small'
                            type='Circular'
                          >
                            {imagesName[0]}
                          </Text>
                          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text type='Circular' size='mini' color={SHIP_GREY_CALM}>
                              Photo
                            </Text>
                            <Text size='tiny' color={SHIP_GREY_CALM}>{' • '}</Text>
                            <Text type='Circular' size='mini' color={SHIP_GREY_CALM}>
                              {formatBytes(imageSize, true)}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </Card>
                  <View style={{ width: WP100, height: 1, backgroundColor: PALE_BLUE, marginVertical: HP2, marginLeft: -WP3 }} />
                  <InputTextLight
                    onChangeText={onChange('title')}
                    value={formData.title}
                    placeholder={'Tuliskan judul fotomu'}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    multiline
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Photo Title'
                  />
                  <InputTextLight
                    onChangeText={onChange('description')}
                    value={formData.description}
                    placeholder={'Deskripsikan fotomu'}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLength={90}
                    maxLengthFocus
                    multiline
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Caption'
                  />
                </View>}
              </ScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

ProfileAddArtScreen.navigationOptions = () => ({
  gesturesEnabled: false
})
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProfileAddArtScreen),
  mapFromNavigationParam
)
