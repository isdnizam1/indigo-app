import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import Container from '../components/Container'
import { GREY, ORANGE, WHITE } from '../constants/Colors'
import Text from '../components/Text'
import _enhancedNavigation from '../navigation/_enhancedNavigation'
import { WP2, WP4, WP6, WP90 } from '../constants/Sizes'
import Image from '../components/Image'
import Button from '../components/Button'
import HeaderNormal from '../components/HeaderNormal'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({})

class PromoteBandScreen extends Component {

  _backHandler = async () => {
    this.props.navigateBack()
  }

  render() {
    const {
      navigateTo
    } = this.props

    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._backHandler}
            text='Promote Artist'
            centered
          />
        )}
        scrollable
      >
        <Image
          source={require('../assets/images/promoteArtist.png')}
          style={{ marginBottom: WP4 }}
          imageStyle={{ width: WP90 }}
          aspectRatio={2}
        />

        <View style={{ paddingHorizontal: WP6 }}>
          <Text color={ORANGE} weight={500} size='extraMassive' style={{ marginBottom: 10 }}>
            Hello !
          </Text>
          <Text color={GREY} size='slight' style={{ marginBottom: 13 }}>
            Welcome to promote artist, we feel so happy that we can help to get the world know more about you!
          </Text>
          <Text color={GREY} size='slight' style={{ marginBottom: 13 }}>
            First, please click the button bellow to create you Artist Page and fill some information about you and your
            music.
          </Text>
          <View style={{ flexDirection: 'row', marginTop: 10 }}>

            <Button
              onPress={() => {
                navigateTo('CreateArtistPage1')
              }}
              contentStyle={{ paddingHorizontal: WP6, paddingVertical: WP2, flexGrow: 0 }}
              soundfren
              rounded
              centered
              text='CREATE ARTIST PAGE'
              textColor={WHITE}
              textSize='small'
              textWeight={400}
              shadow='none'
            />
          </View>
        </View>

      </Container>
    )
  }
}

PromoteBandScreen.propTypes = {}

PromoteBandScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(PromoteBandScreen),
  mapFromNavigationParam
)
