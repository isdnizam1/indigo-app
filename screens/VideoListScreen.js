import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FlatList, View } from 'react-native'
import { SAPPHIRE, WHITE } from '../constants/Colors'
import { Container, Image, Text } from '../components'
import { WP1, WP100, WP3, WP50, WP6, WP8 } from '../constants/Sizes'
import _enhancedNavigation from '../navigation/_enhancedNavigation'
import { getAds } from '../actions/api'
import AdsVideoItem from '../components/ads/AdsVideoItem'
import HeaderNormal from '../components/HeaderNormal'

const mapStateToProps = ({ auth, message }) => ({
  userData: auth.user,
  newMessageCount: message.newMessageCount
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class VideoListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ads: [],
      start: 0,
      limit: 50,
      isReady: false,
    }
  }

  componentDidMount = async () => {
    await this._getAdList()
    this.setState({
      isReady: true
    })
    // BackHandler.addEventListener('hardwareBackPress', this.props.navigateBack)
  }

  componentWillUnmount() {
    // BackHandler.removeEventListener('hardwareBackPress', this.props.navigateBack)
  }

  _getAdList = async (params) => {
    const {
      start,
      limit
    } = this.state

    try {
      const { data } = await getAds({
        ...params,
        status: 'active',
        category: 'video',
        start,
        limit
      })

      if (data.code === 200) {
        this.setState({
          ads: data.result,
        })
      } else if (data.code === 500 && data.message === 'Ads is empty') {
        this.setState({
          ads: [],
        })
      }
    } catch (e) {
      //
    }
  }

  _onPullDownToRefresh = () => {
    this._getAdList
  }

  render() {
    const {
      navigateBack,
      navigateTo,
      isLoading
    } = this.props
    const {
      ads,
      isReady,
    } = this.state
    return (
      <Container
        theme='light'
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={this._onPullDownToRefresh}
        isReady={isReady}
        isLoading={isLoading}
        colors={[SAPPHIRE, SAPPHIRE]}
      >
        <View>
          <Image
            imageStyle={{ width: WP100, height: undefined }}
            aspectRatio={320 / 220}
            source={require('../assets/images/bgSoundsEdu.png')}
          />
          <View style={{ position: 'absolute', left: 0, top: 0, right: 0, bottom: 0 }}>
            <HeaderNormal
              iconLeftOnPress={navigateBack}
              text='Soundfren Edu'
              centered
              textColor={WHITE}
              iconLeftColor={WHITE}
            />
            <View style={{ flex: 1, justifyContent: 'center', paddingBottom: WP3, paddingHorizontal: WP8 }}>
              <Text size='medium' weight={500} color={WHITE}>Upgrade your skill</Text>
              <Text size='medium' color={WHITE}>with Soundfren Edu!</Text>
            </View>
          </View>
        </View>
        <FlatList
          contentContainerStyle={{
            paddingTop: WP6,
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'flex-start'
          }}
          ListEmptyComponent={
            <View style={{ width: '100%', alignItems: 'center' }}>
              <Image
                style={{ marginBottom: WP3 }}
                source={require('../assets/images/empty_experience.png')}
                imageStyle={{ width: WP50, height: undefined }}
                aspectRatio={140 / 152}
                centered
              />
              <View>
                <Text centered size='small' type='NeoSans' style={{ marginBottom: WP1, marginTop: 0 }}>Hey, it’s empty here,</Text>
                <Text centered size='small' type='NeoSans' weight={500}>Come back soon.</Text>
              </View>
            </View>
          }
          data={ads}
          keyExtractor={(item, index) => `key${index}`}
          renderItem={({ item, index }) => (
            <AdsVideoItem
              ads={item}
              key={`${index}`}
              navigateTo={navigateTo}
            />
          )}
        />
      </Container>
    )
  }
}

VideoListScreen.propTypes = {
  navigateTo: PropTypes.func
}

VideoListScreen.defaultProps = {
  navigateTo: () => {
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(VideoListScreen),
  mapFromNavigationParam
)
