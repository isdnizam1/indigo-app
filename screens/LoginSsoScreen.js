/* eslint-disable react/sort-comp */
import React from "react";
import {
  Animated,
  TextInput,
  View,
  TouchableOpacity,
  Dimensions,
  Keyboard,
  BackHandler,
  ImageBackground,
} from "react-native";
import { isEmpty, isNil } from "lodash-es";
import { connect } from "react-redux";
import { Text, _enhancedNavigation, Container } from "sf-components";
import HeaderNormal from "sf-components/HeaderNormal";
import Touchable from "sf-components/Touchable";
import Icon from "sf-components/Icon";
import Spacer from "sf-components/Spacer";
import ButtonV2 from "sf-components/ButtonV2";
import {
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
  SHADOW_GRADIENT,
  NAVY_DARK,
  REDDISH,
  PALE_BLUE,
  LIPSTICK_TWO,
} from "sf-constants/Colors";
import { LinearGradient } from "expo-linear-gradient";
import { HEADER } from "sf-constants/Styles";
import { WP1, WP2, WP3, WP4, WP5, WP8, WP80 } from "sf-constants/Sizes";
import style from "sf-styles/register";
import {
  convertBlobToBase64,
  fetchAsBlob,
  isTabletOrIpad,
} from "sf-utils/helper";
import { postLogin, getCity, postCheckEmail } from "sf-actions/api";
import {
  setUserId,
  setAuthJWT,
  getUserEmail,
  setUserEmail,
  clearLocalStorage,
} from "sf-utils/storage";
import { GET_USER_DETAIL } from "sf-services/auth/actionTypes";
import { SET_REGISTRATION } from "sf-services/register/actionTypes";
import { registerDispatcher } from "sf-services/register";
import { isEmailValid } from "sf-utils/helper";
import { setLoading } from "sf-services/app/actionDispatcher";
import {
  GUN_METAL,
  PALE_LIGHT_BLUE_TWO,
  WHITE_MILK,
} from "../constants/Colors";
import { HP05, HP1, HP10, HP50, WP10, WP100, WP15 } from "../constants/Sizes";
import Wrapper from "./register/v3/Wrapper";
import { postLogin_V2 } from "../actions/api";
import { BlurView } from "expo-blur";
import { SafeAreaView } from "react-native-safe-area-context";
import ConfirmationModalV3 from "../components/ConfirmationModalV3";

const { height } = Dimensions.get("window");

const mapStateToProps = ({ register }) => ({
  register,
});

const mapDispatchToProps = {
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail,
  }),
  resetRegisterData: () => registerDispatcher.resetData(),
  setRegistration: (data) => ({
    type: SET_REGISTRATION,
    response: data,
  }),
  setLoading,
};

const mapFromNavigationParam = (getParam) => ({
  isLoginMMB: getParam("isLoginMMB", false),
});

class LoginSsoScreen extends React.Component {
  fadeIn = new Animated.Value(0);

  constructor(props) {
    super(props);
    this.state = {
      simpleDialogVisible: false,
      errorMessage: null,
      showPassword: false,
      marginTop: 0,
      email: null,
      password: null,
      isLoading: false,
      invalidPasswordCount: 0,
      forgotModalVisible: false,
      signupModalVisible: false,
      emailError: null,
      passwordError: null,
      keyboardVisible: false,
      mobile: true,
      modalVisible: false,
      dataResponse: [],
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this._registrationStep = this._registrationStep.bind(this);
    this.onLayout = this.onLayout.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
  }

  async getBase64(url) {
    let result = await fetchAsBlob(url);
    let base64 = await convertBlobToBase64(result);
    return base64;
  }

  onLayout({ nativeEvent }) {
    const viewHeight = nativeEvent.layout.height;
    this.setState(
      {
        marginTop: (height - viewHeight) / 2 - 85,
      },
      () => {
        Animated.timing(this.fadeIn, {
          toValue: 1,
          duration: 50,
          useNativeDriver: false,
        }).start();
      }
    );
  }

  toggleModal() {
    this.setState({ simpleDialogVisible: !this.state.simpleDialogVisible });
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide
    );
    this.getEmail();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }
  _renderText = (isBold, text) => {
    return (
      <Text
        // type={'NeoSans'}
        weight={isBold ? 700 : 400}
        size={"xmini"}
        color={GUN_METAL}
        type={"Circular"}
      >
        {text}
      </Text>
    );
  };

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({ keyboardVisible: true });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardVisible: false });
  };

  _renderHeader = () => {
    return (
      <View>
        <HeaderNormal
          style={style.headerNormal}
          centered
          iconLeftOnPress={this.props.navigateBack}
          textType={"Circular"}
          textSize={"mini"}
          iconStyle={{ marginRight: WP5 }}
          text={"Login dengan SSO"}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
      </View>
    );
  };

  getEmail = async () => {
    try {
      const userEmail = await getUserEmail();
      if (userEmail !== null) {
        this.setState({ email: userEmail });
      } else {
        this.setState({ email: null });
      }
    } catch (e) {
      //
    }
  };

  onSubmit() {
    const {
      dispatch,
      register: { data, behaviour },
      setUserDetailDispatcher,
      navigateTo,
      resetRegisterData,
      setRegistration,
    } = this.props;
    const { email, password, mobile } = this.state;

    if (!isEmailValid(email)) {
      this.setState({
        modalVisible: false,
        simpleDialogVisible: true,
        emailError: "Email belum terdaftar. Daftar sekarang!",
      });
    } else {
      this.setState(
        {
          modalVisible: false,
          isLoading: true,
        },
        () => {
          postLogin_V2({ email, password, platform: "indigo", mobile })
            .then(async (response) => {
              console.log(response);
              const { status, result } = response.data;
              if (status === "failed") {
                postCheckEmail({ email }).then(({ data: { status } }) => {
                  if (status === "success") {
                    if (this.state.invalidPasswordCount < 1) {
                      this.setState({
                        simpleDialogVisible: true,
                        passwordError: "Password kamu salah. Lupa password?",
                        invalidPasswordCount:
                          this.state.invalidPasswordCount + 1,
                      });
                    } else {
                      this.setState({ forgotModalVisible: true });
                    }
                  } else {
                    this.setState({
                      emailError: "Email belum terdaftar. Daftar Sekarang!",
                    });
                  }
                });
              } else {
                const dataResponse = result;
                let city_name = null;
                let {
                  registration_step,
                  id_user,
                  email,
                  id_city,
                  full_name,
                  profile_type,
                  profile_picture,
                  registration_version,
                  email_status,
                } = dataResponse;

                const isV3 = registration_version == "3.0";
                if (registration_step !== "finish") {
                  this.setState({ modalVisible: true, dataResponse: result });
                  // try {
                  //   if (!isEmpty(profile_picture)) {
                  //     profile_picture = await this.getBase64(profile_picture);
                  //   }
                  // } catch (err) {
                  //   // better keep silent
                  // } finally {
                  //   const step = parseInt(registration_step);

                  //   if (!isNil(id_city)) {
                  //     const cities = await dispatch(getCity);
                  //     const city = cities.filter(
                  //       (city) => parseInt(city.id_city) == parseInt(id_city)
                  //     )[0];
                  //     city_name = city.city_name;
                  //   }
                  //   Promise.resolve(
                  //     setRegistration({
                  //       behaviour: {
                  //         ...behaviour,
                  //         step: !isV3
                  //           ? 3
                  //           : step < 3
                  //           ? email_status === "waiting"
                  //             ? 2
                  //             : 3
                  //           : step + 1,
                  //       },
                  //       data: {
                  //         ...data,
                  //         id_user,
                  //         id_city,
                  //         city_name,
                  //         email,
                  //         profile_type,
                  //         profile_picture,
                  //         name: full_name,
                  //       },
                  //     })
                  //   ).then(() => {
                  //     this.setState(
                  //       {
                  //         disable: false,
                  //         isLoading: false,
                  //       },
                  //       () => {
                  //         navigateTo("RegisterV3");
                  //       }
                  //     );
                  //   });
                  // }
                } else {
                  try {
                    await clearLocalStorage();
                  } catch (e) {
                    //
                  }
                  await setUserId(result.id_user);
                  await setAuthJWT(result.token);
                  await setUserEmail(email);
                  resetRegisterData();
                  setUserDetailDispatcher(result);
                  navigateTo("LoadingScreen", {}, "replace");
                }
              }
            })
            .catch(() => {})
            .then(() => this.setState({ isLoading: false }));
        }
      );
    }
  }

  _registrationStep = async () => {
    const { dataResponse } = this.state;
    const {
      dispatch,
      register: { data, behaviour },
      navigateTo,
      setRegistration,
    } = this.props;
    let city_name = null;
    let {
      registration_step,
      id_user,
      email,
      id_city,
      full_name,
      profile_type,
      profile_picture,
      registration_version,
      email_status,
    } = dataResponse;
    const isV3 = registration_version == "3.0";

    try {
      if (!isEmpty(profile_picture)) {
        profile_picture = await this.getBase64(profile_picture);
      }
    } catch (err) {
      // better keep silent
    } finally {
      const step = parseInt(registration_step);

      if (!isNil(id_city)) {
        const cities = await dispatch(getCity);
        const city = cities.filter(
          (city) => parseInt(city.id_city) == parseInt(id_city)
        )[0];
        city_name = city.city_name;
      }
      Promise.resolve(
        setRegistration({
          behaviour: {
            ...behaviour,
            step: !isV3
              ? 3
              : step < 3
              ? email_status === "waiting"
                ? 2
                : 3
              : step + 1,
          },
          data: {
            ...data,
            id_user,
            id_city,
            city_name,
            email,
            profile_type,
            profile_picture,
            name: full_name,
          },
        })
      ).then(() => {
        this.setState(
          {
            modalVisible: false,
            disable: false,
            isLoading: false,
          },
          () => {
            navigateTo("RegisterV3");
          }
        );
      });
    }
  };

  _renderInformationMMB = () => {
    return (
      <View
        style={{
          flexDirection: "row",
          borderRadius: WP3,
          backgroundColor: WHITE_MILK,
          marginBottom: WP5,
          paddingVertical: WP2,
          paddingHorizontal: WP4,
        }}
      >
        <Text style={{ flexShrink: 1, textAlign: "justify", lineHeight: WP5 }}>
          {this._renderText(false, "Jika anda mengikuti program ")}
          {this._renderText(true, "Muda Maju Bersama 1000 Startup, ")}
          {this._renderText(
            false,
            "masukkan email dengan yang anda daftarkan di "
          )}
          {this._renderText(true, "GoSmart ")}
          {this._renderText(false, "dan masukkan ")}
          {this._renderText(true, "Unique Code ")}
          {this._renderText(
            false,
            "yang anda dapat dari email pada kolom password."
          )}
        </Text>
      </View>
    );
  };

  render() {
    const { navigateTo, isLoginMMB } = this.props;
    const { width, height } = Dimensions.get("window");
    const {
      email,
      emailError,
      password,
      passwordError,
      showPassword,
      isLoading,
      modalVisible,
    } = this.state;
    return (
      <Container renderHeader={this._renderHeader} isLoading={isLoading}>
        <View style={{ flex: 1, width, height }}>
          <ImageBackground
            style={{
              flex: 1,
              width,
              height,
              resizeMode: "cover",
              justifyContent: "space-between",
            }}
            source={require("sf-assets/images/indigo/LoginSsoBg.png")}
          >
            <SafeAreaView style={{ flex: 1 }}>
              <View
                style={{
                  width,
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    marginBottom: HP10 * 0.92,
                    width,
                    justifyContent: "center",
                    alignItems: "center",
                    position: "relative",
                    borderRadius: 8,
                  }}
                >
                  <BlurView
                    tint="light"
                    intensity={90}
                    style={{
                      position: "absolute",
                      height: HP50,
                      width: WP100 * 0.95,
                      borderRadius: 8,
                    }}
                  />
                  <View style={{ width: WP100 * 0.9 }}>
                    <Spacer size={WP2} />
                    <Text
                      color={NAVY_DARK}
                      size={isTabletOrIpad() ? "mini" : "medium"}
                      weight={600}
                      type={"Circular"}
                    >
                      Selamat Datang!
                    </Text>
                    <Spacer size={WP1} />
                    <Text
                      style={style.label}
                      size={isTabletOrIpad() ? "tiny" : "xmini"}
                      weight={400}
                      type={"Circular"}
                    >
                      Login mudah dengan akun Indigo mu
                    </Text>
                    <Spacer size={WP8} />
                    {/* {this._renderInformationMMB()} */}
                    <View style={style.formGroup}>
                      <Text
                        style={style.label}
                        size={isTabletOrIpad() ? "tiny" : "xmini"}
                        weight={400}
                        type={"Circular"}
                      >
                        Username / Email
                      </Text>
                      <TextInput
                        defaultValue={email}
                        onChangeText={(email) =>
                          this.setState({ email, emailError: null })
                        }
                        keyboardType={"email-address"}
                        autoCompleteType={"email"}
                        placeholder={"Masukkan Username atau Email kamu"}
                        style={[
                          style.input,
                          emailError ? style.inputError : null,
                        ]}
                      />
                      {!isEmpty(emailError) && !isEmpty(email) && (
                        <TouchableOpacity
                          onPress={() => {
                            emailError.split(". ")[0] ===
                              "Email belum terdaftar" &&
                              this.props.navigateTo("RegisterV3");
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "row",
                              alignItems: "center",
                            }}
                          >
                            <Text
                              size={isTabletOrIpad() ? "petite" : "tiny"}
                              color={LIPSTICK_TWO}
                              weight={400}
                              type={"Circular"}
                            >
                              {emailError.split(". ")[0]}.{" "}
                              <Text
                                size={isTabletOrIpad() ? "petite" : "tiny"}
                                color={LIPSTICK_TWO}
                                weight={400}
                                style={{ textDecorationLine: "underline" }}
                                type={"Circular"}
                              >
                                {emailError.split(". ")[1]}
                              </Text>
                            </Text>
                          </View>
                        </TouchableOpacity>
                      )}
                      {/*!isEmpty(errorMessage) && (<Text
                  size={'tiny'}
                  color={TOMATO_CALM}
                  weight={500}
                  type={'NeoSans'}
                  >
                  {errorMessage}
                </Text>)*/}
                    </View>
                    <View style={style.formGroup}>
                      <View style={{ flexDirection: "row" }}>
                        <Text
                          style={style.label}
                          size={isTabletOrIpad() ? "tiny" : "xmini"}
                          weight={400}
                          type={"Circular"}
                        >
                          {isLoginMMB ? "Passcode" : "Password"}
                        </Text>
                        {isLoginMMB && (
                          <Text
                            style={{
                              ...style.label,
                              color: PALE_LIGHT_BLUE_TWO,
                              marginLeft: WP1,
                            }}
                            size={isTabletOrIpad() ? "tiny" : "xmini"}
                            weight={400}
                            type={"Circular"}
                          >
                            Silahkan cek email kamu
                          </Text>
                        )}
                      </View>
                      <View>
                        <TextInput
                          defaultValue={password}
                          onChangeText={(password) =>
                            this.setState({ password, passwordError: null })
                          }
                          secureTextEntry={!showPassword}
                          autoCompleteType={"password"}
                          placeholder={
                            isLoginMMB
                              ? "6 digit angka unik di email kamu"
                              : "Masukkan Password kamu"
                          }
                          style={style.input}
                        />
                        <Touchable
                          onPress={() =>
                            this.setState({ showPassword: !showPassword })
                          }
                          style={style.passwordToggler}
                        >
                          <Icon
                            color={SHIP_GREY_CALM}
                            type={"MaterialCommunityIcons"}
                            name={showPassword ? "eye-off" : "eye"}
                          />
                        </Touchable>
                        {!isEmpty(passwordError) && !isEmpty(password) && (
                          <TouchableOpacity
                            onPress={() => {
                              passwordError.split(". ")[0] ===
                                "Password kamu salah" &&
                                this.props.navigateTo("ForgotPasswordScreen");
                            }}
                          >
                            <View
                              style={{
                                flexDirection: "row",
                                alignItems: "center",
                              }}
                            >
                              <Text
                                size={isTabletOrIpad() ? "petite" : "tiny"}
                                color={LIPSTICK_TWO}
                                weight={400}
                                type={"Circular"}
                              >
                                {passwordError.split(". ")[0]}.{" "}
                                <Text
                                  size={isTabletOrIpad() ? "petite" : "tiny"}
                                  color={LIPSTICK_TWO}
                                  weight={400}
                                  style={{ textDecorationLine: "underline" }}
                                  type={"Circular"}
                                >
                                  {passwordError.split(". ")[1]}
                                </Text>
                              </Text>
                            </View>
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                    {!passwordError && (
                      <TouchableOpacity
                        onPress={() => navigateTo("ForgotPasswordScreen")}
                        style={{
                          alignItems: "flex-end",
                          width: 120,
                          marginLeft: "auto",
                          marginTop: -18,
                          paddingVertical: WP2,
                          marginBottom: WP5,
                        }}
                      >
                        <Text
                          centered
                          // type={"NeoSans"}
                          weight={500}
                          size={"xmini"}
                          color={SHIP_GREY_CALM}
                          type={"Circular"}
                        >
                          Lupa password?
                        </Text>
                      </TouchableOpacity>
                    )}
                    <Spacer size={passwordError ? WP8 : WP3} />
                    <View style={{ marginBottom: WP3 }}>
                      <ButtonV2
                        style={{ paddingVertical: WP4 }}
                        textSize={"slight"}
                        text="Masuk"
                        onPress={this.onSubmit}
                        disabled={!password || !email}
                        textColor={WHITE}
                        color={REDDISH}
                      />
                    </View>
                  </View>
                </View>
              </View>
            </SafeAreaView>
          </ImageBackground>
          <ConfirmationModalV3
            visible={modalVisible}
            title={"Kamu belum mengisi data diri akun"}
            titleSize={"large"}
            titleWeight={600}
            subtitle="Harap melengkapi data diri akun terlebih dahulu"
            primary={{
              text: "Lengkapi Sekarang",
              onPress: this._registrationStep,
            }}
          />
        </View>
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(LoginSsoScreen),
  mapFromNavigationParam
);
