import React from 'react'
import { View, ScrollView } from 'react-native'
import { isEmpty } from 'lodash-es'
import { Text, Container, Button, Header, Icon, Form, CodeConfirmationInput, _enhancedNavigation } from '../../components'
import { ORANGE_BRIGHT_DARK, PINK_RED_DARK, WHITE, WHITE20 } from '../../constants/Colors'
import { WP5, HP5, FONT_SIZE, HP20, WP10 } from '../../constants/Sizes'
import { alertMessage } from '../../utils/alert'
import { postInputForgotPassword } from '../../actions/api'

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  email: getParam('email', '')
})

const defaultProps = {
  initialLoaded: true,
  isLoading: false
}

class LoginForgotInputCodeScreen extends React.Component {
  constructor(props) {
    super(props)
  }

    state = {}

    static navigationOptions = ({ navigation }) => ({
      gesturesEnabled: !navigation.getParam('initialLoaded', false),
    })

    static getDerivedStateFromProps(props, state) {
      return null
    }

    componentDidMount = () => { }

    _onForgotPassword = ({ formData }) => {
      const {
        dispatch,
        navigateTo,
        email
      } = this.props

      if (
        isEmpty(formData.forgotten_password_code) || formData.forgotten_password_code.length < 6
      ) alertMessage(null, 'Please fill the form correctly')
      else {
        formData.email = email
        dispatch(postInputForgotPassword, formData)
          .then((dataResponse) => {
            navigateTo(
              'LoginForgotNewPasswordScreen',
              {
                email,
                forgotten_password_code: formData.forgotten_password_code
              }
            )
          })
      }
    }

    render() {
      const {
        navigateBack
      } = this.props
      const {
        isLoading
      } = this.state
      return (
        <Container isLoading={isLoading} colors={[ORANGE_BRIGHT_DARK, PINK_RED_DARK]} theme='light'>
          <Header>
            <Icon onPress={() => { navigateBack() }} size='massive' color={WHITE} name='left' />
          </Header>
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps='handled'
          >
            <View style={{ marginHorizontal: WP5 }}>
              <Text centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>Input Your</Text>
              <Text centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>Forgot Password Code</Text>
              <Form onSubmit={this._onForgotPassword}>
                {({ onChange, onSubmit }) => (
                  <View style={{ marginTop: HP20 }}>
                    <CodeConfirmationInput
                      ref='codeInputRef1'
                      className={'border-b'}
                      codeLength={6}
                      keyboardType='numeric'
                      size={WP10}
                      inputPosition='center'
                      onFulfill={(code) => onSubmit}
                      codeInputStyle={{ fontSize: FONT_SIZE['large'] }}
                      onCodeChange={onChange('forgotten_password_code')}
                    />
                    <Button
                      onPress={onSubmit}
                      style={{ marginTop: HP5 }}
                      rounded
                      centered
                      shadow='none'
                      backgroundColor={WHITE20}
                      textColor={WHITE}
                      text='Continue'
                    />
                  </View>
                )}
              </Form>
            </View>
          </ScrollView>
        </Container>
      )
    }
}

LoginForgotInputCodeScreen.defaultProps = defaultProps
export default _enhancedNavigation(LoginForgotInputCodeScreen, mapFromNavigationParam)
