import React from 'react'
import { View, ScrollView } from 'react-native'
import { isEmpty } from 'lodash-es'
import { Text, Container, Button, Header, Icon, Form, InputText, _enhancedNavigation } from '../../components'
import { ORANGE_BRIGHT_DARK, PINK_RED_DARK, WHITE, WHITE20 } from '../../constants/Colors'
import { WP5, HP5, HP20 } from '../../constants/Sizes'
import { alertMessage } from '../../utils/alert'
import { postForgotNewPassword } from '../../actions/api'

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  email: getParam('email', ''),
  forgotten_password_code: getParam('forgotten_password_code', '')
})

const defaultProps = {
  initialLoaded: true,
  isLoading: false
}

class LoginForgotNewPasswordScreen extends React.Component {
  constructor(props) {
    super(props)
  }

    state = {}

    static navigationOptions = ({ navigation }) => ({
      gesturesEnabled: !navigation.getParam('initialLoaded', false),
    })

    static getDerivedStateFromProps(props, state) {
      return null
    }

    componentDidMount = () => { }

    _onForgotPassword = ({ formData }) => {
      const {
        dispatch,
        email,
        forgotten_password_code
      } = this.props

      if (
        isEmpty(formData.password) ||
            isEmpty(formData.confirm_password)
      ) alertMessage(null, 'Please fill the form correctly')
      else if (
        (formData.password) !==
            (formData.confirm_password)
      ) alertMessage(null, 'Passwords are not the same')
      else {
        formData.email = email
        formData.forgotten_password_code = forgotten_password_code
        dispatch(postForgotNewPassword, formData)
          .then((dataResponse) => {
            this.props.navigation.push(
              'MessageScreen',
              {
                toReset: 'Login',
                title: 'Success',
                message: 'Password has been saved, please login',
              })
          })
      }
    }

    render() {
      const {
        navigateBack
      } = this.props
      const {
        isLoading
      } = this.state
      return (
        <Container isLoading={isLoading} colors={[ORANGE_BRIGHT_DARK, PINK_RED_DARK]} theme='light'>
          <Header>
            <Icon onPress={() => { navigateBack() }} size='massive' color={WHITE} name='left' />
          </Header>
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps='handled'
          >
            <View style={{ marginHorizontal: WP5 }}>
              <Text centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>New Password</Text>
              <Form onSubmit={this._onForgotPassword}>
                {({ onChange, onSubmit }) => (
                  <View style={{ marginTop: HP20 }}>
                    <InputText
                      secureTextEntry
                      toggleSecureTextEntry
                      label='New Password'
                      onChangeText={onChange('password')}
                      returnKeyType='done'
                    />
                    <InputText
                      secureTextEntry
                      toggleSecureTextEntry
                      label='Confirm New Password'
                      onChangeText={onChange('confirm_password')}
                      returnKeyType='done'
                    />
                    <Button
                      onPress={onSubmit}
                      style={{ marginTop: HP5 }}
                      rounded
                      centered
                      shadow='none'
                      backgroundColor={WHITE20}
                      textColor={WHITE}
                      text='Change Password'
                    />
                  </View>
                )}
              </Form>
            </View>
          </ScrollView>
        </Container>
      )
    }
}

LoginForgotNewPasswordScreen.defaultProps = defaultProps
export default _enhancedNavigation(LoginForgotNewPasswordScreen, mapFromNavigationParam)
