import React from 'react'
import { View, ScrollView } from 'react-native'
import { isEmpty } from 'lodash-es'
import { Text, Container, Button, Header, Icon, Form, InputText, _enhancedNavigation } from '../../components'
import { ORANGE_BRIGHT_DARK, PINK_RED_DARK, WHITE, WHITE20 } from '../../constants/Colors'
import { WP5, HP5, HP20 } from '../../constants/Sizes'
import { alertMessage } from '../../utils/alert'
import { postForgotPassword } from '../../actions/api'

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

const defaultProps = {
  initialLoaded: true,
  isLoading: false
}

class LoginForgotPasswordScreen extends React.Component {
  constructor(props) {
    super(props)
  }

  state = {}

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false),
  })

  static getDerivedStateFromProps(props, state) {
    return null
  }

  componentDidMount = () => {}

  _onForgotPassword = ({ formData }) => {
    const {
      dispatch,
      navigateTo
    } = this.props

    if(
      isEmpty(formData.email)
    ) alertMessage(null, 'Please fill the form correctly')
    else {
      dispatch(postForgotPassword, formData)
        .then((dataResponse) => {
          navigateTo(
            'MessageScreen',
            {
              toScreen: 'LoginForgotInputCodeScreen',
              title: 'Sent',
              message: `We have sent reset password code to your email (${dataResponse.email}). Please check your email`,
              toScreenParams: {
                email: dataResponse.email
              }
            }
          )
        })
    }
  }

  render() {
    const {
      navigateBack
    } = this.props
    const {
      isLoading
    } = this.state
    return (
      <Container isLoading={isLoading} colors={[ORANGE_BRIGHT_DARK, PINK_RED_DARK]} theme='light'>
        <Header>
          <Icon onPress={() => { navigateBack() }} size='massive' color={WHITE} name='left'/>
        </Header>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='handled'
        >
          <View style={{ marginHorizontal: WP5 }}>
            <Text centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>Forgot Password</Text>
            <Form onSubmit={this._onForgotPassword}>
              {({ onChange, onSubmit }) => (
                <View style={{ marginTop: HP20 }}>
                  <InputText
                    label='Email'
                    onChangeText={onChange('email')}
                    returnKeyType='done'
                  />
                  <Button
                    onPress={onSubmit}
                    style={{ marginTop: HP5 }}
                    rounded
                    centered
                    shadow='none'
                    backgroundColor={WHITE20}
                    textColor={WHITE}
                    text='Send Recovery Code'
                  />
                </View>
              )}
            </Form>
          </View>
        </ScrollView>
      </Container>
    )
  }
}

LoginForgotPasswordScreen.defaultProps = defaultProps
export default _enhancedNavigation(LoginForgotPasswordScreen, mapFromNavigationParam)
