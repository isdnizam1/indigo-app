import React from "react";
import { connect } from "react-redux";
import {
  AppState,
  BackHandler,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import HTMLElement from "react-native-render-html";
import { isEmpty, noop } from "lodash-es";
import moment from "moment";
import Badge from "sf-components/Badge";
import { getYoutubeId } from "sf-utils/helper";
import * as WebBrowser from "expo-web-browser";
import {
  _enhancedNavigation,
  BottomSheet,
  Container,
  HeaderNormal,
  ReadMore,
  Text,
  Icon,
} from "../../components";
import {
  BLACK,
  BLUE_CHAT,
  NAVY_DARK,
  PALE_BLUE,
  PALE_GREY_THREE,
  PALE_LIGHT_BLUE_TWO,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  TOMATO,
  TRANSPARENT,
  WHITE,
  BLUE_CHAT20,
  GREEN_30,
  GREEN_20,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
} from "../../constants/Colors";
import {
  FONT_SIZE,
  HP1,
  HP100,
  HP15,
  HP2,
  HP3,
  HP45,
  HP50,
  WP05,
  WP1,
  WP10,
  WP100,
  WP105,
  WP12,
  WP15,
  WP2,
  WP3,
  WP4,
  WP405,
  WP5,
  WP50,
  WP6,
  HP5,
} from "../../constants/Sizes";
import VideoPlayer from "../../components/VideoPlayer";
import {
  getAdsDetail,
  getSubscriptionDetail,
  postVideoStatus,
  postLike,
} from "../../actions/api";
import { isIOS, postScreenTimeAdsHelper } from "../../utils/helper";
import { paymentDispatcher } from "../../services/payment";
import { HEADER, SHADOW_STYLE } from "../../constants/Styles";
import ButtonV2 from "../../components/ButtonV2";
import SkeletonContent from "react-native-skeleton-content";
import FeedItemSkeletonConfig from "../../components/feed/FeedItemSkeletonConfig";
import CounterButton from "../../components/feed/CounterButton";
import Touchable from "../../components/Touchable";
import YoutubePlayer from "react-native-youtube-iframe";

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam("id_ads", 0),
  isStartup: getParam("isStartup", false),
  refreshListVideo: getParam("refreshListVideo", noop),
  youtubeId: getParam("youtubeId", null),
});

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
};

const propsDefault = {
  paymentSourceSet: noop,
};
const dateFormat = "YYYY-MM-DD HH:mm:ss";
class PremiumVideoTeaser extends React.Component {
  constructor(props) {
    super(props);
    // this._didFocusSubscription = props.navigation.addListener('focus', () => {
    //   // console.log('isfocus')
    //   this.setState({ viewFrom: moment().format(dateFormat) })
    //   AppState.addEventListener('change', this._handleAppStateChange)
    // }
    // )
    this.focusListener = this.props.navigation.addListener(
      "focus",
      () => {
        console.log("focus");
        this.attachBackAction.bind(this);
        // this.setState({ viewFrom: moment().format(dateFormat) })
        AppState.addEventListener("change", this._handleAppStateChange);
      }
      // this.attachBackAction.bind(this),
    );
  }

  state = {
    isLoading: true,
    isReady: false,
    isKeyboardOpen: false,
    ads: null,
    fullScreen: false,
    playerWidth: Dimensions.get("window").width,
    playerHeight: (Dimensions.get("window").width * 180) / 320,
    hasBeenPlayed: false,
    joinPremium: false,
    readMore: false,
    isS3: true,
    viewFrom: null,
    youtubeId: null,
    appState: AppState.currentState,
  };

  componentDidMount() {
    this.blurListener = this.props.navigation.addListener(
      "blur",
      () => {
        console.log("blur");
        this.releaseBackAction.bind(this);
        if (this.props.refreshListVideo) {
          this.props.refreshListVideo();
        }
        this._postScreenTimeAds();
        AppState.removeEventListener("change", this._handleAppStateChange);
      }
      // this.releaseBackAction.bind(this),
    );
    this._getAdsDetail();
    // this._willBlurSubscription = this.props.navigation.addListener('blur', () => {
    //   // console.log('isBlur')
    //   if (this.props.refreshListVideo) {
    //     this.props.refreshListVideo()
    //   }
    //   this._postScreenTimeAds()
    //   AppState.removeEventListener('change', this._handleAppStateChange)
    // }
    // )
    // setTimeout(this.onWatchRequest.bind(this), 2000)
  }

  componentWillUnmount() {
    try {
      this.focusListener && this.focusListener();
      this.blurListener && this.blurListener();
      typeof this.backHandler === "object" &&
        typeof this.backHandler.remove === "function" &&
        this.backHandler.remove();
    } catch (err) {
      // keep silent
    }
    // this._didFocusSubscription && this._didFocusSubscription()
    // this._willBlurSubscription && this._willBlurSubscription()
  }

  focusListener;
  blurListener;
  // _didFocusSubscription;
  // _willBlurSubscription;

  _handleAppStateChange = (nextAppState) => {
    // console.log(AppState.currentState)
    if (AppState.currentState === "background") {
      this.videoRef && this.videoRef.pauseAsync();
      this._postScreenTimeAds();
    }
    if (
      (this.state.appState === "inactive" ||
        this.state.appState === "background") &&
      nextAppState === "active"
    ) {
      this.setState({ viewFrom: moment().format(dateFormat) });
      // console.log('back to foreground', this.state.viewFrom)
    }
    this.setState({ appState: nextAppState });
  };

  _postScreenTimeAds = async () => {
    const { isLoading, viewFrom } = this.state;
    const {
      id_ads,
      userData: { id_user },
      dispatch,
      isStartup,
    } = this.props;
    // console.log({ id_ads, id_user, viewFrom })
    postScreenTimeAdsHelper({
      viewFrom,
      id_ads,
      id_user,
      dispatch,
      isLoading,
      isStartup,
    });
  };

  _getAdsDetail = async () => {
    const { dispatch, id_ads, userData, navigateTo, navigateBack, isStartup } =
      this.props;
    const { isLoading } = this.state;
    const ads = await dispatch(
      getAdsDetail,
      { id_user: userData.id_user, id_ads },
      noop,
      false,
      isLoading
    );
    ads.additional_data = JSON.parse(ads.additional_data);
    try {
      this.setState({
        isS3:
          ads.additional_data.url_video.indexOf("streamingvideoprovider") == -1,
      });
    } catch (error) {
      // keep silent
    }
    const youtubeId = getYoutubeId(ads.additional_data.url_video);
    if (youtubeId) {
      if (!this.state.id_ads) {
        this.setState({
          youtubeId,
          ads,
          isLoading: false,
          isReady: true,
          viewFrom: moment().format(dateFormat), // record time when ads successfully loaded
        });
      }
    } else {
      // this.blurListener = this.props.navigation.addListener(
      //   'blur',
      //   () => {
      //     console.log('blur')
      //     this.releaseBackAction.bind(this)
      //     if (this.props.refreshListVideo) {
      //       this.props.refreshListVideo()
      //     }
      //     this._postScreenTimeAds()
      //     AppState.removeEventListener('change', this._handleAppStateChange)
      //   }
      //   // this.releaseBackAction.bind(this),
      // )
      this.setState({
        ads,
        isLoading: false,
        isReady: true,
        viewFrom: moment().format(dateFormat), // record time when ads successfully loaded
      });
      // console.log(this.state.viewFrom)
    }
    // console.log('finish')
  };

  initVideo() {
    this.setState({ hasBeenPlayed: true });
  }

  onBackPressed() {
    const { fullScreen, joinPremium } = this.state;
    joinPremium && this.toggleBottomSheet(false);
    return fullScreen || joinPremium;
  }

  releaseBackAction() {
    this.videoRef && this.videoRef.pauseAsync();
    typeof this.backHandler === "object" && this.backHandler.remove();
  }

  onLinkPress = (event, href) =>
    this.props.navigateTo("BrowserScreenNoTab", { url: href });

  onVideoRef(component) {
    this.videoRef = component;
  }

  attachBackAction() {
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.onBackPressed.bind(this)
    );
  }

  actionVideoPlaying = async (status) => {
    const { dispatch, id_ads, userData } = this.props;
    const body = { id_user: userData.id_user, id_ads, status };
    const { isLoading } = this.state;
    const ads = await dispatch(postVideoStatus, body, noop, false, isLoading);
    if (status === "completed") {
      this.componentDidMount();
    }
  };
  joinPremium = React.createRef();

  renderViewMore() {
    return (
      <TouchableOpacity
        style={{ marginTop: WP2 }}
        onPress={() => this.setState({ readMore: true })}
      >
        <Text weight={400} size="xmini" color={REDDISH}>
          Selengkapnya
        </Text>
      </TouchableOpacity>
    );
  }

  onFullscreenTouch() {
    this.setState({ fullScreen: !this.state.fullscreen });
  }

  _rightAction = () => {
    const { navigateTo, id_ads } = this.props;
    navigateTo("ShareScreen", {
      id: id_ads,
      type: "explore",
      title: "Video",
    });
  };

  async onWatchRequest() {
    const {
      ads: { id_ads, is_premium },
    } = this.state;
    const { userData, dispatch, navigateTo, isStartup } = this.props;
    await this.setState({ isLoading: true });
    try {
      const { account_type } = await dispatch(
        getSubscriptionDetail,
        { id_user: userData.id_user },
        noop,
        false,
        false
      );
      if (account_type === "free" && is_premium != "0")
        this.toggleBottomSheet(true);
      else navigateTo("PremiumVideoFull", { id_ads, isStartup });
      // navigateTo('PremiumVideoFull', { id_ads })
    } catch (e) {
      /*silent is gold*/
    } finally {
      await this.setState({ isLoading: false });
    }
  }

  async toggleBottomSheet(shown) {
    await this.setState({ joinPremium: shown });
    this.joinPremium.current?.snapTo(shown ? 1 : 2);
  }

  _renderItemLearn(data, index) {
    return (
      <View style={styles.dotContainer} key={index}>
        <View style={styles.itemDot} />
        <Text
          type="Circular"
          color={SHIP_GREY}
          weight={400}
          size={12}
          lineHeight={20}
        >
          {data}
        </Text>
      </View>
    );
  }

  _renderBadgeVideoStatus() {
    const { ads } = this.state;
    let videoStatus = ads.video_status
      .replace("-", " ")
      .replace(/(^\w|\s\w)/g, (m) => m.toUpperCase());
    if (videoStatus === "Not Played") {
      this.actionVideoPlaying("playing");
      videoStatus = "Playing";
    }
    const textColor =
      videoStatus === "Playing"
        ? BLUE_CHAT
        : videoStatus === "Completed"
        ? GREEN_30
        : SHIP_GREY_CALM;
    const color =
      videoStatus === "Playing"
        ? BLUE_CHAT20
        : videoStatus === "Completed"
        ? GREEN_20
        : PALE_GREY_THREE;
    return (
      <View style={{ alignSelf: "flex-start", marginTop: WP2 }}>
        <Badge textSize="xmini" radius={6} textColor={textColor} color={color}>
          {videoStatus}
        </Badge>
      </View>
    );
  }

  _onPressLike = () => {
    const {
      userData: { id_user },
      isStartup,
      onRefresh,
    } = this.props;
    const { ads } = this.state;

    postLike({
      id_user,
      related_to: "id_ads",
      id_related_to: ads.id_ads,
    }).then(() => {
      this._getAdsDetail();
    });
  };

  _renderHeaderRightModal = () => {
    const {
      userData: { id_user },
      isStartup,
    } = this.props;
    const { ads } = this.state;
    if (isStartup) {
      if (ads !== null) {
        return (
          <TouchableOpacity onPress={this._onPressLike}>
            <View
              style={{
                padding: 15,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                source={
                  ads.is_liked
                    ? require("sf-assets/icons/active_heart.png")
                    : require("sf-assets/icons/outlineHeartBlack.png")
                }
                style={{ width: 24, height: 24 }}
              />
            </View>
          </TouchableOpacity>
        );
      }
    } else {
      return (
        <TouchableOpacity
          onPress={this._rightAction}
          disabled={true}
          style={[HEADER.rightIcon, { paddingVertical: WP2 }]}
        >
          <Icon
            centered
            onPress={this._rightAction}
            background="dark-circle"
            size="large"
            color={SHIP_GREY_CALM}
            name="share-variant"
            type="MaterialCommunityIcons"
          />
        </TouchableOpacity>
      );
    }
  };

  _renderCommentButton = () => {
    const { isLoading } = this.props;
    const { ads } = this.state;
    return (
      <Touchable
        onPress={this._onPressComment}
        style={{
          // position: 'absolute',
          height: HP5,
          // left: 0,
          // top: HP100 - HP5,
          width: WP100,
          elevation: 5,
          backgroundColor: WHITE,
          // borderTopWidth: 1,
          borderTopColor: "#E4E6E7",
        }}
      >
        <View
          style={{
            justifyContent: "space-between",
            alignItems: "center",
            flexDirection: "row",
            flex: 1,
            paddingHorizontal: 10,
            marginBottom: WP5,
          }}
        >
          <CounterButton
            onPress={this._onPressComment}
            images={[null, require("sf-assets/icons/icComment.png")]}
            value={counterLabel(ads?.total_comment, "Komentar", "Komentar")}
            disable={isLoading}
          />
          <Icon
            centered
            name={"chevron-thin-right"}
            type="Entypo"
            color={SHIP_GREY_CALM}
          />
        </View>
      </Touchable>
    );
  };

  _onPressComment = () => {
    const { navigateTo } = this.props;
    const { ads } = this.state;
    this.videoRef && this.videoRef.pauseAsync();
    navigateTo("StartUpCommentScreen", {
      id_ads: ads.id_ads,
      onRefresh: this._getAdsDetail,
    });
  };

  render() {
    const {
      navigateBack,
      isLoading,
      navigateTo,
      paymentSourceSet,
      isStartup,
      refreshListVideo,
    } = this.props;
    const {
      ads,
      isReady,
      fullScreen,
      playerWidth,
      playerHeight,
      hasBeenPlayed,
      readMore,
      isS3,
      youtubeId,
    } = this.state;
    const PLAY_BUTTON_SIZE = WP12;
    const isLiveStreaming =
      ads &&
      ads.additional_data.url_video &&
      ads.additional_data.url_video.indexOf("streaming") >= 0;
    return (
      <Container
        isLoading={isLoading}
        isReady={isReady}
        noStatusBar={fullScreen}
        noStatusBarPadding={fullScreen}
        outsideContent={() => {
          return (
            <BottomSheet
              onCloseEnd={this.toggleBottomSheet.bind(this, false)}
              backdropClose
              innerRef={this.joinPremium}
              snapPoints={[HP50, HP45, -HP100]}
              enabledContentTapInteraction={false}
              renderContent={({ closeBottomSheet }) => (
                <View
                  style={{
                    padding: WP6,
                    justifyContent: "center",
                    alignItems: "center",
                    flex: 1,
                  }}
                >
                  <Image
                    source={require("../../assets/images/limitModalImage.png")}
                    imageStyle={{ height: HP15 }}
                    aspectRatio={182 / 108}
                  />
                  <TouchableOpacity
                    onPress={this.toggleBottomSheet.bind(this, false)}
                    style={{
                      position: "absolute",
                      top: HP1,
                      left: WP2,
                      padding: HP2,
                    }}
                  >
                    <Image
                      source={require("../../assets/icons/close_circle_gray.png")}
                      style={{ height: HP3, width: HP3 }}
                    />
                  </TouchableOpacity>
                  <Text
                    centered
                    color={NAVY_DARK}
                    type="Circular"
                    style={{ marginVertical: HP1 }}
                    weight={600}
                    size="massive"
                  >
                    Oops...!
                  </Text>
                  {isIOS() ? (
                    <Text
                      color={SHIP_GREY}
                      centered
                      type="Circular"
                      style={{ marginVertical: HP3 }}
                      size="xmini"
                    >
                      Unfortunately, this feature is only for premium member and
                      you can’t upgrade it directly on our apps. Please contact
                      administrator for further information.
                    </Text>
                  ) : (
                    <Text
                      color={SHIP_GREY}
                      centered
                      type="Circular"
                      style={{ marginVertical: HP3 }}
                      size="xmini"
                    >
                      This video is only available for premium user. Let’s be
                      our premium member!
                    </Text>
                  )}
                  {!isIOS() && (
                    <View style={{ flexDirection: "row", paddingBottom: WP4 }}>
                      <ButtonV2
                        onPress={() => {
                          paymentSourceSet("soundfren_edu");
                          navigateTo("MembershipScreen");
                        }}
                        style={{ flex: 1, paddingVertical: WP3 }}
                        color={REDDISH}
                        textColor={WHITE}
                        textSize={"mini"}
                        text={"Join Premium"}
                      />
                    </View>
                  )}
                </View>
              )}
            />
          );
        }}
        renderHeader={() =>
          !fullScreen ? (
            <HeaderNormal
              centered
              textSize={"slight"}
              text={isLiveStreaming ? "Live Streaming" : "Video"}
              iconLeftOnPress={() => {
                if (refreshListVideo) {
                  refreshListVideo();
                }
                navigateBack();
              }}
              noRightPadding
              rightComponent={this._renderHeaderRightModal()}
            />
          ) : null
        }
        isAvoidingView
        scrollBackgroundColor={WHITE}
        scrollContentContainerStyle={{ justifyContent: "space-between" }}
      >
        {isS3 &&
          (ads && !hasBeenPlayed ? (
            youtubeId ? (
              <View style={{ width: WP100, height: WP50, marginBottom: HP3 }}>
                <YoutubePlayer
                  height={300}
                  play={true}
                  videoId={youtubeId}
                  onChangeState={noop}
                  onFullScreenChange={
                    fullScreen === true
                      ? ScreenOrientation.lockAsync(
                          ScreenOrientation.OrientationLock.PORTRAIT
                        )
                      : null
                  }
                />
              </View>
            ) : (
              <VideoPlayer
                style={{ width: playerWidth, height: playerHeight, flex: 0 }}
                onRef={this.onVideoRef.bind(this)}
                source={{ uri: ads.additional_data.url_video_teaser }}
                actionVideoPlaying={isStartup ? this.actionVideoPlaying : noop}
              />
            )
          ) : null)}

        {!isS3 && ads && (
          <TouchableOpacity
            onPress={() => {
              {
                /* navigateTo('BrowserScreenNoTab', {
                url: ads.additional_data.url_video,
                forceTitle: isLiveStreaming ? 'Live Streaming' : '',
              }) */
              }
              WebBrowser.openBrowserAsync(ads.additional_data.url_video);
            }}
          >
            <Image
              resizeMode={"cover"}
              style={{ width: WP100, height: WP50 + WP5 }}
              source={{ uri: ads.image }}
            />
            <Image
              style={[
                {
                  width: PLAY_BUTTON_SIZE,
                  height: PLAY_BUTTON_SIZE,
                  position: "absolute",
                  left: "50%",
                  top: "50%",
                  marginLeft: (PLAY_BUTTON_SIZE / 2) * -1,
                  marginTop: (PLAY_BUTTON_SIZE / 2) * -1,
                },
              ]}
              source={require("../../assets/icons/play_button.png")}
            />
          </TouchableOpacity>
        )}
        {ads && hasBeenPlayed ? (
          <TouchableOpacity
            onPress={this.initVideo.bind(this)}
            activeOpacity={0.95}
            style={{
              flex: 0,
              width: playerWidth,
              height: playerHeight,
              backgroundColor: BLACK,
            }}
          >
            <Image
              resizeMode={"contain"}
              style={{ flex: 1, width: playerWidth, height: playerHeight }}
              source={{ uri: ads.additional_data.thumbnail }}
            />
            <Image
              style={styles.playButton}
              source={require("../../assets/icons/video_player/play.png")}
            />
          </TouchableOpacity>
        ) : null}
        {ads && !fullScreen ? (
          <ScrollView>
            <View>
              <View style={{ paddingHorizontal: WP4, paddingVertical: WP6 }}>
                {ads.is_premium != "0" && (
                  <View style={{ alignSelf: "flex-start", marginBottom: WP2 }}>
                    <Badge
                      textSize="xmini"
                      radius={6}
                      textColor={SHIP_GREY_CALM}
                      color={PALE_GREY_THREE}
                    >
                      Premium User
                    </Badge>
                  </View>
                )}
                <Text
                  type="Circular"
                  color={NAVY_DARK}
                  size="large"
                  weight={600}
                  style={{ lineHeight: WP6, marginBottom: WP4 }}
                >
                  {ads.title}
                </Text>
                <View>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginBottom: WP2,
                    }}
                  >
                    <Icon
                      centered
                      name="eye"
                      type="MaterialCommunityIcons"
                      size="tiny"
                      color={PALE_LIGHT_BLUE_TWO}
                      style={{ marginRight: WP1 }}
                    />
                    <Text
                      numberOfLines={1}
                      type="Circular"
                      size="xmini"
                      color={SHIP_GREY}
                      weight={300}
                    >
                      {(ads.total_view || 0)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                      view
                      {ads.total_view && ads.total_view > 1 ? "s" : null}
                    </Text>
                    <View
                      style={{
                        width: WP05,
                        height: WP05,
                        borderRadius: WP05 / 2,
                        backgroundColor: SHIP_GREY,
                        marginHorizontal: WP1,
                      }}
                    />
                    <Text
                      numberOfLines={1}
                      type="Circular"
                      size="xmini"
                      color={SHIP_GREY}
                      weight={300}
                    >
                      {moment(ads.created_at).format("LL")}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Icon
                      centered
                      name="access-time"
                      type="MaterialIcons"
                      size="tiny"
                      color={PALE_LIGHT_BLUE_TWO}
                      style={{ marginRight: WP1 }}
                    />
                    <Text
                      numberOfLines={1}
                      type="Circular"
                      size="xmini"
                      color={SHIP_GREY}
                      weight={300}
                    >
                      Durasi Video: {ads.additional_data.video_duration}
                    </Text>
                  </View>
                  {isStartup && this._renderBadgeVideoStatus()}
                </View>
              </View>

              <View
                style={{
                  padding: WP4,
                  borderBottomWidth: 1,
                  borderTopWidth: 1,
                  borderColor: PALE_BLUE,
                }}
              >
                <Text
                  color={SHIP_GREY}
                  size={"xmini"}
                  weight={400}
                  style={{ marginBottom: WP2 }}
                >
                  Tentang Video
                </Text>
                {readMore ? (
                  <HTMLElement
                    textSelectable
                    html={ads.description.trim().replace(/(\r\n|\n|\r|)/gm, "")}
                    onLinkPress={this.onLinkPress.bind(this)}
                    baseFontStyle={{
                      fontSize: FONT_SIZE["xmini"],
                      fontFamily: "CircularBook",
                      color: SHIP_GREY,
                      lineHeight: WP405,
                    }}
                  />
                ) : (
                  <ReadMore
                    numberOfLines={3}
                    renderViewMore={this.renderViewMore.bind(this)}
                  >
                    <Text
                      color={SHIP_GREY}
                      style={{ lineHeight: WP405 }}
                      size="xmini"
                    >
                      {ads.description
                        .replace(/<\/?[^>]+(>|$)/g, "")
                        .replace(/\s+/g, " ")
                        .trim()}
                    </Text>
                  </ReadMore>
                )}
              </View>

              {isStartup && (
                <View
                  style={{
                    padding: WP4,
                    borderBottomWidth: 1,
                    borderColor: PALE_BLUE,
                  }}
                >
                  <Text
                    color={SHIP_GREY}
                    size={"xmini"}
                    weight={400}
                    style={{ marginBottom: WP2 }}
                  >
                    Yang kamu pelajari:
                  </Text>
                  {ads.additional_data.learn.map(this._renderItemLearn)}
                </View>
              )}
              <View
                style={{
                  padding: WP4,
                  borderBottomWidth: 1,
                  borderColor: PALE_BLUE,
                }}
              >
                <Text
                  color={SHIP_GREY}
                  size={"xmini"}
                  weight={400}
                  style={{ marginBottom: WP2 }}
                >
                  Kategori:
                </Text>
                {!isEmpty(ads.additional_data.category) ? (
                  <View
                    style={{
                      flexDirection: "row",
                      flexWrap: "wrap",
                      // marginBottom: HP3,
                    }}
                  >
                    {ads.additional_data.category.map((category) => (
                      <View
                        key={Math.random()}
                        style={{
                          alignSelf: "flex-start",
                          marginBottom: WP2,
                          marginRight: WP2,
                        }}
                      >
                        <Badge
                          textSize="xmini"
                          radius={6}
                          textColor={SHIP_GREY_CALM}
                          color={PALE_GREY_THREE}
                        >
                          {category}
                        </Badge>
                      </View>
                    ))}
                  </View>
                ) : null}
              </View>
              <View style={{ marginTop: WP1 }}>
                {this._renderCommentButton()}
              </View>
            </View>
          </ScrollView>
        ) : null}
        {ads && !fullScreen && isS3 ? (
          !youtubeId ? (
            <View
              style={{
                backgroundColor: WHITE,
                padding: WP4,
                ...SHADOW_STYLE["shadow"],
              }}
            >
              {!isStartup && (
                <ButtonV2
                  onPress={this.onWatchRequest.bind(this)}
                  style={{ paddingVertical: WP3 }}
                  color={REDDISH}
                  textColor={WHITE}
                  textSize={"mini"}
                  text={"Lihat Full Video"}
                />
              )}
            </View>
          ) : null
        ) : null}
      </Container>
    );
  }
}

const counterLabel = (value, label, empty) => {
  let labelText = value < 1 ? empty : label;
  let valueText = value < 1 ? "" : `${value} `;
  return `${valueText}${labelText}`;
};

const playButtonSize = 52;

const styles = StyleSheet.create({
  btnCategory: {
    borderRadius: WP100,
    borderColor: TOMATO,
    borderWidth: 1,
    paddingVertical: WP05,
    paddingHorizontal: WP4,
    flexGrow: 0,
    flex: 0,
    marginVertical: WP05,
    marginRight: WP105,
  },
  playButton: {
    width: playButtonSize,
    height: playButtonSize,
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: (playButtonSize / 2) * -1,
    marginLeft: (playButtonSize / 2) * -1,
  },
  itemDot: {
    width: 4,
    height: 4,
    borderRadius: 8,
    backgroundColor: SHIP_GREY_CALM,
    marginTop: 8,
    marginRight: 7,
  },
  dotContainer: {
    flexDirection: "row",
  },
});

PremiumVideoTeaser.defaultProps = propsDefault;
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(PremiumVideoTeaser),
  mapFromNavigationParam
);
