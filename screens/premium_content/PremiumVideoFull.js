import React from 'react'
import { connect } from 'react-redux'
import {
  BackHandler,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native'
import HTMLElement from 'react-native-render-html'
import { isEmpty, noop } from 'lodash-es'
import moment from 'moment'
import Badge from 'sf-components/Badge'
import {
  _enhancedNavigation,
  Container,
  ReadMore,
  Text,
  Icon,
} from '../../components'
import {
  BLACK,
  NAVY_DARK,
  PALE_BLUE,
  PALE_GREY_THREE,
  PALE_LIGHT_BLUE_TWO,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  TOMATO,
  WHITE,
} from '../../constants/Colors'
import {
  FONT_SIZE,
  HP3,
  WP05,
  WP1,
  WP100,
  WP105,
  WP2,
  WP4,
  WP405,
  WP6,
} from '../../constants/Sizes'
import VideoPlayer from '../../components/VideoPlayer'
import { getAdsDetail } from '../../actions/api'

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam('id_ads', 0),
  isStartup: getParam('isStartup', false),
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const propsDefault = {}

class PremiumVideoFull extends React.Component {
  constructor(props) {
    super(props)
  }

  state = {
    isLoading: true,
    isReady: false,
    isKeyboardOpen: false,
    ads: null,
    fullScreen: false,
    playerWidth: Dimensions.get('window').width,
    playerHeight: (Dimensions.get('window').width * 180) / 320,
    hasBeenPlayed: false,
    joinPremium: false,
    readMore: false,
  };

  onLinkPress = (event, href) =>
    this.props.navigateTo('BrowserScreenNoTab', { url: href });

  renderViewMore() {
    return (
      <TouchableOpacity
        style={{ marginTop: WP2 }}
        onPress={() => this.setState({ readMore: true })}
      >
        <Text weight={400} size='xmini' color={REDDISH}>
          Selengkapnya
        </Text>
      </TouchableOpacity>
    )
  }

  joinPremium = React.createRef();

  attachBackAction() {
    // this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.onBackPressed.bind(this))
  }

  onVideoRef(component) {
    this.videoRef = component
  }

  releaseBackAction() {
    this.videoRef && this.videoRef.pauseAsync()
    typeof this.backHandler === 'object' && this.backHandler.remove()
  }

  componentWillUnmount() {
    try {
      typeof this.backHandler === 'object' &&
        typeof this.backHandler.remove === 'function' &&
        this.backHandler.remove()
      this.blurListener.remove()
      this.focusListener.remove()
    } catch (error) {
      // silent
    }
  }

  async componentDidMount() {
    this.focusListener = this.props.navigation.addListener(
      'focus',
      this.attachBackAction.bind(this),
    )
    this.blurListener = this.props.navigation.addListener(
      'blur',
      this.releaseBackAction.bind(this),
    )
    const { dispatch, id_ads, userData } = this.props
    const { isLoading } = this.state
    const ads = await dispatch(
      getAdsDetail,
      { id_user: userData.id_user, id_ads },
      noop,
      false,
      isLoading,
    )
    ads.additional_data = JSON.parse(ads.additional_data)
    this.setState({
      ads,
      isLoading: false,
      isReady: true,
    })

    // setTimeout(this.onWatchRequest.bind(this), 2000)
  }

  initVideo() {
    this.setState({ hasBeenPlayed: true })
  }

  // onBackPressed() {
  //   const { fullScreen, joinPremium } = this.state
  //   return (fullScreen || joinPremium)
  // }

  onFullscreenTouch() {
    this.setState({ fullScreen: !this.state.fullscreen })
  }

  _onShareToTimeline = () => {
    const { navigateTo, id_ads } = this.props
    navigateTo('ShareScreen', {
      id: id_ads,
      type: 'explore',
      title: 'Video',
    })
  };

  render() {
    const { navigateBack, isLoading, isStartup } = this.props
    const {
      ads,
      isReady,
      fullScreen,
      playerWidth,
      playerHeight,
      hasBeenPlayed,
      readMore,
    } = this.state
    return (
      <Container
        isLoading={isLoading}
        isReady={isReady}
        theme={'light'}
        noStatusBar={fullScreen}
        noStatusBarPadding={true}
        isAvoidingView
        scrollBackgroundColor={WHITE}
        scrollContentContainerStyle={{ justifyContent: 'space-between' }}
      >
        {ads && !hasBeenPlayed ? (
          <VideoPlayer
            onBackPressed={navigateBack}
            style={{ width: playerWidth, height: playerHeight, flex: 0 }}
            onRef={this.onVideoRef.bind(this)}
            source={{ uri: ads.additional_data.url_video }}
          />
        ) : null}
        {ads && hasBeenPlayed ? (
          <TouchableOpacity
            onPress={this.initVideo.bind(this)}
            activeOpacity={0.95}
            style={{
              flex: 0,
              width: playerWidth,
              height: playerHeight,
              backgroundColor: BLACK,
            }}
          >
            <Image
              resizeMode={'contain'}
              style={{ flex: 1, width: playerWidth, height: playerHeight }}
              source={{ uri: ads.additional_data.thumbnail }}
            />
            <Image
              style={styles.playButton}
              source={require('../../assets/icons/video_player/play.png')}
            />
          </TouchableOpacity>
        ) : null}
        {ads && !fullScreen ? (
          <ScrollView>
            <View>
              <View style={{ paddingHorizontal: WP4, paddingVertical: WP6 }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginBottom: WP2,
                  }}
                >
                  {ads.is_premium != '0' && (
                    <View>
                      <View style={{ alignSelf: 'flex-start' }}>
                        <Badge
                          textSize='xmini'
                          radius={6}
                          textColor={SHIP_GREY_CALM}
                          color={PALE_GREY_THREE}
                        >
                          Premium User
                        </Badge>
                      </View>
                    </View>
                  )}
                  {!isStartup && (
                    <TouchableOpacity onPress={this._onShareToTimeline}>
                      <Icon
                        centered
                        onPress={this._onShareToTimeline}
                        background='dark-circle'
                        size='large'
                        color={SHIP_GREY_CALM}
                        name='share-variant'
                        type='MaterialCommunityIcons'
                      />
                    </TouchableOpacity>
                  )}
                </View>
                <Text
                  type='Circular'
                  color={NAVY_DARK}
                  size='large'
                  weight={600}
                  style={{ lineHeight: WP6, marginBottom: WP4 }}
                >
                  {ads.title}
                </Text>
                <View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginBottom: WP2,
                    }}
                  >
                    <Icon
                      centered
                      name='eye'
                      type='MaterialCommunityIcons'
                      size='tiny'
                      color={PALE_LIGHT_BLUE_TWO}
                      style={{ marginRight: WP1 }}
                    />
                    <Text
                      numberOfLines={1}
                      type='Circular'
                      size='xmini'
                      color={SHIP_GREY}
                      weight={300}
                    >
                      {(ads.total_view || 0)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}{' '}
                      view
                      {ads.total_view && ads.total_view > 1 ? 's' : null}
                    </Text>
                    <View
                      style={{
                        width: WP05,
                        height: WP05,
                        borderRadius: WP05 / 2,
                        backgroundColor: SHIP_GREY,
                        marginHorizontal: WP1,
                      }}
                    />
                    <Text
                      numberOfLines={1}
                      type='Circular'
                      size='xmini'
                      color={SHIP_GREY}
                      weight={300}
                    >
                      {moment(ads.created_at).format('LL')}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon
                      centered
                      name='access-time'
                      type='MaterialIcons'
                      size='tiny'
                      color={PALE_LIGHT_BLUE_TWO}
                      style={{ marginRight: WP1 }}
                    />
                    <Text
                      numberOfLines={1}
                      type='Circular'
                      size='xmini'
                      color={SHIP_GREY}
                      weight={300}
                    >
                      Durasi Video: {ads.additional_data.video_duration}
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  padding: WP4,
                  borderBottomWidth: 1,
                  borderTopWidth: 1,
                  borderColor: PALE_BLUE,
                }}
              >
                <Text
                  color={SHIP_GREY}
                  size={'xmini'}
                  weight={400}
                  style={{ marginBottom: WP2 }}
                >
                  Tentang Video
                </Text>
                {readMore ? (
                  <HTMLElement
                    textSelectable
                    html={ads.description.trim().replace(/(\r\n|\n|\r|)/gm, '')}
                    onLinkPress={this.onLinkPress.bind(this)}
                    baseFontStyle={{
                      fontSize: FONT_SIZE['xmini'],
                      fontFamily: 'CircularBook',
                      color: SHIP_GREY,
                      lineHeight: WP405,
                    }}
                  />
                ) : (
                  <ReadMore
                    numberOfLines={3}
                    renderViewMore={this.renderViewMore.bind(this)}
                  >
                    <Text
                      color={SHIP_GREY}
                      style={{ lineHeight: WP405 }}
                      size='xmini'
                    >
                      {ads.description
                        .replace(/<\/?[^>]+(>|$)/g, '')
                        .replace(/\s+/g, ' ')
                        .trim()}
                    </Text>
                  </ReadMore>
                )}
              </View>

              <View style={{ padding: WP4 }}>
                <Text
                  color={SHIP_GREY}
                  size={'xmini'}
                  weight={400}
                  style={{ marginBottom: WP2 }}
                >
                  Kategori:
                </Text>
                {!isEmpty(ads.additional_data.category) ? (
                  <View
                    style={{
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      marginBottom: HP3,
                    }}
                  >
                    {ads.additional_data.category.map((category) => (
                      <View
                        key={Math.random()}
                        style={{
                          alignSelf: 'flex-start',
                          marginBottom: WP2,
                          marginRight: WP2,
                        }}
                      >
                        <Badge
                          textSize='xmini'
                          radius={6}
                          textColor={SHIP_GREY_CALM}
                          color={PALE_GREY_THREE}
                        >
                          {category}
                        </Badge>
                      </View>
                    ))}
                  </View>
                ) : null}
              </View>
            </View>
          </ScrollView>
        ) : null}
      </Container>
    )
  }
}

const playButtonSize = 52

const styles = StyleSheet.create({
  btnCategory: {
    borderRadius: WP100,
    borderColor: TOMATO,
    borderWidth: 1,
    paddingVertical: WP05,
    paddingHorizontal: WP4,
    flexGrow: 0,
    flex: 0,
    marginVertical: WP05,
    marginRight: WP105,
  },
  playButton: {
    width: playButtonSize,
    height: playButtonSize,
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: (playButtonSize / 2) * -1,
    marginLeft: (playButtonSize / 2) * -1,
  },
})

PremiumVideoFull.defaultProps = propsDefault
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(PremiumVideoFull),
  mapFromNavigationParam,
)
