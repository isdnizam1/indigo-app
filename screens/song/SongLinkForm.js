import React from 'react'
import { Keyboard, ScrollView, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash-es'
import { _enhancedNavigation, Container, Form, Header, Icon, InputTextLight, Text } from '../../components'
import { GREY, GREY_CALM_SEMI, ORANGE_BRIGHT, PURPLE, WHITE } from '../../constants/Colors'
import { WP4 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { alertMessage } from '../../utils/alert'
import { postSongLink, putSongLink } from '../../actions/api'
import { authDispatcher } from '../../services/auth'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', () => {}),
  initialValues: getParam('initialValues', {})
})

class SongLinkForm extends React.Component {

  state = {
    buttonEnabled: true
  }

  _onAddSong = async ({ formData }) => {
    const {
      userData: { id_user },
      navigateBack,
      refreshProfile,
      dispatch,
      getUserDetailDispatcher,
      initialValues
    } = this.props
    const action = isEmpty(initialValues) ? postSongLink : putSongLink
    if(
      isEmpty(formData.title) ||
      isEmpty(formData.songUrl)
    ) alertMessage(null, 'Please fill the form correctly')
    else {
      await dispatch(action, {
        id_user,
        id_journey: initialValues.id_journey,
        title: formData.title,
        description: formData.description,
        url_song: formData.songUrl
      })
      getUserDetailDispatcher({ id_user })
      refreshProfile()
      navigateBack()
    }
  }

  componentDidMount() {
    this.setState({ buttonEnabled: !this.props.initialValues.title })
  }

  onChangeInterceptor({ key, text, state, onChange }) {
    let { initialValues: { title, songUrl, description } } = this.props
    title && this.setState({
      buttonEnabled: title != state.title || songUrl != state.songUrl || description != state.description
    })
  }

  render() {
    const {
      isLoading,
      navigateBack,
      initialValues
    } = this.props
    const { buttonEnabled } = this.state
    return (
      <Container isLoading={isLoading} colors={[WHITE, WHITE]}>
        <Form initialValue={initialValues} onChangeInterceptor={this.onChangeInterceptor.bind(this)} onSubmit={this._onAddSong}>
          {({ onChange, onSubmit, isDirty, formData }) => (
            <View>
              <Header>
                <Icon onPress={() => navigateBack()} size='large' color={GREY} name='close'/>
                <Text size='small' type='SansPro' weight={500}>{isEmpty(initialValues) ? 'Add Song' : 'Edit Song'}</Text>
                <TouchableOpacity
                  disabled={!isDirty || !buttonEnabled} onPress={() => {
                    Keyboard.dismiss()
                    onSubmit()
                  }} activeOpacity={TOUCH_OPACITY}
                >
                  <Text size='small' color={buttonEnabled ? ORANGE_BRIGHT : GREY_CALM_SEMI}>Save</Text>
                </TouchableOpacity>
              </Header>
              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1, paddingHorizontal: WP4 }}
              >
                <InputTextLight
                  value={formData.songUrl}
                  required
                  label='Link Song'
                  valueColor={PURPLE}
                  bordered
                  multiline
                  placeholder='Copy link here (spotify, soundcloud, etc)'
                  onChangeText={onChange('songUrl')}
                />
                <InputTextLight
                  value={formData.title}
                  required
                  label='Song Title'
                  onChangeText={onChange('title')}
                />
                <InputTextLight
                  value={formData.description}
                  multiline
                  label='Description'
                  onChangeText={onChange('description')}
                />
              </ScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SongLinkForm),
  mapFromNavigationParam
)
