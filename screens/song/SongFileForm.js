import React from 'react'
import { connect } from 'react-redux'
import {
  BackHandler,
  ImageBackground,
  ActivityIndicator,
  Keyboard,
  TouchableOpacity,
  View,
} from 'react-native'
import * as DocumentPicker from 'expo-document-picker'
import { includes, isEmpty, isNull, noop, remove, toLower } from 'lodash-es'
import Animated from 'react-native-reanimated'
import { setClearBlueMessage } from 'sf-services/messagebar/actionDispatcher'
import { WP25 } from 'sf-constants/Sizes'
import {
  _enhancedNavigation,
  BottomSheet,
  Button,
  Card,
  Container,
  Form,
  HeaderNormal,
  Icon,
  Image,
  InputTextLight,
  Loader,
  Player,
  Text,
  SelectModalV3,
} from '../../components'
import {
  BLACK,
  GREY_CALM_SEMI,
  GUN_METAL,
  NAVY_DARK,
  ORANGE_BRIGHT,
  PALE_BLUE,
  PALE_BLUE_TWO,
  PALE_GREY,
  PALE_SALMON,
  PALE_WHITE,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SILVER_CALMER,
  SILVER_TWO,
  TOMATO,
  WHITE,
} from '../../constants/Colors'
import {
  HP1,
  HP100,
  HP105,
  HP2,
  HP50,
  WP60,
  WP05,
  WP1,
  WP10,
  WP100,
  WP15,
  WP205,
  WP3,
  WP4,
  WP5,
  WP50,
  WP6,
  WP70,
  WP8,
  WP90,
  HP4,
  WP20,
  WP35,
  WP45,
  HP10,
} from '../../constants/Sizes'
import { formatBytes, selectPhoto } from '../../utils/upload'
import {
  getGenreInterest,
  getProfileSongs,
  postMusicFile,
  postSongFile,
  putSongFile,
} from '../../actions/api'
import { objectMapper } from '../../utils/mapper'
import { SHADOW_STYLE, TEXT_INPUT_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { authDispatcher } from '../../services/auth'
import { convertBlobToBase64, fetchAsBlob, getTime, isIOS } from '../../utils/helper'
import Label from '../../components/Label'
import SelectedItem from '../../components/SelectedItems'
import ButtonV2 from '../../components/ButtonV2'
import { paymentDispatcher } from '../../services/payment'
import { setAddProfile } from '../../utils/review'
import { FORM_VALIDATION, MAPPER_FOR_API, MAPPER_FROM_API } from './_constants'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
  setClearBlueMessage,
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', () => {}),
  initialLoaded: getParam('initialLoaded', false),
  id: getParam('id', 0),
  accountType: getParam('accountType', ''),
})

const propsDefault = {}

const configPackage = {
  free: {
    limit: 3,
  },
  maestro: {
    limit: 9,
  },
  pro: {
    limit: 9,
  },
  basic: {
    limit: 6,
  },
}

class SongFileForm extends React.Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props)

    this._backHandler = this._backHandler.bind(this)
    this._didFocusSubscription = props.navigation.addListener('focus', () =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler),
    )
  }

  state = {
    song: {
      songUploading: 0,
    },
    isReady: !this.props.id,
    isMoreDetails: false,
    buttonEnabled: true,
    isUpload: false,
    isUploadOver: false,
    isUploading: false,
    step: 1,
    percentCompleted: 0,
    isPlaying: false,
    playerState: {},
  };

  async componentDidMount() {
    await this._getInitialScreenData()
    this._willBlurSubscription = this.props.navigation.addListener('blur', () =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler),
    )
    this.setState({ buttonEnabled: !this.props.id })
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  backAlert = React.createRef();
  confirmAlert = React.createRef();
  form = React.createRef();

  _getInitialScreenData = async (...args) => {
    if (this.props.id) {
      await this._getSongDetail(...args)
      await this.setState({
        isReady: true,
      })
    }
  };

  _getSongDetail = async (isLoading = true) => {
    const { id, dispatch } = this.props
    if (id) {
      const dataResponse = await dispatch(
        getProfileSongs,
        { id_journey: id },
        noop,
        false,
        isLoading,
      )
      const songDataFromApi = {
        ...dataResponse,
        songUploading: 100,
        additional_data: JSON.parse(dataResponse.additional_data),
      }
      songDataFromApi.coverImageb64 = await fetchAsBlob(
        songDataFromApi.additional_data.cover_image,
      ).then(convertBlobToBase64)
      const song = objectMapper(songDataFromApi, MAPPER_FROM_API)
      await this.setState({
        song,
        isMoreDetails: !!song.albumName || !!song.year || !!song.composer || !isEmpty(song.genre),
      })
    }
  };

  _backHandler = async () => {
    const { step } = this.state
    if (step !== 1) {
      this.setState({ step: 1 })
    } else if (this.form && this.form.isDirtyState()) {
      this.backAlert.current?.snapTo(0)
    } else {
      const { navigateBack } = this.props
      navigateBack()
    }
  };

  _selectMusicFile = (onChange) => {
    onChange('songUploading')(1)
    DocumentPicker.getDocumentAsync({
      copyToCacheDirectory: isIOS(),
      type: 'audio/mpeg',
    }).then(async ({ type, uri, name, size }) => {
      if (type === 'success') {
        // SUCCESS
        try {
          const fetchResponse = await fetch(uri)
          const blob = await fetchResponse.blob()
          onChange('songFile')({
            uri,
            name: `${name}.mp3`,
            type: blob.type,
          })
          onChange('songSize')(size)

          if (size > 26214400) {
            this.setState({ isUploadOver: true })
            onChange('songFile')({})
            onChange('songSize')(null)
          } else {
            this.setState({ isUploadOver: false })
          }
        } catch (error) {
          // error
          onChange('songUploading')(0)
        }
      } else {
        // FAILED
        onChange('songUploading')(0)
      }
    })
  };

  _postMusic = async ({ formData }) => {
    const {
      id,
      userData,
      dispatch,
      getUserDetailDispatcher,
      refreshProfile,
      navigateBack,
      setClearBlueMessage,
    } = this.props
    this.setState({ isUploading: true })
    const id_song = !isEmpty(formData.songFile)
      ? await this._postMusicFile({ songFile: formData.songFile })
      : formData.songId
    const action = id ? putSongFile : postSongFile
    dispatch(action, {
      ...objectMapper(formData, MAPPER_FOR_API),
      id_user: userData.id_user,
      id_song,
      id_journey: id ? id : undefined,
    }).then(() => {
      this.setState({ isUploading: false })
      getUserDetailDispatcher({ id_user: userData.id_user })
      setClearBlueMessage(!id ? 'Lagu berhasil diupload' : 'Lagu berhasil diperbarui')
      isEmpty(id) && setAddProfile()
      refreshProfile()
      navigateBack()
    })
  };

  _postMusicFile = async ({ songFile }) => {
    const { userData } = this.props
    // let uriParts = songUrl.split('.')
    // let fileType = uriParts[uriParts.length - 1]
    const formData = new FormData()
    formData.append('id_user', userData.id_user)
    formData.append('music_file', songFile)
    const response = await postMusicFile(formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      onUploadProgress: (progressEvent) => {
        const percentCompleted = Math.round((progressEvent.loaded * 90) / progressEvent.total)
        this.setState({ percentCompleted })
      },
    })

    const { data } = response
    if (data.id_song) {
      return data.id_song
    } else {
      return Promise.reject('Upload failed')
    }
  };

  _isSongUploaded = (formData) => {
    return (
      !isEmpty(formData.songUrl) || (!isEmpty(formData.songFile) && !isNull(formData.songSize))
    )
  };
  _isImageUploaded = (formData) => {
    return !isEmpty(formData.coverImageb64)
  };

  _renderUploadingProgress = (formData) => (
    <View style={{ height: 8, backgroundColor: GREY_CALM_SEMI, width: WP70 }}>
      <View style={{ height: 8, backgroundColor: TOMATO, width: `${formData.songUploading}%` }} />
    </View>
  );

  _renderUploadingSuccess = (formData) => (
    <Player songUrl={formData.songUrl || formData.songFile?.uri}>
      {({
        toggle,
        isPlaying,
        changeAndPlay,
        songUrl,
        playbackInstancePosition,
        playbackInstanceDuration,
        isLoading,
      }) => {
        this.setState({
          playerState: { playbackInstancePosition, playbackInstanceDuration },
          isPlaying,
        })
        return (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {isLoading ? (
              <Image
                source={require('../../assets/loading_music.gif')}
                size='mini'
                style={{ marginVertical: 0, flexGrow: 0, marginRight: WP3, padding: WP1 }}
              />
            ) : (
              <Button
                style={{ marginVertical: 0 }}
                toggle
                toggleActiveColor={SILVER_CALMER}
                toggleInactiveTextColor={BLACK}
                contentStyle={{ padding: WP1 }}
                iconName={isPlaying ? 'pause' : 'play'}
                iconType='MaterialCommunityIcons'
                iconColor={BLACK}
                iconSize={WP15}
                onPress={() => {
                  if (!songUrl) {
                    changeAndPlay(formData.songUrl)
                  } else {
                    toggle()
                  }
                }}
                rounded
                shadow='none'
              />
            )}
          </View>
        )
      }}
    </Player>
  );

  ButtonIcon = ({ onPress = () => {}, title, iconName, iconType = 'MaterialCommunityIcons' }) => (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={TOUCH_OPACITY}
      style={{ flexDirection: 'row', alignItems: 'center' }}
    >
      <Icon
        centered
        background='dark-circle'
        size='small'
        color={ORANGE_BRIGHT}
        name={iconName}
        type={iconType}
      />
      <Text type='Circular' size='xmini' weight={400} color={ORANGE_BRIGHT}>
        {title}
      </Text>
    </TouchableOpacity>
  );

  _renderUploadButton = (formData, onChange) => (
    <Button
      style={{
        marginVertical: WP1,
        marginHorizontal: WP1,
        alignSelf: 'center',
      }}
      toggle
      toggleActiveColor={SILVER_CALMER}
      toggleInactiveTextColor={TOMATO}
      backgroundColor={TOMATO}
      marginless
      contentStyle={{
        paddingHorizontal: WP3,
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
      iconName='clouduploado'
      centered
      iconColor={TOMATO}
      onPress={() => this._selectMusicFile(onChange)}
      compact='center'
      rounded
      shadow='none'
      text='Upload Audio File *'
      textColor={WHITE}
      textSize='tiny'
      textWeight={500}
    />
  );
  _renderPlayerBar = () => {
    const {
      playerState: { playbackInstanceDuration, playbackInstancePosition },
    } = this.state
    if (!playbackInstanceDuration || !playbackInstancePosition) return null
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Icon name='equalizer' type='MaterialIcons' color={REDDISH} />
        <Text type='Circular' size='tiny' color={REDDISH} style={{ marginHorizontal: WP1 }}>
          {getTime(playbackInstancePosition)}
        </Text>
        <View style={{ height: 3, backgroundColor: PALE_BLUE_TWO, width: WP20 }}>
          <View
            style={{
              height: 3,
              backgroundColor: REDDISH,
              width: WP20 * (playbackInstancePosition / playbackInstanceDuration),
            }}
          />
        </View>
        <Text type='Circular' size='tiny' color={REDDISH} style={{ marginHorizontal: WP1 }}>
          {getTime(playbackInstanceDuration)}
        </Text>
      </View>
    )
  };

  _renderUpload = (formData, onChange) => {
    const uploadStatus = this._getUploadStatus(formData)
    if (uploadStatus === 'onprogress') return this._renderUploadingProgress(formData, onChange)
    else if (uploadStatus === 'onsuccess') return this._renderUploadingSuccess(formData, onChange)
    else if (uploadStatus === 'onupload') return this._renderUploadButton(formData, onChange)
    else return <Loader isLoading />
  };

  _renderButton = (text, onPress) => (
    <ButtonV2
      onPress={onPress}
      color={REDDISH}
      textColor={WHITE}
      style={{ paddingHorizontal: WP5, width: WP60, marginVertical: HP2 }}
      text={text}
    />
  );

  _getUploadStatus = (formData) => {
    if (!formData.songId && formData.songUploading <= 0) return 'onupload'
    else if (formData.songUploading > 0 && formData.songUploading <= 100 && !formData.songId)
      return 'onprogress'
    else if (formData.songUploading >= 100 || formData.songId) return 'onsuccess'
    else return 'onloading'
  };

  render() {
    const { id, navigateTo, navigateBack, paymentSourceSet, accountType } = this.props
    const { isReady, song, step, isUpload, isUploadOver, isUploading, isPlaying } = this.state
    return (
      <Form
        ref={(form) => (this.form = form)}
        validation={FORM_VALIDATION}
        initialValue={{ genre: [], ...song }}
        onSubmit={this._postMusic}
        onChangeInterceptor={({ state, onChange }) => {
          if (state.errors.coverImageSize) {
            this.setState({ isUploadOver: true })
            onChange('coverImageSize')(0)
            onChange('coverImageUri')('')
            onChange('coverImageb64')('')
          }
        }}
      >
        {({ onChange, onSubmit, formData, isValid }) => (
          <>
            <Container
              isReady={isReady}
              renderHeader={() => (
                <HeaderNormal
                  iconLeftOnPress={this._backHandler}
                  iconLeftWrapperStyle={{ width: WP8, marginRight: WP10 }}
                  textType='Circular'
                  textColor={SHIP_GREY}
                  textWeight={400}
                  textSize={'slight'}
                  text={id ? 'Edit Lagu' : 'Tambahkan Lagu'}
                  centered
                  rightComponent={
                    <View style={{ width: WP25, paddingVertical: WP3, alignItems: 'flex-end' }}>
                      {step === 1 && !isUploading && (
                        <TouchableOpacity
                          onPress={() => {
                            Keyboard.dismiss()
                            onSubmit()
                          }}
                          style={{ width: '100%', paddingRight: WP5 }}
                          activeOpacity={TOUCH_OPACITY}
                          disabled={!isValid}
                        >
                          <Text
                            style={{ textAlign: 'right' }}
                            size='slight'
                            weight={400}
                            color={isValid ? ORANGE_BRIGHT : PALE_SALMON}
                          >
                            Publish
                          </Text>
                        </TouchableOpacity>
                      )}
                      {step === 1 && isUploading && <ActivityIndicator color={REDDISH} />}
                    </View>
                  }
                >
                  <View style={{ width: WP100, height: WP05, backgroundColor: PALE_GREY }}>
                    <Animated.View
                      style={{
                        height: WP05,
                        backgroundColor: REDDISH,
                        width: WP100 * (this.state.percentCompleted / 100),
                      }}
                    />
                  </View>
                </HeaderNormal>
              )}
              isAvoidingView
              scrollable
              scrollBackgroundColor={PALE_WHITE}
            >
              {step === 1 && (
                <View style={{ paddingHorizontal: WP5, marginBottom: HP10 }}>
                  <Text
                    type='Circular'
                    size='xmini'
                    weight={500}
                    color={GUN_METAL}
                    style={{ marginVertical: HP2 }}
                  >
                    ADD SONG & ARTWORK
                  </Text>
                  <Card
                    onPress={() => this.setState({ step: 2 })}
                    style={[{ borderRadius: 5, marginVertical: HP2 }, SHADOW_STYLE['shadowThin']]}
                  >
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}
                    >
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}
                      >
                        <View
                          style={{
                            borderRadius: 7,
                            width: WP15,
                            height: WP15,
                            marginRight: WP4,
                            backgroundColor: this._isSongUploaded(formData) ? NAVY_DARK : PALE_GREY,
                            alignItems: 'center',
                          }}
                        >
                          <Image
                            source={require('../../assets/icons/icMusicalNotes.png')}
                            imageStyle={{ width: WP10, aspectRatio: 1 }}
                            style={{ width: WP10, marginTop: WP205 }}
                          />
                        </View>
                        <View style={{ flex: 1 }}>
                          <Text
                            style={{ marginBottom: WP05 }}
                            color={GUN_METAL}
                            weight={400}
                            size='small'
                            type='Circular'
                            numberOfLines={1}
                          >
                            {this._isSongUploaded(formData)
                              ? formData.songFile?.name || formData.songName
                              : 'Upload Audio File'}
                          </Text>
                          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            {this._isSongUploaded(formData) ? (
                              <Text type='Circular' size='mini' color={REDDISH}>
                                Ubah lagu
                              </Text>
                            ) : (
                              <Text
                                lineHeight={WP5}
                                type='Circular'
                                size='mini'
                                color={SHIP_GREY_CALM}
                              >
                                Upload lagumu max. 25 mb (.mp3)
                              </Text>
                            )}
                          </View>
                        </View>
                        <View>
                          <Icon
                            size='large'
                            name='ios-arrow-forward'
                            type='Ionicons'
                            centered
                            color={SHIP_GREY_CALM}
                          />
                        </View>
                      </View>
                    </View>
                  </Card>
                  <Card
                    onPress={() => this.setState({ step: 3 })}
                    style={[{ borderRadius: 5, marginVertical: HP2 }, SHADOW_STYLE['shadowThin']]}
                  >
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      }}
                    >
                      <View
                        style={{
                          flexDirection: 'row',
                          flexShrink: 1,
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}
                      >
                        {this._isImageUploaded(formData) ? (
                          <Image
                            source={{ uri: formData.coverImageUri }}
                            imageStyle={{ width: WP15, borderRadius: 7, aspectRatio: 1 }}
                            style={{
                              borderRadius: 7,
                              width: WP15,
                              height: WP15,
                              marginRight: WP4,
                              backgroundColor: PALE_GREY,
                              alignItems: 'center',
                            }}
                          />
                        ) : (
                          <View
                            style={{
                              borderRadius: 7,
                              width: WP15,
                              height: WP15,
                              marginRight: WP4,
                              backgroundColor: PALE_GREY,
                              alignItems: 'center',
                            }}
                          >
                            <Image
                              source={require('../../assets/icons/icAddPhoto.png')}
                              imageStyle={{ width: WP10, aspectRatio: 1 }}
                              style={{ width: WP10, marginTop: WP205 }}
                            />
                          </View>
                        )}
                        <View style={{ flex: 1 }}>
                          <Text
                            style={{ marginBottom: WP05 }}
                            color={GUN_METAL}
                            weight={400}
                            size='small'
                            type='Circular'
                            numberOfLines={1}
                          >
                            {this._isImageUploaded(formData)
                              ? formData.coverImageName || formData.songName
                              : 'Upload Image/Artwork'}
                          </Text>
                          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            {this._isImageUploaded(formData) ? (
                              <Text type='Circular' size='mini' color={REDDISH}>
                                Ubah foto
                              </Text>
                            ) : (
                              <Text
                                lineHeight={WP5}
                                type='Circular'
                                size='mini'
                                color={SHIP_GREY_CALM}
                              >
                                Upload file max. 10 mb (.jpg/.png)
                              </Text>
                            )}
                          </View>
                        </View>
                        <View>
                          <Icon
                            size='large'
                            name='ios-arrow-forward'
                            type='Ionicons'
                            centered
                            color={SHIP_GREY_CALM}
                          />
                        </View>
                      </View>
                    </View>
                  </Card>
                  <View
                    style={{
                      width: WP100,
                      height: 1,
                      backgroundColor: PALE_BLUE,
                      marginVertical: HP2,
                      marginLeft: -WP5,
                    }}
                  />
                  <Text
                    type='Circular'
                    size='xmini'
                    weight={500}
                    color={GUN_METAL}
                    style={{ marginVertical: HP2 }}
                  >
                    BASIC INFORMATIONS
                  </Text>
                  <InputTextLight
                    withLabel={false}
                    placeholder='Tuliskan judul lagu karyamu'
                    value={formData.title}
                    onChangeText={onChange('title')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Song Title'
                    multiline
                  />
                  <InputTextLight
                    withLabel={false}
                    placeholder='Tuliskan nama album dari lagumu'
                    value={formData.albumName}
                    onChangeText={onChange('albumName')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Album Name'
                    multiline
                  />
                  <InputTextLight
                    withLabel={false}
                    placeholder='Tuliskan nama artist pada lagumu'
                    value={formData.artistName}
                    onChangeText={onChange('artistName')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Artist Name'
                    multiline
                  />
                  <Label title='Genre' style={{ marginBottom: HP1 }} />
                  <SelectedItem
                    items={formData.genre}
                    onClose={(index) => {
                      let newGenre = formData.genre
                      newGenre.splice(index, 1)
                      onChange('genre')(newGenre)
                    }}
                  />
                  <SelectModalV3
                    refreshOnSelect
                    triggerComponent={
                      <this.ButtonIcon
                        iconName={id ? 'pencil' : 'plus'}
                        title={id ? 'Ubah Genre' : 'Tambahkan Genre'}
                      />
                    }
                    header='Cari Genre'
                    suggestion={getGenreInterest}
                    suggestionKey='interest_name'
                    suggestionPathResult='interest_name'
                    onChange={(value) => {
                      let newGenre = formData.genre
                      includes(newGenre, value)
                        ? remove(newGenre, (genre) => genre === value)
                        : newGenre.push(value)
                      onChange('genre')(newGenre)
                    }}
                    createNew={false}
                    placeholder='Coba cari Genre'
                    selection={formData.professions}
                    selectionKey='name'
                    selectionWording='Genre Terpilih'
                    showSelection
                    onRemoveSelection={(index) => {
                      const newGenre = formData.genre
                      newGenre.splice(index, 1)
                      onChange('genre')(newGenre)
                    }}
                  />

                  <InputTextLight
                    withLabel={false}
                    keyboardType='number-pad'
                    placeholder='Tahun pembuatan lagumu. Contoh: 2020'
                    wording=' '
                    value={formData.year}
                    onChangeText={onChange('year')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    showLength={false}
                    maxLength={4}
                    lineHeight={1}
                    style={{ marginTop: HP105, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Tahun Release'
                  />
                </View>
              )}
              {step === 2 && (
                <View style={{ backgroundColor: PALE_WHITE }}>
                  <ImageBackground
                    source={require('../../assets/images/bgAddSong.png')}
                    imageStyle={{
                      width: WP100,
                      aspectRatio: 360 / 332,
                      resizeMode: 'contain',
                      position: 'absolute',
                      top: 0,
                      right: 0,
                      bottom: 'auto',
                    }}
                    style={{
                      width: WP100,
                      backgroundColor: PALE_WHITE,
                      paddingVertical: HP2,
                    }}
                  >
                    <Card
                      style={[
                        {
                          borderRadius: 12,
                          marginVertical: HP2,
                          marginHorizontal: WP5,
                          padding: 0,
                        },
                        SHADOW_STYLE['shadowThin'],
                      ]}
                    >
                      <View
                        style={{
                          alignItems: 'center',
                          alignSelf: 'stretch',
                          paddingBottom: HP2,
                          borderRadius: 12,
                        }}
                      >
                        <View
                          style={{
                            borderTopLeftRadius: 12,
                            height: (WP90 * 192) / 312,
                            borderTopRightRadius: 12,
                            paddingVertical: HP4,
                            width: WP90,
                            marginBottom: HP2,
                            alignSelf: 'stretch',
                            alignItems: 'center',
                          }}
                        >
                          {!isEmpty(formData.songFile) && !isNull(formData.songSize) ? (
                            <>
                              <Image
                                source={require('../../assets/images/Bannersong.png')}
                                imageStyle={{
                                  width: WP90,
                                  aspectRatio: 312 / 192,
                                  position: 'absolute',
                                  top: 0,
                                  left: 0,
                                  right: 0,
                                  marginLeft: -WP45,
                                  marginTop: -HP4,
                                }}
                              />
                              {this._renderUploadingSuccess(formData)}
                            </>
                          ) : (
                            <>
                              <Icon
                                centered
                                type='Ionicons'
                                name='ios-musical-notes'
                                size={WP35}
                                color={PALE_BLUE}
                              />
                            </>
                          )}
                        </View>
                        {!isEmpty(formData.songFile) && !isNull(formData.songSize) ? (
                          <>
                            <Text
                              numberOfLines={1}
                              style={{ marginVertical: HP2 }}
                              centered
                              type='Circular'
                              size='large'
                              weight={500}
                              color={GUN_METAL}
                            >
                              {formData.songFile?.name}
                            </Text>
                            {isPlaying ? (
                              this._renderPlayerBar()
                            ) : (
                              <Text
                                type='Circular'
                                size='small'
                                centered
                                color={SHIP_GREY}
                                style={{ margin: HP1 }}
                              >
                                {formatBytes(formData.songSize, true)}
                              </Text>
                            )}
                            {this._renderButton('Selanjutnya', () => {
                              this.setState({ step: 1 })
                            })}
                            <Text
                              onPress={() => {
                                this._selectMusicFile(onChange)
                              }}
                              type='Circular'
                              size='xmini'
                              weight={500}
                              centered
                              color={SHIP_GREY}
                              style={{ margin: HP1 }}
                            >
                              Ganti lagu
                            </Text>
                          </>
                        ) : (
                          <>
                            <Text
                              style={{ marginVertical: HP2 }}
                              type='Circular'
                              size='large'
                              weight={500}
                              color={GUN_METAL}
                            >
                              Upload Audio File
                            </Text>
                            <Text
                              lineHeight={WP5}
                              type='Circular'
                              size='small'
                              centered
                              color={SHIP_GREY}
                              style={{ margin: HP1 }}
                            >
                              Upload lagumu dan bagikan kepada semua orang sekarang!
                            </Text>
                            {this._renderButton('Upload lagu', () => {
                              this._selectMusicFile(onChange)
                            })}
                            {isUploadOver && (
                              <Text type='Circular' size='mini' centered color={SHIP_GREY_CALM}>
                                Failed, file too big
                              </Text>
                            )}
                            <Text
                              lineHeight={WP5}
                              type='Circular'
                              size='mini'
                              centered
                              color={SHIP_GREY_CALM}
                            >
                              Upload file max. 25 mb (.mp3)
                            </Text>
                          </>
                        )}
                      </View>
                    </Card>
                    {!isIOS() && !isEmpty(accountType) && (
                      <>
                        <Text
                          style={{ marginHorizontal: WP10, marginVertical: HP2 }}
                          type='Circular'
                          size='mini'
                          centered
                          color={SHIP_GREY_CALM}
                        >
                          Saat ini kamu hanya dapat mengupload
                          <Text
                            style={{ marginHorizontal: WP10, marginVertical: HP2 }}
                            type='Circular'
                            size='mini'
                            weight={500}
                            centered
                            color={SHIP_GREY_CALM}
                          >{` ${configPackage[toLower(accountType)]?.limit} lagu`}</Text>
                          .
                          {accountType === 'free'
                            ? ' Mau upload lebih banyak? daftarkan dirimu ke paket premium sekarang.'
                            : ''}
                        </Text>

                        <Text
                          onPress={() => {
                            paymentSourceSet('uploadfile')
                            navigateTo('MembershipScreen')
                          }}
                          type='Circular'
                          centered
                          size='mini'
                          weight={400}
                          color={REDDISH}
                        >
                          {['pro', 'maestro'].includes(toLower(accountType))
                            ? ''
                            : 'Lihat & Daftar Paket Premium'}
                        </Text>
                      </>
                    )}
                  </ImageBackground>
                </View>
              )}
              {step === 3 && (
                <View style={{ paddingVertical: HP1, alignItems: 'center' }}>
                  <Image
                    source={
                      !isEmpty(formData.coverImageUri)
                        ? { uri: formData.coverImageUri }
                        : require('../../assets/images/uploadPhotoPlaceholder.png')
                    }
                    imageStyle={{
                      width: WP90,
                      aspectRatio: !isEmpty(formData.coverImageUri) ? 1 : 360 / 376,
                      borderRadius: 15,
                      overflow: 'hidden',
                    }}
                    style={{ width: WP90, marginVertical: HP2 }}
                  />
                  <Text type='Circular' size='large' weight={500} color={GUN_METAL}>
                    {'Upload Artwork'}
                  </Text>
                  <Text
                    type='Circular'
                    size='small'
                    centered
                    color={SHIP_GREY}
                    style={{ margin: HP1 }}
                  >
                    {!isEmpty(formData.coverImageName)
                      ? formData.coverImageName
                      : 'Upload gambar artwork dari lagumu'}
                  </Text>
                  <ButtonV2
                    onPress={
                      isEmpty(formData.coverImageUri)
                        ? selectPhoto(onChange, 'coverImage', [1, 1])
                        : () => {
                            this.setState({ step: 1 })
                          }
                    }
                    color={REDDISH}
                    textColor={WHITE}
                    style={{ paddingHorizontal: WP5, width: WP60, marginVertical: HP2 }}
                    text={!isEmpty(formData.coverImageUri) ? 'Selanjutnya' : 'Upload File Artwork'}
                  />
                  {isUploadOver && (
                    <Text type='Circular' size='mini' centered color={SHIP_GREY_CALM}>
                      Failed, too big image
                    </Text>
                  )}
                  {!isEmpty(formData.coverImageUri) ? (
                    <Text
                      onPress={selectPhoto(onChange, 'coverImage', [1, 1])}
                      type='Circular'
                      size='xmini'
                      weight={500}
                      centered
                      color={SHIP_GREY_CALM}
                      style={{ margin: HP1 }}
                    >
                      Ganti foto
                    </Text>
                  ) : (
                    <Text
                      numberOfLines={1}
                      type='Circular'
                      size='mini'
                      centered
                      color={SHIP_GREY_CALM}
                    >
                      {isUpload
                        ? 'Photo berhasil diupload'
                        : 'Upload file max. 10 mb (jpg, jpeg, png)'}
                    </Text>
                  )}
                </View>
              )}
            </Container>
            <BottomSheet
              innerRef={this.backAlert}
              snapPoints={[HP50, -HP100]}
              renderContent={({ closeBottomSheet }) => (
                <View style={{ padding: WP6, justifyContent: 'space-between', flex: 1 }}>
                  <Image
                    source={require('../../assets/images/confirm/addMusicCancelConfirm.png')}
                    imageStyle={{ width: WP50 }}
                    aspectRatio={168 / 90}
                  />
                  <Text centered type='NeoSans' weight={600} size='massive'>
                    Upss, are you sure?
                  </Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Button
                      style={{ flex: 1, marginRight: WP3 }}
                      onPress={closeBottomSheet}
                      backgroundColor={TOMATO}
                      toggle
                      toggleActiveColor={WHITE}
                      toggleInactiveTextColor={TOMATO}
                      centered
                      bottomButton
                      marginless
                      radius={10}
                      shadow='none'
                      textType='NeoSans'
                      textSize='small'
                      textColor={WHITE}
                      textWeight={500}
                      text={'Back'}
                    />
                    <Button
                      style={{ flex: 1, marginLeft: WP3 }}
                      onPress={() => {
                        closeBottomSheet()
                        navigateBack()
                      }}
                      backgroundColor={TOMATO}
                      centered
                      bottomButton
                      marginless
                      radius={10}
                      shadow='none'
                      textType='NeoSans'
                      textSize='small'
                      textColor={WHITE}
                      textWeight={500}
                      text='Discard'
                    />
                  </View>
                </View>
              )}
            />
            <BottomSheet
              innerRef={this.confirmAlert}
              snapPoints={[HP50, -HP100]}
              renderContent={({ closeBottomSheet }) => (
                <View style={{ padding: WP6, justifyContent: 'space-between', flex: 1 }}>
                  <Image
                    source={require('../../assets/images/confirm/addMusicUploadConfirm.png')}
                    imageStyle={{ width: WP50 }}
                    aspectRatio={168 / 82}
                  />
                  <Text centered type='NeoSans' weight={600} size='large'>
                    {id ? 'Are you sure about changes?' : 'Are you sure for added this ?'}
                  </Text>
                  {!id && (
                    <Text centered size='tiny'>
                      Please make sure again, everything on your works information is already
                      correct?
                    </Text>
                  )}
                  <View style={{ flexDirection: 'row' }}>
                    <Button
                      style={{ flex: 1, marginRight: WP3 }}
                      onPress={closeBottomSheet}
                      backgroundColor={TOMATO}
                      toggle
                      toggleActiveColor={WHITE}
                      toggleInactiveTextColor={TOMATO}
                      centered
                      bottomButton
                      marginless
                      radius={10}
                      shadow='none'
                      textType='NeoSans'
                      textSize='small'
                      textColor={WHITE}
                      textWeight={500}
                      text={'No'}
                    />
                    <Button
                      style={{ flex: 1, marginLeft: WP3 }}
                      onPress={() => {
                        closeBottomSheet()
                        onSubmit()
                      }}
                      backgroundColor={TOMATO}
                      centered
                      bottomButton
                      marginless
                      radius={10}
                      shadow='none'
                      textType='NeoSans'
                      textSize='small'
                      textColor={WHITE}
                      textWeight={500}
                      text='Yes'
                    />
                  </View>
                </View>
              )}
            />
          </>
        )}
      </Form>
    )
  }
}

SongFileForm.navigationOptions = () => ({
  gesturesEnabled: false,
})
SongFileForm.defaultProps = propsDefault
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SongFileForm),
  mapFromNavigationParam,
)
