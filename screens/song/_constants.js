/* @flow */

import moment from 'moment'
import * as yup from 'yup'

export const DUMP_DATA_FROM_API = {}

export const DUMP_DATA_FOR_API = {}

export const DUMP_SONG_DATA = {}

// SONG_DATA Validation
export const FORM_VALIDATION = yup.object().shape({
  title: yup.string().required(),
  albumName: yup.string().required(),
  artistName: yup.string().required(),
  coverImageUri: yup.string().required(),
  genre: yup.array().min(1),
  year: yup.string().required(),
  coverImageSize: yup.number().max(10485760, 'Photo size should be less then 10 MB'),
  songSize: yup
    .number()
    .required()
    .max(26214400, 'Song size should be less then 25 MB'),
})

// API_DATA to SONG_DATA
export const MAPPER_FROM_API = {
  id: 'additional_data.id_song',
  title: 'title',
  description: 'description',
  songUrl: 'additional_data.url_song',
  songSize: () => 0,
  artistName: 'additional_data.artist_name',
  coverImageUri: 'additional_data.cover_image',
  coverImageb64: 'coverImageb64',
  albumName: 'album_name',
  year: 'additional_data.year',
  composer: 'additional_data.composer',
  genre: 'additional_data.genre',

  // Player
  songUploading: 'songUploading',
  songName: 'title',
  songType: '',
  songId: 'additional_data.id_song',

  // Audit
  createdBy: 'created_by',
  createdAt: (source) => moment(source.created_at).fromNow(false),
  updatedBy: 'updated_by',
  updatedAt: 'updated_at',
  isNew: (source) => moment().diff(moment(source.update_at), 'days') <= 3,

  // Author Profile
  authorPicture: 'profile_picture',
  authorId: 'id_user',
  authorJobTitle: 'job_title',
  authorCompany: 'company',
  authorCityName: 'location.city_name',
  authorIsPremium: 'is_premium',
}

// SONG_DATA to API_DATA
export const MAPPER_FOR_API = {
  title: 'title',
  id_song: 'songId',
  description: 'description',
  url_song: 'songUrl',
  size_song: 'songSize',
  artist_name: 'artistName',
  cover_image: 'coverImageb64',
  album_name: 'albumName',
  year: 'year',
  composer: 'composer',
  genre: 'genre',
}

export type Song = {
  id: string | number,
  title: string,
  description: string,
  songUrl: string,
  songSize: string,
  artistName: string,
  coverImageUri: string,
  coverImageb64: string,
  albumName: string,
  year: string,
  composer: string,
  genre: Array,

  createdBy: string,
  createdAt: string,
  updatedBy: string,
  updatedAt: string,
  isNew: boolean,
  authorPicture: string,
  authorId: string | number,
  authorJobTitle: string,
  authorCompany: string,
  authorCityName: string,
  authorIsPremium: boolean,
};

export type PropsType = {
  initialLoaded?: boolean,
  id?: number | string,
  previewData?: Song,
};

export type StateType = {
  isReady?: boolean,
  collaboration?: Song,
  hasPreviewData?: boolean,
};
