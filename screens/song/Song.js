import React from 'react'
import { connect } from 'react-redux'
import { TouchableOpacity, View } from 'react-native'
import moment from 'moment'
import { noop, find, toLower } from 'lodash-es'
import {
  _enhancedNavigation,
  Container,
  Text,
  HeaderNormal,
  Image,
  BottomSheet,
  Button,
} from '../../components'
import { SILVER_CALMER, WHITE, TOMATO } from '../../constants/Colors'
import {
  HP1,
  WP10,
  WP100,
  WP20,
  WP4,
  WP5,
  WP75,
  WP90,
  WP6,
  HP15,
  HP50,
} from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { authDispatcher } from '../../services/auth'
import { getSettingDetail } from '../../actions/api'
import { DEFAULT_PAGING } from '../../constants/Routes'
import { isIOS } from '../../utils/helper'
import { paymentDispatcher } from '../../services/payment'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', () => {}),
  limit: getParam('limit', false),
  userType: getParam('userType', 'free'),
  navigateBackOnDone: getParam('navigateBackOnDone', false),
  initialLoaded: getParam('initialLoaded', false),
})

const message = {
  free: {
    message: 'You have reached upload limit, upgrade to upload more songs.',
    button: 'Upgrade to Premium',
    action: (navigateTo, navigateBack, packages, paymentSourceSet) => {
      paymentSourceSet('song_file_upload')
      navigateTo('MembershipScreen')
    },
  },
  basic: {
    message: 'You have reached upload limit, upgrade to upload more songs.',
    button: 'Upgrade to Premium Pro',
    action: (navigateTo, navigateBack, packages) => {
      const currentPackage = find(
        packages,
        (packageDetail) => toLower(packageDetail.package_name) === 'pro',
      )
      navigateTo('PromoteUser3', {
        paymentStatus: 'upgrade',
        selectedPackage: currentPackage,
        startDate: moment().format('DD/MM/YYYY'),
      })
    },
  },
  pro: {
    message: 'You have reached upload limit',
    button: 'Back to profile',
    action: (navigateTo, navigateBack) => {
      navigateBack()
    },
  },
}

class Song extends React.Component {
  state = {
    packages: [],
    isReady: false,
  };

  async componentDidMount() {
    await this._getInitialScreenData()
  }

  _getInitialScreenData = async (...args) => {
    await this._getPackages(...args)
    this.setState({ isReady: true })
  };

  _backHandler = async () => {
    this.props.navigateBack()
  };

  limitAlert = React.createRef();

  _getPackages = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const { dispatch } = this.props
    const dataResponse = await dispatch(
      getSettingDetail,
      {
        setting_name: 'premium_member',
        key_name: 'package',
      },
      noop,
      false,
      isLoading,
    )
    const packages = JSON.parse(dataResponse.value)
    this.setState({
      packages,
    })
  };

  render() {
    const {
      isLoading,
      navigateBack,
      navigateTo,
      refreshProfile,
      navigateBackOnDone,
      limit,
      userType,
      paymentSourceSet,
    } = this.props
    const { isReady, packages } = this.state
    return (
      <Container
        isLoading={isLoading}
        colors={[WHITE, WHITE]}
        scrollable
        isReady={isReady}
        renderHeader={() => (
          <HeaderNormal iconLeftOnPress={this._backHandler} text='Add Song' centered />
        )}
        outsideScrollContent={() => (
          <BottomSheet
            backdropClose
            innerRef={this.limitAlert}
            snapPoints={[HP50, 0]}
            renderContent={({ closeBottomSheet }) => (
              <View
                style={{ padding: WP6, justifyContent: 'center', alignItems: 'center', flex: 1 }}
              >
                <Image
                  source={require('../../assets/images/limitModalImage.png')}
                  imageStyle={{ height: HP15 }}
                  aspectRatio={182 / 108}
                />
                <Text
                  centered
                  type='NeoSans'
                  style={{ marginVertical: HP1 }}
                  weight={600}
                  size='massive'
                >
                  Oops...!
                </Text>
                {isIOS() ? (
                  <Text
                    centered
                    type='NeoSans'
                    style={{ paddingHorizontal: WP5, marginVertical: HP1 }}
                    size='mini'
                  >
                    Unfortunately{' '}
                    <Text
                      centered
                      type='NeoSans'
                      weight={500}
                      style={{ paddingHorizontal: WP5, marginVertical: HP1 }}
                      size='mini'
                    >
                      you have reached upload limit
                    </Text>
                    , this feature is only for premium member and you can’t upgrade it directly on
                    our apps. Please contact administrator for further information.
                  </Text>
                ) : (
                  <Text
                    centered
                    type='NeoSans'
                    style={{ paddingHorizontal: WP20, marginVertical: HP1 }}
                    size='mini'
                  >
                    {message[userType]?.message}
                  </Text>
                )}
                {!isIOS() && (
                  <View style={{ flexDirection: 'row' }}>
                    <Button
                      style={{ flex: 1, marginVertical: HP1 }}
                      onPress={() =>
                        message[userType]?.action(
                          navigateTo,
                          navigateBack,
                          packages,
                          paymentSourceSet,
                        )
                      }
                      backgroundColor={TOMATO}
                      centered
                      bottomButton
                      marginless
                      radius={10}
                      shadow='none'
                      textType='NeoSans'
                      textSize='small'
                      textColor={WHITE}
                      textWeight={500}
                      text={message[userType]?.button}
                    />
                  </View>
                )}
              </View>
            )}
          />
        )}
      >
        <View style={{ alignItems: 'center', paddingVertical: HP1, paddingHorizontal: WP4 }}>
          <Image
            size={WP75}
            source={require('../../assets/images/addMusic.png')}
            aspectRatio={222 / 205}
          />

          <TouchableOpacity
            style={{ alignItems: 'center', marginTop: WP10 }}
            activeOpacity={TOUCH_OPACITY}
            onPress={() => {
              navigateTo(
                'SongLinkForm',
                { refreshProfile },
                navigateBackOnDone ? 'replace' : undefined,
              )
            }}
          >
            <View
              style={{
                marginHorizontal: WP5,
                padding: WP4,
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: SILVER_CALMER,
                width: WP90,
                borderRadius: WP100,
              }}
            >
              <Image size={WP20} source={require('../../assets/images/uploadLink.png')} />
              <View style={{ paddingHorizontal: WP4 }}>
                <Text type='SansPro' weight={500}>
                  Add songs from link
                </Text>
                <Text size='tiny'>Spotify, Joox, Soundcloud, etc</Text>
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={{ alignItems: 'center', marginTop: WP5 }}
            activeOpacity={TOUCH_OPACITY}
            onPress={() => {
              if (limit) {
                this.limitAlert.current?.snapTo(0)
              } else {
                navigateTo(
                  'SongFileForm',
                  { refreshProfile },
                  navigateBackOnDone ? 'replace' : undefined,
                )
              }
            }}
          >
            <View
              style={{
                marginHorizontal: WP5,
                padding: WP4,
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: SILVER_CALMER,
                width: WP90,
                borderRadius: WP100,
              }}
            >
              <Image size={WP20} source={require('../../assets/images/uploadFile.png')} />
              <View style={{ paddingHorizontal: WP4 }}>
                <Text type='SansPro' weight={500}>
                  Add Songs from Storage
                </Text>
                <Text size='tiny'>Internal phone storage</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(Song),
  mapFromNavigationParam,
)
