import React from 'react'
import { connect } from 'react-redux'
import { WebView } from 'react-native-webview'
import {
  _enhancedNavigation,
  Container,
  Header,
  Icon,
  Text,
} from '../../components'
import { NO_COLOR } from '../../constants/Colors'
import { authDispatcher } from '../../services/auth'
import env from '../../utils/env'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', () => {}),
  navigateBackOnDone: getParam('navigateBackOnDone', false),
})

class SongLagukuForm extends React.Component {
  state = {
    isLoading: true,
  };

  render() {
    const { userData, navigateBack, isLoading: isLoadingProps } = this.props
    const { isLoading } = this.state
    const paymentUrl = `${env.webUrl}/register/laguku`
    const queries = `?id_user=${userData.id_user}`
    return (
      <Container
        isLoading={isLoading || isLoadingProps}
        renderHeader={() => (
          <Header>
            <Icon
              onPress={navigateBack}
              size='huge'
              name='chevron-left'
              type='Entypo'
              centered
            />
            <Text size='massive' type='SansPro' weight={500}>
              Connect to laguku.id
            </Text>
            <Icon size='massive' color={NO_COLOR} name='left' />
          </Header>
        )}
      >
        <WebView
          source={{ uri: paymentUrl + queries }}
          allowFileAccess
          onLoadStart={() => {
            this.setState({ isLoading: true })
          }}
          onLoadEnd={() => {
            this.setState({ isLoading: false })
          }}
          onNavigationStateChange={async (state) => {
            if (state.url.includes('payment/ads/finish_mobile')) {
              // await dispatch(postPromoteBand, artistData, noop, false, true)
              // this.props.navigation.popToTop()
              // navigateTo('NotificationStack')
            }
          }}
        />
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SongLagukuForm),
  mapFromNavigationParam,
)
