import { WP35, WP6, WP75, WP8 } from '../../constants/Sizes'

const layouts = {
  title: {
    width: WP35,
    height: WP8
  },
  benefit: {
    width: WP75,
    height: WP6
  }
}

export default {
  layouts
}
