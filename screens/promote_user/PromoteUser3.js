import React, { Component } from 'react'
import moment from 'moment'
import { map, noop, isEmpty, lowerCase, upperCase, reduce, snakeCase } from 'lodash-es'
import { View, TouchableOpacity, Keyboard, ScrollView } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { connect } from 'react-redux'
import idx from 'idx'
import { Header, Icon, Image, Text, Modal, Container, Button, InputTextLight } from '../../components'
import {
  GREY,
  ORANGE_BRIGHT, PINK_PURPLE,
  PURPLE, SILVER,
  SILVER_WHITE,
  WHITE,
  WARM_PURPLE
} from '../../constants/Colors'
import {
  HP1, HP2, WP1,
  WP10,
  WP2, WP20,
  WP4,
  WP6
} from '../../constants/Sizes'
import { getSettingDetail, getSubscriptionVoucher } from '../../actions/api'
import { BORDER_COLOR, BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'
import { currencyFormatter } from '../../utils/helper'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { DEFAULT_PAGING } from '../../constants/Routes'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  selectedPackage: getParam('selectedPackage', {}),
  startDate: getParam('startDate', moment().format('DD/MM/YYYY')),
  paymentStatus: getParam('paymentStatus', 'new')
})

class PromoteUser3 extends Component {
  state = {
    isSelectedMonth: 'per_1_month',
    costPerMonth: 0,
    voucherCodeInput: '',
    voucherCode: null,
    voucherDiscount: 0,
    subscription: [],
    selectedPackage: this.props.selectedPackage
  }

  async componentDidMount() {
    await this._getInitialScreenData()
  }

  _getInitialScreenData = async (...args) => {
    await this._getData(...args)
    this.setState({
      isRefreshing: false
    })
  }

  _getData = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const {
      dispatch
    } = this.props
    //getting data
    const dataResponse = await dispatch(getSettingDetail, {
      'setting_name': 'premium_member',
      'key_name': 'price',
      'package': lowerCase(this.state.selectedPackage.package_name)
    }, noop, false, isLoading)
    const prices = JSON.parse(dataResponse.value)
    let isSelectedMonth = ''
    const packagePrices = reduce(prices.package, (result, pack, index) => {
      let key = snakeCase(pack.duration)
      if(index === 1) isSelectedMonth = key
      result[key] = {
        key,
        name: pack.duration,
        price: pack.discount_price,
        priceText: pack.discount,
        subscriptionDurationInMonth: Number(idx(pack, (_) => ((_.duration).split(' ')[0])))
      }
      return result
    }, {})
    this.setState({
      subscription: packagePrices,
      isSelectedMonth
    })
    // this.setState((state, props) => ({}))
  }

  _getVoucher = async () => {
    const {
      dispatch
    } = this.props

    const {
      voucherCode,
      selectedPackage
    } = this.state

    if (!isEmpty(voucherCode)) {
      const dataResponse = await dispatch(getSubscriptionVoucher, { voucher_code: voucherCode, type: 'premium_user', premium_type: selectedPackage.package_name }, noop, false, true)
      this.setState({
        voucherDiscount: Number(dataResponse.amount_discount || 0)
      })
    }
  }

  _onSelectedPackage = (selectedPackage) => {
    this.scrollSubscription.scrollTo({ x: WP20, y: 0, animated: true })
    this.setState({
      selectedPackage
    }, this._getInitialScreenData)
  }

  _onSubmit = () => {
    const {
      paymentStatus,
      navigateTo
    } = this.props

    const {
      subscription,
      isSelectedMonth
    } = this.state
    const artistData = {
      voucher_code: this.state.voucherCode,
      quantity: idx(subscription, (_) => (_[isSelectedMonth]).subscriptionDurationInMonth),
      type: lowerCase(this.state.selectedPackage.package_name),
      paymentStatus
    }

    navigateTo('PromoteUser4', { artistData })
  }

  render() {
    const {
      navigateBack,
      navigateTo,
      startDate,
      isLoading
    } = this.props

    const {
      isSelectedMonth,
      selectedPackage,
      voucherDiscount,
      voucherCodeInput,
      subscription
    } = this.state
    const selectedSubscriptionPrice = idx(subscription, (_) => (_[isSelectedMonth]).price)
    const selectedSubscriptionName = idx(subscription, (_) => (_[isSelectedMonth]).name)
    const selectedSubscriptionDuration = idx(subscription, (_) => (_[isSelectedMonth]).subscriptionDurationInMonth)
    const currentCost = Number(selectedSubscriptionPrice)

    return (
      <Container
        scrollBackgroundColor={SILVER_WHITE}
        scrollable
        isLoading={isLoading}
        renderHeader={() => (
          <Header style={{ justifyContent: 'flex-start' }}>
            <Icon onPress={navigateBack} size='huge' name='chevron-left' type='Entypo' centered/>
            <Text size='large' type='SansPro' weight={500}>Premium Membership</Text>
          </Header>
        )}
        outsideScrollContent={() => (
          <Button
            onPress={this._onSubmit}
            style={{ marginVertical: 0 }}
            centered
            soundfren
            radius={0}
            width='100%'
            text='PROCEED TO PAYMENT'
            textType='SansPro'
            textWeight={500}
            shadow='none'
          />
        )}
      >
        <View style={{
          paddingVertical: HP1, paddingHorizontal: WP6, backgroundColor: WHITE,
          marginVertical: HP1, flexDirection: 'row', justifyContent: 'space-between',
          alignItems: 'center'
        }}
        >
          <View>
            <Text size='mini' weight={500}>Premium Membership Plan</Text>
            <Text color={PURPLE} size='extraMassive' weight={500}>{selectedPackage.package_name}</Text>
          </View>
          <Text
            onPress={() => navigateTo('PackageScreen', { onSelectedPackage: this._onSelectedPackage, selectedPackage })}
            type='SansPro' color={ORANGE_BRIGHT} weight={500}
          >
            CHANGE
          </Text>
        </View>
        <View style={{ paddingVertical: HP1, paddingHorizontal: WP6, backgroundColor: WHITE, marginBottom: HP1 }}>
          <Text size='mini' weight={500}>How long do you want your <Text color={WARM_PURPLE} size='mini' weight={500}>{`${selectedPackage.package_name} Membership?`}</Text></Text>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            onLayout={() => this.scrollSubscription.scrollTo({ x: WP20, y: 0, animated: true })}
            ref={(ref) => this.scrollSubscription = ref} horizontal contentContainerStyle={{ justifyContent: 'center', flexDirection: 'row', marginVertical: HP1 }}
          >
            {
              map(subscription, (sub, key) => (
                <TouchableOpacity
                  key={`sub-${key}`}
                  activeOpacity={TOUCH_OPACITY}
                  onPress={() => {
                    this.setState({ isSelectedMonth: key })
                  }}
                >
                  <LinearGradient
                    colors={isSelectedMonth === key ? [PURPLE, PINK_PURPLE] : [WHITE, WHITE]}
                    start={[0, .3]}
                    end={[1, 1.4]}
                    locations={[.2, 1]}
                    style={{
                      padding: WP10, borderRadius: WP4, marginHorizontal: WP2, borderColor: isSelectedMonth === key ? 'transparent' : BORDER_COLOR,
                      borderWidth: BORDER_WIDTH
                    }}
                  >
                    <Text centered color={isSelectedMonth === key ? WHITE : undefined} size='mini' weight={500}>{sub.name}</Text>
                    {
                      !isEmpty(sub.priceText) && <Text centered color={isSelectedMonth === key ? WHITE : undefined} size='xtiny'>{sub.priceText}</Text>
                    }
                  </LinearGradient>
                </TouchableOpacity>
              ))
            }
          </ScrollView>
        </View>
        <View style={{
          paddingVertical: HP1, paddingHorizontal: WP6, backgroundColor: WHITE, marginBottom: HP1, flexDirection: 'row',
          alignItems: 'center'
        }}
        >
          <InputTextLight
            label='Voucher Code'
            labelWeight={500}
            labelColor={GREY}
            placeholder='Enter voucher code'
            style={{ flex: 1 }}
            value={voucherCodeInput}
            onChangeText={(text) => {
              this.setState({ voucherCodeInput: text })
            }}
          />
          <Button
            onPress={async () => {
              Keyboard.dismiss()
              await this.setState((state) => ({
                voucherCodeInput: null,
                voucherCode: state.voucherCodeInput
              }))
              this._getVoucher()
            }}
            style={{ marginLeft: WP2 }}
            compact='center'
            rounded
            centered
            soundfren
            width={WP20}
            text='APPLY'
            textSize='mini'
            textType='SansPro'
            textWeight={500}
            shadow='none'
          />
        </View>
        <View style={{
          paddingVertical: HP1, paddingHorizontal: WP6, backgroundColor: WHITE, marginBottom: HP1
        }}
        >
          <View style={{ marginBottom: HP1 }}>
            <Text size='mini' weight={500}>Amount to be paid</Text>
            {
              voucherDiscount > 0 && (
                <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: WP1 }}>
                  <Text
                    style={{
                      textDecorationColor: ORANGE_BRIGHT,
                      textDecorationLine: 'line-through',
                      textDecorationStyle: 'solid'
                    }} size='mini'
                  >{selectedPackage.currency_price} {currentCost}</Text>
                  <View style={{
                    backgroundColor: SILVER,
                    paddingHorizontal: WP1,
                    borderRadius: 4,
                    marginLeft: WP2,
                    flexDirection: 'row'
                  }}
                  >
                    <Text size='mini' weight={500} color={WHITE}>Disc. {voucherDiscount}%</Text>
                    <Icon
                      onPress={() => {
                        this.setState({
                          voucherCode: null,
                          voucherDiscount: 0
                        })
                      }} style={{ marginLeft: WP2 }} centered color={WHITE} name='close' size='mini'
                    />
                  </View>
                </View>
              )
            }
            <Text
              size='huge' weight={500}
              color={ORANGE_BRIGHT}
            >{selectedPackage.currency_price} {currencyFormatter(currentCost - ((currentCost * voucherDiscount) / 100))}</Text>
          </View>
          <Text size='tiny'>
            {'You will also get premium membership for '}
            {<Text size='tiny' weight={500}>{selectedSubscriptionName}</Text>}
            {' from '}
            {<Text size='tiny' weight={500}>{startDate}</Text>}
            {' until '}
            {<Text size='tiny' weight={500}>
              {
                moment(startDate, 'DD/MM/YYYY').add(selectedSubscriptionDuration, 'month').format('DD/MM/YYYY')
              }
            </Text>}
          </Text>
          <Modal
            position='bottom'
            style={{ borderTopRightRadius: 0, borderTopLeftRadius: 0 }}
            renderModalContent={(toggleModal, payload) => (
              <View style={{ paddingHorizontal: WP6, paddingVertical: WP4 }}>
                <Text centered type='SansPro' weight={500}>{upperCase(selectedPackage.package_name)} PLAN BENEFITS
                  !</Text>
                <View style={{ marginTop: HP2 }}>
                  {
                    map(selectedPackage.benefit, (bene, i) => (
                      <View key={`bene-${i}`} style={{ flexDirection: 'row', marginVertical: HP1, justifyContent: 'center' }}>
                        <Image size='tiny' source={{ uri: bene.icon }}/>
                        <Text style={{ flex: 1, marginLeft: WP4 }} size='tiny'>{bene.name}</Text>
                      </View>
                    ))
                  }
                </View>
              </View>
            )}
          >
            {({ toggleModal }, M) => (
              <View>
                <Button
                  onPress={toggleModal}
                  centered
                  shadow='none'
                  textSize='mini'
                  textType='SansPro'
                  textWeight={500}
                  textColor={ORANGE_BRIGHT}
                  text={`SEE ${upperCase(selectedPackage.package_name)} PLAN BENEFITS`}
                />
                {M}
              </View>
            )}
          </Modal>
        </View>
      </Container>
    )
  }
}

PromoteUser3.propTypes = {}

PromoteUser3.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(PromoteUser3),
  mapFromNavigationParam
)
