import React from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import { noop, isNil, isEmpty } from 'lodash-es'
import { _enhancedNavigation, Container, Header, Icon, Text, Image, Button } from '../../components'
import { DEFAULT_PAGING } from '../../constants/Routes'
import { authDispatcher } from '../../services/auth'
import { GET_USER_DETAIL } from '../../services/auth/actionTypes'
import { WHITE, DARK_INDIGO, NO_COLOR, DARK_SLATE_BLUE, TOMATO } from '../../constants/Colors'
import { HP5, WP80, WP90, WP4, WP6, HP3 } from '../../constants/Sizes'
import { postLogPremium } from '../../actions/api'
import { paymentDispatcher } from '../../services/payment'
import bugsnagClient from '../../utils/bugsnag'

const mapStateToProps = ({ auth, payment }) => ({
  userData: auth.user,
  paymentSource: payment.paymentSource,
  idAds: payment.idAds
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setUserDetail: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  }),
  paymentSourceSet: paymentDispatcher.paymentSourceSet
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class PromoteUser1 extends React.Component {
  state = {
    isReady: false,
    isRefreshing: false
  }

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  async componentDidMount() {
    await this._getInitialScreenData()
    this._sendLogPremium()
  }

  componentWillUnmount() {
  }

  _sendLogPremium() {
    const {
      userData,
      dispatch,
      paymentSource,
      idAds,
      paymentSourceSet
    } = this.props
    if(!isEmpty(paymentSource) && !isNil(paymentSource)) {
      paymentSourceSet(paymentSource)
      dispatch(
        postLogPremium,
        { id_user: userData.id_user, previous_screen: paymentSource, id_ads: idAds, },
        noop,
        true,
        true
      )
    } else {
      bugsnagClient.notify(new Error('Untracked paymentSource (this warning doesn\'t cause app crash)'))
    }
  }
  _getInitialScreenData = async (...args) => {
    await this._getData(...args)
    this.setState({
      isReady: true,
      isRefreshing: false
    })
  }

  _getData = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {}

  _backHandler = async () => {
    // this.props.onRefresh()
    // this.props.navigateTo('TimelineScreen')
    // this.props.navigateBack()
  }

  render() {
    const {
      navigateTo,
      navigateBack,
      isLoading,
    } = this.props
    const {
      isReady
    } = this.state
    return (
      <Container
        theme='light'
        scrollable
        scrollBackgroundColor='transparent'
        colors={[DARK_SLATE_BLUE, DARK_INDIGO]}
        type='vertical'
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <Header>
            <Icon
              onPress={navigateBack} background='dark-circle' size='large' color={WHITE}
              name='chevron-left' type='Entypo'
            />
            <Text size='mini' type='NeoSans' weight={500} color={WHITE}>Premium Membership</Text>
            <Icon size='large' color={NO_COLOR} name='left' />
          </Header>
        )}
      >
        <View style={{ flexGrow: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image
            imageStyle={{ width: WP90 }} source={require('../../assets/images/premiumBadges.png')}
            aspectRatio={260 / 240}
          />
          <Text weight={300} style={{ marginTop: HP3, marginHorizontal: WP6 }} centered size='mini' color={WHITE}>For you, who want more. Explore a lot more big opportunies in music industry with our Premium Features !</Text>
          <Button
            onPress={() => {
              navigateTo('PromoteUser2')
            }}
            style={{ marginTop: HP5 }}
            contentStyle={{ paddingVertical: WP4 }}
            backgroundColor={TOMATO}
            centered
            marginless
            width={WP80}
            radius={8}
            text='See Premium Member Benefits'
            textType='NeoSans'
            textSize='mini'
            textWeight={500}
            textColor={WHITE}
            shadow='none'
          />
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(PromoteUser1),
  mapFromNavigationParam
)
