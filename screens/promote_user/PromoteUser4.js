import React, { Component } from 'react'
import { WebView } from 'react-native-webview'
import { connect } from 'react-redux'
import { toLower, get, isNil, isEmpty } from 'lodash-es'
import { NavigationActions } from '@react-navigation/compat'
import Container from '../../components/Container'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import env from '../../utils/env'
import { Header, Icon, Text } from '../../components'
import { TOMATO } from '../../constants/Colors'
import { WP20 } from '../../constants/Sizes'
import { resetAction } from '../../utils/backhandler'
import { paymentDispatcher } from '../../services/payment'
import bugsnagClient from '../../utils/bugsnag'

const mapStateToProps = ({ auth, payment }) => ({
  userData: auth.user,
  paymentSource: payment.paymentSource
})

const mapDispatchToProps = {
  paymentSourceClear: paymentDispatcher.paymentSourceClear
}

const mapFromNavigationParam = (getParam) => ({
  artistData: getParam('artistData', {}),
})

class PromoteUser4 extends Component {
  state = {
    isLoading: true
  }

  maestroAction = () => {
    let { navigation } = this.props
    resetAction(navigation, 1, [NavigationActions.navigate({ routeName: 'VoucherListScreen' })], 'ProfileScreen')
  }

  componentDidMount() {
    const { paymentSource } = this.props
    if(isEmpty(paymentSource) || isNil(paymentSource)) {
      bugsnagClient.notify(new Error('Untracked paymentSource (this warning doesn\'t cause app crash)'))
    }
  }

  maestroProps = () => {
    const { artistData } = this.props
    const packageName = get(artistData, 'type', '')
    const isMaestro = toLower(packageName) === 'maestro'
    const maestroProps = {
      title: 'Welcome to the Club!',
      message: 'Welcome to the club, Maestro!\n' +
        'Join a Soundfren Connect session with your Voucher.',
      image: require('../../assets/images/maestroShieldSuccessPayment02.png'),
      imageAspectRatio: 234 / 194,
      buttonText: 'Check My Voucher',
      buttonAction: this.maestroAction
    }
    return isMaestro ? maestroProps : {}
  }

  render() {
    const {
      userData,
      artistData,
      navigateTo,
      navigateBack,
      isLoading: isLoadingProps,
      paymentSource
    } = this.props
    const {
      isLoading
    } = this.state
    const paymentUrl = `${env.webUrl}/payment/vtweb/vtweb_checkout`
    const queries = `?id_user=${userData.id_user}` +
      `&quantity=${artistData.quantity}` +
      `&voucher_code=${artistData.voucher_code || ''}` +
      `&full_name=${userData.full_name}` +
      `&email=${userData.email}` +
      `&phone=${userData.phone}` +
      `&type=${artistData.type}` +
      '&payment_type=premium_user' +
      `&payment_status=${artistData.paymentStatus}` +
      `&source_from=${paymentSource}`
    return (
      <Container
        isLoading={isLoading || isLoadingProps}
        renderHeader={() => (
          <Header style={{ justifyContent: 'flex-start' }}>
            <Icon onPress={navigateBack} size='huge' name='chevron-left' type='Entypo' centered/>
            <Text size='large' type='SansPro' weight={500}>Premium Membership</Text>
          </Header>
        )}
      >
        <WebView
          source={{ uri: paymentUrl + queries }}
          onLoadStart={() => {
            this.setState({ isLoading: true })
          }}
          onLoadEnd={() => {
            this.setState({ isLoading: false })
          }}
          onNavigationStateChange={async (state) => {
            if(state.url.includes('payment/finish')) {
              this.props.paymentSourceClear()
              this.props.navigation.popToTop()
              navigateTo('FinishScreen', {
                title: 'Welcome to the Club!',
                message: 'We already receive your payment. Take your seat and coffee, Let our team preparing the best for you in 1 x 24 hours.',
                centered: true,
                image: require('../../assets/images/premiumComplete.png'),
                imageAspectRatio: 200 / 197,
                paddingHorizontal: WP20,
                buttonText: 'Go to Profile',
                buttonColor: TOMATO,
                buttonBottom: false,
                buttonAction: navigateBack,
                ...this.maestroProps()
              })
            }
          }}
        />
      </Container>
    )
  }
}

PromoteUser4.propTypes = {}
PromoteUser4.defaultProps = {}
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(PromoteUser4),
  mapFromNavigationParam
)
