/* eslint-disable react/sort-comp */
import React from 'react'
import {
  BackHandler,
  Easing,
  FlatList,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  View,
} from 'react-native'
import { connect } from 'react-redux'
import { getMentionSuggestions } from 'sf-actions/api'
import { isEmpty, omit } from 'lodash-es'
import MenuOptions from 'sf-components/MenuOptions'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'
import { _enhancedNavigation, Avatar, Container, HeaderNormal } from 'sf-components'
import {
  GUN_METAL,
  PALE_BLUE,
  PALE_GREY,
  PINK_RED,
  REDDISH,
  REDDISH_DISABLED,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
  PALE_SALMON
} from 'sf-constants/Colors'
import {
  WP05,
  WP1,
  WP100,
  WP105,
  WP15,
  WP2,
  WP20,
  WP3,
  WP4,
  WP5,
  WP7,
  WP8,
} from 'sf-constants/Sizes'
import { HEADER } from 'sf-constants/Styles'
import Icon from 'sf-components/Icon'
import Image from 'sf-components/Image'
import Text from 'sf-components/Text'
import Touchable from 'sf-components/Touchable'
import ButtonV2 from 'sf-components/ButtonV2'
import CreatePostImages from 'sf-components/CreatePostImages'
import { getWordAt, isEqualObject, isIOS } from 'sf-utils/helper'
import { LinearGradient } from 'expo-linear-gradient'
import { selectPhoto, takePhoto } from 'sf-utils/upload'
// import { BASE64, PARAGRAPHS } from 'sf-constants/Dummies'
import Collapsible from 'react-native-collapsible'
import AutogrowInput from 'react-native-autogrow-input'
import produce from 'immer'
import { TRANSPARENT } from '../constants/Colors'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  refreshFeeds: getParam('refreshFeeds', () => {}),
  topicIds: getParam('topicIds', []),
  is1000Startup: getParam('is1000Startup', false),
})

const style = StyleSheet.create({
  actionBarLeft: {
    width: WP15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 0,
    paddingVertical: WP3,
    marginRight: WP5,
  },
  actionBarRight: {
    width: WP20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 0,
    paddingVertical: WP3,
  },
  writter: {
    flexDirection: 'row',
    paddingHorizontal: WP5,
    paddingTop: WP5,
  },
  writterRight: {
    flex: 1,
    paddingLeft: WP4,
    paddingTop: WP1,
  },
  buttonCommunity: {
    marginTop: WP2,
    flexDirection: 'row',
  },
  buttonCommunityAction: {
    paddingVertical: WP105,
    paddingHorizontal: WP3,
  },
  buttonChevron: {
    marginLeft: WP1,
  },
  bottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: PALE_BLUE,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
    paddingHorizontal: WP1,
    backgroundColor: WHITE,
  },
  bottomViewOther: {
    paddingVertical: WP5,
    paddingHorizontal: WP4,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomIcon: {
    paddingVertical: WP5,
    paddingHorizontal: WP4,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomDivider: {
    width: 1,
    height: WP7 + WP1,
    backgroundColor: PALE_BLUE,
  },
  others: {
    paddingHorizontal: WP5,
    paddingTop: WP7 - WP1,
    paddingBottom: WP7 + WP1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: WHITE,
  },
  viewOtherDescription: {
    flex: 1,
    paddingLeft: WP5,
  },
  viewOtherTitle: {
    marginBottom: WP1,
  },
  post: {
    paddingHorizontal: WP5,
    paddingBottom: WP5,
  },
  postInput: {
    fontFamily: 'CircularBook',
    textAlignVertical: 'top',
    marginVertical: WP3,
    lineHeight: WP5,
    color: GUN_METAL,
  },
  roleOption: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: WP5,
  },
  roleOptionDescription: {
    flex: 1,
    paddingLeft: WP5,
  },
  modalBack: {
    width: WP100 - WP8,
  },
  modalBackSubtitle: {
    lineHeight: WP5,
  },
  modalBackSecondaryButton: {
    backgroundColor: PALE_GREY,
    marginTop: WP1,
    borderRadius: 8,
    paddingVertical: WP3,
  },
})

class FeedPostScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      viewOthers: false,
      description: '',
      role: 'Community',
      images: {
        uri: [],
        b64: [],
        name: [],
        size: [],
      },
      enableNext: false,
      backModal: false,
    }
    this._nextStep = this._nextStep.bind(this)
    this._rightComponent = this._rightComponent.bind(this)
    this._toggleViewOther = this._toggleViewOther.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
    this._onTyping = this._onTyping.bind(this)
    this._onImageTaken = this._onImageTaken.bind(this)
    this._selectPhoto = this._selectPhoto.bind(this)
    this._takePhoto = this._takePhoto.bind(this)
    this._createCollab = this._createCollab.bind(this)
    this._onDeleteImage = this._onDeleteImage.bind(this)
    this._onSelectionChange = this._onSelectionChange.bind(this)
    this._getMentionSuggestion = this._getMentionSuggestion.bind(this)
    this._backHandler = this._backHandler.bind(this)
    this._toggleBackModal = this._toggleBackModal.bind(this)
  }

  componentDidMount() {
    if (!this.props.userData.id_user) {
      this.props.navigateBack()
      this.props.navigateTo('AuthNavigator')
    } else {
      this._didFocusSubscription = this.props.navigation.addListener('focus', () =>
        BackHandler.addEventListener('hardwareBackPress', this._backHandler),
      )
      this._willBlurSubscription = this.props.navigation.addListener(
        'blur',
        (payload) =>
          BackHandler.removeEventListener('hardwareBackPress', this._backHandler),
      )
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const shouldComponentUpdate = !isEqualObject(
      omit(nextState, []),
      omit(this.state, []),
    )
    return shouldComponentUpdate
  }

  static getDerivedStateFromProps(props, state) {
    const enableNext =
      !isEmpty(state.description) && state.description.length <= 500
    if (state.enableNext != enableNext) {
      return { enableNext }
    }
    return null
  }

  componentWillUnmount() {
    try {
      this._didFocusSubscription()
      this._willBlurSubscription()
    } catch (error) {
      // just keep silent dude
    }
  }

  _onDeleteImage = (index) => {
    this.setState(
      produce((draft) => {
        draft.images.uri.splice(index, 1)
        draft.images.b64.splice(index, 1)
      }),
    )
  };

  _toggleViewOther = () => {
    this.setState(
      produce((draft) => {
        draft.viewOthers = !draft.viewOthers
      }),
    )
  };

  _backHandler = () => {
    this._toggleBackModal()
    return true
  };

  _toggleBackModal = () => {
    this.setState(
      produce((draft) => {
        draft.backModal = !draft.backModal
      }),
    )
  };

  _onSelectionChange = ({
    nativeEvent: {
      selection: { end: position },
    },
  }) => {
    const { description } = this.state
    const word = getWordAt(description, position)
    word.length > 1 &&
      word.substring(0, 1) == '@' &&
      getMentionSuggestions({
        start: 0,
        limit: 5,
        full_name: word.substring(1),
      }).then(({ data }) => {
        //do something
      })
  };

  _nextStep = () => {
    const { navigateTo, refreshFeeds, is1000Startup } = this.props
    const { description, images, role } = this.state
    navigateTo('FeedPostTopic', {
      images,
      description,
      refreshFeeds,
      role,
      is1000Startup,
    })
  };

  _rightComponent = () => {
    const { enableNext } = this.state
    return (
      <ButtonV2
        onPress={this._nextStep}
        disabled={!enableNext}
        style={style.actionBarRight}
        text={'Next'}
        borderColor={WHITE}
        textColor={enableNext ? REDDISH : PALE_SALMON}
      />
    )
  };

  _renderHeader = () => {
    const { enableNext } = this.state
    return (
      <View key={enableNext ? 'header-enabled' : 'header-disabled'}>
        <HeaderNormal
          noRightPadding
          iconStyle={style.actionBarLeft}
          iconLeftOnPress={this._backHandler}
          style={style.headerNormal}
          centered
          rightComponent={this._rightComponent}
          textType={'Circular'}
          textSize={'slight'}
          text={'Buat Post'}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
      </View>
    )
  };

  _buttonChevron = () => (
    <View>
      <Icon
        style={style.buttonChevron}
        color={SHIP_GREY_CALM}
        size={'mini'}
        type={'Entypo'}
        name={'chevron-down'}
      />
    </View>
  );

  _getMentionSuggestion = () => {};

  _onTyping = (description, b, c, d) => {
    this.setState(
      produce((draft) => {
        draft.description = description
      }),
    )
  };

  _onImageTaken = (key) => (value) => {
    this.setState(
      produce((draft) => {
        draft.images[key.toLowerCase()].unshift(value)
      }),
    )
  };

  _selectPhoto = () => {
    selectPhoto(this._onImageTaken, '', null, {}, !isIOS() ? [740, null] : null)()
  };

  _takePhoto = () => {
    takePhoto(this._onImageTaken, '', null, {}, !isIOS() ? [740, null] : null)()
  };

  _createCollab = () => {
    this._toggleViewOther()
    this.props.navigateTo('CollaborationForm')
  };

  _onLayoutEditor = ({ nativeEvent: { layout } }) => {};

  _menuRole = [
    {
      onPress: () => {
        try {
          this._toggleRoleModal()
        } catch (error) {
          // Keep silent
        } finally {
          this.setState(
            produce((draft) => {
              draft.role = 'Community'
            }),
          )
        }
      },
      image: require('sf-assets/icons/v3/shareToCommunity.png'),
      title: 'Community',
      subtitle: 'Dilihat & dibagikan oleh publik',
    },
    {
      onPress: () => {
        try {
          this._toggleRoleModal()
        } catch (error) {
          // Keep silent
        } finally {
          this.setState(
            produce((draft) => {
              draft.role = 'Circle'
            }),
          )
        }
      },
      image: require('sf-assets/icons/v3/shareToCircle.png'),
      title: 'Circle',
      subtitle: 'Dilihat & dibagikan hanya oleh teman',
    },
  ];

  _renderMenuRole = ({ item, index }) => {
    return (
      <Touchable onPress={item.onPress} key={item.title}>
        <View style={style.roleOption}>
          <View>
            <Image source={item.image} />
          </View>
          <View style={style.roleOptionDescription}>
            <Text lineHeight={WP5 + WP2} color={GUN_METAL} type={'Circular'}>
              {item.title}
            </Text>
            <Text color={SHIP_GREY_CALM} size={'tiny'} type={'Circular'}>
              {item.subtitle}
            </Text>
          </View>
        </View>
      </Touchable>
    )
  };

  _roleOptions = (
    <View>
      <FlatList renderItem={this._renderMenuRole} data={this._menuRole} />
    </View>
  );

  _roleTrigger = (toggleModal) => {
    this._toggleRoleModal = toggleModal
    return (
      <View style={style.buttonCommunity}>
        <ButtonV2
          onPress={toggleModal}
          rightComponent={this._buttonChevron}
          style={style.buttonCommunityAction}
          text={this.state.role}
          textColor={SHIP_GREY}
        />
      </View>
    )
  };

  render() {
    const {
      userData: { profile_picture, full_name: name }, is1000Startup
    } = this.props
    const {
      isLoading,
      description,
      backModal,
      images: { uri },
    } = this.state
    const allowUploadImage = uri.length < 3

    return (
      <Container
        renderHeader={this._renderHeader}
        scrollable={false}
        isLoading={isLoading}
      >
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{ flex: 1 }}
        >
          <View style={{ flex: 1 }}>
            <View style={style.writter}>
              <View>
                <Avatar size={'small'} image={profile_picture} />
              </View>
              <View style={style.writterRight}>
                <Text
                  size={'slight'}
                  weight={500}
                  color={GUN_METAL}
                  type={'Circular'}
                >
                  {name}
                </Text>
                <MenuOptions
                  options={this._roleOptions}
                  triggerComponent={this._roleTrigger}
                />
              </View>
            </View>
            <View style={style.post}>
              <View>
                <AutogrowInput
                  value={this.state.description}
                  autoFocus={!__DEV__}
                  onChangeText={this._onTyping}
                  spellCheck={false}
                  autoCorrect={false}
                  style={style.postInput}
                  placeholderTextColor={SHIP_GREY_CALM}
                  placeholder={'Ada apa hari ini?'}
                  defaultHeight={60}
                />
              </View>
              <CreatePostImages
                onDelete={this._onDeleteImage}
                images={this.state.images.uri}
              />
            </View>
          </View>
          <View style={style.bottomView}>
            <Touchable disabled={!!is1000Startup} onPress={this._toggleViewOther} style={style.bottomViewOther}>
              <Text color={is1000Startup? TRANSPARENT : SHIP_GREY} weight={400} type={'Circular'} size={'slight'}>
                {'Lainnya '}
              </Text>
              <View>
                <Icon
                  style={style.buttonChevron}
                  color={is1000Startup? TRANSPARENT : SHIP_GREY_CALM}
                  size={'small'}
                  type={'Entypo'}
                  name={!this.state.viewOthers ? 'chevron-up' : 'chevron-down'}
                />
              </View>
            </Touchable>
            <View style={style.bottomIcon}>
              <Text
                color={description.length > 500 ? PINK_RED : GUN_METAL}
                weight={400}
                type={'Circular'}
                size={'mini'}
              >{`${description.length}/500`}</Text>
            </View>
            <View style={style.bottomDivider} />
            <Touchable
              disabled={!allowUploadImage}
              onPress={this._selectPhoto}
              style={style.bottomIcon}
            >
              <View>
                <Icon
                  color={allowUploadImage ? SHIP_GREY_CALM : PALE_BLUE}
                  size={'huge'}
                  type={'MaterialCommunityIcons'}
                  name={'image'}
                />
              </View>
            </Touchable>
            <View style={style.bottomDivider} />
            <Touchable
              disabled={!allowUploadImage}
              onPress={this._takePhoto}
              style={style.bottomIcon}
            >
              <View>
                <Icon
                  color={allowUploadImage ? SHIP_GREY_CALM : PALE_BLUE}
                  size={'huge'}
                  type={'MaterialCommunityIcons'}
                  name={'camera'}
                />
              </View>
            </Touchable>
          </View>

          <Collapsible easing={Easing.ease} collapsed={!this.state.viewOthers}>
            <Touchable onPress={this._createCollab} style={style.others}>
              <View>
                <Image
                  size={'medium'}
                  source={require('sf-assets/icons/v3/plusCircularGrey.png')}
                />
              </View>
              <View style={style.viewOtherDescription}>
                <Text
                  style={style.viewOtherTitle}
                  color={GUN_METAL}
                  weight={400}
                  type={'Circular'}
                  size={'slight'}
                >
                  Add Collaboration Project
                </Text>
                <Text
                  lineHeight={WP4 + WP05}
                  color={SHIP_GREY_CALM}
                  weight={300}
                  type={'Circular'}
                  size={'xmini'}
                >
                  Buat Project untuk kolaborasikan talentamu dan ciptakan karya baru
                  sekarang
                </Text>
              </View>
            </Touchable>
          </Collapsible>
          <ModalMessageView
            style={style.modalBack}
            subtitleStyle={style.modalBackSubtitle}
            buttonSecondaryStyle={style.modalBackSecondaryButton}
            toggleModal={this._toggleBackModal}
            isVisible={backModal}
            title={'Buat Post'}
            titleType='Circular'
            titleSize={'small'}
            titleColor={GUN_METAL}
            subtitle={
              'Post yang sedang kamu buat belum selesai. Lanjutkan membuat post?'
            }
            subtitleType='Circular'
            subtitleSize={'xmini'}
            subtitleWeight={400}
            subtitleColor={SHIP_GREY_CALM}
            image={null}
            buttonPrimaryText={'Lanjutkan'}
            buttonPrimaryTextType='Circular'
            buttonPrimaryTextWeight={400}
            buttonPrimaryBgColor={REDDISH}
            buttonSecondaryText={'Batalkan'}
            buttonSecondaryTextType='Circular'
            buttonSecondaryTextWeight={400}
            buttonSecondaryTextColor={SHIP_GREY_CALM}
            buttonSecondaryAction={this.props.navigateBack}
          />
        </KeyboardAvoidingView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(FeedPostScreen),
  mapFromNavigationParam,
)
