/* eslint-disable react/sort-comp */
import React from 'react'
import { isNil, isEqual, pick, noop } from 'lodash-es'
import {
  View,
  TouchableOpacity,
  BackHandler,
  Dimensions,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Image as RNImage,
} from 'react-native'
import { minimizeApp } from "sf-utils/backhandler";
import { connect } from 'react-redux'
import Constants from 'expo-constants'
import * as AppleAuthentication from 'expo-apple-authentication'
import { Text, _enhancedNavigation, Icon, Image } from 'sf-components'
import {
  RED_GOOGLE,
  WHITE,
  BLUE_FACEBOOK,
  GUN_METAL,
  BLACK,
  TRANSPARENT,
  PALE_LIGHT_BLUE_TWO,
  REDDISH,
  GREEN_MMB,
  NAVY_DARK,
  PALE_BLUE,
  PALE_GREY_TWO,
  SHIP_GREY,
  SHIP_GREY_CALM,
} from "sf-constants/Colors";
import { WP4, WP5, WP3 } from 'sf-constants/Sizes'
import { setUserId } from 'sf-utils/storage'
import { convertBlobToBase64, fetchAsBlob, isIOS } from 'sf-utils/helper'
// import { FacebookAuth, GoogleAuth, AppleAuth } from 'sf-actions/externalAuth'
import {
  getProfileDetail,
  getCity,
  postRegisterV3Facebook,
  postRegisterV3Google,
  postRegisterV3Apple,
} from 'sf-actions/api'
import { GET_USER_DETAIL } from 'sf-services/auth/actionTypes'
import { SET_REGISTRATION } from 'sf-services/register/actionTypes'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import { GET_NOTIFICATION } from 'sf-services/notification/actionTypes'
import Spacer from 'sf-components/Spacer'
import { setLoading } from 'sf-services/app/actionDispatcher'
import { HP1, HP10, HP100, HP2, HP3, HP5, HP50, WP10, WP100, WP12, WP2, WP25, WP305, WP6, WP7, WP8, WP90 } from '../constants/Sizes'
import { ModalMessageView } from '../components'
import ButtonV2 from '../components/ButtonV2'
import { BASE_50, NEUTRAL_950, NEUTRAL_OLD_10, PRIMARY_90 } from '../constants/Colors';
import { BlurView } from "expo-blur";

const borderRadius = 9

const mapStateToProps = ({ register }) => ({
  register,
})

const mapDispatchToProps = {
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail,
  }),
  setNewNotification: (notification) => ({
    type: `${GET_NOTIFICATION}_PUSHER`,
    response: notification,
  }),
  setRegistration: (data) => ({
    type: SET_REGISTRATION,
    response: data,
  }),
  setLoading,
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', true),
  forceReload: getParam('forceReload', false),
})

const style = StyleSheet.create({
  sfLogo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: WP8,
  }
})

class LoginScreen extends React.Component {
  _willBlurSubscription;

  constructor(props) {
    super(props)
    this.handleSocialSignin = this.handleSocialSignin.bind(this)
    this.onFacebookButtonLayout = this.onFacebookButtonLayout.bind(this)
    // alert(this.props.forceReload)
  }

  onFacebookButtonLayout = ({ nativeEvent: { layout } }) => {
    this.setState({ buttonWidth: layout.width, buttonHeight: layout.height })
  };

  handleSocialSignin = (dataResponse) => {
    const {
      dispatch,
      register: { data, behaviour },
      setRegistration,
      navigateTo,
      setUserDetailDispatcher,
    } = this.props
    dispatch(getProfileDetail, { id_user: dataResponse.id_user }).then(
      async (dataResponse) => {
        let city_name = null
        let {
          registration_step,
          id_user,
          email,
          id_city,
          full_name,
          profile_type,
          profile_picture,
          registration_version,
          registered_via,
          gender,
        } = dataResponse
        const isV3 = registration_version == '3.0'
        if (registration_step !== 'finish') {
          try {
            profile_picture = await this.getBase64(profile_picture)
          } catch (err) {
            // better keep silent
          } finally {
            const step = parseInt(registration_step)
            if (!isNil(id_city)) {
              const cities = await dispatch(getCity, { offset: 0 })
              const city = cities.filter(
                (city) => parseInt(city.id_city) == parseInt(id_city),
              )[0]
              city_name = city.city_name
            }
            Promise.resolve(
              setRegistration({
                behaviour: {
                  ...behaviour,
                  step: !isV3 ? 3 : step < 3 ? 3 : step + 1,
                },
                data: {
                  ...data,
                  id_user,
                  id_city,
                  city_name,
                  email,
                  profile_type,
                  profile_picture,
                  name: full_name,
                  registered_via,
                  gender,
                },
              }),
            ).then(() => {
              this.setState(
                {
                  disable: false,
                  isLoading: false,
                },
                () => {
                  navigateTo('RegisterV3')
                },
              )
            })
          }
        } else {
          await setUserId(id_user.toString())
          setUserDetailDispatcher(dataResponse)
          navigateTo(
            this.props.forceReload ? 'LoadingScreen' : 'LoadingModalScreen',
            {},
            'replace',
          )
        }
      },
    )
  };

  state = {
    disable: false,
    emailLogin: false,
    buttonWidth: 210,
    buttonHeight: 23,
    appleLoginAvailable: false,
    isModalConfirmMMBVisible: false,
    typeLogin: null,

  };

  attachBackAction() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this._backHandler,
    )
  }

  releaseBackAction() {
    try {
      this.backHandler.remove()
    } catch (err) {
      /*silent is gold*/
    }
  }

  async getBase64(url) {
    let result = await fetchAsBlob(url)
    let base64 = await convertBlobToBase64(result)
    return base64
  }

  componentDidMount() {
    const { register, navigateTo } = this.props
    this.focusListener = this.props.navigation.addListener(
      'focus',
      this.attachBackAction.bind(this),
    )
    this.blurListener = this.props.navigation.addListener(
      'blur',
      this.releaseBackAction.bind(this),
    )
    register.data.id_user && navigateTo('RegisterV3')
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(
      pick(nextState, ['disable', 'isLoading', 'buttonWidth', 'buttonHeight', 'isModalConfirmMMBVisible', 'typeLogin']),
      pick(this.state, ['disable', 'isLoading', 'buttonWidth', 'buttonHeight', 'isModalConfirmMMBVisible', 'typeLogin']),
    )
  }

  componentWillUnmount() {
    try {
      this.releaseBackAction()
      typeof this.focusListener !== 'undefined' && this.focusListener()
      typeof this.blurListener !== 'undefined' && this.blurListener()
    } catch (err) {
      /*silent is gold*/
    }
  }

  _backHandler = async () => {
    const { emailLogin } = this.state
    if (emailLogin) {
      this.setState({ emailLogin: false })
    } else {
      minimizeApp()
      // this.props.navigateBack()
    }
  };

  _onPressFacebookAuth = async () => {
    const { dispatch } = this.props
    await this.setState({ disable: true })
    const facebookUserDetail = await FacebookAuth()
    if (facebookUserDetail) {
      dispatch(postRegisterV3Facebook, {
        id: facebookUserDetail.id,
        email: facebookUserDetail.email,
        name: (facebookUserDetail.name || '').trim(),
        picture: facebookUserDetail.picture.data.url,
      }).then(this.handleSocialSignin)
    } else {
      await this.setState({ disable: false })
    }
  };

  _onPressGoogleAuth = async () => {
    const { dispatch } = this.props
    await this.setState({ disable: true })
    const googleUserDetail = await GoogleAuth()
    if (googleUserDetail) {
      dispatch(postRegisterV3Google, {
        id: googleUserDetail.id,
        email: googleUserDetail.email,
        name: `${googleUserDetail.givenName} ${googleUserDetail.familyName}`.trim(),
        picture: googleUserDetail.photoUrl,
      }).then(this.handleSocialSignin)
    } else {
      await this.setState({ disable: false })
    }
  };

  _onPressAppleAuth = async () => {
    const { dispatch } = this.props
    await this.setState({ disable: true })

    AppleAuthentication.isAvailableAsync().then(async () => {
      const appleUserDetail = await AppleAuth()
      if (appleUserDetail) {
        dispatch(postRegisterV3Apple, {
          id: appleUserDetail.user,
          email: appleUserDetail.email,
          name:
            appleUserDetail.fullName && appleUserDetail.fullName.givenName
              ? `${appleUserDetail.fullName.givenName} ${appleUserDetail.fullName.familyName}`.trim()
              : '',
        }).then(this.handleSocialSignin)
      } else {
        await this.setState({ disable: false })
      }
    })
  };
  _toggleModalConfirmMMB = () => {
    this.setState({ isModalConfirmMMBVisible: !this.state.isModalConfirmMMBVisible })
  }

  _handleModalMMBSecondary = async () => {
    const { typeLogin } = this.state
    if (typeLogin === 'google')
      this._onPressGoogleAuth()
    else if (typeLogin === 'facebook')
      this._onPressFacebookAuth()
    this._toggleModalConfirmMMB()
  }

  _renderButton = ({ text, backgroundColor, onPress }) => {
    const { width } = Dimensions.get('window')
    const btnWidth = width >= 360 ? 274 : width * 0.775
    return (
      <View>
        <TouchableOpacity
          style={{
            width: btnWidth,
            marginBottom: WP2,
            paddingVertical: WP3,
            alignItems: 'center',
            flexDirection: 'row',
            backgroundColor,
            paddingHorizontal: WP5,
            justifyContent: 'center',
            borderRadius,
          }}
          onPress={onPress}
        >
          {/* <Icon color={WHITE} size='large' name='facebook' type='Entypo' /> */}
          <Text
            type={'Circular'}
            size='xmini'
            centered
            weight={500}
            numberOfLines={1}
            color={WHITE}
            style={{ marginLeft: WP4 }}
          >
            {text}
          </Text>
          <Icon color={TRANSPARENT} size='large' name='facebook' type='Entypo' />
        </TouchableOpacity>
      </View>
    )
  }

  _renderButtonSocial = ({ icon, onPress }) => {
    return (
      <ButtonV2
        onPress={onPress}
        borderColor={TRANSPARENT}
        color={SHIP_GREY_CALM}
        style={{ width: WP12, height: WP12 }}
        icon={icon}
        iconSize={'large'}
        iconType={'FontAwesome'}
        iconColor={WHITE}
      />
    )
  }

  render() {
    const { navigateTo } = this.props
    const { width, height } = Dimensions.get('window')
    const { isModalConfirmMMBVisible, emailLogin } = this.state

    const btnWidth = width >= 360 ? 274 : width * 0.775

    return (
      // <View
      //   style={{
      //     flex: 1,
      //     justifyContent: 'center',
      //     alignItems: 'center',
      //   }}
      // >
      //   <View
      //     style={{
      //       width: width >= 360 ? 332 : width - 25,
      //       borderRadius: WP4,
      //       backgroundColor: WHITE,
      //       alignItems: 'center',
      //       paddingHorizontal: WP6,
      //       paddingTop: WP5,
      //       paddingBottom: WP3,
      //       elevation: 100,
      //     }}
      //   >
      //     <Spacer size={WP6} />
      //     <RNImage
      //       resizeMode={'cover'}
      //       source={require('sf-assets/images/soundfrenLogo.png')}
      //       style={{ width: 140, height: 35, aspectRatio: 240 / 38 }}
      //     />
      //     <Spacer size={WP5 * 1.5} />
      //     {this._renderButton({
      //       text: 'Log In with Email',
      //       backgroundColor: REDDISH,
      //       onPress: () => navigateTo('LoginEmailScreen',)
      //     })}
      //     {/* {this._renderButton({
      //       text: 'Log In Muda Maju Bersama',
      //       backgroundColor: GREEN_MMB,
      //       onPress: () => navigateTo('LoginEmailScreen', { isLoginMMB: true })
      //     })} */}
      //     <View
      //       style={{
      //         flexDirection: 'row',
      //         justifyContent: 'center',
      //         alignItems: 'center',
      //         width: btnWidth,
      //         marginVertical: WP3,
      //       }}
      //     >
      //       <View
      //         style={{
      //           borderBottomWidth: 1.5,
      //           borderBottomColor: PALE_LIGHT_BLUE_TWO,
      //           flex: 1,
      //         }}
      //       />
      //       <Text
      //         weight={500}
      //         style={{ marginHorizontal: WP4 }}
      //         size='xmini'
      //         color={PALE_LIGHT_BLUE_TWO}
      //       >
      //         OR
      //       </Text>
      //       <View
      //         style={{
      //           borderBottomWidth: 1.5,
      //           borderBottomColor: PALE_LIGHT_BLUE_TWO,
      //           flex: 1,
      //         }}
      //       />
      //     </View>
      //     <View style={{ width: btnWidth, flexDirection: 'row', justifyContent: 'space-evenly' }}>

      //       {this._renderButtonSocial({
      //         icon: 'facebook',
      //         onPress: this._onPressFacebookAuth
      //       })}
      //       {this._renderButtonSocial({
      //         icon: 'google',
      //         onPress: this._onPressGoogleAuth
      //       })}
      //       {(isIOS() || __DEV__) && this._renderButtonSocial({
      //         icon: 'apple',
      //         onPress: this._onPressAppleAuth
      //       })}
      //     </View>
      //     <TouchableOpacity
      //       activeOpacity={TOUCH_OPACITY}
      //       // onPress={() => navigateTo('RegisterV3')}
      //       onPress={() => {
      //         this.props.navigation.navigate('AuthNavigator', {
      //           screen: 'RegisterV3',
      //           // params: {
      //           //   forceReload: true,
      //           // },
      //         })
      //       }}
      //       style={{
      //         alignSelf: 'center',
      //         paddingVertical: WP3,
      //         width,
      //         borderTopColor: SHIP_GREY,
      //       }}
      //     >
      //       <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
      //         <Text
      //           style={{ fontStyle: 'italic' }}
      //           type={'Circular'}
      //           size='xmini'
      //           color={SHIP_GREY}
      //         >
      //           {'Belum punya akun? '}
      //         </Text>
      //         <Text type={'Circular'} size='xmini' weight={600} color={NAVY_DARK}>
      //           Daftar disini
      //         </Text>
      //       </View>
      //     </TouchableOpacity>
      //     {/* <TouchableOpacity
      //       activeOpacity={TOUCH_OPACITY}
      //       onPress={() => this.props.navigateBack()}
      //       style={{
      //         alignSelf: 'center',
      //         paddingVertical: WP3,
      //         width,
      //         borderTopColor: GUN_METAL,
      //       }}
      //     >
      //       <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
      //         <Text type={'Circular'} size='xmini' color={SHIP_GREY}>
      //           {'Skip untuk sekarang'}
      //         </Text>
      //       </View>
      //     </TouchableOpacity> */}
      //   </View>
      //   <ModalMessageView
      //     toggleModal={noop}
      //     isVisible={isModalConfirmMMBVisible}
      //     buttonPrimaryAction={() => {
      //       this._toggleModalConfirmMMB()
      //       navigateTo('LoginEmailScreen')
      //     }}
      //     titleType='Circular'
      //     titleSize={'small'}
      //     titleColor={GUN_METAL}
      //     subtitle={'Apakah anda mengikuti program'}
      //     extraSubtitle={'Muda Maju Bersama 1000 Startup?'}
      //     extraSubtitleWeight={700}
      //     subtitleType='Circular'
      //     subtitleSize={'slight'}
      //     subtitleWeight={300}
      //     subtitleLineHeight={WP5}
      //     subtitleColor={GUN_METAL}
      //     imageStyle={{ width: WP25, height: WP25 }}
      //     image={require('sf-assets/images/questionCircle.png')}
      //     buttonPrimaryText={'Ya'}
      //     buttonPrimaryTextType='Circular'
      //     buttonPrimaryTextWeight={400}
      //     buttonPrimaryBgColor={REDDISH}
      //     buttonSecondaryText={'Tidak'}
      //     buttonSecondaryTextType='Circular'
      //     buttonSecondaryTextWeight={400}
      //     buttonSecondaryTextColor={SHIP_GREY_CALM}
      //     buttonSecondaryAction={() => this._handleModalMMBSecondary()}
      //   />
      // </View>
      <View style={{ flex: 1, width, height, backgroundColor: BLACK }}>
        <ImageBackground
          style={{
            flex: 1,
            width,
            height,
            resizeMode: "cover",
            justifyContent: "space-between",
          }}
          source={require("sf-assets/images/indigo/WelcomeBg.png")}
        >
          <SafeAreaView style={{ flex: 1 }}>
            <StatusBar backgroundColor={"#000"} barStyle={"light-content"} />
            <View
              style={{
                width,
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  paddingTop: Constants.statusBarHeight,
                  width: WP100 * 0.9,
                  justifyContent: "center",
                  alignItems: "center",
                  position: "relative",
                  borderRadius: 8,
                }}
              >
                <BlurView
                  tint="light"
                  intensity={90}
                  style={{
                    position: "absolute",
                    height: HP50,
                    width: WP100 * 0.9,
                    borderRadius: 8,
                  }}
                />
                <RNImage
                  resizeMode={"cover"}
                  source={require("sf-assets/images/indigo/LogoText.png")}
                  style={{ width: 140, height: 52, aspectRatio: 240 / 80 }}
                />
                <Spacer size={HP3} />
                <Text
                  numberOfLines={2}
                  centered
                  lineHeight={HP5 * 0.85}
                  size="massive"
                  weight={600}
                  color={NEUTRAL_950}
                >
                  Build Indonesian Startup Ecosystem
                </Text>

                <Spacer size={HP5} />
                <View style={{ paddingHorizontal: WP10 }}>
                  <TouchableOpacity
                    style={{
                      alignSelf: "center",
                      width: WP90 * 0.93,
                      marginVertical: HP1,
                      paddingVertical: 12,
                      alignItems: "center",
                      paddingHorizontal: WP5,
                      flexDirection: "row",
                      backgroundColor: PRIMARY_90,
                      justifyContent: "space-between",
                      borderRadius,
                    }}
                    onPress={() => navigateTo("LoginSsoScreen")}
                  >
                    <Icon
                      name={"email-outline"}
                      size={"large"}
                      type={"MaterialCommunityIcons"}
                      color={WHITE}
                    />
                    <Text
                      numberOfLines={1}
                      centered
                      size="mini"
                      weight={500}
                      color={WHITE}
                    >
                      Login dengan Indigo SSO
                    </Text>
                    <Icon
                      name={"email-outline"}
                      size={"large"}
                      type={"MaterialCommunityIcons"}
                      color={TRANSPARENT}
                    />
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      alignSelf: "center",
                      width: WP90 * 0.93,
                      marginVertical: HP1,
                      paddingVertical: 12,
                      alignItems: "center",
                      paddingHorizontal: WP5,
                      flexDirection: "row",
                      backgroundColor: NAVY_DARK,
                      justifyContent: "space-between",
                      borderRadius,
                    }}
                    onPress={() => navigateTo("LoginEmailScreen")}
                  >
                    <Icon
                      name={"email"}
                      size={"large"}
                      type={"MaterialIcons"}
                      color={BASE_50}
                    />
                    <Text
                      numberOfLines={1}
                      centered
                      size="mini"
                      weight={500}
                      color={BASE_50}
                    >
                      Lanjutkan dengan Email
                    </Text>
                    <Icon
                      name={"email-outline"}
                      size={"large"}
                      type={"MaterialCommunityIcons"}
                      color={TRANSPARENT}
                    />
                  </TouchableOpacity>

                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    onPress={() => navigateTo("RegisterV3")}
                    style={{
                      alignSelf: "center",
                      paddingVertical: WP3,
                    }}
                  >
                    <View
                      style={{ flexDirection: "row", justifyContent: "center" }}
                    >
                      <Text type={"Circular"} size="mini" color={NEUTRAL_950}>
                        {"Belum punya akun? "}
                      </Text>
                      <Text
                        type={"Circular"}
                        size="mini"
                        weight={500}
                        color={REDDISH}
                      >
                        Daftar disini
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <ModalMessageView
              toggleModal={noop}
              isVisible={isModalConfirmMMBVisible}
              buttonPrimaryAction={() => {
                this._toggleModalConfirmMMB();
                navigateTo("LoginEmailScreen");
              }}
              titleType="Circular"
              titleSize={"small"}
              titleColor={GUN_METAL}
              subtitle={"Apakah anda mengikuti program"}
              extraSubtitle={"Muda Maju Bersama 1000 Startup?"}
              extraSubtitleWeight={700}
              subtitleType="Circular"
              subtitleSize={"slight"}
              subtitleWeight={300}
              subtitleLineHeight={WP5}
              subtitleColor={GUN_METAL}
              imageStyle={{ width: WP25, height: WP25 }}
              image={require("sf-assets/images/questionCircle.png")}
              buttonPrimaryText={"Ya"}
              buttonPrimaryTextType="Circular"
              buttonPrimaryTextWeight={400}
              buttonPrimaryBgColor={REDDISH}
              buttonSecondaryText={"Tidak"}
              buttonSecondaryTextType="Circular"
              buttonSecondaryTextWeight={400}
              buttonSecondaryTextColor={SHIP_GREY_CALM}
              buttonSecondaryAction={() => this._handleModalMMBSecondary()}
            />
          </SafeAreaView>
        </ImageBackground>
      </View>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(LoginScreen),
  mapFromNavigationParam,
)
