import React from 'react'
import { isEmpty, isNil, isNaN } from 'lodash-es'
import { connect } from 'react-redux'
import { Image, View, Platform, Image as RNImage } from 'react-native'
import Constants from 'expo-constants'
import { _enhancedNavigation, Container } from 'sf-components'
import { REGISTRATION_STEPS } from 'sf-constants/Routes'
import {
  KEY_COACHMARK_LEVEL,
  KEY_USER_LAST_SKIPPED_STEP,
  KEY_BANNER_POPUP,
  KEY_FORGOT_PASSWROD_EMAIL,
} from 'sf-constants/Storage'
import {
  clearLocalStorage,
  getLocalStorage,
  getUserId,
  setUserCache,
  getUserCache,
  setLocalStorage,
  getRegistrationData,
  getExploreCache,
  getCollabLv1Cache,
  getRecent,
} from 'sf-utils/storage'
import {
  getProfileDetail,
  getCurrentSDKVersion,
  getBannerPopup,
} from 'sf-actions/api'
import { GET_USER_DETAIL } from 'sf-services/auth/actionTypes'
import { SET_REGISTRATION } from 'sf-services/register/actionTypes'
import { GET_NOTIFICATION } from 'sf-services/notification/actionTypes'
import { GET_MESSAGE_UNREAD } from 'sf-services/message/actionTypes'
import { registerUser } from 'sf-utils/registerUser'
import { authDispatcher } from 'sf-services/auth'
import { HP100, WP162 } from 'sf-constants/Sizes'
import { forceUpdate } from 'sf-utils/helper'
import { setExploreData } from 'sf-services/screens/lv0/explore/actionDispatcher'
import { setCollabData } from 'sf-services/screens/lv1/collab/actionDispatcher'
import InteractionManager from 'sf-utils/InteractionManager'
import { KEY_RECENT_SEARCH } from 'sf-constants/Storage'
import { setRecentSearch } from 'sf-services/search/actionDispatcher'
import moment from 'moment'
import { WHITE } from 'sf-constants/Colors'
import { BANNER_SETTER } from '../services/banner/actionTypes'
import { setLoading, setIsLogin } from '../services/app/actionDispatcher'
import { KEY_LAST_TIME_BANNER_POPUP } from '../constants/Storage'
import { setNotifMappingList } from '../utils/storage'
import { getNotificationMapping } from '../actions/api'
import { WP4 } from '../constants/Sizes'

const mapDispatchToProps = {
  setUserDetail: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail,
  }),
  setNewNotification: (notification) => ({
    type: `${GET_NOTIFICATION}_PUSHER`,
    response: notification,
  }),
  setUnreadMessage: (unreadMessageCount) => ({
    type: GET_MESSAGE_UNREAD,
    response: unreadMessageCount,
  }),
  setCoachmarkLevel: (response) => ({
    type: `${KEY_COACHMARK_LEVEL}_SETTER`,
    response,
  }),
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setRegistration: (data) => ({
    type: SET_REGISTRATION,
    response: data,
  }),
  setBanner: (data) => ({
    type: BANNER_SETTER,
    response: data,
  }),
  clearBanner: () => ({
    type: SET_REGISTRATION,
    response: null,
  }),
  setExploreData,
  setCollabData,
  setRecentSearch,
  setLoading,
  setIsLogin,
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
})

class LoadingScreen extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._bootstrapAsync)
  }

  getNotificationMapping = async () => {
    const { dispatch } = this.props

    const mapping = await dispatch(getNotificationMapping, {})

    await setNotifMappingList(mapping)
  };

  getUserData = async (userId) => {
    const { dispatch } = this.props

    if (isEmpty(userId)) return {}

    return await dispatch(getProfileDetail, {
      id_user: userId,
      show: 'all',
    })
  };

  _bootstrapAsync = async () => {
    // __DEV__ && await setUserId("19804");
    const {
      navigateTo,
      setUserDetail,
      setCoachmarkLevel,
      setRegistration,
      setLoading,
      setIsLogin,
    } = this.props
    setIsLogin(false)

    const userId = await getUserId()

    // this._getBanner(userId)
    const asyncResults = await Promise.all([
      this.getNotificationMapping(),
      getExploreCache(),
      getCollabLv1Cache(),
      this.getUserData(userId),
      this._checkSdkVersion(),
    ])

    const explore = asyncResults[1]
    const collab = asyncResults[2]
    const userDataResponse = asyncResults[3]

    getRecent(KEY_RECENT_SEARCH).then((recents) => {
      this.props.setRecentSearch(recents.filter((recent) => !!recent))
    })
    if (!isNil(explore) && !isEmpty(explore)) {
      // no more loading indicator on landing screen
      // update will be done in background and
      // will be updated only if data is not equal
      this.props.setExploreData(JSON.parse(explore))
    }
    if (!isNil(collab) && !isEmpty(collab)) {
      this.props.setCollabData(JSON.parse(explore))
    }
    // Load user data from local storage

    const coachmarkLevel = Number(await getLocalStorage(KEY_COACHMARK_LEVEL)) || 0
    setCoachmarkLevel({ coachmarkLevel })
    if (!isEmpty(userId)) {
      const lastSkippedStep = Number(
        await getLocalStorage(KEY_USER_LAST_SKIPPED_STEP),
      )
      try {
        const step = Number(userDataResponse.registration_step)
        if (userDataResponse && userDataResponse.id_user) {
          setUserDetail(userDataResponse)
          setUserCache(JSON.stringify(userDataResponse))
        } else {
          const cache = await getUserCache()
          setUserDetail(JSON.parse(cache))
        }

        const landingScreen = 'AppNavigator'

        if (
          typeof global.coldStartNotification != 'undefined' &&
          !!global.coldStartNotification
        ) {
          this.props.navigation.navigate(landingScreen, {
            screen: 'ExploreStack',
            initial: true,
            params: {
              screen: global.coldStartNotification.to,
              params: global.coldStartNotification.payload,
              initial: false,
            },
          })
          global.coldStartNotification = null
        } else if (
          isNaN(step) ||
          isNaN(lastSkippedStep) ||
          (step > 3 && userDataResponse.email_status === 'verified')
        ) {
          if (landingScreen == 'AppNavigator') this.props.navigateBack()
          else
            navigateTo(
              landingScreen,
              {
                initialLoaded: true,
              },
              'replace',
            )
        } else {
          const currentStep = lastSkippedStep >= step ? lastSkippedStep + 1 : step
          if (currentStep) {
            navigateTo(
              REGISTRATION_STEPS[currentStep],
              {
                initialLoaded: true,
              },
              'replace',
            )
          } else {
            if (landingScreen == 'AppNavigator') this.props.navigateBack()
            else
              navigateTo(
                landingScreen,
                {
                  initialLoaded: true,
                },
                'replace',
              )
          }
        }
      } catch (e) {
        // e
      }
      registerUser(userId.toString(), this.props)
      setLoading(false)
    } else {
      const registrationData = await getRegistrationData()
      if (!isEmpty(registrationData) && registrationData.data.id_user) {
        Promise.all([setRegistration(registrationData)]).then(() => {
          navigateTo('AuthNavigator', {}, 'replace')
        })
      } else {
        const forgotPasswordEmail = await getLocalStorage(KEY_FORGOT_PASSWROD_EMAIL)
        await clearLocalStorage()
        if (!isEmpty(forgotPasswordEmail))
          await setLocalStorage(KEY_FORGOT_PASSWROD_EMAIL, forgotPasswordEmail)
        this.props.navigateBack()
        // navigateTo('AppNavigator', {}, 'replace')
      }
    }
  };
  async _checkSdkVersion() {
    const { dispatch } = this.props
    const installedSDKVersion = Constants.manifest.sdkVersion.match(/(\d+)/g)[0]
    const response = await dispatch(getCurrentSDKVersion)
    const currentSDKVersion = response.value.match(/(\d+)/g)[0]
    parseInt(installedSDKVersion) < parseInt(currentSDKVersion) && forceUpdate()
  }

  async _getBanner(userId) {
    const { dispatch } = this.props

    if (isEmpty(userId)) return null

    const response = await dispatch(getBannerPopup, {
      id_user: userId,
    })
    if (response.value) {
      const result = JSON.parse(response.value)
      let banner
      if (Array.isArray(result) && result.length > 0) {
        banner = result[0]
      } else {
        banner = result
      }
      if (banner.image) await RNImage.prefetch(banner.image)

      const count = await getLocalStorage(
        KEY_BANNER_POPUP + moment().format('L') + banner.bannerId,
      )
      const lastBannerTime = await getLocalStorage(KEY_LAST_TIME_BANNER_POPUP)
      const interval = moment().unix() - lastBannerTime
      if ((isEmpty(count) || Number(count) < 3) && interval > 3600) {
        if (banner.bannerId === '1') {
          await this._getPackages()
        }
        this.props.setBanner(banner)
        await setLocalStorage(
          KEY_BANNER_POPUP + moment().format('L') + banner.bannerId,
          String(Number(count || 0) + 1),
        )
        await setLocalStorage(KEY_LAST_TIME_BANNER_POPUP, String(moment().unix()))
      }
    } else {
      this.props.clearBanner()
    }
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View
          style={{
            width: WP162 + 20,
            height: WP162 + 20,
            backgroundColor: WHITE,
            borderRadius: WP4,
            justifyContent: 'center',
            alignItems: 'center',
            elevation: 100,
          }}
        >
          <Image
            source={require('sf-assets/loading.gif')}
            style={{
              width: WP162 - 10,
              height: undefined,
              aspectRatio: 840 / 607,
            }}
            fadeDuration={0}
          />
        </View>
      </View>
    )
  }
}

export default _enhancedNavigation(
  connect(null, mapDispatchToProps)(LoadingScreen),
  mapFromNavigationParam,
)
