import React from 'react'
import {
  Animated,
  TextInput,
  View,
  TouchableOpacity,
  Modal,
  Dimensions,
  BackHandler,
  Keyboard
} from 'react-native'
import { isEmpty } from 'lodash-es'
import { connect } from 'react-redux'
import { Text, _enhancedNavigation, Container } from '../components'
import Dialog from '../components/Dialog'
import {
  TOMATO_CALM,
  WHITE,
  GREY_CALM_SEMI,
  GREY
} from '../constants/Colors'
import { WP2, WP4, WP15 } from '../constants/Sizes'
import style from '../styles/register'
import { postCheckRecoveryCode } from '../actions/api'
import { isEmailValid } from '../utils/helper'
import { getLocalStorage } from '../utils/storage'
import { KEY_FORGOT_PASSWROD_EMAIL } from '../constants/Storage'
import { KEYBOARD_VISIBLE } from '../services/helper/actionTypes'
import Wrapper from './register/v3/Wrapper'

const { height } = Dimensions.get('window')

const mapStateToProps = () => ({

})

const mapDispatchToProps = {
  keyboardVisible: (response) => ({
    type: KEYBOARD_VISIBLE,
    response
  })
}

class RecoveryCodeScreen extends React.Component {

  fadeIn = new Animated.Value(0)
  recoveryInput = React.createRef()

  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
      errorMessage: null,
      showPassword: false,
      marginTop: 0,
      password: null,
      isLoading: false,
      isConfirmation: false,
      emailSent: false,
      recovery_code: ''
    }
    this.onChangeText = this.onChangeText.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.onLayout = this.onLayout.bind(this)
    this.backHandler = this.backHandler.bind(this)
  }

  backHandler() {
    return true
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this.props.keyboardVisible(true))
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.props.keyboardVisible(false))
    BackHandler.addEventListener('hardwareBackPress', this.backHandler)
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
    this.props.keyboardVisible(false)
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler)
  }

  onLayout({ nativeEvent }) {
    const viewHeight = nativeEvent.layout.height
    this.setState(
      {
        marginTop: height - viewHeight - viewHeight / 2,
      },
      () => {
        Animated.timing(this.fadeIn, {
          toValue: 1,
          duration: 50,
          useNativeDriver: true
        }).start()
      }
    )
  }

  toggleModal() {
    this.setState({ modalVisible: !this.state.modalVisible })
  }

  async onSubmit() {
    const { navigateTo } = this.props
    const { recovery_code } = this.state
    const email = await getLocalStorage(KEY_FORGOT_PASSWROD_EMAIL)
    if (!isEmailValid(email)) {
      this.setState({
        modalVisible: true,
        errorMessage: 'Incorrect email address',
        isConfirmation: false,
      })
    } else {
      this.setState(
        {
          isLoading: true,
        },
        () => {
          postCheckRecoveryCode({ email, forgotten_password_code: recovery_code })
            .then(({ data }) => {
              const { code } = data
              code == 200 && navigateTo('ResetPasswordScreen')
              code == 500 && this.setState({
                modalVisible: true,
                errorMessage: 'Incorrect recovery code',
                isConfirmation: false,
              })
            })
            .catch((e) => {})
            .then(() => this.setState({ isLoading: false }))
        }
      )
    }
  }

  onChangeText(value, callback) {
    Promise.all([this.setState({ errorMessage: null }), callback(value)]).then(
      () => {
        const { email, setCtaEnabled } = this.props
        setCtaEnabled(!isEmpty(email))
      }
    )
  }

  render() {
    const { navigateBack, navigation, navigateTo } = this.props
    const {
      modalVisible,
      errorMessage,
      recovery_code,
      isLoading,
      isConfirmation,
    } = this.state
    return (
      <Container isLoading={isLoading}>
        <Wrapper
          cantBack={true}
          customBack={navigateBack}
          cta={{
            text: 'Continue',
            enabled: recovery_code.length == 6 && !isLoading,
            onPress: this.onSubmit,
          }}
          headings={['No worries,', 'it’s Sent!']}
        >
          <Animated.View
            style={{ opacity: this.fadeIn }}
            onLayout={this.onLayout}
          >
            <Text style={{ marginBottom: WP15 }}>
              {'Input your recovery code from email.'}
            </Text>
            <View
              style={style.formGroup}
            >
              <Text
                style={style.label}
                size={'mini'}
                weight={500}
                type={'NeoSans'}
              >
                Recovery Code
              </Text>
              <TextInput
                ref={(ref) => { this.recoveryInput = ref }}
                value={recovery_code}
                selectionColor={WHITE}
                onChangeText={(code) => {
                  if(code.length <= 6) this.setState({ recovery_code: code.trim().replace(/\D/g, '') })
                }}
                keyboardType={'number-pad'}
                style={{ ...style.input, opacity: 0, marginBottom: -52 }}
              />
              <TouchableOpacity activeOpacity={1} onPress={() => this.recoveryInput.focus()}>
                <View style={{ flexDirection: 'row', marginTop: WP2, marginHorizontal: -WP2 }}>
                  {
                    [...Array(6).keys()].map((n) => (<Text
                      key={n}
                      size={'semiJumbo'}
                      color={recovery_code[n] ? GREY : WHITE}
                      weight={400}
                      centered
                      style={{
                        flex: 1,
                        borderBottomWidth: 3,
                        borderBottomColor: recovery_code[n] ? GREY : GREY_CALM_SEMI,
                        marginHorizontal: WP2,
                      }}
                                                     >
                      {recovery_code[n] || '-'}
                    </Text>))
                  }
                </View>
              </TouchableOpacity>
            </View>
          </Animated.View>
        </Wrapper>
        <Modal
          transparent
          onRequestClose={this.toggleModal}
          visible={modalVisible}
        >
          <Dialog
            width={270}
            isConfirmation={isConfirmation}
            height={isConfirmation ? 128 : 120}
            cancelText={'Sign Up'}
            confirmText={'Try Again'}
            onCancel={() =>
              this.setState({ modalVisible: false }, () => {
                Promise.resolve(navigation.popToTop()).then(() => {
                  navigateTo('RegisterV3')
                })
              })
            }
            onConfirm={this.toggleModal}
          >
            <Text
              centered
              style={{ marginTop: isConfirmation ? 0 : WP4, paddingHorizontal: WP4 }}
              size={'xmini'}
            >
              {errorMessage}
            </Text>
            {!isConfirmation && (
              <TouchableOpacity
                style={{ paddingVertical: WP4 }}
                onPress={() => {
                  this.toggleModal()
                  this.recoveryInput.blur()
                  setTimeout(() => {
                    this.recoveryInput.focus()
                  }, 500)
                }}
                activeOpacity={0.8}
              >
                <Text
                  centered
                  color={TOMATO_CALM}
                  weight={500}
                  type={'NeoSans'}
                  size={'xmini'}
                >
                  Try Again
                </Text>
              </TouchableOpacity>
            )}
          </Dialog>
        </Modal>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(RecoveryCodeScreen),
  null
)
