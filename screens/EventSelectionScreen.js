import React, { Component } from 'react';
import { connect } from "react-redux";
import { View, StyleSheet, ScrollView, Image, FlatList, BackHandler, ToastAndroid } from "react-native";
import { minimizeApp } from "sf-utils/backhandler";
import InteractionManager from "sf-utils/InteractionManager";
import Container from "../components/Container";
import { Text, _enhancedNavigation } from "../components";
import { BLACK, GUN_METAL, LIGHT_BLUE, NAVY_DARK, PALE_BLUE, PALE_WHITE, REDDISH, RED_20, SHIP_GREY, SHIP_GREY_CALM, TRANSPARENT, WHITE, } from "../constants/Colors";
import { HP1, HP10, HP100, HP2, HP25, HP3, HP5, HP50, WP1, WP100, WP2, WP3, WP5, WP6, WP80, WP85, WP90, WP95 } from '../constants/Sizes';
import ButtonV2 from '../components/ButtonV2';
import { getEventList, postEventParticipant, getEventParticipantList } from '../actions/api';
import { isEmpty, isNil, upperFirst } from "lodash-es";
import EventSelection from '../components/EventSelection';
import HeaderNormal from "sf-components/HeaderNormal";

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
});

const mapFromNavigationParam = (getParam) => ({});

const mapDispatchToProps = {};

class EventSelectionScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventList: [],
      exitable: false,
    };
    this.attachBackAction = this.attachBackAction.bind(this);
    this.releaseBackAction = this.releaseBackAction.bind(this);
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener(
      "focus",
      this.attachBackAction
    );
    this.blurListener = this.props.navigation.addListener(
      "blur",
      this.releaseBackAction
    );
    InteractionManager.runAfterInteractions(() => {
      this._getEventList();
    })
  }

  componentWillUnmount() {
    this.releaseBackAction();
  }

  attachBackAction() {
    const backConfirmation = () => {
      const { exitable } = this.state;
      const { userData: { event_title } } = this.props;
      if(isEmpty(event_title)) {
        if (exitable) minimizeApp();
        else this.setState({ exitable: true }, () => {
          ToastAndroid.show("Press again to exit.", ToastAndroid.SHORT);
          this.timeout = setTimeout(
            () => this.setState({ exitable: false }),
            2000
          );
        });
        return true;
      }
    };
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backConfirmation.bind(this)
    );
  }

  releaseBackAction() {
    typeof this.backHandler === "object" &&
      typeof this.backHandler.remove === "function" &&
      this.backHandler.remove();
  }

  _renderHeader = () => {
    const { userData: { event_title } } = this.props;
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftColor={isEmpty(event_title) ? TRANSPARENT : SHIP_GREY_CALM}
          iconLeftOnPress={() => !isEmpty(event_title) && this.props.navigation.goBack(null)}
          centered
          textType={"Circular"}
          textSize={"slight"}
          text={upperFirst("Select Event")}
        />
      </View>
    );
  };

  _getEventList = async () => {
    const { dispatch, userData: { id_user } } = this.props;

    await dispatch(getEventList, { status: 'active', id_user })
      .then((result) => {
        const data = result;
        if (!isEmpty(data)) this.setState({ eventList: result });
      })
      .catch((err) => console.log(err));
  };

  render() {
    return (
      <Container
        scrollable={true}
        backgroundColor={PALE_WHITE}
        renderHeader={this._renderHeader}
      >
        <EventSelection
          data={this.state.eventList}
          navigateTo={this.props.navigateTo}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
    paddingVertical: 16
  },
});

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(EventSelectionScreen),
  mapFromNavigationParam
);
