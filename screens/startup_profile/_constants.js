/* @flow */

import { compact, isEmpty, reduce, split } from 'lodash-es'
import moment from 'moment'
import * as yup from 'yup'

export const DUMP_DATA_FROM_API = {
  'about_me': 'more about me',
  'access': 'user',
  'account_status': 'active',
  'account_type': 'user',
  'activation_code': null,
  'birthdate': '10 April 2019',
  'client_id': 'NULL',
  'company': 'Skalasound',
  'cover_image': 'https://apidev.soundfren.id/userfiles/images/cover_image/1571152351_322_cover_image.jpg',
  'created_at': '2019-04-04 15:09:36',
  'email': 'ibnu.dev@gmail.com',
  'email_status': 'verified',
  'expo_token': 'ExponentPushToken[gp8fKVISpagXPd4roKYFtI]',
  'forgotten_password': '317107',
  'forgotten_password_time': '1555210754',
  'full_name': 'User 3',
  'gender': 'male',
  'id_city': '1103',
  'id_social': '111238152382463985565',
  'id_user': '322',
  'interest': [{
    'icon': 'https://eventeer.id/assets/image/icon_interest/pop.png',
    'id_interest': '6',
    'id_user': '322',
    'id_user_profile': '1805',
    'interest_name': 'Pop',
  },
  {
    'icon': 'https://eventeer.id/assets/image/icon_interest/jazz.png',
    'id_interest': '5',
    'id_user': '322',
    'id_user_profile': '1806',
    'interest_name': 'Jazz',
  },
  ],
  'is_feedback': '-1',
  'job_title': 'Music Prod',
  'last_login': '2020-02-02 21:40:07',
  'location': {
    'city_name': 'ACEH SELATAN',
    'country_name': 'Indonesia',
    'id_city': '1103',
    'id_country': '1',
    'id_province': '11',
    'order': '100',
    'province_name': 'ACEH',
  },
  'password': 'e83bf8ed8f6b5fbef419a1c1793ea703',
  'phone': '',
  'private_gender': 'false',
  'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/322_profile_picture.jpg',
  'registered_via': 'google',
  'registration_step': 'finish',
  'social_media': {
    'facebook': 'facebook.com/ffffz',
    'instagram': '',
    'twitter': 'twitter.com/uu',
    'website': '',
    'youtube': '',
  },
  'total_connection': 0,
  'total_followers': 3,
  'total_following': 1,
  'total_post': 0,
  'website': 'example.com',
}

export const DUMP_API_DATA = {}

export const DUMP_COLLABORATION_DATA = {}

// PROFILE_DATA Validation
export const FORM_VALIDATION = yup.object().shape({
  profileName: yup.string().required(),
  profilePictureb64: yup.string().required(),
  // coverImageb64: yup.string().required(),
  profileCityId: yup.string().required(),
  profileProfessionTitle: yup.string().required(),
  profileProfessionCompany: yup.string().required(),
  profileInterests: yup.array().test('min-1', 'Minimal 1 value', (value) => !isEmpty(compact(value))),
  profileBirthDate: yup.string().required(),
  profileGender: yup.string().required(),
})

// API_DATA to PROFILE_DATA
export const MAPPER_FROM_API = {
  profilePictureUri: 'profile_picture',
  profilePictureb64: 'profilePictureb64',
  coverImageUri: 'cover_image',
  coverImageb64: 'coverImageb64',
  profileName: 'full_name',
  profileCityId: 'location.id_city',
  profileCityName: 'location.city_name',
  profileProfessionTitle: 'job_title',
  profileProfessionCompany: 'company',
  profileProfessionTitles: ['job', { title: 'job_title' }],
  profileProfessionCompanies: ['job', { company: 'company_name' }],
  profileInterests: (source) => reduce(source.interest, (result, value) => {
    result.push(value.interest_name)
    return result
  }, []),
  profileAbout: 'about_me',
  profileSocialMedia: (source) => {
    const socialMedia = source.social_media
    socialMedia.facebook = split(socialMedia.facebook, '.com/')[1]
    socialMedia.twitter = split(socialMedia.twitter, '.com/')[1]
    socialMedia.instagram = split(socialMedia.instagram, '.com/')[1]
    return socialMedia
  },
  profileGender: 'gender',
  profileGenderShow: (source) => source.private_gender === '0' ? '1' : '0',
  profileBirthDate: (source) => moment(source.birthdate, 'YYYY-MM-DD').format('DD/MM/YYYY'),
  profileBirthDateShow: (source) => source.private_birthdate === '0' ? '1' : '0',
  profileEmail: 'email',
  createdBy: 'created_by',
  createdAt: 'created_at',
  updatedBy: 'updated_by',
  updatedAt: 'updated_at',
}

// PROFILE_DATA to API_DATA
export const MAPPER_FOR_API = {
  'profile_picture': 'profilePictureb64',
  'cover_image': 'coverImageb64',
  'full_name': 'profileName',
  'id_city': 'profileCityId',
  'job_title': 'profileProfessionTitle',
  'company_name': 'profileProfessionCompany',
  'another_job_title': (source) => reduce(source.profileProfessionTitles, (result, value) => {
    result.push(value.title)
    return result
  }, []),
  'another_company_name': (source) => reduce(source.profileProfessionCompanies, (result, value) => {
    result.push(value.company)
    return result
  }, []),
  'about_me': 'profileAbout',
  'interest_name': 'profileInterests',
  'social_media': 'profileSocialMedia',
  'gender': 'profileGender',
  'private_gender': (source) => source.profileGenderShow === '0' ? '1' : '0',
  'birthdate': (source) => moment(source.profileBirthDate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
  'private_birthdate': (source) => source.profileBirthDateShow === '0' ? '1' : '0',
}

export type Profile = {
  id: string | number;
}

export type PropsType = {
  initialLoaded?: boolean;
}

export type StateType = {
  isReady?: boolean;
  profile?: Profile;
}
