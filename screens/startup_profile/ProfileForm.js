import React from 'react'
import { connect } from 'react-redux'
import { BackHandler, Keyboard, TouchableOpacity, View } from 'react-native'
import { isEmpty, map, noop, startCase, toLower } from 'lodash-es'
import {
  _enhancedNavigation,
  Avatar,
  Button,
  Card,
  Checkbox,
  Container,
  Form,
  HeaderNormal,
  Icon,
  Image,
  InputDate,
  InputModal,
  InputTextLight,
  Menu,
  SelectionBar,
  Text,
} from '../../components'
import { BLUE_LIGHT, GREY50, GREY_CALM_SEMI, GREY_DISABLE, SILVER_GREY, TOMATO, WHITE } from '../../constants/Colors'
import { HP1, HP105, IMAGE_SIZE, WP1, WP100, WP2, WP3, WP30, WP4, WP6 } from '../../constants/Sizes'
import { selectPhoto, takePhoto } from '../../utils/upload'
import { convertBlobToBase64, fetchAsBlob } from '../../utils/helper'
import { getCity, getCompany, getGenreInterest, getJob, getProfileDetail, postProfileUpdate } from '../../actions/api'
import { objectMapper } from '../../utils/mapper'
import { BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'
import BackModal from '../../components/BackModal'
import { authDispatcher } from '../../services/auth'
import { FORM_VALIDATION, MAPPER_FOR_API, MAPPER_FROM_API } from './_constants'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
}

const GENDERS = ['male', 'female']

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  refreshProfile: getParam('refreshProfile', () => {}),
})

const propsDefault = {}

class ProfileForm extends React.Component {
  constructor(props) {
    super(props)
    this._didFocusSubscription = props.navigation.addListener('focus', (payload) =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler),
    )
  }

  state = {
    profile: {},
    collaborationDurations: [],
    isReady: false,
    backModal: false,
  };

  async componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('blur', (payload) =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler),
    )
    await this._getInitialScreenData()
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _didFocusSubscription;
  _willBlurSubscription;

  _getInitialScreenData = async (...args) => {
    await this._getProfileDetail(...args)
    await this.setState({
      isReady: true,
    })
  };

  _getProfileDetail = async (isLoading = true) => {
    const { userData, dispatch } = this.props
    const dataResponse = await dispatch(getProfileDetail, { id_user: userData.id_user, show: 'all' }, noop, true, true)
    const profileData = objectMapper(dataResponse.result, MAPPER_FROM_API)
    const profilePictureb64 = !profileData.profilePictureUri
      ? null
      : await fetchAsBlob(profileData.profilePictureUri).then(convertBlobToBase64)
    const coverImageb64 = !profileData.coverImageUri
      ? null
      : await fetchAsBlob(profileData.coverImageUri).then(convertBlobToBase64)
    const profileDataNew = {
      ...profileData,
      profilePictureb64,
      coverImageb64,
    }
    await this.setState({ profile: profileDataNew })
  };

  _backHandler = async () => {
    const { navigateBack } = this.props
    const { backModal } = this.state

    if (this.form && this.form.isDirtyState()) {
      this.setState({ backModal: !backModal })
    } else {
      navigateBack()
    }
  };

  _postProfileUpdate = async ({ formData }) => {
    const { userData, dispatch } = this.props
    const dataForApi = objectMapper(formData, MAPPER_FOR_API)
    if (!dataForApi.about_me) dataForApi.about_me = ''
    await dispatch(
      postProfileUpdate,
      {
        id_user: userData.id_user,
        ...dataForApi,
      },
      noop,
      true,
      true,
    )
    this.props.getUserDetailDispatcher({ id_user: userData.id_user })
    this.props.refreshProfile()
    this.props.navigateBack()
  };

  _genreItem = (genre, index, onChange, formData) => {
    return (
      <View key={index}>
        <View
          style={{
            marginRight: WP2,
            marginVertical: WP1,
            paddingHorizontal: WP2,
            paddingVertical: WP1,
            borderRadius: WP100,
            justifyContent: 'space-between',
            alignSelf: 'baseline',
            flexDirection: 'row',
            borderWidth: 1,
            borderColor: TOMATO,
          }}
        >
          <Text size='xtiny' style={{ marginLeft: WP2 }} color={TOMATO}>
            {genre}
          </Text>
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            onPress={() => {
              const newGenre = formData.profileInterests
              newGenre.splice(index, 1)
              onChange('profileInterests')(newGenre)
            }}
          >
            <Icon centered name='closecircle' color={TOMATO} size='tiny' style={{ marginHorizontal: WP2 }} />
          </TouchableOpacity>
        </View>
      </View>
    )
  };

  render() {
    const { navigateBack, isLoading } = this.props
    const { isReady, profile, backModal } = this.state
    return (
      <Form
        ref={(form) => (this.form = form)}
        validation={FORM_VALIDATION}
        initialValue={{ profileProfessionTitles: [], profileProfessionCompanies: [], ...profile }}
        onSubmit={this._postProfileUpdate}
      >
        {({ onChange, onSubmit, formData, isValid, isDirty }) => (
          <Container
            isLoading={isLoading}
            isReady={isReady}
            renderHeader={() => (
              <HeaderNormal
                iconLeftOnPress={this._backHandler}
                text='Edit Profile'
                rightComponent={
                  <TouchableOpacity
                    disabled={!isValid || !isDirty}
                    activeOpacity={TOUCH_OPACITY}
                    style={{
                      paddingHorizontal: WP3,
                      paddingVertical: WP1,
                      borderRadius: 6,
                      borderWidth: BORDER_WIDTH,
                      borderColor: isValid && isDirty ? TOMATO : GREY_CALM_SEMI,
                      backgroundColor: isValid && isDirty ? TOMATO : WHITE,
                    }}
                    onPress={() => {
                      Keyboard.dismiss()
                      onSubmit()
                    }}
                  >
                    <Text
                      centered
                      size='tiny'
                      type='NeoSans'
                      weight={500}
                      color={isValid && isDirty ? WHITE : GREY_CALM_SEMI}
                    >
                      Save
                    </Text>
                  </TouchableOpacity>
                }
              />
            )}
            isAvoidingView
            scrollable
            scrollBackgroundColor={WHITE}
            scrollContentContainerStyle={{ justifyContent: 'space-between' }}
          >
            <BackModal
              modalVisible={backModal}
              navigateBack={navigateBack}
              dismiss={() => this.setState({ backModal: false })}
            />

            <View style={{ backgroundColor: GREY_CALM_SEMI }}>
              <View style={{ flexDirection: 'row', backgroundColor: WHITE, padding: WP6, paddingBottom: 0 }}>
                <View>
                  <Text centered size='xtiny' weight={500} style={{ paddingHorizontal: WP2 }}>
                    Profile Picture
                  </Text>
                  <View style={{ padding: WP2 }}>
                    <Avatar
                      noPlaceholder
                      shadow={false}
                      size='extraMassive'
                      imageStyle={[!formData.profilePictureUri && { borderWidth: 1, borderColor: GREY_CALM_SEMI }]}
                      image={formData.profilePictureUri}
                    />
                    <Menu
                      options={[
                        {
                          onPress: selectPhoto(onChange, 'profilePicture', [1, 1]),
                          name: 'Select from library',
                          iconName: 'photo-library',
                        },
                        {
                          onPress: takePhoto(onChange, 'profilePicture', [1, 1]),
                          name: 'Take a picture',
                          iconName: 'camera',
                        },
                      ]}
                      triggerComponent={(toggleModal) => (
                        <Button
                          onPress={toggleModal}
                          iconName='image'
                          iconType='Entypo'
                          iconSize='tiny'
                          style={{
                            marginVertical: 0,
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                          contentStyle={{ padding: WP2 }}
                          shadow='none'
                          colors={[SILVER_GREY, SILVER_GREY]}
                          rounded
                          iconColor={WHITE}
                        />
                      )}
                    />
                  </View>
                  {/*{*/}
                  {/*  formData.profilePictureUri && (*/}
                  {/*    <Button*/}
                  {/*      onPress={() => onChange('profilePictureUri')(null)}*/}
                  {/*      iconName='close'*/}
                  {/*      iconType='FontAwesome'*/}
                  {/*      iconSize='mini'*/}
                  {/*      style={{ marginVertical: 0, position: 'absolute', bottom: WP4, right: WP4 }}*/}
                  {/*      contentStyle={{ padding: WP1 }}*/}
                  {/*      shadow='none'*/}
                  {/*      colors={[TOMATO, TOMATO]}*/}
                  {/*      rounded*/}
                  {/*      iconColor={WHITE}*/}
                  {/*    />*/}
                  {/*  )*/}
                  {/*}*/}
                </View>
                <View style={{ flex: 1 }}>
                  <Text centered size='xtiny' weight={500} style={{ paddingHorizontal: WP2 }}>
                    Header Picture
                  </Text>
                  <View style={{ padding: WP2, flex: 1, justifyContent: 'center' }}>
                    <Image
                      source={!isEmpty(formData.coverImageUri) ? { uri: formData.coverImageUri } : null}
                      imageStyle={[
                        { borderRadius: WP2, height: IMAGE_SIZE['extraMassive'] },
                        !formData.coverImageUri && { borderWidth: 1, borderColor: GREY_CALM_SEMI },
                      ]}
                      aspectRatio={1.65}
                    />
                    <Menu
                      options={[
                        {
                          onPress: selectPhoto(onChange, 'coverImage', [2, 1]),
                          name: 'Select from library',
                          iconName: 'photo-library',
                        },
                        {
                          onPress: takePhoto(onChange, 'coverImage', [2, 1]),
                          name: 'Take a picture',
                          iconName: 'camera',
                        },
                      ]}
                      triggerComponent={(toggleModal) => (
                        <Button
                          onPress={toggleModal}
                          iconName='image'
                          iconType='Entypo'
                          iconSize='tiny'
                          style={{
                            marginVertical: 0,
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                          contentStyle={{ padding: WP2 }}
                          shadow='none'
                          colors={[SILVER_GREY, SILVER_GREY]}
                          rounded
                          iconColor={WHITE}
                        />
                      )}
                    />
                  </View>
                  {/*{*/}
                  {/*  formData.coverImageUri && (*/}
                  {/*    <Button*/}
                  {/*      onPress={() => onChange('coverImageUri')(null)}*/}
                  {/*      iconName='close'*/}
                  {/*      iconType='FontAwesome'*/}
                  {/*      iconSize='mini'*/}
                  {/*      style={{ marginVertical: 0, position: 'absolute', bottom: WP4, right: WP4 }}*/}
                  {/*      contentStyle={{ padding: WP1 }}*/}
                  {/*      shadow='none'*/}
                  {/*      colors={[TOMATO, TOMATO]}*/}
                  {/*      rounded*/}
                  {/*      iconColor={WHITE}*/}
                  {/*    />*/}
                  {/*  )*/}
                  {/*}*/}
                </View>
              </View>
              <Card first>
                <InputTextLight
                  withLabel={false}
                  placeholder='Johny Genjreng'
                  wording='Name of Profile'
                  value={formData.profileName}
                  onChangeText={onChange('profileName')}
                />
                <InputModal
                  refreshOnSelect
                  triggerComponent={
                    <InputTextLight
                      withLabel={false}
                      placeholder='ex: Jakarta'
                      wording='City'
                      value={formData.profileCityName}
                    />
                  }
                  header='Select City'
                  suggestion={getCity}
                  suggestionKey='city_name'
                  suggestionPathResult='city_name'
                  suggestionPathValue='id_city'
                  selected={formData.profileCityName}
                  onChange={(city) => {
                    onChange('profileCityName')(city.city_name)
                    onChange('profileCityId')(city.id_city)
                  }}
                  suggestionCreateNewOnEmpty={false}
                  createNew={false}
                  asObject={true}
                  reformatFromApi={(text) => startCase(toLower(text))}
                  placeholder='Search city here...'
                />
              </Card>
              <Card>
                <Text size='xtiny' weight={500}>
                  PRIMARY PROFESSION
                </Text>
                <InputModal
                  extraResult
                  refreshOnSelect
                  triggerComponent={
                    <InputTextLight
                      withLabel={false}
                      placeholder='ex: Guitarist'
                      wording='Profession'
                      value={formData.profileProfessionTitle}
                    />
                  }
                  header='Select Profession'
                  suggestion={getJob}
                  suggestionKey='job_title'
                  suggestionPathResult='job_title'
                  selected={formData.profileProfessionTitle}
                  onChange={onChange('profileProfessionTitle')}
                  reformatFromApi={(text) => startCase(toLower(text))}
                  placeholder='Search profession here...'
                />
                <InputModal
                  extraResult
                  refreshOnSelect
                  triggerComponent={
                    <InputTextLight
                      withLabel={false}
                      placeholder='ex: Blueband Reunion'
                      wording='Company / Group / Team'
                      value={formData.profileProfessionCompany}
                    />
                  }
                  header='Select Company / Group / Team'
                  suggestion={getCompany}
                  suggestionKey='company_name'
                  suggestionPathResult='company_name'
                  selected={formData.profileProfessionCompany}
                  onChange={onChange('profileProfessionCompany')}
                  reformatFromApi={(text) => startCase(toLower(text))}
                  placeholder='Search company here...'
                />
                <View>
                  {map(formData.profileProfessionTitles, (profileProfessionTitle, index) => (
                    <View
                      key={`profession-alt-${index}`}
                      style={{
                        borderRadius: WP4,
                        borderWidth: 2,
                        borderColor: BLUE_LIGHT,
                        padding: WP4,
                        marginVertical: WP1,
                      }}
                    >
                      <Text size='xtiny' weight={500}>
                        ALTERNATIVE PROFESSION
                      </Text>
                      <InputModal
                        extraResult
                        refreshOnSelect
                        triggerComponent={
                          <InputTextLight
                            withLabel={false}
                            placeholder='ex: Guitarist'
                            wording='Profession'
                            value={profileProfessionTitle.title}
                          />
                        }
                        header='Select Profession'
                        suggestion={getJob}
                        suggestionKey='job_title'
                        suggestionPathResult='job_title'
                        selected={profileProfessionTitle.title}
                        onChange={(text) => {
                          onChange('profileProfessionTitles', true, index)({ title: text })
                        }}
                        reformatFromApi={(text) => startCase(toLower(text))}
                        placeholder='Search profession here...'
                      />
                      <InputModal
                        extraResult
                        refreshOnSelect
                        triggerComponent={
                          <InputTextLight
                            withLabel={false}
                            placeholder='ex: Blueband Reunion'
                            wording='Company / Group / Team'
                            value={formData.profileProfessionCompanies[index]?.company}
                          />
                        }
                        header='Select Company / Group / Team'
                        suggestion={getCompany}
                        suggestionKey='company_name'
                        suggestionPathResult='company_name'
                        selected={formData.profileProfessionCompanies[index]?.company}
                        onChange={(text) => {
                          onChange('profileProfessionCompanies', true, index)({ company: text })
                        }}
                        reformatFromApi={(text) => startCase(toLower(text))}
                        placeholder='Search company here...'
                      />
                      <Icon
                        background='dark-circle'
                        backgroundColor={GREY50}
                        style={{ position: 'absolute', top: WP4, right: WP4 }}
                        color={WHITE}
                        name='close'
                        size='tiny'
                        onPress={() => {
                          const { profileProfessionTitles, profileProfessionCompanies } = formData
                          profileProfessionTitles.splice(index, 1)
                          profileProfessionCompanies.splice(index, 1)
                          onChange('profileProfessionTitles')(profileProfessionTitles)
                          onChange('profileProfessionCompanies')(profileProfessionCompanies)
                        }}
                      />
                    </View>
                  ))}
                </View>
                <Button
                  width='40%'
                  style={{
                    alignSelf: 'flex-start',
                    marginTop: HP1,
                  }}
                  onPress={() => {
                    const newProfileProfessionTitles = formData.profileProfessionTitles
                    newProfileProfessionTitles.push({ title: formData.profileProfessionTitle })
                    onChange('profileProfessionTitles')(newProfileProfessionTitles)

                    const newProfileProfessionCompanies = formData.profileProfessionCompanies
                    newProfileProfessionCompanies.push({ company: formData.profileProfessionCompany })
                    onChange('profileProfessionCompanies')(newProfileProfessionCompanies)
                  }}
                  marginless
                  contentStyle={{ paddingHorizontal: WP1, paddingLeft: WP3, justifyContent: 'space-between' }}
                  iconName='add-circle-outline'
                  iconType='MaterialIcons'
                  iconSize='mini'
                  iconPosition='right'
                  iconColor={WHITE}
                  compact='center'
                  rounded
                  backgroundColor={BLUE_LIGHT}
                  shadow='none'
                  text='Profession'
                  textColor={WHITE}
                  textType='NeoSans'
                  textSize='xtiny'
                  textWeight={500}
                />
              </Card>
              <Card>
                <Text size='xtiny' weight={500}>
                  INTEREST
                </Text>
                <View>
                  <View style={{ flexDirection: 'row', marginTop: HP105 }}>
                    <InputModal
                      triggerComponent={
                        <Button
                          style={{
                            alignSelf: 'flex-start',
                            marginTop: HP1,
                          }}
                          marginless
                          contentStyle={{
                            width: WP30,
                            paddingHorizontal: WP1,
                            paddingLeft: WP3,
                            justifyContent: 'space-between',
                          }}
                          iconName='add-circle-outline'
                          iconType='MaterialIcons'
                          iconSize='mini'
                          iconPosition='right'
                          iconColor={WHITE}
                          compact='center'
                          rounded
                          backgroundColor={BLUE_LIGHT}
                          shadow='none'
                          text='Genre'
                          textColor={WHITE}
                          textType='NeoSans'
                          textSize='xtiny'
                          textWeight={500}
                        />
                      }
                      header='Select Genre'
                      suggestion={getGenreInterest}
                      suggestionKey='interest_name'
                      suggestionPathResult='interest_name'
                      onChange={(value) => {
                        const newGenre = formData.profileInterests
                        if (!newGenre.includes(value)) {
                          newGenre.push(value)
                        }
                        onChange('profileInterests')(newGenre)
                      }}
                      selected=''
                      createNew={true}
                      placeholder='Search genre here...'
                      refreshOnSelect={true}
                    />
                  </View>
                </View>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: HP105 }}>
                  {!formData.profileInterests && (
                    <View
                      style={{
                        alignSelf: 'baseline',
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: GREY_DISABLE,
                        borderRadius: WP100,
                      }}
                    >
                      <Text
                        type='NeoSans'
                        size='xtiny'
                        style={{ padding: WP1, paddingHorizontal: WP3 }}
                        color={GREY_DISABLE}
                      >
                        ex: Jazz
                      </Text>
                    </View>
                  )}
                  {formData.profileInterests &&
                    map(formData.profileInterests, (genre, index) => this._genreItem(genre, index, onChange, formData))}
                </View>
              </Card>
              <Card>
                <Text size='xtiny' weight={500}>
                  ABOUT ME
                </Text>
                <InputTextLight
                  withLabel={false}
                  bordered
                  size='tiny'
                  maxLength={300}
                  multiline
                  placeholder='Describe about yourself'
                  value={formData.profileAbout}
                  onChangeText={onChange('profileAbout')}
                />
              </Card>
              <Card>
                <Text size='xtiny' weight={500}>
                  SOCIAL MEDIA
                </Text>
                <InputTextLight
                  withLabel={false}
                  textAlignVertical='center'
                  placeholder='username'
                  size='tiny'
                  iconImage={require('../../assets/icons/social_media/instagram.png')}
                  textIcon='instagram.com/'
                  wording='Instagram'
                  value={formData.profileSocialMedia?.instagram}
                  onChangeText={onChange('profileSocialMedia.instagram')}
                />
                <InputTextLight
                  withLabel={false}
                  textAlignVertical='center'
                  placeholder='paste your Youtube URL link here'
                  size='tiny'
                  iconImage={require('../../assets/icons/social_media/youtube.png')}
                  wording='Youtube'
                  value={formData.profileSocialMedia?.youtube}
                  onChangeText={onChange('profileSocialMedia.youtube')}
                />
                <InputTextLight
                  withLabel={false}
                  textAlignVertical='center'
                  placeholder='username'
                  size='tiny'
                  iconImage={require('../../assets/icons/social_media/facebook.png')}
                  textIcon='facebook.com/'
                  wording='Facebook'
                  value={formData.profileSocialMedia?.facebook}
                  onChangeText={onChange('profileSocialMedia.facebook')}
                />
                <InputTextLight
                  withLabel={false}
                  textAlignVertical='center'
                  placeholder='username'
                  size='tiny'
                  iconImage={require('../../assets/icons/social_media/twitter.png')}
                  textIcon='twitter.com/'
                  wording='Twitter'
                  value={formData.profileSocialMedia?.twitter}
                  onChangeText={onChange('profileSocialMedia.twitter')}
                />
                <InputTextLight
                  withLabel={false}
                  textAlignVertical='center'
                  placeholder='paste your URL link here'
                  size='tiny'
                  iconImage={require('../../assets/icons/social_media/website2.png')}
                  wording='Website'
                  value={formData.profileSocialMedia?.website}
                  onChangeText={onChange('profileSocialMedia.website')}
                />
              </Card>
              <Card>
                <Text size='xtiny' weight={500}>
                  PRIVATE INFORMATION
                </Text>
                <SelectionBar
                  options={[
                    { key: 'male', text: 'Male' },
                    { key: 'female', text: 'Female' },
                  ]}
                  selectedValue={formData.profileGender}
                  onPress={onChange('profileGender')}
                />
                <Checkbox
                  onPress={onChange('profileGenderShow')}
                  disabled={!GENDERS.includes(formData.profileGender)}
                  checked={formData.profileGenderShow}
                  text='Show gender on Profile'
                />
                <InputDate
                  value={formData.profileBirthDate}
                  dateFormat='DD/MM/YYYY'
                  mode='date'
                  wording='Date of Birthday'
                  placeholder='29/01/1990'
                  onChangeText={onChange('profileBirthDate')}
                />
                <Checkbox
                  onPress={onChange('profileBirthDateShow')}
                  checked={formData.profileBirthDateShow}
                  text='Show Birthday on Profile'
                />
                <InputTextLight
                  withLabel={false}
                  editable={false}
                  placeholder='paste your Youtube URL link here'
                  iconImage={require('../../assets/icons/social_media/email2.png')}
                  wording='Email'
                  value={formData.profileEmail}
                  onChangeText={onChange('profileEmail')}
                />
              </Card>
            </View>
          </Container>
        )}
      </Form>
    )
  }
}

ProfileForm.defaultProps = propsDefault
export default _enhancedNavigation(connect(mapStateToProps, mapDispatchToProps)(ProfileForm), mapFromNavigationParam)
