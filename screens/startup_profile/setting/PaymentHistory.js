import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TouchableOpacity, View } from 'react-native'
import moment from 'moment'
import { isEmpty, noop } from 'lodash-es'
import Container from '../../../components/Container'
import HeaderNormal from '../../../components/HeaderNormal'
import { Image, Text } from '../../../components'
import { getPaymentList } from '../../../actions/api'
import _enhancedNavigation from '../../../navigation/_enhancedNavigation'
import { GREEN_30, GUN_METAL, ORANGE20, PALE_BLUE_TWO, SHIP_GREY_CALM } from '../../../constants/Colors'
import { currencyFormatter } from '../../../utils/helper'
import { WP10, WP3, WP5, WP6 } from '../../../constants/Sizes'

const PAYMENT_STATUS_COLOR = {
  'Menunggu Pembayaran': ORANGE20,
  'pending': ORANGE20,
  'Pembayaran Gagal': SHIP_GREY_CALM,
  'Pembayaran Berhasil': GREEN_30
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({})

class PaymentHistory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      paymentList: []
    }
  }

  componentDidMount = async () => {
    const { dispatch, userData } = this.props
    const paymentList = await dispatch(getPaymentList, { id_user: userData.id_user }, noop, true, true)
    if (paymentList.code === 200) {
      this.setState({ paymentList: paymentList.result || [] })
    }
  }

  _backHandler = () => {
    this.props.navigateBack()
  }

  _renderHeader = () => {
    return (
      <HeaderNormal
        iconLeftOnPress={this._backHandler}
        centered
        textType={'Circular'}
        iconLeft={'chevron-left'}
        iconLeftSize={'large'}
        textSize={'slight'}
        iconLeftType={'Entypo'}
        text='Daftar Pembelian'
        shadow
      />
    )
  }

  _renderItem = (payment, index) => {
    const { navigateTo } = this.props
    const url = payment.payment_midtrans
    return (
      <TouchableOpacity
        onPress={() => {
          if (!isEmpty(url)) {
            navigateTo('PaymentWebView', { url })
          }
        }}
        key={index}
        style={{
          flexDirection: 'row', alignItems: 'flex-start', paddingHorizontal: WP3, paddingVertical: WP5,
          borderBottomColor: PALE_BLUE_TWO, borderBottomWidth: 1
        }}
      >
        <Image source={require('../../../assets/icons/payment_list.png')} size='xsmall' imageStyle={{ marginRight: WP3 }} centered={false}/>
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text type='Circular' size='mini' color={SHIP_GREY_CALM}>{`Order ID ${payment.order_id}`}</Text>
            <Text type='Circular' size='mini' color={SHIP_GREY_CALM}>{`${moment(payment.created_at).format('DD/MM/YYYY | HH:mm')}`}</Text>
          </View>
          <Text type='Circular' size='slight' weight={400} color={GUN_METAL}>{payment.item_name}</Text>
          <Text type='Circular' size='slight' weight={400} color={GUN_METAL}>{`Rp ${currencyFormatter(payment.price_after_discount)}`}</Text>
          <Text type='Circular' size='mini' color={PAYMENT_STATUS_COLOR[payment.status]}>{payment.status}</Text>
          {
            !isEmpty(payment.deadline_payment) &&
            <Text type='Circular' size='mini' color={SHIP_GREY_CALM}>
              {`Batas pembayaran: ${moment(payment.deadline_payment).format('DD/MM/YYYY | HH:mm')}`}
            </Text>
          }
        </View>
      </TouchableOpacity>
    )
  }

  _renderEmpty = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', paddingBottom: WP10 }}>
        <Image source={require('../../../assets/images/payment_empty.png')} size='medium' imageStyle={{ marginBottom: WP5 }}/>
        <Text type='Circular' size='small' weight={400} centered color={GUN_METAL}>Tidak ada pembelian</Text>
        <Text type='Circular' size='slight' centered color={SHIP_GREY_CALM}>Kamu belum melakukan pembelian apapun</Text>
      </View>
    )
  }

  render() {
    const { isLoading } = this.props
    const { paymentList } = this.state
    const isEmptyList = isEmpty(paymentList)
    return (
      <Container
        renderHeader={this._renderHeader}
        scrollable={!isEmptyList}
        isLoading={isLoading}
      >
        {
          isEmptyList ?
            this._renderEmpty() :

            <View style={{ paddingBottom: WP6 }}>
              {
                paymentList.map(this._renderItem)
              }
            </View>
        }
      </Container>
    )
  }
}

PaymentHistory.propTypes = {}

PaymentHistory.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(PaymentHistory),
  mapFromNavigationParam
)
