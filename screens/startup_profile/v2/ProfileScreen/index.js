import React, { Component } from "react";
import { connect } from "react-redux";
import { ActivityIndicator, Image, View, BackHandler } from "react-native";
import { noop, isNil, upperFirst, isEmpty, isArray, get } from "lodash-es";
import { LinearGradient } from "expo-linear-gradient";
import { TabBar, TabView } from "react-native-tab-view";
import InteractionManager from "sf-utils/InteractionManager";
import Icon from "sf-components/Icon";
import MenuOptions from "sf-components/MenuOptions";
import {
  getProfileDetailV3,
  getSubscriptionDetail,
  getUserFeed,
  postProfileFollow,
  postProfileUnfollow,
  postUploadCoverImage,
} from "sf-actions/api";
import {
  PALE_BLUE_TWO,
  PALE_WHITE,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from "sf-constants/Colors";
import { WP305, WP5, WP6 } from "sf-constants/Sizes";
import _enhancedNavigation from "sf-navigation/_enhancedNavigation";
import Container from "sf-components/Container";
import HeaderNormal from "sf-components/HeaderNormal";
import Text from "sf-components/Text";
import Touchable from "sf-components/Touchable";
import ButtonV2 from "sf-components/ButtonV2";
import ImageComponent from "sf-components/Image";
import MessageBarClearBlue from "sf-components/messagebar/MessageBarClearBlue";
import { initiateRoom, isIOS } from "sf-utils/helper";
import { paymentDispatcher } from "sf-services/payment";
import { GET_USER_DETAIL } from "sf-services/auth/actionTypes";
import produce from "immer";
import { setCameFromBottomNavigation } from "sf-services/helper/actionDispatcher";
import { emptyClearBlueMessage } from "sf-services/messagebar/actionDispatcher";
import { HEADER } from "../../../../constants/Styles";
import { clearAddProfile, showReviewIfNotYet } from "../../../../utils/review";
import { deleteJourney, getStartupSubmission } from "../../../../actions/api";
import ContactButton from "../../../profile/v2/ProfileScreen/ContactButton";
import SnackBar from "../../../../components/snackbar/SnackBar";
import style from "./style";
import ProfileHeader from "./ProfileHeader";
import PersonalTab from "./TabDetail/PersonalTab";
import TeamTab from "./TabDetail/TeamTab";
import ActivityTab from "./TabDetail/ActivityTab";
import StartUpTab from "./TabDetail/StartUpTab";
import { ModalMessageView } from "../../../../components";
import { WP80 } from "../../../../constants/Sizes";

let snackBar = React.createRef();

const mapStateToProps = ({
  auth,
  review,
  payment: { paymentSource },
  song: { playback, soundObject },
  helper: { cameFromBottomNavigation },
}) => ({
  userData: auth.user,
  addProfile: review.addProfile,
  paymentSource,
  playback,
  soundObject,
  cameFromBottomNavigation,
});

const mapFromNavigationParam = (getParam) => ({
  idUser: getParam("idUser", 0),
  navigateBackOnDone: getParam("navigateBackOnDone", false),
  shouldShowTrialModal: getParam("shouldShowTrialModal", false),
});

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
  setUserDetail: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail,
  }),
  setCameFromBottomNavigation,
  emptyClearBlueMessage,
};

class StartUpProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      containerKey: Math.random(),
      cameFromBottomNavigation: props.cameFromBottomNavigation,
      id_user: props.idUser,
      isMine: props.idUser == 0 || props.userData.id_user == props.idUser,
      profile: null,
      shouldComponentUpdate: false,
      index: 0,
      routes: [
        {
          key: "personal",
          title: "PERSONAL",
        },
        // temporary disabled
        // {
        //   key: 'startup',
        //   title: 'STARTUP',
        // },
        // {
        //   key: 'team',
        //   title: 'TEAM',
        // },
        {
          key: "activity",
          title: "ACTIVITY",
        },
      ],
      song: [],
      showAllSong: false,
      video: [],
      performance_journey: [],
      artwork: [],
      feeds: [],
      schedules: [],
      initialized: false,
      accountType: null,
      updateKey: Math.random(),
      isActivityReady: false,
      following: false,
      uploadingCover: false,
      shouldShowTrial: true,
      shouldShowTrialModal: props.shouldShowTrialModal,
      claimFreeTrial: false,
      loadingChat: false,
      slicedArtwork: [],
      isLoadingSong: false,
      dotsOptions: [
        null,
        {
          onPress: noop,
          iconName: "account-plus",
          iconColor: SHIP_GREY_CALM,
          iconSize: "huge",
          title: "Follow This Artist",
        },
        {
          onPress: () =>
            props.navigateTo("ShareScreen", {
              id_artist: props.idUser,
              id: props.idUser,
              type: "profile",
              title: "Bagikan Profile",
            }),
          iconName: "share-variant",
          iconColor: SHIP_GREY_CALM,
          iconSize: "huge",
          title: "Share",
        },
        {
          onPress: () => props.navigateTo("FeedbackScreen"),
          iconName: "message-text",
          iconColor: SHIP_GREY_CALM,
          iconSize: "huge",
          title: "Feedback & Report",
        },
      ],
      deletededFeeds: [],
      startupData: {},
      modalIsVisible: false,
    };

    // API CALLS
    this._getProfile = this._getProfile.bind(this);
    this._onFollow = this._onFollow.bind(this);
    this._onUploadCover = this._onUploadCover.bind(this);

    // COMPONENT
    this._renderScene = this._renderScene.bind(this);
    this._messageIcon = this._messageIcon.bind(this);

    // NAVIGATIONS
    this._onSubscribe = this._onSubscribe.bind(this);
    this._onEditProfile = this._onEditProfile.bind(this);
    this._onSetting = this._onSetting.bind(this);

    // OTHERS
    this._setIndex = this._setIndex.bind(this);
    this._getIdUser = this._getIdUser.bind(this);
    this._header = this._header.bind(this);
    this._onDeletePost = this._onDeletePost.bind(this);
  }

  _toggleModal = () => {
    this.setState({
      modalIsVisible: !this.state.modalIsVisible,
    });
  };

  shouldComponentUpdate(nextProps, nextState) {
    const shouldUpdate =
      nextState.shouldComponentUpdate || nextState.song != this.state.song;
    return shouldUpdate;
  }

  static getDerivedStateFromProps(state) {
    if (!!state.profile && !state.isMine) {
      const followed = state.profile.followed_by_viewer;
      const dotsOptions = produce(state.dotsOptions, (draft) => {
        draft[1].title = followed
          ? "Unfollow This Artist"
          : "Follow This Artist";
        draft[1].iconName = followed ? "account-minus" : "account-plus";
        draft[1].onPress = followed
          ? state._onFollow(false)
          : state._onFollow(true);
      });
      return { dotsOptions };
    }
    return null;
  }

  _onUploadCover = (cover_image) => {
    this.setState(
      produce((draft) => {
        draft.uploadingCover = true;
      }),
      () => {
        const {
          userData: { id_user },
          dispatch,
        } = this.props;
        dispatch(postUploadCoverImage, {
          id_user,
          cover_image,
        }).then(() =>
          this._getUserProfile().then((profile) =>
            this.setState(
              produce((draft) => {
                draft.profile = profile;
                draft.uploadingCover = false;
              })
            )
          )
        );
      }
    );
  };

  _onFollow = (isFollowAction) => () => {
    const { following } = this.state;
    const { userData: my } = this.props;
    const params = {
      [isFollowAction ? "id_user" : "id_user_following"]: this._getIdUser(),
      [isFollowAction ? "followed_by" : "id_user"]: my.id_user,
    };
    !following &&
      this.setState(
        produce((draft) => {
          draft.following = true;
        }),
        async () => {
          try {
            await (isFollowAction ? postProfileFollow : postProfileUnfollow)(
              params
            );
          } finally {
            // this._refreshSongOnFollow()
          }
        }
      );
  };

  _onEditProfile = () => {
    this.props.navigateTo("StartUpProfileForm", {
      refreshProfile: this._getProfile,
      onSuccessEditProfile: () => {
        this.snackBar._fadeIn();
      },
    });
  };

  _onSubscribe = () => {
    this.props.paymentSourceSet("profile");
    this.props.navigateTo("MembershipScreen");
  };

  _tabShadow = (
    <LinearGradient
      colors={[
        "rgba(0,0,0,.042)",
        "rgba(0,0,0,.028)",
        "rgba(0,0,0,.012)",
        "rgba(0,0,0,0)",
      ]}
      style={style.tabShadow}
    />
  );

  _onDeletePost = (idJourney) => {
    const { dispatch } = this.props;
    let { deletededFeeds } = this.state;
    this.setState({ deletededFeeds: [...deletededFeeds, parseInt(idJourney)] });
    dispatch(deleteJourney, idJourney);
  };

  _renderScene = ({ route }) => {
    const {
      artwork,
      experience,
      index,
      initialized,
      isLoadingSong,
      isMine,
      profile,
      schedules,
      showAllSong,
      slicedArtwork,
      song,
      video,
      accountType,
    } = this.state;
    const {
      navigateTo,
      userData: { id_user: idViewer },
      dispatch,
    } = this.props;
    return {
      personal: () => (
        <View style={index == 0 ? style.shown : style.hidden}>
          {this._tabShadow}
          <PersonalTab
            dispatch={dispatch}
            profile={profile}
            name={profile.full_name}
            onSubscribe={this._onSubscribe}
            navigateTo={navigateTo}
            onEditProfile={this._onEditProfile}
            isMine={isMine}
            initialized={initialized}
            items={profile.role?.portfolio}
            refreshProfile={() => {}}
          />
        </View>
      ),
      // temporary disabled
      // startup: () => (
      //   <View style={this.state.index == 1 ? style.shown : style.hidden}>
      //     {this._tabShadow}
      //     <StartUpTab
      //       profile={profile}
      //       changeIndex={this._setIndex}
      //       startupData={this.state.startupData}
      //       onSubscribe={this._toggleModal}
      //       navigateTo={navigateTo}
      //       isMine={isMine}
      //       items={[]}
      //     />
      //   </View>
      // ),
      // team: () => (
      //   <View style={this.state.index == 2 ? style.shown : style.hidden}>
      //     {this._tabShadow}
      //     <TeamTab profile={profile} startupData={this.state.startupData} />
      //   </View>
      // ),
      activity: () => (
        <View style={this.state.index == 1 ? style.shown : style.hidden}>
          {this._tabShadow}
          <ActivityTab
            dispatch={dispatch}
            profile={profile}
            name={profile.full_name}
            onSubscribe={this._onSubscribe}
            navigateTo={navigateTo}
            isMine={isMine}
            initialized={initialized}
            items={profile.role.portofolio}
            refreshProfile={() => {}}
          />
        </View>
      ),
    }[route.key]();
  };

  _setIndex = (index) => {
    this.setState(
      produce((draft) => {
        draft.index = index;
      })
    );
  };

  _getIdUser = () => {
    let id_user = this.state.isMine
      ? this.props.userData.id_user
      : this.state.id_user;
    return id_user;
  };

  _getUserProfile = () => {
    const { id_user, isMine } = this.state;
    const { dispatch, userData } = this.props;
    const profileParams = isMine
      ? { id_user: userData.id_user }
      : { id_user, id_viewer: userData.id_user };
    return new Promise((resolve) => {
      dispatch(getProfileDetailV3, profileParams).then((result) => {
        resolve(result);
      });
    });
  };

  _sendMessage = () => {
    this.setState(
      produce((draft) => {
        draft.loadingChat = true;
      }),
      () => {
        initiateRoom(this.props.userData, this.state.profile).then(
          (chatRoomDetail) => {
            this.setState(
              produce((draft) => {
                draft.loadingChat = false;
              }),
              () => {
                this.props.navigateTo("ChatRoomScreen", chatRoomDetail);
              }
            );
          }
        );
      }
    );
  };

  _getProfile = (refresh = false) => {
    const { userData: my } = this.props;
    this.setState(
      produce((draft) => {
        draft.shouldComponentUpdate = false;
        if (!isIOS())
          draft.dotsOptions[0] = {
            onPress:
              get(my, "account_type") === "user" ? this._onSubscribe : noop,
            image: require("sf-assets/icons/badge.png"),
            imageSize: WP6,
            titleColor: REDDISH,
            titleWeight: 500,
            title:
              get(my, "account_type") === "user"
                ? "Upgrade Premium"
                : "Premium User",
          };
      }),
      () => {
        Promise.all([
          this._getUserProfile(), // PROFILE
          this.props.dispatch(getStartupSubmission, {
            id_user: this._getIdUser(),
          }), // SUBSCRIPTION
        ])
          .catch(() => {
            this._getProfile();
          })
          .then((results) => {
            this.setState(
              produce((draft) => {
                draft.profile = results[0];
                draft.startupData = results[1];
                draft.initialized = true;
                // draft.accountType = results[6].package_type
                //   ? results[6].package_type.toLowerCase()
                //   : 'free'
              }),
              () =>
                this.setState(
                  produce((draft) => {
                    draft.shouldComponentUpdate = true;
                  })
                )
            );
          });
      }
    );
  };

  _backHandler = () => {
    const { cameFromBottomNavigation } = this.state;
    const { navigateTo, navigateBack } = this.props;

    cameFromBottomNavigation && navigateTo("ExploreStack");
    !cameFromBottomNavigation && navigateBack();
    return true;
  };

  _attachBackAction() {
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this._backHandler
    );
  }

  _releaseBackAction() {
    try {
      this.backHandler.remove();
    } catch (err) {
      /*silent is gold*/
    }
  }

  // eslint-disable-next-line react/sort-comp
  componentDidMount() {
    const { navigateBack } = this.props;
    this.setState(
      produce((draft) => {
        draft._onFollow = this._onFollow;
      })
    );
    this.props.setCameFromBottomNavigation(false);
    InteractionManager.runAfterInteractions(this._getProfile);
    const { navigation } = this.props;
    this._willBlurSubscription = navigation.addListener("blur", () => {
      this._releaseBackAction();
      const { playback, soundObject } = this.props;
      playback.isLoaded && soundObject.pauseAsync();
    });
    this._didFocusSubscription = navigation.addListener("focus", () => {
      const {
        userData: { id_user: userId },
      } = this.props;
      if (!userId) {
        navigateBack();
        this.props.navigation.navigate("AuthNavigator", {
          // screen: 'LoginScreen',
          screen: "LoginEmailScreen",
          params: {
            forceReload: true,
          },
        });
      } else {
        if (!isNil(this.state.profile)) {
          this._getUserProfile().then((profile) =>
            this.setState(
              produce((draft) => {
                draft.profile = profile;
              })
            )
          );
        }
        this._attachBackAction();
        setTimeout(this.props.emptyClearBlueMessage, 4000);
      }
    });
  }

  // eslint-disable-next-line no-dupe-class-members
  componentWillUnmount() {
    try {
      this._willBlurSubscription();
      this._didFocusSubscription();
    } catch (err) {
      // keep silent
    }
  }

  _closeTrial = () =>
    this.setState(
      produce((draft) => {
        draft.shouldShowTrial = false;
      })
    );

  _onSetting = () => this.props.navigateTo("StartupProfileSettingScreen");

  _hamburgerMenu = (
    <View style={style.hamburgerMenu}>
      <Touchable onPress={this._onSetting}>
        <View style={HEADER.rightIcon}>
          <Image
            style={style.hamburgerMenuImage}
            source={require("sf-assets/icons/moreOption.png")}
          />
        </View>
      </Touchable>
    </View>
  );

  _renderModalToggler = (toggleModal) => (
    <Touchable onPress={toggleModal} style={HEADER.rightIcon}>
      <Icon
        centered
        size="small"
        color={SHIP_GREY_CALM}
        name="dots-three-horizontal"
        type="Entypo"
      />
    </Touchable>
  );

  _header = () => {
    const { isMine, profile } = this.state;
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftOnPress={this._backHandler}
          centered
          textType={"Circular"}
          textSize={"slight"}
          text={upperFirst(
            isMine ? "Profile" : !isNil(profile) ? profile.full_name : ""
          )}
          rightComponent={
            isMine ? (
              this._hamburgerMenu
            ) : (
              <MenuOptions
                options={this.state.dotsOptions}
                triggerComponent={this._renderModalToggler}
              />
            )
          }
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
        <View>
          <MessageBarClearBlue />
        </View>
      </View>
    );
  };

  _messageIcon = () => {
    return this.state.loadingChat ? (
      <ActivityIndicator color={REDDISH} />
    ) : (
      <ImageComponent
        style={style.sendIcon}
        size={WP305}
        aspectRatio={1 / 1}
        source={require("sf-assets/icons/send.png")}
      />
    );
  };

  _refreshProfile = () => {
    this._getProfile(true);
  };

  _renderTabBar = (props) => (
    <TabBar
      {...props}
      // scrollEnabled
      renderLabel={({ route, color }) => (
        <View style={style.tabBar}>
          <View
            style={
              color == REDDISH ? style.tabBarInnerActive : style.tabBarInner
            }
          >
            <View style={style.tabBarInnerWrapper}>
              <View style={{ height: WP5, width: 1 }} />
              {/* {route.title === 'SCHEDULE' && (
                <Image style={style.tabBarInnerWrapperImage} source={sfLogoCircle} />
              )} */}
              <Text weight={600} size={"xmini"} type={"Circular"} color={color}>
                {route.title}
              </Text>
              <View style={{ height: WP5, width: 1 }} />
            </View>
          </View>
        </View>
      )}
      style={style.tabBarStyle}
      contentContainerStyle={style.tabBarContentContainerStyle}
      indicatorStyle={style.tabBarIndicatorStyle}
      indicatorContainerStyle={style.tabBarIndicatorContainerStyle}
      tabStyle={style.tabBarTabStyle}
      activeColor={REDDISH}
      inactiveColor={SHIP_GREY_CALM}
    />
  );

  render() {
    const {
      accountType,
      following,
      index,
      isMine,
      profile,
      routes,
      containerKey,
    } = this.state;
    return (
      <Container
        key={containerKey}
        onPullDownToRefresh={this._refreshProfile}
        renderHeader={this._header}
        isLoading={isNil(profile)}
        isReady={!isNil(profile)}
        scrollable
        hasBottomNavbar
        scrollBackgroundColor={PALE_WHITE}
      >
        <SnackBar
          ref={(ref) => {
            this.snackBar = ref;
          }}
          label="Sukses Edit Profile"
          iconName="check-bold"
        />
        {!isNil(profile) && (
          <View>
            <ProfileHeader
              uploadingCover={this.state.uploadingCover}
              uploadCover={this._onUploadCover}
              isMine={this.state.isMine}
              accountType={accountType}
              profile={profile}
              navigateTo={this.props.navigateTo}
              getProfile={this._getProfile}
              editProfile={this._onEditProfile}
            />
            {isMine && (
              <View style={style.profileActions}>
                <ButtonV2
                  icon={"pencil"}
                  disabled={false}
                  textColor={SHIP_GREY}
                  borderColor={PALE_BLUE_TWO}
                  style={style.profileActionsEdit}
                  onPress={this._onEditProfile}
                  text={"Edit Profile"}
                />
              </View>
            )}
            {!isMine && (
              <View style={style.profileActions}>
                {!profile.followed_by_viewer && (
                  <ButtonV2
                    onPress={this._onFollow(true)}
                    icon={!following ? "account-plus" : null}
                    style={style.profileActionsEdit}
                    forceBorderColor={following}
                    borderColor={REDDISH}
                    textColor={following ? REDDISH : WHITE}
                    iconColor={following ? REDDISH : WHITE}
                    color={following ? "rgba(255, 101, 31, 0.1)" : REDDISH}
                    disabled={following}
                    text={following ? "Following" : "Follow"}
                  />
                )}
                {profile.followed_by_viewer && (
                  <ButtonV2
                    onPress={this._onFollow(false)}
                    textColor={following ? WHITE : REDDISH}
                    borderColor={
                      following ? "rgba(255, 101, 31, 0.1)" : REDDISH
                    }
                    forceBorderColor
                    style={style.profileActionsEdit}
                    color={following ? REDDISH : "rgba(255, 101, 31, 0.1)"}
                    disabled={following}
                    text={following ? "Unfollowing" : "Following"}
                  />
                )}
                <View style={style.profileActionsSeparator} />
                <ButtonV2
                  leftComponent={this._messageIcon}
                  onPress={this._sendMessage}
                  style={style.profileActionsEdit}
                  textColor={this.state.loadingChat ? REDDISH : SHIP_GREY}
                  text={this.state.loadingChat ? null : "Message"}
                />
                <View style={style.profileActionsSeparator} />
                <ContactButton
                  navigateTo={this.props.navigateTo}
                  profile={profile}
                />
              </View>
            )}
            <TabView
              // scrollEnabled
              renderTabBar={this._renderTabBar}
              swipeEnabled={false}
              navigationState={{ index, routes }}
              renderScene={this._renderScene}
              onIndexChange={this._setIndex}
            />
          </View>
        )}
        <ModalMessageView
          aspectRatio={838 / 676}
          fullImage
          toggleModal={this._toggleModal}
          imageStyle={{ width: WP80, marginTop: 33 }}
          isVisible={this.state.modalIsVisible}
          image={require("sf-assets/images/bgUnderDevelopment.png")}
          buttonPrimaryAction={this._toggleModal}
          titleSize={"massive"}
          buttonPrimaryTextWeight={500}
          buttonPrimaryTextSize={"small"}
          buttonPrimaryBgColor={REDDISH}
          buttonPrimaryText={"OK"}
          title={"Coming Soon"}
          subtitle={
            "This page is still under maintenance. We’ll inform you when the page is ready."
          }
        />
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(StartUpProfileScreen),
  mapFromNavigationParam
);
