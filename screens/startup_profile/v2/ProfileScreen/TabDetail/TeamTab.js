import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { FlatList, Image, View } from 'react-native'
import { isEmpty, upperFirst } from 'lodash-es'
import { isIOS } from 'sf-utils/helper'
import Text from 'sf-components/Text'
import ButtonV2 from 'sf-components/ButtonV2'
import { GUN_METAL, NAVY_DARK, REDDISH, SHIP_GREY_CALM, WHITE, } from 'sf-constants/Colors'
import { Avatar } from '../../../../../components'
import { GREY_CALM_SEMI } from '../../../../../constants/Colors'
import style from './style'

class TeamTab extends PureComponent {
  static propTypes = {
    profile: PropTypes.object,
  };

  constructor(props) {
    super(props)
  }

  _renderItem = ({ item, index }) => {
    return (
      <View style={style.contentWithPadding}>
          <View style={style.teamItem}>
            <Avatar
              noPlaceholder
              shadow={false}
              size='regular'
              imageStyle={[{ borderWidth: 1, borderColor: GREY_CALM_SEMI }]}
              image={item.profile_picture}
            />
            <View style={style.teamInfo}>
              <Text
                size={'small'}
                color={GUN_METAL}
                type={'Circular'}
                weight={500}
              >
                {item.full_name}
              </Text>
              <Text
                size={'mini'}
                color={SHIP_GREY_CALM}
                type={'Circular'}
                weight={400}
              >
                {item.position}
              </Text>
            </View>
          </View>
        </View>
    )
  };

  _keyExtractor = (item) => item.id_schedule;

  _onAdd = () => {
    this.props.navigateTo('StartUpProfileForm', { refreshProfile: this.props.refreshProfile })
  }

  render() {
    const { profile, startupData } = this.props
    let teamMember = Object.assign([], startupData.team)
    if (startupData.ceo.id_user !== '') {
      teamMember.unshift({
        ...startupData.ceo,
        position: 'CEO'
      })
    }
    return (
      <View>
        <View style={style.headingWithPadding}>
          <Text
            size={'medium'}
            color={NAVY_DARK}
            type={'Circular'}
            weight={600}
          >
            Semua
          </Text>
        </View>
        {startupData.team.length == 0
        ? <View style={style.contentWithPadding}>
        <View style={style.teamItem}>
          <Avatar
            noPlaceholder
            shadow={false}
            size='regular'
            imageStyle={[{ borderWidth: 1, borderColor: GREY_CALM_SEMI }]}
            image={profile.profile_picture}
          />
          <View style={style.teamInfo}>
            <Text
              size={'small'}
              color={GUN_METAL}
              type={'Circular'}
              weight={500}
            >
              {profile.full_name}
            </Text>
            <Text
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={400}
            >
              {'Belum memiliki posisi'}
            </Text>
          </View>
        </View>
      </View>
        : <FlatList
          data={teamMember}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={null}
          />}
      </View>
    )
  }
}

export default TeamTab
