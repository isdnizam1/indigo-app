import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { FlatList, Image, View, TouchableOpacity } from 'react-native'
import {
  toLower,
  includes,
  omit,
  isNil,
  isEmpty,
  isString,
  isObject,
  get,
  upperFirst,
} from 'lodash-es'
import { isIOS } from 'sf-utils/helper'
import Text from 'sf-components/Text'
import ButtonV2 from 'sf-components/ButtonV2'
import { GUN_METAL, NAVY_DARK, REDDISH, SHIP_GREY_CALM, WHITE, } from 'sf-constants/Colors'
import { Card } from '../../../../../components'
import { WP2, WP5, WP20, WP15, WP12, WP60, WP8, WP50 } from '../../../../../constants/Sizes'
import { SHADOW_STYLE } from '../../../../../constants/Styles'
import { NavigateToInternalBrowser, NavigateToInternalBrowserNoTab } from '../../../../../utils/helper'
import style from './style'

class PersonalTab extends PureComponent {
  static propTypes = {
    items: PropTypes.array,
    profile: PropTypes.object,
    isMine: PropTypes.bool,
    initialized: PropTypes.bool,
    navigateTo: PropTypes.func,
    onSubscribe: PropTypes.func,
    showTrialModal: PropTypes.func,
    name: PropTypes.string,
    accountType: PropTypes.string,
  };

  constructor(props) {
    super(props)
  }

  _renderItem = ({ item, index }) => {
    const { onEditProfile } = this.props
    let portofolioIcon = require('sf-assets/icons/linkIcon.png')
    if (toLower(item).includes('github')) {
      portofolioIcon = require('sf-assets/images/github.png')
    } else if (toLower(item).includes('behance')) {
      portofolioIcon = require('sf-assets/images/behance.png')
    } else if (toLower(item).includes('dribbble')) {
      portofolioIcon = require('sf-assets/images/dribble.png')
    }
    return (
      <TouchableOpacity
        onPress={() => NavigateToInternalBrowserNoTab({ url: item })} style={{
        marginBottom: WP5,
        backgroundColor: 'white',
        ...SHADOW_STYLE.shadowSoft
        }}
      >
        <Card>
          <View style={{ elevation: 3, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <View style={{ marginRight: WP5 }}>
              <Image source={portofolioIcon} style={{ width: WP12, height: WP12 }} />
            </View>
            <View>
              <Text
                size={'small'}
                color={GUN_METAL}
                type={'Circular'}
                weight={500}
                ellipsizeMode={'tail'}
                numberOfLines={1}
                style={{ maxWidth: WP50 }}
              >
                {item}
              </Text>
              <TouchableOpacity onPress={onEditProfile}>
              <Text
                size={'mini'}
                color={REDDISH}
                type={'Circular'}
                weight={400}
              >
                Edit Attachment
              </Text>
              </TouchableOpacity>
            </View>
            <View style={{ marginLeft: WP5 }}>
              <Image source={require('sf-assets/icons/icNext.png')} style={{ width: WP8, height: WP8 }} />
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    )
  };

  _keyExtractor = (item) => item.id_schedule;

  _onAdd = () => {
    this.props.navigateTo('StartUpProfileForm', { refreshProfile: this.props.refreshProfile })
  }

  render() {
    const { items, initialized, isMine, profile } = this.props
    return (
      <View>
        <View style={style.headingWithPadding}>
          <Text
            size={'medium'}
            color={NAVY_DARK}
            type={'Circular'}
            weight={600}
          >
            Attachment
          </Text>
          {/* {isMine && !isEmpty(items) && profile.account_type === 'premium' && (<Text
            onPress={this._onAdd}
            size={'mini'}
            color={REDDISH}
            type={'Circular'}
            weight={300}
                                                                               >
            Tambahkan Schedule +
          </Text>)} */}
        </View>
        {isEmpty(items) && initialized && (
          isMine ? <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require('sf-assets/icons/portofolio.png')}
            />
            <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
              Lengkapi Attachment Kamu
            </Text>
            <Text
              centered
              style={style.emptyStateDescription}
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={300}
            >
              {'Tunjukkan pencapaian kamu'}
            </Text>

            <ButtonV2
              onPress={this._onAdd}
              style={style.emptyStateCta}
              color={REDDISH}
              textColor={WHITE}
              textSize={'mini'}
              text={'Lengkapi Attachment'}
            />

          </View> : <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require('sf-assets/icons/v3/scheduleCircle.png')}
            />
            <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
              Belum ada jadwal tampil
            </Text>
            <Text
              centered
              style={style.emptyStateDescription}
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={300}
            >
              {upperFirst(this.props.name.split(' ')[0])} saat ini belum memiliki jadwal bermain.
            </Text>
          </View>
        )}
        <FlatList
          data={items}
          contentContainerStyle={{ paddingHorizontal: WP5 }}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={null}
        />
      </View>
    )
  }
}

export default PersonalTab
