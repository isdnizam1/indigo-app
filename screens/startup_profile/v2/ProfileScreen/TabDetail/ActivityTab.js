import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { FlatList, Image, View } from 'react-native'
import { isEmpty, upperFirst } from 'lodash-es'
import { isIOS } from 'sf-utils/helper'
import Text from 'sf-components/Text'
import ButtonV2 from 'sf-components/ButtonV2'
import { GUN_METAL, NAVY_DARK, REDDISH, SHIP_GREY_CALM, WHITE, } from 'sf-constants/Colors'
import style from './style'

class ActivityTab extends PureComponent {
  static propTypes = {
    items: PropTypes.array,
    profile: PropTypes.object,
    isMine: PropTypes.bool,
    initialized: PropTypes.bool,
    navigateTo: PropTypes.func,
    onSubscribe: PropTypes.func,
    showTrialModal: PropTypes.func,
    name: PropTypes.string,
    accountType: PropTypes.string,
  };

  constructor(props) {
    super(props)
  }

  _renderItem = ({ item, index }) => {
    return (
      <View />
    )
  };

  _keyExtractor = (item) => item.id_schedule;

  _onAdd = () => {
    this.props.navigateTo('StartupMissionHomeScreen')
  }

  render() {
    const { items, initialized, isMine, profile } = this.props
    return (
      <View>
        <View style={style.headingWithPadding}>
          <Text
            size={'medium'}
            color={NAVY_DARK}
            type={'Circular'}
            weight={600}
          >
            Semua
          </Text>
        </View>
        {isEmpty(items) && initialized && (
          <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require('sf-assets/icons/activityIcon.png')}
            />
            <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
              Belum ada aktivitas
            </Text>
            <Text
              centered
              style={style.emptyStateDescription}
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={300}
            >
              {'Silahkan mulai aktivitas kamu '}
            </Text>

            <ButtonV2
              onPress={this._onAdd}
              style={style.emptyStateCta}
              color={REDDISH}
              textColor={WHITE}
              textSize={'mini'}
              text={'Menuju Mission'}
            />

          </View>
        )}
        <FlatList
          data={items}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={null}
        />
      </View>
    )
  }
}

export default ActivityTab
