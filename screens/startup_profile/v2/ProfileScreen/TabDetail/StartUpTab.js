import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { FlatList, Image, View, TouchableOpacity } from 'react-native'
import { isEmpty, upperFirst } from 'lodash-es'
import { isIOS } from 'sf-utils/helper'
import Text from 'sf-components/Text'
import ButtonV2 from 'sf-components/ButtonV2'
import {
  GUN_METAL,
  NAVY_DARK,
  REDDISH,
  SHIP_GREY_CALM,
  WHITE,
} from 'sf-constants/Colors'
import { ImageBackground } from 'react-native'
import {
  WP100,
  WP05,
  WP5,
  HP05,
  WP7,
  WP8,
  WP10,
  HP10,
  HP5,
  HP2,
  HP1,
  HP3,
} from '../../../../../constants/Sizes'
import { Card, Avatar } from '../../../../../components'
import { PALE_GREY, SHIP_GREY, GREY_CALM_SEMI, PALE_BLUE_TWO, TRANSPARENT, REDDISH_DISABLED } from '../../../../../constants/Colors'
import Label from '../../../../../components/Label'
import style from './style'
import { propsType } from '../../../../../components/InputTextLight'

class StartUpTab extends PureComponent {
  static propTypes = {
    items: PropTypes.array,
    profile: PropTypes.object,
    startupData: PropTypes.object,
    isMine: PropTypes.bool,
    navigateTo: PropTypes.func,
    // onSubscribe: PropsType.func
  };

  constructor(props) {
    super(props)
  }

  renderInformation = () => {
    const { startupData } = this.props
    return (
      <View style={{ padding: WP5 }}>
        <View style={{ marginBottom: HP1 }}>
          <Text size={'medium'} color={NAVY_DARK} type={'Circular'} weight={600}>
            {'Informasi Startup'}
          </Text>
        </View>
        <View
          style={{
            borderBottomWidth: 2,
            borderBottomColor: PALE_GREY,
            paddingBottom: HP3,
          }}
        >
          <Label title={'Nama Startup:'} />
          <View>
            <Text
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={200}
              style={{ paddingVertical: HP1 }}
            >
              {startupData.name || 'Belum ada nama startup'}
            </Text>
          </View>
          <Label title={'Inovasi:'} />
          <View
            style={{
              marginVertical: HP1,
              paddingHorizontal: HP1,
              paddingVertical: HP2,
              backgroundColor: PALE_GREY,
              borderRadius: 6,
            }}
          >
            <Text
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={200}
            >
              {startupData.inovation_title || 'Belum ada judul'}
            </Text>
          </View>
          <Label title={'Category:'} />
          <View
            style={{
              marginVertical: HP1,
              paddingHorizontal: HP1,
              paddingVertical: HP2,
              backgroundColor: PALE_GREY,
              borderRadius: 6,
            }}
          >
            <Text
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={200}
            >
              {startupData.category || '-'}
            </Text>
          </View>
        </View>
        <View
          style={{
            borderBottomWidth: 2,
            borderBottomColor: PALE_GREY,
            paddingVertical: HP3,
          }}
        >
          <Label title={'Tentang Startup ini'} />
          <View>
            <Text
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={200}
              style={{ paddingVertical: HP1 }}
            >
              {startupData.inovation_description || 'Belum ada deskripsi'}
            </Text>
          </View>
        </View>

        {/* TEMPORARY DISABLED */}

        {/* <View style={{ borderBottomWidth: 2, borderBottomColor: PALE_GREY, paddingVertical: HP3 }}>
          <Label title={'Tanggal Registrasi'} />
          <View style={{marginBottom: HP2}}>
            <Text
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={200}
              style={{ paddingVertical: HP1 }}
            >
              Startup belum terdaftar
            </Text>
          </View>
          <Label title={'Status Kepesertaan:'} />
          <View>
            <Text
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={200}
              style={{ paddingVertical: HP1 }}
            >
              Startup belum terdaftar
            </Text>
          </View>
        </View> */}
      </View>
    )
  };

  renderTeam = () => {
    const { startupData } = this.props
    let teamMember = Object.assign([], startupData.team)
    if (startupData.ceo.id_user !== '') {
      teamMember.unshift({
        ...startupData.ceo,
        position: 'CEO'
      })
    }
    return (
      <View style={{ paddingHorizontal: WP5 }}>
        <View
          style={{
            marginTop: HP2,
            marginBottom: HP1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <View>
            <Text size={'medium'} color={NAVY_DARK} type={'Circular'} weight={600}>
              Komposisi Tim
            </Text>
            <Text
              size={'small'}
              color={SHIP_GREY}
              type={'Circular'}
              // weight={400}
            >
              Tim startup kamu
            </Text>
          </View>
          <TouchableOpacity onPress={() => this.props.changeIndex(2)}>
            <Text
              size={'mini'}
              color={REDDISH}
              type={'Circular'}
              // weight={400}
            >
              Lihat Semua
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <FlatList
            contentContainerStyle={{ paddingVertical: HP3 }}
            data={teamMember}
            horizontal
            renderItem={this.renderTeamItem}
          />
        </View>
      </View>
    )
  };

  renderTeamItem = (data) => {
    const { item, index } = data
    if (index < 2) {
      return (
        <View style={{ justifyContent: 'flex-start', alignItems: 'center' }}>
          <Avatar
            size={'ultraMassive'}
            shadow
            image={item.profile_picture}
            imageStyle={[{ borderWidth: 1, borderColor: GREY_CALM_SEMI }]}
            style={{
              marginRight: 12,
              overflow: 'hidden',
            }}
          />
          <Text size={'small'} color={GUN_METAL} type={'Circular'} weight={700}>
            {item.full_name}
          </Text>
          <Text size={'mini'} color={SHIP_GREY_CALM} type={'Circular'} weight={200}>
            {item.position}
          </Text>
        </View>
      )
    } else {
      return <View />
    }
  };

  _keyExtractor = (item) => item.id_schedule;

  _onAdd = () => {
    // if (this.props.startupData.id_submission) {
    //   this.props.navigateTo('StartupSubmissionForm', { idAds: 7 })
    // } else {
    //   this.props.navigateTo('SubmissionPreview', { id_ads: 7 })
    // }
    this.props.onSubscribe()
  };

  render() {
    const { startupData } = this.props
    return (
      <View>
        <ImageBackground
          source={
            startupData.name
              ? require('sf-assets/images/bgStartup.png')
              : require('sf-assets/images/startUpHeaderPlaceholder.png')
          }
          style={{ width: WP100 }}
        >
          <View
            style={{
              width: WP100,
              height: 202,
              alignItems: 'flex-start',
              justifyContent: 'center',
              padding: WP5,
            }}
          >
            <Text size={'semiJumbo'} color={WHITE} type={'Circular'} weight={700}>
              {startupData.name || 'Your Startup'}
            </Text>
            <Text
              size={'mini'}
              color={'#BDBDBD'}
              type={'Circular'}
              weight={400}
              style={{ marginTop: HP05 }}
            >
              {startupData.domisili.city_name || 'Your City'}
            </Text>
          </View>
        </ImageBackground>
        <View
          style={{
            paddingVertical: HP2,
            justifyContent: 'center',
            alignItems: 'center',
            borderBottomWidth: 2,
            borderBottomColor: PALE_GREY,
          }}
        >
          {/* comingsoon */}
          {/* <ButtonV2
            onPress={this._onAdd}
            icon={startupData.id_submission ? 'pencil' : false}
            style={{
              ...style.emptyStateCta,
              backgroundColor: startupData.id_submission ? PALE_GREY : REDDISH
            }}
            textColor={startupData.id_submission ? SHIP_GREY : WHITE}
            borderColor={startupData.id_submission ? PALE_BLUE_TWO : TRANSPARENT}
            textSize={'mini'}
            text={startupData.id_submission ? 'Edit Startup' : 'Daftarkan Startup'}
          /> */}
          
          <ButtonV2
            style={{
              ...style.emptyStateCta,
              backgroundColor: REDDISH_DISABLED,
            }}
            textColor={WHITE}
            borderColor={TRANSPARENT}
            textSize={'mini'}
            // text={startupData.id_submission ? 'Edit Startup' : 'Daftarkan Startup'}
            text={'Daftarkan Startup (Coming Soon)'}
            disabled={true}
          />
        </View>
        {this.renderInformation()}
        {this.renderTeam()}
      </View>
    )
  }
}

export default StartUpTab
