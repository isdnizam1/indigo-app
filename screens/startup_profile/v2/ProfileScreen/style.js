import { StyleSheet } from 'react-native'
import Constants from 'expo-constants'
import {
  WP1,
  WP105,
  WP2,
  WP3,
  WP305,
  WP4,
  WP5,
  WP6,
  WP7,
  WP10,
  WP15,
  WP70,
  WP100,
} from 'sf-constants/Sizes'
import {
  PALE_WHITE,
  WHITE,
  PALE_BLUE,
  REDDISH,
  TRANSPARENT,
  PALE_GREY
} from 'sf-constants/Colors'
import { HP08, HP10, HP05, HP5, HP6, HP8, HP1, HP2 } from '../../../../constants/Sizes'

export default StyleSheet.create({
  header: {
    paddingBottom: WP2,
    paddingTop: WP2 + Constants.statusBarHeight,
    elevation: 7,
    backgroundColor: WHITE,
  },
  profileHeader: {
    padding: WP5,
    // backgroundColor: PALE_WHITE,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
    // flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileHeaderAvatar: {
    marginTop: HP6,
    marginBottom: HP1
  },
  profileHeaderInfo: {
    // paddingLeft: WP4,
    flex: 1
  },
  profileHeaderInfoJob: {
    marginTop: WP1,
    marginBottom: WP2,
  },
  profileHeaderInfoFollows: {
    marginBottom: HP1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  profileActions: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: WP5,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
    backgroundColor: WHITE,
  },
  profileActionsSeparator: {
    width: WP2
  },
  profileActionsPremium: {
    flex: 1,
    height: WP10,
    marginRight: WP4,
  },
  profileActionsEdit: {
    flex: 1,
    paddingVertical: 0,
    height: WP10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F4F6F8'
  },
  profileActionsPremiumLogo: {
    width: WP7,
    height: WP7,
  },
  tabBar: { paddingHorizontal: WP3, marginTop: 0 },
  tabBarInner: {
    marginTop: 1.2,
    borderBottomWidth: WP1 - 1,
    borderBottomColor: TRANSPARENT,
    paddingHorizontal: WP1,
    minHeight: WP5
  },
  tabBarInnerActive: {
    marginTop: 1.2,
    borderBottomWidth: WP1 - 1,
    borderBottomColor: REDDISH,
    paddingHorizontal: WP1,
    paddingBottom: WP305,
    minHeight: WP5
  },
  tabBarInnerWrapper: { flexDirection: 'row', alignItems: 'center' },
  tabBarInnerWrapperImage: { width: WP5, height: WP5, marginRight: WP105 },
  tabContent: {
    paddingHorizontal: WP5,
    paddingVertical: WP7,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  tabContentNoPadding: {
    paddingVertical: WP7,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  tabContentNoPaddingNoBorder: {
    paddingVertical: WP7
  },
  tabBarStyle: {
    backgroundColor: WHITE,
    paddingBottom: 0,
    marginBottom: 0,
    elevation: 0,
  },
  tabBarTabStyle: {
    width: 'auto',
    paddingHorizontal: 0,
    height: 'auto',
    marginTop: 0,
    marginBottom: 0,
    paddingVertical: 0,
    elevation: 0
  },
  tabBarContentContainerStyle: { paddingBottom: 0, marginBottom: 0 },
  tabBarIndicatorStyle: { height: 0 },
  tabBarIndicatorContainerStyle: { height: 0 },
  tabShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    zIndex: 999
  },
  tabShadowWrapper: {
    width: WP100,
    height: WP3,
    backgroundColor: WHITE
  },
  contact: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: WP5,
    borderTopWidth: 1,
    borderTopColor: PALE_GREY,
    paddingVertical: WP5
  },
  contactImage: {
    width: WP7,
    height: WP7,
    marginRight: WP3
  },
  artistHeader: {
  },
  artistHeaderCover: {
    paddingBottom: WP15
  },
  artistHeaderCoverGradient: {
    width: WP100,
    height: WP100 * (140/360) * 0.825,
    position: 'absolute',
    bottom: WP15,
    left: 0
  },
  artistHeaderCoverImage: {
    width: WP100,
    height: WP100 * (140/360)
  },
  artistHeaderCoverAvatar: {
    position: 'absolute',
    bottom: 0,
    left: WP70 / 2 - 1.75,
    backgroundColor: WHITE,
    padding: 3.5,
    borderRadius: WP15 + 3.5
  },
  artistHeaderName: {
    marginTop: WP3,
    marginBottom: WP5
  },
  artistHeaderFollows: {
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
    paddingBottom: WP4
  },
  artistHeaderFollowsItem: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  artistHeaderDescription: {
    paddingHorizontal: WP5,
    paddingTop: WP4,
    paddingBottom: WP4,
  },
  artistHeaderDescriptionCompany: {
    marginBottom: WP2
  },
  artistHeaderDescriptionAbout: {
    marginBottom: WP2,
    lineHeight: WP5 + 2,
    textAlign: 'center'
  },
  artistHeaderCamera: {
    position: 'absolute',
    right: 0,
    bottom: WP15,
    paddingVertical: WP5,
    paddingHorizontal: WP5
  },
  artistHeaderCameraIcon: {
    width: WP10 + 2,
    height: WP10 + 2,
  },
  hamburgerMenuImage: {
    width: WP5,
    height: WP5
  },
  profileHeaderFreeTrial: {
    width: WP100,
    height: WP100 * (69 / 360),
    justifyContent: 'center',
    paddingHorizontal: WP5
  },
  profileHeaderFreeTrialBackground: {
    width: WP100,
    height: WP100 * (69 / 360),
    position: 'absolute',
    top: 0,
    left: 0
  },
  profileHeaderFreeTrialLink: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: WP1
  },
  profileHeaderFreeTrialLinkArrow: {
    width: WP6,
    height: WP6,
    marginLeft: WP1,
    marginBottom: -2
  },
  profileHeaderFreeTrialClose: {
    position: 'absolute',
    padding: WP305,
    marginRight: WP1,
    top: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  profileHeaderFreeTrialCloseIcon: {
    width: WP6,
    height: WP6
  },
  shown: {
    display: 'flex'
  },
  hidden: {
    display: 'none'
  },
  headerShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
  sendIcon: { marginRight: WP1 + 1.25 },
  emptyHeading: {
    paddingHorizontal: WP5,
    paddingVertical: WP3
  },
  emptyState: {
    alignItems: 'center',
    paddingTop: WP7,
    paddingBottom: WP7,
  },
  emptyStateIcon: {
    width: WP15,
    height: WP15,
    marginBottom: WP5
  },
  emptyStateDescription: {
    lineHeight: WP5,
    marginTop: WP2
  },
  profileAbout: {
    // backgroundColor: WHITE,
    paddingHorizontal: WP5,
    paddingTop: WP4
  },
  readMore: {
    paddingTop: WP1,
    paddingBottom: WP4
  }
})
