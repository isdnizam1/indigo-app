import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { get } from 'lodash-es'
import { StyleSheet, TouchableOpacity, View, Image } from 'react-native'
import Avatar from 'sf-components/Avatar'
import Text from 'sf-components/Text'
import { selectPhoto, takePhoto } from 'sf-utils/upload'
import { GUN_METAL, NAVY_DARK, REDDISH, SHIP_GREY, SHIP_GREY_CALM, WHITE, } from 'sf-constants/Colors'
import { WP10, WP2, WP3, WP4, WP40, WP5, WP6, WP85 } from 'sf-constants/Sizes'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import { WP100 } from '../../../../constants/Sizes'
import { ReadMore } from '../../../../components'
import style from './style'

const borderRadius = WP2

class ProfileHeader extends Component {
  static propTypes = {
    profile: PropTypes.object,
    accountType: PropTypes.string,
    isMine: PropTypes.bool,
    uploadingCover: PropTypes.bool,
    uploadCover: PropTypes.func,
    navigateTo: PropTypes.func,
    editProfile: PropTypes.func,
  };

  constructor(props) {
    super(props)
    this.state = {
      shouldRenderBio: true
    }
  }

  _onSelectCover = (key) => (
    text ) => {
    key == 'coverb64' && this.props.uploadCover(text)
  };

  componentDidUpdate = (prevProps, prevState) => {
    const isBioChanged = get(prevProps.profile, 'about_me') !== get(this.props.profile, 'about_me')

    if (isBioChanged && prevState.shouldRenderBio) {
      this.setState({ shouldRenderBio: false })
    } else if (!prevState.shouldRenderBio) {
      this.setState({ shouldRenderBio: true })
    }
  }

  _renderBio = () => {
    const {
      profile
    } = this.props

    if (!this.state.shouldRenderBio) return (
      <Text
        style={style.artistHeaderDescriptionAbout}
        size={'slight'}
        type={'Circular'}
        weight={300}
        color={SHIP_GREY}
        numberOfLines={4}
      >
        {get(profile, 'about_me')}
      </Text>
    )

    return (
      <ReadMore
        numberOfLines={4}
        renderTruncatedFooter={this._renderTruncatedFooter}
        renderRevealedFooter={this._renderRevealedFooter}
      >
        <Text
          style={style.artistHeaderDescriptionAbout}
          size={'slight'}
          type={'Circular'}
          weight={300}
          color={SHIP_GREY}
        >
          {get(profile, 'about_me')}
        </Text>
      </ReadMore>
    )
  }

  _zoomAvatar = () => {
    const {
      navigateTo,
      profile: { profile_picture: url, full_name: title },
    } = this.props
    navigateTo('GalleryScreen', {
      images: [
        {
          url,
          title,
        },
      ],
    })
  };

  _coverOptions = [
    {
      onPress: selectPhoto(this._onSelectCover, 'cover', [18, 7], null, [
        820,
        820 * (7 / 18),
      ]),
      title: 'Choose from Library',
      withBorder: true,
    },
    {
      onPress: takePhoto(this._onSelectCover, 'cover', [18, 7], null, [
        820,
        820 * (7 / 18),
      ]),
      title: 'Take Photo',
    },
  ];

  render() {
    const {
      profile,
      isMine,
      navigateTo,
      getProfile
    } = this.props
    return (
      <View>
        <Image
          tint={'black'}
          style={{ width: WP100, height: 180, resizeMode: 'cover', position: 'absolute' }}
          // aspectRatio={132 / 360}
          source={require('../../../../assets/images/bgConnect.png')}
        />
        <View style={style.profileHeader}>
          <View style={style.profileHeaderAvatar}>
            <Avatar
              onPress={this._zoomAvatar}
              size={'ultraMassive'}
              shadow={false}
              image={get(profile, 'profile_picture')}
              verifiedStatus={get(profile, 'verified_status')}
              isProfile={true}
            />
          </View>
          <View style={style.profileHeaderInfo}>
            <Text lineHeight={WP6} type={'Circular'} centered weight={600} color={NAVY_DARK}>
              {get(profile, 'full_name')}
            </Text>
            <Text
              style={style.profileHeaderInfoJob}
              size={'mini'}
              type={'Circular'}
              centered
              weight={400}
              color={SHIP_GREY}
            >
              {/* {get(profile, 'job_title')} */}
              {profile.job_title ? profile.job_title : 'Belum melengkapi Profile'}
            </Text>
            <View style={style.profileHeaderInfoFollows}>
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() => {}
                  // navigateTo(
                  //   'ProfileFollowScreen',
                  //   {
                  //     isMine,
                  //     idUser: get(profile, 'id_user'),
                  //     onRefresh: () => getProfile(),
                  //   },
                  //   'push'
                  // )
                }
                style={style.profileHeaderInfoFollows}
              >
                <Text
                  size={'slight'}
                  type={'Circular'}
                  weight={400}
                  color={GUN_METAL}
                >{`${get(profile, 'total_followers')} `}</Text>
                <Text
                  size={'slight'}
                  type={'Circular'}
                  weight={300}
                  color={SHIP_GREY_CALM}
                >
                  Followers {'   '}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() => {}
                  // navigateTo(
                  //   'ProfileFollowScreen',
                  //   {
                  //     isMine,
                  //     idUser: get(profile, 'id_user'),
                  //     initialTab: 1,
                  //     onRefresh: () => getProfile(),
                  //   },
                  //   'push'
                  // )
                }
                style={style.profileHeaderInfoFollows}
              >
                <Text
                  size={'slight'}
                  type={'Circular'}
                  weight={400}
                  color={GUN_METAL}
                >{`${get(profile, 'total_following')} `}</Text>
                <Text
                  size={'slight'}
                  type={'Circular'}
                  weight={300}
                  color={SHIP_GREY_CALM}
                >
                  Following
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View>
            {this._renderBio()}
          </View>
        </View>
      </View>
    )
  }
}

export default ProfileHeader
