/* @flow */

import { compact, isEmpty, reduce, split } from 'lodash-es'
import * as yup from 'yup'

export const DUMP_DATA_FROM_API = {
  'about_me': 'more about me',
  'access': 'user',
  'account_status': 'active',
  'account_type': 'user',
  'activation_code': null,
  'birthdate': '10 April 2019',
  'client_id': 'NULL',
  'company': 'Skalasound',
  'cover_image': 'https://apidev.soundfren.id/userfiles/images/cover_image/1571152351_322_cover_image.jpg',
  'created_at': '2019-04-04 15:09:36',
  'email': 'ibnu.dev@gmail.com',
  'email_status': 'verified',
  'expo_token': 'ExponentPushToken[gp8fKVISpagXPd4roKYFtI]',
  'forgotten_password': '317107',
  'forgotten_password_time': '1555210754',
  'full_name': 'User 3',
  'gender': 'male',
  'id_city': '1103',
  'id_social': '111238152382463985565',
  'id_user': '322',
  'interest': [{
    'icon': 'https://eventeer.id/assets/image/icon_interest/pop.png',
    'id_interest': '6',
    'id_user': '322',
    'id_user_profile': '1805',
    'interest_name': 'Pop',
  },
  {
    'icon': 'https://eventeer.id/assets/image/icon_interest/jazz.png',
    'id_interest': '5',
    'id_user': '322',
    'id_user_profile': '1806',
    'interest_name': 'Jazz',
  },
  ],
  'is_feedback': '-1',
  'job_title': 'Music Prod',
  'last_login': '2020-02-02 21:40:07',
  'location': {
    'city_name': 'ACEH SELATAN',
    'country_name': 'Indonesia',
    'id_city': '1103',
    'id_country': '1',
    'id_province': '11',
    'order': '100',
    'province_name': 'ACEH',
  },
  'password': 'e83bf8ed8f6b5fbef419a1c1793ea703',
  'phone': '',
  'private_gender': 'false',
  'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/322_profile_picture.jpg',
  'registered_via': 'google',
  'registration_step': 'finish',
  'social_media': {
    'facebook': 'facebook.com/ffffz',
    'instagram': '',
    'twitter': 'twitter.com/uu',
    'website': '',
    'youtube': '',
  },
  'total_connection': 0,
  'total_followers': 3,
  'total_following': 1,
  'total_post': 0,
  'website': 'example.com',
}

export const DUMP_API_DATA = {}

export const DUMP_COLLABORATION_DATA = {}

// PROFILE_DATA Validation
export const FORM_VALIDATION = yup.object().shape({
  profileName: yup.string().required(),
  profileCityId: yup.string().required(),
  profileInterests: yup.array().test('min-1', 'Minimal 1 value', (value) => !isEmpty(compact(value))),
  profileBirthDate: yup.string(),
  profileGender: yup.string(),
})

// API_DATA to PROFILE_DATA
export const MAPPER_FROM_API = {
  profilePictureUri: 'profile_picture',
  profilePictureb64: 'profilePictureb64',
  coverImageUri: 'cover_image',
  coverImageb64: 'coverImageb64',
  profileName: 'full_name',
  profileCityId: 'id_city',
  profileCityName: 'location.city_name',
  profileProfessionTitle: 'job_title',
  profileProfessionCompany: 'company',
  profileInterests: (source) => reduce(source.interest, (result, value) => {
    result.push(value.interest_name)
    return result
  }, []),
  profileAbout: 'about_me',
  // profileSocialMedia: (source) => {
  //   if (isEmpty(source.social_media)) {
  //     const social_media = {
  //       facebook: '',
  //       instagram: '',
  //       twitter: '',
  //       youtube: '',
  //       website: '',
  //     }
  //     return social_media
  //   } else {
  //     let socialMedia = source.social_media
  //     socialMedia.facebook = split(socialMedia.facebook, '.com/')[1]
  //     socialMedia.twitter = split(socialMedia.twitter, '.com/')[1]
  //     socialMedia.instagram = split(socialMedia.instagram, '.com/')[1]
  //     return socialMedia
  //   }
  // },
  instagram: 'contact.instagram',
  youtube: 'contact.youtube',
  profileGender: 'gender',
  profileBirthDate: 'birthdate',
  profileEmail: 'email',
  createdBy: 'created_by',
  createdAt: 'created_at',
  updatedBy: 'updated_by',
  updatedAt: 'updated_at',
  roleTitle: 'role.role',
  rolePortfolio: 'role.portfolio',
  is_locked_role: 'role.is_locked_role'
}

// PROFILE_DATA to API_DATA
export const MAPPER_FOR_API = {
  'full_name': 'profileName',
  'id_city': 'profileCityId',
  'about_me': 'profileAbout',
  'company_name': 'profileProfessionCompany',
  'job_title': 'profileProfessionTitle',
  'profile_picture': 'profilePictureb64',
  'social_media': 'profileSocialMedia',
  'interest_name': 'profileInterests',
  'gender': 'profileGender',
  'birthdate': 'profileBirthDate',
  'role': {
    'title': 'roleTitle',
    'portfolio': 'rolePortfolio'
  }
}

export type Profile = {
  id: string | number;
}

export type PropsType = {
  initialLoaded?: boolean;
}

export type StateType = {
  isReady?: boolean;
  profile?: Profile;
}
