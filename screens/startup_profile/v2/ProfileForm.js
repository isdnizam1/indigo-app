import React from "react";
import { connect } from "react-redux";
import {
  BackHandler,
  Keyboard,
  TouchableOpacity,
  View,
  StyleSheet,
  FlatList,
} from "react-native";
import { isEmpty, map, noop, startCase, toLower } from "lodash-es";
import {
  _enhancedNavigation,
  Container,
  Form,
  Icon,
  Image,
  InputTextLight,
  SelectionBar,
  Text,
  ModalMessageView,
  SelectModalV3,
  SelectDate,
  MenuOptions,
  BottomSheet,
  InputModal,
} from "../../../components";

import {
  WHITE,
  SHIP_GREY_CALM,
  PALE_SALMON,
  REDDISH,
  SHIP_GREY,
  PALE_GREY_TWO,
  GUN_METAL,
  PALE_GREY,
  PALE_BLUE_TWO,
  PALE_BLUE,
  PALE_LIGHT_BLUE_TWO,
  SILVER_TWO,
  CLEAR_BLUE,
  ORANGE_BRIGHT,
  RED_20,
  LIPSTICK_TWO,
  YELLOW,
} from "../../../constants/Colors";
import {
  WP1,
  WP100,
  WP2,
  WP3,
  WP4,
  WP6,
  WP5,
  WP15,
  WP8,
  WP305,
  WP7,
  WP25,
  WP205,
  HP5,
  HP1,
  HP50,
  HP100,
} from "../../../constants/Sizes";
import { selectPhoto, takePhoto } from "../../../utils/upload";
import { convertBlobToBase64, fetchAsBlob } from "../../../utils/helper";
import {
  getCity,
  getCompany,
  getGenreInterest,
  getRoleStartUp,
  getProfileDetailV3,
  getGenreSuggestion,
  postEditProfile1000Startup,
  getJobStartUp,
} from "../../../actions/api";
import { objectMapper } from "../../../utils/mapper";
import { TOUCH_OPACITY, TEXT_INPUT_STYLE } from "../../../constants/Styles";
import { authDispatcher } from "../../../services/auth";
import SelectModalV4 from "../../../components/SelectModalV4";
import { FORM_VALIDATION, MAPPER_FOR_API, MAPPER_FROM_API } from "./_constants";

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
};

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam("initialLoaded", false),
  refreshProfile: getParam("refreshProfile", () => {}),
  onSuccessEditProfile: getParam("onSuccessEditProfile", () => {}),
});

const propsDefault = {};

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: "100%",
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 6,
  },
});

const Card = ({
  children,
  title,
  maxLength,
  border = true,
  backgroundColor = PALE_GREY_TWO,
  style,
}) => (
  <View
    style={{
      backgroundColor,
      paddingHorizontal: WP4,
      paddingTop: WP4,
      paddingBottom: maxLength ? WP4 : WP6,
      borderBottomColor: PALE_BLUE,
      borderBottomWidth: border ? 1 : 0,
      ...style,
    }}
  >
    {title ? (
      <Text
        type="Circular"
        size="xmini"
        weight={600}
        color={GUN_METAL}
        style={{ marginBottom: WP4 }}
      >
        {title}
      </Text>
    ) : null}
    {children}
  </View>
);

const Label = ({ title, subtitle, marginBottom = WP1 }) => (
  <Text
    weight={400}
    type="Circular"
    size={"xmini"}
    color={SHIP_GREY}
    style={{ marginBottom }}
  >
    {title}
    {subtitle && (
      <Text
        weight={300}
        type="Circular"
        size={"xmini"}
        color={PALE_LIGHT_BLUE_TWO}
      >
        {" "}
        {subtitle}
      </Text>
    )}
  </Text>
);

const ButtonIcon = ({
  onPress = () => {},
  title,
  iconName,
  iconType = "MaterialCommunityIcons",
  iconPosition = "left",
}) => (
  <TouchableOpacity
    onPress={onPress}
    activeOpacity={TOUCH_OPACITY}
    style={{ flexDirection: "row", alignItems: "center" }}
  >
    {iconPosition == "right" ? (
      <Text type="Circular" size="xmini" weight={400} color={ORANGE_BRIGHT}>
        {title}
      </Text>
    ) : null}
    <Icon
      centered
      background="dark-circle"
      size="small"
      color={ORANGE_BRIGHT}
      name={iconName}
      type={iconType}
    />
    {iconPosition == "left" ? (
      <Text type="Circular" size="xmini" weight={400} color={ORANGE_BRIGHT}>
        {title}
      </Text>
    ) : null}
  </TouchableOpacity>
);

class StartUpProfileForm extends React.Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this._didFocusSubscription = props.navigation.addListener("focus", () =>
      BackHandler.addEventListener("hardwareBackPress", this._backHandler)
    );
  }

  state = {
    profile: {},
    collaborationDurations: [],
    suggestionGenre: [],
    isReady: false,
    backModal: false,
    expandSocmed: false,
    dummyProfilePictureb64: "",
    roleStartUp: [],
  };

  async componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener("blur", () =>
      BackHandler.removeEventListener("hardwareBackPress", this._backHandler)
    );
    await this._getInitialScreenData();
    await this._getRoleStartUp();
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription();
    this._willBlurSubscription && this._willBlurSubscription();
  }

  _getInitialScreenData = async (...args) => {
    await this._getGenreSuggestion();
    await this._getProfileDetail(...args);
    await this.setState({
      isReady: true,
    });
  };

  _getGenreSuggestion = async () => {
    const { dispatch } = this.props;
    try {
      const { result } = await dispatch(
        getGenreSuggestion,
        {},
        noop,
        true,
        false
      );
      await this.setState({
        suggestionGenre: result,
      });
    } catch (e) {
      // silent
    }
  };

  _getRoleStartUp = async () => {
    const { dispatch } = this.props;
    try {
      const { result } = await dispatch(getRoleStartUp, {}, noop, true, false);
      await this.setState({
        roleStartUp: result,
      });
    } catch (e) {
      // silent
    }
  };

  _getProfileDetail = async () => {
    const { userData, dispatch } = this.props;
    const dataResponse = await dispatch(
      getProfileDetailV3,
      { id_user: userData.id_user, id_viewer: userData.id_user },
      noop,
      true,
      true
    );
    const profileData = objectMapper(dataResponse.result, MAPPER_FROM_API);
    const profilePictureb64 = !profileData.profilePictureUri
      ? null
      : await fetchAsBlob(profileData.profilePictureUri).then(
          convertBlobToBase64
        );
    const coverImageb64 = !profileData.coverImageUri
      ? null
      : await fetchAsBlob(profileData.coverImageUri).then(convertBlobToBase64);
    const profileDataNew = {
      ...profileData,
      profilePictureb64,
      coverImageb64,
    };
    await this.setState({
      profile: profileDataNew,
      dummyProfilePictureb64: profilePictureb64,
    });
  };

  _backHandler = async () => {
    const { navigateBack } = this.props;
    const { backModal } = this.state;

    if (this.form && this.form.isDirtyState()) {
      this.setState({ backModal: !backModal });
    } else {
      navigateBack();
    }
  };

  _postProfileUpdate = async ({ formData }) => {
    const { userData, dispatch } = this.props;
    const { dummyProfilePictureb64 } = this.state;
    const dataForApi = objectMapper(formData, MAPPER_FOR_API);
    let encoded = new URLSearchParams();
    encoded.append("id_user", userData.id_user);
    encoded.append("full_name", formData.profileName);
    encoded.append("id_city", formData.profileCityId);
    encoded.append("about_me", formData.profileAbout);
    encoded.append("company_name", formData.profileProfessionCompany);
    encoded.append("job_title", formData.profileProfessionTitle);
    encoded.append("profile_picture", formData.profilePictureb64);
    if (formData.instagram !== "") {
      try {
        if (formData.instagram.includes("instagram")) {
          encoded.append("social_media[instagram]", formData.instagram);
        } else {
          encoded.append(
            "social_media[instagram]",
            `https://www.instagram.com/${formData.instagram}`
          );
        }
      } catch (error) {}
    }
    encoded.append("social_media[youtube]", formData.youtube);
    encoded.append("gender", formData.profileGender);
    encoded.append("birthdate", formData.profileBirthDate);
    encoded.append("role[role]", formData.roleTitle);

    formData.rolePortfolio?.map((item) => {
      if (item !== "") {
        encoded.append("role[portfolio][]", item);
      }
    });
    formData.profileInterests.map((item) => {
      encoded.append("interest_name[]", item);
    });
    if (!dataForApi.about_me) dataForApi.about_me = "";
    if (dataForApi.profile_picture == dummyProfilePictureb64)
      dataForApi.profile_picture = "";
    try {
      // await dispatch(postEditProfile1000Startup, encoded, null, true)
      postEditProfile1000Startup(encoded).then((response) => {
        // triggered on success update profile
        this.props.getUserDetailDispatcher({ id_user: userData.id_user });
        this.props.refreshProfile();
        this.props.onSuccessEditProfile();
        this.props.navigateBack();
      });
    } catch (e) {
      console.log(`error ${JSON.stringify(e)}`);
    }
  };

  _genreItem = (genre, index, onChange, formData) => {
    return (
      <View
        key={index}
        style={{
          marginRight: WP2,
          marginBottom: WP2,
          flexDirection: "row",
          alignItems: "center",
          paddingHorizontal: WP3,
          paddingVertical: WP2,
          borderRadius: 6,
          backgroundColor: WHITE,
          borderWidth: 1,
          borderColor: PALE_BLUE_TWO,
        }}
      >
        <Text
          size="xmini"
          type="Circular"
          weight={400}
          color={SHIP_GREY}
          style={{ marginRight: WP205 }}
        >
          {genre}
        </Text>
        <Icon
          onPress={() => {
            const newGenre = formData.profileInterests;
            newGenre.splice(index, 1);
            onChange("profileInterests")(newGenre);
          }}
          centered
          size="small"
          color={PALE_BLUE_TWO}
          name={"close"}
          type="MaterialCommunityIcons"
        />
      </View>
    );
  };

  render() {
    const { navigateBack, isLoading } = this.props;
    const { isReady, profile, backModal, suggestionGenre, expandSocmed } =
      this.state;
    let newProfile = profile;
    delete newProfile.profilePictureb64;
    return (
      <Form
        ref={(form) => (this.form = form)}
        validation={FORM_VALIDATION}
        initialValue={{ ...profile }}
        onSubmit={this._postProfileUpdate}
      >
        {({ onChange, onSubmit, formData, isValid, isDirty }) => {
          return (
            <Container
              theme="dark"
              isLoading={isLoading}
              isReady={isReady}
              renderHeader={() => (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    paddingHorizontal: WP5,
                    paddingVertical: WP2,
                    backgroundColor: WHITE,
                  }}
                >
                  <View style={{ width: WP15 }}>
                    <Icon
                      onPress={() => this._backHandler()}
                      background="dark-circle"
                      size="large"
                      color={SHIP_GREY_CALM}
                      name="chevron-left"
                      type="Entypo"
                    />
                  </View>
                  <Text
                    type="Circular"
                    size="mini"
                    weight={400}
                    color={SHIP_GREY}
                    centered
                  >
                    Edit Profile
                  </Text>
                  <TouchableOpacity
                    style={{ width: WP15, alignContent: "flex-end" }}
                    disabled={!isValid || !isDirty}
                    onPress={() => {
                      Keyboard.dismiss();
                      onSubmit();
                    }}
                  >
                    <Text
                      type="Circular"
                      size="mini"
                      weight={400}
                      color={!isValid || !isDirty ? PALE_SALMON : REDDISH}
                      style={{ textAlign: "right" }}
                    >
                      Simpan
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
              isAvoidingView
              scrollable
              scrollBackgroundColor={PALE_GREY_TWO}
            >
              <Image
                tint={"black"}
                style={{
                  width: WP100,
                  height: 180,
                  resizeMode: "contain",
                  position: "absolute",
                }}
                // aspectRatio={132 / 360}
                source={require("../../../assets/images/bgConnect.png")}
              />
              <ModalMessageView
                style={{ width: WP100 - WP8 }}
                contentStyle={{ paddingHorizontal: WP4 }}
                toggleModal={() => this.setState({ backModal: false })}
                isVisible={backModal}
                title={"Perubahan belum tersimpan"}
                titleType="Circular"
                titleSize={"small"}
                titleColor={GUN_METAL}
                titleStyle={{ marginBottom: WP4 }}
                subtitle={
                  "Kamu memiliki beberapa perubahan yang belum tersimpan, apakah kamu yakin untuk keluar?"
                }
                subtitleType="Circular"
                subtitleSize={"xmini"}
                subtitleWeight={400}
                subtitleColor={SHIP_GREY_CALM}
                subtitleStyle={{ marginBottom: WP3 }}
                image={null}
                buttonPrimaryText={"Tidak"}
                buttonPrimaryContentStyle={{
                  borderRadius: 8,
                  paddingVertical: WP305,
                }}
                buttonPrimaryTextType="Circular"
                buttonPrimaryTextWeight={400}
                buttonPrimaryBgColor={REDDISH}
                buttonSecondaryText={"Ya"}
                buttonSecondaryStyle={{
                  backgroundColor: PALE_SALMON,
                  marginTop: WP1,
                  borderRadius: 8,
                  paddingVertical: WP305,
                }}
                buttonSecondaryTextType="Circular"
                buttonSecondaryTextWeight={400}
                buttonSecondaryTextColor={REDDISH}
                buttonSecondaryAction={() => navigateBack()}
              />

              <View style={{ backgroundColor: PALE_GREY_TWO }}>
                <Card
                  style={{
                    paddingTop: WP7,
                    paddingBottom: WP8,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <View
                    style={{
                      width: WP25,
                      height: WP25,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Image
                      size="massive"
                      imageStyle={{ borderRadius: WP25 / 2 }}
                      source={
                        formData.profilePictureUri == "remove" ||
                        isEmpty(formData.profilePictureUri)
                          ? require("../../../assets/icons/icUser.png")
                          : { uri: formData.profilePictureUri }
                      }
                    />
                    <MenuOptions
                      options={[
                        {
                          onPress: selectPhoto(
                            onChange,
                            "profilePicture",
                            [1, 1],
                            null,
                            [380, 380]
                          ),
                          title: "Choose from Library",
                          withBorder: true,
                        },
                        {
                          onPress: takePhoto(
                            onChange,
                            "profilePicture",
                            [1, 1],
                            null,
                            [380, 380]
                          ),
                          title: "Take Photo",
                          withBorder: true,
                        },
                        {
                          onPress: () => {
                            onChange("profilePictureUri")("remove");
                            onChange("profilePictureb64")("remove");
                          },
                          title: "Remove Current Photo",
                        },
                      ]}
                      triggerComponent={(toggleModal) => (
                        <TouchableOpacity
                          activeOpacity={TOUCH_OPACITY}
                          onPress={toggleModal}
                          style={{
                            width: WP8,
                            height: WP8,
                            borderRadius: WP8 / 2,
                            backgroundColor: YELLOW,
                            position: "absolute",
                            right: 0,
                            bottom: 0,
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <Icon
                            centered
                            size="slight"
                            color={WHITE}
                            name="camera-plus"
                            type="MaterialCommunityIcons"
                          />
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                </Card>
                <Card title={"BASIC INFORMATION"}>
                  <Label title={"Nama"} />
                  <InputTextLight
                    onChangeText={
                      (formData.profileName || "").length < 54
                        ? onChange("profileName")
                        : noop
                    }
                    value={formData.profileName}
                    placeholder="Tuliskan nama lengkap"
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY_CALM}
                    bordered
                    size="xmini"
                    type="Circular"
                    returnKeyType={"next"}
                    style={{ marginTop: 0, marginBottom: WP4 }}
                    textInputStyle={styles.input}
                    containerStyle={{ height: WP5 }}
                  />
                  <Label title={"Domisili Kota"} />
                  <SelectModalV3
                    refreshOnSelect
                    triggerComponent={
                      <View style={[styles.input, { marginBottom: WP4 }]}>
                        <Text
                          type="Circular"
                          size="xmini"
                          weight={300}
                          color={
                            isEmpty(formData.profileCityName)
                              ? SILVER_TWO
                              : SHIP_GREY_CALM
                          }
                        >
                          {isEmpty(formData.profileCityName)
                            ? "Pilih Kota"
                            : formData.profileCityName}
                        </Text>
                      </View>
                    }
                    header="Domisili Kota"
                    suggestion={getCity}
                    suggestionKey="city_name"
                    suggestionPathResult="city_name"
                    suggestionPathValue="id_city"
                    onChange={(city) => {
                      onChange("profileCityName")(city.city_name);
                      onChange("profileCityId")(city.id_city);
                    }}
                    suggestionCreateNewOnEmpty={false}
                    createNew={false}
                    asObject={true}
                    reformatFromApi={(text) => startCase(toLower(text))}
                    placeholder="Coba cari Kota"
                  />
                  <Label title={"Bio"} />
                  <InputTextLight
                    onChangeText={onChange("profileAbout")}
                    value={formData.profileAbout}
                    placeholder="Tuliskan deskripsi singkat tentang dirimu"
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY_CALM}
                    bordered
                    size="mini"
                    type="Circular"
                    returnKeyType={"next"}
                    wording=" "
                    maxLength={160}
                    maxLengthFocus
                    multiline
                    maxLine={5}
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={styles.input}
                  />
                </Card>
                <Card title={"ROLE"}>
                  <Label title={"Role"} />
                  <SelectModalV4
                    refreshOnSelect
                    disabled={formData.is_locked_role}
                    triggerComponent={
                      <View
                        style={[
                          styles.input,
                          { marginBottom: WP4 },
                          formData.is_locked_role && {
                            backgroundColor: "rgba(223, 227, 232, 0.2)",
                          },
                        ]}
                      >
                        <Text
                          type="Circular"
                          size="xmini"
                          weight={300}
                          color={
                            isEmpty(formData.roleTitle)
                              ? SILVER_TWO
                              : SHIP_GREY_CALM
                          }
                        >
                          {isEmpty(formData.roleTitle)
                            ? "Contoh: Hacker / Hipster / Hustler"
                            : formData.roleTitle}
                        </Text>
                      </View>
                    }
                    header="Pilih Role"
                    items={this.state.roleStartUp}
                    onChange={onChange("roleTitle")}
                    reformatFromApi={(text) => startCase(toLower(text))}
                    placeholder="Coba cari Role"
                  />
                  <Label title={"Link Attachment"} />
                  {formData.rolePortfolio?.map((item, index) => (
                    <InputTextLight
                      key={`${index}team`}
                      bordered
                      size="mini"
                      returnKeyType={"next"}
                      placeholder="Link Attachment"
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      value={item}
                      onChangeText={onChange("rolePortfolio", true, index)}
                      textInputStyle={TEXT_INPUT_STYLE["inputV2"]}
                      labelv2={`Attachment #${index + 1}`}
                      lineHeight={1}
                      multiline
                    />
                  ))}
                  <View style={{ marginBottom: HP1 }}>
                    <ButtonIcon
                      iconName={"plus"}
                      title={"Tambahkan Attachment"}
                      onPress={() => onChange("rolePortfolio", true)("")}
                    />
                  </View>
                  <Label title={"Posisi"} />

                  <InputModal
                    extraResult
                    refreshOnSelect
                    triggerComponent={
                      <InputTextLight
                        onChangeText={onChange("profileProfessionTitle")}
                        value={formData.profileProfessionTitle}
                        placeholder="Contoh: Chief of Executive Officer"
                        placeholderTextColor={SILVER_TWO}
                        color={SHIP_GREY_CALM}
                        bordered
                        size="xmini"
                        type="Circular"
                        returnKeyType={"next"}
                        style={{ marginTop: 0, marginBottom: WP4 }}
                        textInputStyle={styles.input}
                        containerStyle={{ height: WP5 }}
                      />
                    }
                    header="Select Profession"
                    suggestion={getJobStartUp}
                    suggestionKey="job_title"
                    suggestionPathResult="job_title"
                    selected={formData.profileProfessionTitle}
                    onChange={onChange("profileProfessionTitle")}
                    reformatFromApi={(text) => startCase(toLower(text))}
                    placeholder="Search profession here..."
                  />
                </Card>
                <Card title={"INTEREST"}>
                  <Label title={"Genre:"} marginBottom={WP2} />
                  <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                    {formData.profileInterests &&
                      map(formData.profileInterests, (genre, index) =>
                        this._genreItem(genre, index, onChange, formData)
                      )}
                  </View>
                  {/* button genre */}
                  <SelectModalV3
                    refreshOnSelect
                    triggerComponent={
                      <ButtonIcon iconName={"plus"} title={"Tambahkan Genre"} />
                    }
                    header="Genre"
                    suggestion={getGenreInterest}
                    suggestionKey="interest_name"
                    suggestionPathResult="interest_name"
                    onChange={(value) => {
                      const newGenre = formData.profileInterests;
                      if (!newGenre.includes(value)) {
                        newGenre.push(value);
                      }
                      onChange("profileInterests")(newGenre);
                    }}
                    createNew={true}
                    placeholder="Cari Genre"
                    selection={formData.profileInterests}
                    selectionWording="Genre Terpilih"
                    showSelection
                    onRemoveSelection={(index) => {
                      const newGenre = formData.profileInterests;
                      newGenre.splice(index, 1);
                      onChange("profileInterests")(newGenre);
                    }}
                    quickSearchTitle={"Saran Pencarian Genre"}
                    quickSearchList={suggestionGenre}
                  />
                </Card>
                <Card title={"CONTACT & SOCIAL MEDIA"}>
                  <Label title={"Instagram"} />
                  <InputTextLight
                    onChangeText={onChange("instagram")}
                    value={formData.instagram}
                    placeholder="Contoh: eventeer"
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY_CALM}
                    bordered
                    size="xmini"
                    type="Circular"
                    returnKeyType={"next"}
                    style={{ marginTop: 0, marginBottom: WP4 }}
                    textInputStyle={styles.input}
                    containerStyle={{ height: WP5 }}
                  />
                  <Label title={"Youtube"} />
                  <InputTextLight
                    onChangeText={onChange("youtube")}
                    value={formData.youtube}
                    placeholder="Masukkan link/url youtube channel"
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY_CALM}
                    bordered
                    size="xmini"
                    type="Circular"
                    returnKeyType={"next"}
                    keyboardType={"url"}
                    error={
                      formData.youtube
                        ?.toLowerCase()
                        .match(
                          /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/
                        ) == null &&
                      !isEmpty(formData.youtube) &&
                      "Url Youtube tidak valid.\nContoh: https://www.youtube.com/channel/{Channel-ID}"
                    }
                    style={{ marginTop: 0, marginBottom: WP4 }}
                    textInputStyle={styles.input}
                    containerStyle={{ height: WP5 }}
                  />
                  <Label title={"Email"} />
                  <InputTextLight
                    onChangeText={onChange("profileEmail")}
                    value={formData.profileEmail}
                    placeholder="Contoh: info@soundfren.com"
                    backgroundColor={"rgba(223, 227, 232, 0.2)"}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY_CALM}
                    bordered
                    editable={false}
                    size="xmini"
                    type="Circular"
                    returnKeyType={"next"}
                    keyboardType={"url"}
                    style={{ marginTop: 0, marginBottom: WP4 }}
                    textInputStyle={styles.input}
                    containerStyle={{ height: WP5 }}
                  />
                </Card>
                <Card title={"PRIVATE INFORMATION"} border={false}>
                  <Label title="Gender" />
                  <SelectionBar
                    options={[
                      { key: "male", text: "Male" },
                      { key: "female", text: "Female" },
                    ]}
                    selectedValue={formData.profileGender}
                    onPress={onChange("profileGender")}
                    style={{ marginBottom: WP4 }}
                  />
                  <SelectDate
                    label="Tanggal Lahir"
                    value={formData.profileBirthDate}
                    dateFormat="YYYY-MM-DD"
                    maximumDate={new Date()}
                    mode="date"
                    onChangeDate={onChange("profileBirthDate")}
                    style={{ marginBottom: 0 }}
                    inputStyle={
                      !formData.profileBirthDate
                        ? { borderColor: RED_20 }
                        : null
                    }
                  />
                </Card>
              </View>
            </Container>
          );
        }}
      </Form>
    );
  }
}

StartUpProfileForm.navigationOptions = () => ({
  gesturesEnabled: false,
});

StartUpProfileForm.defaultProps = propsDefault;
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(StartUpProfileForm),
  mapFromNavigationParam
);
