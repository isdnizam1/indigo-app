import React from 'react'
import { View, Image as RNImage, AppState, TouchableOpacity, ScrollView } from 'react-native'
import { noop, get } from 'lodash-es'
import { connect } from 'react-redux'
import HTMLElement from 'react-native-render-html'
import moment from 'moment'
import { LinearGradient } from 'expo-linear-gradient'
import { getAdsDetail, postLike } from '../../actions/api'
import { NavigateToInternalBrowser, postScreenTimeAdsHelper } from '../../utils/helper'
import { FONT_SIZE, WP100, WP2, WP3, WP4, WP5, WP6, HP100, HP10, HP5 } from '../../constants/Sizes'
import {
  NAVY_DARK,
  NO_COLOR,
  PALE_BLUE,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SKELETON_HIGHLIGHT,
  SKELETON_COLOR,
  WHITE,
} from '../../constants/Colors'
import ImageComponent from '../../components/Image'
import Text from '../../components/Text'
import Container from '../../components/Container'
import HeaderNormal from '../../components/HeaderNormal'
import AdsFeedback from '../../components/ads/AdsFeedback'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { HEADER } from '../../constants/Styles'
import NewsItem from './components/NewsItem'
import CounterButton from '../../components/feed/CounterButton'
import FeedItemSkeletonConfig from '../../components/feed/FeedItemSkeletonConfig'
import SkeletonContent from 'react-native-skeleton-content'
import Touchable from '../../components/Touchable'
import { Icon } from '../../components'

const styles = {
  section: {
    paddingHorizontal: WP5,
  },
  divider: {
    backgroundColor: PALE_BLUE,
    height: 1,
    marginTop: WP3,
    marginBottom: WP5,
  },
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam('onRefresh', () => {}),
  idAds: getParam('id_ads', 0),
  is1000Startup: getParam('is1000Startup', false),
  refreshListLearn: getParam('refreshListLearn', () => {})
})

const dateFormat = 'YYYY-MM-DD HH:mm:ss'
class NewsDetailScreen extends React.Component {

  constructor(props) {
    super(props)
    // console.log(props.navigation.addListener)
    this._didFocusSubscription = props.navigation.addListener('focus', () => {
      // console.log('isfocus')
      this.setState({ viewFrom: moment().format(dateFormat) })
      AppState.addEventListener('change', this._handleAppStateChange)
    }
    )
  }

  state = {
    ads: {},
    isReady: false,
    showFeedback: true,
    viewFrom: null,
  };

  async componentDidMount() {
    if (!this.props.userData.id_user) {
      this.props.navigateBack()
      this.props.navigateTo('AuthNavigator')
    } else await this._getInitialScreenData()

    // console.log(this.props.is1000Startup)

    this._willBlurSubscription = this.props.navigation.addListener('blur', () => {
      // console.log('isBlur')
      this._postScreenTimeAds()
      if (this.props.refreshListLearn) {
        this.props.refreshListLearn()
      }
      AppState.removeEventListener('change', this._handleAppStateChange)
    }
    )
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _didFocusSubscription;
  _willBlurSubscription;

  _handleAppStateChange = (nextAppState) => {
    // console.log(AppState.currentState)
    if (AppState.currentState === 'background') {
      this._postScreenTimeAds()
    }
    if ((this.state.appState === 'inactive' || this.state.appState === 'background') && nextAppState === 'active') {
      this.setState({ viewFrom: moment().format(dateFormat) })
      // console.log('back to foreground', this.state.viewFrom)
    }
    this.setState({ appState: nextAppState })
    // console.log({ appState: this.state.appState })
  }

  _postScreenTimeAds = () => {
    const { viewFrom } = this.state
    const { idAds, userData: { id_user }, dispatch, is1000Startup } = this.props
    // console.log({ idAds, id_user, viewFrom, is1000Startup })
    postScreenTimeAdsHelper({ viewFrom, id_ads: idAds, id_user, dispatch, isLoading: true, isStartup: is1000Startup })
  }

  _getInitialScreenData = async (...args) => {
    await this._getAdsDetail(...args)
    await this.setState({
      isReady: true,
    })
  };

  _getAdsDetail = async (isLoading = true) => {
    const {
      dispatch,
      userData: { id_user },
      idAds: id_ads,
    } = this.props
    const dataResponse = await dispatch(
      getAdsDetail,
      { id_user, id_ads },
      noop,
      false,
      isLoading,
    )
    await this.setState({ ads: dataResponse })
  };

  _onSubmitFeedbackCallback = () => {
    this.setState({ showFeedback: false })
  };

  _onLinkPress = (evt, href, htmlAttribs) => {
    const { navigateTo } = this.props

    const splitedHref = href.split('/')

    const uriPrefix = splitedHref[0]
    const relatedId = splitedHref[splitedHref.length - 1]
    if (uriPrefix === 'user') {
      navigateTo('ProfileScreen', { idUser: relatedId })
    } else {
      NavigateToInternalBrowser({
        url: href,
      })
    }
  };

  _onPressLike = () => {
    const {
      userData: { id_user },
      isStartup,
      onRefresh
    } = this.props
    const {
      ads,
    } = this.state

    postLike({
      id_user,
      related_to: 'id_ads',
      id_related_to: ads.id_ads
    }).then(() => {
      this._getAdsDetail()
    })
  }

  _renderHeaderRightButton = () => {
    const {
      userData: { id_user },
      is1000Startup,
    } = this.props
    const {
      ads,
    } = this.state

    if (is1000Startup) {
      if (ads !== null) {
        return (
          <TouchableOpacity onPress={this._onPressLike}>
            <View style={{ padding: 15, justifyContent: 'center', alignItems: 'center' }}>
              <RNImage
                source={ads.is_liked
                  ? require('sf-assets/icons/active_heart.png')
                  : require('sf-assets/icons/outlineHeartBlack.png')
                }
                style={{ width: 24, height: 24 }}
              />
            </View>
          </TouchableOpacity>
        )
      }
    }
  }

  _renderCommentButton = () => {
    const { isLoading } = this.props
    const {
      ads
    } = this.state
    return (
      <Touchable
        onPress={this._onPressComment}
        style={{
          // position: 'absolute',
          // height: HP5,
          // left: 0,
          // top: HP100 - HP5,
          // width: WP100,
          // elevation: 5,
          backgroundColor: WHITE,
          borderTopWidth: 1,
          borderTopColor: '#E4E6E7'
        }}>
        <View style={{
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
          // flex: 1,
          paddingVertical: WP2,
          paddingHorizontal: WP2,
          }}>
          <CounterButton
            onPress={this._onPressComment}
            images={[null, require('sf-assets/icons/icComment.png')]}
            value={counterLabel(
              ads.total_comment,
              'Komentar',
              'Komentar',
            )}
            disable={isLoading}
          />
          <Icon
            centered
            name={'chevron-thin-right'}
            type='Entypo'
            color={SHIP_GREY_CALM}
          />
        </View>
      </Touchable>
    )
  };

  _onPressComment = () => {
    const { navigateTo } = this.props
    const {
      ads
    } = this.state
    navigateTo('StartUpCommentScreen',{
      id_ads: ads.id_ads,
      onRefresh: this._getAdsDetail
    })
  }

  render() {
    const { isLoading, navigateBack, navigateTo, idAds, dispatch } = this.props

    const { ads, isReady, showFeedback } = this.state

    return (
      <Container
        theme='dark'
        isLoading={isLoading}
        isReady={isReady}

        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={navigateBack}
              text={'Article'}
              textSize={'slight'}
              // rightComponent={
              //   <ImageComponent
              //     source={require('../../assets/icons/icShareInactive.png')}
              //     style={HEADER.rightIcon}
              //     size='xtiny'
              //     onPress={() =>
              //       navigateTo('ShareScreen', {
              //         id: idAds,
              //         type: 'explore',
              //         title: 'Bagikan Artikel',
              //       })
              //     }
              //   />
              // }
              rightComponent={this._renderHeaderRightButton()}
              centered
            />
            <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
          </View>
        )}
        // scrollable
        // scrollBackgroundColor={NO_COLOR}
        scrollContentContainerStyle={{ justifyContent: 'space-between' }}
      >
        <ScrollView style={{  backgroundColor: NO_COLOR }} contentContainerStyle={{paddingVertical: WP6,}}>
          <View style={{ ...styles.section, marginBottom: WP4 }}>
            <Text
              type='Circular'
              color={REDDISH}
              weight={500}
              size='tiny'
              style={{ marginBottom: WP2 }}
            >
              ARTICLE
            </Text>
            <Text
              type='Circular'
              weight={600}
              color={NAVY_DARK}
              size='huge'
              style={{ marginBottom: WP2 }}
            >
              {ads.title}
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <Text color={SHIP_GREY_CALM} size='xmini' type='Circular'>
                {moment(ads.created_at).format('D MMMM YYYY')}
              </Text>
              <Text color={SHIP_GREY_CALM} size='xmini' type='Circular'>
                {'   ·   '}
              </Text>
              <Text color={SHIP_GREY_CALM} size='xmini' type='Circular'>
                {ads.mins_read}
              </Text>
            </View>
          </View>

          <RNImage
            aspectRatio={4 / 3}
            source={{ uri: ads.image }}
            style={{ marginBottom: WP3, width: WP100 }}
          />
          {!!get(ads, 'additional_data.caption_image') && (
            <View style={{ paddingHorizontal: WP5 }}>
              <Text type={'Circular'} size={'mini'} color={SHIP_GREY_CALM}>
                {get(ads, 'additional_data.caption_image')}
              </Text>
            </View>
          )}

          <View style={{ ...styles.section }}>
            <HTMLElement
              html={
                ads.description ? ads.description.replace(/(\r\n|\n|\r|)/gm, '') : ''
              }
              baseFontStyle={{
                fontFamily: 'CircularBook',
                fontSize: FONT_SIZE.mini,
                lineHeight: WP5 + 1,
                color: SHIP_GREY,
              }}
              onLinkPress={this._onLinkPress}
            />
          </View>

          {showFeedback && (
            <AdsFeedback
              ads={ads}
              dispatch={dispatch}
              onSubmitCallback={this._onSubmitFeedbackCallback}
            />
          )}
          <View style={styles.divider} />

          <View style={{ ...styles.section }}>
            <Text
              weight={600}
              color={NAVY_DARK}
              type='Circular'
              style={{ marginBottom: WP4 }}
            >
              Baca Lainnya
            </Text>
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
              }}
            >
              {ads.other_news &&
                ads.other_news.map((item, index) => (
                  <NewsItem ads={item} key={index} navigateTo={navigateTo} />
                ))}
            </View>
          </View>
          
        </ScrollView>
        {this._renderCommentButton()}
      </Container>
    )
  }
}

const counterLabel = (value, label, empty) => {
  let labelText = value < 1 ? empty : label
  let valueText = value < 1 ? '' : `${value} `
  return `${valueText}${labelText}`
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(NewsDetailScreen),
  mapFromNavigationParam,
)
