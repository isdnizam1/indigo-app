import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import noop from 'lodash-es/noop'
import Image from 'sf-components/Image'
import Text from 'sf-components/Text'
import { WP1, WP105, WP2, WP25, WP3, WP5 } from 'sf-constants/Sizes'
import { GUN_METAL, SHIP_GREY_CALM } from 'sf-constants/Colors'
import Icon from 'sf-components/Icon'

const AllNewsItem = ({ ads, navigateTo, is1000Startup=false }) => {
  return (
    <TouchableOpacity
      style={{ marginBottom: WP5, flexDirection: 'row' }}
      onPress={() => {
        navigateTo('NewsDetailScreen', { id_ads: ads.id_ads, is1000Startup }, 'push')
      }}
    >
      <Image
        aspectRatio={1.352}
        source={{ uri: ads.image }}
        imageStyle={{ width: WP25, borderRadius: 5 }}
        style={{ marginBottom: WP2, marginRight: WP5 }}
      />
      <View style={{ flex: 1 }}>
        <Text numberOfLines={3} ellipsizeMode='tail' color={GUN_METAL} weight={400} size='slight' style={{ marginBottom: WP1 }}>
          {ads.title}
        </Text>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            source={require('sf-assets/icons/icon_eye.png')}
            imageStyle={{ width: WP3 }} aspectRatio={1}
            style={{ marginRight: WP105 }}
          />
          <Text color={SHIP_GREY_CALM} size='xmini' type='Circular'>{ads.total_view}</Text>
          <Icon centered color={SHIP_GREY_CALM} size='tiny' type='Entypo' name='dot-single'/>
          <Text color={SHIP_GREY_CALM} weight={200} size='xmini'>
            {ads.mins_read}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

AllNewsItem.propTypes = {
  ads: PropTypes.objectOf(PropTypes.any),
  navigateTo: PropTypes.func
}

AllNewsItem.defaultProps = {
  ads: {},
  navigateTo: noop
}

export default AllNewsItem
