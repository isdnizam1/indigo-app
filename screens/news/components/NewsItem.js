import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import noop from 'lodash-es/noop'
import Image from '../../../components/Image'
import Text from '../../../components/Text'
import { WP1, WP105, WP2, WP3, WP43, WP5 } from '../../../constants/Sizes'
import { GUN_METAL, SHIP_GREY_CALM } from '../../../constants/Colors'
import Icon from '../../../components/Icon'

const NewsItem = ({ ads, navigateTo, withViewCount, is1000Startup=false }) => {
  return (
    <TouchableOpacity
      style={{ width: WP43, marginBottom: WP5 }}
      onPress={() => {
        navigateTo('NewsDetailScreen', { id_ads: ads.id_ads, is1000Startup }, 'push')
      }}
    >
      <Image
        aspectRatio={1.34}
        source={{ uri: ads.image }}
        imageStyle={{ width: '100%', borderRadius: 5 }}
        style={{ marginBottom: WP2 }}
      />
      <Text numberOfLines={3} ellipsizeMode='tail' color={GUN_METAL} weight={400} size='slight' style={{ marginBottom: WP1 }}>
        {ads.title}
      </Text>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        {
          withViewCount && (
            <Fragment>
              <Image
                source={require('../../../assets/icons/icon_eye.png')}
                imageStyle={{ width: WP3 }} aspectRatio={1}
                style={{ marginRight: WP105 }}
              />
              <Text color={SHIP_GREY_CALM} size='xmini' type='Circular'>{ads.total_view}</Text>
              <Icon centered color={SHIP_GREY_CALM} size='tiny' type='Entypo' name='dot-single'/>
            </Fragment>
          )
        }
        <Text color={SHIP_GREY_CALM} weight={200} size='xmini'>
          {ads.mins_read}
        </Text>
      </View>
    </TouchableOpacity>
  )
}

NewsItem.propTypes = {
  ads: PropTypes.objectOf(PropTypes.any),
  navigateTo: PropTypes.func,
  withViewCount: PropTypes.bool
}

NewsItem.defaultProps = {
  ads: {},
  navigateTo: noop,
  withViewCount: false
}

export default NewsItem
