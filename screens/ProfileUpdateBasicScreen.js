import React from 'react'
import { ScrollView, View, TouchableOpacity, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import { noop } from 'lodash-es'
import { _enhancedNavigation, Container, Header, Icon, Text, Form, InputTextLight, InputDate } from '../components'
import { ORANGE_BRIGHT, WHITE } from '../constants/Colors'
import { HP2, WP4 } from '../constants/Sizes'
import { TOUCH_OPACITY } from '../constants/Styles'
import { postProfileUpdateBasic } from '../actions/api'
import { authDispatcher } from '../services/auth'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', () => {})
})

class ProfileUpdateBasicScreen extends React.Component {
  _onUpdateBasic = async ({ formData }) => {
    const {
      userData: { id_user },
      navigateBack,
      refreshProfile,
      dispatch,
      getUserDetailDispatcher,
    } = this.props

    await dispatch(postProfileUpdateBasic, {
      id_user,
      gender: formData.gender,
      birthdate: formData.birthdate,
      website: formData.website
    }, noop, true)
    getUserDetailDispatcher({ id_user })
    refreshProfile()
    navigateBack()
  }

  render() {
    const {
      userData: user,
      isLoading,
      navigateBack
    } = this.props

    return (
      <Container scrollable isLoading={isLoading} colors={[WHITE, WHITE]}>
        <Form initialValue={user} onSubmit={this._onUpdateBasic}>
          {({ onChange, onSubmit, formData, isDirty }) => (
            <View style={{ flex: 1 }}>
              <Header>
                <Icon onPress={() => navigateBack()} size='massive' color={ORANGE_BRIGHT} name='close'/>
                <Text size='massive' type='SansPro' weight={500}>Basic Info</Text>
                <TouchableOpacity
                  disabled={!isDirty} onPress={() => {
                    Keyboard.dismiss()
                    onSubmit()
                  }} activeOpacity={TOUCH_OPACITY}
                >
                  <Text size='large' color={ORANGE_BRIGHT}>Save</Text>
                </TouchableOpacity>
              </Header>
              <ScrollView
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps='handled'
              >
                <View style={{ flex: 1, marginVertical: HP2, marginHorizontal: WP4 }}>
                  <InputTextLight
                    value={formData.gender}
                    label='Gender'
                    onChangeText={onChange('gender')}
                  />
                  <InputDate
                    value={formData.birthdate}
                    label='Birthdate'
                    onChangeText={onChange('birthdate')}
                  />
                  <InputTextLight
                    disabled
                    editable={false}
                    value={formData.email}
                    label='Email'
                    onChangeText={onChange('email')}
                  />
                  <InputTextLight
                    value={formData.website}
                    label='Website'
                    onChangeText={onChange('website')}
                  />
                </View>
              </ScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProfileUpdateBasicScreen),
  mapFromNavigationParam
)
