import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { View } from 'react-native'
import { TabBar, TabView } from 'react-native-tab-view'
import { GREY, GREY_WARM, ORANGE_BRIGHT, WHITE } from '../constants/Colors'
import { Container, Text } from '../components'
import { WP45 } from '../constants/Sizes'
import _enhancedNavigation from '../navigation/_enhancedNavigation'
import { getAds } from '../actions/api'
import AdsAnnouncementItem from '../components/ads/AdsAnnouncementItem'
import AdsBandItem from '../components/ads/AdsBandItem'
import AdsCollaborationItem from '../components/ads/AdsCollaborationItem'
import AdsTab from '../components/ads/AdsTab'
import AdsEventItem from '../components/ads/AdsEventItem'
import AdsAuditionItem from '../components/ads/AdsAuditionItem'
import { BORDER_STYLE } from '../constants/Styles'
import { AD_ROUTES } from './ads/AdsConfig'

const mapStateToProps = ({ auth, message }) => ({
  userData: auth.user,
  newMessageCount: message.newMessageCount
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  category: getParam('category', 'announcement'),
  availableAd: getParam('availableAd', {
    announcement: true,
    band: true,
    collaboration: true,
    venue: true,
    newsUpdate: true,
    event: true,
    submission: true,
    video: true,
    soundconnect: true,
    soundplay: true
  }),
  filters: getParam('filters', {})
})

class AdsScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      ads: [],
      start: 0,
      limit: 50,
      index: this._mapCategory().indexOf(this.props.category) || 0,
      isReady: false,
      isTabReady: false,
      tabHeight: undefined,
      routes: this._getRoutes(),
      categories: this._mapCategory()
    }
  }

  _getRoutes = () => {
    let { availableAd } = this.props
    const routes = [
      AD_ROUTES.announcement,
    ]

    if (availableAd.collaboration) routes.push(AD_ROUTES.collaboration)
    if (availableAd.submission) routes.push(AD_ROUTES.submission)
    if (availableAd.band) routes.push(AD_ROUTES.band)

    if (availableAd.event) routes.push(AD_ROUTES.event)
    return routes
  }

  _mapCategory = () => {
    const { availableAd } = this.props
    const categories = [AD_ROUTES.announcement.category]

    if (availableAd.collaboration) categories.push(AD_ROUTES.collaboration.category)
    if (availableAd.submission) categories.push(AD_ROUTES.submission.category)
    if (availableAd.band) categories.push(AD_ROUTES.band.category)

    if (availableAd.event) categories.push(AD_ROUTES.event.category)
    return categories
  }

  componentDidMount = async () => {
    await this._getAdList()
    this.setState({
      isReady: true
    })
    // BackHandler.addEventListener('hardwareBackPress', this.props.navigateBack)
  }

  componentWillUnmount() {
    // BackHandler.removeEventListener('hardwareBackPress', this.props.navigateBack)
  }

  _getAdList = async (params) => {
    const {
      index,
      routes,
      start,
      limit
    } = this.state

    try {
      const { data } = await getAds({
        ...params,
        status: 'active',
        category: routes[index].category,
        start,
        limit
      })

      if (data.code === 200) {
        this.setState({
          ads: data.result,
          isTabReady: true
        })
      } else if (data.code === 500 && data.message === 'Ads is empty') {
        this.setState({
          ads: [],
          isTabReady: true
        })
      }
    } catch (e) {
      //
    }
  }

  _onChangeTab = (index) => {
    this.setState({
      isTabReady: false,
      index
    }, this._getAdList)
  }

  _onPullDownToRefresh = () => {
    this.setState({
      isTabReady: false
    }, this._getAdList)
  }

  _getCategoryIndex = (category) => {
    return this.state.categories.indexOf(category)
  }

  render() {
    const {
      navigateTo,
      isLoading
    } = this.props
    const {
      ads,
      isReady,
      isTabReady,
      tabHeight,
      index,
      routes
    } = this.state

    return (
      <Container
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={this._onPullDownToRefresh}
        onPullUpToLoad={async () => {
        }}
        isReady={isReady}
        isLoading={isLoading}
        type='horizontal'
        renderHeader={() => null}
      >
        <TabView
          style={{ height: tabHeight }}
          navigationState={this.state}
          renderScene={
            ({ route, jumpTo }) => {
              switch (route.key) {
              case 'announcement':
                return (
                  <AdsTab
                    isTabReady={isTabReady}
                    adsList={index === this._getCategoryIndex('announcement') ? ads : []}
                    navigateTo={navigateTo}
                    AdsItem={AdsAnnouncementItem}
                    route={route.key}
                    activeRoute={routes[index].key}
                  />
                )
              case 'event':
                return (
                  <AdsTab
                    isTabReady={isTabReady}
                    adsList={index === this._getCategoryIndex('event') ? ads : []}
                    navigateTo={navigateTo}
                    AdsItem={AdsEventItem}
                    route={route.key}
                    activeRoute={routes[index].key}
                  />
                )
              case 'band':
                return (
                  <AdsTab
                    isTabReady={isTabReady}
                    adsList={index === this._getCategoryIndex('band') ? ads : []}
                    navigateTo={navigateTo}
                    AdsItem={AdsBandItem}
                    route={route.key}
                    numColumns={2}
                    activeRoute={routes[index].key}
                  />
                )
              case 'submission':
                return (
                  <AdsTab
                    isTabReady={isTabReady}
                    adsList={index === this._getCategoryIndex('submission') ? ads : []}
                    navigateTo={navigateTo}
                    AdsItem={AdsAuditionItem}
                    route={route.key}
                    activeRoute={routes[index].key}
                  />
                )
              case 'collaboration':
                return (
                  <AdsTab
                    isTabReady={isTabReady}
                    adsList={index === this._getCategoryIndex('collaboration') ? ads : []}
                    navigateTo={navigateTo}
                    AdsItem={AdsCollaborationItem}
                    route={route.key}
                    activeRoute={routes[index].key}
                    getAds={this._getAdList}
                    filters={this.props.filters || {}}
                  />
                )
                // case 'news_update':
                //   return (
                //     <AdsTab
                //       isTabReady={isTabReady}
                //       adsList={index === this._getCategoryIndex('news_update') ? ads : []}
                //       navigateTo={navigateTo}
                //       AdsItem={AdsAnnouncementItem}
                //       route={route.key}
                //       activeRoute={routes[index].key}
                //     />
                //   )
                // case 'venue':
                //   return (
                //     <AdsTab
                //       isTabReady={isTabReady}
                //       adsList={index === 4 ? ads : []}
                //       navigateTo={navigateTo}
                //       AdsItem={AdsVenueItem}
                //       route={route.key}
                //       activeRoute={routes[index].key}
                //     />
                //   )
              }
            }
          }
          renderTabBar={(props) => (
            <TabBar
              {...props}
              style={[{
                backgroundColor: WHITE, elevation: 0,
                ...BORDER_STYLE['bottom']
              }]}
              indicatorStyle={{ backgroundColor: ORANGE_BRIGHT }}
              scrollEnabled
              tabStyle={{ width: WP45 }}
              renderLabel={({ route, focused, color }) => {
                return (
                  <View>
                    <Text centered size='mini' color={focused ? GREY : GREY_WARM} weight={400}>
                      {route.title}
                    </Text>
                  </View>
                )
              }}
            />
          )}
          onIndexChange={this._onChangeTab}
        />
      </Container>
    )
  }
}

AdsScreen.propTypes = {
  category: PropTypes.string,
  isLoading: PropTypes.bool,
  navigateTo: PropTypes.func
}

AdsScreen.defaultProps = {
  navigateTo: () => {
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(AdsScreen),
  mapFromNavigationParam
)
