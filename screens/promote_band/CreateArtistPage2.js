import React, { Component } from 'react'
import { ActivityIndicator, ScrollView, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { LinearGradient } from 'expo-linear-gradient'
import { cloneDeep, noop } from 'lodash-es'
import { CheckBox } from 'react-native-elements'
import Icon from '../../components/Icon'
import { NO_COLOR, ORANGE, PURPLE, WHITE } from '../../constants/Colors'
import Text from '../../components/Text'
import Container from '../../components/Container'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { WP1, WP10, WP100, WP3, WP4, WP6, WP8 } from '../../constants/Sizes'
import { getProfileSongs, getProfileVideos } from '../../actions/api'
import Image from '../../components/Image'
import { workIcon } from '../../components/promote/PromoteComponent'
import { invalidButtonStyle, validButtonStyle } from '../../components/promote/PromoteConstant'
import { ArtistPageSteps } from '../../components/StepCircle'
import { isPremiumUser } from '../../utils/helper'
import { alertBackUnsaved } from '../../utils/alert'
import HeaderNormal from '../../components/HeaderNormal'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  artistData: getParam('artistData', {})
})

class CreateArtistPage2 extends Component {

  constructor(props) {
    super(props)
    const artistData = this.props.artistData
    artistData.songs = []
    this.state = {
      artistData,
      songs: [],
      videos: [],
      loading: true
    }
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('focus', this.fetchListing.bind(this))
  }

  componentWillUnmount() {
    this.focusListener()
  }

  async fetchListing() {
    this.setState({ loading: true })
    const {
      dispatch, userData
    } = this.props
    const songsResponse = await dispatch(getProfileSongs, { id_user: userData.id_user }, noop, true, true)
    const videosResponse = await dispatch(getProfileVideos, { id_user: userData.id_user }, noop, true, true)
    this.setState({ songs: songsResponse.result || [], videos: videosResponse.result || [], loading: false })
  }

  _isEmpty() {
    const { artistData } = this.state

    return (artistData.songs.length === 0)
  }

  _backHandler = async () => {
    if (this._isEmpty()) {
      this.props.navigateBack()
    } else {
      alertBackUnsaved(this.props.navigateBack)
    }
  }

  _isValid = () => {
    const { artistData } = this.state

    if (artistData.songs.length === 0) return invalidButtonStyle

    return validButtonStyle
  }

  _handleCheck = (song) => {
    const artistData = cloneDeep(this.state.artistData)

    const index = artistData.songs.findIndex((element) => element.id_journey === song.id_journey)

    if (index < 0) artistData.songs.push(song)
    else artistData.songs.splice(index, 1)
    this.setState({ artistData })

  }

  _journeyItem = (item, index) => {
    const {
      artistData
    } = this.state

    const indexSong = artistData.songs.findIndex((element) => element.id_journey === item.id_journey)

    return (
      <CheckBox
        key={index}
        title={
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={workIcon[item.type]} style={{ marginHorizontal: WP3 }} imageStyle={{ height: WP8 }}/>
            <Text size='slight'>{item.title}</Text>
          </View>
        }
        checked={indexSong >= 0}
        onPress={() => this._handleCheck(item)}
        containerStyle={{ backgroundColor: NO_COLOR, borderWidth: 0, marginHorizontal: -1000, paddingVertical: -(WP4) }}
        wrapperStyle={{ backgroundColor: NO_COLOR, borderWidth: 0 }}
      />
    )
  }

  render() {
    const {
      navigateTo,
      userData
    } = this.props

    const {
      videos, songs, artistData, loading
    } = this.state

    const isValid = this._isValid()

    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._backHandler}
            text='Create Artist Page'
            centered
          />
        )}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ backgroundColor: WHITE }}
          style={{ flexGrow: 1 }}
        >
          <ArtistPageSteps index={2} isPremium={isPremiumUser(userData.account_type)}/>

          <View style={{ flex: 1, paddingHorizontal: WP6, paddingBottom: WP10 }}>
            <View style={{ marginBottom: 10 }}>
              <Text size='extraMassive' weight={500} color={ORANGE} style={{ marginBottom: 10 }}>Select Songs</Text>
              <Text>Select one or more of your songs to be featured in your Artist Page</Text>
            </View>
            {loading ? <View style={{ marginBottom: WP4 }}><ActivityIndicator color={ORANGE} size={'large'} /></View> : null}
            {!loading ? <View style={{ marginBottom: WP4 }}>
              {
                songs.map((song, index) => (
                  this._journeyItem(song, index)
                ))
              }
              {
                videos.map((video, index) => (
                  this._journeyItem(video, index)
                ))
              }
            </View> : null}
            <View style={{ paddingHorizontal: WP6 }}>
              <TouchableOpacity
                style={{
                  flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderRadius: WP1,
                  borderWidth: 0.75, borderStyle: 'dotted', borderColor: PURPLE, padding: WP4
                }}
                onPress={async () => {
                  navigateTo('ProfileScreen', { isOpenAddWorks: true, navigateBackOnDone: true, tabToShow: 1 })
                }}
              >
                <View
                  style={{ padding: 3, backgroundColor: PURPLE, borderRadius: WP100, flexGrow: 0, marginRight: WP1 }}
                >
                  <Icon name='plus' color={WHITE}/>
                </View>
                <Text color={PURPLE} centered weight={400}>
                  Add new music/video
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        <LinearGradient
          colors={isValid.buttonColor}
          start={[0, 0]} end={[1, 0]}
          style={{ width: WP100, padding: WP4, flexGrow: 0 }}
        >
          <TouchableOpacity
            disabled={isValid.disabled}
            onPress={async () => {
              navigateTo('CreateArtistPage3', { artistData })
            }}
          >
            <Text color={WHITE} centered weight={500}>
              NEXT
            </Text>
          </TouchableOpacity>
        </LinearGradient>

      </Container>
    )
  }
}

CreateArtistPage2.propTypes = {}

CreateArtistPage2.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(CreateArtistPage2),
  mapFromNavigationParam
)
