import React, { Component } from 'react'
import { BackHandler, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { cloneDeep } from 'lodash-es'
import { LinearGradient } from 'expo-linear-gradient'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { GREY, GREY_PLACEHOLDER, WHITE } from '../../constants/Colors'
import Text from '../../components/Text'
import Container from '../../components/Container'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { WP10, WP100, WP4, WP5 } from '../../constants/Sizes'
import InputTextLight from '../../components/InputTextLight'
import ImagePromoteForm from '../../components/promote/ImagePromoteForm'
import { getCity } from '../../actions/api'
import InputModal from '../../components/InputModal'
import GenrePromoteForm from '../../components/promote/GenrePromoteForm'
import ArtistPromoteFrom from '../../components/promote/ArtistPromoteFrom'
import SocmedPromoteForm from '../../components/promote/SocmedPromoteForm'
import { invalidButtonStyle, validButtonStyle } from '../../components/promote/PromoteConstant'
import { ArtistPageSteps } from '../../components/StepCircle'
import { isPremiumUser } from '../../utils/helper'
import BackModal from '../../components/BackModal'
import HeaderNormal from '../../components/HeaderNormal'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  artistData: getParam('artistData', {})
})

class CreateArtistPage1 extends Component {
  _didFocusSubscription
  _willBlurSubscription

  constructor(props) {
    super(props)
    this.state = {
      artistData: {},
      invalidField: {},
      backModal: false
    }
    this._didFocusSubscription = props.navigation.addListener('focus', (payload) =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler)
    )
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('blur', (payload) =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
    )
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _isEmpty() {
    const { artistData } = this.state

    return !(artistData.artist_picture ||
      artistData.banner_picture ||
      artistData.title ||
      artistData.genre ||
      artistData.members ||
      artistData.location ||
      artistData.phone ||
      artistData.description ||
      artistData.social_media)

  }

  _backHandler = async () => {
    const { navigateBack } = this.props
    const { backModal } = this.state
    if (this._isEmpty()) {
      navigateBack()
    } else {
      this.setState({ backModal: !backModal })
    }
  }

  _isValid = () => {
    const { artistData } = this.state

    if (!artistData.artist_picture) return invalidButtonStyle
    if (!artistData.banner_picture) return invalidButtonStyle
    if (!artistData.title) return invalidButtonStyle
    if (!artistData.genre) return invalidButtonStyle
    if (!artistData.members) return invalidButtonStyle
    if (!artistData.location) return invalidButtonStyle
    if (!artistData.phone) return invalidButtonStyle
    if (!artistData.description) return invalidButtonStyle
    if (!artistData.social_media) return invalidButtonStyle

    return validButtonStyle
  }

  _onChange = (key) => (value) => {
    const updatedArtistData = cloneDeep(this.state.artistData)
    updatedArtistData[key] = value
    this.setState({ artistData: updatedArtistData })
  }

  render() {
    const {
      navigateTo,
      navigateBack,
      userData
    } = this.props

    const {
      artistData,
      backModal
    } = this.state

    const isValid = this._isValid()

    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._backHandler}
            text='Create Artist Page'
            centered
          />
        )}
      >
        <BackModal
          modalVisible={backModal}
          navigateBack={navigateBack}
          dismiss={() => this.setState({ backModal: false })}
        />

        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ backgroundColor: WHITE }}
          style={{ flexGrow: 1 }}
        >

          <ArtistPageSteps index={1} isPremium={isPremiumUser(userData.account_type)}/>

          <View style={{ paddingHorizontal: WP5, paddingBottom: WP10 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ width: wp('27%') }}>
                <ImagePromoteForm
                  required
                  label='Artist Picture'
                  image={artistData.artist_picture}
                  onChange={this._onChange('artist_picture')}
                  aspectRatio={[1, 1]}
                  imageAspectRatio={1}
                />
              </View>

              <View style={{ width: wp('54%') }}>
                <ImagePromoteForm
                  required
                  label='Page Banner'
                  image={artistData.banner_picture}
                  onChange={this._onChange('banner_picture')}
                  aspectRatio={[2, 1]}
                  imageAspectRatio={2}
                />
              </View>
            </View>

            <InputTextLight
              required
              value={artistData.title}
              label='Artist Name'
              labelWeight={400}
              labelColor={GREY}
              placeholderTextColor={GREY_PLACEHOLDER}
              placeholder='Your band, group, or solo name'
              onChangeText={this._onChange('title')}
            />

            <GenrePromoteForm
              required
              genre={artistData.genre}
              onChange={this._onChange('genre')}
            />

            <ArtistPromoteFrom
              required
              members={artistData.members}
              onChange={this._onChange('members')}
            />

            <InputModal
              triggerComponent={(
                <InputTextLight
                  required
                  value={artistData.location ? artistData.location.city_name : ''}
                  label='City'
                  labelWeight={400}
                  labelColor={GREY}
                  placeholderTextColor={GREY_PLACEHOLDER}
                  placeholder='Select city...'
                  editable={false}
                />
              )}
              header='Select City'
              suggestion={getCity}
              suggestionKey='city_name'
              suggestionPathResult='city_name'
              suggestionPathValue='id_city'
              onChange={this._onChange('location')}
              suggestionCreateNewOnEmpty={false}
              selected={artistData.location ? artistData.location.city_name : ''}
              asObject={true}
              createNew={false}
              placeholder='Select city here...'
            />

            <InputTextLight
              required
              value={artistData.phone}
              label='Phone Number'
              labelWeight={400}
              labelColor={GREY}
              keyboardType='phone-pad'
              placeholder='08-xxx-xxx-xxx'
              placeholderTextColor={GREY_PLACEHOLDER}
              onChangeText={this._onChange('phone')}
            />

            <InputTextLight
              required
              value={artistData.description}
              label='About You'
              multiline={true}
              size='slight'
              labelWeight={400}
              labelColor={GREY}
              placeholderTextColor={GREY_PLACEHOLDER}
              placeholder='Tell us more about you'
              onChangeText={this._onChange('description')}
            />

            <SocmedPromoteForm
              required
              socialMedia={artistData.social_media}
              onChange={this._onChange('social_media')}
            />
          </View>
        </KeyboardAwareScrollView>

        <LinearGradient
          colors={isValid.buttonColor}
          start={[0, 0]} end={[1, 0]}
          style={{ width: WP100, padding: WP4, flexGrow: 0 }}
        >
          <TouchableOpacity
            disabled={isValid.disabled}
            onPress={async () => {
              navigateTo('CreateArtistPage2', { artistData })
            }}
          >
            <Text color={WHITE} centered weight={500}>
              NEXT
            </Text>
          </TouchableOpacity>
        </LinearGradient>

      </Container>
    )
  }
}

CreateArtistPage1.propTypes = {}

CreateArtistPage1.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(CreateArtistPage1),
  mapFromNavigationParam
)
