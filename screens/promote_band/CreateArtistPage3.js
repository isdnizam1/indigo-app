import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { cloneDeep, noop } from 'lodash-es'
import { GREY, GREY50, GREY80, ORANGE, WHITE } from '../../constants/Colors'
import Text from '../../components/Text'
import Container from '../../components/Container'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { HP100, WP1, WP100, WP2, WP3, WP4, WP45, WP50, WP6, WP8, WP80 } from '../../constants/Sizes'
import { postPromoteBand, postLogPremium } from '../../actions/api'
import Modal from '../../components/Modal'
import Button from '../../components/Button'
import Image from '../../components/Image'
import { formatGenreList, isIOS, isPremiumUser } from '../../utils/helper'
import { DetailSection, formatMember, workIcon } from '../../components/promote/PromoteComponent'
import { promoteStyle } from '../../components/promote/PromoteConstant'
import { ArtistPageSteps } from '../../components/StepCircle'
import Loader from '../../components/Loader'
import { BORDER_STYLE } from '../../constants/Styles'
import SubmitModal from '../../components/SubmitModal'
import HeaderNormal from '../../components/HeaderNormal'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  artistData: getParam('artistData', {}),
})

class CreateArtistPage3 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      artistData: this.props.artistData,
      backModal: false,
      loading: false,
    }
  }

  _getListSongId = (songs) => {
    const songIds = []
    songs.map((song) => {
      songIds.push(song.id_journey)
    })
    return songIds
  }
  _getListMemberId = (members) => {
    const memberIds = []
    members.map((member) => {
      memberIds.push(member.name)
    })
    return memberIds
  }

  _onSubmit = (toggleModal) => async () => {
    const {
      navigateTo,
      dispatch, userData
    } = this.props

    const artistData = cloneDeep(this.state.artistData)
    artistData.description = artistData.description.replace(/\n/g, '<br>')

    await this.setState({ loading: true })
    artistData.songs = this._getListSongId(artistData.songs)
    artistData.members = this._getListMemberId(artistData.members)
    artistData.id_user = userData.id_user
    artistData.location_name = artistData.location.city_name
    delete artistData.location
    await dispatch(postPromoteBand, artistData, noop, true, true)
    this.setState({ loading: false })

    if(isPremiumUser(userData.account_type) || isIOS()) {
      toggleModal()
    } else {
      dispatch(postLogPremium, { id_user: userData.id_user, previous_screen: 'Create Artist' }, noop, true, true)
      navigateTo('CreateArtistPage4', { artistData })
    }

  }

  _renderPopUpMessage = () => {
    if (isIOS()) return (
      <Text size='mini' color={GREY} centered style={{ marginBottom: WP2 }}>Your artist page submission is success. We will review it first and contact you before we put in the Artist Spotlight Page.</Text>
    )
    return (
      <Text size='mini' color={GREY} centered style={{ marginBottom: WP2 }}>Your artist page submission is success. We will review it first before we put it in the Artist Spotlight Page</Text>
    )
  }

  render() {
    const {
      navigateBack,
      resetNavigation,
      userData
    } = this.props

    const {
      artistData, loading, backModal
    } = this.state

    return (
      <Container
        scrollable
        scrollBackgroundColor={WHITE}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text='Create Artist Page'
            centered
          />
        )}
        outsideScrollContent={() => {
          return (
            <Modal
              isVisible={false}
              position='bottom'
              closeOnBackdrop={true}
              swipeDirection='none'
              renderModalContent={({ toggleModal, payload }) => (
                <View style={{ paddingHorizontal: WP6, paddingVertical: WP4 }}>
                  <Image
                    source={require('../../assets/images/promoteArtistPreview.png')}
                    imageStyle={{ width: WP80 }}
                    aspectRatio={2.083}
                  />
                  <Text size='massive' weight={600} color={GREY} centered style={{ marginBottom: WP2 }}>Hurraaay !</Text>
                  {
                    this._renderPopUpMessage()
                  }

                  <Button
                    soundfren
                    rounded
                    style={{ flexGrow: 0, paddingHorizontal: WP6, width: undefined }}
                    onPress={() => {
                      toggleModal()
                      resetNavigation('ExploreScreen')
                      this.props.navigation.navigate('ExploreScreen')
                    }}
                    textCentered
                    centered
                    text='THANK YOU!'
                    textSize='mini'
                    textWeight={400}
                  />
                </View>
              )}
            >
              {({ toggleModal }, M) => (
                <View>
                  <SubmitModal
                    modalVisible={backModal}
                    submit={this._onSubmit(toggleModal)}
                    dismiss={() => this.setState({ backModal: false })}
                  />
                  <Button
                    onPress={() => this.setState({ backModal: true })}
                    style={{ marginVertical: 0 }}
                    centered
                    soundfren
                    radius={0}
                    width='100%'
                    text={isPremiumUser(userData.account_type) || isIOS() ? 'SUBMIT' : 'NEXT'}
                    textType='SansPro'
                    textWeight={500}
                    shadow='none'
                  />
                  {M}
                </View>
              )}

            </Modal>
          )
        }}
      >
        {
          loading &&
          <View style={{ position: 'absolute', width: WP100, height: HP100, backgroundColor: GREY50, zIndex: 999 }}>
            <Loader isLoading />
          </View>
        }
        <ArtistPageSteps index={3} isPremium={isPremiumUser(userData.account_type)}/>

        <View style={{ marginBottom: WP6, backgroundColor: 'rgb(250,250,250)' }}>
          <View style={{ paddingHorizontal: WP6 }}>
            <Text size='extraMassive' weight={500} color={ORANGE}>Preview</Text>
            <Text>Check it out! Below is how your Artist Page will be displayed to the world!</Text>
          </View>
        </View>

        <View style={{ flex: 1, paddingHorizontal: WP6 }}>

          <View style={promoteStyle.sectionPreview}>
            <Text size='large' weight={500} color={GREY} style={{ marginBottom: WP3 }}>Thumbnail View</Text>
            <View style={{ width: WP50, alignSelf: 'center' }}>
              <Image
                aspectRatio={1}
                imageStyle={{ width: WP45 }}
                source={{ uri: `data:image/gif;base64,${artistData.artist_picture}` }}
              />
              <Text size='large' weight={400} color={GREY} centered>{artistData.title}</Text>
              <Text size='mini' color={GREY80} centered>{formatGenreList(artistData.genre)}</Text>
            </View>
          </View>

          <View>
            <Text size='large' weight={500} color={GREY} style={{ marginBottom: WP3 }}>Detail View</Text>
            <Image
              style={{ marginHorizontal: -(WP6), marginBottom: WP2 }}
              aspectRatio={2}
              imageStyle={{ width: WP100 }}
              source={{ uri: `data:image/gif;base64,${artistData.banner_picture}` }}
            />

            <View style={[promoteStyle.sectionPreview]}>
              <Text size='massive' weight={500} color={GREY} style={{ marginBottom: WP2 }}>{artistData.title}</Text>
              <DetailSection label='genre' content={formatGenreList(artistData.genre)}/>
              <DetailSection label='members' content={formatMember(artistData.members)} contentColor={ORANGE}/>
              <DetailSection label='Location' content={artistData.location.city_name}/>
            </View>

            <View style={[promoteStyle.sectionPreview]}>
              <Text size='medium' weight={400} color={GREY} style={promoteStyle.sectionHeader}>{`About ${artistData.title}`}</Text>
              <Text size='slight' color={GREY}>{artistData.description}</Text>
            </View>

            <View style={[promoteStyle.sectionPreview]}>
              <Text size='medium' weight={400} color={GREY} style={promoteStyle.sectionHeader}>Songs</Text>
              {
                artistData.songs.map((item, index) => (
                  <View
                    key={index} style={{ ...BORDER_STYLE['top'], flexDirection: 'row',
                      alignItems: 'center', paddingVertical: WP1,
                    }}
                  >
                    <Image source={workIcon[item.type]} style={{ marginRight: WP3 }} imageStyle={{ height: WP8 }}/>
                    <Text color={GREY} size='slight'>{item.title}</Text>
                  </View>
                ))
              }
            </View>

            <View style={[promoteStyle.sectionPreview]}>
              <Text size='medium' weight={400} color={GREY} style={promoteStyle.sectionHeader}>Find Us on</Text>
              {
                artistData.social_media.map((item, index) => (
                  <View key={index} style={{ flexDirection: 'row' }}>
                    <Text size='slight' color={GREY} style={{ marginRight: WP3 }}>{item.name}</Text>
                    <Text size='slight' color={ORANGE}>{item.username}</Text>
                  </View>
                ))
              }
            </View>
          </View>
        </View>
      </Container>
    )
  }
}

CreateArtistPage3.propTypes = {}

CreateArtistPage3.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(CreateArtistPage3),
  mapFromNavigationParam
)
