import React, { Component } from 'react'
import { Keyboard, TouchableOpacity, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import moment from 'moment'
import { connect } from 'react-redux'
import { cloneDeep, isEmpty, lowerCase, map, noop, upperCase } from 'lodash-es'
import Icon from '../../components/Icon'
import { GREY, ORANGE_BRIGHT, PINK_PURPLE, PURPLE, SILVER, SILVER_WHITE, WHITE } from '../../constants/Colors'
import Text from '../../components/Text'
import Container from '../../components/Container'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { HP1, HP2, WP1, WP10, WP2, WP20, WP4, WP6 } from '../../constants/Sizes'
import { getSettingDetail, getSubscriptionVoucher } from '../../actions/api'
import Button from '../../components/Button'
import { ArtistPageSteps } from '../../components/StepCircle'
import { BORDER_COLOR, BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'
import InputTextLight from '../../components/InputTextLight'
import { currencyFormatter } from '../../utils/helper'
import { DEFAULT_PAGING } from '../../constants/Routes'
import { Image, Modal } from '../../components'
import HeaderNormal from '../../components/HeaderNormal'
import { paymentDispatcher } from '../../services/payment'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet
}

const mapFromNavigationParam = (getParam) => ({
  artistData: getParam('artistData', {})
})

const subscription = {
  'per_1_month': {
    key: 'per_1_month',
    name: '1 month',
    subscriptionDurationInMonth: 1
  },
  'per_2_month': {
    key: 'per_2_month',
    name: '2 month',
    subscriptionDurationInMonth: 2
  }
}

class CreateArtistPage4 extends Component {
  state = {
    artistData: this.props.artistData,
    // artistData: data,
    isSelectedMonth: 'per_1_month',
    costPerMonth: 0,
    voucherCodeInput: null,
    voucherCode: null,
    voucherDiscount: 0,
    selectedPackage: {}
  }

  async componentDidMount() {
    await this._getInitialScreenData()
  }

  _getInitialScreenData = async (...args) => {
    await this._getData(this.state.isSelectedMonth)
  }

  _getData = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const {
      dispatch
    } = this.props

    const dataResponse = await dispatch(getSettingDetail, {
      'setting_name': 'premium_member',
      'key_name': 'package'
    }, noop, false, isLoading)
    const packages = JSON.parse(dataResponse.value)
    this.setState({
      packages,
      selectedPackage: packages[0]
    })
  }

  _getVoucher = async () => {
    const {
      dispatch
    } = this.props

    const {
      voucherCode
    } = this.state

    if (!isEmpty(voucherCode)) {
      const dataResponse = await dispatch(getSubscriptionVoucher, { voucher_code: voucherCode, type: 'premium_user' }, noop, false, true)
      this.setState({
        voucherDiscount: Number(dataResponse.amount_discount || 0)
      })
    }
  }

  _getListSongId = (songs) => {
    const songIds = []
    songs.map((song) => {
      songIds.push(song.id_journey)
    })
    return songIds
  }
  _getListMemberId = (members) => {
    const memberIds = []
    members.map((member) => {
      memberIds.push(member.name)
    })
    return memberIds
  }

  _onSelectedPackage = (selectedPackage) => {
    this.setState({
      selectedPackage
    })
  }

  _onSubmit = async () => {
    const {
      navigateTo,
      userData,
      paymentSourceSet
    } = this.props

    const artistData = cloneDeep(this.state.artistData)
    artistData.songs = this._getListSongId(artistData.songs)
    artistData.members = this._getListMemberId(artistData.members)
    artistData.id_user = userData.id_user
    artistData.location_name = artistData.location.city_name
    artistData.voucher_code = this.state.voucherCode
    artistData.quantity = (subscription[this.state.isSelectedMonth]).subscriptionDurationInMonth
    artistData.type = lowerCase(this.state.selectedPackage.package_name)
    delete artistData.location
    paymentSourceSet('artist_showcase')
    navigateTo('CreateArtistPage5', { artistData })
  }

  render() {
    const {
      navigateBack,
      navigateTo,
      isLoading
    } = this.props

    const {
      isSelectedMonth,
      selectedPackage,
      voucherDiscount,
      voucherCodeInput
    } = this.state

    const selectedSubscription = subscription[isSelectedMonth]
    const currentCost = Number(selectedPackage.price) * selectedSubscription.subscriptionDurationInMonth

    return (
      <Container
        scrollBackgroundColor={SILVER_WHITE}
        scrollable
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text='Create Artist Page'
            centered
          />
        )}
        outsideScrollContent={() => (
          <Button
            onPress={this._onSubmit}
            style={{ marginVertical: 0 }}
            centered
            soundfren
            radius={0}
            width='100%'
            text='PROCEED TO PAYMENT'
            textType='SansPro'
            textWeight={500}
            shadow='none'
          />
        )}
      >
        <ArtistPageSteps index={4}/>
        <View style={{
          paddingVertical: HP1, paddingHorizontal: WP6, backgroundColor: WHITE,
          marginVertical: HP1, flexDirection: 'row', justifyContent: 'space-between',
          alignItems: 'center'
        }}
        >
          <View>
            <Text size='mini' weight={500}>Premium Membership Plan</Text>
            <Text color={PURPLE} size='extraMassive' weight={500}>{selectedPackage.package_name}</Text>
          </View>
          <Text
            onPress={() => navigateTo('PackageScreen', { onSelectedPackage: this._onSelectedPackage, selectedPackage })}
            type='SansPro' color={ORANGE_BRIGHT} weight={500}
          >
            CHANGE
          </Text>
        </View>
        <View style={{ paddingVertical: HP1, paddingHorizontal: WP6, backgroundColor: WHITE, marginBottom: HP1 }}>
          <Text size='mini' weight={500}>How long do you want your artist page to be displayed ?</Text>
          <View style={{ justifyContent: 'center', flexDirection: 'row', marginVertical: HP1 }}>
            {
              map(subscription, (sub, key) => (
                <TouchableOpacity
                  key={key}
                  activeOpacity={TOUCH_OPACITY}
                  onPress={() => {
                    this.setState({ isSelectedMonth: key })
                  }}
                >
                  <LinearGradient
                    colors={isSelectedMonth === key ? [PURPLE, PINK_PURPLE] : [WHITE, WHITE]}
                    start={[0, .3]}
                    end={[1, 1.4]}
                    locations={[.2, 1]}
                    style={{
                      padding: WP10, borderRadius: WP4, marginHorizontal: WP2, borderColor: BORDER_COLOR,
                      borderWidth: isSelectedMonth === key ? 0 : BORDER_WIDTH
                    }}
                  >
                    <Text color={isSelectedMonth === key ? WHITE : undefined} size='mini' weight={500}>{sub.name}</Text>
                  </LinearGradient>
                </TouchableOpacity>
              ))
            }
          </View>
        </View>
        <View style={{
          paddingVertical: HP1, paddingHorizontal: WP6, backgroundColor: WHITE, marginBottom: HP1, flexDirection: 'row',
          alignItems: 'center'
        }}
        >
          <InputTextLight
            label='Voucher Code'
            labelWeight={500}
            labelColor={GREY}
            placeholder='Enter voucher code'
            style={{ flex: 1 }}
            value={voucherCodeInput}
            onChangeText={(text) => {
              this.setState({ voucherCodeInput: text })
            }}
          />
          <Button
            onPress={async () => {
              Keyboard.dismiss()
              await this.setState((state) => ({
                voucherCodeInput: null,
                voucherCode: state.voucherCodeInput
              }))
              this._getVoucher()
            }}
            style={{ marginLeft: WP2 }}
            compact='center'
            rounded
            centered
            soundfren
            width={WP20}
            text='APPLY'
            textSize='mini'
            textType='SansPro'
            textWeight={500}
            shadow='none'
          />
        </View>
        <View style={{
          paddingVertical: HP1, paddingHorizontal: WP6, backgroundColor: WHITE, marginBottom: HP1
        }}
        >
          <View style={{ marginBottom: HP1 }}>
            <Text size='mini' weight={500}>Amount to be paid</Text>
            {
              voucherDiscount > 0 && (
                <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: WP1 }}>
                  <Text
                    style={{
                      textDecorationColor: ORANGE_BRIGHT,
                      textDecorationLine: 'line-through',
                      textDecorationStyle: 'solid'
                    }} size='mini'
                  >{selectedPackage.currency_price} {currentCost}</Text>
                  <View style={{
                    backgroundColor: SILVER,
                    paddingHorizontal: WP1,
                    borderRadius: 4,
                    marginLeft: WP2,
                    flexDirection: 'row'
                  }}
                  >
                    <Text size='mini' weight={500} color={WHITE}>Disc. {voucherDiscount}%</Text>
                    <Icon
                      onPress={() => {
                        this.setState({
                          voucherCode: null,
                          voucherDiscount: 0
                        })
                      }} style={{ marginLeft: WP2 }} centered color={WHITE} name='close' size='mini'
                    />
                  </View>
                </View>
              )
            }
            <Text
              size='huge' weight={500}
              color={ORANGE_BRIGHT}
            >{selectedPackage.currency_price} {currencyFormatter(currentCost - ((currentCost * voucherDiscount) / 100))}</Text>
          </View>
          <Text size='tiny'>
            {'You will also get premium membership for '}
            {<Text size='tiny' weight={500}>{selectedSubscription.name}</Text>}
            {' from '}
            {<Text size='tiny' weight={500}>{moment().format('DD/MM/YYYY')}</Text>}
            {' until '}
            {<Text size='tiny' weight={500}>
              {
                moment().add(selectedSubscription.subscriptionDurationInMonth, 'month').format('DD/MM/YYYY')
              }
            </Text>}
          </Text>
          <Modal
            position='bottom'
            style={{ borderTopRightRadius: 0, borderTopLeftRadius: 0 }}
            renderModalContent={(toggleModal, payload) => (
              <View style={{ paddingHorizontal: WP6, paddingVertical: WP4 }}>
                <Text centered type='SansPro' weight={500}>{upperCase(selectedPackage.package_name)} PLAN BENEFITS
                  !</Text>
                <View style={{ marginTop: HP2 }}>
                  {
                    map(selectedPackage.benefit, (bene) => (
                      <View style={{ flexDirection: 'row', marginVertical: HP1, justifyContent: 'center' }}>
                        <Image size='tiny' source={{ uri: bene.icon }}/>
                        <Text style={{ flex: 1, marginLeft: WP4 }} size='tiny'>{bene.name}</Text>
                      </View>
                    ))
                  }
                </View>
              </View>
            )}
          >
            {({ toggleModal }, M) => (
              <View>
                <Button
                  onPress={toggleModal}
                  centered
                  shadow='none'
                  textSize='mini'
                  textType='SansPro'
                  textWeight={500}
                  textColor={ORANGE_BRIGHT}
                  text={`SEE ${upperCase(selectedPackage.package_name)} PLAN BENEFITS`}
                />
                {M}
              </View>
            )}
          </Modal>
        </View>
      </Container>
    )
  }
}

CreateArtistPage4.propTypes = {}

CreateArtistPage4.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateArtistPage4),
  mapFromNavigationParam
)
