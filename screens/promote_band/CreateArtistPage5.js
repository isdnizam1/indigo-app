import React, { Component } from 'react'
import { WebView } from 'react-native-webview'
import { connect } from 'react-redux'
import Container from '../../components/Container'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import env from '../../utils/env'
import HeaderNormal from '../../components/HeaderNormal'
import { paymentDispatcher } from '../../services/payment'

const mapStateToProps = ({ auth, payment }) => ({
  userData: auth.user,
  paymentSource: payment.paymentSource
})

const mapDispatchToProps = {
  paymentSourceClear: paymentDispatcher.paymentSourceClear
}

const mapFromNavigationParam = (getParam) => ({
  artistData: getParam('artistData', {}),
})

class CreateArtistPage5 extends Component {
  state = {
    isLoading: true
  }
  render() {
    const {
      userData,
      artistData,
      navigateTo,
      navigateBack,
      isLoading: isLoadingProps,
      paymentSource
    } = this.props
    const {
      isLoading
    } = this.state
    const paymentUrl = `${env.webUrl}/payment/vtweb/vtweb_checkout`
    const queries = `?id_user=${userData.id_user}` +
      `&quantity=${artistData.quantity}` +
      `&voucher_code=${artistData.voucher_code || ''}` +
      `&full_name=${userData.full_name}` +
      `&email=${userData.email}` +
      `&phone=${artistData.phone_number}` +
      `&type=${artistData.type}` +
      '&payment_type=promote_artist' +
      `&source_from=${paymentSource}`
    return (
      <Container
        isLoading={isLoading || isLoadingProps}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text='Premium Membership'
            centered
          />
        )}
      >
        <WebView
          source={{ uri: paymentUrl + queries }}
          onLoadStart={() => {
            this.setState({ isLoading: true })
          }}
          onLoadEnd={() => {
            this.setState({ isLoading: false })
          }}
          onNavigationStateChange={async (state) => {
            if(state.url.includes('payment/finish')) {
              // await dispatch(postPromoteBand, artistData, noop, false, true)
              this.props.paymentSourceClear()
              this.props.navigation.popToTop()
              navigateTo('NotificationStack')
            }
          }}
        />
      </Container>
    )
  }
}

CreateArtistPage5.propTypes = {}

CreateArtistPage5.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateArtistPage5),
  mapFromNavigationParam
)
