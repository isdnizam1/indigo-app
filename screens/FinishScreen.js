import React from 'react'
import { connect } from 'react-redux'
import { BackHandler, View } from 'react-native'
import { noop } from 'lodash-es'
import { fromBottom } from 'react-navigation-transitions'
import {
  _enhancedNavigation,
  Container,
  Button,
  Image, Text
} from '../components'
import { HP1, HP2, HP4, WP10, WP4, WP6 } from '../constants/Sizes'
import { BLACK, GREY_WARM, ORANGE, WHITE } from '../constants/Colors'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  image: getParam('image', require('../assets/images/hurray.png')),
  imageAspectRatio: getParam('imageAspectRatio', 271 / 152),
  title: getParam('title', 'Hurray!'),
  message: getParam('message', 'Your did a great activities, we will record you as a good person in the world'),
  paddingHorizontal: getParam('paddingHorizontal', WP6),
  centered: getParam('centered', false),
  buttonText: getParam('buttonText', 'Back'),
  buttonTextColor: getParam('buttonTextColor', WHITE),
  buttonAction: getParam('buttonAction', noop),
  buttonBottom: getParam('buttonBottom', true),
  buttonColor: getParam('buttonColor', ORANGE)
})

class FinishScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    transitionConfig: () => fromBottom()
  })

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.props.buttonAction)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.props.buttonAction)
  }

  _renderCenteredContent(element) {
    const { centered } = this.props
    if (centered) {
      return (
        <View>
          {element}
        </View>
      )
    } else return element
  }

  render() {
    const {
      image,
      imageAspectRatio,
      title,
      message,
      centered,
      paddingHorizontal,
      buttonText,
      buttonTextColor,
      buttonAction,
      buttonBottom,
      buttonColor
    } = this.props
    return (
      <Container style={{ justifyContent: 'space-between', flex: 1 }}>
        {
          this._renderCenteredContent(
            <>
              <Image
                source={image}
                style={{ paddingHorizontal, justifyContent: centered ? 'flex-end' : 'center' }}
                imageStyle={{ width: '100%' }}
                aspectRatio={imageAspectRatio}
              />
              <View style={{ paddingHorizontal: WP10, justifyContent: centered ? 'flex-start' : 'center' }}>
                <Text centered={centered} color={BLACK} weight={500} type='NeoSans' size='extraMassive' style={{ marginVertical: HP2, marginTop: centered ? HP4 : HP2 }}>
                  {title}
                </Text>
                <View style={{ marginVertical: HP1 }}>
                  <Text centered={centered} color={GREY_WARM} size='xmini' style={{ marginBottom: HP2 }}>
                    {message}
                  </Text>
                </View>
              </View>
            </>
          )
        }
        <Button
          style={{ paddingHorizontal: buttonBottom ? 0 : WP4 }}
          bottomButton
          marginless={buttonBottom}
          onPress={buttonAction}
          backgroundColor={buttonColor}
          centered
          shadow='none'
          radius={buttonBottom ? undefined : WP4}
          textType='NeoSans'
          textSize='small'
          textColor={buttonTextColor}
          textWeight={500}
          text={buttonText}
        />
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(FinishScreen),
  mapFromNavigationParam
)
