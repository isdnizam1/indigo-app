/* eslint-disable react/sort-comp */
import React from "react";
import { Easing, BackHandler, StyleSheet, View, Platform } from "react-native";
import { connect } from "react-redux";
import { sortBy, get, isEmpty } from "lodash-es";
import { _enhancedNavigation, Container, HeaderNormal } from "sf-components";
import {
  TRANSPARENT,
  NAVY_DARK,
  GUN_METAL,
  SHIP_GREY_CALM,
  PALE_BLUE,
  SHIP_GREY,
  WHITE,
  SHADOW_GRADIENT,
  REDDISH,
  REDDISH_DISABLED,
} from "sf-constants/Colors";
import { WP2, WP105, WP3, WP5, WP15, WP20 } from "sf-constants/Sizes";
import Image from "sf-components/Image";
import Icon from "sf-components/Icon";
import Text from "sf-components/Text";
import ButtonV2 from "sf-components/ButtonV2";
import Touchable from "sf-components/Touchable";
import { getListTopics, postJourney } from "sf-actions/api";
import { LinearGradient } from "expo-linear-gradient";
import { HEADER } from "sf-constants/Styles";
import produce from "immer";
import Collapsible from "react-native-collapsible";
import {
  setProgressComponent,
  setProgressComplete,
} from "sf-services/helper/actionDispatcher";
import { shouldReloadFeed } from "../services/timeline/actionDispatcher";
import { showReviewIfNotYet } from "../utils/review";

const style = StyleSheet.create({
  actionBarLeft: {
    width: WP15,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 0,
    paddingRight: 0,
    paddingVertical: WP3,
    marginRight: WP5,
  },
  actionBarRight: {
    width: WP20,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 0,
    paddingRight: 0,
    paddingVertical: WP3,
  },
  topics: {
    flexDirection: "row",
    flexWrap: "wrap",
    padding: WP5,
  },
  topic: {
    paddingVertical: WP105,
    paddingHorizontal: WP3,
    marginRight: WP3,
    marginBottom: WP3,
  },
  selectedTopics: {
    padding: WP5,
    backgroundColor: "rgb(244, 246, 248)",
    borderTopWidth: 1,
    borderTopColor: PALE_BLUE,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  selectedTopic: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: WHITE,
    borderRadius: WP2,
    marginVertical: WP105,
    paddingTop: WP3,
    paddingBottom: WP3,
    paddingLeft: WP3,
    borderWidth: Platform.select({ ios: 1, android: 0 }),
    borderColor: Platform.select({ ios: PALE_BLUE, android: TRANSPARENT }),
    elevation: Platform.select({ ios: 0, android: 10 }),
  },
  selectedTopicDescription: {
    paddingLeft: WP5,
    flex: 1,
  },
  removeTopic: {
    padding: WP5,
  },
  sectionHeader: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: WP5,
    marginTop: WP5 + WP3,
  },
  progressAnim: {
    height: "100%",
    paddingHorizontal: WP5,
    flexDirection: "row",
    alignItems: "center",
  },
});

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapFromNavigationParam = (getParam) => ({
  refreshFeeds: getParam("refreshFeeds", () => {}),
  description: getParam("description"),
  is1000Startup: getParam("is1000Startup", false),
  role: getParam("role"),
  images: getParam("images", {}),
});

class FeedPostTopic extends React.Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props);
    this.state = {
      topics: [],
      selected: [],
      selectedIds: [],
      isLoading: false,
    };
    this._didFocusSubscription = props.navigation.addListener(
      "focus",
      (payload) =>
        BackHandler.addEventListener("hardwareBackPress", this._backHandler)
    );
    this._willBlurSubscription = this.props.navigation.addListener(
      "blur",
      (payload) =>
        BackHandler.removeEventListener("hardwareBackPress", this._backHandler)
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  componentDidMount() {
    const {
      dispatch,
      userData: { id_user },
    } = this.props;
    dispatch(getListTopics, { id_user, show: true }).then((result) => {
      const topics = sortBy(
        result
          .filter(
            ({ topic_name }) => ["News", "Trending"].indexOf(topic_name) == -1
          )
          .map(({ id_topic, topic_name, description, image }) => {
            const priority = [
              "8",
              "6",
              "4",
              "2",
              "5",
              "3",
              "9",
              "7",
              "11",
              "15",
            ].indexOf(id_topic);
            return {
              id_topic,
              topic_name,
              description,
              image,
              priority: priority >= 0 ? priority : 100,
            };
          }),
        "priority"
      );
      topics.length % 2 != 0 && topics.push({ id_topic: null });
      this.setState({ isLoading: false, topics }, () => {});
    });
  }

  componentWillUnmount() {}

  _backHandler = async () => {
    const { navigateBack } = this.props;
    navigateBack();
  };

  _nextStep = () => {
    const {
      description,
      refreshFeeds,
      images: { b64: image, name: image_name },
      userData: { id_user, total_post },
      navigation: { popToTop, jumpTo },
      setProgressComponent,
      setProgressComplete,
      shouldReloadFeed,
      navigateTo,
      navigateBack,
      is1000Startup,
    } = this.props;
    const { selectedIds: id_topic } = this.state;
    setProgressComplete(false);
    setProgressComponent(
      <View style={style.progressAnim}>
        <Text
          weight={400}
          color={SHIP_GREY_CALM}
          size={"mini"}
          type={"Circular"}
        >
          Publish post...
        </Text>
      </View>
    );
    postJourney({
      description,
      id_user,
      image,
      image_name,
      id_topic,
      role: this.props.role.toLowerCase(),
    })
      .then((dataResponse) => {
        setProgressComplete(true);
        setProgressComponent(
          <View style={style.progressAnim}>
            <Text
              weight={400}
              color={SHIP_GREY_CALM}
              size={"mini"}
              type={"Circular"}
            >
              {"Publish post berhasil  "}
            </Text>
            <Image
              size={"badge"}
              source={require("sf-assets/icons/v3/tickReddishSemiTransparent.png")}
            />
          </View>
        );
        refreshFeeds();
      })
      .catch((err) => {
        setProgressComplete(true);
        refreshFeeds();
      });
    shouldReloadFeed(true);
    if (!is1000Startup) {
      popToTop();
      jumpTo("FeedStack", {});
    } else {
      navigateBack();
      navigateBack();
    }
    if (total_post > 0) setTimeout(() => showReviewIfNotYet(), 2000);
  };

  _rightComponent = () => {
    const enableNext =
      this.state.selectedIds.length > 0 && !this.state.isLoading;
    return (
      <ButtonV2
        onPress={this._nextStep}
        disabled={!enableNext}
        style={style.actionBarRight}
        text={"Publish"}
        borderColor={WHITE}
        textColor={enableNext ? REDDISH : REDDISH_DISABLED}
      />
    );
  };

  _renderHeader = () => {
    const enableNext = this.state.selectedIds.length > 0;
    return (
      <View key={enableNext ? "header-enabled" : "header-disabled"}>
        <HeaderNormal
          noRightPadding
          iconStyle={style.actionBarLeft}
          iconLeftOnPress={this.props.navigateBack}
          style={style.headerNormal}
          centered
          rightComponent={this._rightComponent}
          textType={"Circular"}
          textSize={"slight"}
          text={"Pilih Topik"}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
      </View>
    );
  };

  _selectTopic = (topic) => () => {
    this.setState(
      produce((draft) => {
        draft.selectedIds.push(topic.id_topic);
      })
    );
  };

  _renderTopic = (topic, index) => {
    const { is1000Startup } = this.props;
    const { selectedIds } = this.state;
    const disabled = selectedIds.length >= 2;
    return get(topic, "topic_name") &&
      (!is1000Startup || (is1000Startup && topic.id_topic == 26)) ? (
      <View key={`topic-${topic.id_topic}`}>
        {selectedIds.indexOf(topic.id_topic) == -1 ? (
          <ButtonV2
            disabled={disabled}
            onPress={this._selectTopic(topic)}
            style={style.topic}
            text={topic.topic_name}
            textColor={disabled ? PALE_BLUE : SHIP_GREY}
          />
        ) : null}
      </View>
    ) : (
      <View key={Math.random()} />
    );
  };

  _removeTopic = (topic) => () => {
    this.setState(
      produce((draft) => {
        draft.selectedIds.splice(draft.selectedIds.indexOf(topic.id_topic), 1);
      })
    );
  };

  _renderSelectedTopic = (topic, index) => {
    const { selectedIds } = this.state;
    return (
      <View key={`selectedTopic-${topic.id_topic}`}>
        {selectedIds.indexOf(topic.id_topic) >= 0 ? (
          <View style={style.selectedTopic}>
            <View>
              <Image size={"medium"} source={{ uri: topic.image }} />
            </View>
            <View style={style.selectedTopicDescription}>
              <Text
                color={GUN_METAL}
                size={"slight"}
                weight={500}
                type={"Circular"}
              >
                {topic.topic_name}
              </Text>
              <Text color={SHIP_GREY_CALM} size={"xmini"} type={"Circular"}>
                {topic.description}
              </Text>
            </View>
            <View>
              <Touchable
                onPress={this._removeTopic(topic)}
                style={style.removeTopic}
              >
                <Icon color={SHIP_GREY_CALM} name={"close"} size={"large"} />
              </Touchable>
            </View>
          </View>
        ) : null}
      </View>
    );
  };

  render() {
    const { selectedIds } = this.state;
    let { topics } = this.state;
    const { is1000Startup } = this.props;
    if (is1000Startup) {
      topics = topics.filter(
        (topic) =>
          topic.id_topic === "26" || topic.topic_name === "1000 Startup"
      );
    } else {
      topics = topics.filter(
        (topic) =>
          topic.id_topic !== "26" || topic.topic_name !== "1000 Startup"
      );
    }

    return (
      <Container
        scrollable={false}
        renderHeader={this._renderHeader}
        colors={[WHITE, WHITE]}
      >
        <Collapsible easing={Easing.ease} collapsed={selectedIds.length == 0}>
          <View style={style.selectedTopics}>
            {topics.map(this._renderSelectedTopic)}
          </View>
        </Collapsible>
        <View>
          <View style={style.sectionHeader}>
            <Text
              color={NAVY_DARK}
              size={"small"}
              weight={600}
              type={"Circular"}
            >
              Topik Pilihan
            </Text>
            {!this.props.is1000Startup && (
              <Text
                color={GUN_METAL}
                size={"mini"}
                weight={500}
                type={"Circular"}
              >
                {`${selectedIds.length}/2`}
              </Text>
            )}
          </View>
          <View style={style.topics}>
            {isEmpty(topics) ? (
              <View>
                <Text
                  type="Circular"
                  size="small"
                  weight={400}
                  color={SHIP_GREY_CALM}
                >
                  Maaf, untuk saat ini belum tersedia
                </Text>
              </View>
            ) : (
              topics.map(this._renderTopic)
            )}
          </View>
        </View>
      </Container>
    );
  }
}

const mapDispatchToProps = {
  setProgressComponent,
  setProgressComplete,
  shouldReloadFeed,
};

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(FeedPostTopic),
  mapFromNavigationParam
);
