import React from 'react'
import { Keyboard, ActivityIndicator, ScrollView, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { isEmpty, noop } from 'lodash-es'
import * as yup from 'yup'
import axios from 'axios'
import { setClearBlueMessage } from 'sf-services/messagebar/actionDispatcher'
import { WP5 } from 'sf-constants/Sizes'
import { _enhancedNavigation, Container, Form, InputTextLight, Text } from '../components'
import { ORANGE_BRIGHT, PALE_SALMON, REDDISH, SHIP_GREY, SILVER_TWO, WHITE } from '../constants/Colors'
import { WP2, WP4, WP8, WP12, WP18 } from '../constants/Sizes'
import { SHADOW_STYLE, TEXT_INPUT_STYLE, TOUCH_OPACITY } from '../constants/Styles'
import { alertMessage } from '../utils/alert'
import { postProfileAddVideo, putProfileEditVideo } from '../actions/api'
import { authDispatcher } from '../services/auth'
import HeaderNormal from '../components/HeaderNormal'
import { setAddProfile } from '../utils/review'

export const FORM_VALIDATION = yup.object().shape({
  title: yup.string().required(),
  videoUrl: yup.string().required()
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setClearBlueMessage
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', noop),
  initialValues: getParam('initialValues', {}),
  navigateBackOnDone: getParam('navigateBackOnDone', false)
})

class ProfileAddVideoScreen extends React.Component {
  state = {
    isUploading: false
  }

  _onAddVideo = async ({ formData }) => {
    const {
      userData: { id_user },
      navigateBack,
      refreshProfile,
      dispatch,
      initialValues,
      getUserDetailDispatcher
    } = this.props
    const action = isEmpty(initialValues) ? postProfileAddVideo : putProfileEditVideo

    try {
      this.setState({ isUploading: true })
      await axios.get(formData.videoUrl)
      await dispatch(action, {
        id_user,
        id_journey: initialValues.id_journey,
        title: formData.title,
        description: formData.description,
        url_video: formData.videoUrl
      })
      getUserDetailDispatcher({ id_user })
      this.props.setClearBlueMessage(isEmpty(initialValues) ? 'Video berhasil diupload' : 'Video berhasil diperbarui')
      isEmpty(initialValues) && await setAddProfile()
      refreshProfile()
      navigateBack()
    } catch (e) {
      alertMessage('Oops!', 'Please insert valid video url')
      this.setState({ isUploading: false })
    }

  }

  render() {
    const {
      initialValues,
      navigateBack
    } = this.props

    const {
      isUploading
    } = this.state

    return (
      <Container colors={[WHITE, WHITE]}>
        <Form initialValue={initialValues} validation={FORM_VALIDATION} onSubmit={this._onAddVideo}>
          {({ onChange, onSubmit, formData, isValid }) => (
            <View style={{ overflow: 'hidden' }}>
              <HeaderNormal
                iconLeftWrapperStyle={{ width: WP8, marginRight: WP12 }}
                iconLeftOnPress={() => navigateBack()}
                wrapperStyle={{
                  ...SHADOW_STYLE.shadowBold,
                  shadowOffset: {
                    width: 0,
                    height: 10,
                  },
                  shadowOpacity: 0.05,
                  backgroundColor: WHITE,
                  marginBottom: WP8,
                  paddingBottom: WP2
                }}
                textType='Circular'
                textColor={SHIP_GREY}
                textWeight={400}
                textSize={'slight'}
                text='Upload Video'
                centered
                rightComponent={(
                  <View style={{ alignItems: 'flex-end', width: WP18, marginRight: WP5 }}>
                    {!isUploading &&
                      <TouchableOpacity
                        onPress={() => {
                          Keyboard.dismiss()
                          onSubmit()
                        }}
                        activeOpacity={TOUCH_OPACITY}
                        disabled={!isValid}
                      >
                        <Text style={{ textAlign: 'right' }} size='slight' weight={400} color={isValid ? ORANGE_BRIGHT : PALE_SALMON}>Upload</Text>
                      </TouchableOpacity>
                    }
                    {isUploading &&
                      <ActivityIndicator color={REDDISH} />
                    }
                  </View>
                )}
              />

              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1, paddingHorizontal: WP4 }}
              >
                <InputTextLight
                  withLabel={false}
                  placeholder='Masukan link/url video kamu'
                  value={formData.videoUrl}
                  onChangeText={onChange('videoUrl')}
                  placeholderTextColor={SILVER_TWO}
                  color={SHIP_GREY}
                  bordered
                  size='mini'
                  type='Circular'
                  returnKeyType={'next'}
                  wording=' '
                  maxLengthFocus
                  lineHeight={1}
                  style={{ marginTop: 0, marginBottom: 0 }}
                  textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                  labelv2='Link Video'
                  editable={!isUploading}
                />
                <InputTextLight
                  withLabel={false}
                  placeholder='Tuliskan judul video kamu'
                  value={formData.title}
                  onChangeText={onChange('title')}
                  placeholderTextColor={SILVER_TWO}
                  color={SHIP_GREY}
                  bordered
                  size='mini'
                  type='Circular'
                  returnKeyType={'next'}
                  wording=' '
                  maxLengthFocus
                  lineHeight={1}
                  style={{ marginTop: 0, marginBottom: 0 }}
                  textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                  labelv2='Judul Video'
                  editable={!isUploading}
                />
              </ScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProfileAddVideoScreen),
  mapFromNavigationParam
)
