import React from "react";
import { Platform, View } from "react-native";
import Constants from "expo-constants";
import { isEqual, pick } from "lodash-es";
import { connect } from "react-redux";
import Image from "sf-components/Image";
import Text from "sf-components/Text";
import Container from "sf-components/Container";
import { clearLocalStorage } from "../../../utils/storage";
import {
  NAVY_DARK,
  REDDISH,
  SHIP_GREY,
  WHITE,
} from "../../../constants/Colors";
import {
  WP100,
  WP162,
  HP100,
  HP12,
  WP8,
  WP4,
  WP5,
  WP3,
} from "../../../constants/Sizes";
import { registerDispatcher } from "../../../services/register";
import Spacer from "../../../components/Spacer";
import ButtonV2 from "../../../components/ButtonV2";
import Wrapper from "./Wrapper";

const mapStateToProps = ({ auth, register }) => ({
  step: register.behaviour.step,
  showPassword: register.behaviour.showPassword,
  ctaEnabled: register.behaviour.ctaEnabled,
  performHttp: register.behaviour.performHttp,
  email: register.data.email,
  password: register.data.password,
});

const mapDispatchToProps = {
  setPasswordVisibility: (visible) =>
    registerDispatcher.setPasswordVisibility(visible),
  setCtaEnabled: (enabled) => registerDispatcher.setCtaEnabled(enabled),
  setEmail: (value) => registerDispatcher.setData({ email: value }),
  setPassword: (value) => registerDispatcher.setData({ password: value }),
  setStep: (step) => registerDispatcher.setStep(step),
  setPerformHttp: (performHttp) =>
    registerDispatcher.setPerformHttp(performHttp),
};

class Finish extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit() {
    const { setStep, step } = this.props;
    // this.setState({
    //   isLoading: true
    // });
    // console.log('xxxx')
    // clearLocalStorage()
    setStep(step + 1);
  }
  componentDidMount() {}
  shouldComponentUpdate(nextProps, nextState) {
    const propFields = ["ctaEnabled", "performHttp"];
    const stateFields = ["isLoading"];
    return (
      (!isEqual(pick(this.props, propFields), pick(nextProps, propFields)) ||
        !isEqual(pick(this.state, nextState), pick(nextState, stateFields))) &&
      nextProps.step == 7
    );
  }

  render() {
    const { isLoading } = this.state;
    if (isLoading)
      return (
        <Container>
          <View
            style={{
              flex: 1,
              width: WP100,
              height: HP100,
              alignItems: "center",
              marginTop:
                Platform.OS == "android" ? -Constants.statusBarHeight : 0,
              justifyContent: "center",
            }}
          >
            <Image
              source={require('../../../assets/loading.gif')}
              style={{ width: WP162, height: undefined, aspectRatio: 840 / 607 }}
              fadeDuration={0}
            />
          </View>
        </Container>
      );
    else
      return (
        <Wrapper>
          <View>
            <Spacer size={HP12} />
            <Image
              size={"ultraMassive"}
              source={require("sf-assets/icons/v3/congratulationCheck.png")}
            />
            <Spacer size={WP8} />
            <Text centered size={"medium"} weight={600} color={NAVY_DARK}>
              Profil kamu berhasil dibuat
            </Text>
            <Spacer size={WP4} />
            <Text centered lineHeight={WP5} size={"mini"} color={SHIP_GREY}>
              {
                "Selamat! Mulai ikuti rangkaian event dan acara \nsekarang juga darimanapun"
              }
            </Text>
            <Spacer size={WP8} />
            <View style={{ flexDirection: "row", justifyContent: "center" }}>
              <ButtonV2
                style={{ paddingHorizontal: WP8, paddingVertical: WP3 }}
                color={REDDISH}
                onPress={this.onSubmit}
                textSize={"slight"}
                textColor={WHITE}
                text={"Mulai Sekarang"}
              />
            </View>
          </View>
        </Wrapper>
      );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Finish);
