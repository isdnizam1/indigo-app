import React from 'react'
import { TextInput, View, TouchableOpacity,
  StyleSheet
 } from 'react-native'
import { isEmpty, isEqual, pick, isNil, startCase, toLower } from 'lodash-es'
import { connect } from 'react-redux'
import { SelectModalV3, Text,
  InputTextLight
 } from 'sf-components'
import { WP2, WP4, WP6, WP8 } from 'sf-constants/Sizes'
import {
  getJob,
  getCompany,
  postRegisterV3CreateCompanyJob,
  postRegisterV3Skip,
} from 'sf-actions/api'

import style from 'sf-styles/register'
import { registerDispatcher } from 'sf-services/register'
import { GUN_METAL, REDDISH, SHIP_GREY_CALM, WHITE ,SILVER_TWO,SHIP_GREY,PALE_BLUE_TWO } from 'sf-constants/Colors'
import Label from 'sf-components/register/v3/Label'
import Spacer from 'sf-components/Spacer'
import ButtonV2 from 'sf-components/ButtonV2'
import ButtonIcon from 'sf-components/register/v3/ButtonIcon'
import { NO_COLOR} from '../../../constants/Colors'
import Wrapper from './Wrapper'


const JOB_SUGGESTIONS = [
  { job_title: 'UI/UX Design dan Product Management' },
  { job_title: 'Front End' },
  { job_title: 'Back End' },
  { job_title: 'Cyber Security' },
  { job_title: 'Data Science' },
  { job_title: 'React Native' },
  { job_title: 'Blockchain' },
  { job_title: 'Deep Learning' },
  { job_title: 'Machine Learning' },
]

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: '100%',
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 6,
  },
})
const mapStateToProps = ({ auth, register }) => ({
  step: register.behaviour.step,
  ctaEnabled: register.behaviour.ctaEnabled,
  performHttp: register.behaviour.performHttp,
  motivational_letter: register.data.motivational_letter,
  namee: register.data.namee,
  id_user: register.data.id_user,
  profile_type: register.data.profile_type,
  job_title: register.data.job_title,
  profile_picture: register.data.profile_picture,
})

const mapDispatchToProps = {
  setName: (name) => registerDispatcher.setData({ name }),
  setNamee: (namee) => registerDispatcher.setData({ namee }),
  setMotivation_letter: (motivational_letter) => registerDispatcher.setData({ motivational_letter }),
  setCtaEnabled: (enabled) => registerDispatcher.setCtaEnabled(enabled),
  setJobTitle: (job_title) => registerDispatcher.setData({ job_title }),
  setProfileType: (profile_type) => registerDispatcher.setData({ profile_type }),
  setStep: (step) => registerDispatcher.setStep(step),
  setPerformHttp: (performHttp) => registerDispatcher.setPerformHttp(performHttp),
}


class SelectProfession extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSolo: false,
    }
    this.onChangeText = this.onChangeText.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.shouldCtaEnabled = this.shouldCtaEnabled.bind(this)
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    const {
      profile_type,
      // setCtaEnabled,
      setStep,
      step,
      // job_title,
      // company_name,
    } = nextProps
  
      // setStep(step - 1)
       this.shouldCtaEnabled()
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   const propFields = [
  //     'ctaEnabled',
  //     'performHttp',
  //     'job_title',
  //     'step',
  //   ]
  //   const stateFields = ['isSolo']
  //   return (
  //     (!isEqual(pick(this.props, propFields), pick(nextProps, propFields)) ||
  //       !isEqual(pick(this.state, stateFields), pick(nextState, stateFields))) &&
  //     nextProps.step == 5
  //   )
  // }

  UNSAFE_componentWillUpdate(nextProps, nextState) {
    const { isSolo } = nextState
  }
_onChangeName = async (value) => {
    const { setMotivation_letter } = this.props;
    console.log(value);
     await setMotivation_letter(value)
    this.shouldCtaEnabled()
  };



  // _onChangeName = async (value) => {
  //   const { setNamee } = this.props
  //   console.log(value);
  //   if ((value || '').length < 54) await setNamee(value)
  //   this.shouldCtaEnabled()
  // };
  onSubmit() {
    let {
      setPerformHttp,
      setStep,
      step,
      namee,
      company_name,
      motivational_letter,
      id_user,
      job_title,
      profile_picture,
    } = this.props
    if (!isNil(profile_picture) && profile_picture.indexOf('base64') >= 0)
      profile_picture = profile_picture.split('base64,')[1]
    Promise.resolve(setPerformHttp(true)).then(() => {
      postRegisterV3CreateCompanyJob({
        company_name,
        motivational_letter,
        id_user,
        job_title,
        profile_picture,
      })
        .then(({ data }) => {
          // const { code } = data
          setStep(5)
        })
        .catch((e) => {})
        .then(() => setPerformHttp(false))
    })
  }

  _onSkip = () => {
    const { setPerformHttp, setStep, id_user } = this.props

    Promise.resolve(setPerformHttp(true)).then(() => {
      postRegisterV3Skip({
        id_user,
      })
        .then(({ data: { code } }) => {
          code == 200 && setStep(7)
        })
        .catch((e) => {})
        .then(() => setPerformHttp(false))
    })
  };

  onChangeText(value, callback) {
    Promise.resolve(callback(value)).then(() => {
      const { company_name, setCtaEnabled } = this.props
      setCtaEnabled(!isEmpty(company_name))
    })
  }

  shouldCtaEnabled() {
    const { isSolo } = this.state
    const { setCtaEnabled, job_title, company_name, motivational_letter} = this.props
    setCtaEnabled(
      !job_title ? !isEmpty(job_title): !isEmpty(job_title),
    )
  }

  referralLink() {
    return (
      <TouchableOpacity activeOpacity={0.8}>
        <View style={{ flexDirection: 'row', paddingVertical: WP2 }}>
          <Text type={'NeoSans'} size={'xmini'}>
            {'Have a '}
          </Text>
          <Text weight={400} type={'NeoSans'} size={'xmini'}>
            Referral Code?
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  // _onChangeCity = async (job_title) => {
  //   const { job_title } = this.props
  //   await setIdCity(job_title)
  //   await setCityName(city.city_name)
  //   this.shouldCtaEnabled()
  // };

  // _onChangeProfession = async (city) => {
  //   const {  setJobTitle } = this.props
  //   await setJobTitle(city)
  //   console.log(city,'city');
  //   this.shouldCtaEnabled()
  // };

  _onChangeProfession = (job_title) => {
    Promise.resolve(this.props.setJobTitle(job_title)).then(() => {
      // setTimeout(() => this.shouldCtaEnabled(), 1)
      this.shouldCtaEnabled()
    })
  };

  _onRemoveProfesion = () => {
    Promise.resolve(this.props.setJobTitle('')).then(() => {
      // setTimeout(() => this.shouldCtaEnabled(), 1)
      this.shouldCtaEnabled()
    })
  };

  render() {
    const { isSolo } = this.state
    let {
      // setCtaEnabled,
      ctaEnabled,
      company_name,
      // city,
      job_title,
      motivational_letter,
      // profile_picture,
      setCompanyName,
      // setIdCity,
      setJobTitle,
      namee,
      performHttp,
    } = this.props;

    return (
      <Wrapper showAvatar noHorizontalPadding>
        <View style={{ paddingHorizontal: WP6 }}>
          <Label
            title='PROFESSION INFORMATIONS'
            titleColor={GUN_METAL}
            marginBottom={WP4}
            titleWeight={600}
          />



          <SelectModalV3
            extraResult={false}
            selection={isEmpty(job_title) ? '' : job_title}
            showSelection={!!job_title}
            onRemoveSelection={() => this._onRemoveProfesion()}
            refreshOnSelect
            quickSearchTitle={'Saran Pencarian Profesi'}
            // quickSearchList={JOB_SUGGESTIONS}
            quickSearchKey={'job_title'}
            triggerComponent={
              <TouchableOpacity activeOpacity={0.9}>
                <View style={style.formGroup}>
                  <Label title={"Profesi"} marginBottom={0} />
                  <TextInput
                    placeholder='Pilih profesi yang ingin diikuti'
                    defaultValue={job_title}
                    editable={false}
                    style={[style.input, { opacity: performHttp ? 0.6 : 1 }]}
                  />
                </View>
              </TouchableOpacity>
            }
            header={'Profesi'}
            suggestion={getJob}
            suggestionKey='job_title'
            suggestionPathResult='job_title'
            onChange={this._onChangeProfession}
            suggestionCreateNewOnEmpty={false}
            createNew={true}
            asObject={false}
            showSuggestion={false}
            reformatFromApi={(text) => {
              return startCase(toLower(text || ''))
            }}
            placeholder='Coba cari profesi'
          />


   
         {/* <View style={style.formGroup}>
              <Text
                style={style.label}
                size={ 'tiny'}
                weight={400}
                type={'Circular'}
              >
                {'Tuliskan motivational letter'}
              </Text>
                <InputTextLight
          // innerRef={this.specificationRef}
          onChangeText={this._onChangeName}
          value={motivational_letter}
          placeholder={'Tuliskan "WHY" (alasan kamu mengikuti event ini)'}
          placeholderTextColor={SILVER_TWO}
          color={SHIP_GREY}
          bordered
          size='mini'
          type='Circular'
          returnKeyType={'next'}
          wording=' '
          maxLength={1000}
          maxLengthFocus
          multiline
          lineHeight={1}
          style={{ marginTop: 0, marginBottom: 0 }}
          textInputStyle={styles.input}
        />
              
            </View> */}

          <Spacer size={WP8} />
          <ButtonV2
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Lanjutkan'
            onPress={this.onSubmit}
            disabled={!(ctaEnabled && !performHttp)}
            textColor={WHITE}
            color={REDDISH}
          />
          {/* <ButtonV2
            onPress={this._onSkip}
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Skip'
            disabled={false}
            textColor={SHIP_GREY_CALM}
            color={NO_COLOR}
          /> */}
        </View>
      </Wrapper>
    )
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(SelectProfession)
