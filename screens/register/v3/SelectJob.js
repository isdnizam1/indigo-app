import React from 'react'
import { Image, TextInput, ImageBackground, TouchableOpacity, View, } from 'react-native'
import { isEmpty, isEqual, pick, get } from 'lodash-es'
import { connect } from 'react-redux'
import { _enhancedNavigation, Text } from 'sf-components'
import { NAVY_DARK, PALE_BLUE, SHIP_GREY, SHADOW_GRADIENT_REVERSE, WHITE, REDDISH } from 'sf-constants/Colors'
import { KEYBOARD_VISIBLE } from 'sf-services/helper/actionTypes'
import { getJobGroup, postRegisterV3SelectJobGroup, postRegisterV3Skip } from 'sf-actions/api'
import { isTabletOrIpad, isIOS } from 'sf-utils/helper'
import styles from 'sf-styles/register'
import { registerDispatcher } from 'sf-services/register'
import ButtonV2 from 'sf-components/ButtonV2'
import Spacer from 'sf-components/Spacer'
import { WP1, WP305, WP4, WP405, WP6, WP2 } from 'sf-constants/Sizes'
import { LinearGradient } from 'expo-linear-gradient'
import { HEADER } from 'sf-constants/Styles'
import { NO_COLOR, SHIP_GREY_CALM } from '../../../constants/Colors'
import Wrapper from './Wrapper'

const style = styles
const tomatoTick = require('sf-assets/icons/tomatoTickWhiteCircle.png')

const mapStateToProps = ({ auth, register }) => ({
  step: register.behaviour.step,
  ctaEnabled: register.behaviour.ctaEnabled,
  performHttp: register.behaviour.performHttp,
  id_user: register.data.id_user,
  profile_type: register.data.profile_type,
  job_group_name: register.data.job_group_name,
  other_job_group_name: register.data.other_job_group_name,
})

const mapDispatchToProps = {
  setCtaEnabled: (enabled) => registerDispatcher.setCtaEnabled(enabled),
  setName: (name) => registerDispatcher.setData({ name }),
  setJobGroupName: (job_group_name) =>
    registerDispatcher.setData({ job_group_name }),
  setOtherJobGroupName: (other_job_group_name) =>
    registerDispatcher.setData({ other_job_group_name }),
  setStep: (step) => registerDispatcher.setStep(step),
  setPerformHttp: (performHttp) =>
    registerDispatcher.setPerformHttp(performHttp),
  keyboardVisible: (response) => ({
    type: KEYBOARD_VISIBLE,
    response
  })
}

const Config = {
  personal: {
    title: 'Pilih kategori yang sesuai denganmu',
    subtitle: 'Pilih 2 kategori untuk memulai',
    otherJobId: 15,
    minSelectedJob: 2
  },
  group: {
    title: 'Kategori Group/Band/Organisasi',
    subtitle: 'Pilih salah satu kategori untuk memulai',
    otherJobId: 16,
    minSelectedJob: 1
  },
  music_enthusiast: {
    title: 'Pilih kategori yang sesuai denganmu',
    subtitle: 'Pilih 2 kategori untuk memulai',
    otherJobId: 15,
    minSelectedJob: 2
  },
}

class SelectJob extends React.Component {
  swiper = React.createRef();

  constructor(props) {
    super(props)
    this.state = {
      activeIndex: 0,
      options: [],
      otherSelected: false
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.getJobGroup = this.getJobGroup.bind(this)
    this.isSelected = this.isSelected.bind(this)
  }

  constructJob = () => {
    const {
      other_job_group_name,
      job_group_name,
    } = this.props

    let selected = []
    if (!isEmpty(job_group_name)) selected = [...job_group_name]
    if (!isEmpty(other_job_group_name)) selected.push(other_job_group_name)
    return selected
  }

  onSubmit() {
    const {
      setPerformHttp,
      setStep,
      step,
      profile_type,
      id_user,
    } = this.props
    Promise.resolve(setPerformHttp(true)).then(() => {
      postRegisterV3SelectJobGroup({
        job_group_name: this.constructJob(),
        profile_type,
        id_user,
      })
        .then(({ data: { code } }) => {
          code == 200 && setStep(step + (profile_type === 'personal' ? 1 : 2))
        })
        .catch((e) => {})
        .then(() => setPerformHttp(false))
    })
  }

  _onSkip = () => {
    const {
      setPerformHttp,
      setStep,
      id_user,
    } = this.props

    Promise.resolve(setPerformHttp(true)).then(() => {
      postRegisterV3Skip({
        id_user,
      })
        .then(({ data: { code } }) => {
          code == 200 && setStep(7)
        })
        .catch((e) => {})
        .then(() => setPerformHttp(false))
    })
  }

  getJobGroup() {
    const { dispatch, profile_type } = this.props
    dispatch(getJobGroup, { profile_type: profile_type === 'music_enthusiast' ? 'personal' : profile_type })
      .then((options) => {
        this.setState({ options })
      })
    return true
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { step, keyboardVisible } = this.props
    nextProps.step == 4 && keyboardVisible(true) // force prop to pull down CTA Button
    step == 4 && nextProps.step != 4 && keyboardVisible(false) // force prop to pull up CTA Button
    step != 4 &&
      nextProps.step == 4 &&
      this.getJobGroup() &&
      this.validateForm()
  }

  shouldComponentUpdate(nextProps, nextState) {
    const propFields = [
      'ctaEnabled',
      'performHttp',
      'job_group_name',
      'profile_type',
      'step',
      'other_job_group_name'
    ]
    const stateFields = ['options', 'otherSelected']
    return (
      !isEqual(pick(this.props, propFields), pick(nextProps, propFields)) ||
      !isEqual(pick(this.state, stateFields), pick(nextState, stateFields))
    ) && (nextProps.step == 4 || this.props.step == 5)
  }

  validateForm = () => {
    const {
      profile_type,
      setCtaEnabled
    } = this.props

    const isValid = this.constructJob().length >= Config[profile_type].minSelectedJob
    setCtaEnabled(isValid)
  }

  isSelected = (item) => {
    const {
      profile_type,
      job_group_name
    } = this.props

    if (Number(item.id_job_group) === Config[profile_type].otherJobId) {
      return this.state.otherSelected
    }
    return job_group_name && job_group_name.includes(item.job_group_name)
  };

  onSelectCategory = (job) => async () => {
    const {
      setJobGroupName,
      profile_type,
      job_group_name
    } = this.props

    const { otherSelected } = this.state

    job.job_group_name == 'Lainnya' && setTimeout(() => this.scrollView.scrollToEnd({
      animated: true
    }), 500)

    let selectedJob = job_group_name ? [...job_group_name] : []
    if (Number(job.id_job_group) === Config[profile_type].otherJobId) {
      if (otherSelected) {
        await this.props.setOtherJobGroupName(undefined)
      }
      if(!otherSelected) {
        if(selectedJob.length >= 2) {
          selectedJob = [selectedJob[0]]
        }
      }
      this.setState({ otherSelected: !otherSelected })
    } else {
      if (selectedJob.includes(job.job_group_name)) {
        selectedJob = selectedJob.filter((item) => item !== job.job_group_name)
      } else {
        if(selectedJob.length == 2) {
          selectedJob = [selectedJob[0]]
        }
        if(selectedJob.length == 1 && otherSelected) {
          selectedJob = []
        }
        selectedJob.push(job.job_group_name)
      }
    }
    await setJobGroupName(selectedJob)
    this.validateForm()
  }

  _scrollViewRef = (ref) => { this.scrollView = ref }

  render() {
    const { options, otherSelected } = this.state
    const {
      ctaEnabled,
      performHttp,
      profile_type = 'personal',
      other_job_group_name,
      setOtherJobGroupName
    } = this.props

    return (
      <View style={{ flex: 1 }}>
        <Wrapper onScrollViewRef={this._scrollViewRef}>
          <View ref={this._scrollViewRef} style={{ paddingBottom: WP405 }}>
            <View style={{ marginBottom: WP6 }}>
              <Text
                color={NAVY_DARK}
                size={isTabletOrIpad() ? 'mini' : 'medium'}
                weight={600}
                lineHeight={WP6}
                type={'Circular'}
              >
                {get(Config[profile_type], 'title')}
              </Text>
              <Spacer size={WP1} />
              <Text type='Circular' color={SHIP_GREY} size='mini'>
                {get(Config[profile_type], 'subtitle')}
              </Text>
            </View>

            <View style={styles.jobGroupWrapper}>
              {
                options.map((job, index) => (
                  <TouchableOpacity
                    onPress={this.onSelectCategory(job)}
                    key={index}
                    style={styles.jobGroupName}
                  >
                    <ImageBackground
                      source={{ uri: job.icon }}
                      imageStyle={{ width: '100%', height: '100%' }}
                      style={{
                        width: '100%', height: '100%', position: 'absolute'
                      }}
                    />
                    {this.isSelected(job) && (
                      <>
                        <View style={{ position: 'absolute', width: '100%', height: '100%', backgroundColor: 'rgba(255, 101, 31, .75)' }}/>
                        <Image style={styles.jobGroupTick} source={tomatoTick} />
                      </>
                    )}
                    <View style={styles.jobGroupNameWrapper}>
                      <Text type='Circular' color={WHITE} weight={500} lineHeight={WP405}>{job.job_group_name}</Text>
                    </View>
                  </TouchableOpacity>
                ))
              }
            </View>

            {
              otherSelected && (
                <View style={style.formGroup}>
                  <Text
                    style={style.label}
                    size={isTabletOrIpad() ? 'tiny' : 'xmini'}
                    weight={400}
                    type={'Circular'}
                  >
                    Tuliskan kategori lainnya
                  </Text>
                  <TextInput
                    autoFocus
                    onSubmitEditing={() => setTimeout(() => this.scrollView.scrollToEnd({ animated: true }), 500)}
                    placeholder='Tuliskan kategori lainnya'
                    value={other_job_group_name}
                    style={style.input}
                    editable={this.props.step == 4}
                    readonly={this.props.step != 4}
                    disabled={this.props.step != 4}
                    onChangeText={async (value) => {
                      await setOtherJobGroupName(value)
                      this.validateForm()
                    }}

                  />
                </View>
              )
            }

          </View>

        </Wrapper>
        <LinearGradient colors={SHADOW_GRADIENT_REVERSE} style={HEADER.shadow} />
        <View style={{ paddingHorizontal: WP6, paddingVertical: WP305, borderTopWidth: 0.75, borderTopColor: PALE_BLUE }}>
          <ButtonV2
            onPress={this.onSubmit}
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Pilih Kategori'
            disabled={!(ctaEnabled && !performHttp)}
            textColor={WHITE}
            color={REDDISH}
          />
          <ButtonV2
            onPress={this._onSkip}
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Skip'
            disabled={false}
            textColor={SHIP_GREY_CALM}
            color={NO_COLOR}
          />
        </View>
        {isIOS() && <Spacer size={WP2} />}
      </View>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SelectJob)
)
