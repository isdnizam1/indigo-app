import React from 'react'
import { View } from 'react-native'
import { isEmpty, isEqual, pick, noop } from 'lodash-es'
import { connect } from 'react-redux'
import Text from 'sf-components/Text'
import Image from 'sf-components/Image'
import Spacer from 'sf-components/Spacer'
import Touchable from 'sf-components/Touchable'
import ButtonV2 from 'sf-components/ButtonV2'
import ConfirmationModalV3 from 'sf-components/ConfirmationModalV3'
import { PALE_BLUE, WHITE, REDDISH, NAVY_DARK, SHIP_GREY } from 'sf-constants/Colors'
import { WP3, WP4, WP5, WP8, HP12 } from 'sf-constants/Sizes'
import { registerDispatcher } from 'sf-services/register'
import { postRegisterV3SendVerificationEmail } from 'sf-actions/api'
import { goToMailbox } from 'sf-utils/helper'
import Wrapper from './Wrapper'

const mapStateToProps = ({ auth, register }) => ({
  step: register.behaviour.step,
  showPassword: register.behaviour.showPassword,
  performHttp: register.behaviour.performHttp,
  id_user: register.data.id_user,
  email: register.data.email
})

const mapDispatchToProps = {
  setCtaEnabled: (enabled) => registerDispatcher.setCtaEnabled(enabled),
  setEmail: (value) => registerDispatcher.setData({ email: value }),
  setPassword: (value) => registerDispatcher.setData({ password: value }),
  setIdUser: (value) => registerDispatcher.setData({ id_user: value }),
  setStep: (step) => registerDispatcher.setStep(step),
  resetData: () => registerDispatcher.resetData(),
  setPerformHttp: (performHttp) => registerDispatcher.setPerformHttp(performHttp),
}

class EmailVerification extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
      countdown: 0
    }
    this.onChangeText = this.onChangeText.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.onResetRegistration = this.onResetRegistration.bind(this)
    this.runCountdown = this.runCountdown.bind(this)
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    const { modalVisible } = this.state
    const { step } = this.props
    console.log(99991, step, nextProps);
    nextProps.step != 2 && step == 2 && modalVisible && this.setState({ modalVisible: false })
  }

  runCountdown() {
    const { countdown } = this.state
    countdown > 0 && setTimeout(() => {
      this.setState({ countdown: countdown - 1 }, this.runCountdown)
    }, 1000)
  }

  onSubmit() {
    const { setPerformHttp, email } = this.props
    Promise.resolve(setPerformHttp(true)).then(() => {
      postRegisterV3SendVerificationEmail({ email, verified_by: 'mobile' }).then(({ data }) => {
        const { code } = data
        code == 200 && this.setState({ modalVisible: true, countdown: 30 }, this.runCountdown)
      }).catch((err) => {}).then(() => {
        setPerformHttp(false)
      })
    })
  }

  onResetRegistration() {
    const { setStep, setEmail, setPassword, setIdUser } = this.props
    this.setState({
      modalVisible: false
    }, () => {
      Promise.all([
        setPassword(null),
        setEmail(null),
        setIdUser(null),
      ]).then(() => {
        setTimeout(() => setStep(1), 1000)
      })
    })
  }

  onChangeText(value, callback) {
    Promise.resolve(callback(value)).then(() => {
      const { email, password, setCtaEnabled } = this.props
      setCtaEnabled(!isEmpty(email) && !isEmpty(password))
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    const propFields = ['performHttp']
    const stateFields = ['countdown', 'modalVisible']
    return !isEqual(
      pick(this.props, propFields),
      pick(nextProps, propFields)
    ) || !isEqual(
      pick(this.state, stateFields),
      pick(nextState, stateFields)
    )
  }

  render() {
    const {
      email
    } = this.props
    const {
      modalVisible,
      countdown
    } = this.state
    return (<View>
      <Wrapper>
        <View>
          <Spacer size={HP12} />
          <Image size={'ultraMassive'} source={require('sf-assets/icons/v3/emailVerification.png')} />
          <Spacer size={WP8} />
          <Text
            centered
            size={'medium'}
            weight={600}
            color={NAVY_DARK}
          >
            Verifikasi Email
          </Text>
          <Spacer size={WP4} />
          <Text
            centered
            lineHeight={WP5}
            size={'mini'}
            color={SHIP_GREY}
          >
            Kami akan mengirim verifikasi email ke{'\n'}{email}
          </Text>
          <Spacer size={WP8} />
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <ButtonV2
              style={{ paddingHorizontal: WP8, paddingVertical: WP3 }}
              color={REDDISH}
              onPress={this.onSubmit}
              textSize={'slight'}
              textColor={WHITE}
              text={'Kirim Verifikasi'}
            />
          </View>
        </View>
      </Wrapper>
      <ConfirmationModalV3
        visible={modalVisible}
        title={'Email verifikasi telah terkirim'}
        subtitle='Kami telah mengirimkan email akun'
        extraSubtitle={'verifikasi melalui email kamu'}
        primary={{
          text: 'Buka Email',
          onPress: () => goToMailbox(email)
        }}
        secondary={{
          text: `Kirim ulang verifikasi ${(countdown > 0 ? `(00:0${countdown})` : '').replace(/\d(\d\d)/, '$1')}`,
          onPress: countdown == 0 ? this.onSubmit : noop,
          textColor: countdown == 0 ? REDDISH : null,
          backgroundColor: countdown == 0 ? WHITE : null,
          style: {
            borderWidth: 1,
            borderColor: countdown == 0 ? REDDISH : PALE_BLUE
          }
        }}
        customFooter={<View style={{ width: '100%' }}>
          <Spacer size={WP5} />
          <Touchable onPress={this.onResetRegistration} style={{ width: '100%' }}>
            <View style={{ width: '100%' }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderTopWidth: 1, borderTopColor: PALE_BLUE, width: '100%', paddingVertical: WP5 }}>
                <Text
                  size={'xmini'}
                  color={SHIP_GREY}
                >
                  
                  Terjadi kesalahan?{' '}
                </Text>
                <Text
                  weight={500}
                  size={'xmini'}
                  color={SHIP_GREY}
                >
                  Ulangi Pendaftaran
                </Text>
              </View>
            </View>
          </Touchable>
        </View>}
      />
      {/*<Modal
        transparent
        visible={modalVisible}
      >
        <Dialog
          width={isTabletOrIpad() ? 450 : 250}
          height={isTabletOrIpad() ? 430 : 240}
          isConfirmation={false}
          options={[
            {
              text: 'Check Email',
              enabled: true,
              color: REDDISH,
              onPress: () => goToMailbox(email)
            },
            {
              text: `Resend Link ${(countdown > 0 ? `(00:0${countdown})` : '').replace(/\d(\d\d)/, '$1')}`.trim(),
              enabled: countdown == 0,
              color: countdown == 0 ? GREY : GREY_CALM_SEMI,
              onPress: this.onSubmit
            },
            {
              text: 'Reset Registration',
              enabled: countdown == 0,
              color: countdown == 0 ? GREY : GREY_CALM_SEMI,
              onPress: this.onResetRegistration
            }
          ]}
        >
          <Text
            weight={400}
            centered
            size={'xmini'}
          >
            {'Verification has been sent\nto your email inbox'}
          </Text>
        </Dialog>
      </Modal>*/}
    </View>)
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(EmailVerification)
