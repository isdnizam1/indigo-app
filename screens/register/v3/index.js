import React from 'react'
import { connect } from 'react-redux'
import {
  Animated,
  BackHandler,
  Dimensions,
  Keyboard,
  Modal,
  ScrollView,
  View,
} from 'react-native'
import { isNil, noop } from 'lodash-es'
import { _enhancedNavigation, Container } from 'sf-components'
import HeaderNormal from 'sf-components/HeaderNormal'
import { GET_USER_DETAIL } from 'sf-services/auth/actionTypes'
import { KEYBOARD_VISIBLE } from 'sf-services/helper/actionTypes'
import style from 'sf-styles/register'
import { registerDispatcher } from 'sf-services/register'
import { getProfileDetail } from 'sf-actions/api'
import { clearLocalStorage, setUserId } from 'sf-utils/storage'
import { setLoading } from 'sf-services/app/actionDispatcher'
import { LinearGradient } from 'expo-linear-gradient'
import { HEADER } from 'sf-constants/Styles'
import { SHADOW_GRADIENT, SHIP_GREY, WHITE } from 'sf-constants/Colors'
import { WP5 } from 'sf-constants/Sizes'
import Credentials from './Credentials'
import EmailVerification from './EmailVerification'
import SetupProfile from './SetupProfile'
import SelectJob from './SelectJob'
import SelectProfession from './SelectProfession'
import SelectGenre from './SelectGenre'
import Finish from './Finish'

const WINDOW_WIDTH = Dimensions.get('window').width

const mapStateToProps = ({ auth, register }) => ({
  userData: auth.user,
  focused: register.behaviour.focused,
  totalStep: register.behaviour.totalStep,
  step: register.behaviour.step,
  id_user: register.data.id_user,
  ctaEnabled: register.behaviour.ctaEnabled,
  progress: register.behaviour.progress,
  profile_type: register.data.profile_type,
  httpProgress: register.behaviour.httpProgress,
  httpVisibility: register.behaviour.httpVisibility,
  regData: register.data,
})

const mapDispatchToProps = {
  setCtaEnabled: (enabled) => registerDispatcher.setCtaEnabled(enabled),
  setStep: (step) => registerDispatcher.setStep(step),
  setIdUser: (id_user) => registerDispatcher.setData({ id_user }),
  setFocus: (focused) => registerDispatcher.setFocus(focused),
  resetData: () => registerDispatcher.resetData(),
  setPerformHttp: (performHttp) => registerDispatcher.setPerformHttp(performHttp),
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail,
  }),
  keyboardVisible: (response) => ({
    type: KEYBOARD_VISIBLE,
    response,
  }),
  setProfileType: (profile_type) => registerDispatcher.setData({ profile_type }),
  setLoading,
}

const mapFromNavigationParam = (getParam) => ({})

class RegistrationV3 extends React.Component {
  scrollView = React.createRef();

  constructor(props) {
    super(props)
    this.state = {
      scrollEnabled: false,
      isReady: false,
      isLoading: false,
    }
    this.startScroll = this.startScroll.bind(this)
    this.lockScrollview = this.lockScrollview.bind(this)
    this.handleBackButton = this.handleBackButton.bind(this)
  }

  handleBackButton() {
    const { step, setStep, profile_type, setProfileType, navigateBack } = this.props
    if (step <= 1) navigateBack()
    else if (step == 2 || (step == 3 && !profile_type)) {
      this.props.resetData()
      navigateBack()
    } else if (step != 3 && step != 7) setStep(step - 1)
    else if (step == 3 && !!profile_type) {
      setProfileType(null)
    }
    return true
  }

  lockScrollview() {
    this.setState({ scrollEnabled: false })
  }

  componentDidMount() {
    // this.props.setIdUser(null)
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () =>
      this.props.keyboardVisible(true),
    )
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () =>
      this.props.keyboardVisible(false),
    )
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)
    const { setStep, setFocus, setPerformHttp, step } = this.props
    Promise.resolve(setFocus(true)).then(() => {
      setPerformHttp(false)
      const nextStep = step > 0 ? step : 1
      Promise.resolve(setStep(0)).then(() => {
        // setTimeout(() => setStep(6), 500)
        setTimeout(() => setStep(nextStep), 250)
      })
      setTimeout(() => this.setState({ isReady: true }), 500)
    })
    this.intervalChecker = setInterval(
      () => {
        const {
          step,
          regData: { id_user },
        } = this.props
        step == 2 &&
          getProfileDetail({ id_user }).then(
            ({
              data: {
                result: { email_status },
              },
            }) => {
              this.props.setProfileType('personal')
              email_status === 'verified' && setStep(3)
            },
          )
      },
      __DEV__ ? 3000 : 7500,
    )
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      totalStep,
      step,
      setStep,
      navigateBack,
      id_user,
      resetData,
      setFocus,
      profile_type,
      setUserDetailDispatcher,
      navigateTo,
    } = this.props
    // __DEV__ && resetData()
    const { isReady } = this.state
    if (step > 2 && isNil(id_user)) setStep(1)
    if (step == 0 && prevProps.step == 1 && isReady) navigateBack()
    else if (
      (prevProps.step != step && step > 0) ||
      profile_type != prevProps.profile_type
    ) {
      if (step <= totalStep) this.startScroll()
      else {
        this.setState({ isLoading: true }, () => {
          Promise.resolve(setFocus(false)).then(() => {
            getProfileDetail({ id_user }).then(async ({ data }) => {
              resetData()
              await clearLocalStorage()
              await setUserId(id_user.toString())
              setUserDetailDispatcher(data.result)
              navigateTo('LoadingScreen', {}, 'replace')
            })
          })
        })
      }
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
    const { setFocus, progress } = this.props
    clearInterval(this.intervalChecker)
    Promise.all([progress.setValue(0), setFocus(false)])
  }

  startScroll() {
    const { navigateBack, step } = this.props
    if (step == 0) navigateBack()
    else
      this.setState(
        {
          scrollEnabled: true,
        },
        () => {
          try {
            setTimeout(() => {
              try {
                this.props.focused &&
                  this.scrollView.scrollTo({
                    x: WINDOW_WIDTH * (step - 1),
                    y: 0,
                    animated: true,
                  })
              } catch (e) {
                // Keep silent
              } finally {
                setTimeout(this.lockScrollview, 1500)
              }
            }, 150)
          } catch (e) {
            // keep silent
          }
        },
      )
  }

  _renderHeader = () => {
    const { step, profile_type } = this.props
    let invisibleBack = step == 7
    return (
      <View>
        <HeaderNormal
          style={style.headerNormal}
          centered
          iconLeftOnPress={invisibleBack ? noop : this.handleBackButton}
          textType={'Circular'}
          // textSize={'mini'}
          iconLeftColor={invisibleBack ? WHITE : SHIP_GREY}
          iconStyle={{ marginRight: WP5 }}
          textColor={SHIP_GREY}
          text={
            [
              null,
              'Create New Account',
              'Verifikasi',
              profile_type ? 'Buat Profile' : 'Pilih Profile',
              'Kategori',
              'Interest',
              'Buat Profile',
              'Selamat Bergabung',
            ][this.props.step]
          }
          textSize={'slight'}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
      </View>
    )
  };

  _scrollViewRef = (ref) => {
    this.scrollView = ref
  };

  render() {
    const { scrollEnabled, isLoading } = this.state
    const { progress, httpProgress, httpVisibility } = this.props
    return (
      <Container
        renderHeader={this._renderHeader}
        scrollable={false}
        // scrollBackgroundColor={PALE_WHITE}
      >
        {!isLoading && (
          <View style={style.progressWrapper}>
            <Animated.View style={{ ...style.progressInner, width: progress }} />
          </View>
        )}
        <ScrollView
          scrollEnabled={scrollEnabled}
          showsHorizontalScrollIndicator={false}
          ref={this._scrollViewRef}
          keyboardShouldPersistTaps={'always'}
          keyboardDismissMode={'on-drag'}
          horizontal
        >
          <Credentials />

          <EmailVerification />

          <SetupProfile />

          {/* <SelectJob /> */}

          <SelectProfession />

          <SelectGenre />

          <Finish />
        </ScrollView>
        <View style={style.httpProgressWrapper}>
          <Animated.View
            style={{
              ...style.httpProgress,
              width: httpProgress,
              opacity: httpVisibility,
            }}
          />
        </View>
        <Modal transparent visible={scrollEnabled} />
      </Container>
    )
  }
}

RegistrationV3.navigationOptions = () => ({
  gesturesEnabled: false,
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(RegistrationV3),
  mapFromNavigationParam,
)
