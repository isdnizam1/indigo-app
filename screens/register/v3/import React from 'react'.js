import React from 'react'
import { TextInput, View, TouchableOpacity } from 'react-native'
import { isEmpty, isEqual, pick, isNil, startCase, toLower } from 'lodash-es'
import { connect } from 'react-redux'
import { SelectModalV3, Text } from 'sf-components'
import { WP2, WP4, WP6, WP8 } from 'sf-constants/Sizes'
import {
  getJob,
  getCompany,
  postRegisterV3CreateCompanyJob,
  postRegisterV3Skip,
} from 'sf-actions/api'
import style from 'sf-styles/register'
import { registerDispatcher } from 'sf-services/register'
import { GUN_METAL, REDDISH, SHIP_GREY_CALM, WHITE } from 'sf-constants/Colors'
import Label from 'sf-components/register/v3/Label'
import Spacer from 'sf-components/Spacer'
import ButtonV2 from 'sf-components/ButtonV2'
import ButtonIcon from 'sf-components/register/v3/ButtonIcon'
import { NO_COLOR } from '../../../constants/Colors'
import Wrapper from './Wrapper'


const JOB_SUGGESTIONS = [
  { job_title: 'UIUX Designer' },
  { job_title: 'Front End' },
  { job_title: 'Back End' },
  { job_title: 'Cyber Security' },
  { job_title: 'Data Science' },
  { job_title: 'React Native' },
  { job_title: 'Product Management' },
]

const mapStateToProps = ({ auth, register }) => ({
  step: register.behaviour.step,
  showPassword: register.behaviour.showPassword,
  ctaEnabled: register.behaviour.ctaEnabled,
  performHttp: register.behaviour.performHttp,
  company_name: register.data.company_name,
  motivational_letter: register.data.motivational_letter,
  id_user: register.data.id_user,
  profile_type: register.data.profile_type,
  job_title: register.data.job_title,
  profile_picture: register.data.profile_picture,
})

const mapDispatchToProps = {
  setName: (motivational_letter) => registerDispatcher.setData({ motivational_letter }),
  setCtaEnabled: (enabled) => registerDispatcher.setCtaEnabled(enabled),
  setCompanyName: (company_name) => registerDispatcher.setData({ company_name }),
  setJobTitle: (job_title) => registerDispatcher.setData({ job_title }),
  setProfileType: (profile_type) => registerDispatcher.setData({ profile_type }),
  setStep: (step) => registerDispatcher.setStep(step),
  setPerformHttp: (performHttp) => registerDispatcher.setPerformHttp(performHttp),
}


class SelectProfession extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSolo: false,
    }
    this.onChangeText = this.onChangeText.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.shouldCtaEnabled = this.shouldCtaEnabled.bind(this)
  }

  UNSAFE_componentWillReceiveProps(nextProps, nextState) {
    const {
      profile_type,
      // setCtaEnabled,
      setStep,
      step,
      // job_title,
      // company_name,
    } = nextProps
    if (nextProps.step == 5) {
      if (profile_type !== 'group') setStep(step - 1)
      else this.shouldCtaEnabled()
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const propFields = [
      'ctaEnabled',
      'performHttp',
      'job_title',
      'profile_picture',
      'step',
      'company_name',
    ]
    const stateFields = ['isSolo']
    return (
      (!isEqual(pick(this.props, propFields), pick(nextProps, propFields)) ||
        !isEqual(pick(this.state, stateFields), pick(nextState, stateFields))) &&
      nextProps.step == 5
    )
  }

  UNSAFE_componentWillUpdate(nextProps, nextState) {
    const { isSolo } = nextState
    // const { setCtaEnabled } = this.props
    const { company_name, setCompanyName } = nextProps
    isSolo && !isEmpty(company_name) && setCompanyName(null)
  }
_onChangeName = async (value) => {
    const { setName } = this.props
    if ((value || '').length < 54) await setName(value)
    this.shouldCtaEnabled()
  };

  onSubmit() {
    let {
      setPerformHttp,
      setStep,
      step,
      job_title,
      company_name,
      motivational_letter,
      id_user,
      profile_picture,
    } = this.props
    if (!isNil(profile_picture) && profile_picture.indexOf('base64') >= 0)
      profile_picture = profile_picture.split('base64,')[1]
    Promise.resolve(setPerformHttp(true)).then(() => {
      console.log(motivational_letter)
      postRegisterV3CreateCompanyJob({
        job_title,
        company_name,
        motivational_letter,
        id_user,
        profile_picture,
      })
        .then(({ data }) => {
          // const { code } = data
          setStep(step + 1)
        })
        .catch((e) => {})
        .then(() => setPerformHttp(false))
    })
  }

  _onSkip = () => {
    const { setPerformHttp, setStep, id_user } = this.props

    Promise.resolve(setPerformHttp(true)).then(() => {
      postRegisterV3Skip({
        id_user,
      })
        .then(({ data: { code } }) => {
          code == 200 && setStep(7)
        })
        .catch((e) => {})
        .then(() => setPerformHttp(false))
    })
  };

  onChangeText(value, callback) {
    Promise.resolve(callback(value)).then(() => {
      const { company_name, setCtaEnabled } = this.props
      setCtaEnabled(!isEmpty(company_name))
    })
  }

  shouldCtaEnabled() {
    const { isSolo } = this.state
    const { setCtaEnabled, job_title, company_name, motivational_letter} = this.props
    setCtaEnabled(
      !motivational_letter ? !isEmpty(job_title): !isEmpty(job_title),
    )
  }

  referralLink() {
    return (
      <TouchableOpacity activeOpacity={0.8}>
        <View style={{ flexDirection: 'row', paddingVertical: WP2 }}>
          <Text type={'NeoSans'} size={'xmini'}>
            {'Have a '}
          </Text>
          <Text weight={400} type={'NeoSans'} size={'xmini'}>
            Referral Code?
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  // _onChangeCity = async (job_title) => {
  //   const { job_title } = this.props
  //   await setIdCity(job_title)
  //   await setCityName(city.city_name)
  //   this.shouldCtaEnabled()
  // };

  _onChangeProfession = async (city) => {
    const {  setJobTitle } = this.props
    await setJobTitle(city.job_title)
    console.log(city);
    this.shouldCtaEnabled()
  };


  // _onChangeProfession = (job_title) => {
  //   Promise.resolve(this.props.setJobTitle(job_title)).then(() => {
  //     setTimeout(() => this.shouldCtaEnabled(), 1)
  //   })
  // };

  render() {
    const { isSolo } = this.state
    let {
      // setCtaEnabled,
      ctaEnabled,
      company_name,
      // city,
      job_title,
      motivational_letter,
      // profile_picture,
      setCompanyName,
      // setIdCity,
      setJobTitle,
      performHttp,
    } = this.props;
    console.log(job_title,'sssss');

    return (
      <Wrapper showAvatar noHorizontalPadding>
        <View style={{ paddingHorizontal: WP6 }}>
          <Label
            title='STREAM INFORMATIONS'
            titleColor={GUN_METAL}
            marginBottom={WP4}
            titleWeight={600}
          />



          <SelectModalV3
            extraResult={false}
            selection={isEmpty(job_title) ? '' : job_title}
            showSelection={!!job_title}
            onRemoveSelection={() => {
              setJobTitle('')
            }}
            refreshOnSelect
            quickSearchTitle={'Saran Pencarian Stram'}
            quickSearchList={JOB_SUGGESTIONS}
            quickSearchKey={'job_title'}
            triggerComponent={
              <TouchableOpacity activeOpacity={0.9}>
                <View style={style.formGroup}>
                  <Label title={"Stream"} marginBottom={0} />
                  <TextInput
                    placeholder='Pilih stream yang ingin diikuti'
                    defaultValue={job_title}
                    editable={false}
                    style={[style.input, { opacity: performHttp ? 0.6 : 1 }]}
                  />
                </View>
              </TouchableOpacity>
            }
            header={'Stream'}
            suggestion={getJob}
            suggestionKey='job_title'
            suggestionPathResult='job_title'
            onChange={this._onChangeProfession}
            suggestionCreateNewOnEmpty={false}
            createNew={false}
            asObject={false}
            showSuggestion={true}
            reformatFromApi={(text) => {
              return startCase(toLower(text || ''))
            }}
            placeholder='Coba cari stream'
          />


   


         <View style={style.formGroup}>
              <Text
                style={style.label}
                size={ 'tiny'}
                weight={400}
                type={'Circular'}
              >
                {'Tuliskan motivational letter'}
              </Text>
              <TextInput
                value={motivational_letter}
                editable={!performHttp}
                onChangeText={this._onChangeName}
                autoCompleteType={'name'}
                placeholder={'Tuliskan "WHY" (alasan kamu mengikuti event ini)'}
                style={style.input}
              />
            </View>

          <Spacer size={WP8} />
          <ButtonV2
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Lanjutkan'
            onPress={this.onSubmit}
            disabled={!(ctaEnabled && !performHttp)}
            textColor={WHITE}
            color={REDDISH}
          />
          {/* <ButtonV2
            onPress={this._onSkip}
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Skip'
            disabled={false}
            textColor={SHIP_GREY_CALM}
            color={NO_COLOR}
          /> */}
        </View>
      </Wrapper>
    )
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(SelectProfession)
