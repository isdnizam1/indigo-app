import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { isEqual, pick, isNil, map } from 'lodash-es'
import { connect } from 'react-redux'
import { Icon, SelectModalV3, Text, _enhancedNavigation } from 'sf-components'
import {
  WHITE,
  GUN_METAL,
  REDDISH,
  PALE_BLUE_TWO,
  SHIP_GREY,
} from 'sf-constants/Colors'
import { WP2, WP6, WP4, WP3, WP205, WP8 } from 'sf-constants/Sizes'
import {
  getGenreInterest,
  getGenreSuggestion,
  postRegisterV3SelectGenre,
  postRegisterV3Skip,
} from 'sf-actions/api'
import style from 'sf-styles/register'
import { registerDispatcher } from 'sf-services/register'
import Label from 'sf-components/register/v3/Label'
import Spacer from 'sf-components/Spacer'
import ButtonV2 from 'sf-components/ButtonV2'
import ButtonIcon from 'sf-components/register/v3/ButtonIcon'
import { NO_COLOR, SHIP_GREY_CALM } from '../../../constants/Colors'
import Wrapper from './Wrapper'

const mapStateToProps = ({ auth, register }) => ({
  step: register.behaviour.step,
  ctaEnabled: register.behaviour.ctaEnabled,
  id_user: register.data.id_user,
  profile_picture: register.data.profile_picture,
  interest_name: register.data.interest_name,
  profile_type: register.data.profile_type,
  job_group_name: register.data.job_group_name,
})

const mapDispatchToProps = {
  setInterestName: (interest_name) => registerDispatcher.setData({ interest_name }),
  setStep: (step) => registerDispatcher.setStep(step),
  setPerformHttp: (performHttp) => registerDispatcher.setPerformHttp(performHttp),
}

class Selectgenre extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedGenre: [],
      suggestionGenre: [],
    }
    this._getGenreSuggestion = this._getGenreSuggestion.bind(this)
    this.onSelected = this.onSelected.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { setCtaEnabled } = this.props
    const { step, interest_name } = nextProps
    step != 6 &&
      nextProps.step == 6 &&
      this._getGenreSuggestion() &&
      setCtaEnabled(interest_name.length > 0)
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   const propFields = ['ctaEnabled', 'performHttp', 'interest_name', 'step']
  //   const stateFields = ['suggestionGenre']
  //   return (
  //     (!isEqual(pick(this.props, propFields), pick(nextProps, propFields)) ||
  //       !isEqual(pick(this.state, stateFields), pick(nextState, stateFields)) ||
  //       this.props.interest_name.length != nextProps.interest_name.length ||
  //       true) &&
  //     nextProps.step == 6
  //   )
  // }

  onSelected(item) {
    let { interest_name, setInterestName } = this.props
    const index = interest_name.indexOf(item)
    if (index == -1) {
      interest_name.length < 3 && interest_name.push(item)
    } else interest_name.splice(index, 1);
    Promise.all(setInterestName(interest_name)).then(() => {
      this.setState({ selectedGenre: interest_name })
    });

  }

  onSubmit() {
    let {
      setPerformHttp,
      setStep,
      step,
      interest_name,
      id_user,
      profile_picture,
    } = this.props
    if (!isNil(profile_picture) && profile_picture.indexOf('base64') >= 0)
      profile_picture = profile_picture.split('base64,')[1]
    Promise.resolve(setPerformHttp(true)).then(() => {
      postRegisterV3SelectGenre({
        interest_name,
        id_user,
        profile_picture,
      })
        .then(({ data: { code } }) => {
          // code == 200 && setStep(step + 1)
          code == 200 && setStep(step + 2) // step + 1 => skip setjob step
        })
        .catch((e) => {})
        .then(() => setPerformHttp(false))
    })
  }

  _onSkip = () => {
    const { setPerformHttp, setStep, id_user } = this.props

    Promise.resolve(setPerformHttp(true)).then(() => {
      postRegisterV3Skip({
        id_user,
      })
        .then(({ data: { code } }) => {
          code == 200 && setStep(7)
        })
        .catch((e) => {})
        .then(() => setPerformHttp(false))
    })
  };

  _getGenreSuggestion = () => {
    const { dispatch } = this.props
    dispatch(getGenreSuggestion, {}).then((genre) => {
      this.setState({ suggestionGenre: genre })
    })
    return true
  };

  _genreItem = (genre, index) => {
    return (
      <View
        key={Math.random() + index}
        style={{
          marginRight: WP2,
          marginBottom: WP2,
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: WP3,
          paddingVertical: WP2,
          borderRadius: 6,
          backgroundColor: WHITE,
          borderWidth: 1,
          borderColor: PALE_BLUE_TWO,
        }}
      >
        <Text
          size='xmini'
          type='Circular'
          weight={400}
          color={SHIP_GREY}
          style={{ marginRight: WP205 }}
        >
          {genre}
        </Text>
        <Icon
          onPress={() => this.onSelected(genre)}
          centered
          size='small'
          color={PALE_BLUE_TWO}
          name={'close'}
          type='MaterialCommunityIcons'
        />
      </View>
    )
  };

  render() {
    const { suggestionGenre,selectedGenre } = this.state
    const {
      ctaEnabled,
      interest_name,
      setInterestName,
      performHttp,
      profile_type,
      job_group_name,
      step,
      setStep,
    } = this.props;
    const isGroup = profile_type != 'persosnal'
    return (
      <Wrapper key={Math.random()} showAvatar noHorizontalPadding>
        <View style={{ paddingHorizontal: WP6 }}>
          <Label
            title={isGroup ? 'BASIC INFORMATIONS' : 'INTEREST'}
            titleColor={GUN_METAL}
            marginBottom={WP4}
            titleWeight={600}
          />

          <Label
            title={'Interest:'}
            subtitle={'contoh: UI/UX, React JS, Machine Learning'}
            marginBottom={WP2}
          />
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {selectedGenre &&
              map(selectedGenre, (genre, index) => this._genreItem(genre, index))}
          </View>
          <SelectModalV3
            refreshOnSelect
            disabled={interest_name.length >= 3}
            triggerComponent={
              interest_name.length < 3 ? <ButtonIcon iconName={'plus'} title={'Tambahkan Interest'} /> : null
            }
            header='Interest'
            suggestion={getGenreInterest}
            suggestionKey='interest_name'
            suggestionPathResult='interest_name'
            onChange={(item) => this.onSelected(item)}
            createNew={true}
            placeholder='Cari Interest'
            selection={interest_name}
            selectionWording='Interest Terpilih'
            showSelection
            onRemoveSelection={(index) => {
              const newGenre = interest_name
              newGenre.splice(index, 1)
              Promise.all(setInterestName(newGenre)).then(() => {
                this.setState({ selectedGenre: newGenre })
              })
            }}
            quickSearchTitle={'Saran Pencarian Genre'}
            quickSearchList={suggestionGenre}
          />
          <Spacer size={WP8} />
          <ButtonV2
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Pilih Interest'
            onPress={this.onSubmit}
            disabled={!(ctaEnabled && !performHttp && interest_name.length > 0)}
            textColor={WHITE}
            color={REDDISH}
          />
          {/* <ButtonV2
            onPress={this._onSkip}
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Skip'
            disabled={false}
            textColor={SHIP_GREY_CALM}
            color={NO_COLOR}
          /> */}
        </View>
      </Wrapper>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(Selectgenre),
)
