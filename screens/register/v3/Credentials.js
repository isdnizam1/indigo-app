import React from "react";
import { TextInput, View } from "react-native";
import { isEmpty, isEqual, pick, includes, isNil } from "lodash-es";
import { connect } from "react-redux";
import { Text } from "sf-components";
import Spacer from "sf-components/Spacer";
import Icon from "sf-components/Icon";
import ButtonV2 from "sf-components/ButtonV2";
import Touchable from "sf-components/Touchable";
import ConfirmationModalV3 from "sf-components/ConfirmationModalV3";
import {
  LIPSTICK_TWO,
  SHIP_GREY_CALM,
  REDDISH,
  WHITE,
  NAVY_DARK,
} from "sf-constants/Colors";
import { WP1, WP2, WP4, WP8 } from "sf-constants/Sizes";
import style from "sf-styles/register";
import { postRegisterV3, postCheckEmail } from "sf-actions/api";
import { isEmailValid, isTabletOrIpad } from "sf-utils/helper";
import { registerDispatcher } from "sf-services/register";
import Wrapper from "./Wrapper";
import { setAuthJWT } from "sf-utils/storage";

const mapStateToProps = ({ auth, register }) => ({
  step: register.behaviour.step,
  showPassword: register.behaviour.showPassword,
  ctaEnabled: register.behaviour.ctaEnabled,
  performHttp: register.behaviour.performHttp,
  email: register.data.email,
  id_user: register.data.id_user,
  password: register.data.password,
});

const mapDispatchToProps = {
  setPasswordVisibility: (visible) =>
    registerDispatcher.setPasswordVisibility(visible),
  setCtaEnabled: (enabled) => registerDispatcher.setCtaEnabled(enabled),
  setEmail: (email) => registerDispatcher.setData({ email }),
  setPassword: (password) => registerDispatcher.setData({ password }),
  setIdUser: (id_user) => registerDispatcher.setData({ id_user }),
  setStep: (step) => registerDispatcher.setStep(step),
  setPerformHttp: (performHttp) =>
    registerDispatcher.setPerformHttp(performHttp),
};

class Credentials extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      emailError: null,
      passwordError: null,
      emailAvailable: false,
    };
    this.onChangeText = this.onChangeText.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.checkEmail = this.checkEmail.bind(this);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { step } = this.props;
    step == 1 &&
      nextProps.step != 1 &&
      this.setState({
        modalVisible: false,
        emailError: null,
        passwordError: null,
        emailAvailable: false,
      });
  }

  checkEmail() {
    const { email } = this.props;
    if (!isEmpty(email)) {
      if (!isEmailValid(email)) {
        this.setState({
          emailError: "Email belum sesuai, mohon periksa kembali.",
        });
      } else {
        postCheckEmail({ email }).then(({ data: { status } }) => {
          if (status === "success")
            this.setState({
              emailError: "Email telah digunakan. Gunakan email lainnya",
              emailAvailable: false,
              modalVisible: false,
            });
          else this.setState({ emailError: null, emailAvailable: true });
        });
      }
    }
  }

  toggleModal() {
    this.setState({ modalVisible: !this.state.modalVisible });
  }

  onSubmit() {
    const {
      setPerformHttp,
      setStep,
      setCtaEnabled,
      step,
      email,
      password,
      setIdUser,
      id_user,
    } = this.props;
    if (!isEmailValid(email)) {
      this.setState({ emailError: "Your email is invalid" }, this.toggleModal);
    } else {
      Promise.all([this.toggleModal(), setPerformHttp(true)]).then(() => {
        let postData = { email, password };
        if (!isNil(id_user)) postData.id_user = id_user;
        postRegisterV3(postData)
          .then(async ({ data: { code, message, result } }) => {
            if (includes(message || "", "already"))
              this.setState({ emailError: "Your email address has been used" });
            else if (includes(message || "", "character"))
              this.setState({
                passwordError: "Password must be at least 6 characters long",
              });
            else {
              const { id_user, token } = result;
              setIdUser(id_user.toString());
              await setAuthJWT(token);
              setCtaEnabled(true);
              setStep(step + 1);
            }
          })
          .catch((e) => {
            // keep silent
          })
          .then(() => {
            setPerformHttp(false);
          });
      });
    }
  }

  onChangeText(value, callback, field) {
    Promise.all([
      field === "email" &&
        this.setState({ emailAvailable: false, emailError: null }),
      callback(value),
    ]).then(() => {
      setTimeout(() => {
        const { email, password, setCtaEnabled, id_user } = this.props;
        const isEnabled =
          !isEmpty(email) &&
          !isEmpty(password) &&
          isEmailValid(email) &&
          (password || "").length >= 6;
        setCtaEnabled(isEnabled);
        if (field === "email" && isEmailValid(email)) {
          try {
            typeof this.timeout !== "undefined" && clearTimeout(this.timeout);
          } catch (e) {
            // Keep silent
          } finally {
            this.timeout = setTimeout(() => {
              postCheckEmail({ email }).then(({ data: { status } }) => {
                if (isNil(id_user) && status === "success")
                  this.setState({
                    emailError: "Email telah digunakan. Gunakan email lainnya",
                    emailAvailable: false,
                  });
                else this.setState({ emailError: null, emailAvailable: true });
              });
            }, 500);
          }
        }
        if (field === "password") {
          this.setState({
            passwordError:
              (password || "").length < 6 && !isEmpty(password)
                ? "Password minimal 6 karakter"
                : null,
          });
        }
      }, 250);
    });
  }

  componentDidMount() {
    const { email, password, setCtaEnabled } = this.props;
    setCtaEnabled(!isEmpty(email) && !isEmpty(password));
  }

  shouldComponentUpdate(nextProps, nextState) {
    const propsFields = [
      "ctaEnabled",
      "showPassword",
      "performHttp",
      "id_user",
      "step",
    ];
    const stateFields = [
      "modalVisible",
      "emailError",
      "passwordError",
      "emailAvailable",
    ];
    return (
      !isEqual(pick(this.props, propsFields), pick(nextProps, propsFields)) ||
      !isEqual(pick(this.state, stateFields), pick(nextState, stateFields))
    );
  }

  render() {
    const {
      email,
      password,
      setEmail,
      setPassword,
      showPassword,
      performHttp,
      setPasswordVisibility,
    } = this.props;
    const { modalVisible, emailError, passwordError, emailAvailable } =
      this.state;
    return (
      <View>
        <Wrapper>
          <Spacer size={WP2} />
          <Text
            color={NAVY_DARK}
            size={isTabletOrIpad() ? "mini" : "medium"}
            weight={600}
            type={"Circular"}
          >
            Buat akun
          </Text>
          <Spacer size={WP1} />
          <Text
            style={style.label}
            size={isTabletOrIpad() ? "tiny" : "xmini"}
            weight={400}
            type={"Circular"}
          >
            Buat akunmu dan mulai berkolaborasi
          </Text>
          <Spacer size={WP8} />
          <View style={style.formGroup}>
            <Text
              style={style.label}
              size={isTabletOrIpad() ? "tiny" : "xmini"}
              weight={400}
              type={"Circular"}
            >
              Email
            </Text>
            <TextInput
              onBlur={this.checkEmail}
              defaultValue={email}
              editable={!performHttp}
              onChangeText={(value) =>
                this.onChangeText(value, setEmail, "email")
              }
              keyboardType={"email-address"}
              autoCompleteType={"email"}
              placeholder={"Tuliskan email"}
              style={[
                style.input,
                emailError ? style.inputError : null,
                { opacity: performHttp ? 0.6 : 1 },
              ]}
            />
            {!isEmpty(emailError) && !isEmpty(email) && (
              <Text
                size={isTabletOrIpad() ? "petite" : "tiny"}
                color={LIPSTICK_TWO}
                weight={400}
                type={"Circular"}
              >
                {emailError}
              </Text>
            )}
          </View>
          <View style={style.formGroup}>
            <Text
              style={style.label}
              size={isTabletOrIpad() ? "tiny" : "xmini"}
              weight={400}
              type={"Circular"}
            >
              Password
            </Text>
            <View>
              <TextInput
                defaultValue={password}
                onChangeText={(value) =>
                  this.onChangeText(value, setPassword, "password")
                }
                secureTextEntry={!showPassword}
                autoCompleteType={"password"}
                placeholder={"Tuliskan password"}
                style={[
                  style.input,
                  passwordError ? style.inputError : null,
                  { opacity: performHttp ? 0.6 : 1 },
                ]}
              />
              {/*<Text
              onPress={() => setPasswordVisibility(!showPassword)}
              style={style.passwordToggler}
              size={isTabletOrIpad() ? 'xtiny' : 'tiny'}
              color={showPassword ? GREY : GREY_PLACEHOLDER}
              weight={400}
              type={'Circular'}
            >
              {showPassword ? 'Hide' : 'Show'}
            </Text>*/}
              <Touchable
                onPress={() => setPasswordVisibility(!showPassword)}
                style={style.passwordToggler}
              >
                <Icon
                  color={SHIP_GREY_CALM}
                  type={"MaterialCommunityIcons"}
                  name={showPassword ? "eye-off" : "eye"}
                />
              </Touchable>
            </View>
            {!isEmpty(passwordError) && !isEmpty(password) && (
              <Text
                size={isTabletOrIpad() ? "petite" : "tiny"}
                color={LIPSTICK_TWO}
                weight={400}
                type={"Circular"}
              >
                {passwordError}
              </Text>
            )}
            <Spacer size={WP8} />
            <ButtonV2
              style={{ paddingVertical: WP4 }}
              textSize={"slight"}
              text="Buat Akun"
              onPress={() =>
                Promise.all([this.toggleModal(), this.checkEmail()])
              }
              disabled={emailError || passwordError || !password || !email}
              textColor={WHITE}
              color={REDDISH}
            />
          </View>
        </Wrapper>
        <ConfirmationModalV3
          visible={modalVisible && !emailError && emailAvailable}
          title={`Lanjutkan buat akun dengan ${email}`}
          subtitle="Belum ada akun yang menggunakan email ini"
          primary={{
            text: "Ya, Lanjutkan",
            onPress: this.onSubmit,
          }}
          secondary={{
            text: "Batalkan",
            onPress: this.toggleModal,
          }}
        />
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Credentials);
