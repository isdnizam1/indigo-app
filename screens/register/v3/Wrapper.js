import React from 'react'
import { View } from 'react-native'
import { isEmpty, noop } from 'lodash-es'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'
import { Text } from 'sf-components'
import Avatar from 'sf-components/register/v3/Avatar'
import Divider from 'sf-components/Divider'
import Spacer from 'sf-components/Spacer'
import { NAVY_DARK } from 'sf-constants/Colors'
import { WP3, WP7 } from 'sf-constants/Sizes'
import style from 'sf-styles/register'
import { isTabletOrIpad } from 'sf-utils/helper'
import { registerDispatcher } from 'sf-services/register'
import { PALE_WHITE } from '../../../constants/Colors'
import { HEADER } from '../../../constants/Styles'

const mapStateToProps = ({ auth, register }) => ({
  step: register.behaviour.step,
  name: register.data.name,
  job_title: register.data.job_title,
  profile_type: register.data.profile_type,
  job_group_name: register.data.job_group_name,
  company_name: register.data.company_name
})

const mapDispatchToProps = {
  setStep: (step) => registerDispatcher.setStep(step)
}

class Wrapper extends React.PureComponent {

  render() {

    const {
      headings,
      children,
      name,
      noHorizontalPadding,
      showAvatar,
      wrapperStyle = {},
      step
    } = this.props

    return (<View style={style.wrapper}>
      {/*!cantBack && (<TouchableOpacity
        onPress={!cantBack ? (customBack || handleBackButton) : null}
        style={style.arrowBack}
      >
        <Icon
          size={24}
          color={cantBack ? WHITE : GREY}
          name={'ios-arrow-back'}
        />
      </TouchableOpacity>)*/}
      <KeyboardAwareScrollView
        ref={this.props.onScrollViewRef || noop}
        keyboardShouldPersistTaps={'handled'}
        keyboardDismissMode={'on-drag'}
        style={[style.contentScrollView, { backgroundColor: PALE_WHITE }, noHorizontalPadding ? style.noHorizontalPadding : null, wrapperStyle]}
        extraScrollHeight={HEADER.height}
      >
        {showAvatar && <Spacer size={WP7} />}
        {showAvatar && isEmpty(headings) && (<View style={style.headingProfession}>
          {showAvatar && (<Avatar />)}
          <Spacer size={WP3} />
          {step != 3 && <Text
            color={NAVY_DARK}
            weight={600}
            size={isTabletOrIpad() ? 'xmini' : 'medium'}
            type={'Circular'}
                        >{name}</Text>}
          {/*<Text
            style={style.headingJobTitle}
            weight={500}
            size={isTabletOrIpad() ? 'xmini' : 'medium'}
            type={'NeoSans'}
          >{profile_type == 'personal' ? (job_title || job_group_name) : null} {profile_type == 'personal' && company_name ? `at ${company_name}` : (profile_type == 'group' ? job_group_name : null)}</Text>*/}
        </View>)}
        {showAvatar && <Spacer size={WP7} />}
        {showAvatar && <Divider />}
        <View style={style.wrapperInner}>
          {children}
        </View>
      </KeyboardAwareScrollView>
    </View>)

  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Wrapper)
