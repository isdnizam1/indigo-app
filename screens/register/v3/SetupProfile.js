/* eslint-disable react/sort-comp */
import React from 'react'
import {
  Animated,
  KeyboardAvoidingView,
  Platform,
  Clipboard,
  Image,
  TextInput,
  TouchableOpacity,
  View,
  Modal,
} from 'react-native'
import {
  isEmpty,
  isEqual,
  isNil,
  isObject,
  pick,
  startCase,
  toLower,
  get,
} from 'lodash-es'
import { connect } from 'react-redux'
import { Text } from 'sf-components'
import {
  REDDISH,
  PALE_WHITE,
  GREEN_TOSCA,
  GUN_METAL,
  NAVY_DARK,
  LIPSTICK_TWO,
  PALE_BLUE,
  SHIP_GREY_CALM,
  WHITE,
} from 'sf-constants/Colors'
import { WP1, WP2, WP4, WP5, WP6, WP8 } from 'sf-constants/Sizes'
import {
  getCity,
  getCompany,
  getDetailReferralCode,
  postRegisterV3SetupProfile,
} from 'sf-actions/api'
import style from 'sf-styles/register'
import { isTabletOrIpad } from 'sf-utils/helper'
import { registerDispatcher } from 'sf-services/register'
import { SHADOW_STYLE } from 'sf-constants/Styles'
import SFImage from 'sf-components/Image'
import SelectModalV3 from 'sf-components/SelectModalV3'
import Icon from 'sf-components/Icon'
import Spacer from 'sf-components/Spacer'
import ButtonV2 from 'sf-components/ButtonV2'
import { RED_GOOGLE } from 'sf-constants/Colors'
import SelectionBar from '../../../components/SelectionBar'
import { GREEN_30, SHIP_GREY } from '../../../constants/Colors'
import Wrapper from './Wrapper'

const icTickTosca = require('sf-assets/icons/tickCircleToscaNoPadding.png')
const DEFAULT_REFERRAL_POSITION = -600
const PROFILE_TYPES = ['personal', 'group']
const mapStateToProps = ({
  auth,
  register,
  helper: {
    keyboard: { visible: keyboardShown },
  },
}) => ({
  step: register.behaviour.step,
  ctaEnabled: register.behaviour.ctaEnabled,
  performHttp: register.behaviour.performHttp,
  name: register.data.name,
  profile_type: register.data.profile_type,
  id_city: register.data.id_city,
  id_user: register.data.id_user,
  city_name: register.data.city_name,
  id_city: register.data.id_city,
  id_company: register.data.id_company,
  company_name: register.data.company_name,
  referral_code: register.data.referral_code,
  profile_picture: register.data.profile_picture,
  gender: register.data.gender,
  registered_via: register.data.registered_via,
  keyboardShown,
})
const mapDispatchToProps = {
  setCtaEnabled: (enabled) => registerDispatcher.setCtaEnabled(enabled),
  setName: (name) => registerDispatcher.setData({ name }),
  setIdCity: (id_city) => registerDispatcher.setData({ id_city }),
  setIdCompany: (id_company) => registerDispatcher.setData({ id_company }),
  setCityName: (city_name) => registerDispatcher.setData({ city_name }),
  setCompanyName: (company_name) => registerDispatcher.setData({ company_name }),
  setProfileType: (profile_type) => registerDispatcher.setData({ profile_type }),
  setReferralCode: (referral_code) => registerDispatcher.setData({ referral_code }),
  setStep: (step) => registerDispatcher.setStep(step),
  setPerformHttp: (performHttp) => registerDispatcher.setPerformHttp(performHttp),
  setProfilePicture: (base64) =>
    registerDispatcher.setData({ profile_picture: base64 }),
  setRegisterData: (data) => registerDispatcher.setData(data),
}

const Config = {
  personal: {
    headerLabel: 'PROFILE PERSONAL',
    nameLabel: 'Nama',
    namePlaceholder: 'Tuliskan nama kamu',
    defaultAvatar: require('sf-assets/images/default_avatar.png'),
    selectProfile: {
      header: 'Peserta',
      subHeader: 'Buat profile untuk peserta',
      image: require('sf-assets/images/profile_type_personal.png'),
    },
  },
  music_enthusiast: {
    headerLabel: 'PROFILE PERSONAL',
    nameLabel: 'Nama',
    namePlaceholder: 'Tuliskan nama panggung/pribadi kamu',
    defaultAvatar: require('sf-assets/images/default_avatar.png'),
    selectProfile: {
      header: 'Music Enthusiast',
      subHeader: 'Buat sebagai penikmat musik',
      image: require('sf-assets/images/music_enthusiast.png'),
    },
  },
  group: {
    headerLabel: 'PROFILE PANITIA PENYELENGGARA',
    nameLabel: 'Nama',
    namePlaceholder: 'Tuliskan nama  kamu',
    defaultAvatar: require('sf-assets/images/default_avatar_group.png'),
    selectProfile: {
      header: 'Penyelenggara',
      subHeader: 'Buat profile untuk penyelenggara',
      image: require('sf-assets/images/profile_type_group.png'),
    },
  },
}

class SetupProfile extends React.Component {
  scrollView = React.createRef();
  nameInput = React.createRef();
  referralInput = React.createRef();
  referralAnim = new Animated.Value(DEFAULT_REFERRAL_POSITION);

  constructor(props) {
    super(props)
    this.state = {
      activeIndex: 0,
      isValidReferral: null,
      scrollEnabled: false,
      referal: '',
      showReferalPopup: false,
      dontHaveReferral: false,
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.onPasteReferral = this.onPasteReferral.bind(this)
    this.validateReferral = this.validateReferral.bind(this)
    this.shouldCtaEnabled = this.shouldCtaEnabled.bind(this)
  }

  async onPasteReferral() {
    const text = await Clipboard.getString()
    this.referralInput.setNativeProps({ text })
    this.validateReferral(text)
  }

  validateReferral() {
    const { setReferralCode } = this.props
    const { referal } = this.state
    !isEmpty(referal) &&
      getDetailReferralCode({ referral_code: referal })
        .then(({ data }) => {
          const { code } = data
          this.setState({ isValidReferral: code == 200 }, () => {
            code == 200 && setReferralCode(referal)
          })
        })
        .catch((e) => {})
        .then(this.shouldCtaEnabled)
  }

  onSubmit() {
    let {
      setPerformHttp,
      setStep,
      step,
      name,
      id_user,
      id_city,
      id_company,
      referral_code,
      profile_type,
      profile_picture,
      gender,
    } = this.props
    if (!isNil(profile_picture) && profile_picture.indexOf('base64') >= 0)
      profile_picture = profile_picture.split('base64,')[1]
    Promise.resolve(setPerformHttp(true)).then(() => {
      postRegisterV3SetupProfile({
        name,
        id_city,
        id_user,
        referral_code,
        profile_type,
        id_company,
        profile_picture,
        gender,
      })
        .then(({ data }) => {
          if(profile_type=='group'){
            setStep(step + 3)
          }else{
          setStep(step + 1 )
          }
          this.nameInput.blur()
        })
        .catch((e) => {})
        .then(() => setPerformHttp(false))
    })
  }

  componentDidMount() {}

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { name, id_city, step, setCtaEnabled } = this.props
    const { dontHaveReferral, isValidReferral } = this.state
    if (nextProps.step == 3) {
      step != 3 &&
        setCtaEnabled(
          !isEmpty(name) && !isNil(id_city) && (isValidReferral || dontHaveReferral),
        )
    }
  }
  shouldCtaEnabled() {
    const {
      name,
      id_city,
      setCtaEnabled,
      ctaEnabled,
      gender,
      profile_type,
      registered_via,
    } = this.props
    const { isValidReferral, dontHaveReferral } = this.state
    const validGender =true;
    // const validGender =
    //   profile_type !== 'group'
    //     ? registered_via !== 'apple'
    //       ? !isEmpty(gender)
    //       : true
    //     : true
    const validCity = registered_via !== 'apple' ? !isNil(id_city) : true
    const nextCtaStatus =
      !isEmpty(name) &&
      (isValidReferral || dontHaveReferral) &&
      validGender
    ctaEnabled != nextCtaStatus && setCtaEnabled(nextCtaStatus)
  }

  shouldComponentUpdate(nextProps, nextState) {
    const propFields = [
      'ctaEnabled',
      'performHttp',
      'city_name',
      'profile_picture',
      'id_city',
      'referral_code',
      'step',
      'profile_type',
      'name',
      'gender',
      'keyboardShown',
    ]
    const stateFields = [
      'activeIndex',
      'isValidReferral',
      'scrollEnabled',
      'referal',
      'showReferalPopup',
      'dontHaveReferral',
    ]
    return (
      (!isEqual(pick(this.props, propFields), pick(nextProps, propFields)) ||
        !isEqual(pick(this.state, stateFields), pick(nextState, stateFields))) &&
      nextProps.step == 3
    )
  }

  referralLink() {
    const { referral_code } = this.props
    return isEmpty(referral_code) ? (
      <TouchableOpacity
        onPress={() => {
          this.setState({ showReferalPopup: true }, () => {
            setTimeout(() => {
              try {
                this.referralInput.focus()
              } catch (err) {
                // keep silent
              }
            }, 500)
          })
        }}
        activeOpacity={0.8}
      >
        <View style={{ flexDirection: 'row', paddingVertical: WP2 }}>
          <Text type={'Circular'} size={isTabletOrIpad() ? 'xtiny' : 'xmini'}>
            {'Punya '}
          </Text>
          <Text
            weight={500}
            type={'Circular'}
            size={isTabletOrIpad() ? 'xtiny' : 'xmini'}
          >
            kode referal?
          </Text>
        </View>
      </TouchableOpacity>
    ) : (
      <View
        style={{
          flexDirection: 'row',
          paddingVertical: WP2,
          alignItems: 'center',
        }}
      >
        <Text
          style={{ marginRight: WP2 }}
          weight={500}
          type={'Circular'}
          size={isTabletOrIpad() ? 'xtiny' : 'xmini'}
        >
          Referral Code Active
        </Text>
        <Image
          style={{ width: 20, height: 20, borderRadius: 10 }}
          source={icTickTosca}
        />
      </View>
    )
  }

  _onChangeName = async (value) => {
    const { setName } = this.props
    if ((value || '').length < 54) await setName(value)
    this.shouldCtaEnabled()
  };

  _onChangeCity = async (city) => {
    const { setIdCity, setCityName } = this.props
    await setIdCity(city.id_city)
    await setCityName(city.city_name)
    this.shouldCtaEnabled()
  };

  _onChangeProgram = async (company) => {
    const { setIdCompany, setCompanyName } = this.props
    await setIdCompany(company.id_company)
    await setCompanyName(company.company_name)
    this.shouldCtaEnabled()
  };

  _onChangeGender = async (gender) => {
    const { setRegisterData } = this.props
    await setRegisterData({ gender })
    this.shouldCtaEnabled()
  };

  _renderProfileForm = () => {
    const { isValidReferral } = this.state
    const {
      ctaEnabled,
      name,
      id_city,
      city_name,
      referral_code,
      profile_type,
      gender,
      performHttp,
      registered_via,
      company_name,
      id_company
    } = this.props

    const isButtonEnabled =
      !isEmpty(name)  && !isEmpty(gender) 
      // && !isEmpty(company_name) 


    return (
      <View>
        <Wrapper showAvatar>
          <View>
            <Text
              color={GUN_METAL}
              size={isTabletOrIpad() ? 'xmini' : 'mini'}
              weight={600}
              type={'Circular'}
            >
              {get(Config[profile_type], 'headerLabel')}
            </Text>

            <Spacer size={WP4} />

            <View style={style.formGroup}>
              <Text
                style={style.label}
                size={isTabletOrIpad() ? 'tiny' : 'xmini'}
                weight={400}
                type={'Circular'}
              >
                {get(Config[profile_type], 'nameLabel')}
              </Text>
              <TextInput
                value={name}
                editable={!performHttp}
                onChangeText={this._onChangeName}
                autoCompleteType={'name'}
                placeholder={get(Config[profile_type], 'namePlaceholder')}
                style={style.input}
              />
            </View>
            

            <View style={{ ...style.formGroup, marginBottom: WP2 }}>
                    <Text
                      style={style.label}
                      size={isTabletOrIpad() ? 'tiny' : 'xmini'}
                      weight={400}
                      type={'Circular'}
                    >
                      Gender
                    </Text>
                    <SelectionBar
                      options={[
                        { key: 'male', text: 'Male' },
                        { key: 'female', text: 'Female' },
                      ]}
                      selectedValue={gender}
                      onPress={this._onChangeGender}
                      style={{ marginBottom: WP4 }}
                    />
                  </View>
            {registered_via !== 'apple' && profile_type !== 'group'  && (
              <>
                <SelectModalV3
                  refreshOnSelect
                  triggerComponent={
                    <View style={style.formGroup}>
                      <Text
                        style={style.label}
                        size={isTabletOrIpad() ? 'tiny' : 'xmini'}
                        weight={400}
                        type={'Circular'}
                      >
                        Kota
                      </Text>
                      <TextInput
                        defaultValue={city_name}
                        editable={false}
                        placeholder='Tuliskan kota domisili kamu saat ini'
                        style={style.input}
                      />
                    </View>
                  }
                  header='Kota/Lokasi'
                  suggestion={getCity}
                  suggestionKey='city_name'
                  suggestionPathResult='city_name'
                  suggestionPathValue='id_city'
                  onChange={this._onChangeCity}
                  createNew={true}
                  reformatFromApi={(text) => startCase(toLower(text))}
                  placeholder='Coba cari Kota'
                  selection={city_name}
                  showSelection
                  onRemoveSelection={() => this._onChangeCity({})}
                  asObject
                />
              
              </>
            )}

               

                    {/* <SelectModalV3
                  refreshOnSelect
                  triggerComponent={
                    <View style={style.formGroup}>
                      <Text
                        style={style.label}
                        size={isTabletOrIpad() ? 'tiny' : 'xmini'}
                        weight={400}
                        type={'Circular'}
                      >
                        Program
                      </Text>
                      <TextInput
                        defaultValue={company_name}
                        editable={false}
                        placeholder='Tuliskan program yang kamu ikuti'
                        style={style.input}
                      />
                    </View>
                  }
                  header='Program'
                  suggestion={getCompany}
                  suggestionKey='company_name'
                  suggestionPathResult='company_name'
                  suggestionPathValue='id_company'
                  onChange={this._onChangeProgram}
                  createNew={false}
                  reformatFromApi={(text) => startCase(toLower(text))}
                  placeholder='Coba cari Program'
                  selection={company_name}
                  showSelection
                  onRemoveSelection={() => this._onChangeProgram({})}
                  asObject
                /> */}


            <View style={{ ...style.formGroup, marginBottom: 0 }}>
              <Text
                style={style.label}
                size={isTabletOrIpad() ? 'tiny' : 'xmini'}
                weight={400}
                type={'Circular'}
              >
                Kode Referral
              </Text>
              {!this.state.dontHaveReferral && (
                <View>
                  <TextInput
                    autoCapitalize={'characters'}
                    defaultValue={referral_code || this.state.referal}
                    ref={(ref) => {
                      this.referralInput = ref
                    }}
                    onChangeText={(referal) =>
                      this.setState(
                        { referal, isValidReferral: null },
                        this.validateReferral,
                      )
                    }
                    editable={!performHttp}
                    autoCompleteType={'name'}
                    placeholder={'Tulis kode referral'}
                    style={style.input}
                  />
                  {!isEmpty(this.state.referal) && !!isValidReferral && (
                    <Text color={GREEN_30} size={'xmini'}>
                      Your referral code is valid
                    </Text>
                  )}
                  {!isEmpty(this.state.referal) && !isValidReferral && (
                    <Text color={RED_GOOGLE} size={'xmini'}>
                      Your referral code is invalid
                    </Text>
                  )}
                </View>
              )}
            </View>
            <TouchableOpacity
              onPress={() => {
                this.setState(
                  { dontHaveReferral: !this.state.dontHaveReferral },
                  this.shouldCtaEnabled,
                )
              }}
            >
              <View
                style={{
                  paddingTop: WP2,
                  paddingBottom: WP2,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <Icon
                  size={'huge'}
                  color={!this.state.dontHaveReferral ? SHIP_GREY_CALM : REDDISH}
                  type='MaterialCommunityIcons'
                  name={
                    !this.state.dontHaveReferral
                      ? 'checkbox-blank-outline'
                      : 'checkbox-marked'
                  }
                />
                <Spacer size={WP2} horizontal />
                <Text type='Circular' size={'xmini'} color={SHIP_GREY_CALM}>
                  Tidak memiliki kode referral
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <Spacer size={WP4} />
          <ButtonV2
            onPress={this.onSubmit}
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Buat Akun'
            disabled={!isButtonEnabled}
            textColor={WHITE}
            color={REDDISH}
          />
        </Wrapper>

        {/* {!this.props.keyboardShown && (
          <View style={{ paddingHorizontal: WP8, backgroundColor: PALE_WHITE }}>
            <View
              style={{
                borderTopColor: PALE_BLUE,
                borderTopWidth: 1,
                alignItems: 'center',
                paddingVertical: WP6,
              }}
            >
              {this.referralLink()}
            </View>
          </View>
        )} */}
        <Modal transparent visible={this.state.showReferalPopup}>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            style={{ flex: 1, justifyContent: 'flex-end' }}
          >
            <View style={style.referralBackground}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginBottom: WP1,
                  paddingLeft: WP6,
                  borderBottomWidth: 1,
                  borderTopWidth: 1,
                  borderColor: PALE_BLUE,
                }}
              >
                <Text
                  size={'slight'}
                  weight={600}
                  color={NAVY_DARK}
                  type={'Circular'}
                >
                  Kode Referal
                </Text>
                <TouchableOpacity
                  style={{ paddingVertical: WP4, paddingHorizontal: WP6 }}
                  onPress={() => {
                    try {
                      this.referralInput.blur()
                    } finally {
                      this.setState({ showReferalPopup: false })
                    }
                  }}
                >
                  <Icon size={'large'} name={'close'} />
                </TouchableOpacity>
              </View>
              {!this.state.dontHaveReferral && (
                <View
                  style={{
                    marginTop: WP4,
                    marginBottom: WP1,
                    paddingHorizontal: WP6,
                  }}
                >
                  <Text
                    style={style.label}
                    size={isTabletOrIpad() ? 'tiny' : 'xmini'}
                    weight={400}
                    type={'Circular'}
                  >
                    Kode Referal
                  </Text>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flex: 1 }}>
                      <TextInput
                        defaultValue={referral_code || this.state.referal}
                        ref={(ref) => {
                          this.referralInput = ref
                        }}
                        onChangeText={(referal) =>
                          this.setState({ referal, isValidReferral: null })
                        }
                        returnKeyType='return'
                        onSubmitEditing={() =>
                          this.validateReferral(this.state.referal)
                        }
                        style={[
                          style.input,
                          isValidReferral == false ? style.inputError : null,
                        ]}
                      />
                    </View>
                    <View style={{ paddingLeft: WP4 }}>
                      <ButtonV2
                        onPress={() => this.validateReferral(this.state.referal)}
                        style={{
                          height: isTabletOrIpad() ? 50 : 38,
                          paddingHorizontal: WP6,
                        }}
                        textColor={WHITE}
                        color={REDDISH}
                        text={'Pakai'}
                        disabled={!this.state.referal}
                      />
                    </View>
                  </View>
                  {/*<Text
                  onPress={this.onPasteReferral}
                  style={{ ...style.passwordToggler, paddingTop: 20 }}
                  size={isTabletOrIpad() ? 'xtiny' : 'xmini'}
                  color={GREY}
                  weight={500}
                  type={'Circular'}
                >
                  Paste
                </Text>*/}
                </View>
              )}
              {isValidReferral != null && (
                <TouchableOpacity
                  onPress={() => this.setState({ isValidReferral: null })}
                  style={{ paddingHorizontal: WP6 }}
                >
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text
                      color={isValidReferral ? GREEN_TOSCA : LIPSTICK_TWO}
                      size={'tiny'}
                      weight={400}
                      type={'Circular'}
                    >
                      {!isValidReferral ? 'Kode tidak valid' : 'Kode referal valid'}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            </View>
          </KeyboardAvoidingView>
        </Modal>
      </View>
    )
  };

  _renderSelectProfile = () => {
    const { setRegisterData } = this.props

    return (
      <Wrapper noHorizontalPadding>
        <View style={{ paddingHorizontal: WP6 }}>
          <View style={{ marginBottom: WP6 }}>
            <Text
              color={NAVY_DARK}
              size={isTabletOrIpad() ? 'mini' : 'medium'}
              weight={600}
              type={'Circular'}
            >
              Kategori profile
            </Text>
            <Spacer size={WP1} />
            <Text
              style={style.label}
              size={isTabletOrIpad() ? 'tiny' : 'xmini'}
              weight={400}
              type={'Circular'}
            >
              Buat profile yang sesuai dengan kebutuhanmu
            </Text>
          </View>
          <View>
            {PROFILE_TYPES.map((profile) => {
              const profileOptions = Config[profile].selectProfile
              return (
                <TouchableOpacity
                  key={profile}
                  style={{
                    ...SHADOW_STYLE.shadow,
                    shadowOpacity: 0.1,
                    padding: WP4,
                    backgroundColor: WHITE,
                    marginBottom: WP5,
                    borderRadius: 5,
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '100%',
                  }}
                  onPress={async () => {
                    setRegisterData({
                      profile_type: profile,
                    })
                    await setRegisterData({
                      // name: undefined,
                      // job_group_name: undefined,
                      // other_job_group_name: undefined,
                      // profile_picture: undefined,
                      // id_city: undefined,
                      // city_name: undefined,
                      gender: undefined,
                    })
                    this.shouldCtaEnabled()
                  }}
                >
                  <SFImage source={profileOptions.image} size='medium' />
                  <Spacer horizontal size={WP2} />
                  <View
                    style={{
                      paddingHorizontal: WP2,
                      flex: 1,
                      justifyContent: 'center',
                    }}
                  >
                    <Text type='Circular' color={GUN_METAL} weight={400}>
                      {profileOptions.header}
                    </Text>
                    <Text
                      type='Circular'
                      color={SHIP_GREY_CALM}
                      size='xmini'
                      ellipsizeMode='tail'
                    >
                      {profileOptions.subHeader}
                    </Text>
                  </View>
                  <Icon
                    type='Entypo'
                    name='chevron-right'
                    centered
                    color={SHIP_GREY_CALM}
                  />
                </TouchableOpacity>
              )
            })}
          </View>
        </View>
      </Wrapper>
    )
  };

  render() {
    const { profile_type } = this.props

    // return !profile_type ? this._renderSelectProfile() : this._renderProfileForm()
    return this._renderProfileForm()
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SetupProfile)
