import React, { Fragment } from 'react'
import { ImageBackground, View } from 'react-native'
import { withNavigation } from '@react-navigation/compat'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash-es'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { _enhancedNavigation, ProfileCardWork } from '../../components'
import { GREY_CALM, GREY_WARM, WHITE, TEAL } from '../../constants/Colors'
import Text from '../../components/Text'
import { WP05, WP1, WP105, WP2, WP3, WP5, WP6, WP10, WP100, WP4, HP05, HP1 } from '../../constants/Sizes'
import Icon from '../../components/Icon'
import { fullDateToMonthDate, toMonthDate } from '../../utils/date'
import { postRemoveExperiences } from '../../actions/api'
import { TOUCH_OPACITY } from '../../constants/Styles'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

const styles = {
  container: {
    backgroundColor: GREY_CALM
  },
  editIcon: {
    position: 'absolute',
    right: WP5,
    top: WP2
  },
  header: {
    wrapper: {
      backgroundColor: WHITE,
      marginTop: WP105,
      marginBottom: WP05,
      paddingVertical: WP2,
      paddingHorizontal: WP5,
      flexDirection: 'row',
      alignItems: 'center'
    },
    iconWrapper: {
      marginRight: WP3
    },
    icon: {
      height: WP6
    },
    text: {},
    seeAll: {
      backgroundColor: WHITE,
      marginTop: WP105,
      marginBottom: WP05,
      paddingVertical: WP2,
      paddingHorizontal: WP5,
      alignItems: 'center'
    }
  },
  experience: {
    wrapper: {
      backgroundColor: WHITE
    },
    title: {
      marginBottom: WP1
    },
    info: {
      marginTop: WP05,
      marginBottom: WP1
    },
    description: {
      marginBottom: WP1
    },
    subHeader: {
      size: 'mini'
    },
    infoText: {
      size: 'tiny'
    },
    mediaWrapper: {
      flex: 1
    },
    media: {
      flex: 1, aspectRatio: 2,
      flexDirection: 'column-reverse'
    }
  }
}

const experienceRenderOption = (experience) => {
  const additionalData = JSON.parse(experience.additional_data)
  if (experience.type === 'music_journey') {
    return {
      title: additionalData.role,
      subHeader: (
        <Fragment>
          <Text {...styles.experience.subHeader}>at {additionalData.company}</Text>
        </Fragment>
      ),
      info: (
        <Fragment>
          <Text {...styles.experience.infoText}>{toMonthDate(additionalData.date_join.start)}</Text>
          <Icon centered color={GREY_WARM} type='Entypo' name='dot-single'/>
          <Text {...styles.experience.infoText}>{toMonthDate(additionalData.date_join.until)}</Text>
        </Fragment>
      )
    }
  }

  if (experience.type === 'performance_journey')
    return {
      title: experience.title,
      subHeader: (
        <Fragment>
          <Text {...styles.experience.subHeader}>at {additionalData.event_location}</Text>
        </Fragment>
      ),
      info: (
        <Fragment>
          <Text {...styles.experience.infoText}>{fullDateToMonthDate(additionalData.event_date)}</Text>
        </Fragment>
      )
    }

  return {}
}

class ExperiencesTab extends React.Component {
  _renderJourney = (experience, experienceConstant, type) => {
    const additionalData = JSON.parse(experience.additional_data)
    const { navigateTo } = this.props
    return (
      <TouchableOpacity activeOpacity={TOUCH_OPACITY} onPress={() => navigateTo('DetailExperienceScreen', { data: experience })} style={styles.experience.wrapper}>
        {
          !isEmpty(additionalData.media) && (
            <View style={styles.experience.mediaWrapper}>
              {
                additionalData.media.slice(0, 1).map((item, index) => (
                  <TouchableOpacity
                    key={`${index}${new Date()}`}
                    onPress={() => navigateTo('ImagePreviewScreen', { images: this._getImageFromMedia(additionalData.media) })}
                  >
                    <ImageBackground
                      style={styles.experience.media}
                      imageStyle={{ aspectRatio: 1, borderTopLeftRadius: 12, borderTopRightRadius: 12 }}
                      source={{ uri: item }}
                    >
                      <View style={{ backgroundColor: TEAL, borderTopRightRadius: WP100, paddingHorizontal: WP4, paddingVertical: WP1, alignSelf: 'baseline' }}>
                        <Text size='tiny' type='NeoSans' weight={500} color={WHITE}>{type}</Text>
                      </View>
                    </ImageBackground>
                  </TouchableOpacity>
                ))
              }
            </View>
          )
        }
        <View style={{ paddingLeft: WP4, marginVertical: WP2 }}>
          <Text weight={500} style={styles.experience.title} size='mini'>{experienceConstant.title}</Text>
          <View style={{ flexDirection: 'row' }}>
            {experienceConstant.subHeader}
          </View>
          <View style={{ flexDirection: 'row' }}>
            {experienceConstant.info}
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _getImageFromMedia = (media) => {
    const images = []
    media.map((item) => {
      const image = {
        url: item
      }
      images.push(image)
    })
    return images
  }

  _deleteExperience = async (id_journey) => {
    const { getExperiences, dispatch } = this.props
    try {
      await dispatch(postRemoveExperiences, { id_journey })
    } catch (e) {
      //
    }
    await getExperiences()
  }

  _editExperience = async (experience) => {
    const {
      navigateTo,
      getExperiences
    } = this.props

    if (experience.type === 'music_journey') {
      navigateTo('EditExperienceMusic', { refreshData: getExperiences, experience })
    } else if (experience.type === 'performance_journey') {
      navigateTo('EditExperiencePerformance', { refreshData: getExperiences, experience })
    } else {
      // wrong
    }
  }

  render() {
    const {
      experiences,
      isTabReady,
      isMine,
      getExperiences,
      navigateTo,
      user,
    } = this.props

    if (!isTabReady) return (
      <View />
    )

    return (
      <View style={{ flexGrow: 0, backgroundColor: WHITE }}>
        <ProfileCardWork
          actionAdd={() => {
            navigateTo('CreateExperience', { refreshProfile: getExperiences })
          }}
          actionEdit={this._editExperience}
          actionDelete={this._deleteExperience}
          header='Performance Journey'
          category='Performance'
          isMine={isMine}
          dataSource={experiences['performance']}
          isEditing={false}
          user={user}
          refreshProfile={getExperiences}
          navigateTo={navigateTo}
          noDescription
          limit
          limitNum={1}
          noCardPadding
          idPerItem='id_journey'
          itemMargin={HP1}
          renderContent={
            ({ data, renderAction }) => {
              return (
                <View>
                  {
                    this._renderJourney(data, experienceRenderOption(data), 'Performance Journey')

                  }
                  {
                    renderAction()
                  }
                </View>
              )
            }}
        />
        <View style={{ marginVertical: HP05 }} />
        <ProfileCardWork
          actionAdd={() => {
            navigateTo('CreateExperience', { refreshProfile: getExperiences })
          }}
          actionEdit={this._editExperience}
          actionDelete={this._deleteExperience}
          header='Music Experience'
          category='Journey'
          isMine={isMine}
          dataSource={experiences['music']}
          isEditing={false}
          noDescription
          noCardPadding
          user={user}
          refreshProfile={getExperiences}
          navigateTo={navigateTo}
          limit
          limitNum={1}
          idPerItem='id_journey'
          itemMargin={HP1}
          renderContent={
            ({ data, renderAction }) => {
              return (
                <View>
                  {
                    this._renderJourney(data, experienceRenderOption(data), 'Music Experience')

                  }
                  {
                    renderAction()
                  }
                </View>
              )
            }}
        />
        <View style={{ marginVertical: WP10 }} />
      </View>

    )
  }
}

export default withNavigation(_enhancedNavigation(
  connect(mapStateToProps, {})(ExperiencesTab),
  mapFromNavigationParam
))
