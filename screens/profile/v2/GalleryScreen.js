import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import {
  Image,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
  StatusBar,
} from 'react-native'
import Constants from 'expo-constants'
import { connect } from 'react-redux'
import { isObject, isNil, isEmpty, noop } from 'lodash-es'
import Swiper from 'react-native-swiper'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'
import SoundfrenExploreOptions from 'sf-components/explore/SoundfrenExploreOptions'
import { Modal } from 'sf-components'
import { postProfileDelJourney } from 'sf-actions/api'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import Icon from '../../../components/Icon'
import Text from '../../../components/Text'
import Avatar from '../../../components/Avatar'
import {
  WP105,
  WP2,
  WP4,
  WP5,
  WP12,
  WP15,
  WP100,
} from '../../../constants/Sizes'
import {
  WHITE,
  SHIP_GREY_CALM,
  TRANSPARENT,
  GUN_METAL,
  REDDISH,
} from '../../../constants/Colors'
import _enhancedNavigation from '../../../navigation/_enhancedNavigation'
import InteractionManager from '../../../utils/InteractionManager'
import ArtworkMenu from './ProfileScreen/TabComponents/ArtworkMenu'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  images: getParam('images', []),
  index: getParam('index', 0),
  refreshProfile: getParam('refreshProfile', noop),
  profile: getParam('profile', null),
  ownFeed: getParam('ownFeed', false),
})

const mapDispatchToProps = {}

const style = StyleSheet.create({
  gallery: {
    flex: 1,
    backgroundColor: 'black',
  },
  galleryHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: WP100,
    marginTop: Constants.statusBarHeight,
  },
  galleryHeaderBack: {
    paddingVertical: WP2,
    width: WP12,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: WP4,
  },
  galleryHeaderIndicator: {
    paddingVertical: WP2,
    width: WP12,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: WP4,
  },
  galleryHeaderTitle: {
    flex: 1,
  },
  galleryMain: {
    flex: 1,
    justifyContent: 'center',
  },
  galleryMainImage: {
    width: WP100,
    height: WP100,
    marginVertical: WP5,
  },
  firstThumb: {
    marginRight: WP2,
    marginLeft: WP5,
    marginVertical: WP2,
  },
  thumb: {
    marginVertical: WP2,
    marginRight: WP2,
  },
  thumbImage: {
    width: WP15,
    height: WP15,
    borderRadius: WP105,
    paddingVertical: WP2,
  },
  thumbInactiveOverlay: {
    width: WP15,
    height: WP15,
    borderRadius: WP105,
    backgroundColor: 'rgba(0,0,0,.65)',
    position: 'absolute',
    top: 0,
    left: 0,
  },
  galleryOwner: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: WP4,
  },
  galleryOwnerInfo: {
    paddingLeft: WP4,
  },
  invisible: {
    opacity: 0,
  },
})

class GalleryScreen extends Component {
  static propTypes = {
    profile: PropTypes.object,
    images: PropTypes.array,
    index: PropTypes.number,
  };

  constructor(props) {
    super(props)
    this.state = {
      index: this.props.index,
      title: null,
      isReady: false,
      artWork: null,
      deletedIds: [],
    }
    this._renderThumbnail = this._renderThumbnail.bind(this)
    this._updateScreen = this._updateScreen.bind(this)
    this._onIndexChange = this._onIndexChange.bind(this)
    this._renderContent = this._renderContent.bind(this)
  }

  // eslint-disable-next-line react/sort-comp
  _renderContent = (image) => {
    const { owner } = image
    const { ownFeed } = this.props
    return (<View key={`gallery-${image.url}`} style={style.galleryMain}>
      <Modal
        position='bottom'
        swipeDirection={null}
        renderModalContent={({ toggleModal }) => {
          return (
            <SoundfrenExploreOptions
              menuOptions={ArtworkMenu}
              userData={this.props.userData}
              navigateTo={this.props.navigateTo}
              onClose={toggleModal}
              onDelete={() => this.setState({ artWork: image })}
            />
          )
        }}
      >
        {({ toggleModal }, M) => (
          <Fragment>
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              disabled={!ownFeed}
              onLongPress={ownFeed? toggleModal : noop }
              useForeground
            >
              {isObject(owner) && (
                <View style={style.galleryOwner}>
                  <View>
                    <Avatar image={owner.profile_picture} />
                  </View>
                  <View style={style.galleryOwnerInfo}>
                    <Text weight={500} size={'small'} color={WHITE}>
                      {owner.full_name}
                    </Text>
                    <Text size={'mini'} color={SHIP_GREY_CALM}>
                      {owner.job_title}
                    </Text>
                  </View>
                </View>
              )}
              <Image
                resizeMode={'contain'}
                style={style.galleryMainImage}
                source={{ uri: image.url }}
              />
              {!isEmpty(image.description) && !isNil(image.description) && (
                <Text size={'mini'} color={WHITE} centered>
                  {image.description ?? ''}
                </Text>
              )}
            </TouchableOpacity>
            {M}
          </Fragment>
        )}
      </Modal>
    </View>)
    // return (
    //   <View key={`gallery-${image.url}`} style={style.galleryMain}>
    //     {isObject(owner) && (
    //       <View style={style.galleryOwner}>
    //         <View>
    //           <Avatar image={owner.profile_picture} />
    //         </View>
    //         <View style={style.galleryOwnerInfo}>
    //           <Text weight={500} size={"small"} color={WHITE}>
    //             {owner.full_name}
    //           </Text>
    //           <Text size={"mini"} color={SHIP_GREY_CALM}>
    //             {owner.job_title}
    //           </Text>
    //         </View>
    //       </View>
    //     )}
    //     <Image
    //       resizeMode={"contain"}
    //       style={style.galleryMainImage}
    //       source={{ uri: image.url }}
    //     />
    //     {!isEmpty(image.description) && !isNil(image.description) && (
    //       <Text size={"mini"} color={WHITE} centered>
    //         {image.description ?? ""}
    //       </Text>
    //     )}
    //   </View>
    // );
  };

  _onIndexChange = (index) => {
    this.setState(
      {
        index,
      },
      this._updateScreen
    )
  };

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._updateScreen)
  }

  _updateScreen = () => {
    setTimeout(() => {
      try {
        const { index } = this.state
        const { images } = this.props
        const { title } = images[index]
        this.setState({ title })
      } catch (err) {
        // silent is gold
      }
    }, 350)
  };

  _renderThumbnail = ({ item: image, index }) => {
    const currentIndex = this.state.index
    const scrollAmount = index - currentIndex
    return (
      <TouchableWithoutFeedback
        onPress={() => this.swiper.scrollBy(scrollAmount)}
      >
        <View style={index == 0 ? style.firstThumb : style.thumb}>
          <Image style={style.thumbImage} source={{ uri: image.url }} />
          {index != this.state.index && (
            <View style={style.thumbInactiveOverlay} />
          )}
        </View>
      </TouchableWithoutFeedback>
    )
  };

  _onDeleteArtWork = () => {
    const { deletedIds, artWork } = this.state
    postProfileDelJourney({
      id_journey: artWork.id_journey,
    }).then(() => {
      this.props.refreshProfile()
      deletedIds.push(artWork.id_journey)
      this.setState({ artWork: null, deletedIds })
    })
  };

  componentDidUpdate = (prevProps, prevState) => {
    if(this.props.images.filter((img) => this.state.deletedIds.indexOf(img.id_journey) == -1).length == 0) prevProps.navigateBack()
  }

  render() {
    const { deletedIds, index, title } = this.state
    const { images, navigateBack } = this.props
    return (
      <View style={style.gallery}>
        <StatusBar barStyle={'light-content'} />
        <View style={style.galleryHeader}>
          <TouchableWithoutFeedback onPress={navigateBack}>
            <View style={style.galleryHeaderBack}>
              <Icon
                type={'MaterialCommunityIcons'}
                color={WHITE}
                size={'small'}
                name={'close'}
              />
            </View>
          </TouchableWithoutFeedback>
          <View style={style.galleryHeaderTitle}>
            <Text numberOfLines={2} size={'mini'} color={WHITE} centered>
              {title}
            </Text>
          </View>
          <TouchableWithoutFeedback>
            <View style={style.galleryHeaderIndicator}>
              <Text
                size={'mini'}
                color={images.filter((img) => deletedIds.indexOf(img.id_journey) == -1).length > 1 ? WHITE : TRANSPARENT}
                centered
              >
                {index + 1}/{images.filter((img) => deletedIds.indexOf(img.id_journey) == -1).length}
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <Swiper
          ref={(ref) => (this.swiper = ref)}
          keyboardShouldPersistTaps={'handled'}
          index={index}
          onIndexChanged={this._onIndexChange}
          showsPagination={false}
          loop={false}
        >
          {images.filter((img) => deletedIds.indexOf(img.id_journey) == -1).map(this._renderContent)}
        </Swiper>
        {images.length > 1 && (
          <View>
            <FlatList
              horizontal
              data={images.filter((img) => deletedIds.indexOf(img.id_journey) == -1)}
              keyExtractor={(item) => `gallery-thumbnail-${item.url}`}
              renderItem={this._renderThumbnail}
            />
          </View>
        )}
        <ModalMessageView
          style={{}}
          subtitleStyle={{}}
          buttonSecondaryStyle={{}}
          toggleModal={noop}
          isVisible={!!this.state.artWork}
          title={'Hapus Artwork'}
          titleType='Circular'
          titleSize={'small'}
          titleColor={GUN_METAL}
          subtitle={
            'Apakah kamu yakin untuk menghapus secara permanen artwork ini?'
          }
          subtitleType='Circular'
          subtitleSize={'xmini'}
          subtitleWeight={400}
          subtitleColor={SHIP_GREY_CALM}
          image={null}
          buttonPrimaryText={'Batalkan'}
          buttonPrimaryTextType='Circular'
          buttonPrimaryTextWeight={400}
          buttonPrimaryBgColor={REDDISH}
          buttonPrimaryAction={() => this.setState({ artWork: null })}
          buttonSecondaryText={'Hapus'}
          buttonSecondaryTextType='Circular'
          buttonSecondaryTextWeight={400}
          buttonSecondaryTextColor={SHIP_GREY_CALM}
          buttonSecondaryAction={this._onDeleteArtWork}
        />
      </View>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(GalleryScreen),
  mapFromNavigationParam
)
