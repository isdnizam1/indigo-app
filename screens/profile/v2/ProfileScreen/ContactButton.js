import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { View, FlatList, Image, TouchableOpacity } from 'react-native'
import { isEmpty, isObject } from 'lodash-es'
import * as Linking from 'expo-linking'
import Modal from '../../../../components/Modal'
import ButtonV2 from '../../../../components/ButtonV2'
import Text from '../../../../components/Text'
import { SHIP_GREY, PALE_BLUE_TWO } from '../../../../constants/Colors'
import style from './style'

const icons = {
  instagram: require('../../../../assets/icons/v3/instagramCircle.png'),
  twitter: require('../../../../assets/icons/v3/twitterCircle.png'),
  youtube: require('../../../../assets/icons/v3/youtubeCircle.png'),
  facebook: require('../../../../assets/icons/v3/facebookCircle.png'),
  website: require('../../../../assets/icons/v3/websiteCircle.png'),
  email: require('../../../../assets/icons/v3/emailCircle.png'),
  close: require('../../../../assets/icons/v3/closeGrey.png'),
}

class ContactButton extends PureComponent {
  static propTypes = {
    profile: PropTypes.object,
  };

  constructor(props) {
    super(props)
  }

  _renderContact = (toggleModal) => ({ item }) => {
    let { contact } = this.props.profile
    if(!isObject(contact)) contact = {}
    if (typeof contact['close'] === 'undefined') contact['close'] = 'Tutup'
    const icon = icons[item]
    let url = contact[item]
    let onPress = () => this.props.navigateTo('BrowserScreenNoTab', { url })
    if (item == 'email') {
      url = `mailto:${url}`
      onPress = () => Linking.openURL(url)
    }
    const isClose = item == 'close'
    if (isClose) {
      onPress = () => {}
    }
    return (
      <TouchableOpacity
        onPress={() => {
          Promise.all([onPress(), toggleModal()])
        }}
      >
        <View style={style.contact}>
          <Image style={style.contactImage} source={icon} />
          <Text
            weight={isClose ? 400 : 300}
            size={isClose ? 'small' : 'slight'}
            type={'Circular'}
          >
            {contact[item]}
          </Text>
        </View>
      </TouchableOpacity>
    )
  };

  _notEmptyContact = () => {
    let { contact } = this.props.profile
    if(!isObject(contact)) contact = {}
    return Array.from(
      new Set([
        ...Object.keys(contact).filter((item) => !isEmpty(contact[item])),
        'close',
      ])
    )
  };

  render() {
    return (
      <Modal
        position='bottom'
        style={{
          borderTopLeftRadius: 0,
          borderTopRightRadius: 0,
        }}
        swipeDirection={null}
        renderModalContent={({ toggleModal }) => {
          return (
            <FlatList
              data={this._notEmptyContact()}
              keyExtractor={(item) => item}
              renderItem={this._renderContact(toggleModal)}
            />
          )
        }}
      >
        {({ toggleModal }, M) => (
          <Fragment>
            <ButtonV2
              disabled={false}
              textColor={SHIP_GREY}
              borderColor={PALE_BLUE_TWO}
              style={style.profileActionsEdit}
              onPress={toggleModal}
              text={'Contact'}
            />
            {M}
          </Fragment>
        )}
      </Modal>
    )
  }
}

export default ContactButton
