import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Image, FlatList } from 'react-native'
import { isEmpty, upperFirst } from 'lodash-es'
import { postProfileDelJourney } from 'sf-actions/api'
import Text from 'sf-components/Text'
import Icon from 'sf-components/Icon'
import Touchable from 'sf-components/Touchable'
import ButtonV2 from 'sf-components/ButtonV2'
import MenuOptions from 'sf-components/MenuOptions'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'
import {
  NAVY_DARK,
  GUN_METAL,
  SHIP_GREY_CALM,
  REDDISH,
  WHITE,
} from 'sf-constants/Colors'
import { WP5 } from 'sf-constants/Sizes'
import style from './style'

class OverviewExperience extends Component {
  static propTypes = {
    items: PropTypes.array,
    isMine: PropTypes.bool,
    navigateTo: PropTypes.func,
    refreshProfile: PropTypes.func,
    initialized: PropTypes.bool,
    name: PropTypes.string,
    profile: PropTypes.object,
  };

  constructor(props) {
    super(props)
    this.state = {
      deleteModalVisible: false,
      experience: {}
    }
    this._cancelDelete = this._cancelDelete.bind(this)
    this._confirmDelete = this._confirmDelete.bind(this)
  }

  _cancelDelete = () => {
    this.setState({
      deleteModalVisible: false,
      experience: {}
    })
  }

  _confirmDelete = () => {
    const { id_journey } = this.state.experience
    this.setState({
      deleteModalVisible: false
    })
    postProfileDelJourney({
      id_journey
    }).then(this.props.refreshProfile)
  }

  _renderMedia = (images) => ({ item, index }) => {
    const { profile: owner, navigateTo } = this.props
    const title = ''
    const onPress = () => navigateTo('GalleryScreen', {
      images: images.map((url) => ({
        owner,
        url,
        title
      })),
      index
    })
    return (<Touchable useForeground onPress={onPress}>
      <View style={style.experienceMedia}>
        <Image style={style.experienceMediaImage} source={{ uri: item }} />
      </View>
    </Touchable>)
  }

  _keyExtractorMedia = (item) => item

  _onAdd = () => this.props.navigateTo('CreateExperiencePerformance', { refreshProfile: this.props.refreshProfile })

  _triggerModal = (toggleModal) => (
    <Touchable
      useForeground
      onPress={toggleModal}
    >
      <View style={style.dotsWrapper}>
        <Icon
          background='dark-circle'
          size='mini'
          color={SHIP_GREY_CALM}
          name='dots-three-horizontal'
          type='Entypo'
          centered
        />
      </View>
    </Touchable>
  )

  _renderItem = ({ item, item: { additional_data: add }, index }) => {
    return (<View style={style.experience}>
      <View style={style.experienceLine} />
      <View style={style.experienceDot}>
        <Image
          resizeMode={'cover'} style={style.experienceDotImage} source={
            index == 0 ? require('sf-assets/icons/v3/dotActive.png') : require('sf-assets/icons/v3/dotInactive.png')
          }
        />
      </View>
      <View style={style.experienceContent}>
        <View style={style.row}>
          <View style={style.flex1}>
            <Text size={'mini'} type={'Circular'} color={GUN_METAL} weight={400}>{upperFirst(item.title)}</Text>
          </View>
          {this.props.isMine && <View>
            <MenuOptions
              options={[
                {
                  onPress: () => this.props.navigateTo('ShareScreen', {
                    id: item.id_journey,
                    type: 'profile',
                    title: 'Bagikan Pengalaman'
                  }),
                  title: 'Share',
                  iconName: 'share-variant',
                  iconColor: SHIP_GREY_CALM,
                  iconSize: 'huge',
                  validation: true
                },
                {
                  onPress: () => this.props.navigateTo('CreateExperiencePerformance', {
                    experience: item,
                    refreshProfile: this.props.refreshProfile
                  }),
                  title: 'Edit',
                  iconName: 'pencil',
                  iconColor: SHIP_GREY_CALM,
                  iconSize: 'huge',
                  validation: true
                },
                {
                  onPress: () =>
                    this.setState({
                      experience: item,
                      deleteModalVisible: true,
                    }),
                  title: 'Hapus',
                  iconName: 'delete',
                  iconColor: SHIP_GREY_CALM,
                  iconSize: 'huge',
                  validation: true
                }
              ]}
              triggerComponent={this._triggerModal}
            />
          </View>}
        </View>
        <Text type={'Circular'} color={GUN_METAL} weight={600}>{`${upperFirst(add.role)}, ${upperFirst(add.company)}`}</Text>
        <Text type={'Circular'} weight={400} color={SHIP_GREY_CALM} size={'xmini'}>
          {`${item.event_date_id}   ·   ${upperFirst(add.event_location)}`}
        </Text>
        <FlatList
          horizontal
          data={add.media}
          renderItem={this._renderMedia(add.media)}
          keyExtractor={this._keyExtractorMedia}
          extraData={null}
        />
      </View>
    </View>)
  };

  _keyExtractor = (item) => item.id_journey;

  render() {
    const { items, initialized, isMine } = this.props
    return (
      <View>
        <View style={style.heading}>
          <Text
            size={'medium'}
            color={NAVY_DARK}
            type={'Circular'}
            weight={600}
          >
            Experiences
          </Text>
          {isMine && !isEmpty(items) && (<Text
            onPress={this._onAdd}
            size={'mini'}
            color={REDDISH}
            type={'Circular'}
            weight={300}
                                         >
            Tambahkan Experience +
          </Text>)}
        </View>
        {isEmpty(items) && initialized && (
          <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require('sf-assets/icons/v3/journeyExperienceIcon.png')}
            />
            {!isMine && <View>
              <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
                Belum ada pengalaman
              </Text>
              <Text
                centered
                style={style.emptyStateDescription}
                size={'mini'}
                color={SHIP_GREY_CALM}
                type={'Circular'}
                weight={300}
              >
                {upperFirst(this.props.name.split(' ')[0])} belum membagikan pengalaman
              </Text>
            </View>}
            {isMine && <View>
              <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
                Bagikan Pengalaman Bermusik
              </Text>
              <Text
                centered
                style={style.emptyStateDescription}
                size={'mini'}
                color={SHIP_GREY_CALM}
                type={'Circular'}
                weight={300}
              >
                Bagikan pengalamanmu agar orang lain dapat mengajakmu untuk berkolaborasi bersama
              </Text>
            </View>}
            {isMine && <ButtonV2
              onPress={this._onAdd}
              style={style.emptyStateCta}
              color={REDDISH}
              textColor={WHITE}
              textSize={'mini'}
              text={'Bagikan Pengalaman'}
                       />}
          </View>
        )}
        <FlatList
          data={items}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={null}
        />
        <ModalMessageView
          toggleModal={this._cancelDelete}
          isVisible={this.state.deleteModalVisible}
          title={'Hapus Experience'}
          titleType='Circular'
          titleSize={'small'}
          titleColor={GUN_METAL}
          subtitle={
            'Apakah kamu yakin ingin menghapus experience yang telah kamu buat ini?'
          }
          subtitleType='Circular'
          subtitleSize={'xmini'}
          subtitleWeight={300}
          subtitleLineHeight={WP5}
          subtitleColor={SHIP_GREY_CALM}
          image={null}
          buttonPrimaryText={'Tidak'}
          buttonPrimaryTextType='Circular'
          buttonPrimaryTextWeight={400}
          buttonPrimaryBgColor={REDDISH}
          buttonSecondaryText={'Ya'}
          buttonSecondaryTextType='Circular'
          buttonSecondaryTextWeight={400}
          buttonSecondaryTextColor={SHIP_GREY_CALM}
          buttonSecondaryAction={this._confirmDelete}
        />
      </View>
    )
  }
}

export default OverviewExperience
