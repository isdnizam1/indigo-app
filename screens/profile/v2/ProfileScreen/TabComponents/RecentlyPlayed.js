import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { View, Image, FlatList, TouchableOpacity } from 'react-native'
import { isEmpty } from 'lodash-es'
import Text from 'sf-components/Text'
import ButtonV2 from 'sf-components/ButtonV2'
import {
  NAVY_DARK,
  GUN_METAL,
  SHIP_GREY_CALM,
  REDDISH,
  WHITE,
} from 'sf-constants/Colors'
import { NavigateToInternalBrowser } from 'sf-utils/helper'
import style from './style'

// This component will be completely fixed by aang

class RecentlyPlayed extends PureComponent {
  static propTypes = {
    title: PropTypes.string,
    emptyIcon: PropTypes.any,
    emptyTitle: PropTypes.string,
    emptyDescription: PropTypes.string,
    emptyCta: PropTypes.string,
    emptyCtaOnPress: PropTypes.func,
    items: PropTypes.array,
    isMine: PropTypes.bool,
    navigateTo: PropTypes.func,
    initialized: PropTypes.bool,
  };

  constructor(props) {
    super(props)
  }

  _navInternalBrowser = (url) => () => {
    NavigateToInternalBrowser({ url })
  };

  _onAdd = () => alert('Under development');

  _renderItem = ({ item, item: { additional_data: add }, index }) => {
    return <TouchableOpacity onPress={this._navInternalBrowser(add.url_video)} />
  };

  _keyExtractor = (item) => item.id_journey;

  render() {
    const { items, initialized, isMine } = this.props
    return (
      <View>
        <View style={style.headingWithPadding}>
          <Text size={'medium'} color={NAVY_DARK} type={'Circular'} weight={600}>
            {this.props.title}
          </Text>
        </View>
        {isEmpty(items) && initialized && (
          <View style={style.emptyState}>
            <Image style={style.emptyStateIcon} source={this.props.emptyIcon} />
            <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
              {this.props.emptyTitle}
            </Text>
            <Text
              centered
              style={style.emptyStateDescription}
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={300}
            >
              {this.props.emptyDescription}
            </Text>
            {isMine && (
              <ButtonV2
                onPress={this.props.emptyCtaOnPress}
                style={style.emptyStateCta}
                color={REDDISH}
                textColor={WHITE}
                textSize={'mini'}
                text={this.props.emptyCta}
              />
            )}
          </View>
        )}
        <FlatList
          horizontal
          data={items}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={null}
        />
      </View>
    )
  }
}

export default RecentlyPlayed
