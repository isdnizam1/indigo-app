import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { View, Image, FlatList } from 'react-native'
import { isEmpty, upperFirst } from 'lodash-es'
import Text from 'sf-components/Text'
import Song from 'sf-components/Song'
import ButtonV2 from 'sf-components/ButtonV2'
import {
  NAVY_DARK,
  GUN_METAL,
  SHIP_GREY_CALM,
  REDDISH,
  WHITE,
} from 'sf-constants/Colors'
import style from './style'

class OverviewSong extends PureComponent {
  static propTypes = {
    idViewer: PropTypes.any,
    items: PropTypes.array,
    isMine: PropTypes.bool,
    initialized: PropTypes.bool,
    navigateTo: PropTypes.func,
    onPlay: PropTypes.func,
    refreshProfile: PropTypes.func,
    refreshFollow: PropTypes.func,
    name: PropTypes.string,
    accountType: PropTypes.string,
  };

  constructor(props) {
    super(props)
  }

  _renderItem = ({ item, index }) => {
    return (<Song
      idViewer={this.props.idViewer}
      isMine={this.props.isMine}
      firstSong={index == 0}
      song={item}
      loading={this.props.loading}
      isProfileScreen
      navigateTo={this.props.navigateTo}
      refreshData={this.props.refreshFollow}
            />)
  };

  _keyExtractor = (item) => item.id_journey;

  _onAdd = () => this.props.navigateTo('SongFileForm', {
    refreshProfile: this.props.refreshProfile,
    accountType: this.props.accountType
  })

  render() {
    const { loading, items, initialized, isMine, isProfileScreen } = this.props
    const data = loading ? isProfileScreen ? [] : [1, 2, 3] : items
    return (
      <View>
        <View style={style.heading}>
          <Text
            size={'medium'}
            color={NAVY_DARK}
            type={'Circular'}
            weight={600}
          >
            Semua Lagu
          </Text>
          {isMine && !isEmpty(data) && (<Text
            onPress={this._onAdd}
            size={'mini'}
            color={REDDISH}
            type={'Circular'}
            weight={300}
                                        >
            Tambahkan Lagu +
          </Text>)}
        </View>
        {isEmpty(data) && initialized && (
          <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require('sf-assets/icons/v3/journeySongIcon.png')}
            />
            {!isMine && <View>
              <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
                Belum ada lagu
              </Text>
              <Text
                centered
                style={style.emptyStateDescription}
                size={'mini'}
                color={SHIP_GREY_CALM}
                type={'Circular'}
                weight={300}
              >
                {upperFirst(this.props.name.split(' ')[0])} belum mengupload attachment
              </Text>
            </View>}
            {isMine && <View>
              <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
                Upload Karya Musikmu
              </Text>
              <Text
                centered
                style={style.emptyStateDescription}
                size={'mini'}
                color={SHIP_GREY_CALM}
                type={'Circular'}
                weight={300}
              >
                Bagikan lagumu dan gapai pendengar barumu
              </Text>
            </View>}
            {isMine && <ButtonV2
              onPress={this._onAdd}
              style={style.emptyStateCta}
              color={REDDISH}
              textColor={WHITE}
              textSize={'mini'}
              text={'Upload Attachment'}
                       />}
          </View>
        )}
        <FlatList
          data={data}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={null}
        />
      </View>
    )
  }
}

export default OverviewSong