import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { FlatList, Image, View } from 'react-native'
import { isEmpty, upperFirst } from 'lodash-es'
import { isIOS } from 'sf-utils/helper'
import Text from 'sf-components/Text'
import ButtonV2 from 'sf-components/ButtonV2'
import { GUN_METAL, NAVY_DARK, REDDISH, SHIP_GREY_CALM, WHITE, } from 'sf-constants/Colors'
import ScheduleListItem from '../../schedule/ScheduleListItem'
import style from './style'

class Schedule extends PureComponent {
  static propTypes = {
    items: PropTypes.array,
    profile: PropTypes.object,
    isMine: PropTypes.bool,
    initialized: PropTypes.bool,
    navigateTo: PropTypes.func,
    onSubscribe: PropTypes.func,
    showTrialModal: PropTypes.func,
    name: PropTypes.string,
    accountType: PropTypes.string,
  };

  constructor(props) {
    super(props)
  }

  _renderItem = ({ item, index }) => {
    return (
      <ScheduleListItem
        key={index}
        schedule={item}
        navigateTo={this.props.navigateTo}
        refreshProfile={this.props.refreshProfile}
        dispatch={this.props.dispatch}
        isMine={this.props.isMine}
      />
    )
  };

  _keyExtractor = (item) => item.id_schedule;

  _onAdd = () => {
    this.props.navigateTo('ScheduleForm', { refreshProfile: this.props.refreshProfile })
  }

  render() {
    const { items, initialized, isMine, profile } = this.props
    return (
      <View>
        <View style={style.headingWithPadding}>
          <Text
            size={'medium'}
            color={NAVY_DARK}
            type={'Circular'}
            weight={600}
          >
            Schedule
          </Text>
          {isMine && !isEmpty(items) && profile.account_type === 'premium' && (<Text
            onPress={this._onAdd}
            size={'mini'}
            color={REDDISH}
            type={'Circular'}
            weight={300}
                                                                               >
            Tambahkan Schedule +
          </Text>)}
        </View>
        {isEmpty(items) && initialized && (
          isMine ? <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require('sf-assets/icons/v3/scheduleCircle.png')}
            />
            <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
              Jadwal Tampil
            </Text>
            <Text
              centered
              style={style.emptyStateDescription}
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={300}
            >
              {'Bagikan jadwal bermainmu disini. Fitur ini\nkhusus untuk pengguna premium'}
            </Text>
            {
              profile.account_type === 'premium' && (
                <ButtonV2
                  onPress={this._onAdd}
                  style={style.emptyStateCta}
                  color={REDDISH}
                  textColor={WHITE}
                  textSize={'mini'}
                  text={'Tambahkan Schedule +'}
                />
              )
            }
            {!isIOS() && profile.account_type == 'user' && profile.free_trial && <ButtonV2
              onPress={this.props.onSubscribe}
              style={style.emptyStateCta}
              color={REDDISH}
              textColor={WHITE}
              textSize={'mini'}
              text={'Daftar Premium'}
                                                                                 />}
            {profile.account_type == 'user' && profile.free_trial && <ButtonV2
              onPress={this.props.showTrialModal} // Modal located at ProfileHeader.js
              color={WHITE}
              textColor={REDDISH}
              textSize={'mini'}
              text={'Coba free trial 14 hari'}
                                                                     />}
            {profile.account_type == 'user' && profile.free_trial && <View style={style.emptyStateBenefit}>
              <Text
                centered
                style={style.emptyStateDescription}
                size={'mini'}
                color={SHIP_GREY_CALM}
                type={'Circular'}
                weight={300}
              >
                {'Setiap jadwal yang kamu bagikan akan kami\nsalurkan ke semua followers kamu melalui\ninbox mereka.'}
              </Text>
            </View>}
          </View> : <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require('sf-assets/icons/v3/scheduleCircle.png')}
            />
            <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
              Belum ada jadwal tampil
            </Text>
            <Text
              centered
              style={style.emptyStateDescription}
              size={'mini'}
              color={SHIP_GREY_CALM}
              type={'Circular'}
              weight={300}
            >
              {upperFirst(this.props.name.split(' ')[0])} saat ini belum memiliki jadwal bermain.
            </Text>
          </View>
        )}
        <FlatList
          data={items}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={null}
        />
      </View>
    )
  }
}

export default Schedule
