export const ArtworkMenu = (props) => ({
  withPromoteUser: false,
  menus: [
    {
      type: 'menu',
      image: require('sf-assets/icons/mdi_delete.png'),
      imageStyle: {},
      name: 'Hapus',
      textStyle: {},
      onPress: () => {
        props.onClose()
        setTimeout(props.onDelete, 500)
      }
    },
    {
      type: 'separator'
    },
    {
      type: 'menu',
      image: require('sf-assets/icons/close.png'),
      imageStyle: {},
      name: 'Tutup',
      textStyle: {},
      onPress: props.onClose
    },
  ]
})

export default ArtworkMenu
