import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import {
  Image,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
} from 'react-native'
import { connect } from 'react-redux'
import { LinearGradient } from 'expo-linear-gradient'
import Text from 'sf-components/Text'
import Container from 'sf-components/Container'
import HeaderNormal from 'sf-components/HeaderNormal'
import { WP105, WP2, WP4, WP30 } from 'sf-constants/Sizes'
import {
  WHITE,
  SHIP_GREY_CALM,
  SHIP_GREY,
  SHADOW_GRADIENT,
  GUN_METAL,
  REDDISH,
} from 'sf-constants/Colors'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import InteractionManager from 'sf-utils/InteractionManager'
import { HEADER } from 'sf-constants/Styles'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'
import SoundfrenExploreOptions from 'sf-components/explore/SoundfrenExploreOptions'
import { Modal } from 'sf-components'
import { postProfileDelJourney } from 'sf-actions/api'
import { noop } from 'lodash-es'
import { NavigationEvents } from '@react-navigation/compat'
import ArtworkMenu from './ArtworkMenu'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  images: getParam('images', []),
  index: getParam('index', 0),
  refreshProfile: getParam('refreshProfile', noop),
  getArtwork: getParam('getArtwork', noop),
  profile: getParam('profile', null),
})

const mapDispatchToProps = {}

const style = StyleSheet.create({
  artWorkWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: WP4,
  },
  artWork: {
    width: WP30 + WP2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: WP105,
  },
  artWorkImage: {
    width: WP30,
    height: WP30,
    borderRadius: WP2,
  },
})

class ArtworkGridScreen extends Component {
  static propTypes = {
    profile: PropTypes.object,
    images: PropTypes.array,
    index: PropTypes.number,
  };

  constructor(props) {
    super(props)
    this.state = {
      index: this.props.index,
      title: null,
      isReady: false,
      artWork: null,
      deletedIds: [],
      images: props.images,
    }
    this._renderThumbnail = this._renderThumbnail.bind(this)
    this._updateScreen = this._updateScreen.bind(this)
    this._onIndexChange = this._onIndexChange.bind(this)
  }

  _onIndexChange = (index) => {
    this.setState(
      {
        index,
      },
      this._updateScreen,
    )
  };

  _onDeleteArtWork = () => {
    const { deletedIds, artWork } = this.state
    postProfileDelJourney({
      id_journey: artWork.id_journey,
    }).then(() => {
      this.props.refreshProfile()
      deletedIds.push(artWork.id_journey)
      this.setState({ artWork: null, deletedIds })
    })
  };

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._updateScreen)
  }

  _updateScreen = () => {
    const { index, images } = this.state
    const { title } = images[index]
    this.setState({ title })
  };

  _renderThumbnail = ({ item: image, index }) => {
    const currentIndex = this.state.index
    const scrollAmount = index - currentIndex
    return (
      <TouchableWithoutFeedback onPress={() => this.swiper.scrollBy(scrollAmount)}>
        <View style={index == 0 ? style.firstThumb : style.thumb}>
          <Image style={style.thumbImage} source={{ uri: image.url }} />
          {index != this.state.index && <View style={style.thumbInactiveOverlay} />}
        </View>
      </TouchableWithoutFeedback>
    )
  };

  _renderItem = ({ item, index }) => {
    const { totaItems, navigateTo } = this.props
    const { images } = this.state
    let onPress = () =>
      navigateTo('GalleryScreen', {
        images,
        index,
        refreshProfile: this.props.refreshProfile,
      })
    return (
      <Modal
        position='bottom'
        swipeDirection={null}
        renderModalContent={({ toggleModal }) => {
          return (
            <SoundfrenExploreOptions
              menuOptions={ArtworkMenu}
              userData={this.props.userData}
              navigateTo={this.props.navigateTo}
              onClose={toggleModal}
              onDelete={() => this.setState({ artWork: item })}
            />
          )
        }}
      >
        {({ toggleModal }, M) => (
          <Fragment>
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onLongPress={toggleModal}
              useForeground
              onPress={onPress}
            >
              <View style={style.artWork}>
                <Image resizeMode={'cover'} style={style.artWorkImage} source={{ uri: item.url }} />
                {index == 3 && totaItems > 4 && (
                  <View style={style.artWorkOther}>
                    <Text size={'slight'} color={WHITE} centered type={'Circular'} weight={600}>
                      + {totaItems - 4} Lainnya
                    </Text>
                  </View>
                )}
              </View>
            </TouchableOpacity>
            {M}
          </Fragment>
        )}
      </Modal>
    )
  };

  _keyExtractor = (item) => item.id_journey;

  _getArtwork = () => {
    this.props.getArtwork().then((result) => {
      this.setState({
        images: result.map((img) => {
          return {
            ...img,
            url: img.attachment_file,
          }
        }),
      })
    })
  };

  render() {
    const { navigateBack } = this.props
    const { images, deletedIds } = this.state
    return (
      <Container
        renderHeader={() => (
          <View>
            <HeaderNormal
              withExtraPadding
              iconLeftOnPress={navigateBack}
              centered
              textType={'Circular'}
              iconLeft={'ios-arrow-back'}
              iconLeftSize={'large'}
              textSize={'slight'}
              iconLeftType={'Ionicons'}
              text={'Artwork & Gallery'}
              textColor={SHIP_GREY}
              iconLeftColor={SHIP_GREY_CALM}
              rightComponent={null}
            />
            <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
          </View>
        )}
      >
        <SafeAreaView style={style.artWorkWrapper}>
          <FlatList
            data={images.filter((journey) => deletedIds.indexOf(journey.id_journey) == -1)}
            columnWrapperStyle={style.artWorkColumnWrapper}
            renderItem={this._renderItem}
            keyExtractor={this._keyExtractor}
            extraData={null}
            numColumns={3}
          />
        </SafeAreaView>
        <NavigationEvents onWillFocus={this._getArtwork} />
        <ModalMessageView
          style={{}}
          subtitleStyle={{}}
          buttonSecondaryStyle={{}}
          toggleModal={noop}
          isVisible={!!this.state.artWork}
          title={'Hapus Artwork'}
          titleType='Circular'
          titleSize={'small'}
          titleColor={GUN_METAL}
          subtitle={'Apakah kamu yakin untuk menghapus secara permanen artwork ini?'}
          subtitleType='Circular'
          subtitleSize={'xmini'}
          subtitleWeight={400}
          subtitleColor={SHIP_GREY_CALM}
          image={null}
          buttonPrimaryText={'Batalkan'}
          buttonPrimaryTextType='Circular'
          buttonPrimaryTextWeight={400}
          buttonPrimaryBgColor={REDDISH}
          buttonPrimaryAction={() => this.setState({ artWork: null })}
          buttonSecondaryText={'Hapus'}
          buttonSecondaryTextType='Circular'
          buttonSecondaryTextWeight={400}
          buttonSecondaryTextColor={SHIP_GREY_CALM}
          buttonSecondaryAction={this._onDeleteArtWork}
        />
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ArtworkGridScreen),
  mapFromNavigationParam,
)
