import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { View, Image, FlatList } from 'react-native'
import { isEmpty, upperFirst } from 'lodash-es'
import Text from 'sf-components/Text'
import ButtonV2 from 'sf-components/ButtonV2'
import Video from 'sf-components/Video'
import {
  NAVY_DARK,
  GUN_METAL,
  SHIP_GREY_CALM,
  REDDISH,
  WHITE,
} from 'sf-constants/Colors'
import style from './style'

class OverviewVideo extends PureComponent {
  static propTypes = {
    idViewer: PropTypes.any,
    items: PropTypes.array,
    isMine: PropTypes.bool,
    refreshProfile: PropTypes.func,
    navigateTo: PropTypes.func,
    initialized: PropTypes.bool,
    name: PropTypes.string,
  };

  constructor(props) {
    super(props)
  }

  _onAdd = () => this.props.navigateTo('ProfileAddVideoScreen', {
    refreshProfile: this.props.refreshProfile
  })

  _renderItem = ({ item, index }) => {
    return (
      <Video
        idViewer={this.props.idViewer}
        refreshProfile={this.props.refreshProfile}
        isMine={this.props.isMine}
        navigateTo={this.props.navigateTo}
        video={item}
        firstVideo={index == 0}
      />
    )
  };

  _keyExtractor = (item) => item.id_journey;

  render() {
    const { items, initialized, isMine } = this.props
    return (
      <View>
        <View style={style.headingWithPadding}>
          <Text
            size={'medium'}
            color={NAVY_DARK}
            type={'Circular'}
            weight={600}
          >
            Semua Video
          </Text>
          {isMine && !isEmpty(items) && (<Text
            onPress={this._onAdd}
            size={'mini'}
            color={REDDISH}
            type={'Circular'}
            weight={300}
                                         >
            Tambahkan Video +
          </Text>)}
        </View>
        {isEmpty(items) && initialized && (
          <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require('sf-assets/icons/v3/journeyVideoIcon.png')}
            />
            {!isMine && <View>
              <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
                Belum ada videoclip
              </Text>
              <Text
                centered
                style={style.emptyStateDescription}
                size={'mini'}
                color={SHIP_GREY_CALM}
                type={'Circular'}
                weight={300}
              >
                {upperFirst(this.props.name.split(' ')[0])} belum mengupload video
              </Text>
            </View>}
            {isMine && <View>
              <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
                Bagikan Videoclip
              </Text>
              <Text
                centered
                style={style.emptyStateDescription}
                size={'mini'}
                color={SHIP_GREY_CALM}
                type={'Circular'}
                weight={300}
              >
                Promosikan videoclip lagumu disini
              </Text>
            </View>}
            {isMine && <ButtonV2
              onPress={this._onAdd}
              style={style.emptyStateCta}
              color={REDDISH}
              textColor={WHITE}
              textSize={'mini'}
              text={'Upload Video'}
                       />}
          </View>
        )}
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={items}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={null}
        />
      </View>
    )
  }
}

export default OverviewVideo
