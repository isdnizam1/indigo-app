import { StyleSheet } from 'react-native'
import { WP2, WP3, WP305, WP4, WP5, WP7, WP15, WP25, WP50, WP70, WP90 } from '../../../../../constants/Sizes'
import { PALE_BLUE } from '../../../../../constants/Colors'

export default StyleSheet.create({
  heading: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: WP2
  },
  headingWithPadding: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: WP2,
    paddingHorizontal: WP5
  },
  emptyState: {
    alignItems: 'center',
    paddingTop: WP7,
    paddingBottom: WP7,
  },
  emptyStateIcon: {
    width: WP15,
    height: WP15,
    marginBottom: WP5
  },
  emptyStateDescription: {
    lineHeight: WP5,
    marginTop: WP2
  },
  emptyStateCta: {
    marginTop: WP5,
    paddingVertical: WP2,
    paddingHorizontal: WP5
  },
  firstVideo: {
    paddingVertical: WP2,
    width: WP70,
    marginRight: WP7,
    marginLeft: WP5
  },
  video: {
    paddingVertical: WP2,
    width: WP70,
    marginRight: WP7
  },
  videoCoverWrapper: {
    width: WP70,
    height: WP70 * 0.5615,
    borderRadius: WP2,
    backgroundColor: PALE_BLUE,
    marginBottom: WP3
  },
  videoCover: {
    width: WP70,
    height: WP70 * 0.5615,
    borderRadius: WP2
  },
  videoContent: {
    flexGrow: 1
  },
  videoContentMeta: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: WP2
  },
  videoContentAction: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: PALE_BLUE,
    marginTop: WP3,
  },
  videoContentActionButton: {
    paddingVertical: 0,
    paddingRight: WP2,
    marginRight: WP3
  },
  experience: {
    paddingLeft: WP7,
  },
  experienceContent: {
    paddingBottom: WP7,
    paddingTop: WP3
  },
  experienceLine: {
    width: 1.5,
    backgroundColor: PALE_BLUE,
    position: 'absolute',
    left: 5.5,
    top: 0,
    marginTop: WP4,
    height: '100%'
  },
  experienceDot: {
    top: -5,
    left: 0,
    position: 'absolute'
  },
  experienceDotImage: {
    width: 12,
    height: 24,
    top: WP305
  },
  experienceMediaImage: {
    width: WP25,
    height: WP25,
    borderRadius: WP2,
    marginTop: WP2,
    marginRight: WP2
  },
  artWork: {
    width: WP50 - WP3,
    height: WP50 - WP3,
    alignItems: 'center',
    justifyContent: 'center'
  },
  artWorkImage: {
    width: WP50 - WP7,
    height: WP50 - WP7,
    resizeMode: 'cover',
    marginTop: WP2,
    borderRadius: WP2
  },
  artWorkWrapper: {
    paddingHorizontal: WP3
  },
  artWorkOther: {
    top: WP3,
    position: 'absolute',
    width: WP50 - WP7,
    height: WP50 - WP7,
    borderRadius: WP2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  emptyStateBenefit: {
    borderTopWidth: 1,
    width: WP90,
    borderColor: PALE_BLUE,
    marginTop: WP5,
    paddingTop: WP3
  },
  artWorkColumnWrapper: { flexWrap: 'wrap', flex: 1, alignItems: 'center' },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  flex1: {
    flex: 1
  }
})