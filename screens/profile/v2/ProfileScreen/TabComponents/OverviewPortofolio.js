import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, Image, FlatList } from "react-native";
import { isEmpty, upperFirst } from "lodash-es";
import Text from "sf-components/Text";
import ButtonV2 from "sf-components/ButtonV2";
import Video from "sf-components/ListItem";
import Touchable from "sf-components/Touchable";
import { Card, ListItem, Button, Icon, Avatar } from "react-native-elements";
import LinkPreviewCard from "sf-components/LinkPreviewCard";
import {
  NAVY_DARK,
  GUN_METAL,
  SHIP_GREY_CALM,
  REDDISH,
  WHITE,
} from "sf-constants/Colors";
import style from "./style";
import { isIOS, NavigateToInternalBrowser } from "sf-utils/helper";
import { YELLOW } from "../../../../../constants/Colors";

class OverviewPortofolio extends PureComponent {
  static propTypes = {
    idViewer: PropTypes.any,
    items: PropTypes.array,
    isMine: PropTypes.bool,
    refreshProfile: PropTypes.func,
    navigateTo: PropTypes.func,
    initialized: PropTypes.bool,
    name: PropTypes.string,
  };

  constructor(props) {
    super(props);
  }
  _onAdd = () =>
    this.props.navigateTo("PortofolioForm", {
      refreshProfile: this.props.refreshProfile,
    });
  _onEdit(index, title, url_portofolio) {
    this.props.navigateTo("PortofolioForm", {
      refreshProfile: this.props.refreshProfile,
      id: index,
      title: title,
      link_portofolio: url_portofolio,
      action: "edit",
    });
    console.log;
  }

  _goToWeb(url_portofolio) {
    NavigateToInternalBrowser({ url: url_portofolio });
  }

  _renderItem = ({ item, index }) => {
    const { items, initialized, isMine } = this.props;

    return (
      <Card
        containerStyle={{
          borderRadius: 9,
          padding: 7,
          marginLeft: 0,
          marginRight: 9,
          marginTop: 10,
          marginBottom: 5,
        }}
      >
        <ListItem style={{ borderRadius: 28, padding: 0 }}>
          <Avatar source={require("sf-assets/images/bx_bx-link-alt.png")} />
          <ListItem.Content>
            <ListItem.Title style={{ fontWeight: "bold" }}>
              {item.title}
            </ListItem.Title>
            {isMine && (
              <ListItem.Subtitle
                onPress={() =>
                  this._onEdit(index + 1, item.title, item.link_portofolio)
                }
                style={{ color: REDDISH }}
              >
                {"Edit Attachment"}
              </ListItem.Subtitle>
            )}
          </ListItem.Content>
          <ListItem.Chevron
            onPress={() => this._goToWeb(item.link_portofolio)}
          />
        </ListItem>
      </Card>
    );
  };
  _keyExtractor = (item) => item.id_journey;

  render() {
    const { items, initialized, isMine } = this.props;
    if (items) {
      var valuePorto = JSON.parse(items.value);
    } else {
      var valuePorto = [];
    }
    return (
      <View>
        <View style={style.heading}>
          <Text
            size={"medium"}
            color={NAVY_DARK}
            type={"Circular"}
            weight={600}
          >
            Semua Attachment
          </Text>
          {isMine && !isEmpty(items) && (
            <Text
              onPress={this._onAdd}
              size={"mini"}
              color={REDDISH}
              type={"Circular"}
              weight={300}
            >
              Tambahkan Attachment +
            </Text>
          )}
        </View>

        {isEmpty(items) && initialized && (
          <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require("sf-assets/icons/v3/journeyPortoIcon.png")}
            />
            {!isMine && (
              <View>
                <Text centered color={GUN_METAL} type={"Circular"} weight={500}>
                  Upload attachment
                </Text>
                <Text
                  centered
                  style={style.emptyStateDescription}
                  size={"mini"}
                  color={SHIP_GREY_CALM}
                  type={"Circular"}
                  weight={300}
                >
                  {upperFirst(this.props.name.split(" ")[0])} belum mengupload
                  attachment
                </Text>
              </View>
            )}
            {isMine && (
              <View>
                <Text centered color={GUN_METAL} type={"Circular"} weight={500}>
                  Upload attachment
                </Text>
                <Text
                  centered
                  style={style.emptyStateDescription}
                  size={"mini"}
                  color={SHIP_GREY_CALM}
                  type={"Circular"}
                  weight={300}
                >
                  {
                    "Masukkan attachment untuk kebutuhan event dan \n program yang kamu ikuti"
                  }
                </Text>
              </View>
            )}
            {isMine && (
              <ButtonV2
                onPress={this._onAdd}
                style={style.emptyStateCta}
                color={YELLOW}
                textColor={WHITE}
                textSize={"mini"}
                text={"Tambahkan Attachment"}
              />
            )}
          </View>
        )}

        {/* {
    items.map((u, i) => {
      return (
        <View key={i}>
                <Card>

          <Image
            resizeMode="cover"
            source={require('sf-assets/images/default_avatar.png')
                      }
          />
          <Text>{"ssss"}</Text>
</Card>

        </View>
      );
    })
  } */}

        <FlatList
          data={valuePorto}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          extraData={null}
        />
      </View>
    );
  }
}

export default OverviewPortofolio;
