import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Image,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native'
import { isEmpty, isObject, upperFirst } from 'lodash-es'
import Text from 'sf-components/Text'
import ButtonV2 from 'sf-components/ButtonV2'
import {
  NAVY_DARK,
  GUN_METAL,
  SHIP_GREY_CALM,
  REDDISH,
  WHITE,
} from 'sf-constants/Colors'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'
import SoundfrenExploreOptions from 'sf-components/explore/SoundfrenExploreOptions'
import { Modal } from 'sf-components'
import { postProfileDelJourney } from 'sf-actions/api'
import { noop } from 'lodash-es/noop'
import ArtworkMenu from './ArtworkMenu'
import style from './style'

class OverviewArtWork extends Component {
  static propTypes = {
    profile: PropTypes.object,
    totaItems: PropTypes.number,
    items: PropTypes.array,
    artwork: PropTypes.array,
    isMine: PropTypes.bool,
    navigateTo: PropTypes.func,
    refreshProfile: PropTypes.func,
    initialized: PropTypes.bool,
    name: PropTypes.string,
  };

  constructor(props) {
    super(props)
    this.state = {
      artWork: null,
    }
  }

  _onAdd = () =>
    this.props.navigateTo('ProfileAddArtScreen', {
      refreshProfile: this.props.refreshProfile,
    });

  _renderItem = ({ item, index }) => {
    const {
      totaItems,
      navigateTo,
      profile: owner,
      artwork: images,
    } = this.props
    let onPress = () =>
      navigateTo(index == 3 ? 'ArtworkGridScreen' : 'GalleryScreen', {
        images: images.map(({ attachment_file: url, title, description, id_journey }) => ({
          url,
          title,
          owner,
          description,
          id_journey
        })),
        index,
        refreshProfile: this.props.refreshProfile,
        getArtwork: this.props.getArtwork
      })
    return (
      <Modal
        position='bottom'
        swipeDirection={null}
        renderModalContent={({ toggleModal }) => {
          return (
            <SoundfrenExploreOptions
              menuOptions={ArtworkMenu}
              userData={this.props.userData}
              navigateTo={this.props.navigateTo}
              onClose={toggleModal}
              onDelete={() => this.setState({ artWork: item })}
            />
          )
        }}
      >
        {({ toggleModal }, M) => (
          <Fragment>
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onLongPress={toggleModal}
              useForeground
              onPress={onPress}
            >
              <View style={style.artWork}>
                <Image
                  resizeMode={'cover'}
                  style={style.artWorkImage}
                  source={{ uri: item.attachment_file }}
                />
                {index == 3 && totaItems > 4 && (
                  <View style={style.artWorkOther}>
                    <Text
                      size={'slight'}
                      color={WHITE}
                      centered
                      type={'Circular'}
                      weight={600}
                    >
                      + {totaItems - 4} Lainnya
                    </Text>
                  </View>
                )}
              </View>
            </TouchableOpacity>
            {M}
          </Fragment>
        )}
      </Modal>
    )
  };

  _keyExtractor = (item) => item.id_journey;

  _onDeleteArtWork = () => {
    postProfileDelJourney({
      id_journey: this.state.artWork.id_journey
    }).then(() => {
      this.props.refreshProfile()
      this.setState({ artWork: null })
    })
  };

  render() {
    const { items, initialized, isMine } = this.props
    return (
      <View>
        <View style={style.headingWithPadding}>
          <Text
            size={'medium'}
            color={NAVY_DARK}
            type={'Circular'}
            weight={600}
          >
            Artwork & Gallery
          </Text>
          {isMine && !isEmpty(items) && (
            <Text
              onPress={this._onAdd}
              size={'mini'}
              color={REDDISH}
              type={'Circular'}
              weight={300}
            >
              Tambahkan Foto +
            </Text>
          )}
        </View>
        {isEmpty(items) && initialized && (
          <View style={style.emptyState}>
            <Image
              style={style.emptyStateIcon}
              source={require('sf-assets/icons/v3/journeyArtWorkIcon.png')}
            />
            {!isMine && (
              <View>
                <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
                  Belum ada foto
                </Text>
                <Text
                  centered
                  style={style.emptyStateDescription}
                  size={'mini'}
                  color={SHIP_GREY_CALM}
                  type={'Circular'}
                  weight={300}
                >
                  {upperFirst(this.props.name.split(' ')[0])} belum membagikan
                  foto
                </Text>
              </View>
            )}
            {isMine && (
              <View>
                <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
                  Upload foto & artwork
                </Text>
                <Text
                  centered
                  style={style.emptyStateDescription}
                  size={'mini'}
                  color={SHIP_GREY_CALM}
                  type={'Circular'}
                  weight={300}
                >
                  Bagikan foto atau artwork lagumu
                </Text>
              </View>
            )}
            {isMine && (
              <ButtonV2
                onPress={this._onAdd}
                style={style.emptyStateCta}
                color={REDDISH}
                textColor={WHITE}
                textSize={'mini'}
                text={'Upload Foto'}
              />
            )}
          </View>
        )}
        <SafeAreaView style={style.artWorkWrapper}>
          <FlatList
            data={items}
            columnWrapperStyle={style.artWorkColumnWrapper}
            renderItem={this._renderItem}
            keyExtractor={this._keyExtractor}
            extraData={null}
            numColumns={2}
          />
          <ModalMessageView
            style={{}}
            subtitleStyle={{}}
            buttonSecondaryStyle={{}}
            toggleModal={noop}
            isVisible={isObject(this.state.artWork)}
            title={'Hapus Artwork'}
            titleType='Circular'
            titleSize={'small'}
            titleColor={GUN_METAL}
            subtitle={
              'Apakah kamu yakin untuk menghapus secara permanen artwork ini?'
            }
            subtitleType='Circular'
            subtitleSize={'xmini'}
            subtitleWeight={400}
            subtitleColor={SHIP_GREY_CALM}
            image={null}
            buttonPrimaryText={'Batalkan'}
            buttonPrimaryTextType='Circular'
            buttonPrimaryTextWeight={400}
            buttonPrimaryBgColor={REDDISH}
            buttonPrimaryAction={() => this.setState({ artWork: null })}
            buttonSecondaryText={'Hapus'}
            buttonSecondaryTextType='Circular'
            buttonSecondaryTextWeight={400}
            buttonSecondaryTextColor={SHIP_GREY_CALM}
            buttonSecondaryAction={this._onDeleteArtWork}
          />
        </SafeAreaView>
      </View>
    )
  }
}

export default OverviewArtWork
