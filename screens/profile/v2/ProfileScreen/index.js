import React, { Component } from "react";
import { connect } from "react-redux";
import {
  ActivityIndicator,
  Image,
  View,
  BackHandler,
  ScrollView,
} from "react-native";
import { noop, isNil, upperFirst, isEmpty, isArray, get } from "lodash-es";
import { LinearGradient } from "expo-linear-gradient";
import { TabBar, TabView } from "react-native-tab-view";
import InteractionManager from "sf-utils/InteractionManager";
import Icon from "sf-components/Icon";
import MenuOptions from "sf-components/MenuOptions";
import {
  getJourneyV2,
  getProfileDetail,
  getProfileDetailV2,
  getSchedule,
  getSubscriptionDetail,
  getUserFeed,
  postClaimFreeTrial,
  postProfileFollow,
  postProfileUnfollow,
  postUploadCoverImage,
} from "sf-actions/api";
import {
  PALE_BLUE_TWO,
  PALE_WHITE,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  GUN_METAL,
  WHITE,
  NAVY_DARK,
} from "sf-constants/Colors";
import { WP305, WP5, WP6 } from "sf-constants/Sizes";
import _enhancedNavigation from "sf-navigation/_enhancedNavigation";
import Container from "sf-components/Container";
import HeaderNormal from "sf-components/HeaderNormal";
import Text from "sf-components/Text";
import Touchable from "sf-components/Touchable";
import ButtonV2 from "sf-components/ButtonV2";
import ImageComponent from "sf-components/Image";
import MessageBarClearBlue from "sf-components/messagebar/MessageBarClearBlue";
import { _journeyMapper, initiateRoom, isIOS } from "sf-utils/helper";
import { paymentDispatcher } from "sf-services/payment";
import { GET_USER_DETAIL } from "sf-services/auth/actionTypes";
import produce from "immer";
import { setCameFromBottomNavigation } from "sf-services/helper/actionDispatcher";
import { emptyClearBlueMessage } from "sf-services/messagebar/actionDispatcher";
import ProfileMyPostTab from "../../../ProfileMyPostTab";
import { HEADER } from "../../../../constants/Styles";
import { clearAddProfile, showReviewIfNotYet } from "../../../../utils/review";
import { deleteJourney } from "../../../../actions/api";
import style from "./style";
import OverviewPortofolio from "./TabComponents/OverviewPortofolio";
import OverviewSong from "./TabComponents/OverviewSong";
import OverviewVideo from "./TabComponents/OverviewVideo";
import OverviewExperience from "./TabComponents/OverviewExperience";
import OverviewArtWork from "./TabComponents/OverviewArtWork";
import Schedule from "./TabComponents/Schedule";
import RecentlyPlayed from "./TabComponents/RecentlyPlayed";
import ContactButton from "./ContactButton";
import ProfileHeader from "./ProfileHeader";
import { WP14, WP20 } from "../../../../constants/Sizes";
import { GREEN, ORANGE, ORANGE_TOMATO } from "../../../../constants/Colors";

const mapStateToProps = ({
  auth,
  review,
  payment: { paymentSource },
  song: { playback, soundObject },
  helper: { cameFromBottomNavigation },
}) => ({
  userData: auth.user,
  addProfile: review.addProfile,
  paymentSource,
  playback,
  soundObject,
  cameFromBottomNavigation,
});

const mapFromNavigationParam = (getParam) => ({
  idUser: getParam("idUser", 0),
  navigateBackOnDone: getParam("navigateBackOnDone", false),
  shouldShowTrialModal: getParam("shouldShowTrialModal", false),
  isAcceptMember: getParam("isAcceptMember", {}),
  onSubmitCallback: getParam("onSubmitCallback", () => {}),
});

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
  setUserDetail: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail,
  }),
  setCameFromBottomNavigation,
  emptyClearBlueMessage,
};

const sfLogoWhite = require("sf-assets/icons/sfLogoWhite.png");
const sfLogoCircle = require("sf-assets/icons/v3/sfCircleBadge.png");

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      containerKey: Math.random(),
      cameFromBottomNavigation: props.cameFromBottomNavigation,
      id_user: props.idUser,
      isMine: props.idUser == 0 || props.userData.id_user == props.idUser,
      portofolio: null,
      profile: null,
      shouldComponentUpdate: false,
      index: 0,
      isAcceptMember:
        typeof this.props.isAcceptMember === "object"
          ? false
          : this.props.isAcceptMember,
      portofolioss: [
        {
          url_video: "overview",
          title: "OVERVIEW",
        },
        {
          url_video: "activities",
          title: "ACTIVITIES",
        },
        // {
        //   key: 'schedule',
        //   title: 'SCHEDULE',
        // },
        // {
        //   key: 'recently_played',
        //   title: 'RECENTLY PLAYED',
        // },
      ],
      routes: [
        {
          key: "overview",
          title: "OVERVIEW",
        },
        {
          key: "activities",
          title: "ACTIVITIES",
        },
        // {
        //   key: 'schedule',
        //   title: 'SCHEDULE',
        // },
        // {
        //   key: 'recently_played',
        //   title: 'RECENTLY PLAYED',
        // },
      ],
      song: [],
      showAllSong: false,
      video: [],
      performance_journey: [],
      artwork: [],
      feeds: [],
      schedules: [],
      initialized: false,
      accountType: null,
      updateKey: Math.random(),
      isActivityReady: false,
      following: false,
      uploadingCover: false,
      shouldShowTrial: true,
      shouldShowTrialModal: props.shouldShowTrialModal,
      claimFreeTrial: false,
      loadingChat: false,
      slicedArtwork: [],
      isLoadingSong: false,
      dotsOptions: [
        null,
        {
          onPress: noop,
          iconName: "account-plus",
          iconColor: SHIP_GREY_CALM,
          iconSize: "huge",
          title: "Follow This Artist",
        },
        {
          onPress: () =>
            props.navigateTo("ShareScreen", {
              id_artist: props.idUser,
              id: props.idUser,
              type: "profile",
              title: "Bagikan Profile",
            }),
          iconName: "share-variant",
          iconColor: SHIP_GREY_CALM,
          iconSize: "huge",
          title: "Share",
        },
        {
          onPress: () => props.navigateTo("FeedbackScreen"),
          iconName: "message-text",
          iconColor: SHIP_GREY_CALM,
          iconSize: "huge",
          title: "Feedback & Report",
        },
      ],
      deletededFeeds: [],
    };

    // API CALLS
    this._getProfile = this._getProfile.bind(this);
    this._getJourney = this._getJourney.bind(this);
    this._getInitialFeeds = this._getInitialFeeds.bind(this);
    this._getFeeds = this._getFeeds.bind(this);
    this._claimTrial = this._claimTrial.bind(this);
    this._onFollow = this._onFollow.bind(this);
    this._onUploadCover = this._onUploadCover.bind(this);

    // COMPONENT
    this._renderScene = this._renderScene.bind(this);
    this._messageIcon = this._messageIcon.bind(this);
    this._activityTab = this._activityTab.bind(this);

    // NAVIGATIONS
    this._onSubscribe = this._onSubscribe.bind(this);
    this._onEditProfile = this._onEditProfile.bind(this);
    this._onSetting = this._onSetting.bind(this);
    this._goToArtistSpotlight = this._goToArtistSpotlight.bind(this);

    // OTHERS
    this._setIndex = this._setIndex.bind(this);
    this._getIdUser = this._getIdUser.bind(this);
    this._closeTrial = this._closeTrial.bind(this);
    this._onTrial = this._onTrial.bind(this);
    this._showTrialModal = this._showTrialModal.bind(this);
    this._hideTrialModal = this._hideTrialModal.bind(this);
    this._header = this._header.bind(this);
    this._onDeletePost = this._onDeletePost.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    const shouldUpdate =
      nextState.shouldComponentUpdate || nextState.song != this.state.song;
    return shouldUpdate;
  }

  static getDerivedStateFromProps(props, state) {
    if (!!state.profile && !state.isMine) {
      const followed = state.profile.followed_by_viewer;
      const dotsOptions = produce(state.dotsOptions, (draft) => {
        draft[1].title = followed
          ? "Unfollow This Participant"
          : "Follow This Participant";
        draft[1].iconName = followed ? "account-minus" : "account-plus";
        draft[1].onPress = followed
          ? state._onFollow(false)
          : state._onFollow(true);
      });
      return { dotsOptions };
    }
    return null;
  }

  _onSubmit = (shortlist) => {
    const { navigateTo, navigateBack, onSubmitCallback } = this.props;
    const { id_user } = this.state;
    navigateBack();
    onSubmitCallback(id_user, shortlist);
  };

  _onUploadCover = (cover_image) => {
    this.setState(
      produce((draft) => {
        draft.uploadingCover = true;
      }),
      () => {
        const {
          userData: { id_user },
          dispatch,
        } = this.props;
        dispatch(postUploadCoverImage, {
          id_user,
          cover_image,
        }).then(() =>
          this._getUserProfile().then((profile) =>
            this.setState(
              produce((draft) => {
                draft.profile = profile;
                draft.uploadingCover = false;
              })
            )
          )
        );
      }
    );
  };

  _goToArtistSpotlight = () => this.props.navigateTo("ArtistSpotlightScreen");

  _onTrial = () => {
    this.setState(
      produce((draft) => {
        draft.claimFreeTrial = true;
        draft.shouldShowTrial = false;
      }),
      this._claimTrial
    );
  };

  _claimTrial = () => {
    const {
      dispatch,
      setUserDetail,
      userData: { id_user },
    } = this.props;
    dispatch(postClaimFreeTrial, {
      id_user,
    }).then(() => {
      this.setState(
        produce((draft) => {
          draft.shouldShowTrialModal = false;
        }),
        () => {
          this._getProfile();
          dispatch(getProfileDetail, { id_user, show: "all" }).then(
            (dataResponse) => {
              setUserDetail(dataResponse);
            }
          );
        }
      );
    });
  };

  _getInitialFeeds = () => {
    const { feeds } = this.state;
    return getUserFeed({
      timeline_from: this._getIdUser(),
      id_user: this.props.userData.id_user,
      start: isArray(feeds) ? feeds.length : 0,
      limit: 10,
    });
  };

  _onAdd = () =>
    this.props.navigateTo("FeedPostScreen", {
      refreshProfile: this.props.refreshProfile,
      accountType: this.props.accountType,
    });

  _getFeeds = () => {
    const { feeds } = this.state;
    getUserFeed({
      timeline_from: this._getIdUser(),
      id_user: this.props.userData.id_user,
      start: isArray(feeds) ? feeds.length : 0,
      limit: 20,
    }).then(({ data: { result } }) => {
      feeds &&
        this.setState(
          produce((draft) => {
            (result || []).map((feed) => {
              draft.feeds.push(feed);
            });
            draft.isActivityReady = true;
          })
        );
      // this.setState({
      //   feeds: [...feeds, ...(result || [])],
      //   isActivityReady: true,
      // });
    });
  };

  _onFollow = (isFollowAction) => () => {
    const { following } = this.state;
    const { userData: my } = this.props;
    const params = {
      [isFollowAction ? "id_user" : "id_user_following"]: this._getIdUser(),
      [isFollowAction ? "followed_by" : "id_user"]: my.id_user,
    };
    !following &&
      this.setState(
        produce((draft) => {
          draft.following = true;
        }),
        async () => {
          try {
            await (isFollowAction ? postProfileFollow : postProfileUnfollow)(
              params
            );
          } finally {
            this._refreshSongOnFollow();
          }
        }
      );
  };

  _refreshSongOnFollow = () => {
    this.setState(
      produce((draft) => {
        draft.shouldComponentUpdate = false;
        draft.song = [];
        draft.isLoadingSong = true;
      }),
      () => {
        Promise.all([
          this._getUserProfile(), // PROFILE
          this._getJourney("song", 100, true), // SONGS
        ]).then((results) => {
          this.setState(
            produce((draft) => {
              draft.following = false;
              draft.isLoadingSong = false;
              draft.profile = results[0];
              draft.shouldComponentUpdate = true;
              draft.song = _journeyMapper(results[1]);
            })
          );
        });
      }
    );
  };

  _onEditProfile = () => {
    this.props.navigateTo("ProfileForm", { refreshProfile: this._getProfile });
  };

  _onSubscribe = () => {
    this.props.paymentSourceSet("profile");
    this.props.navigateTo("MembershipScreen");
  };

  _tabShadow = (
    <LinearGradient
      colors={[
        "rgba(0,0,0,.042)",
        "rgba(0,0,0,.028)",
        "rgba(0,0,0,.012)",
        "rgba(0,0,0,0)",
      ]}
      style={style.tabShadow}
    />
  );

  _onDeletePost = (idJourney) => {
    const { dispatch } = this.props;
    let { deletededFeeds } = this.state;
    this.setState({ deletededFeeds: [...deletededFeeds, parseInt(idJourney)] });
    dispatch(deleteJourney, idJourney);
  };

  _activityTab = () => {
    const { feeds, isMine, index, isActivityReady } = this.state;
    return (
      <View style={this.state.index == 1 ? style.shown : style.hidden}>
        <View style={style.tabShadowWrapper}>{this._tabShadow}</View>
        {isEmpty(feeds) && (
          <View>
            <View style={style.emptyHeading}>
              <Text
                size={"medium"}
                color={NAVY_DARK}
                type={"Circular"}
                weight={600}
              >
                Aktivitas
              </Text>
            </View>
            <View style={style.emptyState}>
              <Image
                style={style.emptyStateIcon}
                source={require("sf-assets/icons/v3/plusCircle.png")}
              />
              <View>
                <Text centered color={GUN_METAL} type={"Circular"} weight={500}>
                  Belum ada postingan
                </Text>
                <Text
                  centered
                  style={style.emptyStateDescription}
                  size={"mini"}
                  color={SHIP_GREY_CALM}
                  type={"Circular"}
                  weight={300}
                >
                  {isMine
                    ? "Semua postingan  yang kamu lakukan \n akan tampil pada halaman ini"
                    : upperFirst(this.state.profile.full_name.split(" ")[0]) +
                      " belum ada postingan"}
                  {"   "}
                </Text>
                <View>
                  {isMine && (
                    <ButtonV2
                      onPress={this._onAdd}
                      style={style.emptyStateCta}
                      color={REDDISH}
                      textColor={WHITE}
                      textSize={"mini"}
                      text={"Buat Postingan"}
                    />
                  )}
                </View>
              </View>
            </View>
          </View>
        )}
        {!isEmpty(feeds) && (
          <ProfileMyPostTab
            isTabReady={isActivityReady}
            feeds={feeds}
            deletededFeeds={this.state.deletededFeeds}
            isActive={index == 1 && isActivityReady}
            getFeeds={this._getFeeds}
            isMine={isMine}
            onDeletePost={this._onDeletePost}
            getInitialScreenData={() => {}}
          />
        )}
      </View>
    );
  };

  _showTrialModal = () => {
    this.setState(
      produce((draft) => {
        draft.shouldShowTrialModal = true;
      })
    );
  };

  _hideTrialModal = () => {
    this.setState(
      produce((draft) => {
        draft.shouldShowTrialModal = false;
      })
    );
  };

  _showAllSong = () => {
    this.setState(
      produce((draft) => {
        draft.showAllSong = true;
      })
    );
  };

  _renderScene = ({ route }) => {
    const {
      artwork,
      experience,
      index,
      initialized,
      isLoadingSong,
      isMine,
      profile,
      schedules,
      showAllSong,
      slicedArtwork,
      song,
      video,
      portofolio,
      accountType,
    } = this.state;
    const {
      navigateTo,
      userData: { id_user: idViewer },
      dispatch,
    } = this.props;
    return {
      overview: () => (
        <View style={index == 0 ? style.shown : style.hidden}>
          {this._tabShadow}
          <View style={style.tabContent}>
            <OverviewPortofolio
              idViewer={idViewer}
              refreshProfile={this._refreshProfile}
              name={profile.full_name}
              navigateTo={navigateTo}
              isMine={isMine}
              initialized={initialized}
              items={portofolio}
            />
          </View>
          {/* <View style={style.tabContentNoPadding}>
            <OverviewVideo
              idViewer={idViewer}
              refreshProfile={this._refreshProfile}
              name={profile.full_name}
              navigateTo={navigateTo}
              isMine={isMine}
              initialized={initialized}
              items={video}
            />
          </View>
          <View style={style.tabContent}>
            <OverviewExperience
              refreshProfile={this._refreshProfile}
              profile={profile}
              name={profile.full_name}
              navigateTo={navigateTo}
              isMine={isMine}
              initialized={initialized}
              items={experience}
            />
          </View>
          <View style={style.tabContentNoPadding}>
            <OverviewArtWork
              totaItems={artwork.length}
              profile={profile}
              name={profile.full_name}
              userData={this.props.userData}
              refreshProfile={this._refreshProfile}
              navigateTo={navigateTo}
              isMine={isMine}
              initialized={initialized}
              artwork={artwork}
              items={slicedArtwork}
              getArtwork={() => this._getJourney('visual_art', 200, true)}
            />
          </View> */}
        </View>
      ),
      activities: this._activityTab,
      schedule: () => (
        <View style={this.state.index == 2 ? style.shown : style.hidden}>
          {this._tabShadow}
          <View style={style.tabContentNoPaddingNoBorder}>
            <Schedule
              dispatch={dispatch}
              profile={profile}
              name={profile.full_name}
              onTrial={this._onTrial}
              showTrialModal={this._showTrialModal}
              onSubscribe={this._onSubscribe}
              navigateTo={navigateTo}
              isMine={isMine}
              initialized={initialized}
              items={schedules}
              refreshProfile={this._getSchedules}
            />
          </View>
        </View>
      ),
      recently_played: () => (
        <View style={this.state.index == 3 ? style.shown : style.hidden}>
          {this._tabShadow}
          <View style={style.tabContentNoPadding}>
            <RecentlyPlayed
              title={"Lagu yang disukai"}
              emptyTitle={
                isMine
                  ? "Belum ada lagu yang kamu sukai"
                  : "Belum ada lagu yang disukai"
              }
              emptyIcon={require("sf-assets/icons/v3/heartCircle.png")}
              emptyDescription={
                isMine
                  ? "Cek artist spotlight kami dan temukan lagu\nyang kamu sukai sekarang"
                  : `${
                      upperFirst(profile.full_name).split(" ")[0]
                    } belum memilih lagu yang disukai`
              }
              emptyCta={"Lihat Artist Spotlight"}
              emptyCtaOnPress={this._goToArtistSpotlight}
              navigateTo={navigateTo}
              isMine={isMine}
              initialized={initialized}
              items={[]}
            />
          </View>
          <View style={style.tabContentNoPadding}>
            <RecentlyPlayed
              title={"Lagu yang diputar"}
              emptyTitle={
                isMine
                  ? "Kamu belum memutar lagu apapun"
                  : "Belum ada lagu yang dimainkan"
              }
              emptyIcon={require("sf-assets/icons/v3/videoCircle.png")}
              emptyDescription={
                isMine
                  ? "Cek artist spotlight kami dan temukan kumpulan\nlagu-lagu dari artist pilihan kami sekarang."
                  : `${
                      upperFirst(profile.full_name).split(" ")[0]
                    } belum memutar lagu`
              }
              emptyCta={"Lihat Artist Spotlight"}
              emptyCtaOnPress={this._goToArtistSpotlight}
              navigateTo={navigateTo}
              isMine={isMine}
              initialized={initialized}
              items={[]}
            />
          </View>
        </View>
      ),
    }[route.key]();
  };

  _setIndex = (index) => {
    const { profile } = this.state;
    this.setState(
      produce((draft) => {
        draft.index = index;
      }),
      () => {
        if (index == 1) {
          this._getFeeds();
        } else if (index == 2 && profile.account_type === "premium") {
          this._getSchedules();
        }
      }
    );
  };

  _getIdUser = () => {
    let id_user = this.state.isMine
      ? this.props.userData.id_user
      : this.state.id_user;
    return id_user;
  };

  _getJourney = (type, limit, refresh = false) => {
    let id_user = this._getIdUser();
    let start = refresh
      ? 0
      : this.state[type == "visual_art" ? "artwork" : type].length;
    return new Promise((resolve, reject) => {
      getJourneyV2({
        type,
        id_user,
        id_viewer: this.props.userData.id_user,
        start,
        limit: limit || 3,
      }).then(({ data: { result } }) => {
        return resolve(result || []);
      });
    });
  };

  _getSchedules = async () => {
    let id_user = this._getIdUser();
    try {
      const {
        data: { result: schedules },
      } = await getSchedule({ id_user });
      this.setState(
        produce((draft) => {
          draft.schedules = schedules;
        })
      );
    } catch (e) {}
  };

  _getUserProfile = () => {
    const { id_user, isMine } = this.state;
    const { dispatch, userData } = this.props;
    const profileParams = isMine
      ? { id_user: userData.id_user }
      : { id_user, id_viewer: userData.id_user };

    return new Promise((resolve, reject) => {
      dispatch(getProfileDetailV2, profileParams).then((result) =>
        resolve(result)
      );
    });
  };

  _sendMessage = () => {
    this.setState(
      produce((draft) => {
        draft.loadingChat = true;
      }),
      () => {
        initiateRoom(this.props.userData, this.state.profile).then(
          (chatRoomDetail) => {
            this.setState(
              produce((draft) => {
                draft.loadingChat = false;
              }),
              () => {
                this.props.navigateTo("ChatRoomScreen", chatRoomDetail);
              }
            );
          }
        );
      }
    );
  };

  _getProfile = (refresh = false) => {
    const { userData: my } = this.props;
    this.setState(
      // produce((draft) => {
      //   draft.shouldComponentUpdate = false
      //   if (!isIOS())
      //     draft.dotsOptions[0] = {
      //       onPress: get(my, 'account_type') === 'user' ? this._onSubscribe : noop,
      //       image: require('sf-assets/icons/badge.png'),
      //       imageSize: WP6,
      //       titleColor: REDDISH,
      //       titleWeight: 500,
      //       title:
      //         get(my, 'account_type') === 'user'
      //           ? 'Upgrade Premium'
      //           : 'Premium User',
      //     }
      // }),
      () => {
        if (this.state.index == 2) this._getSchedules();
        Promise.all([
          this._getUserProfile(), // PROFILE
          this._getJourney("song", 100, refresh), // SONGS
          this._getJourney("video", 50, refresh), // VIDEOS
          this._getJourney("performance_journey", 50, refresh), // EXPERIENCES
          this._getJourney("visual_art", 200, refresh), // VISUAL ART
          this._getInitialFeeds(),
          this.props.dispatch(getSubscriptionDetail, {
            id_user: this._getIdUser(),
          }), // SUBSCRIPTION
        ])
          .catch((error) => {
            this._getProfile();
          })
          .then((results) => {
            if (refresh && this.props.addProfile) {
              clearAddProfile().then(() => {
                if (
                  !isEmpty(this.state.song) &&
                  !isEmpty(this.state.video) &&
                  !isEmpty(this.state.experience) &&
                  !isEmpty(this.state.artwork)
                ) {
                  setTimeout(() => showReviewIfNotYet(), 1000);
                }
              });
            }
            this.setState(
              produce((draft) => {
                draft.profile = results[0];
                draft.portofolio = results[0].portofolio;
                draft.song = _journeyMapper(results[1]);
                draft.video = _journeyMapper(results[2]);
                draft.experience = _journeyMapper(results[3]);
                draft.artwork = _journeyMapper(results[4]);
                draft.slicedArtwork = _journeyMapper(results[4]).slice(0, 4);
                draft.feeds = [
                  ...this.state.feeds,
                  ...(results[5].data?.result || []),
                ];
                draft.initialized = true;
                draft.accountType = results[6].package_type
                  ? results[6].package_type.toLowerCase()
                  : "free";
              }),
              () =>
                this.setState(
                  produce((draft) => {
                    draft.shouldComponentUpdate = true;
                  })
                )
            );
          });
      }
    );
  };

  _backHandler = () => {
    const { cameFromBottomNavigation } = this.state;
    const { navigateTo, navigateBack } = this.props;

    cameFromBottomNavigation && navigateTo("ExploreStack");
    !cameFromBottomNavigation && navigateBack();
    return true;
  };

  _attachBackAction() {
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this._backHandler
    );
  }

  _releaseBackAction() {
    try {
      this.backHandler.remove();
    } catch (err) {
      /*silent is gold*/
    }
  }

  // eslint-disable-next-line react/sort-comp
  componentDidMount() {
    const {
      userData: { id_user },
      navigateBack,
      navigateTo,
    } = this.props;
    this.setState(
      produce((draft) => {
        draft._onFollow = this._onFollow;
      })
    );
    this.props.setCameFromBottomNavigation(false);
    InteractionManager.runAfterInteractions(this._getProfile);
    const { navigation } = this.props;
    this._willBlurSubscription = navigation.addListener("blur", () => {
      this._releaseBackAction();
      const { playback, soundObject } = this.props;
      playback.isLoaded && soundObject.pauseAsync();
    });
    this._didFocusSubscription = navigation.addListener("focus", () => {
      const {
        userData: { id_user: userId },
      } = this.props;
      if (!userId) {
        navigateBack();
        this.props.navigation.navigate("AuthNavigator", {
          // screen: "LoginScreen",
          screen: "LoginEmailScreen",
          params: {
            forceReload: true,
          },
        });
      } else {
        if (!isNil(this.state.profile)) {
          this._getUserProfile().then((profile) =>
            this.setState(
              produce((draft) => {
                draft.profile = profile;
              })
            )
          );
        }
        this._attachBackAction();
        setTimeout(this.props.emptyClearBlueMessage, 4000);
      }
    });
  }

  // eslint-disable-next-line no-dupe-class-members
  componentWillUnmount() {
    try {
      this._willBlurSubscription();
      this._didFocusSubscription();
    } catch (err) {
      // keep silent
    }
  }

  _closeTrial = () =>
    this.setState(
      produce((draft) => {
        draft.shouldShowTrial = false;
      })
    );

  _onSetting = () => this.props.navigateTo("SettingScreen");

  _hamburgerMenu = (
    <View style={style.hamburgerMenu}>
      <Touchable onPress={this._onSetting}>
        <View style={HEADER.rightIcon}>
          <Image
            style={style.hamburgerMenuImage}
            source={require("sf-assets/icons/v3/hamburgerMenu.png")}
          />
        </View>
      </Touchable>
    </View>
  );

  _renderModalToggler = (toggleModal) => (
    <Touchable onPress={toggleModal} style={HEADER.rightIcon}>
      <Icon
        centered
        size="small"
        color={SHIP_GREY_CALM}
        name="dots-three-horizontal"
        type="Entypo"
      />
    </Touchable>
  );

  _header = () => {
    const { isMine, profile } = this.state;
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftOnPress={this._backHandler}
          centered
          textType={"Circular"}
          textSize={"slight"}
          text={upperFirst(
            isMine ? "Your Profile" : !isNil(profile) ? profile.full_name : ""
          )}
          rightComponent={
            isMine ? (
              this._hamburgerMenu
            ) : (
              <MenuOptions
                options={this.state.dotsOptions}
                triggerComponent={this._renderModalToggler}
              />
            )
          }
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
        <View>
          <MessageBarClearBlue />
        </View>
      </View>
    );
  };

  _messageIcon = () => {
    return this.state.loadingChat ? (
      <ActivityIndicator color={REDDISH} />
    ) : (
      <ImageComponent
        style={style.sendIcon}
        size={WP305}
        aspectRatio={1 / 1}
        source={require("sf-assets/icons/send.png")}
      />
    );
  };

  _refreshProfile = () => {
    this._getProfile(true);
  };

  _renderTabBar = (props) => (
    <TabBar
      {...props}
      scrollEnabled
      renderLabel={({ route, focused, color }) => (
        <View style={style.tabBar}>
          <View
            style={
              color == REDDISH ? style.tabBarInnerActive : style.tabBarInner
            }
          >
            <View style={style.tabBarInnerWrapper}>
              <View style={{ height: WP5, width: 1 }} />
              {route.title === "SCHEDULE" && (
                <Image
                  style={style.tabBarInnerWrapperImage}
                  source={sfLogoCircle}
                />
              )}
              <Text weight={600} size={"xmini"} type={"Circular"} color={color}>
                {route.title}
              </Text>
              <View style={{ height: WP5, width: 1 }} />
            </View>
          </View>
        </View>
      )}
      style={style.tabBarStyle}
      contentContainerStyle={style.tabBarContentContainerStyle}
      indicatorStyle={style.tabBarIndicatorStyle}
      indicatorContainerStyle={style.tabBarIndicatorContainerStyle}
      tabStyle={style.tabBarTabStyle}
      activeColor={REDDISH}
      inactiveColor={SHIP_GREY_CALM}
    />
  );

  render() {
    const {
      accountType,
      following,
      index,
      isMine,
      profile,
      portofolio,
      routes,
      containerKey,
      isAcceptMember,
    } = this.state;

    return (
      <Container
        key={containerKey}
        onPullDownToRefresh={this._refreshProfile}
        renderHeader={this._header}
        isLoading={isNil(profile)}
        isReady={!isNil(profile)}
        // scrollable
        hasBottomNavbar
        scrollBackgroundColor={PALE_WHITE}
      >
        {!isNil(profile) && (
          <ScrollView>
            <View>
              <ProfileHeader
                uploadingCover={this.state.uploadingCover}
                uploadCover={this._onUploadCover}
                claimFreeTrial={this.state.claimFreeTrial}
                showTrialModal={this._showTrialModal}
                hideTrialModal={this._hideTrialModal}
                shouldShowTrialModal={this.state.shouldShowTrialModal}
                onTrial={this._onTrial}
                isMine={this.state.isMine}
                shouldShowTrial={this.state.shouldShowTrial}
                onTrialClosed={this._closeTrial}
                accountType={accountType}
                profile={profile}
                navigateTo={this.props.navigateTo}
                getProfile={this._getProfile}
                editProfile={this._onEditProfile}
              />
              {isMine && (
                <View style={style.profileActions}>
                  {accountType === "free" && (
                    <ButtonV2
                      color={REDDISH}
                      onPress={this._onSubscribe}
                      style={style.profileActionsPremium}
                      text={!isIOS() ? "Premium" : null}
                      textColor={WHITE}
                    >
                      {isIOS() && (
                        <Image
                          style={style.profileActionsPremiumLogo}
                          source={sfLogoWhite}
                        />
                      )}
                    </ButtonV2>
                  )}
                  <ButtonV2
                    icon={"pencil"}
                    disabled={false}
                    textColor={SHIP_GREY}
                    borderColor={PALE_BLUE_TWO}
                    style={style.profileActionsEdit}
                    onPress={this._onEditProfile}
                    text={"Edit Profile"}
                  />
                </View>
              )}
              {!isMine && (
                <View style={style.profileActions}>
                  {!profile.followed_by_viewer && (
                    <ButtonV2
                      onPress={this._onFollow(true)}
                      icon={!following ? "account-plus" : null}
                      style={style.profileActionsEdit}
                      forceBorderColor={following}
                      borderColor={REDDISH}
                      textColor={following ? REDDISH : WHITE}
                      iconColor={following ? REDDISH : WHITE}
                      color={following ? "rgba(255, 101, 31, 0.1)" : REDDISH}
                      disabled={following}
                      text={following ? "Following" : "Follow"}
                    />
                  )}
                  {profile.followed_by_viewer && (
                    <ButtonV2
                      onPress={this._onFollow(false)}
                      textColor={following ? WHITE : REDDISH}
                      borderColor={
                        following ? "rgba(255, 101, 31, 0.1)" : REDDISH
                      }
                      forceBorderColor
                      style={style.profileActionsEdit}
                      color={following ? REDDISH : "rgba(255, 101, 31, 0.1)"}
                      disabled={following}
                      text={following ? "Unfollowing" : "Following"}
                    />
                  )}
                  {/* <View style={style.profileActionsSeparator} />
                <ButtonV2
                  leftComponent={this._messageIcon}
                  onPress={this._sendMessage}
                  style={style.profileActionsEdit}
                  textColor={this.state.loadingChat ? REDDISH : SHIP_GREY}
                  text={this.state.loadingChat ? null : 'Message'}
                /> */}
                  <View style={style.profileActionsSeparator} />
                  <ContactButton
                    navigateTo={this.props.navigateTo}
                    profile={profile}
                  />
                </View>
              )}

              <TabView
                scrollEnabled={false}
                renderTabBar={this._renderTabBar}
                swipeEnabled={false}
                navigationState={{ index, routes }}
                renderScene={this._renderScene}
                onIndexChange={this._setIndex}
              />
            </View>
          </ScrollView>
        )}
        {isAcceptMember && (
          <View
            style={{
              bottom: 0,
              position: "absolute",
              width: "100%",
              height: WP20,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <ButtonV2
              onPress={() => this._onSubmit(2)}
              icon={"close"}
              style={{ ...style.profileActionsEdit, height: WP14 }}
              forceBorderColor={following}
              borderColor={REDDISH}
              textColor={ORANGE}
              textSize={"xsmall"}
              iconColor={ORANGE_TOMATO}
              color={WHITE}
              disabled={following}
              text={"Deny"}
            />
            <ButtonV2
              onPress={() => this._onSubmit(1)}
              icon={"check"}
              style={{ ...style.profileActionsEdit, height: WP14 }}
              forceBorderColor={following}
              borderColor={REDDISH}
              textColor={GREEN}
              iconColor={GREEN}
              textSize={"xsmall"}
              color={WHITE}
              disabled={following}
              text={"Accept"}
            />
          </View>
        )}
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProfileScreen),
  mapFromNavigationParam
);
