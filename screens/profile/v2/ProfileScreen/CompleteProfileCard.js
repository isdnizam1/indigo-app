import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import noop from 'lodash-es/noop'
import Image from '../../../../components/Image'
import Text from '../../../../components/Text'
import {
  PALE_GREY_THREE,
  PALE_WHITE,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
} from '../../../../constants/Colors'
import { WP105, WP2, WP205, WP3, WP5 } from '../../../../constants/Sizes'
import Icon from '../../../../components/Icon'
import { SHADOW_STYLE } from '../../../../constants/Styles'

const styles = {
  progressWrapper: {
    backgroundColor: PALE_GREY_THREE,
    borderRadius: 6,
    overflow: 'hidden',
    height: WP105,
    marginVertical: WP105,
  },
  progressInner: {
    backgroundColor: REDDISH,
    height: WP105,
  },
  completeProfileSection: {
    backgroundColor: PALE_GREY_THREE,
    padding: WP5,
  },
  completeProfileContainer: {
    borderRadius: WP205,
    backgroundColor: PALE_WHITE,
    ...SHADOW_STYLE.shadowSoft,
  },
  body: {
    paddingHorizontal: WP5,
    paddingVertical: WP3,
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    borderBottomColor: PALE_GREY_THREE,
    borderBottomWidth: 2,
  },
  footer: {
    paddingHorizontal: WP5,
    paddingVertical: WP2,
  },
}

const CompleteProfileCard = ({ profileProgress, editProfile, profile }) => {
  const { completed = 0, total_progress = 0 } = profileProgress
  const _progress = () => {
    const progressValue = 40 + (60 / total_progress) * completed
    if (profile.profile_completed) {
      return (
        <View style={styles.progressWrapper}>
          <View style={{ ...styles.progressInner, width: `${100}%` }} />
        </View>
      )
    } else {
      return (
        <View style={styles.progressWrapper}>
          <View style={{ ...styles.progressInner, width: `${progressValue}%` }} />
        </View>
      )
    }
  }
  if (profile.profile_completed) {
    return (
      <View style={styles.completeProfileSection}>
        <TouchableOpacity
          style={styles.completeProfileContainer}
          onPress={editProfile}
        >
          <View style={styles.body}>
            <Image
              source={require('sf-assets/images/complete_profile_card.png')}
              style={{ marginRight: WP3 }}
            />
            <View style={{ flex: 1, flexGrow: 1, marginRight: WP3 }}>
              <Text type='Circular' weight={400} size='mini' color={SHIP_GREY}>
                Kelengkapan Profile
                {/* <Text type='Circular' weight={400} size='mini' color={REDDISH}>
                  {`  ${completed} / ${total_progress}`}
                </Text> */}
              </Text>
              {_progress()}
              <Text type='Circular' weight={300} size='mini' color={SHIP_GREY_CALM}>
                Profile sudah
                <Text type='Circular' weight={300} size='mini' color={REDDISH}>
                  {' lengkap'}
                </Text>
              </Text>
            </View>

            <Icon
              type='Entypo'
              name='chevron-right'
              centered
              color={REDDISH}
              size='huge'
            />
          </View>
          <View style={styles.footer}>
            <Text type='Circular' weight={300} size='tiny' color={SHIP_GREY_CALM}>
              {profile.registered_referral_code.toLowerCase() === '1000startup' ||
              profile.registered_referral_code.toLowerCase() === 'mudamajubersama'
                ? 'Lengkapi profilemu & daftarkan Startupmu untuk mulai mencari tim'
                : profileProgress.label}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  } else {
    return (
      <View style={styles.completeProfileSection}>
        <TouchableOpacity
          style={styles.completeProfileContainer}
          onPress={editProfile}
        >
          <View style={styles.body}>
            <Image
              source={require('sf-assets/images/complete_profile_card.png')}
              style={{ marginRight: WP3 }}
            />
            <View style={{ flex: 1, flexGrow: 1, marginRight: WP3 }}>
              <Text type='Circular' weight={400} size='mini' color={SHIP_GREY}>
                Lengkapi profilemu
                <Text type='Circular' weight={400} size='mini' color={REDDISH}>
                  {`  ${completed} / ${total_progress}`}
                </Text>
              </Text>
              {_progress()}
              <Text type='Circular' weight={300} size='mini' color={SHIP_GREY_CALM}>
                Tuliskan
                <Text type='Circular' weight={300} size='mini' color={REDDISH}>
                  {` ${profileProgress.type} `}
                </Text>
                di edit profile
              </Text>
            </View>

            <Icon
              type='Entypo'
              name='chevron-right'
              centered
              color={REDDISH}
              size='huge'
            />
          </View>
          <View style={styles.footer}>
            <Text type='Circular' weight={300} size='tiny' color={SHIP_GREY_CALM}>
              {profile.registered_referral_code.toLowerCase() === '1000startup' ||
              profile.registered_referral_code.toLowerCase() === 'mudamajubersama'
                ? 'Lengkapi profilemu & daftarkan Startupmu untuk mulai mencari tim'
                : profileProgress.label}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

CompleteProfileCard.propTypes = {
  profileProgress: PropTypes.objectOf(PropTypes.any),
  editProfile: PropTypes.func,
}

CompleteProfileCard.defaultProps = {
  profileProgress: {},
  editProfile: noop,
}

export default CompleteProfileCard
