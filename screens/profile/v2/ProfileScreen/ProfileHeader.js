import React, { Component } from "react";
import PropTypes from "prop-types";
import { LinearGradient } from "expo-linear-gradient";
import { get, isEmpty, isObject } from "lodash-es";
import {
  ActivityIndicator,
  Image,
  Modal,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import Avatar from "sf-components/Avatar";
import Text from "sf-components/Text";
import Touchable from "sf-components/Touchable";
import { selectPhoto, takePhoto } from "sf-utils/upload";
import MenuOptions from "sf-components/MenuOptions";
import {
  GUN_METAL,
  NAVY_DARK,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from "sf-constants/Colors";
import { WP10, WP2, WP3, WP4, WP40, WP5, WP6, WP85 } from "sf-constants/Sizes";
import { TOUCH_OPACITY } from "sf-constants/Styles";
import ReadMore from "react-native-read-more-text";
import style from "./style";
import CompleteProfileCard from "./CompleteProfileCard";

const borderRadius = WP2;

const modal = StyleSheet.create({
  modal: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,.6)",
    justifyContent: "center",
    alignItems: "center",
  },
  modalContent: {
    backgroundColor: WHITE,
    width: WP85,
    minHeight: WP40,
    justifyContent: "center",
    alignItems: "center",
    borderRadius,
  },
  modalContentIllustration: {
    width: WP40,
    height: WP40,
    marginTop: WP10,
    marginBottom: WP5,
    paddingHorizontal: WP5,
  },
  modalContentTitle: {
    marginBottom: WP3,
  },
  modalContentDescription: {
    marginBottom: WP10,
    paddingHorizontal: WP5,
    lineHeight: WP5,
  },
  modalContentCta: {
    backgroundColor: REDDISH,
    width: "100%",
    paddingVertical: WP4 + 2,
    borderBottomLeftRadius: borderRadius,
    borderBottomRightRadius: borderRadius,
  },
  modalClose: {
    position: "absolute",
    top: 0,
    right: 0,
    padding: WP3,
  },
  modalCloseIcon: {
    width: WP6,
    height: WP6,
  },
});

class ProfileHeader extends Component {
  static propTypes = {
    profile: PropTypes.object,
    accountType: PropTypes.string,
    shouldShowTrial: PropTypes.bool,
    shouldShowTrialModal: PropTypes.bool,
    isMine: PropTypes.bool,
    claimFreeTrial: PropTypes.bool,
    uploadingCover: PropTypes.bool,
    showTrialModal: PropTypes.func,
    hideTrialModal: PropTypes.func,
    onTrial: PropTypes.func,
    onTrialClosed: PropTypes.func,
    uploadCover: PropTypes.func,
    navigateTo: PropTypes.func,
    editProfile: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this._renderTruncatedFooter = this._renderTruncatedFooter.bind(this);
    this._renderRevealedFooter = this._renderRevealedFooter.bind(this);
    this.state = {
      shouldRenderBio: true,
    };
  }

  _onSelectCover =
    (key, multiple = false, index) =>
    (text, replace = false) => {
      key == "coverb64" && this.props.uploadCover(text);
    };

  componentDidUpdate = (prevProps, prevState) => {
    const isBioChanged =
      get(prevProps.profile, "about_me") !==
      get(this.props.profile, "about_me");

    if (isBioChanged && prevState.shouldRenderBio) {
      this.setState({ shouldRenderBio: false });
    } else if (!prevState.shouldRenderBio) {
      this.setState({ shouldRenderBio: true });
    }
  };

  _zoomAvatar = () => {
    const {
      navigateTo,
      profile: { profile_picture: url, full_name: title },
    } = this.props;
    navigateTo("GalleryScreen", {
      images: [
        {
          url,
          title,
        },
      ],
    });
  };

  _coverOptions = [
    {
      onPress: selectPhoto(this._onSelectCover, "cover", [18, 7], null, [
        820,
        820 * (7 / 18),
      ]),
      title: "Choose from Library",
      withBorder: true,
    },
    {
      onPress: takePhoto(this._onSelectCover, "cover", [18, 7], null, [
        820,
        820 * (7 / 18),
      ]),
      title: "Take Photo",
    },
  ];

  _renderRevealedFooter = (callback) => {
    return (
      <Touchable onPress={callback} style={style.readMore}>
        <Text type={"Circular"} color={REDDISH} size={"slight"}>
          Read less
        </Text>
      </Touchable>
    );
  };

  _renderTruncatedFooter = (callback) => {
    return (
      <Touchable onPress={callback} style={style.readMore}>
        <Text type={"Circular"} color={REDDISH} size={"slight"}>
          Read more
        </Text>
      </Touchable>
    );
  };

  _renderBio = () => {
    const { profile } = this.props;

    if (!this.state.shouldRenderBio)
      return (
        <Text
          style={style.artistHeaderDescriptionAbout}
          size={"slight"}
          type={"Circular"}
          weight={300}
          color={SHIP_GREY}
          numberOfLines={4}
        >
          {get(profile, "about_me")}
        </Text>
      );

    return (
      <ReadMore
        numberOfLines={4}
        renderTruncatedFooter={this._renderTruncatedFooter}
        renderRevealedFooter={this._renderRevealedFooter}
      >
        <Text
          style={style.artistHeaderDescriptionAbout}
          size={"slight"}
          type={"Circular"}
          weight={300}
          color={SHIP_GREY}
        >
          {get(profile, "about_me")}
        </Text>
      </ReadMore>
    );
  };

  _renderCompleteProfile = () => {
    return <View style={style.completeProfileContainer} />;
  };

  render() {
    const {
      profile,
      shouldShowTrial,
      shouldShowTrialModal,
      uploadingCover,
      onTrialClosed,
      isMine,
      showTrialModal,
      hideTrialModal,
      onTrial,
      claimFreeTrial,
      navigateTo,
      getProfile,
      editProfile,
    } = this.props;
    return get(profile, "account_type") === "user" ? (
      <View>
        {isMine && !profile.profile_completed && (
          <CompleteProfileCard
            profileProgress={profile.profile_progress}
            editProfile={editProfile}
            profile={profile}
          />
        )}
        {shouldShowTrial &&
          isMine &&
          get(profile, "account_type") == "user" &&
          get(profile, "free_trial") && (
            <View>
              <TouchableWithoutFeedback onPress={showTrialModal}>
                <View style={style.profileHeaderFreeTrial}>
                  <Image
                    source={require("sf-assets/images/bgWaveDark.png")}
                    style={style.profileHeaderFreeTrialBackground}
                  />
                  <Text
                    size={"mini"}
                    type={"Circular"}
                    weight={500}
                    color={WHITE}
                  >
                    Free trial 14 hari upgrade ke Artist Profile
                  </Text>
                  <View style={style.profileHeaderFreeTrialLink}>
                    <Text
                      size={"mini"}
                      type={"Circular"}
                      weight={500}
                      color={REDDISH}
                    >
                      Upgrade Sekarang
                    </Text>
                    <Image
                      style={style.profileHeaderFreeTrialLinkArrow}
                      source={require("sf-assets/icons/v3/chevronRightReddish.png")}
                    />
                  </View>
                </View>
              </TouchableWithoutFeedback>
              <View style={style.profileHeaderFreeTrialClose}>
                <TouchableWithoutFeedback onPress={onTrialClosed}>
                  <Image
                    style={style.profileHeaderFreeTrialCloseIcon}
                    source={require("sf-assets/icons/v3/closeGrey.png")}
                  />
                </TouchableWithoutFeedback>
              </View>
            </View>
          )}
        <View style={style.profileHeader}>
          <View style={style.profileHeaderAvatar}>
            <Avatar
              onPress={this._zoomAvatar}
              size={"huge"}
              shadow={false}
              image={get(profile, "profile_picture")}
              verifiedStatus={get(profile, "verified_status")}
              isProfile={true}
            />
          </View>
          <View style={style.profileHeaderInfo}>
            <Text
              lineHeight={WP6}
              type={"Circular"}
              weight={600}
              color={NAVY_DARK}
            >
              {get(profile, "full_name")}
            </Text>
            <Text
              style={style.profileHeaderInfoJob}
              size={"slight"}
              type={"Circular"}
              weight={400}
              color={SHIP_GREY}
            >
              {get(profile, "job_title")}
            </Text>
            <View style={style.profileHeaderInfoFollows}>
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() =>
                  navigateTo(
                    "ProfileFollowScreen",
                    {
                      isMine,
                      idUser: get(profile, "id_user"),
                      onRefresh: () => getProfile(),
                    },
                    "push"
                  )
                }
                style={style.profileHeaderInfoFollows}
              >
                <Text
                  size={"slight"}
                  type={"Circular"}
                  weight={400}
                  color={GUN_METAL}
                >{`${get(profile, "total_followers")} `}</Text>
                <Text
                  size={"slight"}
                  type={"Circular"}
                  weight={300}
                  color={SHIP_GREY_CALM}
                >
                  Followers {"   "}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() =>
                  navigateTo(
                    "ProfileFollowScreen",
                    {
                      isMine,
                      idUser: get(profile, "id_user"),
                      initialTab: 1,
                      onRefresh: () => getProfile(),
                    },
                    "push"
                  )
                }
                style={style.profileHeaderInfoFollows}
              >
                <Text
                  size={"slight"}
                  type={"Circular"}
                  weight={400}
                  color={GUN_METAL}
                >{`${get(profile, "total_following")} `}</Text>
                <Text
                  size={"slight"}
                  type={"Circular"}
                  weight={300}
                  color={SHIP_GREY_CALM}
                >
                  Following
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={style.profileAbout}>
          {this._renderBio()}
          {isObject(get(profile, "location")) && (
            <Text
              size={"mini"}
              type={"Circular"}
              weight={300}
              color={SHIP_GREY_CALM}
            >
              {get(profile, "location.city_name")},{" "}
              {get(profile, "location.country_name")}
            </Text>
          )}
        </View>
        <Modal
          transparent
          animationType={"fade"}
          onRequestClose={hideTrialModal}
          visible={shouldShowTrialModal}
        >
          <View style={modal.modal}>
            {claimFreeTrial && (
              <View style={modal.modalContent}>
                <ActivityIndicator color={REDDISH} size={"large"} />
              </View>
            )}
            {!claimFreeTrial && (
              <View style={modal.modalContent}>
                <Image
                  source={require("sf-assets/icons/v3/freeTrialIllustration.png")}
                  style={modal.modalContentIllustration}
                />
                <Text
                  style={modal.modalContentTitle}
                  centered
                  type={"Circular"}
                  weight={500}
                  color={GUN_METAL}
                >
                  14 Hari Free Trial Artist Profile
                </Text>
                <Text
                  style={modal.modalContentDescription}
                  size={"mini"}
                  type={"Circular"}
                  weight={300}
                  centered
                  color={SHIP_GREY_CALM}
                >
                  Tunggu apa lagi? Tunjukan karya dan gapai pendengar barumu
                  sekarang
                </Text>
                <View style={modal.modalContentCta}>
                  <TouchableWithoutFeedback onPress={onTrial}>
                    <Text
                      size={"slight"}
                      type={"Circular"}
                      weight={500}
                      centered
                      color={WHITE}
                    >
                      Coba Sekarang
                    </Text>
                  </TouchableWithoutFeedback>
                </View>
                <View style={modal.modalClose}>
                  <TouchableWithoutFeedback onPress={hideTrialModal}>
                    <Image
                      source={require("sf-assets/icons/v3/closeGrey.png")}
                      style={modal.modalCloseIcon}
                    />
                  </TouchableWithoutFeedback>
                </View>
              </View>
            )}
          </View>
        </Modal>
      </View>
    ) : (
      <View style={style.artistHeader}>
        <View style={style.artistHeaderCover}>
          <Image
            style={style.artistHeaderCoverImage}
            resizeMode={"cover"}
            source={
              isEmpty(get(profile, "cover_image"))
                ? require("../../../../assets/images/v3/defaultCover.png")
                : { uri: get(profile, "cover_image") }
            }
          />
          {/* <LinearGradient
            colors={['rgba(0,0,0,0)', 'rgba(0,0,0,.3)', 'rgba(0,0,0,.7)']}
            style={style.artistHeaderCoverGradient}
          /> */}
          <View style={style.artistHeaderCoverAvatar}>
            <Avatar
              onPress={this._zoomAvatar}
              size={"extraMassive"}
              shadow={false}
              image={get(profile, "profile_picture")}
            />
          </View>
          {isMine && (
            <View style={style.artistHeaderCamera}>
              <MenuOptions
                options={this._coverOptions}
                triggerComponent={(toggleModal) => (
                  <TouchableWithoutFeedback onPress={toggleModal}>
                    {uploadingCover ? (
                      <ActivityIndicator color={REDDISH} size={"large"} />
                    ) : (
                      <Image
                        style={style.artistHeaderCameraIcon}
                        source={require("sf-assets/icons/v3/cameraWithCircleShape.png")}
                      />
                    )}
                  </TouchableWithoutFeedback>
                )}
              />
            </View>
          )}
        </View>
        <View style={style.artistHeaderName}>
          <Text
            size={"large"}
            centered
            type={"Circular"}
            weight={600}
            color={NAVY_DARK}
          >
            {get(profile, "full_name")}
          </Text>
          <Text
            centered
            style={style.profileHeaderInfoJob}
            size={"slight"}
            type={"Circular"}
            weight={400}
            color={SHIP_GREY}
          >
            {get(profile, "job_title")}
          </Text>
        </View>
        <View style={style.artistHeaderFollows}>
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            onPress={() =>
              navigateTo(
                "ProfileFollowScreen",
                {
                  isMine,
                  idUser: get(profile, "id_user"),
                  onRefresh: () => getProfile(),
                },
                "push"
              )
            }
            style={style.artistHeaderFollowsItem}
          >
            <Text
              size={"slight"}
              type={"Circular"}
              weight={400}
              color={GUN_METAL}
            >{`${get(profile, "total_followers")} `}</Text>
            <Text
              size={"slight"}
              type={"Circular"}
              weight={300}
              color={SHIP_GREY_CALM}
            >
              Followers
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            onPress={() =>
              navigateTo(
                "ProfileFollowScreen",
                {
                  isMine,
                  idUser: get(profile, "id_user"),
                  initialTab: 1,
                  onRefresh: () => getProfile(),
                },
                "push"
              )
            }
            style={style.artistHeaderFollowsItem}
          >
            <Text
              size={"slight"}
              type={"Circular"}
              weight={400}
              color={GUN_METAL}
            >{`${get(profile, "total_following")} `}</Text>
            <Text
              size={"slight"}
              type={"Circular"}
              weight={300}
              color={SHIP_GREY_CALM}
            >
              Following
            </Text>
          </TouchableOpacity>
          {/* <View style={style.artistHeaderFollowsItem}>
            <Text
              size={'slight'}
              type={'Circular'}
              weight={400}
              color={GUN_METAL}
            >{`${get(profile, 'total_listener')} `}</Text>
            <Text
              size={'slight'}
              type={'Circular'}
              weight={300}
              color={SHIP_GREY_CALM}
            >
              Listeners
            </Text>
          </View> */}
        </View>
        <View style={style.artistHeaderDescription}>
          <Text
            style={style.artistHeaderDescriptionCompany}
            size={"slight"}
            type={"Circular"}
            weight={400}
            color={GUN_METAL}
          >
            {get(profile, "company")}
          </Text>
          {this._renderBio()}
          {isObject(get(profile, "location")) && (
            <Text
              size={"mini"}
              type={"Circular"}
              weight={300}
              color={SHIP_GREY_CALM}
            >
              {get(profile, "location.city_name")},{" "}
              {get(profile, "location.country_name")}
            </Text>
          )}
        </View>
      </View>
    );
  }
}

export default ProfileHeader;
