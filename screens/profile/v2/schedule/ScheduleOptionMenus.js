export const scheduleOptions = (props) => {
  const privateMenu = [
    {
      type: 'menu',
      image: require('../../../../assets/icons/mdi_edit.png'),
      imageStyle: {},
      name: 'Edit',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('ScheduleForm', { initialValues: props.schedule, refreshProfile: props.refreshProfile })
      }
    },
    {
      type: 'menu',
      image: require('../../../../assets/icons/mdi_delete.png'),
      imageStyle: {},
      name: 'Delete',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.onDelete()
      }
    }
  ]

  const menus = [
    // {
    //   type: 'menu',
    //   image: require('../../../../assets/icons/mdi_share.png'),
    //   imageStyle: {},
    //   name: 'Share',
    //   textStyle: {},
    //   onPress: () => {
    //     props.onClose()
    //   }
    // },
    {
      type: 'separator'
    },
    {
      type: 'menu',
      image: require('../../../../assets/icons/close.png'),
      imageStyle: {},
      name: 'Tutup',
      textStyle: {},
      onPress: props.onClose
    },
  ]

  if (props.isMine) {
    menus.splice.apply(menus, [0, 0].concat(privateMenu))
  }

  return {
    withPromoteUser: false,
    menus
  }
}

export default scheduleOptions
