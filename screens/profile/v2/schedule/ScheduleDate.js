import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import moment from 'moment'
import Text from '../../../../components/Text'
import { PALE_LIGHT_BLUE_TWO, TOMATO, WHITE } from '../../../../constants/Colors'
import { WP135, WP17 } from '../../../../constants/Sizes'

const config = {
  size: {
    small: {
      width: WP135,
      monthSize: 'mini',
      dateSize: 'small',
    },
    large: {
      width: WP17,
      monthSize: 'slight',
      dateSize: 'medium',
    }
  }
}

class ScheduleDate extends Component {
  render() {
    const {
      date,
      size,
      style
    } = this.props
    const currentSize = config.size[size] || config.size.small
    return (
      <View style={{
        borderWidth: 1,
        borderColor: PALE_LIGHT_BLUE_TWO,
        borderRadius: 8,
        overflow: 'hidden',
        width: currentSize.width,
        height: currentSize.width,
        ...style
      }}
      >
        <View style={{ backgroundColor: TOMATO }}>
          <Text centered color={WHITE} weight={500} size={currentSize.monthSize}>{moment(date).format('MMM')}</Text>
        </View>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text centered weight={500}size={currentSize.dateSize} >{moment(date).format('DD')}</Text>
        </View>
      </View>
    )
  }
}

ScheduleDate.propTypes = {
  date: PropTypes.string,
  size: PropTypes.string,
}

ScheduleDate.defaultProps = {
  size: 'small'
}

export default ScheduleDate
