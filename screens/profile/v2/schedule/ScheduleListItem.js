import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import moment from 'moment/min/moment-with-locales'
import { connect } from 'react-redux'
import Text from '../../../../components/Text'
import {
  GREY_WARM,
  GUN_METAL,
  SHIP_GREY,
  SHIP_GREY_CALM,
} from '../../../../constants/Colors'
import Icon from '../../../../components/Icon'
import { HP1, WP2, WP3, WP4, WP5, WP6, WP8 } from '../../../../constants/Sizes'
import SoundfrenExploreOptions from '../../../../components/explore/SoundfrenExploreOptions'
import Modal from '../../../../components/Modal'
import { deleteSchedule } from '../../../../actions/api'
import Image from '../../../../components/Image'
import ScheduleOptionMenus from './ScheduleOptionMenus'
import ScheduleDate from './ScheduleDate'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

class ScheduleListItem extends Component {
  _onDelete = async () => {
    const { dispatch, userData, schedule, refreshProfile } = this.props
    const body = {
      id_user: userData.id_user,
      id_schedule: schedule.id_schedule,
    }
    try {
      await dispatch(deleteSchedule, body)
      refreshProfile()
    } catch (e) {}
  };

  _scheduleDetail = ({ toggleModal }) => {
    const { userData, schedule } = this.props

    return (
      <View
        style={{
          alignItems: 'center',
          padding: WP6,
          paddingTop: WP8,
        }}
      >
        <Image
          style={{ position: 'absolute', right: WP3, top: WP3 }}
          source={require('../../../../assets/icons/close.png')}
          imageStyle={{ width: WP6 }}
          onPress={toggleModal}
        />
        <ScheduleDate date={schedule.event_date} size='large' style={{}} />
        <View style={{}}>
          <View style={{ marginVertical: WP4 }}>
            <Text
              centered
              color={GUN_METAL}
              weight={500}
              type='Circular'
              size='medium'
            >
              {schedule.event_name}
            </Text>
          </View>
          <View style={{ marginBottom: WP6 }}>
            <Text centered type='Circular' color={SHIP_GREY} size='mini'>
              {moment(schedule.event_date).locale('id').format('ddd, DD MMM YYYY')}
            </Text>
            <Text centered type='Circular' color={SHIP_GREY} size='mini'>
              {schedule.event_location}
            </Text>
            <Text centered type='Circular' color={SHIP_GREY} size='mini'>
              {moment(schedule.event_date).locale('id').format('hh:mm A')}
            </Text>
          </View>

          <Text centered type='Circular' color={SHIP_GREY} size='xmini'>
            Performance by
          </Text>
          <Text
            centered
            type='Circular'
            color={GUN_METAL}
            size='slight'
            weight={400}
          >
            {schedule.full_name}
          </Text>
        </View>
      </View>
    )
  };

  _renderOptions = () => {
    const { schedule, navigateTo, isMine, refreshProfile } = this.props

    return (
      <Modal
        position='bottom'
        swipeDirection='none'
        renderModalContent={({ toggleModal }) => {
          return (
            <SoundfrenExploreOptions
              menuOptions={ScheduleOptionMenus}
              navigateTo={navigateTo}
              onClose={toggleModal}
              schedule={schedule}
              onDelete={this._onDelete}
              isMine={isMine}
              refreshProfile={refreshProfile}
            />
          )
        }}
      >
        {({ toggleModal }, M) => (
          <View style={{ paddingTop: WP2 }}>
            <Icon
              style={{ flexGrow: 0 }}
              centered
              onPress={toggleModal}
              name={'dots-three-horizontal'}
              type='Entypo'
              color={GREY_WARM}
            />
            {M}
          </View>
        )}
      </Modal>
    )
  };

  render() {
    const { schedule, isMine } = this.props

    return (
      <Modal
        position='center'
        swipeDirection='none'
        renderModalContent={this._scheduleDetail}
        style={{
          borderBottomRightRadius: HP1,
          borderBottomLeftRadius: HP1,
        }}
      >
        {({ toggleModal }, M) => (
          <>
            <TouchableOpacity
              onPress={toggleModal}
              style={{
                flexDirection: 'row',
                paddingHorizontal: WP5,
                paddingVertical: WP2,
                marginBottom: WP2,
              }}
            >
              <ScheduleDate
                date={schedule.event_date}
                size='small'
                style={{ marginRight: WP3, alignSelf: 'center' }}
              />
              <View
                style={{
                  flexGrow: 1,
                  flex: 1,
                  justifyContent: 'center',
                  paddingRight: WP3,
                }}
              >
                <Text
                  color={GUN_METAL}
                  weight={500}
                  numberOfLines={1}
                  ellipsizeMode='tail'
                  type='Circular'
                >
                  {schedule.event_name}
                </Text>
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    type='Circular'
                    color={SHIP_GREY_CALM}
                    size='xmini'
                    numberOfLines={1}
                    ellipsizeMode='tail'
                  >
                    {moment(schedule.event_date)
                      .locale('id')
                      .format('ddd, DD MMM YYYY hh:mm A')}
                    <Text size='tiny' color={SHIP_GREY_CALM}>
                      {' • '}
                    </Text>
                    {schedule.event_location}
                  </Text>
                </View>
              </View>
              {isMine && this._renderOptions()}
            </TouchableOpacity>
            {M}
          </>
        )}
      </Modal>
    )
  }
}

ScheduleListItem.propTypes = {}

ScheduleListItem.defaultProps = {}

export default connect(mapStateToProps, {})(ScheduleListItem)
