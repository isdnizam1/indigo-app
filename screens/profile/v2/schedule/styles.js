import { WP3, WP4, WP6 } from '../../../../constants/Sizes'
import { PALE_BLUE } from '../../../../constants/Colors'

export default {
  formSection: {
    paddingHorizontal: WP4,
    paddingBottom: WP3,
    marginBottom: WP6,
    borderBottomColor: PALE_BLUE,
    borderBottomWidth: 1
  }
}
