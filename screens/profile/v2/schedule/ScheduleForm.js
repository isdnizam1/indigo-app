import * as yup from 'yup'
import { isEmpty, noop, isUndefined } from 'lodash-es'
import React from 'react'
import { Keyboard, ScrollView, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { CheckBox } from 'react-native-elements'
import moment from 'moment'
import { authDispatcher } from 'sf-services/auth'
import { postSchedule, putSchedule } from 'sf-actions/api'
import { alertMessage } from 'sf-utils/alert'
import Container from 'sf-components/Container'
import { GUN_METAL, ORANGE_BRIGHT, PALE_SALMON, REDDISH, SHIP_GREY, SHIP_GREY_CALM, SILVER_TWO, WHITE } from 'sf-constants/Colors'
import Form from 'sf-components/Form'
import HeaderNormal from 'sf-components/HeaderNormal'
import { SHADOW_STYLE, TEXT_INPUT_STYLE, TOUCH_OPACITY } from 'sf-constants/Styles'
import { HP2, WP1, WP2, WP3, WP308, WP4, WP5, WP8 } from 'sf-constants/Sizes'
import Text from 'sf-components/Text'
import Image from 'sf-components/Image'
import InputTextLight from 'sf-components/InputTextLight'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import SelectDate from 'sf-components/SelectDate'
import SelectTime from 'sf-components/SelectTime'
import { FONTS } from 'sf-constants/Fonts'
import STYLES from './styles'

export const FORM_VALIDATION = yup.object().shape({
  event_name: yup.string().required(),
  event_location: yup.string().required(),
  date: yup.string().required(),
  time: yup.string().required()
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', noop),
  initialValues: getParam('initialValues', {}),
  navigateBackOnDone: getParam('navigateBackOnDone', false)
})

class ScheduleForm extends React.Component {
  state = {
    isUploading: false
  }

  _onAddSchedule = async ({ formData }) => {
    const {
      userData: { id_user },
      navigateBack,
      refreshProfile,
      dispatch,
      initialValues
    } = this.props
    const action = isEmpty(initialValues) ? postSchedule : putSchedule
    if (
      isEmpty(formData.event_name) ||
      isEmpty(formData.event_location) ||
      isEmpty(formData.date) ||
      isEmpty(formData.time)
    ) alertMessage('Oops!', 'Please fill the form correctly')
    else {
      let body = {
        id_user,
        event_name: formData.event_name,
        event_location: formData.event_location,
        event_date: `${formData.date } ${formData.time}`,
        broadcast_notif: formData.broadcast_notif
      }
      if(!isUndefined(initialValues.id_schedule)) body.id_schedule = initialValues.id_schedule
      if(moment(new Date()).diff(moment(body.event_date)) >= 0) {
        alertMessage('Oops!', 'Tanggal dan waktu schedule tidak boleh kurang dari waktu sekarang')
      } else {
        this.setState({ isUploading: true })
        dispatch(action, body).then((result) => {
          refreshProfile()
          navigateBack()
        })
      }
    }
  }

  render() {
    const {
      initialValues,
      navigateBack
    } = this.props

    const {
      isUploading
    } = this.state

    return (
      <Container colors={[WHITE, WHITE]}>
        <Form
          initialValue={{
            broadcast_notif: 0, ...initialValues,
            date: moment(initialValues.event_date).format('YYYY-MM-DD'),
            time: moment(initialValues.event_date).format('HH:mm')
          }}
          validation={FORM_VALIDATION}
          onSubmit={this._onAddSchedule}
        >
          {({ onChange, onSubmit, formData, isValid }) => (
            <View style={{ overflow: 'hidden' }}>
              <HeaderNormal
                iconLeftOnPress={() => navigateBack()}
                wrapperStyle={{
                  ...SHADOW_STYLE.shadowBold,
                  shadowOffset: {
                    width: 0,
                    height: 10,
                  },
                  shadowOpacity: 0.05,
                  backgroundColor: WHITE,
                  marginBottom: WP8,
                  paddingBottom: WP2
                }}
                textType='Circular'
                textColor={SHIP_GREY}
                textWeight={400}
                text={isEmpty(initialValues) ? 'Tambah Schedule' : 'Edit Schedule'}
                centered
                rightComponent={(
                  <View style={{ position: 'absolute', right: 0 }}>
                    {!isUploading &&
                    <TouchableOpacity
                      onPress={() => {
                        Keyboard.dismiss()
                        onSubmit()
                      }}
                      activeOpacity={TOUCH_OPACITY}
                      disabled={!isValid}
                    >
                      <Text size='slight' weight={400} color={isValid ? ORANGE_BRIGHT : PALE_SALMON}>
                        {isEmpty(initialValues) ? 'Tambah' : 'Simpan'}
                      </Text>
                    </TouchableOpacity>
                    }
                    {isUploading &&
                    <Image
                      spinning
                      source={require('sf-assets/icons/ic_loading.png')}
                      imageStyle={{ width: WP8, aspectRatio: 1 }}
                    />
                    }
                  </View>
                )}
              />

              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}
              >
                <View style={STYLES.formSection}>
                  <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>SCHEDULE INFO</Text>

                  <InputTextLight
                    withLabel={false}
                    placeholder='Nama event/acara yang kamu ikuti'
                    value={formData.event_name}
                    onChangeText={onChange('event_name')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Nama Event'
                    editable={!isUploading}
                  />
                  <InputTextLight
                    withLabel={false}
                    placeholder='Lokasi event/acara yang kamu ikuti'
                    value={formData.event_location}
                    onChangeText={onChange('event_location')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Lokasi Event'
                    editable={!isUploading}
                  />

                  <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
                  Tanggal Event
                  </Text>

                  <SelectDate
                    value={formData.date}
                    dateFormat='YYYY-MM-DD'
                    mode='date'
                    minimumDate={new Date()}
                    onChangeDate={onChange('date')}
                    style={{ marginBottom: WP4 }}
                    disabled={isUploading}
                  />

                  <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
                  Waktu Tampil Mulai Pukul:
                  </Text>

                  <SelectTime
                    value={formData.time}
                    timeFormat='HH:mm'
                    mode='time'
                    onChangeDate={onChange('time')}
                    style={{ marginBottom: WP4 }}
                  />
                </View>
                <View style={{ ...STYLES.formSection, borderBottomWidth: 0 }}>
                  <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP2 }}>
                    Bagikan Jadwal Konser
                  </Text>
                  <Text weight={300} type='Circular' size={'xmini'} color={SHIP_GREY_CALM} style={{ marginBottom: WP4 }}>
                    Sekarang kamu dapat membagikan jadwal konsermu kepada semua followers. Bagikan informasi ini kepada mereka untuk datang & melihat penampilanmu sekarang.
                  </Text>

                  <CheckBox
                    disabled={isUploading}
                    checked={formData.broadcast_notif == 1}
                    containerStyle={{
                      backgroundColor: WHITE, borderWidth: 0, margin: 0,
                      paddingHorizontal: -WP2, marginBottom: WP2
                    }}
                    onPress={() => {
                      onChange('broadcast_notif')(formData.broadcast_notif == 1 ? 0 : 1)
                    }}
                    title='Bagikan kepada followers saya'
                    textStyle={{ fontSize: WP308, color: formData.broadcast_notif == 1 ? REDDISH : SHIP_GREY_CALM }}
                    fontFamily={FONTS.Circular['200']}
                    wrapperStyle={{ marginHorizontal: -WP2, marginTop: -WP3 }}
                    checkedIcon={(
                      <Image
                        imageStyle={{ width: WP5, aspectRatio: 1 }}
                        source={require('sf-assets/icons/icCheckboxActive.png')}
                      />
                    )}
                    uncheckedIcon={(
                      <Image
                        imageStyle={{ width: WP5, aspectRatio: 1 }}
                        source={require('sf-assets/icons/icCheckboxInactive.png')}
                      />
                    )}
                  />
                </View>
              </ScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ScheduleForm),
  mapFromNavigationParam
)
