import React from 'react'
import { connect } from 'react-redux'
import { FlatList } from 'react-native'
import { uniqWith, isEqual } from 'lodash-es'
import { _enhancedNavigation, Container, HeaderNormal, Loader } from '../components'
import { WP5, HP1, HP3 } from '../constants/Sizes'
import { WHITE } from '../constants/Colors'
import { getAdvancedSearch } from '../actions/api'
import People from '../components/search/People'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  peoples: getParam('peoples', [])
})

class FriendSuggestionScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      peoples: props.peoples,
      isLoading: false,
      endDataReached: false
    }
    this.moreSuggestions = this.moreSuggestions.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.isLoading != nextState.isLoading
  }

  moreSuggestions() {
    const { dispatch, userData: { id_user } } = this.props
    const { peoples, isLoading, endDataReached } = this.state
    !endDataReached && !isLoading && this.setState({ isLoading: true }, () => {
      dispatch(getAdvancedSearch, { type: 'people', action: 'suggestion', start: peoples.length, limit: 15, id_user }).then((result) => {
        this.setState({
          peoples: uniqWith([...peoples, ...result.people], isEqual)
        }, () => this.setState({ isLoading: false }))
        result.people.length == 0 && this.setState({
          endDataReached: true
        })
      })
    })
  }

  componentDidMount() {
    this.moreSuggestions()
  }

  render() {
    const { navigateTo, navigateBack } = this.props
    const { peoples, isLoading } = this.state
    return (
      <Container
        scrollable={false}
        isReady={true}
        renderHeader={() => (
          <HeaderNormal
            style={{ paddingVertical: HP1 }}
            backgroundColor={WHITE}
            iconLeftOnPress={navigateBack}
            text='People Suggestions'
          />
        )}
      >
        <FlatList
          data={peoples}
          onEndReached={this.moreSuggestions}
          onEndReachedThreshold={0.5}
          ListFooterComponent={() => {
            return isLoading ? (<Loader size={'mini'} isLoading={isLoading} />) : null
          }}
          keyExtractor={(item, index) => item.id_user}
          renderItem={({ item, index, separators }) => {
            const user = item
            return (<People style={{ paddingLeft: WP5, paddingRight: WP5, marginTop: index == 0 ? HP3 : 0 }} navigateTo={navigateTo} user={user} />)
          }}
        />
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(FriendSuggestionScreen),
  mapFromNavigationParam
)
