import React from 'react'
import { Platform, StyleSheet, TouchableOpacity, View } from 'react-native'
import Constants from 'expo-constants'
import { connect } from 'react-redux'
import { map } from 'lodash-es'
import { setLoading } from 'sf-services/app/actionDispatcher'
import { _enhancedNavigation, Container, Icon, Text } from '../components'
import {
  GREY_LIGHT,
  GUN_METAL,
  PALE_GREY,
  PALE_GREY_TWO,
  SHIP_GREY_CALM,
} from '../constants/Colors'
import { WP12, WP2, WP4, WP5 } from '../constants/Sizes'
import { TOUCH_OPACITY } from '../constants/Styles'
import { removeLocalStorage } from '../utils/storage'
import { KEY_USER_ID, KEY_USER_LAST_SKIPPED_STEP } from '../constants/Storage'
import Image from '../components/Image'
import { initiateRoom } from '../utils/helper'
import HeaderNormal from '../components/HeaderNormal'
import { GET_USER_DETAIL } from '../services/auth/actionTypes'

const styles = StyleSheet.create({
  buttonContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: WP4,
    marginHorizontal: WP4,
    borderBottomWidth: 1,
    borderBottomColor: PALE_GREY,
  },
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  setLoading,
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail,
  }),
}

const mapFromNavigationParam = () => ({})

class StartupProfileSettingScreen extends React.Component {
  _onPressChatAdmin = async () => {
    const { userData, navigateTo, navigateBack } = this.props
    const user = {
      id_user: 960,
    }
    const chatRoomDetail = await initiateRoom(userData, user)
    const onRefresh = async () => {}
    const navigateBackCallback = async () => {
      navigateBack()
    }
    await navigateTo('ChatRoomScreen', {
      ...chatRoomDetail,
      onRefresh,
      navigateBackCallback,
    })
  };

  render() {
    const { navigateTo, navigateBack, userData, setLoading } = this.props
    const DATA_MENU = [
      {
        iconName: 'lock',
        title: 'Change Password',
        onPress: () => navigateTo('ProfileUpdatePasswordScreen'),
        validation: true
      },
      {
        iconName: 'message-text',
        title: 'Feedback & Report',
        onPress: () => navigateTo('FeedbackScreen'),
        validation: true,
      },
      {
        iconName: 'exit-to-app',
        title: 'Log out',
        onPress: async () => {
          await removeLocalStorage([KEY_USER_ID, KEY_USER_LAST_SKIPPED_STEP])
          this.props.setUserDetailDispatcher({})
          navigateTo('LoadingScreen', {}, 'replace')
        },
        validation: true,
      },
    ]
    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text={'Settings'}
            textSize={'mini'}
            centered
          />
        )}
      >
        <View style={{ flex: 1, backgroundColor: PALE_GREY_TWO, paddingTop: WP2 }}>
          {map(DATA_MENU, (menu, index) => {
            return (
              menu.validation && (
                <TouchableOpacity
                  key={`${Math.random() + index}`}
                  onPress={menu.onPress}
                  activeOpacity={TOUCH_OPACITY}
                  style={styles.buttonContainer}
                >
                  <View style={{ width: WP12, alignItems: 'flex-start' }}>
                    {menu.image ? (
                      <View>
                        <Image
                          imageStyle={{
                            width: WP5,
                            aspectRatio: 1,
                            alignSelf: 'flex-start',
                          }}
                          source={menu.image}
                        />
                      </View>
                    ) : menu.iconName ? (
                      <Icon
                        size={'large'}
                        color={SHIP_GREY_CALM}
                        name={menu.iconName}
                        type={'MaterialCommunityIcons'}
                      />
                    ) : null}
                  </View>
                  <View style={{ alignItems: 'flex-start', flexGrow: 1 }}>
                    <Text
                      type='Circular'
                      color={GUN_METAL}
                      weight={menu.is_premium ? 400 : 300}
                      size='mini'
                    >
                      {menu.title}
                    </Text>
                  </View>
                </TouchableOpacity>
              )
            )
          })}
          <View style={{ padding: WP4 }}>
            <Text
              type='Circular'
              size='xmini'
              weight={300}
              color={GREY_LIGHT}
            >{`${Constants.manifest.extra.build.description} Version ${Constants.manifest.version} (${Constants.manifest.extra.build.number})`}</Text>
          </View>
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(StartupProfileSettingScreen),
  mapFromNavigationParam,
)
