const SUCCESS_INFO = {
  title: 'Draft audition diterima',
  content: 'Soundfren akan mereview audition ini dan menghubungi kamu segera',
  image: require('../../assets/icons/icSuccesLarge.png')
}

const EXIT_WARNING = {
  title: 'Buat Audition',
  content: 'Pembuatan audition sedang berlangsung.\n Lanjutkan buat audition?'
}

export default {
  SUCCESS_INFO,
  EXIT_WARNING
}
