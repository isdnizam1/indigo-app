import * as yup from 'yup'
import React from 'react'
import { BackHandler, Clipboard, Keyboard, ScrollView, TouchableOpacity, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { connect } from 'react-redux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { postAdsSubmission } from '../../actions/api'
import { WP1, WP100, WP25, WP30, WP6, WP8 } from '../../constants/Sizes'
import styles from '../create_session/styles'
import Image from '../../components/Image'
import Container from '../../components/Container'
import { NO_COLOR, ORANGE_BRIGHT, PALE_SALMON, PALE_WHITE, REDDISH, SHADOW_GRADIENT, SHIP_GREY, SILVER_TWO } from '../../constants/Colors'
import Form from '../../components/Form'
import HeaderNormal from '../../components/HeaderNormal'
import { HEADER, TEXT_INPUT_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import Text from '../../components/Text'
import PopUpInfo from '../../components/popUp/PopUpInfo'
import InputTextLight from '../../components/InputTextLight'
import Icon from '../../components/Icon'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import SnackBar from '../../components/snackbar/SnackBar'
import ProgressBar from '../../components/snackbar/ProgressBar'
import { submissionDispatcher } from '../../services/submission'
import constants from './constants'

export const FORM_VALIDATION = yup.object().shape({
  description: yup.string().required(),
  term_condition: yup.string().required()
})

const mapStateToProps = ({ auth, submission }) => ({
  userData: auth.user,
  currentSubmission: submission.data
})

const mapDispatchToProps = {
  setDataSubmission: submissionDispatcher.submissionDataSet,
  clearDataSubmission: submissionDispatcher.submissionDataClear
}

const mapFromNavigationParam = (getParam) => ({
  submissionDetail: getParam('submissionDetail', {})
})

class CreateSubmissionInfoForm extends React.Component {
  static navigationOptions = () => ({
    gesturesEnabled: true
  })

  constructor(props) {
    super(props)
    this.state = {
      isUploading: false,
      isSuccess: false
    }
  }
  snackBar = React.createRef()
  progressBar = React.createRef()

  _onSuccessCallback = () => {
    this.props.navigation.popToTop()
    this.props.navigateTo('NotificationScreen', { defaultTab: 'activities' }, 'push')
    // this.props.navigation.navigate('ExploreScreen')
    // this.props.navigation.navigate('Submission')
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _backHandler = async () => {
    if (this.state.isSuccess) {
      this._onSuccessCallback()
    } else {
      this.props.navigateBack()
    }
  }

  _onSubmit = async ({ formData }) => {
    const { dispatch, submissionDetail, userData, clearDataSubmission } = this.props
    const { term_condition, description, submission_reference = '' } = formData
    this.setState({ isUploading: true })
    this.progressBar.current._animatedProgress(WP30)
    const body = {
      ...submissionDetail,
      id_user: userData.id_user,
      term_condition, description, submission_reference
    }
    try {
      await dispatch(postAdsSubmission, body)
      this.progressBar.current._animatedProgress(WP100)
      this.snackBar.current._fadeIn()
      clearDataSubmission()
      this.setState({ isUploading: false, isSuccess: true })
    } catch (e) {
      this.setState({ isUploading: false })
    }
  }

  render() {
    let {
      navigateBack, currentSubmission, setDataSubmission
    } = this.props

    const {
      isUploading, isSuccess
    } = this.state

    return (
      <Container
        colors={[PALE_WHITE, PALE_WHITE]} isAvoidingView={true}
      >
        <Form
          validation={FORM_VALIDATION}
          onSubmit={this._onSubmit}
          initialValue={currentSubmission}
          onChangeInterceptor={({ key, text }) => setDataSubmission({ [key]: text })}
        >
          {({ onChange, formData, onSubmit, isValid }) => (
            <View style={{ overflow: 'hidden' }}>
              <View>
                <HeaderNormal
                  iconLeftOnPress={navigateBack}
                  textType='Circular'
                  textColor={SHIP_GREY}
                  textWeight={400}
                  text='Buat Audition'
                  textSize={'slight'}
                  centered
                  withDummyRightIcon
                  rightComponent={(
                    <View style={{ position: 'absolute', right: 0, alignItems: 'center' }}>
                      {!isUploading &&
                        <TouchableOpacity
                          onPress={() => {
                            Keyboard.dismiss()
                            onSubmit()
                          }}
                          activeOpacity={TOUCH_OPACITY}
                          disabled={!isValid}
                          style={HEADER.rightIcon}
                        >
                          <Text type='Circular' size='slight' weight={400} color={isValid ? ORANGE_BRIGHT : PALE_SALMON}>Submit</Text>
                        </TouchableOpacity>
                      }
                      {isUploading &&
                          <Image
                            spinning
                            source={require('sf-assets/icons/ic_loading.png')}
                            imageStyle={{ width: WP8, aspectRatio: 1 }}
                            style={HEADER.rightIcon}
                          />
                      }
                    </View>
                  )}
                />
                <ProgressBar
                  ref={this.progressBar}
                />
                <LinearGradient
                  colors={SHADOW_GRADIENT}
                  style={HEADER.shadow}
                />
              </View>

              {
                isSuccess && (
                  <PopUpInfo
                    closeOnBackdrop={true}
                    isVisible={isSuccess}
                    template={constants.SUCCESS_INFO}
                    onPress={this._onSuccessCallback}
                  />
                )
              }

              <View>
                <SnackBar
                  ref={this.snackBar}
                  label='Draft Audition terkirim'
                  iconName='check-bold'
                />
                <KeyboardAwareScrollView
                  keyboardDismissMode='interactive'
                  keyboardShouldPersistTaps='always'
                  extraScrollHeight={HEADER.height}
                >
                  <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{ paddingBottom: WP25, backgroundColor: NO_COLOR }}
                  >
                    <View style={{ ...styles.section, marginTop: WP6, paddingBottom: WP6 }}>
                      <InputTextLight
                        withLabel={false}
                        placeholder='Tuliskan deskripsi Audition'
                        value={formData.description}
                        onChangeText={onChange('description')}
                        placeholderTextColor={SILVER_TWO}
                        color={SHIP_GREY}
                        bordered
                        size='mini'
                        type='Circular'
                        returnKeyType={'next'}
                        wording=' '
                        maxLengthFocus
                        maxLength={250}
                        showLength
                        maxLine={8}
                        multiline
                        lineHeight={1}
                        style={{ marginTop: 0, marginBottom: 0 }}
                        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                        labelv2='Deskripsi Audition'
                        editable={!isUploading}
                      />

                      <InputTextLight
                        withLabel={false}
                        placeholder='Jelaskan syarat dan ketentuan acaramu'
                        value={formData.term_condition}
                        onChangeText={onChange('term_condition')}
                        placeholderTextColor={SILVER_TWO}
                        color={SHIP_GREY}
                        bordered
                        size='mini'
                        type='Circular'
                        returnKeyType={'next'}
                        wording=' '
                        maxLengthFocus
                        maxLength={500}
                        showLength
                        maxLine={11}
                        multiline
                        lineHeight={1}
                        style={{ marginTop: 0, marginBottom: 0 }}
                        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                        labelv2='Syarat & Ketentuan'
                        editable={!isUploading}
                      />

                    </View>

                    <View style={{ ...styles.section, borderBottomWidth: 0 }}>
                      <InputTextLight
                        withLabel={false}
                        placeholder='Masukkan link/url referensi kolaborasimu'
                        value={formData.submission_reference}
                        onChangeText={onChange('submission_reference')}
                        placeholderTextColor={SILVER_TWO}
                        color={SHIP_GREY}
                        bordered
                        size='mini'
                        type='Circular'
                        returnKeyType={'next'}
                        wording=' '
                        maxLengthFocus
                        lineHeight={1}
                        style={{ marginTop: 0, marginBottom: 0 }}
                        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                        labelv2='Informasi lainnya '
                        subLabel='(Opsional)'
                        editable={!isUploading}
                        multiline
                        maxLine={8}
                      />
                      <TouchableOpacity
                        style={{ flexDirection: 'row', alignItems: 'center' }}
                        onPress={async () => {
                          const text = await Clipboard.getString()
                          onChange('submission_reference')(text)
                        }}
                      >
                        <Icon name='link' type='MaterialIcons' color={REDDISH} size='large' style={{ marginRight: WP1 }}/>
                        <Text size='mini' color={REDDISH} weight={400} type='Circular'>Paste Link</Text>
                      </TouchableOpacity>
                    </View>
                  </ScrollView>
                </KeyboardAwareScrollView>
              </View>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateSubmissionInfoForm),
  mapFromNavigationParam
)
