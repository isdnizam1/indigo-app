import * as yup from 'yup'
import { map, startCase, toLower } from 'lodash-es'
import React from 'react'
import { BackHandler, Keyboard, Platform, ScrollView, StatusBar, TouchableOpacity, View } from 'react-native'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import * as ImageManipulator from 'expo-image-manipulator'
import { LinearGradient } from 'expo-linear-gradient'
import { CheckBox } from 'react-native-elements'
import { connect } from 'react-redux'
import { getCity, getGenreInterest, getGenreSuggestion } from 'sf-actions/api'
import Modal from 'sf-components/Modal'
import { HP10, HP2, WP1, WP105, WP2, WP205, WP3, WP305, WP308, WP4, WP5, WP6, WP8 } from 'sf-constants/Sizes'
import Image from 'sf-components/Image'
import Container from 'sf-components/Container'
import { GUN_METAL, NO_COLOR, ORANGE_BRIGHT, PALE_BLUE_TWO, PALE_LIGHT_BLUE_TWO, PALE_SALMON, PALE_WHITE, REDDISH, SHADOW_GRADIENT, SHIP_GREY, SHIP_GREY_CALM, SILVER_TWO, WHITE } from 'sf-constants/Colors'
import Form from 'sf-components/Form'
import HeaderNormal from 'sf-components/HeaderNormal'
import { HEADER, LINEAR_TYPE, TEXT_INPUT_STYLE, TOUCH_OPACITY } from 'sf-constants/Styles'
import Text from 'sf-components/Text'
import { FONTS } from 'sf-constants/Fonts'
import InputTextLight from 'sf-components/InputTextLight'
import Icon from 'sf-components/Icon'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import SelectDate from 'sf-components/SelectDate'
import SelectModalV3 from 'sf-components/SelectModalV3'
import ExitWarningPopUp from 'sf-components/popUp/ExitWarningPopUp'
import { submissionDispatcher } from 'sf-services/submission'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import MenuItem from '../create_session/component/MenuItem'
import styles from './styles'
import constants from './constants'

export const FORM_VALIDATION = yup.object().shape({
  title: yup.string().required(),
  location_name: yup.string().required(),
  startDate: yup.string().required(),
  endDate: yup.string().required(),
  image: yup.string().required(),
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  clearDataSubmission: submissionDispatcher.submissionDataClear
}

const mapFromNavigationParam = (getParam) => ({
})

class CreateSubmissionDetailForm extends React.Component {
  static navigationOptions = () => ({
    gesturesEnabled: false
  })

  constructor(props) {
    super(props)
    this.state = {
      showExitWarning: false,
      isUploading: false,
      benefits: [undefined]
    }
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _backHandler = async () => {
    this.setState({ showExitWarning: true })
  }

  _selectPhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCameraRoll === 'granted') {
      const { uri } = await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
      return await ImageManipulator.manipulateAsync(uri, [{
        resize: {
          width: 620,
          height: 620
        }
      }], {
        base64: true,
        compress: 0.9,
        format: ImageManipulator.SaveFormat.JPG
      })
    }
  }

  _takePhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
      const { uri } = await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
      return await ImageManipulator.manipulateAsync(uri, [{
        resize: {
          width: 620,
          height: 620
        }
      }], {
        base64: true,
        compress: 0.9,
        format: ImageManipulator.SaveFormat.JPG
      })
    }
  }

  _handlePhoto = async (result, onChange) => {
    if (Platform.OS === 'ios') StatusBar.setHidden(false)
    if (!result) {
      return
    }

    onChange('image')(result.base64)
    onChange('image_uri')(result.uri)
  }

  _onSubmit = async ({ formData }) => {
    const { navigateTo } = this.props
    const { title, location_name, startDate, endDate, image, genre } = formData
    const { benefits } = this.state
    const submissionDetail = {
      title, location_name, startDate, endDate, image, genre, benefits
    }
    navigateTo('CreateSubmissionInfoForm', { submissionDetail })
  }

  _onAddBenefit = () => {
    const newBenefits = [...this.state.benefits]
    newBenefits.push('')
    this.setState({ benefits: newBenefits })
  }

  _onEditBenefits = (index) => (value) => {
    const {
      benefits
    } = this.state
    benefits[index] = value
    this.setState({
      benefits
    })
  }

  _onDeleteBenefits = (index) => () => {
    const {
      benefits
    } = this.state
    benefits.splice(index, 1)
    this.setState({
      benefits
    })
  }

  _renderPoster = (image, onChange) => {
    return (
      <Modal
        renderModalContent={({ toggleModal }) => (
          <View
            style={{
              paddingVertical: WP3
            }}
          >
            <MenuItem
              onPress={() => {
                this._selectPhoto().then((result) => {
                  toggleModal()
                  this._handlePhoto(result, onChange)
                })
              }}
              label='Choose from Library'
            />
            <MenuItem
              label='Take Photo'
              onPress={() => {
                this._takePhoto().then((result) => {
                  toggleModal()
                  this._handlePhoto(result, onChange)
                })
              }}
            />

            <MenuItem
              label='Remove Current Photo'
              onPress={() => {
                onChange('image_uri')(undefined)
                onChange('image')(undefined)
                toggleModal()
              }}
            />
            <MenuItem
              type='separator'
            />
            <MenuItem
              image={require('sf-assets/icons/close.png')}
              label='Tutup'
              onPress={toggleModal}
            />
          </View>
        )}
      >
        {
          ({ toggleModal }, M) => (
            <>
              <LinearGradient
                style={styles.imageWrapper}
                colors={[PALE_BLUE_TWO, PALE_LIGHT_BLUE_TWO]}
                {...LINEAR_TYPE.vertical}
              >
                {image ? (
                  <Image style={styles.imageWrapper} imageStyle={styles.imageWrapper} source={{ uri: image }} />
                ) : (
                  <Image style={styles.imagePlaceholder} imageStyle={styles.imagePlaceholder} source={require('sf-assets/images/Logo_SF_Simple.png')} />
                )}
              </LinearGradient>
              <TouchableOpacity
                onPress={toggleModal} activeOpacity={0.8}
                style={{ flexDirection: 'row', alignItems: 'center', marginTop: WP3 }}
              >
                <Image style={styles.cameraIcon} imageStyle={styles.cameraIcon} source={require('sf-assets/icons/icCamera_Reddish.png')} />
                <Text color={REDDISH} weight={400} size='xmini'>{image ? 'Ubah Poster' : 'Upload Poster'}</Text>
              </TouchableOpacity>
              <Text color={SHIP_GREY_CALM} weight={300} size='xmini'>File size max. 10 mb (jpg, jpeg, png)</Text>
              {M}
            </>
          )
        }
      </Modal>
    )
  }

  _genreItem = (genre, index, onChange, formData) => {
    return (
      <View
        key={index}
        style={{
          marginRight: WP2,
          marginBottom: WP2,
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: WP3,
          paddingVertical: WP2,
          borderRadius: 6,
          backgroundColor: WHITE,
          borderWidth: 1,
          borderColor: PALE_BLUE_TWO
        }}
      >
        <Text size='xmini' type='Circular' weight={400} color={SHIP_GREY} style={{ marginRight: WP205 }}>{genre}</Text>
        <Icon
          onPress={() => {
            const newGenre = formData.genre
            newGenre.splice(index, 1)
            onChange('genre')(newGenre)
          }}
          centered
          size='small'
          color={PALE_BLUE_TWO}
          name={'close'}
          type='MaterialCommunityIcons'
        />
      </View>
    )
  }

  render() {
    const {
      navigateBack, clearDataSubmission
    } = this.props
    const {
      isUploading, benefits, showExitWarning
    } = this.state

    return (
      <Container colors={[PALE_WHITE, PALE_WHITE]} isAvoidingView={true}>
        <Form
          validation={FORM_VALIDATION}
          onSubmit={this._onSubmit}
          initialValue={{
            genre: []
          }}
        >
          {({ onChange, formData, onSubmit, isValid }) => (
            <View style={{ overflow: 'hidden' }}>
              <View>
                <HeaderNormal
                  iconLeftOnPress={this._backHandler}
                  iconLeftWrapperStyle={{ marginRight: WP8 }}
                  textType='Circular'
                  textColor={SHIP_GREY}
                  textSize={'slight'}
                  textWeight={400}
                  text='Buat Audition'
                  centered
                  rightComponent={(
                    <View style={{ paddingRight: WP2 }}>
                      {!isUploading &&
                        <TouchableOpacity
                          onPress={() => {
                            Keyboard.dismiss()
                            onSubmit()
                          }}
                          activeOpacity={TOUCH_OPACITY}
                          disabled={!isValid}
                          style={HEADER.rightIcon}
                        >
                          <Text type='Circular' size='slight' weight={400} color={isValid ? ORANGE_BRIGHT : PALE_SALMON}>Next</Text>
                        </TouchableOpacity>
                      }
                      {isUploading &&
                        <Image
                          spinning
                          source={require('sf-assets/icons/ic_loading.png')}
                          imageStyle={{ width: WP8, aspectRatio: 1 }}
                          style={HEADER.rightIcon}
                        />
                      }
                    </View>
                  )}
                />
                <LinearGradient
                  colors={SHADOW_GRADIENT}
                  style={HEADER.shadow}
                />
              </View>

              <ExitWarningPopUp
                isVisible={showExitWarning}
                onCancel={() => {
                  this.setState({ showExitWarning: false })
                  navigateBack()
                  clearDataSubmission()
                }}
                onContinue={() => this.setState({ showExitWarning: false })}
                template={constants.EXIT_WARNING}
              />

              <KeyboardAwareScrollView
                keyboardDismissMode='interactive'
                keyboardShouldPersistTaps='always'
                extraScrollHeight={HEADER.height}
              >
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{ paddingBottom: HP10, backgroundColor: NO_COLOR }}
                >
                  <View style={{ ...styles.section, alignItems: 'center', paddingVertical: WP6 }}>
                    {this._renderPoster(formData.image_uri, onChange)}
                  </View>

                  <View style={styles.section}>
                    <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>INFORMASI</Text>

                    <InputTextLight
                      withLabel={false}
                      placeholder='Tuliskan judul Audition'
                      value={formData.title}
                      onChangeText={onChange('title')}
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      bordered
                      size='mini'
                      type='Circular'
                      returnKeyType={'next'}
                      wording=' '
                      maxLengthFocus
                      lineHeight={1}
                      style={{ marginTop: 0, marginBottom: 0 }}
                      textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                      labelv2='Judul Audition'
                      editable={!isUploading}
                      maxLength={70}
                      showLength
                    />

                    <SelectModalV3
                      refreshOnSelect
                      triggerComponent={(
                        <InputTextLight
                          withLabel={false}
                          placeholder='Tuliskan Kota/lokasi'
                          value={formData.location_name}
                          onChangeText={onChange('location_name')}
                          placeholderTextColor={SILVER_TWO}
                          color={SHIP_GREY}
                          bordered
                          size='mini'
                          type='Circular'
                          returnKeyType={'next'}
                          wording=' '
                          maxLengthFocus
                          lineHeight={1}
                          style={{ marginTop: 0, marginBottom: 0 }}
                          textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                          labelv2='Kota/Lokasi Audition'
                          editable={false}
                        />
                      )}
                      header='Kota/Lokasi'
                      suggestion={getCity}
                      suggestionKey='city_name'
                      suggestionPathResult='city_name'
                      onChange={onChange('location_name')}
                      createNew={true}
                      reformatFromApi={(text) => startCase(toLower(text))}
                      placeholder='Coba cari Kota'
                      selection={formData.location_name}
                      showSelection
                      onRemoveSelection={() => onChange('locationName')('')}
                    />

                    <CheckBox
                      disabled={isUploading}
                      checked={formData.location_name === 'Online'}
                      containerStyle={{
                        backgroundColor: WHITE, borderWidth: 0, margin: 0,
                        paddingHorizontal: -WP2, marginBottom: WP2
                      }}
                      onPress={() => {
                        const isChecked = formData.location_name === 'Online'
                        onChange('location_name')(isChecked ? undefined : 'Online')
                      }}
                      title='Online Audition'
                      textStyle={{ fontSize: WP308, color: formData.location_name === 'Online' ? REDDISH : SHIP_GREY }}
                      fontFamily={FONTS.Circular['200']}
                      wrapperStyle={{ marginHorizontal: -WP2, marginTop: -WP3 }}
                      checkedIcon={(
                        <Image
                          imageStyle={{ width: WP5, aspectRatio: 1 }}
                          source={require('sf-assets/icons/icCheckboxActive.png')}
                        />
                      )}
                      uncheckedIcon={(
                        <Image
                          imageStyle={{ width: WP5, aspectRatio: 1 }}
                          source={require('sf-assets/icons/icCheckboxInactive.png')}
                        />
                      )}
                    />

                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 1, marginRight: WP105 }}>
                        <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
                          Tanggal dimulai
                        </Text>
                        <SelectDate
                          value={formData.startDate}
                          dateFormat='DD/MM/YYYY'
                          minimumDate={new Date()}
                          mode='date'
                          onChangeDate={onChange('startDate')}
                          style={{ marginBottom: WP4 }}
                          disabled={false}
                        >
                          <Text
                            type='Circular' size='mini'
                            style={{
                              ...TEXT_INPUT_STYLE.inputV2,
                              color: formData.startDate ? SHIP_GREY : SILVER_TWO
                            }}
                          >
                            {formData.startDate || 'Start Date'}
                          </Text>
                        </SelectDate>
                      </View>

                      <View style={{ flex: 1, marginLeft: WP105 }}>
                        <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
                        Tanggal selesai
                        </Text>
                        <SelectDate
                          value={formData.endDate}
                          dateFormat='DD/MM/YYYY'
                          minimumDate={new Date()}
                          mode='date'
                          onChangeDate={onChange('endDate')}
                          style={{ marginBottom: WP4 }}
                          disabled={false}
                        >
                          <Text
                            type='Circular' size='mini'
                            style={{
                              ...TEXT_INPUT_STYLE.inputV2,
                              color: formData.endDate ? SHIP_GREY : SILVER_TWO
                            }}
                          >
                            {formData.endDate || 'Deadline'}
                          </Text>
                        </SelectDate>
                      </View>
                    </View>
                  </View>

                  <View style={{ ...styles.section }}>
                    <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>BENEFITS</Text>

                    {
                      benefits.map((item, index) => (
                        <InputTextLight
                          key={index}
                          withLabel={false}
                          placeholder='Tuliskan deskripsi singkat tentang benefit'
                          value={item}
                          onChangeText={this._onEditBenefits(index)}
                          placeholderTextColor={SILVER_TWO}
                          color={SHIP_GREY}
                          maxLength={70}
                          showLength
                          bordered
                          size='mini'
                          type='Circular'
                          returnKeyType={'next'}
                          wording=' '
                          maxLengthFocus
                          lineHeight={1}
                          style={{ marginTop: 0, marginBottom: 0 }}
                          textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                          editable={!isUploading}
                          labelv2={`Benefit #${index + 1}`}
                          onRemoveAble={this._onDeleteBenefits(index)}
                          removeIcon={(
                            <Icon
                              style={{ marginLeft: WP305 }}
                              centered
                              size='small'
                              color={PALE_LIGHT_BLUE_TWO}
                              onPress={this._onDeleteBenefits(index)}
                              name='close' type='MaterialCommunityIcons'
                            />
                          )}
                        />
                      ))
                    }
                    <TouchableOpacity onPress={this._onAddBenefit}>
                      <Text color={REDDISH} type='Circular' weight={400} size='mini'>
                        + Tambahkan Benefit Lainnya
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <View style={{ ...styles.section, borderBottomWidth: 0 }}>
                    <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>
                      {'Genre '}
                      <Text weight={200} type='Circular' size={'xmini'} color={SILVER_TWO}>{' (Contoh: Rock)'}</Text>
                    </Text>

                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                      {
                        formData.genre && map(formData.genre, (genre, index) => (
                          this._genreItem(genre, index, onChange, formData)
                        ))
                      }
                    </View>
                    {/* button genre */}
                    <SelectModalV3
                      refreshOnSelect
                      triggerComponent={(
                        <Text color={REDDISH} type='Circular' weight={400} size='mini'>
                          + Tambahkan Genre
                        </Text>
                      )}
                      header='Genre'
                      suggestion={getGenreInterest}
                      suggestionKey='interest_name'
                      suggestionPathResult='interest_name'
                      onChange={(value) => {
                        const newGenre = formData.genre
                        if (!newGenre.includes(value)) {
                          newGenre.push(value)
                        }
                        onChange('genre')(newGenre)
                      }}
                      createNew={true}
                      placeholder='Cari Genre'
                      selection={formData.genre}
                      selectionWording='Genre Terpilih'
                      showSelection
                      onRemoveSelection={(index) => {
                        const newGenre = formData.genre
                        newGenre.splice(index, 1)
                        onChange('genre')(newGenre)
                      }}
                      quickSearchTitle={'Saran Pencarian Genre'}
                      quickSearchList={[]}
                      quickSearch={getGenreSuggestion}
                    />
                  </View>

                </ScrollView>
              </KeyboardAwareScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateSubmissionDetailForm),
  mapFromNavigationParam
)
