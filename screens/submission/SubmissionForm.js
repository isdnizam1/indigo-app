import React from 'react'
import { connect } from 'react-redux'
import { ImageBackground, TouchableOpacity, View, BackHandler } from 'react-native'
import { compact, isEmpty, map } from 'lodash-es'
import * as yup from 'yup'
import moment from 'moment'
import {
  _enhancedNavigation,
  Container,
  Image,
  Button,
  HeaderNormal,
  Menu,
  Form,
  Card,
  InputTextLight, Icon, Text, InputDate, InputModal
} from '../../components'
import { WHITE, ORANGE, GREY_CALM_SEMI, BLUE_LIGHT, GREY_DISABLE, TOMATO } from '../../constants/Colors'
import { WP6, WP100, WP2, HP105, WP10, WP40, WP105, HP1, WP3, WP1, WP30 } from '../../constants/Sizes'
import { selectPhoto, takePhoto } from '../../utils/upload'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { convertBlobToBase64, fetchAsBlob } from '../../utils/helper'
import BackModal from '../../components/BackModal'
import { getGenreInterest } from '../../actions/api'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

const validation = yup.object().shape({
  coverImageb64: yup.string().required(),
  title: yup.string().required(),
  startDate: yup.string().required(),
  endDate: yup.string().required(),
  location: yup.string().required(),
  requirements: yup.array().test('min-1', 'Minimal 1 value', (value) => !isEmpty(compact(value))),
  description: yup.string().required(),
  benefits: yup.array().test('min-1', 'Minimal 1 value', (value) => !isEmpty(compact(value))),
})

// eslint-disable-next-line no-unused-vars
export const DUMP_VALUE = {
  coverImageUri: 'https://cdn.zeplin.io/5c2c0018f8b21d20e3fa14ff/assets/39923193-2EC6-40DB-A576-D08456F1185A.png',
  title: 'Soundfren Meet Up vol.4 x Addie MS',
  startDate: '25/11/2019',
  endDate: '29/11/2019',
  location: 'Aula Simonofonia Jakarta',
  requirements: [
    'Memiliki lagu ciptaan sendiri',
    'Bila bentuk grup, maksimal 5 orang'
  ],
  description: 'Kolaborasi spesial antara Soundfren dengan musisi yang juga komposer dan pengaba dari Twilight Orchestra, yaitu Addie MS.',
  benefits: [
    'Kontrak bermain selama 1 tahun'
  ]
}

const propsDefault = {
  initialValue: {}
}

class SubmissionForm extends React.Component {
  _didFocusSubscription
  _willBlurSubscription

  constructor(props) {
    super(props)
    this._didFocusSubscription = props.navigation.addListener('focus', () =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler)
    )
  }

  state = {
    isReady: isEmpty(this.props.initialValue),
    initialValue: this.props.initialValue,
    backModal: false,
    isAllGenre: true
  }

  async componentDidMount() {
    const { initialValue } = this.props
    if (!isEmpty(initialValue)) {
      // do re-format from api or normalize the value for the form
      const coverImageb64 = await fetchAsBlob(initialValue.coverImageUri).then(
        convertBlobToBase64
      )
      this.setState({
        initialValue: {
          ...initialValue,
          coverImageb64,
          coverImageUri: initialValue.coverImageUri,
        },
        isReady: true,
      })
    }
    this._willBlurSubscription = this.props.navigation.addListener('blur', () =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
    )
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _backHandler = async () => {
    const { navigateBack } = this.props
    const { backModal } = this.state

    if (this.form && this.form.isDirtyState()) {
      this.setState({ backModal: !backModal })
    } else {
      navigateBack()
    }
  }

  _onCreateSubmission = ({ formData }) => {
    this.props.navigateTo('SubmissionPreview', { formData, isCreating: true })
  }

  _genreItem = (genre, index, onChange, formData) => {
    return (
      <View
        key={index}
      >
        <View style={{
          marginRight: WP2, marginVertical: WP1,
          paddingHorizontal: WP2, paddingVertical: WP1, borderRadius: WP100,
          justifyContent: 'space-between',
          alignSelf: 'baseline', flexDirection: 'row', borderWidth: 1, borderColor: TOMATO }}
        >
          <Text size='xtiny' style={{ marginLeft: WP2 }} color={TOMATO}>{genre}</Text>
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            onPress={() => {
              const newGenre = formData.genres
              newGenre.splice(index, 1)
              onChange('genres')(newGenre)
            }}
          >
            <Icon
              centered
              name='closecircle'
              color={TOMATO}
              size='tiny'
              style={{ marginHorizontal: WP2 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    const { navigateBack } = this.props
    const { isReady, initialValue, backModal, isAllGenre } = this.state
    return (
      <Form
        ref={(form) => this.form = form}
        validation={validation}
        initialValue={{ genres: [], requirements: [''], benefits: [''], ...initialValue }}
        onSubmit={this._onCreateSubmission}
      >
        {({ onChange, onSubmit, formData, isValid, isDirty }) => (
          <Container
            isReady={isReady}
            renderHeader={() => (
              <HeaderNormal
                iconLeftOnPress={this._backHandler}
                text='Create Submission'
              />
            )}
            isAvoidingView
            scrollable
            scrollBackgroundColor={GREY_CALM_SEMI}
            scrollContentContainerStyle={{ justifyContent: 'space-between' }}
          >
            <View>
              <ImageBackground
                source={{ uri: formData.coverImageUri }}
                style={{
                  width: WP100,
                  backgroundColor: GREY_CALM_SEMI,
                  aspectRatio: 32 / 13,
                }}
              >
                <Menu
                  options={[
                    {
                      onPress: selectPhoto(onChange, 'coverImage', [32, 13]),
                      name: 'Select from library',
                      iconName: 'photo-library',
                    },
                    {
                      onPress: takePhoto(onChange, 'coverImage', [32, 13]),
                      name: 'Take a picture',
                      iconName: 'camera',
                    },
                  ]}
                  triggerComponent={(toggleModal) => (
                    <Image
                      onPress={toggleModal}
                      size='tiny'
                      source={require('../../assets/icons/elementAddPhotoBanner.png')}
                      style={{
                        aspectRatio: 23 / 21,
                        bottom: WP6,
                        right: WP6,
                        position: 'absolute',
                      }}
                    />
                  )}
                />
              </ImageBackground>
              <Card first>
                <InputTextLight
                  withLabel={false}
                  maxLength={40}
                  placeholder='Submission Title'
                  value={formData.title}
                  onChangeText={onChange('title')}
                  wording='Title'
                  required
                />
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1, marginRight: WP10 }}>
                    <InputDate
                      required
                      value={formData.startDate}
                      dateFormat='DD/MM/YYYY'
                      mode='date'
                      wording='Date of Submission'
                      minimumDate={moment().format('DD/MM/YYYY')}
                      maximumDate={formData.maxStartDate}
                      placeholder='Start Date'
                      onChangeText={(text) => {
                        onChange('startDate')(text)
                        onChange('minEndDate')(text)
                      }}
                    />
                  </View>
                  <View style={{ flex: 1, marginLeft: WP10 }}>
                    <InputDate
                      required
                      value={formData.endDate}
                      dateFormat='DD/MM/YYYY'
                      mode='date'
                      wording='Deadline'
                      minimumDate={formData.minEndDate}
                      placeholder='End Date'
                      onChangeText={(text) => {
                        onChange('endDate')(text)
                        onChange('maxStartDate')(text)
                      }}
                    />
                  </View>
                </View>
                <InputTextLight
                  required
                  withLabel={false}
                  wording='Location of Event'
                  placeholder='Studio, Cafe, or another cool place'
                  value={formData.location}
                  onChangeText={onChange('location')}
                />
              </Card>
              <Card>
                <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ isAllGenre: true })
                      onChange('genres')([])
                    }}
                    activeOpacity={TOUCH_OPACITY}
                    style={{ width: WP40, backgroundColor: isAllGenre ? BLUE_LIGHT: WHITE, borderTopLeftRadius: HP1, borderBottomLeftRadius: HP1, flexGrow: 1, padding: WP105, borderWidth: 1, borderColor: BLUE_LIGHT }}
                  >
                    <Text
                      type='NeoSans'
                      size='mini'
                      weight={500}
                      color={isAllGenre ? WHITE: BLUE_LIGHT}
                      centered
                    >All Genre</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ isAllGenre: false })
                    }}
                    activeOpacity={TOUCH_OPACITY}
                    style={{ width: WP40, backgroundColor: !isAllGenre ? BLUE_LIGHT : WHITE, borderTopRightRadius: HP1, borderBottomRightRadius: HP1, flexGrow: 1, padding: WP105, borderWidth: 1, borderColor: BLUE_LIGHT }}
                  >
                    <Text
                      type='NeoSans'
                      size='mini'
                      weight={500}
                      color={!isAllGenre ? WHITE : BLUE_LIGHT}
                      centered
                    >Specific Genre</Text>
                  </TouchableOpacity>
                </View>
                <Text size='xtiny' style={{ marginTop: HP1 }} color={GREY_DISABLE}>Music Preferences for this Submission</Text>
                { !isAllGenre && <View>
                  <View
                    style={{ flexDirection: 'row', marginTop: HP105 }}
                  >
                    <InputModal
                      triggerComponent={(
                        <Button
                          style={{
                            alignSelf: 'flex-start', marginTop: HP1
                          }}
                          marginless
                          contentStyle={{ width: WP30, paddingHorizontal: WP1, paddingLeft: WP3, justifyContent: 'space-between' }}
                          iconName='add-circle-outline'
                          iconType='MaterialIcons'
                          iconSize='mini'
                          iconPosition='right'
                          iconColor={WHITE}
                          compact='center'
                          rounded
                          backgroundColor={BLUE_LIGHT}
                          shadow='none'
                          text='Genre'
                          textColor={WHITE}
                          textType='NeoSans'
                          textSize='xtiny'
                          textWeight={500}
                        />
                      )}
                      header='Select Genre'
                      suggestion={getGenreInterest}
                      suggestionKey='interest_name'
                      suggestionPathResult='interest_name'
                      onChange={(value) => {
                        const newGenre = formData.genres
                        if (!newGenre.includes(value)) {
                          newGenre.push(value)
                        }
                        onChange('genres')(newGenre)
                      }}
                      selected=''
                      createNew={true}
                      placeholder='Search genre here...'
                      refreshOnSelect={true}
                    />
                  </View>
                </View>
                }
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: HP105 }}>
                  {
                    !isAllGenre && !formData.genres &&
                    <View style={{ alignSelf: 'baseline', flexDirection: 'row', borderWidth: 1, borderColor: GREY_DISABLE, borderRadius: WP100 }}>
                      <Text
                        type='NeoSans'
                        size='xtiny'
                        style={{ padding: WP1, paddingHorizontal: WP3 }}
                        color={GREY_DISABLE}
                      >ex: Jazz</Text>
                    </View>
                  }
                  {
                    !isAllGenre && formData.genres &&
                    map(formData.genres, (genre, index) => (
                      this._genreItem(genre, index, onChange, formData)
                    ))
                  }
                </View>
              </Card>
              <Card>
                {map(formData.requirements, (requirement, index) => (
                  <InputTextLight
                    withLabel={false}
                    maxLength={40}
                    wording={`Requirements ${index + 1}`}
                    placeholder='Memiliki lagu ciptaan sendiri'
                    value={formData.requirements[index]}
                    required={index === 0}
                    onRemoveAble={
                      index === 0 && formData.requirements.length === 1
                        ? null
                        : () => {
                          const { requirements } = formData
                          requirements.splice(index, 1)
                          onChange('requirements', true)(requirements, true)
                        }
                    }
                    onChangeText={onChange('requirements', true, index)}
                  />
                ))}
                <View
                  activeOpacity={TOUCH_OPACITY}
                  style={{ flexDirection: 'row', marginTop: HP105 }}
                >
                  <Button
                    style={{
                      alignSelf: 'flex-start', marginTop: HP1
                    }}
                    onPress={() => {
                      onChange('requirements', true)('')
                    }}
                    marginless
                    contentStyle={{ width: WP30, paddingHorizontal: WP1, paddingLeft: WP3, justifyContent: 'space-between' }}
                    iconName='add-circle-outline'
                    iconType='MaterialIcons'
                    iconSize='mini'
                    iconPosition='right'
                    iconColor={WHITE}
                    compact='center'
                    rounded
                    backgroundColor={BLUE_LIGHT}
                    shadow='none'
                    text='Requirement'
                    textColor={WHITE}
                    textType='NeoSans'
                    textSize='xtiny'
                    textWeight={500}
                  />
                </View>
              </Card>
              <Card>
                {map(formData.benefits, (benefit, index) => (
                  <InputTextLight
                    withLabel={false}
                    maxLength={40}
                    wording={`Benefits ${index + 1}`}
                    placeholder='Kontrak bermain selama satu tahun'
                    value={formData.benefits[index]}
                    required={index === 0}
                    onRemoveAble={
                      index === 0 && formData.benefits.length === 1
                        ? null
                        : () => {
                          const { benefits } = formData
                          benefits.splice(index, 1)
                          onChange('benefits', true)(benefits, true)
                        }
                    }
                    onChangeText={onChange('benefits', true, index)}
                  />
                ))}
                <View
                  style={{ flexDirection: 'row', marginTop: HP105 }}
                >
                  <Button
                    style={{
                      alignSelf: 'flex-start', marginTop: HP1
                    }}
                    onPress={() => {
                      onChange('benefits', true)('')
                    }}
                    marginless
                    contentStyle={{ width: WP30, paddingHorizontal: WP1, paddingLeft: WP3, justifyContent: 'space-between' }}
                    iconName='add-circle-outline'
                    iconType='MaterialIcons'
                    iconSize='mini'
                    iconPosition='right'
                    iconColor={WHITE}
                    compact='center'
                    rounded
                    backgroundColor={BLUE_LIGHT}
                    shadow='none'
                    text='Benefit'
                    textColor={WHITE}
                    textType='NeoSans'
                    textSize='xtiny'
                    textWeight={500}
                  />
                </View>
              </Card>
              <Card>
                <InputTextLight
                  required
                  withLabel={false}
                  bordered
                  size='tiny'
                  maxLength={300}
                  multiline
                  wording='Description of your Submission'
                  placeholder='This Submission give you the best oppourtunites..'
                  value={formData.description}
                  onChangeText={onChange('description')}
                />
              </Card>
            </View>
            <Button
              disable={!isValid || !isDirty}
              bottomButton
              marginless
              onPress={onSubmit}
              backgroundColor={ORANGE}
              centered
              shadow='none'
              textType='NeoSans'
              textSize='small'
              textColor={WHITE}
              textWeight={500}
              text='Create'
            />
            <View>
              <BackModal
                modalVisible={backModal}
                navigateBack={navigateBack}
                dismiss={() => this.setState({ backModal: false })}
              />
            </View>
          </Container>
        )}
      </Form>
    )
  }
}

SubmissionForm.defaultProps = propsDefault
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SubmissionForm),
  mapFromNavigationParam
)
