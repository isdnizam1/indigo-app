import { WP1, WP25, WP3, WP4, WP40, WP6 } from '../../constants/Sizes'
import { PALE_BLUE } from '../../constants/Colors'

export default {
  section: {
    paddingHorizontal: WP4,
    paddingVertical: WP3,
    marginBottom: WP4,
    borderBottomColor: PALE_BLUE,
    borderBottomWidth: 1
  },
  imageWrapper: {
    width: WP40,
    height: WP40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10
  },
  imagePlaceholder: {
    width: WP25,
  },
  cameraIcon: {
    width: WP6,
    aspectRatio: 1,
    marginRight: WP1
  }
}
