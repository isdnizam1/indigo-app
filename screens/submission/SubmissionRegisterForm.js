import React, { Component } from 'react'
import { View, BackHandler, Keyboard, TouchableOpacity, 
  TextInput,
  Picker,
  StyleSheet,

  Animated } from 'react-native'
import { connect } from 'react-redux'
import { compact, isEmpty, remove } from 'lodash-es'
import { map, noop, reduce } from 'lodash-es'
import * as yup from 'yup'
import { setClearBlueMessage } from 'sf-services/messagebar/actionDispatcher'
import { WP405 } from 'sf-constants/Sizes'
import SelectModalV4 from 'sf-components/SelectModalV4'
import style from 'sf-styles/register'
import Label from '../../components/Label'

import {
  _enhancedNavigation,
  Container,
  Card,
  Icon,
  Text,
  Image,
  InputTextLight,
  Button,
  Modal,
  HeaderNormal,
  SelectDate,
  Form,
  ModalMessageView,
} from '../../components'
import {
  GREY_WARM,
  NO_COLOR,
  ORANGE,
  WHITE,
  PALE_GREY,
  REDDISH,
  ORANGE_BRIGHT,
  PALE_SALMON,
  SHIP_GREY,
  PALE_WHITE,
  GUN_METAL,
  SILVER_TWO,
  SHIP_GREY_CALM,
  PALE_BLUE_TWO,
  PALE_LIGHT_BLUE_TWO,
  PALE_GREY_TWO,
  BLUE10
} from '../../constants/Colors'
import {
  HP1,
  WP100,
  WP2,
  WP3,
  WP4,
  WP40,
  WP15,
  WP1,
  WP105,
  WP05,
  WP5,
  HP2,
  WP10,
  WP205,
  HP5,
  WP8,
  WP305,
} from '../../constants/Sizes'
import { getAdsDetail, submitSubmission } from '../../actions/api'
import { daysRemaining } from '../../utils/date'
import { jobDispatcher } from '../../services/job'
import { SHADOW_STYLE, TEXT_INPUT_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { isIOS, isPremiumUser, getImageSong } from '../../utils/helper'
import { paymentDispatcher } from '../../services/payment'
import HorizontalLine from '../../components/HorizontalLine'

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: '100%',
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 6,
  },
})

const mapStateToProps = ({ auth, job }) => ({
  userData: auth.user,
  jobRegistrationForm: job.jobRegistrationForm,
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  idAds: getParam('idAds', 0),
  adsDetail: getParam('ads', 0),
})

const mapDispatchToProps = {
  jobRegistrationSave: jobDispatcher.jobRegistrationSave,
  jobRegistrationClear: jobDispatcher.jobRegistrationClear,
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
  setClearBlueMessage,
}

const RESPONSE_TYPES = {
  success: {
    title: 'Hooray!',
    message: 'Your registration form has been submitted',
    image: require('../../assets/images/successSubmision.png'),
    imageRatio: 238 / 134.2,
    btnText: 'Back to explore',
  },
  failed: {
    title: 'Registration Failed',
    message: 'It may you make a mistakes somewhere',
    image: require('../../assets/images/submission.png'),
    imageRatio: 80 / 82,
    btnText: 'OK !',
  },
  not_premium: {
    title: 'Ooops',
    message: 'This Audition is only available for premium user. Let’s be our premium member!',
    image: require('../../assets/images/notPremiumUser.png'),
    imageRatio: 182 / 108,
    btnText: 'Join Premium',
  },
}

const validation = yup.object().shape({
  bandName: yup.string().required(),
  phoneNumber: yup.string().required(),
  works: yup.array().test('min-1', 'Minimal 1 value', (value) => !isEmpty(compact(value))),
  social_media: yup.array().test('min-1', 'Minimal 1 value', (value) => !isEmpty(compact(value))),
})

const ButtonIcon = ({
  onPress = () => {},
  title,
  iconName,
  iconType = 'MaterialCommunityIcons',
  iconPosition = 'left',
  textColor = REDDISH,
  iconColor = REDDISH,
}) => (
  <TouchableOpacity
    onPress={onPress}
    activeOpacity={TOUCH_OPACITY}
    style={{ flexDirection: 'row', alignItems: 'center' }}
  >
    {iconPosition == 'right' ? (
      <Text
        type='Circular'
        size='xmini'
        weight={400}
        color={textColor}
        style={{ marginRight: WP105 }}
      >
        {title}
      </Text>
    ) : null}
    <Icon centered size='small' color={iconColor} name={iconName} type={iconType} />
    {iconPosition == 'left' ? (
      <Text
        type='Circular'
        size='xmini'
        weight={400}
        color={textColor}
        style={{ marginLeft: WP105 }}
      >
        {title}
      </Text>
    ) : null}
  </TouchableOpacity>
)

class SubmissionRegisterForm extends Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props)
    this._didFocusSubscription = props.navigation.addListener('focus', () =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler),
    )
  }
  state = {
    ads: { additional_data: { date: {}, location: {} } },
    isReady: false,
    backModal: false,
    percentCompleted: 0,
    isUpdate: false,
    user_submission : false,
    phone :false,
    editableForm : true,
    disableForm : false
  };

  async componentDidMount() {
    await this._getInitialScreenData()
    this._willBlurSubscription = this.props.navigation.addListener('blur', () =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler),
    )
  }
  _get_key_name(formData,key){
    var cc = 'formData.'+key;
      return cc;
  }
  _getInitialScreenData = async (...args) => {
    await this._getAdsDetail(...args)
    await this.setState({
      isReady: true,
    })
  };
  _mapUserSubmission = (userSubmission) => {
    return {
      bandName: userSubmission.band_name,
      phoneNumber: userSubmission.phone,
      social_media: JSON.parse(userSubmission.social_media),
      works: userSubmission.journey,
    }
  };
  _onSubmissionRegister = async ({ formData }) => {
    const {
      userData: { id_user },
      idAds,
      dispatch,
    } = this.props
    const { ads } = this.state
    const additionalData = ads.additional_data
    formData.id_user = id_user;
    formData.id_ads = idAds;
    // formData.full_name = formData.bandName;
    // formData.phone = formData.phoneNumber;
    delete formData.isDirty;
    delete formData.isValid;
    delete formData.errors;
    delete formData.avoidScreenUpdate;
    return await dispatch(submitSubmission, formData, null, true)
  };

  _getAdsDetail = async (isLoading = true) => {
    const {
      dispatch,
      jobRegistrationSave,
      userData: { id_user },
      idAds: id_ads,
    } = this.props
    const dataResponse = await dispatch(getAdsDetail, { id_user, id_ads }, noop, false, isLoading)
    let isUpdate = false

    if (dataResponse.user_joined) {
      var user_submission = JSON.parse(dataResponse.user_submission.additional_data);
      var phone = dataResponse.user_submission.phone;
      // jobRegistrationSave(this._mapUserSubmission(dataResponse.user_submission))
      isUpdate = true
      var editableForm = false
      var disableForm = true
    } else {
      var user_submission = false;
      var phone = false;
      var editableForm = true
      var disableForm = false
      // jobRegistrationSave({ social_media: [''] })
    }
    this.setState({
      ads: { ...dataResponse, additional_data: JSON.parse(dataResponse.additional_data) },
      isUpdate,
      user_submission :  user_submission,
      phone :  phone,
      editableForm :  editableForm,
      disableForm :  disableForm,
    })

  };

  _backHandler = async () => {
    const { navigateBack } = this.props
    const { backModal } = this.state

    if (this.form && this.form.isDirtyState()) {
      this.setState({ backModal: !backModal })
    } else {
      navigateBack()
    }
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
    this.props.jobRegistrationClear()
  }

  _renderTop = (ads, additionalData) => (
    <>
      <Image
        tint={'black'}
        imageStyle={{ width: WP100, height: undefined }}
        aspectRatio={360 / 168}
        source={require('sf-assets/images/bgSubmission.png')}
      />
      <Card
        style={[
          {
            borderRadius: 5,
            padding: WP4,
            marginBottom: HP2,
            marginTop: -(WP100 / (360 / 168)) / 2 + HP2,
          },
          SHADOW_STYLE['shadowThin'],
        ]}
      >
        <View
          style={{
            flexDirection: 'row',
          }}
        >
          <Image
            source={{ uri: ads.image }}
            imageStyle={{ borderRadius: 12, width: WP15 }}
            style={{ width: WP15 }}
          />
          <View style={{ marginLeft: WP3, flex: 1, justifyContent: 'center' }}>
            <Text numberOfLines={2} weight={500} type='Circular' color={GUN_METAL} size='small'>
              {ads.title}
            </Text>
            {!!Number(ads.is_premium) && !isIOS() && (
              <Button
                rounded
                colors={[ORANGE, ORANGE]}
                compact='center'
                style={{ alignItems: 'flex-start' }}
                contentStyle={{ paddingHorizontal: WP105 }}
                contentSt
                marginless
                toggle
                toggleActive={false}
                toggleInactiveTextColor={ORANGE}
                text='Audition for user premium'
                textSize='xtiny'
                shadow='none'
                textWeight={300}
              />
            )}
            <View style={{ marginTop: HP1, flexDirection: 'row' }}>
              <Icon name='calendar' color={SHIP_GREY_CALM} />
              <Text
                type='Circular'
                style={{ marginBottom: WP1 }}
                color={GREY_WARM}
                size='tiny'
              >{` ${
                daysRemaining(additionalData.date.end) == 1
                  ? 'Hari Terakhir'
                  : `${daysRemaining(additionalData.date.end)} hari lagi`
              }  ·  ${additionalData.location.name}`}</Text>
            </View>
          </View>
        </View>
        {ads.is_premium === '1' && (
          <>
            <HorizontalLine color={PALE_GREY} style={{ marginVertical: HP1, marginLeft: -WP4 }} />
            <View style={{ flexDirection: 'row' }}>
              <View style={{ backgroundColor: PALE_GREY, padding: WP1, borderRadius: WP105 }}>
                <Text weight={400} type='Circular' color={SHIP_GREY_CALM} size='mini'>
                  {' '}
                  Premium User{' '}
                </Text>
              </View>
            </View>
          </>
        )}
      </Card>
    </>
  );

  _renderProfile = ({ onChange, formData }) => (
    <>
      <Text
        type='Circular'
        color={GUN_METAL}
        weight={500}
        size='xmini'
        style={{ marginBottom: HP1 }}
      >
        PROFIL
      </Text>
      <InputTextLight
        bordered
        size='mini'
        editable={this.state.editableForm}
        returnKeyType={'next'}
        placeholder='Masukan nama anda'
        placeholderTextColor={SILVER_TWO}
        color={SHIP_GREY}
        value={this.state.user_submission.full_name ? this.state.user_submission.full_name  : formData.full_name ? formData.full_name : this.props.userData.full_name?this.props.userData.full_name:null}
        onChangeText={onChange('full_name')}
        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
        labelv2='Nama'
        lineHeight={1}
        multiline
      />
      <InputTextLight
        bordered
        size='mini'
        returnKeyType={'done'}
        keyboardType={'numeric'}
        editable={this.state.editableForm}
        placeholder='Tuliskan nomor Whatsapp'
        placeholderTextColor={SILVER_TWO}
        color={SHIP_GREY}
        value={this.state.phone ? this.state.phone  : formData.phone ? formData.phone : this.props.userData.phone?this.props.userData.phone:null}
        onChangeText={onChange('phone')}
        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
        labelv2='No. HP'
        subLabel='(Whatsapp)'
        lineHeight={1}
        multiline
      />
    </>
  );

  _renderForm = ({ item,index,onChange,formData }) => (
    <>
    {item.type == 'text' ?
      <InputTextLight
          key={`${index}`}
          bordered
          size='mini'
          editable={this.state.editableForm}
          returnKeyType={'next'}
          placeholder={item.title}
          placeholderTextColor={SILVER_TWO}
          color={SHIP_GREY}
          value={this.state.user_submission[item.form_name] ? this.state.user_submission[item.form_name] :item }
          onChangeText={onChange(item.form_name)}
          textInputStyle={TEXT_INPUT_STYLE['inputV2']}
          labelv2={item.title}
          lineHeight={1}
        />
    : item.type == 'longText' ?
        <InputTextLight
        key={`${index}`}
        bordered
        size='mini'
        editable={this.state.editableForm}
        returnKeyType={'next'}
        placeholder={item.title}
        placeholderTextColor={SILVER_TWO}
        color={SHIP_GREY}
        multiline
        value={this.state.user_submission[item.form_name] ? this.state.user_submission[item.form_name] :item }
        maxLine={10}
        lineHeight={2}
        onChangeText={onChange(item.form_name)}
        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
        labelv2={item.title}
        lineHeight={1}
        multiline
      />
    : item.type == 'number' ?
    <InputTextLight
    key={`${index}`}
    bordered
    size='mini'
    returnKeyType={'done'}
    editable={this.state.editableForm}
    keyboardType={'numeric'}
    placeholder={item.title}
    placeholderTextColor={SILVER_TWO}
    color={SHIP_GREY}
    multiline
    value={this.state.user_submission[item.form_name] ? this.state.user_submission[item.form_name] :item }
    maxLine={10}
    lineHeight={2}
    onChangeText={onChange(item.form_name)}
    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
    labelv2={item.title}
    lineHeight={1}
    multiline
  />
  : item.type == 'date' ?
  <View>
<Label title={item.title} style={{marginBottom:10}} />
<SelectDate
    disabled={this.state.disableForm}
    value={this.state.user_submission[item.form_name] ? this.state.user_submission[item.form_name] :null }
    dateFormat='YYYY-MM-DD'
    mode='date'
    // minimumDate={new Date()}
    onChangeDate={onChange(item.form_name)}
    // style={{ marginBottom: WP4 }}
    // disabled={isUploading}
  />
</View>

  : item.type == 'dropdown' ?
<View>
<Label title={item.title} />
<SelectModalV4
disabled={this.state.disableForm}
refreshOnSelect
triggerComponent={
  <View style={[styles.input, { marginBottom: WP4 },  { backgroundColor: '#fffff' }]}>
    <Text
      type='Circular'
      size='xmini'
      weight={300}
      // color={SILVER_TWO }
    >
      {this.state.user_submission[item.form_name] ? this.state.user_submission[item.form_name] 
      : formData[item.form_name]}
    </Text>
  </View>
  }
  header={item.form_name} 
  items={item.data}
  value={this.state.user_submission[item.form_name] ? this.state.user_submission[item.form_name] :item }
  onChange={onChange(item.form_name)}
  reformatFromApi={(text) => startCase(toLower(text))}
  placeholder={item}
  />
  </View>
  : null }
    </>
  );
  _renderSocialMedia = ({ ads,onChange, formData }) => (
    <>
      <Text
        type='Circular'
        color={GUN_METAL}
        weight={500}
        size='xmini'
        style={{ marginBottom: HP1 }}
      >
        SUBMISSION FORM
      </Text>
      {ads.additional_data.submission_form?.map((item, index) => (
        this._renderForm({ item,index,onChange,formData })
      ))}
    </>
  );
  render() {
    const {
      isLoading,
      navigateTo,
      jobRegistrationForm,
      jobRegistrationSave,
      userData,
      navigateBack,
      id,
      paymentSourceSet,
      adsDetail
    } = this.props
    const { ads, isReady, backModal, isUpdate,user_submission } = this.state
    const additionalData = ads.additional_data;
    return (
      <Form
        ref={(form) => (this.form = form)}
        // validation={validation}
        // initialValue={{ ...jobRegistrationForm }}
        onSubmit={this._onSubmissionRegister}
      >
        {({ onChange, onSubmit, formData, isValid }) => (
          <Container
            isLoading={isLoading}
            isReady={isReady}
            renderHeader={() => (
              <HeaderNormal
                iconLeftOnPress={this._backHandler}
                textType='Circular'
                textColor={SHIP_GREY}
                textWeight={400}
                textSize={'slight'}
                text={id ? 'Ubah Submission' : 'Submit Submission'}
                iconLeftWrapperStyle={{ marginRight: WP10 }}
                centered
                rightComponent={
                  <TouchableOpacity
                    onPress={async () => {
                      Keyboard.dismiss()
                      if (
                        isIOS() ||
                        (isPremiumUser(userData.account_type) && !!Number(ads.is_premium)) ||
                        !Number(ads.is_premium)
                      ) {
                        this.setState({ percentCompleted: 20 })
                        const response = await onSubmit()
                        if (response.code === 200) this.setState({ percentCompleted: 70 })
                        this.setState({ percentCompleted: 100 })
                        this.props.setClearBlueMessage('Pendaftaran berhasil terkirim')
                        this.props.popToTop()
                        this.props.navigateTo(
                          'NotificationScreen',
                          { defaultTab: 'activities', review: true },
                          'push',
                        )
                      } else {
                        jobRegistrationSave(formData)
                        this.setState({ percentCompleted: 0 })
                      }
                    }}
                    style={{ paddingHorizontal: WP405 }}
                    activeOpacity={TOUCH_OPACITY}
                    disabled={isValid}
                  >
                    { !user_submission  ?
                    <Text size='slight' weight={400} color={isValid ? BLUE10 : REDDISH}>
                      Submit
                    </Text>
                    : null }
                  </TouchableOpacity>
                }
              >
                <View style={{ width: WP100, height: WP05, backgroundColor: PALE_GREY }}>
                  <Animated.View
                    style={{
                      height: WP05,
                      backgroundColor: REDDISH,
                      width: WP100 * (this.state.percentCompleted / 100),
                    }}
                  />
                </View>
              </HeaderNormal>
            )}
            isAvoidingView
            scrollable
            scrollBackgroundColor={PALE_WHITE}
            outsideScrollContent={() => (
              <Modal
                closeOnBackdrop
                position='bottom'
                style={{
                  backgroundColor: NO_COLOR,
                }}
                modalStyle={{ margin: 0 }}
                renderModalContent={({ toggleModal, payload: { responseType } }) => {
                  const currentWording = RESPONSE_TYPES[responseType]
                  return currentWording ? (
                    <View
                      style={{
                        backgroundColor: PALE_WHITE,
                        height: '100%',
                        width: WP100,
                        padding: WP4,
                      }}
                    >
                      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                          source={currentWording.image}
                          style={{ marginBottom: WP15 }}
                          imageStyle={{ height: WP40 }}
                          aspectRatio={currentWording.imageRatio}
                        />
                        <Text centered type='NeoSans' size='massive' weight={500}>
                          {currentWording.title}
                        </Text>
                        <Text
                          centered
                          size='mini'
                          color={GREY_WARM}
                          style={{ marginVertical: WP3 }}
                        >
                          {currentWording.message}
                        </Text>
                      </View>
                      <Button
                        onPress={() => {
                          toggleModal()
                          if (responseType === 'not_premium') {
                            jobRegistrationSave(formData)
                            paymentSourceSet('submission')
                            navigateTo('MembershipScreen')
                          } else {
                            this.props.jobRegistrationClear()
                            this.props.popToTop()
                            navigateTo('NotificationStack')
                          }
                        }}
                        backgroundColor={ORANGE}
                        centered
                        bottomButton
                        radius={8}
                        shadow='none'
                        textType='NeoSans'
                        textSize='small'
                        textColor={WHITE}
                        textWeight={500}
                        text={currentWording.btnText}
                      />
                    </View>
                  ) : null
                }}
              >
                {({ toggleModal }, M) => (
                  <View
                    style={{
                      paddingHorizontal: WP4,
                    }}
                  >
                    {M}
                  </View>
                )}
              </Modal>
            )}
          >
            <View style={{ paddingHorizontal: WP5, paddingBottom: HP5 }}>
              <ModalMessageView
                style={{ width: WP100 - WP8 }}
                contentStyle={{ paddingHorizontal: WP4 }}
                toggleModal={() => this.setState({ backModal: false })}
                isVisible={backModal}
                title={isUpdate ? 'Batalkan Perubahan?' : 'Join Audition'}
                titleType='Circular'
                titleSize={'small'}
                titleColor={GUN_METAL}
                titleStyle={{ marginBottom: WP4 }}
                subtitle={
                  isUpdate
                    ? 'Informasi pendaftaran Submission belum dilakukan. \nBatalkan perubahan?'
                    : 'Proses pendaftaran kamu belum selesai. \nLanjutkan pendaftaran?'
                }
                subtitleType='Circular'
                subtitleSize={'xmini'}
                subtitleWeight={400}
                subtitleColor={SHIP_GREY_CALM}
                subtitleStyle={{ marginBottom: WP3 }}
                image={null}
                buttonPrimaryText={isUpdate ? 'Batalkan' : 'Lanjutkan'}
                buttonPrimaryContentStyle={{ borderRadius: 8, paddingVertical: WP305 }}
                buttonPrimaryTextType='Circular'
                buttonPrimaryTextWeight={400}
                buttonPrimaryBgColor={REDDISH}
                buttonPrimaryAction={() => {
                  if (isUpdate) {
                    navigateBack()
                  } else {
                    this.setState({ backModal: false })
                  }
                }}
                buttonSecondaryText={isUpdate ? 'Lanjutkan Ubah informasi' : 'Batalkan'}
                buttonSecondaryStyle={{
                  backgroundColor: PALE_GREY,
                  marginTop: WP1,
                  borderRadius: 8,
                  paddingVertical: WP305,
                }}
                buttonSecondaryTextType='Circular'
                buttonSecondaryTextWeight={400}
                buttonSecondaryTextColor={SHIP_GREY_CALM}
                buttonSecondaryAction={() => {
                  if (isUpdate) {
                    this.setState({ backModal: false })
                  } else {
                    navigateBack()
                  }
                }}
              />
              {this._renderTop(ads, additionalData)}
              <HorizontalLine style={{ marginLeft: -WP5, marginVertical: HP2 }} />
              {this._renderProfile({ ads, additionalData, onChange, formData })}
              <HorizontalLine style={{ marginLeft: -WP5, marginVertical: HP2 }} />
              {this._renderSocialMedia({ ads, onChange, formData })}
            </View>
          </Container>
        )}
      </Form>
    )
  }
}

SubmissionRegisterForm.navigationOptions = () => ({
  gesturesEnabled: false,
})
SubmissionRegisterForm.defaultProps = {
  userData: {},
  isLoading: false,
  onRefresh: noop,
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SubmissionRegisterForm),
  mapFromNavigationParam,
)
