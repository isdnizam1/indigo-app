import React from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import { _enhancedNavigation, Button, Container, HeaderNormal, Image, Text } from '../../components'
import { authDispatcher } from '../../services/auth'
import { GET_USER_DETAIL } from '../../services/auth/actionTypes'
import { BLACK, GREY_WARM, ORANGE, WHITE } from '../../constants/Colors'
import { HP1, HP2, WP6, WP70 } from '../../constants/Sizes'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setUserDetail: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class Submission extends React.Component {
  state = {
    isReady: true,
    isRefreshing: false
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  render() {
    const {
      navigateTo
    } = this.props
    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._backHandler}
            text='Create Submission'
            centered
          />
        )}
      >
        <Image
          source={require('../../assets/images/elementImagesIllustration.png')}
          style={{ flexGrow: .5 }}
          imageStyle={{ width: WP70 }}
          aspectRatio={242 / 124}
        />
        <View style={{ paddingHorizontal: WP6, flexGrow: .5 }}>
          <Text color={BLACK} weight={500} type='NeoSans' size='extraMassive' style={{ marginVertical: HP2 }}>
            Hi There!
          </Text>
          <View style={{ marginVertical: HP1 }}>
            <Text color={GREY_WARM} size='xmini' style={{ marginBottom: HP2 }}>
              Welcome to our Submission Post! We’re excited to help you to find the best and match talents for your activity or events.
            </Text>
            <Text color={GREY_WARM} size='xmini'>
              So what are you waiting for? Click the button below to start your journey with us!
            </Text>
          </View>
        </View>
        <Button
          bottomButton
          marginless
          onPress={() => navigateTo('CreateSubmissionDetailForm')}
          backgroundColor={ORANGE}
          centered
          shadow='none'
          textType='NeoSans'
          textSize='small'
          textColor={WHITE}
          textWeight={500}
          text='Let’s get Started'
        />
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(Submission),
  mapFromNavigationParam
)
