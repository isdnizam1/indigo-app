import React, { Component } from 'react'
import {
  View,
  BackHandler,
  Keyboard,
  TouchableOpacity,
  Animated,
  StyleSheet,
} from 'react-native'
import { connect } from 'react-redux'
import { compact, isEmpty, remove, startCase, toLower } from 'lodash-es'
import { map, noop, reduce } from 'lodash-es'
import * as yup from 'yup'
import { setClearBlueMessage } from 'sf-services/messagebar/actionDispatcher'
import { WP405 } from 'sf-constants/Sizes'
import {
  _enhancedNavigation,
  Container,
  Card,
  Icon,
  Text,
  Image,
  InputTextLight,
  Button,
  Modal,
  HeaderNormal,
  Form,
  ModalMessageView,
  SelectModalV3,
  InputModal,
} from '../../components'
import {
  GREY_WARM,
  NO_COLOR,
  ORANGE,
  WHITE,
  PALE_GREY,
  REDDISH,
  ORANGE_BRIGHT,
  PALE_SALMON,
  SHIP_GREY,
  PALE_WHITE,
  GUN_METAL,
  SILVER_TWO,
  SHIP_GREY_CALM,
  PALE_LIGHT_BLUE_TWO,
  PALE_GREY_TWO,
  PALE_BLUE_TWO,
  TRANSPARENT,
} from '../../constants/Colors'
import {
  HP1,
  WP100,
  WP2,
  WP3,
  WP4,
  WP40,
  WP15,
  WP1,
  WP105,
  WP05,
  WP5,
  HP2,
  WP10,
  WP205,
  HP5,
  WP8,
  WP305,
  WP85,
  WP43,
  HP105,
} from '../../constants/Sizes'
import { getAdsDetail, getCityV3, getProvince, postStartupSubmission, getStartupCategory, getJobStartUp, getAdvancedSearch, getPeople, getStartupUser } from '../../actions/api'
import { daysRemaining } from '../../utils/date'
import { jobDispatcher } from '../../services/job'
import {
  SHADOW_STYLE,
  TEXT_INPUT_STYLE,
  TOUCH_OPACITY,
} from '../../constants/Styles'
import { isIOS, isPremiumUser, getImageSong } from '../../utils/helper'
import { paymentDispatcher } from '../../services/payment'
import HorizontalLine from '../../components/HorizontalLine'
import Label from '../../components/Label'
import InputDropdown from '../create_session/component/InputDropdown'
import SelectModalV4 from '../../components/SelectModalV4'

const mapStateToProps = ({ auth, job }) => ({
  userData: auth.user,
  jobRegistrationForm: job.jobRegistrationForm,
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  idAds: getParam('idAds', getParam('id_ads', 0)),
})

const mapDispatchToProps = {
  jobRegistrationSave: jobDispatcher.jobRegistrationSave,
  jobRegistrationClear: jobDispatcher.jobRegistrationClear,
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
  setClearBlueMessage,
}

const RESPONSE_TYPES = {
  success: {
    title: 'Hooray!',
    message: 'Your registration form has been submitted',
    image: require('../../assets/images/successSubmision.png'),
    imageRatio: 238 / 134.2,
    btnText: 'Back to explore',
  },
  failed: {
    title: 'Registration Failed',
    message: 'It may you make a mistakes somewhere',
    image: require('../../assets/images/submission.png'),
    imageRatio: 80 / 82,
    btnText: 'OK !',
  },
  not_premium: {
    title: 'Ooops',
    message:
      'This Audition is only available for premium user. Let’s be our premium member!',
    image: require('../../assets/images/notPremiumUser.png'),
    imageRatio: 182 / 108,
    btnText: 'Join Premium',
  },
}

const validation = yup.object().shape({
  name: yup.string().required(),
  // phoneNumber: yup.string().required(),
  // team: yup.array().test('min-1', 'Minimal 1 value', (value) => !isEmpty(compact(value))),
})

const ButtonIcon = ({
  onPress = () => {},
  title,
  iconName,
  iconType = 'MaterialCommunityIcons',
  iconPosition = 'left',
  textColor = ORANGE_BRIGHT,
  iconColor = ORANGE_BRIGHT,
}) => (
  <TouchableOpacity
    onPress={onPress}
    activeOpacity={TOUCH_OPACITY}
    style={{ flexDirection: 'row', alignItems: 'center' }}
  >
    {iconPosition == 'right' ? (
      <Text
        type='Circular'
        size='xmini'
        weight={400}
        color={textColor}
        style={{ marginRight: WP105 }}
      >
        {title}
      </Text>
    ) : null}
    <Icon centered size='small' color={iconColor} name={iconName} type={iconType} />
    {iconPosition == 'left' ? (
      <Text
        type='Circular'
        size='xmini'
        weight={400}
        color={textColor}
        style={{ marginLeft: WP105 }}
      >
        {title}
      </Text>
    ) : null}
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: '100%',
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 6,
  },
})

class StartupSubmissionForm extends Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props)
    this._didFocusSubscription = props.navigation.addListener('focus', () =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler),
    )
  }
  state = {
    ads: { additional_data: { date: {}, location: {} } },
    isReady: false,
    backModal: false,
    percentCompleted: 0,
    isUpdate: false,
    peoples: [],
    team: [
      {
        full_name: '',
        id_user: 0,
        position: '',
      },
    ],
    categoryStartUp: [],
    selectedCategory: {
      label: '',
      value: '',
    },
  };

  async componentDidMount() {
    await this._getInitialScreenData()

    this._willBlurSubscription = this.props.navigation.addListener('blur', () =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler),
    )
  }

  _getCategoryStartUp = async () => {
    const { dispatch } = this.props
    try {
      const { result } = await dispatch(getStartupCategory, {}, noop, true, false)
      const newResult = result.map((item) => {
        return {
          label: item,
          value: item,
        }
      })
      await this.setState({
        categoryStartUp: result,
      })
    } catch (e) {
      // silent
    }
  };

  _getInitialScreenData = async (...args) => {
    await this._getAdsDetail(...args)
    await this._getCategoryStartUp()
    await this.setState({
      isReady: true,
    })
  };

  _mapUserSubmission = (userSubmission) => {
    const lastJourney = userSubmission.journey[userSubmission.journey.length - 1]
    this.setState({
      selectedCategory: {
        label: lastJourney.category,
        value: lastJourney.category,
      },
      team: lastJourney.team,
    })

    return {
      name: lastJourney.name,
      innovationTitle: lastJourney.inovation_title,
      profileCityId: lastJourney.domisili.id_city,
      profileCityName: lastJourney.domisili.city_name,
      profileProvinceId: lastJourney.domisili.id_province,
      profileProvinceName: lastJourney.domisili.province_name,
      inovationDescription: lastJourney.inovation_description,
      ceoUserId: lastJourney.ceo.id_user,
      category: lastJourney.category,
      ceoName: lastJourney.ceo.full_name,
    }
  };

  _onSubmissionRegister = async ({ formData }) => {
    const {
      userData: { id_user },
      idAds,
      dispatch,
    } = this.props
    const { ads, team } = this.state
    let encoded = new URLSearchParams()
    encoded.append('id_ads', 7)
    encoded.append('id_user', id_user)
    encoded.append('name', formData.name)
    encoded.append('inovation_title', formData.innovationTitle)
    encoded.append('domisili', formData.profileCityId)
    // province_name: formData.team,
    encoded.append('category', formData.category)
    encoded.append('inovation_description', formData.inovationDescription)
    // registration_date: registration_date,
    encoded.append('ceo', formData.ceoUserId)
    encoded.append('id_city', formData.profileCityId)
    team.map((item) => {
      encoded.append('team[]', JSON.stringify(item))
    })
    const result = await dispatch(postStartupSubmission, encoded, null, true)
    return result
  };

  getPeople(name) {
    this.props
      .dispatch(getAdvancedSearch, {
        type: 'people',
        search: name,
        start: peoples.length,
        limit: 15,
        id_user,
      })
      .then((result) => {
        this.setState(
          {
            peoples: uniqWith([...peoples, ...result.people], isEqual),
          },
          () => this.setState({ isLoading: false }),
        )
        result.people.length == 0 &&
          this.setState({
            endDataReached: true,
          })
      })
  }

  _getAdsDetail = async (isLoading = true) => {
    const {
      dispatch,
      jobRegistrationSave,
      userData: { id_user },
      idAds: id_ads,
    } = this.props
    const dataResponse = await dispatch(
      getAdsDetail,
      { id_user, id_ads: 7 },
      noop,
      false,
      isLoading,
    )
    let isUpdate = false
    if (dataResponse.user_submission) {
      jobRegistrationSave(this._mapUserSubmission(dataResponse.user_submission))
      isUpdate = true
    } else {
      jobRegistrationSave({ team: [''] })
    }
    this.setState({
      ads: {
        ...dataResponse,
        additional_data: JSON.parse(dataResponse.additional_data),
      },
      isUpdate,
    })
  };

  _backHandler = async () => {
    const { navigateBack } = this.props
    const { backModal } = this.state

    if (this.form && this.form.isDirtyState()) {
      this.setState({ backModal: !backModal })
    } else {
      navigateBack()
    }
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
    this.props.jobRegistrationClear()
  }

  _renderTop = (ads, additionalData) => (
    <>
      <Image
        tint={'black'}
        imageStyle={{ width: WP100, height: undefined }}
        aspectRatio={360 / 168}
        source={require('sf-assets/images/bgSubmission.png')}
      />
      <Card
        style={[
          {
            borderRadius: 5,
            padding: WP4,
            marginBottom: HP2,
            marginTop: -(WP100 / (360 / 168)) / 2 + HP2,
          },
          SHADOW_STYLE['shadowThin'],
        ]}
      >
        <View
          style={{
            flexDirection: 'row',
          }}
        >
          <Image
            source={{ uri: ads.image }}
            imageStyle={{ borderRadius: 12, width: WP15 }}
            style={{ width: WP15 }}
          />
          <View style={{ marginLeft: WP3, flex: 1, justifyContent: 'center' }}>
            <Text
              numberOfLines={2}
              weight={500}
              type='Circular'
              color={GUN_METAL}
              size='small'
            >
              {ads.title}
            </Text>
            {!!Number(ads.is_premium) && !isIOS() && (
              <Button
                rounded
                colors={[ORANGE, ORANGE]}
                compact='center'
                style={{ alignItems: 'flex-start' }}
                contentStyle={{ paddingHorizontal: WP105 }}
                contentSt
                marginless
                toggle
                toggleActive={false}
                toggleInactiveTextColor={ORANGE}
                text='Audition for user premium'
                textSize='xtiny'
                shadow='none'
                textWeight={300}
              />
            )}
            <View style={{ marginTop: HP1, flexDirection: 'row' }}>
              <Icon name='calendar' color={SHIP_GREY_CALM} />
              <Text
                type='Circular'
                style={{ marginBottom: WP1 }}
                color={GREY_WARM}
                size='tiny'
              >{` ${
                daysRemaining(additionalData.date.end) == 1
                  ? 'Hari Terakhir'
                  : `${daysRemaining(additionalData.date.end)} hari lagi`
              }  ·  ${additionalData.location.name}`}</Text>
            </View>
          </View>
        </View>
        {ads.is_premium === '1' && (
          <>
            <HorizontalLine
              color={PALE_GREY}
              style={{ marginVertical: HP1, marginLeft: -WP4 }}
            />
            <View style={{ flexDirection: 'row' }}>
              <View
                style={{
                  backgroundColor: PALE_GREY,
                  padding: WP1,
                  borderRadius: WP105,
                }}
              >
                <Text
                  weight={400}
                  type='Circular'
                  color={SHIP_GREY_CALM}
                  size='mini'
                >
                  {' '}
                  Premium User{' '}
                </Text>
              </View>
            </View>
          </>
        )}
      </Card>
    </>
  );

  _onChangeCategory = (value) => {
    this.setState({
      selectedCategory: {
        label: value,
        value,
      },
    })
  };

  _renderProfile = ({ onChange, formData }) => (
    <>
      <Text
        type='Circular'
        color={GUN_METAL}
        weight={500}
        size='xmini'
        style={{ marginBottom: HP1 }}
      >
        PROFIL STARTUP
      </Text>
      <InputTextLight
        bordered
        size='mini'
        returnKeyType={'next'}
        placeholder='Tuliskan nama startup'
        placeholderTextColor={SILVER_TWO}
        color={SHIP_GREY}
        value={formData.name}
        onChangeText={onChange('name')}
        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
        labelv2='Nama Startup'
        lineHeight={1}
        multiline
      />
      <InputTextLight
        bordered
        size='mini'
        returnKeyType={'next'}
        placeholder='Tuliskan judul inovasi'
        placeholderTextColor={SILVER_TWO}
        color={SHIP_GREY}
        value={formData.innovationTitle}
        onChangeText={onChange('innovationTitle')}
        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
        labelv2='Judul Inovasi'
        lineHeight={1}
        multiline
      />
      <Label
        style={{ marginTop: HP105, marginBottom: WP1 }}
        title={'Domisili Provinsi'}
        subtitle={'(contoh: Manado, Gorontalo)'}
      />
      <SelectModalV3
        refreshOnSelect
        triggerComponent={
          <View style={[styles.input, { marginBottom: WP4 }]}>
            <Text
              type='Circular'
              size='xmini'
              weight={300}
              color={
                isEmpty(formData.profileProvinceName) ? SILVER_TWO : SHIP_GREY_CALM
              }
            >
              {isEmpty(formData.profileProvinceName)
                ? 'Pilih Provinsi'
                : formData.profileProvinceName}
            </Text>
          </View>
        }
        header='Domisili Provinsi'
        suggestion={getProvince}
        suggestionKey='province_name'
        suggestionPathResult='province_name'
        suggestionPathValue='id_province'
        onChange={(province) => {
          onChange('profileProvinceName')(province.province_name)
          onChange('profileProvinceId')(province.id_province)
        }}
        suggestionCreateNewOnEmpty={false}
        createNew={false}
        asObject={true}
        reformatFromApi={(text) => startCase(toLower(text))}
        placeholder='Coba cari Provinsi'
      />
      <Label
        style={{ marginTop: HP105, marginBottom: WP1 }}
        title={'Domisili Kota'}
        subtitle={'(contoh: Manado, Gorontalo)'}
      />
      <SelectModalV3
        refreshOnSelect
        triggerComponent={
          <View style={[styles.input, { marginBottom: WP4 }]}>
            <Text
              type='Circular'
              size='xmini'
              weight={300}
              color={isEmpty(formData.profileCityName) ? SILVER_TWO : SHIP_GREY_CALM}
            >
              {isEmpty(formData.profileCityName)
                ? 'Pilih Kota'
                : formData.profileCityName}
            </Text>
          </View>
        }
        header='Domisili Kota'
        suggestion={getCityV3}
        id_province={formData.profileProvinceId}
        suggestionKey='city_name'
        suggestionPathResult='city_name'
        suggestionPathValue='id_city'
        onChange={(city) => {
          onChange('profileCityName')(city.city_name)
          onChange('profileCityId')(city.id_city)
        }}
        suggestionCreateNewOnEmpty={false}
        createNew={false}
        asObject={true}
        reformatFromApi={(text) => startCase(toLower(text))}
        placeholder='Coba cari Kota'
      />
      <Label title={'Nama CEO Inovasi'} subtitle={'(contoh: Andika Leonardo)'} />
      <SelectModalV3
        refreshOnSelect
        triggerComponent={
          <View style={[styles.input, { marginBottom: WP4 }]}>
            <Text
              type='Circular'
              size='xmini'
              weight={300}
              color={isEmpty(formData.ceoName) ? SILVER_TWO : SHIP_GREY_CALM}
            >
              {isEmpty(formData.ceoName)
                ? 'Tuliskan nama Ketua Inovasi'
                : formData.ceoName}
            </Text>
          </View>
        }
        header='Ketua Inovasi'
        suggestion={getStartupUser}
        suggestionKey='full_name'
        suggestionPathResult='full_name'
        suggestionPathValue='id_user'
        onChange={(ceo) => {
          onChange('ceoName')(ceo.full_name)
          onChange('ceoUserId')(ceo.id_user)
        }}
        suggestionCreateNewOnEmpty={false}
        createNew={false}
        asObject={true}
        reformatFromApi={(text) => startCase(toLower(text))}
        placeholder='Tuliskan nama Ketua Inovasi'
      />
      <Label title={'Category Inovasi'} subtitle={'(Pilih salah satu)'} />
      {/* <InputDropdown
        placeholder={'Pilih salah satu Category'}
        value={this.state.selectedCategory.value}
        options={this.state.categoryStartUp}
        onChange={this._onChangeCategory}
      /> */}
      <SelectModalV4
        refreshOnSelect
        triggerComponent={
          <View style={[styles.input, { marginBottom: WP4 }]}>
            <Text
              type='Circular'
              size='xmini'
              weight={300}
              color={
                isEmpty(formData.category) ? SILVER_TWO : SHIP_GREY_CALM
              }
            >
              {isEmpty(formData.category)
                ? 'Pilih salah satu Category'
                : formData.category}
            </Text>
          </View>
        }
        header='Pilih Role'
        items={this.state.categoryStartUp}
        onChange={onChange('category')}
        reformatFromApi={(text) => startCase(toLower(text))}
        placeholder='Coba cari Category'
      />
      <InputTextLight
        bordered
        size='mini'
        returnKeyType={'next'}
        placeholder='Tuliskan deskripsi inovasi'
        placeholderTextColor={SILVER_TWO}
        color={SHIP_GREY}
        value={formData.inovationDescription}
        onChangeText={onChange('inovationDescription')}
        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
        labelv2='Deskripsi Inovasi'
        subLabel={'(Problem, Solution, Market)'}
        lineHeight={1}
        multiline
      />
    </>
  );

  onChangeTeamUser = (data, index) => {
    let team = Object.assign([], this.state.team)
    team[index] = {
      ...team[index],
      full_name: data.full_name,
      id_user: data.id_user,
    }
    this.setState({
      team,
    })
  };

  onChangeTeamPosition = (data, index) => {
    let team = Object.assign([], this.state.team)
    team[index] = {
      ...team[index],
      position: data,
    }
    this.setState({
      team,
    })
  };

  _renderSocialMedia = ({ onChange, formData }) => (
    <>
      <Text
        type='Circular'
        color={GUN_METAL}
        weight={500}
        size='xmini'
        style={{ marginBottom: HP1 }}
      >
        KOMPOSISI TIM
        <Text type='Circular' size={'mini'} color={PALE_LIGHT_BLUE_TWO}>
          {' (Nama dan Jabatan)'}
        </Text>
      </Text>
      {this.state.team.map((item, index) => (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
          }}
        >
          <View style={{ width: WP43 }}>
            <Label title={'Nama'} />
            <SelectModalV3
              refreshOnSelect
              triggerComponent={
                <View style={[styles.input, { marginBottom: WP4 }]}>
                  <Text
                    type='Circular'
                    size='xmini'
                    weight={300}
                    color={isEmpty(item.full_name) ? SILVER_TWO : SHIP_GREY_CALM}
                  >
                    {isEmpty(item.full_name) ? 'Tuliskan nama' : item.full_name}
                  </Text>
                </View>
              }
              header='Anggota'
              suggestion={getStartupUser}
              suggestionKey='full_name'
              suggestionPathResult='full_name'
              suggestionPathValue='id_user'
              onChange={(data) => this.onChangeTeamUser(data, index)}
              suggestionCreateNewOnEmpty={false}
              createNew={false}
              asObject={true}
              reformatFromApi={(text) => startCase(toLower(text))}
              placeholder='Tuliskan nama'
            />
          </View>
          <View style={{ width: WP43 }}>
            <Label title={'Posisi'} />
            <InputModal
              extraResult
              hideSearch
              refreshOnSelect
              triggerComponent={
                <InputTextLight
                  onChangeText={(data) => this.onChangeTeamPosition(data, index)}
                  value={item.position}
                  placeholder='Tuliskan Posisi'
                  placeholderTextColor={SILVER_TWO}
                  color={SHIP_GREY_CALM}
                  bordered
                  size='xmini'
                  type='Circular'
                  returnKeyType={'next'}
                  style={{ marginTop: 0, marginBottom: WP4 }}
                  textInputStyle={styles.input}
                  containerStyle={{ height: WP5 }}
                />
              }
              header='Pilih Posisi'
              suggestion={getJobStartUp}
              suggestionKey='job_title'
              suggestionPathResult='job_title'
              selected={item.position}
              onChange={(data) => this.onChangeTeamPosition(data, index)}
              reformatFromApi={(text) => startCase(toLower(text))}
              placeholder='Search profession here...'
            />
          </View>
        </View>
      ))}
      <ButtonIcon
        iconName={'plus'}
        title={'Tambahkan Anggota Tim'}
        onPress={() => this.addTeamMember()}
      />
    </>
  );

  addTeamMember = () => {
    let newTeam = Object.assign([], this.state.team)
    newTeam.push({
      full_name: '',
      id_user: 0,
      position: '',
    })
    this.setState({
      team: newTeam,
    })
  };
  render() {
    const {
      isLoading,
      navigateTo,
      jobRegistrationForm,
      jobRegistrationSave,
      userData,
      navigateBack,
      id,
      paymentSourceSet,
    } = this.props
    const { ads, isReady, backModal, isUpdate } = this.state
    const additionalData = ads.additional_data
    return (
      <Form
        ref={(form) => (this.form = form)}
        validation={validation}
        initialValue={{ ...jobRegistrationForm }}
        onSubmit={this._onSubmissionRegister}
      >
        {({ onChange, onSubmit, formData, isValid }) => (
          <Container
            isLoading={isLoading}
            isReady={isReady}
            renderHeader={() => (
              <HeaderNormal
                iconLeftOnPress={this._backHandler}
                textType='Circular'
                textColor={SHIP_GREY}
                textWeight={400}
                textSize={'slight'}
                text={'Submission'}
                iconLeftWrapperStyle={{ marginRight: WP10 }}
                centered
                rightComponent={
                  <TouchableOpacity
                    onPress={async () => {
                      Keyboard.dismiss()
                      if (
                        isIOS() ||
                        (isPremiumUser(userData.account_type) &&
                          !!Number(ads.is_premium)) ||
                        !Number(ads.is_premium)
                      ) {
                        this.setState({ percentCompleted: 20 })
                        const response = await onSubmit()
                        if (response.code === 200)
                          this.setState({ percentCompleted: 70 })
                        this.setState({ percentCompleted: 100 })
                        this.props.setClearBlueMessage(
                          'Startup Submission Berhasil',
                        )
                        this.props.popToTop()
                        this.props.navigateTo('StartUpProfileScreen', {
                          refreshProfile: true,
                        })
                      } else {
                        jobRegistrationSave(formData)
                        this.setState({ percentCompleted: 0 })
                      }
                    }}
                    style={{ paddingHorizontal: WP405 }}
                    activeOpacity={TOUCH_OPACITY}
                    disabled={!isValid}
                  >
                    <Text
                      size='slight'
                      weight={400}
                      color={isValid ? ORANGE_BRIGHT : PALE_SALMON}
                    >
                      Submit
                    </Text>
                  </TouchableOpacity>
                }
              >
                <View
                  style={{ width: WP100, height: WP05, backgroundColor: PALE_GREY }}
                >
                  <Animated.View
                    style={{
                      height: WP05,
                      backgroundColor: REDDISH,
                      width: WP100 * (this.state.percentCompleted / 100),
                    }}
                  />
                </View>
              </HeaderNormal>
            )}
            isAvoidingView
            scrollable
            scrollBackgroundColor={PALE_WHITE}
          >
            <View style={{ paddingHorizontal: WP5, paddingBottom: HP5 }}>
              <ModalMessageView
                style={{ width: WP100 - WP8 }}
                contentStyle={{ paddingHorizontal: WP4 }}
                toggleModal={() => this.setState({ backModal: false })}
                isVisible={backModal}
                title={isUpdate ? 'Batalkan Perubahan?' : 'Join Audition'}
                titleType='Circular'
                titleSize={'small'}
                titleColor={GUN_METAL}
                titleStyle={{ marginBottom: WP4 }}
                subtitle={
                  isUpdate
                    ? 'Informasi pendaftaran Audition belum dilakukan. \nBatalkan perubahan?'
                    : 'Proses pendaftaran kamu belum selesai. \nLanjutkan pendaftaran?'
                }
                subtitleType='Circular'
                subtitleSize={'xmini'}
                subtitleWeight={400}
                subtitleColor={SHIP_GREY_CALM}
                subtitleStyle={{ marginBottom: WP3 }}
                image={null}
                buttonPrimaryText={isUpdate ? 'Batalkan' : 'Lanjutkan'}
                buttonPrimaryContentStyle={{
                  borderRadius: 8,
                  paddingVertical: WP305,
                }}
                buttonPrimaryTextType='Circular'
                buttonPrimaryTextWeight={400}
                buttonPrimaryBgColor={REDDISH}
                buttonPrimaryAction={() => {
                  if (isUpdate) {
                    navigateBack()
                  } else {
                    this.setState({ backModal: false })
                  }
                }}
                buttonSecondaryText={
                  isUpdate ? 'Lanjutkan Ubah informasi' : 'Batalkan'
                }
                buttonSecondaryStyle={{
                  backgroundColor: PALE_GREY,
                  marginTop: WP1,
                  borderRadius: 8,
                  paddingVertical: WP305,
                }}
                buttonSecondaryTextType='Circular'
                buttonSecondaryTextWeight={400}
                buttonSecondaryTextColor={SHIP_GREY_CALM}
                buttonSecondaryAction={() => {
                  if (isUpdate) {
                    this.setState({ backModal: false })
                  } else {
                    navigateBack()
                  }
                }}
              />
              {this._renderTop(ads, additionalData)}
              <HorizontalLine style={{ marginLeft: -WP5, marginVertical: HP2 }} />
              {this._renderProfile({ ads, additionalData, onChange, formData })}
              <HorizontalLine style={{ marginLeft: -WP5, marginVertical: HP2 }} />
              {this._renderSocialMedia({ ads, additionalData, onChange, formData })}
            </View>
          </Container>
        )}
      </Form>
    )
  }
}

StartupSubmissionForm.navigationOptions = () => ({
  gesturesEnabled: false,
})
StartupSubmissionForm.defaultProps = {
  userData: {},
  isLoading: false,
  onRefresh: noop,
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(StartupSubmissionForm),
  mapFromNavigationParam,
)
