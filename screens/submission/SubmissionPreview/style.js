import { StyleSheet } from 'react-native'
import { WP05, WP1, WP100, WP15, WP2, WP20, WP3, WP30, WP308, WP4, WP40, WP5, WP6, WP7 } from 'sf-constants/Sizes'
import { PALE_BLUE, PALE_GREY, PALE_WHITE, SHIP_GREY, WHITE } from 'sf-constants/Colors'
import { SHADOW_STYLE } from '../../../constants/Styles'

const coverHeight = (WP100 * 315) / 930

export default StyleSheet.create({
  spacer05x: {
    height: WP05,
  },
  spacer1x: {
    height: WP1,
  },
  spacer2x: {
    height: WP2,
  },
  spacer3x: {
    height: WP3,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowCentered: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerBackground: {
    width: WP100,
    marginBottom: WP20,
  },
  headerCover: {
    width: WP40,
    height: WP40,
    position: 'absolute',
    left: WP30,
    top: WP20,
    borderRadius: WP2,
    backgroundColor: WHITE,
    elevation: 10,
  },
  headerCoverImage: {
    width: WP40,
    height: WP40,
    borderRadius: WP2,
  },
  summary: {
    paddingHorizontal: WP5,
    paddingBottom: WP7,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  content: {
    padding: WP5,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  contentTerms: {
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
    backgroundColor: PALE_GREY,
  },
  contentTermsHeading: {
    paddingHorizontal: WP5,
    paddingVertical: WP4,
  },
  contentTermsDescription: {
    paddingHorizontal: WP5,
    marginTop: WP4,
  },
  bullet: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: WP1,
  },
  bulletIcon: {
    paddingTop: WP05,
    marginRight: WP2,
  },
  bulletItem: {
    flex: 1,
  },
  genreItem: {
    marginRight: WP2,
    marginVertical: WP1,
  },
  genreWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  readMore: {
    marginTop: WP2,
    paddingVertical: WP1,
  },
  otherInfo: {
    backgroundColor: WHITE,
    borderRadius: WP2,
    padding: WP4,
    flexDirection: 'row',
    alignItems: 'center',
    ...SHADOW_STYLE['shadowThin'],
  },
  otherInfoThumb: {
    width: WP15 + WP1,
    height: WP15 + WP1,
    borderRadius: WP2,
  },
  otherInfoDescription: {
    flex: 1,
    paddingLeft: WP5,
    paddingRight: WP2,
  },
  cta: {
    backgroundColor: WHITE,
    padding: WP4,
    elevation: 10,
  },
  ctaButton: {
    paddingVertical: WP4,
  },
  headerNormal: {
    padding: 0,
    paddingRight: 0,
  },
  dotsTouchable: {
    padding: WP4,
  },
  contentDescriptionText: {
    fontSize: WP308,
    fontFamily: 'CircularBook',
    color: SHIP_GREY,
    lineHeight: WP6,
    flexWrap: 'wrap',
  },
  moreDescriptionToggler: {
    paddingVertical: WP2,
    flexDirection: 'row',
    alignItems: 'center',
  },
})
