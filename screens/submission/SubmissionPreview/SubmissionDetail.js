/* eslint-disable react/sort-comp */
import React, { Fragment } from 'react'
import {
  Animated,
  Dimensions,
  Easing,
  FlatList,
  StatusBar as StatusBarRN,
  View,
} from 'react-native'
import Container from 'sf-components/Container'
import Text from 'sf-components/Text'
import Icon from 'sf-components/Icon'
import Badge from 'sf-components/Badge'
import Touchable from 'sf-components/Touchable'
import ButtonV2 from 'sf-components/ButtonV2'
import Modal from 'sf-components/Modal'
import { WP1, WP2, WP3, WP405, WP5, WP6 } from 'sf-constants/Sizes'
import { get, isEmpty, noop, upperFirst } from 'lodash-es'
import {
  GUN_METAL,
  NAVY_DARK,
  PALE_GREY,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from 'sf-constants/Colors'
import Collapsible from 'react-native-collapsible'
import { isIOS, NavigateToInternalBrowser } from 'sf-utils/helper'
import SoundfrenExploreOptions from 'sf-components/explore/SoundfrenExploreOptions'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'
import HTMLElement from 'react-native-render-html'
import { toNormalDate } from 'sf-utils/date'
import * as Animatable from 'react-native-animatable'
import { SUBMISSION_OPTIONS } from '../v2/SubmissionOptions'
import {
  FONT_SIZE,
  WP100,
  WP105,
  WP15,
  WP16,
  WP20,
  WP4,
} from '../../../constants/Sizes'
import {
  NO_COLOR,
  PALE_WHITE,
  RED_20,
  VERY_LIGHT_PINK_TWO,
} from '../../../constants/Colors'
import { daysRemaining } from '../../../utils/date'
import { getStatusBarHeight } from '../../../utils/iphoneXHelper'
import { FONTS } from '../../../constants/Fonts'
import { Image } from '../../../components'
import style from './style'

const StatusBar = Animatable.createAnimatableComponent(StatusBarRN)
const descriptionCollapsedHeight = Dimensions.get('window').width * 0.225

class SubmissionDetail extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      scrollY: new Animated.Value(0),
      viewTerms: false,
      moreDescription: false,
      showMoreDescriptionToggler: false,
      descriptionHeight: 0,
      joinModalVisible: false,
    }
    this._header = this._header.bind(this)
    this._bulletList = this._bulletList.bind(this)
    this._keyStringExtractor = this._keyStringExtractor.bind(this)
    this._goToWeb = this._goToWeb.bind(this)
    this._toggleTerms = this._toggleTerms.bind(this)
    this._joinAudition = this._joinAudition.bind(this)
    this._joinEvent = this._joinEvent.bind(this)
    this._renderModalToggler = this._renderModalToggler.bind(this)
    this._renderModalContent = this._renderModalContent.bind(this)
    this._rightComponent = this._rightComponent.bind(this)
    this._toggleJoinModal = this._toggleJoinModal.bind(this)
    this._onLinkPress = this._onLinkPress.bind(this)
    this._onDescriptionLayout = this._onDescriptionLayout.bind(this)
    this._toggleFullDescription = this._toggleFullDescription.bind(this)
  }

  _toggleFullDescription = () => {
    this.setState((state) => ({
      moreDescription: !state.moreDescription,
    }))
  };

  _onLinkPress = (event, url) =>
    this.props.navigateTo('BrowserScreenNoTab', { url });

  _renderModalContent = ({ toggleModal }) => {
    const { ads, userData, navigateTo } = this.props
    const category = get(ads, 'category')
    const isEvent = category == 'event'
    const idAds = ads?.id_ads
    return (
      <SoundfrenExploreOptions
        menuOptions={SUBMISSION_OPTIONS}
        userData={userData}
        navigateTo={navigateTo}
        onClose={toggleModal}
        onShare={() => {
          navigateTo('ShareScreen', {
            id: idAds,
            type: 'explore',
            title: `Share ${
              isEvent ? 'Event' : isAds == 7 ? 'Submission' : 'Audition'
            }`,
          })
        }}
      />
    )
  };

  _toggleJoinModal = () => {
    this.setState((state) => ({ joinModalVisible: !state.joinModalVisible }))
  };

  componentDidMount() {
    if (!this.props.userData.id_user) {
      this.props.navigateBack()
      this.props.navigateTo('AuthNavigator')
    }
  }

  _renderModalToggler = ({ toggleModal }, M) => (
    <Fragment>
      <Touchable style={style.dotsTouchable} onPress={toggleModal}>
        <View>
          <Icon
            centered
            size='small'
            color={SHIP_GREY_CALM}
            name='dots-three-horizontal'
            type='Entypo'
          />
        </View>
      </Touchable>
      {M}
    </Fragment>
  );

  _rightComponent = (iconColor) => (
    <Modal
      position='bottom'
      swipeDirection={null}
      renderModalContent={this._renderModalContent}
    >
      {({ toggleModal }, M) => (
        <Fragment>
          <Touchable style={style.dotsTouchable} onPress={toggleModal}>
            <View>
              <Icon
                centered
                size='small'
                color={iconColor}
                name='dots-three-horizontal'
                type='Entypo'
              />
            </View>
          </Touchable>
          {M}
        </Fragment>
      )}
    </Modal>
  );

  _header = (category) => {
    const { navigateBack, ads } = this.props
    const bgColor = this.state.scrollY.interpolate({
      inputRange: [-10, 0, WP5, WP15],
      outputRange: [WHITE, NO_COLOR, NO_COLOR, WHITE],
      extrapolate: 'clamp',
    })
    const iconColor = this.state.scrollY.interpolate({
      inputRange: [-10, 0, WP5, WP20],
      outputRange: [SHIP_GREY_CALM, WHITE, WHITE, SHIP_GREY_CALM],
      extrapolate: 'clamp',
    })
    const title =
      category == 'event'
        ? 'Event'
        : category == 'submission'
        ? ads.id_ads == 7
          ? 'Submission Detail'
          : 'Submission Detail'
        : ''

    return (
      <View style={{ width: WP100, position: 'absolute', zIndex: 999, top: 0 }}>
        <StatusBar
          animated
          translucent={true}
          backgroundColor={bgColor}
          barStyle={'dark-content'}
        />
        <Animated.View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: bgColor,
            paddingTop: getStatusBarHeight(true),
          }}
        >
          <Touchable
            onPress={() => navigateBack()}
            style={{ padding: WP2, paddingHorizontal: WP4 }}
          >
            <Icon
              centered
              background='dark-circle'
              backgroundColor='rgba(255,255,255, 0.16)'
              size='huge'
              color={iconColor}
              name='chevron-left'
              type='Entypo'
            />
          </Touchable>

          <Animated.View
            style={{
              flex: 1,
              borderRadius: 6,
              alignItems: 'center',
              padding: WP2,
              marginHorizontal: WP2,
              marginRight: WP16,
            }}
          >
            <Animated.Text
              style={{
                color: iconColor,
                fontFamily: FONTS['Circular'][400],
                fontSize: FONT_SIZE['mini'],
                lineHeight: WP5,
              }}
              centered
            >
              {title}
            </Animated.Text>
          </Animated.View>
        </Animated.View>
      </View>
    )
  };

  _keyStringExtractor = (item) =>
    (item || Math.random())
      .toLowerCase()
      .replace(/[^\w ]+/g, '')
      .replace(/ +/g, '-');

  _bulletList = ({ item, index }) => {
    return (
      <View style={style.bullet}>
        <View style={style.bulletIcon}>
          <Text size={'slight'} color={SHIP_GREY_CALM} type={'Circular'}>
            •
          </Text>
        </View>
        <View style={style.bulletItem}>
          <Text lineHeight={WP6} size={'mini'} color={SHIP_GREY} type={'Circular'}>
            {item}
          </Text>
        </View>
      </View>
    )
  };

  _renderRevealedFooter = (handlePress) => {
    return <View />
    // return <Touchable onPress={handlePress} style={style.readMore}>
    //   <Text type={'Circular'} color={REDDISH} size={'mini'}>Selengkapnya</Text>
    // </Touchable>
  };

  _renderTruncatedFooter = (handlePress) => {
    return (
      <Touchable onPress={handlePress} style={style.readMore}>
        <Text type={'Circular'} color={REDDISH} size={'mini'}>
          Selengkapnya
        </Text>
      </Touchable>
    )
  };

  _onDescriptionLayout = ({
    nativeEvent: {
      layout,
      layout: { height },
    },
  }) => {
    this.setState({
      showMoreDescriptionToggler: isIOS()
        ? true
        : height >= descriptionCollapsedHeight,
      descriptionHeight:
        height < descriptionCollapsedHeight ? height : descriptionCollapsedHeight,
    })
  };

  _genreList = (item) => {
    return (
      <View style={style.genreItem}>
        <Badge textColor={SHIP_GREY_CALM} color={PALE_GREY}>
          {upperFirst(item)}
        </Badge>
      </View>
    )
  };

  _toggleTerms = () => {
    this.setState((state) => ({
      viewTerms: !state.viewTerms,
    }))
  };

  _goToWeb = () => {
    const { ads } = this.props
    !!get(ads, 'meta.title') && NavigateToInternalBrowser(get(ads, 'meta'))
  };

  _joinAudition = () => {
    const {
      userData: { account_type },
      ads,
      ads: { is_premium, id_ads: idAds },
      navigateTo,
    } = this.props

    const isAllowJoin =
      (account_type != 'user' && is_premium == '1') || is_premium == '0'
    const url = get(ads, 'additional.redirect.link')
    if (!isAllowJoin) {
      this._toggleJoinModal()
    } else if (isAllowJoin) {
      if (!url) {
        if (idAds == 7) {
          navigateTo('StartupSubmissionForm', { idAds })
        } else {
          navigateTo('SubmissionRegisterForm', { idAds, ads })
        }
      } else if (url) {
        NavigateToInternalBrowser({ url })
      }
    }
  };

  _joinEvent = () => {
    const { ads } = this.props
    const url = get(ads, 'additional.redirect.link')
    NavigateToInternalBrowser({ url })
  };

  render() {
    const { ads, onRequestUpgrade } = this.props
    const { viewTerms, joinModalVisible } = this.state
    const additional = get(ads, 'additional')
    const day_left = Number(get(ads, 'day_left'))
    const category = get(ads, 'category')
    const isEvent = category == 'event'

    const isExpired = daysRemaining(get(additional, 'date.end'), false) < 0,
      createdBy = `${isEvent ? 'Event by' : 'Promoted by'} ${get(
        ads,
        'created_by',
      )}`,
      dateAudition = isExpired
        ? 'Telah Berakhir'
        : day_left < 0
        ? 'Closed'
        : day_left == 0
        ? 'Hari terakhir'
        : `${get(ads, 'day_left')} hari lagi ∙ ${
            get(ads, 'location') || get(additional, 'location.name')
          }`,
      dateEvent = `${toNormalDate(
        get(additional, 'date.start'),
        true,
      )} - ${toNormalDate(get(additional, 'date.end'), true)}`,
      dateLocation = `  ${isEvent ? dateEvent : dateAudition}`,
      aboutTitle = `Tentang ${isEvent ? 'Event' : 'Submission'} ini`,
      locationLink = get(additional, 'location.link'),
      buttonTitleEvent = get(additional, 'redirect.name')

    return (
      <Container
        isLoading={!ads}
        scrollable={false}
        noStatusBarPadding
        statusBarBackground={NO_COLOR}
      >
        <View style={{ flex: 1, backgroundColor: PALE_WHITE }}>
          {this._header(category)}
          <Animated.ScrollView
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: { contentOffset: { y: this.state.scrollY } },
                },
              ],
              { useNativeDriver: false },
            )}
            style={style.scrollView}
          >
            <View style={style.header}>
              <Image
                aspectRatio={360 / 156}
                imageStyle={{ width: WP100, height: undefined }}
                style={style.headerBackground}
                source={
                  isExpired
                    ? require('sf-assets/images/bgSubmissionExpired.png')
                    : require('sf-assets/images/bgSubmission.png')
                }
              />
              <View style={style.headerCover}>
                <Image
                  resizeMode={'cover'}
                  imageStyle={style.headerCoverImage}
                  source={get(ads, 'image') ? { uri: ads.image } : null}
                />
              </View>
            </View>
            <View style={style.summary}>
              <View style={style.rowCentered}>
                <Badge textColor={SHIP_GREY_CALM} color={PALE_GREY}>
                  {get(ads, 'is_premium') == '1' ? 'Premium User' : null}
                </Badge>
                {isExpired && (
                  <Badge
                    style={{ marginLeft: WP105 }}
                    textColor={RED_20}
                    color={VERY_LIGHT_PINK_TWO}
                  >
                    Telah Berakhir
                  </Badge>
                )}
              </View>
              <View style={style.spacer2x} />
              <Text
                color={NAVY_DARK}
                weight={600}
                centered
                size={'large'}
                type={'Circular'}
              >
                {upperFirst(get(ads, 'title'))}
              </Text>
              <View style={style.spacer2x} />
              <Text
                color={SHIP_GREY}
                weight={300}
                centered
                size={'mini'}
                type={'Circular'}
              >
                {createdBy}
              </Text>
              <View style={style.spacer2x} />
              <View style={style.rowCentered}>
                <View>
                  <Icon
                    size={'mini'}
                    color={SHIP_GREY_CALM}
                    name='calendar-range'
                    type='MaterialCommunityIcons'
                  />
                </View>
                <Text
                  color={SHIP_GREY_CALM}
                  weight={300}
                  centered
                  size={'mini'}
                  type={'Circular'}
                >
                  {dateLocation}
                </Text>
              </View>
            </View>
            {isEvent ? (
              <>
                <View style={style.content}>
                  <View style={style.contentDescription}>
                    <Text
                      color={SHIP_GREY}
                      weight={400}
                      size={'mini'}
                      type={'Circular'}
                    >
                      Tiket:
                    </Text>
                    <View style={style.spacer2x} />
                    <HTMLElement
                      textSelectable
                      html={upperFirst(get(additional, 'specification') || '')
                        .replace(/(\r\n|\n|\r|)/gm, '')
                        .trim()}
                      onLinkPress={this._onLinkPress}
                      baseFontStyle={style.contentDescriptionText}
                    />
                  </View>
                </View>
                <View style={style.content}>
                  <View style={style.contentDescription}>
                    <Text
                      color={SHIP_GREY}
                      weight={400}
                      size={'mini'}
                      type={'Circular'}
                    >
                      Venue & Lokasi:
                    </Text>
                    <View style={style.spacer2x} />
                    <Text
                      lineHeight={WP6}
                      size={'mini'}
                      color={SHIP_GREY}
                      type={'Circular'}
                    >
                      {get(additional, 'location.name')}
                    </Text>
                    {!isEmpty(locationLink) && (
                      <Touchable
                        onPress={() =>
                          this.props.navigateTo('BrowserScreenNoTab', {
                            url: locationLink,
                          })
                        }
                        style={style.moreDescriptionToggler}
                      >
                        <Text size={'mini'} color={REDDISH} type={'Circular'}>
                          Lihat lokasi
                        </Text>
                      </Touchable>
                    )}
                  </View>
                </View>
              </>
            ) : (
              <>
                <View style={style.content}>
                  <View style={style.contentDescription}>
                    <Text
                      color={SHIP_GREY}
                      weight={400}
                      size={'mini'}
                      type={'Circular'}
                    >
                      Benefits:
                    </Text>
                    <View style={style.spacer1x} />
                    <FlatList
                      data={get(additional, 'benefit') || []}
                      keyExtractor={this._keyStringExtractor}
                      renderItem={this._bulletList}
                    />
                  </View>
                </View>
                <View style={style.contentTerms}>
                  <Collapsible easing={Easing.ease} collapsed={!viewTerms}>
                    <View style={style.contentTermsDescription}>
                      <Text
                        color={SHIP_GREY}
                        weight={400}
                        size={'mini'}
                        type={'Circular'}
                      >
                        Syarat & Ketentuan:
                      </Text>
                      <View style={style.spacer2x} />
                      <View>
                        <HTMLElement
                          textSelectable
                          html={upperFirst(get(additional, 'terms_condition') || '')
                            .replace(/(\r\n|\n|\r|)/gm, '')
                            .trim()}
                          onLinkPress={this._onLinkPress}
                          baseFontStyle={style.contentDescriptionText}
                        />
                      </View>
                    </View>
                  </Collapsible>
                  <Touchable
                    style={style.contentTermsHeading}
                    onPress={this._toggleTerms}
                  >
                    <View style={style.row}>
                      <Text
                        color={REDDISH}
                        weight={400}
                        size={'mini'}
                        type={'Circular'}
                      >
                        {viewTerms ? 'Sembunyikan' : 'Lihat Syarat & Ketentuan'}
                      </Text>
                      <View>
                        <Icon
                          size={'large'}
                          color={REDDISH}
                          type={'MaterialCommunityIcons'}
                          name={viewTerms ? 'chevron-up' : 'chevron-down'}
                        />
                      </View>
                    </View>
                  </Touchable>
                </View>

                <View style={style.content}>
                  <View style={style.contentDescription}>
                    <Text
                      color={SHIP_GREY}
                      weight={400}
                      size={'mini'}
                      type={'Circular'}
                    >
                      Category:
                    </Text>
                    <View style={style.spacer3x} />
                    <View style={style.genreWrapper}>
                      {(get(additional, 'genre') || []).map(this._genreList)}
                    </View>
                  </View>
                </View>
              </>
            )}
            <View style={style.content}>
              <View style={style.contentDescription}>
                <Text color={SHIP_GREY} weight={400} size={'mini'} type={'Circular'}>
                  {aboutTitle}
                </Text>
                <View style={style.spacer2x} />
                {/*<ReadMore
                numberOfLines={6}
                renderTruncatedFooter={this._renderTruncatedFooter}
                renderRevealedFooter={this._renderRevealedFooter}
              >
							</ReadMore>*/}

                <Collapsible
                  collapsedHeight={
                    this.state.descriptionHeight || descriptionCollapsedHeight
                  }
                  collapsed={!this.state.moreDescription}
                >
                  <View
                    onLayout={this._onDescriptionLayout}
                    style={{
                      overflowWrap: 'break-word',
                      paddingRight: WP4,
                      marginRight: -WP4,
                    }}
                  >
                    <HTMLElement
                      textSelectable
                      html={upperFirst(get(ads, 'description') || '')
                        .replace(/(\r\n|\n|\r|)/gm, '')
                        .trim()}
                      onLinkPress={this._onLinkPress}
                      baseFontStyle={style.contentDescriptionText}
                    />
                  </View>
                </Collapsible>
                {this.state.showMoreDescriptionToggler && (
                  <Touchable
                    onPress={this._toggleFullDescription}
                    style={style.moreDescriptionToggler}
                  >
                    <Text
                      color={REDDISH}
                      weight={400}
                      size={'mini'}
                      type={'Circular'}
                    >
                      {this.state.moreDescription
                        ? 'Tampilkan sedikit  '
                        : 'Selengkapnya  '}
                    </Text>
                    <View>
                      <Icon
                        centered
                        size='small'
                        color={REDDISH}
                        name={
                          this.state.moreDescription ? 'chevron-up' : 'chevron-down'
                        }
                        type='Entypo'
                      />
                    </View>
                  </Touchable>
                )}
              </View>
            </View>
            {get(ads, 'meta.title') && (
              <View style={style.content}>
                <View style={style.contentDescription}>
                  <Text
                    color={SHIP_GREY}
                    weight={400}
                    size={'mini'}
                    type={'Circular'}
                  >
                    Informasi lainnya
                  </Text>
                  <View style={style.spacer3x} />
                  <Touchable onPress={this._goToWeb} style={style.otherInfo}>
                    <Image
                      style={style.otherInfoThumb}
                      source={
                        get(ads, 'meta.image')
                          ? { uri: get(ads, 'meta.image') }
                          : require('../../../assets/images/default_avatar.png')
                      }
                    />
                    <View style={style.otherInfoDescription}>
                      <Text
                        color={GUN_METAL}
                        weight={500}
                        size={'mini'}
                        numberOfLines={2}
                        lineHeight={WP405}
                        type={'Circular'}
                      >
                        {upperFirst(get(ads, 'meta.title'))}
                      </Text>
                      <View style={style.spacer1x} />
                      <Text
                        numberOfLines={1}
                        color={SHIP_GREY_CALM}
                        weight={300}
                        size={'xmini'}
                        type={'Circular'}
                      >
                        {get(ads, 'meta.url')}
                      </Text>
                    </View>
                  </Touchable>
                </View>
              </View>
            )}
            <ModalMessageView
              toggleModal={this._toggleJoinModal}
              isVisible={joinModalVisible}
              title={'Premium User'}
              titleType='Circular'
              titleSize={'small'}
              titleColor={GUN_METAL}
              subtitle={
                !isIOS()
                  ? 'Layanan ini dapat kamu gunakan apabila kamu adalah premium user'
                  : 'Paket premium kamu belum aktif. Silahkan hubungi admin Soundfren untuk dapat mengaktifkan paket premium dan mengakses layanan ini.'
              }
              subtitleType='Circular'
              subtitleSize={'xmini'}
              subtitleWeight={300}
              subtitleLineHeight={WP5}
              subtitleColor={SHIP_GREY_CALM}
              image={null}
              buttonPrimaryText={!isIOS() ? 'Pelajari lebih lanjut' : null}
              buttonPrimaryTextType='Circular'
              buttonPrimaryTextWeight={400}
              buttonPrimaryBgColor={REDDISH}
              buttonPrimaryAction={onRequestUpgrade}
              buttonSecondaryText={!isIOS() ? 'Lihat nanti' : 'Tutup'}
              buttonSecondaryTextType='Circular'
              buttonSecondaryTextWeight={400}
              buttonSecondaryBgColor={PALE_GREY}
              buttonSecondaryStyle={{
                paddingVertical: WP3,
                borderRadius: WP2,
                marginTop: WP1,
              }}
              buttonSecondaryAction={noop}
            />
          </Animated.ScrollView>
          {isEvent ? (
            <View style={style.cta}>
              <ButtonV2
                onPress={this._joinEvent}
                style={style.ctaButton}
                color={REDDISH}
                textColor={WHITE}
                textSize={'small'}
                text={
                  buttonTitleEvent == '' || buttonTitleEvent == '-'
                    ? 'Dapatkan Tiket'
                    : buttonTitleEvent
                }
              />
            </View>
          ) : day_left >= 0 && !isExpired ? (
            <View style={style.cta}>
              <ButtonV2
                onPress={this._joinAudition}
                style={style.ctaButton}
                color={REDDISH}
                textColor={WHITE}
                textSize={'small'}
                text={
                  !get(ads, 'user_joined')
                    ? 'Daftar Sekarang'
                    : 'Lihat Form Pendaftaran'
                }
              />
            </View>
          ) : null}
        </View>
      </Container>
    )
  }
}

export default SubmissionDetail
