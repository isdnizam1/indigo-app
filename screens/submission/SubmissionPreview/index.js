import React from 'react'
import { View, StyleSheet } from 'react-native'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import InteractionManager from 'sf-utils/InteractionManager'
import { NavigationEvents } from '@react-navigation/compat'
import { getAdsDetail } from 'sf-actions/api'
import { connect } from 'react-redux'
import { omit, get, isUndefined } from 'lodash-es'
import axios from 'axios'
import { parse } from 'node-html-parser'
import { paymentDispatcher } from 'sf-services/payment'
import { AllHtmlEntities as Entities } from 'html-entities'
import SubmissionDetail from './SubmissionDetail'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
}

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam('idAds', getParam('id_ads', '')),
})

class SubmissionPreview extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      ads: null,
    }
    this._getAds = this._getAds.bind(this)
    this._refreshAds = this._refreshAds.bind(this)
    this._onRequestUpgrade = this._onRequestUpgrade.bind(this)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._getAds)
  }

  _getAds = () => {
    const {
      id_ads,
      dispatch,
      userData: { id_user },
    } = this.props
    dispatch(getAdsDetail, { id_ads, id_user }).then(async (ads) => {
      ads.additional = JSON.parse(ads.additional_data)
      let url = get(ads, 'additional.submission_reference')
      this.setState((state, props) => {
        return { ads: { ...(state.ads || {}), ...omit(ads, 'additional_data') } }
      })
      try {
        if (url) {
          if (url.substring(0, 4) != 'http') url = `http://${url}`
          const { data: html } = await axios.get(url)
          const root = parse(html)
          const entities = new Entities()
          ads.meta = {
            url,
            title: entities.decode(
              root.querySelector('title').innerHTML.toString().trim(),
            ),
          }
          const meta = root
            .querySelector('meta')
            .parentNode.childNodes.map((meta) => meta.outerHTML)
            .filter((meta) => !isUndefined(meta) && meta.indexOf('og:image') >= 0)
          if (meta.length > 0) {
            ads.meta.image = parse(meta[0])
              .querySelector('meta')
              .getAttribute('content')
          } else {
            const link = root
              .querySelector('link')
              .parentNode.childNodes.map((meta) => meta.outerHTML)
              .filter(
                (link) =>
                  !isUndefined(link) &&
                  (link.indexOf('rel="icon') >= 0 ||
                    link.indexOf('rel="shortcut') >= 0 ||
                    link.indexOf('apple-touch-icon-precomposed') >= 0),
              )
            if (link.length > 0) {
              ads.meta.image = parse(link.pop())
                .querySelector('link')
                .getAttribute('href')
            }
          }
        }
      } catch (e) {
        ads.meta = {
          url,
          title: url,
          image: null,
        }
      } finally {
        this.setState((state, props) => {
          return { ads: omit(ads, 'additional_data') }
        })
      }
    })
  };

  _refreshAds = () => {
    !!this.state.ads && this._getAds()
  };

  _onRequestUpgrade = () => {
    const { navigateTo, paymentSourceSet } = this.props
    paymentSourceSet('Submission Detail')
    navigateTo('MembershipScreen')
  };

  render() {
    const { ads } = this.state
    const { userData, navigateBack, navigateTo } = this.props
    return (
      <View style={styles.container}>
        <NavigationEvents onDidFocus={this._refreshAds} />
        <SubmissionDetail
          onRequestUpgrade={this._onRequestUpgrade}
          navigateTo={navigateTo}
          navigateBack={navigateBack}
          userData={userData}
          ads={ads}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SubmissionPreview),
  mapFromNavigationParam,
)
