import React from 'react'
import { View, StyleSheet } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { LinearGradient } from 'expo-linear-gradient'
import { isEmpty, upperFirst, get } from 'lodash-es'
import { WP1, WP105, WP2, WP35, WP6, WP4, WP30, WP50, WP5, WP20, WP05 } from 'sf-constants/Sizes'
import Image from 'sf-components/Image'
import { SHADOW_STYLE } from 'sf-constants/Styles'
import Text from 'sf-components/Text'
import { TOMATO, WHITE, GUN_METAL, SHIP_GREY_CALM, SKELETON_COLOR, SKELETON_HIGHLIGHT, PALE_GREY, SHIP_GREY } from 'sf-constants/Colors'
import { daysRemaining, toNormalDate } from '../../../utils/date'
import { Icon } from '../../../components'
import { PALE_LIGHT_BLUE_TWO, PALE_WHITE, REDDISH, RED_20, VERY_LIGHT_PINK_TWO,ORANGE20,GREEN30 } from '../../../constants/Colors'

const styles = StyleSheet.create({
  container: {
    flex: 1, flexDirection: 'row', alignItems: 'flex-start', paddingTop: WP4, borderBottomColor: PALE_GREY, paddingRight: WP4
  },
  imageWrapper: {
    width: WP30, height: WP30, borderRadius: 6, ...SHADOW_STYLE['shadowThin']
  }
})

const SubmissionItemV2 = ({ ads, loading, line, expiredSection = false }) => {

  const additionalData = loading ? {} : JSON.parse(ads.additional_data),
    isEvent = !loading && ads?.category == 'event',
    location = loading ? 'Lokasi Event / Audition' : additionalData?.location?.name,
    title = loading ? 'Judul Event / Audition' : ads?.title,
    subtitle = loading ? '' : isEvent ? `${toNormalDate(additionalData?.date?.start)} - ${toNormalDate(additionalData?.date?.end)}` : upperFirst(get(additionalData, 'benefit.0')),
    user_submission = loading ? '' : ads.user_submission?.shortlist==1 ? `Confirmed` : ads.user_submission?.shortlist==0 ? 'Waiting' : null,
    label = loading ? '' : isEvent ? 'Events' : ads?.day_left == 0 ? 'Hari Terakhir' : ads?.day_left <= 7 ? `${ads?.day_left} hari lagi` : null,
    labelColor = loading ? [] : isEvent ? ['rgb(20, 158, 142)', 'rgb(66, 223, 144)'] : [TOMATO, TOMATO],
    isExpired = daysRemaining(additionalData?.date?.end, false) < 0,
    labelAds = expiredSection ? null : isExpired && ads?.day_left == 0 ? null : label,
    labelBottom = isExpired && !expiredSection ? 'Telah Berakhir' : null,
    labelBottomColor = [PALE_LIGHT_BLUE_TWO, PALE_LIGHT_BLUE_TWO]

  return (
    <View style={[styles.container, { paddingBottom: line ? WP4 : 0, borderBottomWidth: line ? 1 : 0, }]}>
      <View style={{ marginRight: WP4 }}>
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[styles.imageWrapper]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <View style={{ borderRadius: 6, overflow: 'hidden' }}>
            <Image
              source={{ uri: ads.image }}
              imageStyle={{ width: WP30, aspectRatio: 1 }}
            />
            {
              expiredSection && (
                <View
                  style={{
                    position: 'absolute',
                    alignSelf: 'center',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'rgba(255, 255, 255,0.5)',
                  }}
                />
              )
            }
            {
              !isEmpty(labelAds) && (
                <View
                  style={{
                    position: 'absolute',
                    alignSelf: 'center',
                    top: 0,
                    left: 0,
                  }}
                >
                  <View
                    style={{
                      overflow: 'hidden',
                      borderBottomRightRadius: 6
                    }}
                  >
                    <LinearGradient
                      colors={labelColor}
                      start={[1, 0]} end={[0, 0]}
                      style={{
                        paddingHorizontal: WP105,
                        paddingVertical: WP1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Text type='Circular' size='tiny' color={WHITE} weight={500} centered>{label}</Text>
                    </LinearGradient>
                  </View>
                </View>
              )
            }

            {
              labelBottom && (
                <View
                  style={{
                    position: 'absolute',
                    alignSelf: 'center',
                    bottom: 0,
                    left: 0,
                  }}
                >
                  <View
                    style={{
                      overflow: 'hidden',
                      borderTopEndRadius: 6
                    }}
                  >
                    <LinearGradient
                      colors={labelBottomColor}
                      start={[1, 0]} end={[0, 0]}
                      style={{
                        paddingHorizontal: WP105,
                        paddingVertical: WP1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Text type='Circular' size='tiny' color={PALE_WHITE} weight={500} centered>{labelBottom}</Text>
                    </LinearGradient>
                  </View>
                </View>
              )
            }
          </View>
        </SkeletonContent>
      </View>
      <SkeletonContent
        containerStyle={{ flex: 1 }}
        layout={[
          { width: WP20, height: WP4, marginBottom: WP105 },
          { width: WP50, height: WP5, marginBottom: WP105 },
          { width: WP35, height: WP5, marginBottom: WP2 },
          { width: WP50, height: WP4, },
        ]}
        isLoading={loading}
        boneColor={SKELETON_COLOR}
        highlightColor={SKELETON_HIGHLIGHT}
      >
        <View>
          <Text numberOfLines={1} type='Circular' color={SHIP_GREY_CALM} size='xmini' weight={300} style={{ marginBottom: WP1 }}>{upperFirst(location)}</Text>
          <Text numberOfLines={2} type='Circular' color={GUN_METAL} size='mini' weight={500} style={{ lineHeight: WP6, marginBottom: WP105 }}>{upperFirst(title)}</Text>
          {
            !expiredSection ?
              (
                <View style={{ flexWrap: 'wrap', flexDirection: 'row', alignItems: isEvent ? 'center' : 'flex-start' }}>
                  {
                    isEvent ?
                      (
                        <Icon
                          type='MaterialCommunityIcons'
                          name='calendar-today'
                          color={REDDISH}
                          size='slight'
                          centered
                          style={{ marginTop: WP05 }}
                        />
                      ) : (
                        <Image
                          source={require('sf-assets/icons/icMedali.png')}
                          size='xtiny'
                          centered={false}
                          style={{ marginTop: WP05 }}
                        />
                      )
                  }
                  <Text numberOfLines={2} type='Circular' color={SHIP_GREY} size='xmini' weight={300} style={{ marginLeft: WP1, flex: 1 }}>{subtitle}</Text>
                
                </View>
              ) : (
                <View>
                  <View style={{ alignSelf: 'flex-start', backgroundColor: VERY_LIGHT_PINK_TWO, borderRadius: 6, paddingVertical: WP1, paddingHorizontal: WP105 }}>
                    <Text numberOfLines={1} type='Circular' color={RED_20} size='xmini' weight={400}>Telah Berakhir</Text>
                  </View>
                </View>
              )
          }
        </View>
        {user_submission=='Confirmed' ?
            <View style={{ alignSelf: 'flex-start', backgroundColor: GREEN30, borderRadius: 6, paddingVertical: WP1, paddingHorizontal: WP105 }}>
              <Text numberOfLines={1} type='Circular' color={PALE_WHITE} size='xmini' weight={400}>{user_submission}</Text>
            </View>
            :
            user_submission=='Waiting' ?
            <View style={{ alignSelf: 'flex-start', backgroundColor: ORANGE20, borderRadius: 6, paddingVertical: WP1, paddingHorizontal: WP105 }}>
              <Text numberOfLines={1} type='Circular' color={PALE_WHITE} size='xmini' weight={400}>{user_submission}</Text>
            </View>
            : null }
      </SkeletonContent>
    </View>
  )
}

SubmissionItemV2.propTypes = {}

SubmissionItemV2.defaultProps = {}

export default SubmissionItemV2
