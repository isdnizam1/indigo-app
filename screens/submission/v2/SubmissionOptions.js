import { ORANGE_BRIGHT } from '../../../constants/Colors'
import { FONTS } from '../../../constants/Fonts'

export const SUBMISSION_OPTIONS = (props) => {
  let menu = [
    {
      type: 'menu',
      image: require('../../../assets/icons/badgeSoundfrenPremium.png'),
      imageStyle: {},
      name: 'Premium',
      textStyle: { color: ORANGE_BRIGHT, fontFamily: FONTS.Circular['500'] },
      onPress: () => { },
      joinButton: false,
      isPremiumMenu: true
    },
    {
      type: 'menu',
      image: require('../../../assets/icons/icPlusShipGrey.png'),
      imageStyle: {},
      name: 'Buat Audition',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('CreateSubmissionDetailForm')
      }
    },
    {
      type: 'menu',
      image: require('../../../assets/icons/icAboutShipGrey.png'),
      imageStyle: {},
      name: 'Tentang Submission',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('AboutFeatureScreen', {
          content: {
            image: require('../../../assets/images/bgAboutSubmission.png'),
            imageRatio: 360 / 198,
            title: 'Submission',
            subtitle: 'Beragam kegiatan untuk aktivitas bermusik kamu ada disini',
            header: 'Tentang Submission',
            paragraph: [
              'Soundfren menghadirkan berbagai event dan audisi yang memungkinkan untuk mengembangkan pengalaman bermusikmu. Untuk mengikuti audisi ini, kamu hanya perlu untuk mendaftarkan diri dengan mengelengkapi profilmu dan mengikuti instruksi yang diberikan pada setiap audisi.',
              'Audisi yang pernah ada di Soundfren mencakupi Archipelago Festival, Liga Musik Kampus, Useetv Indie Air dan akan bertambah terus audisi lainya, tentunya kamu dapat mengikuti audisi ini dengan mudah melalui Soundfren!',
            ],
            route: 'Submission',
            buttonTitle: 'Jelajahi Submission'
          }
        })
      }
    },
    {
      type: 'menu',
      image: require('../../../assets/icons/mdi_chat.png'),
      imageStyle: {},
      name: 'Feedback & Report',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('FeedbackScreen')
      }
    },
    {
      type: 'separator'
    },
    {
      type: 'menu',
      image: require('../../../assets/icons/close.png'),
      imageStyle: {},
      name: 'Tutup',
      textStyle: {},
      onPress: props.onClose
    },
  ]
  if (props.onShare) menu.splice(1, 0, {
    type: 'menu',
    image: require('../../../assets/icons/mdi_share.png'),
    imageStyle: {},
    name: 'Share',
    textStyle: {},
    onPress: () => {
      props.onClose()
      props.onShare()
    }
  })
  return {
    withPromoteUser: true,
    menus: menu
  }
}

export default SUBMISSION_OPTIONS
