import React, { Component, Fragment } from 'react'
import {
  View,
  TouchableOpacity,
  Animated as RNAnimated,
  StatusBar as StatusBarRN,
  SafeAreaView,
  FlatList,
  Platform,
} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import SkeletonContent from 'react-native-skeleton-content'
import * as Animatable from 'react-native-animatable'
import { isEmpty, noop } from 'lodash-es'
import Touchable from 'sf-components/Touchable'
import {
  GUN_METAL,
  NAVY_DARK,
  NO_COLOR,
  PALE_BLUE,
  PALE_GREY,
  PALE_GREY_TWO,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
  SHADOW_GRADIENT,
} from 'sf-constants/Colors'
import { Container, Icon, Image, Text, Modal, HeaderNormal } from 'sf-components'
import {
  WP100,
  WP12,
  WP15,
  WP2,
  WP3,
  WP4,
  WP5,
  WP50,
  WP6,
  WP70,
  WP1,
  WP8,
  HP50,
} from "sf-constants/Sizes";
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import { getAdsSubmission, getAdsSubmissionLocation } from 'sf-actions/api'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import SoundfrenExploreOptions from 'sf-components/explore/SoundfrenExploreOptions'
import { getStatusBarHeight } from 'sf-utils/iphoneXHelper'
import { setDataSubmission } from 'sf-services/screens/lv1/submission/actionDispatcher'
import { LinearGradient } from 'expo-linear-gradient'
import Animated from 'react-native-reanimated'
import ModalV2 from '../../../components/ModalV2'
import style from '../../profile/v2/ProfileScreen/style'
import { SHADOW_STYLE } from '../../../constants/Styles'
import { restrictedAction } from '../../../utils/helper'
import { SUBMISSION_OPTIONS } from './SubmissionOptions'
import SubmissionItemV2 from './SubmissionItemV2'
import EmptyState from "../../../components/EmptyState";

const StatusBar = Animatable.createAnimatableComponent(StatusBarRN)
const HEADER_HEIGHT = WP12
const BANNER_SIZE = (WP100 * 255) / 360

const mapStateToProps = ({
  auth,
  screen: {
    lv1: { submission },
  },
}) => ({
  userData: auth.user,
  submission,
})
const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
})

class Submission extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: true,
      location: 'Semua Kota',
      locationList: [],
      locationPosition: 'bottom',
      statusBarTheme: 'light',
      headerOpacity: new RNAnimated.Value(0),
    }
    this.y = 0
  }

  async componentDidMount() {
    await this._getLocation()
    await this._getContent('')
  }

  _getLocation = async () => {
    const { dispatch } = this.props
    try {
      const { result } = await dispatch(
        getAdsSubmissionLocation,
        {},
        noop,
        true,
        false,
      )
      this.setState({
        locationList: ['Semua Kota', ...result],
      })
    } catch (error) {
      //
    }
  };

  _getContent = async (
    location = '',
    params = { start: 0, limit: 10 },
    loadMore = false,
    isLoading = false,
  ) => {
    const { dispatch, submissionm,userData } = this.props

    try {
      const { result } = await dispatch(
        getAdsSubmission,
        {
          status: 'active',
          id_user: userData.id_user,
          location_name: location == 'Semua Kota' ? '' : location,
          ...params,
        },
        noop,
        true,
        isLoading,
      )
      if (!loadMore) {
        this.props.setDataSubmission({
          about: result.about,
          event_audition: result.event_audition,
          prev_event_audition: result.prev_event_audition,
          isLoaded: true,
        })
      } else {
        const mergeData = [...submission.event_audition, ...result.event_audition]
        this.props.setDataSubmission({ event_audition: mergeData, isLoaded: true })
      }
    } catch (e) {
      this.props.setDataSubmission({ isLoaded: true })
    }
  };

  _onChangeLocation = (value) => {
    this.setState(
      {
        location: value,
      },
      () => {
        this._getContent(value)
        this.y = 0
      },
    )
  };

  _headerSearch = () => {
    const { navigateBack } = this.props
    const bgColor = NO_COLOR
    const iconColor = WHITE
    const barStyle = 'dark'
    return (
      <View
        style={{
          width: WP100,
          height: HEADER_HEIGHT,
          position: 'absolute',
          zIndex: 999,
          top: 0,
        }}
      >
        <StatusBar
          animated
          translucent={true}
          backgroundColor={bgColor}
          barStyle={`${barStyle}-content`}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: bgColor,
            paddingTop: getStatusBarHeight(true),
          }}
        >
          <Touchable
            onPress={() => navigateBack()}
            style={{ padding: WP2, paddingHorizontal: WP4 }}
          >
            <Icon
              centered
              background='dark-circle'
              backgroundColor='rgba(255,255,255, 0.16)'
              size='huge'
              color={iconColor}
              name='chevron-left'
              type='Entypo'
            />
          </Touchable>
          <View style={{ paddingVertical: WP2 }} />
          {/* <Icon
            centered
            background='dark-circle'
            size='huge'
            style={{ marginRight: WP5 }}
            color={iconColor}
            name='dots-three-horizontal'
            type='Entypo'
          /> */}
        </View>
      </View>
    )
  };


  _headerSection = () => {
    const {
      submission: { about, isLoaded },
      navigateTo,
      navigation,
      userData,
    } = this.props
    const isLoading = !isLoaded
    const title = 'Submission'
    const subtitle = 'Semua aktivitas dan program dari Event \n kamu ada disini'
    return (
      <View>
        <View style={{ height: BANNER_SIZE, width: WP100 }}>
          <Image
            tint={'black'}
            imageStyle={{ width: WP100, height: undefined }}
            aspectRatio={360 / 255}
            source={require('sf-assets/images/bgSubmissionSquare.png')}
          />
          <View
            style={{
              position: 'absolute',
              left: 0,
              top: getStatusBarHeight(true),
              right: 0,
              bottom: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <SkeletonContent
              containerStyle={{ alignItems: 'center', paddingHorizontal: WP12 }}
              layout={[
                { width: WP50, height: WP6, marginBottom: WP2 },
                { width: WP70, height: WP4, marginBottom: WP6 },
                { width: WP50, height: WP8, borderRadius: 6 },
              ]}
              isLoading={isLoading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View style={{ alignItems: 'center' }}>
                <Text
                  type='Circular'
                  size='large'
                  weight={600}
                  color={WHITE}
                  centered
                  style={{ marginBottom: WP2 }}
                >
                  {title}
                </Text>
                <Text
                  type='Circular'
                  size='mini'
                  weight={300}
                  color={WHITE}
                  centered
                  style={{ lineHeight: WP6 }}
                >
                  {subtitle}
                </Text>
                {/* <Touchable
                  activeOpacity={TOUCH_OPACITY}
                  onPress={restrictedAction({
                    action: () => navigateTo('CreateSubmissionDetailForm'),
                    userData,
                    navigation,
                  })}
                  style={{
                    width: WP50,
                    backgroundColor: REDDISH,
                    borderRadius: 6,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: WP6,
                    paddingVertical: WP2,
                    ...SHADOW_STYLE['shadowThin'],
                  }}
                >
                  <Text
                    type='Circular'
                    size='mini'
                    weight={500}
                    color={WHITE}
                    centered
                  >
                    {'Buat Audition'}
                  </Text>
                </Touchable> */}
              </View>
            </SkeletonContent>
          </View>
        </View>
        {this._modalLocation(this._locationSection())}
      </View>
    )
  };

  _locationSection = () => {
    return (
      <View
        pointerEvents='none'
        style={{
          width: '100%',
          backgroundColor: PALE_GREY_TWO,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: WP4,
          paddingTop: WP4,
          paddingBottom: WP3,
          borderBottomColor: PALE_BLUE,
          borderBottomWidth: 1,
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
          <Icon
            centered
            background='dark-circle'
            size='small'
            color={REDDISH}
            name='location-on'
            type='MaterialIcons'
          />
          <Text numberOfLines={1} style={{ flex: 1 }}>
            <Text type='Circular' size='mini' weight={300} color={SHIP_GREY}>
              Cari di:
            </Text>
            <Text type='Circular' size='mini' weight={300} color={REDDISH}>{` ${
              isEmpty(this.state.location) ? 'Semua Kota' : this.state.location
            }`}</Text>
          </Text>
        </View>
        <Text type='Circular' size='xmini' weight={400} color={REDDISH}>
          Edit
        </Text>
      </View>
    )
  };

  _modalLocation = (triggercomponent) => {
    return (
      <Modal
        style={{
          flex: 1,
          borderTopRightRadius: 0,
          borderTopLeftRadius: 0,
        }}
        renderModalContent={({ toggleModal }) => {
          return (
            <SafeAreaView style={{ flex: 1, backgroundColor: WHITE }}>
              <View
                style={{
                  backgroundColor: PALE_GREY_TWO,
                  height: '100%',
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingHorizontal: WP4,
                    paddingVertical: WP2,
                    backgroundColor: WHITE,
                    borderBottomColor: PALE_BLUE,
                    borderBottomWidth: 1,
                  }}
                >
                  <View style={{ width: WP15 }}>
                    <Icon
                      onPress={toggleModal}
                      background='dark-circle'
                      size='huge'
                      color={SHIP_GREY_CALM}
                      name='chevron-left'
                      type='Entypo'
                    />
                  </View>
                  <Text
                    type='Circular'
                    color={SHIP_GREY}
                    size='mini'
                    weight={400}
                    style={{ lineHeight: WP5 }}
                    centered
                  >
                    Filter Kota/Lokasi
                  </Text>
                  <View style={{ width: WP15 }} />
                </View>

                <FlatList
                  style={{ flex: 1 }}
                  contentContainerStyle={{ paddingTop: WP6, paddingHorizontal: WP4 }}
                  ListHeaderComponent={() => (
                    <View style={{ paddingBottom: WP3 }}>
                      <Text
                        type='Circular'
                        color={NAVY_DARK}
                        size='small'
                        weight={600}
                      >
                        Kota/Lokasi tersedia
                      </Text>
                    </View>
                  )}
                  data={this.state.locationList}
                  renderItem={({ item, index }) => (
                    <TouchableOpacity
                      onPress={() => {
                        this._onChangeLocation(item)
                        toggleModal()
                      }}
                      style={{
                        width: '100%',
                        paddingVertical: WP2,
                        borderBottomColor: PALE_GREY,
                        borderBottomWidth: 1,
                      }}
                    >
                      <Text
                        type='Circular'
                        color={this.state.location == item ? REDDISH : GUN_METAL}
                        size='mini'
                        weight={this.state.location == item ? 400 : 300}
                      >
                        {item}
                      </Text>
                    </TouchableOpacity>
                  )}
                />
              </View>
            </SafeAreaView>
          )
        }}
      >
        {({ toggleModal }, M) => (
          <Fragment>
            <TouchableOpacity onPress={toggleModal} activeOpacity={0.5}>
              <View pointerEvents='none'>{triggercomponent}</View>
            </TouchableOpacity>
            {M}
          </Fragment>
        )}
      </Modal>
    )
  };

  _submissionSection = (section) => {
    const {
      submission: { isLoaded },
      submission,
      userData,
      navigation,
    } = this.props
    const isLoading = !isLoaded
    const { navigateTo } = this.props
    const dummyData = [1, 2, 3, 4, 5]
    const data = submission[section]
    const dataList = isLoading ? dummyData : !isEmpty(data) ? data : []
    const isPrevSection = section == 'prev_event_audition'
    return (
      <>
        {isPrevSection && (
          <View
            style={{
              borderTopWidth: 1,
              borderTopColor: PALE_BLUE,
              marginTop: WP6,
              paddingTop: WP6,
              paddingHorizontal: WP4,
            }}
          >
            <Text type="Circular" size="small" color={NAVY_DARK} weight={600}>
              {"Submission Sebelumnya"}
            </Text>
          </View>
        )}
        {!isEmpty(dataList) || isLoading ? (
          <View
            style={{
              flexGrow: 0,
              paddingLeft: WP4,
              paddingTop: isPrevSection ? 0 : WP4,
            }}
          >
            {dataList.map((item, index) => {
              return (
                <TouchableOpacity
                  disabled={isLoading}
                  key={`${index}${new Date()}`}
                  activeOpacity={TOUCH_OPACITY}
                  onPress={() => {
                    const { id_ads } = item;
                    restrictedAction({
                      action: () => navigateTo("SubmissionPreview", { id_ads }),
                      userData,
                      navigation,
                    })();
                  }}
                >
                  <SubmissionItemV2
                    ads={item}
                    loading={isLoading}
                    line={index < dataList.length - 1}
                    expiredSection={isPrevSection}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
        ) : (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              height: HP50,
            }}
          >
            <EmptyState
              image={require(`sf-assets/icons/ic_submission_emptystate.png`)}
              title={"Belum ada submission saat ini"}
              message={"Nantikan submission untuk kamu"}
            />
          </View>
        )}
      </>
    );
  };

  _onScroll = ({
    nativeEvent: {
      contentOffset: { y: scrollPosition },
    },
  }) => {
    const { statusBarTheme: theme, headerOpacity } = this.state
    if (scrollPosition > 15 && theme === 'light') {
      RNAnimated.timing(headerOpacity, {
        toValue: 1,
        useNativeDriver: true,
        duration: 250,
        delay: 0,
        isInteraction: false,
      }).start()
      this.setState({
        statusBarTheme: 'dark',
      })
    }
    if (scrollPosition <= 15 && theme === 'dark') {
      RNAnimated.timing(headerOpacity, {
        toValue: 0,
        useNativeDriver: true,
        duration: 250,
        delay: 0,
        isInteraction: false,
      }).start()
      this.setState({
        statusBarTheme: 'light',
      })
    }
  };

  render() {
    const { isReady, location } = this.state
    const {
      submission: { event_audition },
      userData,
      navigation,
    } = this.props
    return (
      <Container
        isReady={isReady}
        isLoading={false}
        noStatusBarPadding
        statusBarBackground={NO_COLOR}
      >
        <Animated.ScrollView
          onScroll={this._onScroll}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        >
          <View style={{ flex: 1 }}>
            <NavigationEvents onWillFocus={() => this._getContent(location)} />
            {this._headerSearch()}
            {this._headerSection()}
            <Container
              noStatusBarPadding
              scrollable
              scrollBackgroundColor={WHITE}
              onPullDownToRefresh={() => this._getContent(location)}
              onScrollViewRef={(ref) => (this.scrollVertical = ref)}
              scrollEventThrottle={16}
              scrollBottomPadding={Platform.OS == 'ios' ? HEADER_HEIGHT : 0}
              scrollContentContainerStyle={{ paddingBottom: WP4 }}
              onPullUpToLoad={
                event_audition?.length > 0
                  ? async () => {
                      await this._getContent(
                        location,
                        { start: event_audition?.length, limit: 10 },
                        true,
                        false,
                      )
                    }
                  : null
              }
            >
              {this._submissionSection('event_audition')}
              {this._submissionSection('prev_event_audition')}
            </Container>
          </View>
        </Animated.ScrollView>
        <RNAnimated.View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            backgroundColor: WHITE,
            paddingTop: getStatusBarHeight(true),
            opacity: this.state.headerOpacity,
            paddingHorizontal: WP1,
          }}
        >
          <HeaderNormal
            withExtraPadding
            iconLeftOnPress={this.props.navigateBack}
            centered
            textType={'Circular'}
            textSize={'slight'}
            text={'Submission'}
            rightComponent={() => {
              return (
                <ModalV2
                  renderModalContent={({ toggleModal }) => {
                    return (
                      <SoundfrenExploreOptions
                        menuOptions={SUBMISSION_OPTIONS}
                        userData={this.props.userData}
                        navigateTo={this.props.navigateTo}
                        onClose={toggleModal}
                      />
                    )
                  }}
                >
                  {({ toggleModal }, M) => (
                    <Fragment>
                      <Touchable
                        onPress={restrictedAction({
                          action: () => navigateTo('CreateSubmissionDetailForm'),
                          userData,
                          navigation,
                        })}
                        style={{ padding: WP2, paddingHorizontal: WP4 }}
                      >
                        <Icon
                          centered
                          background='dark-circle'
                          size='huge'
                          color={SHIP_GREY}
                          name='dots-three-horizontal'
                          type='Entypo'
                        />
                      </Touchable>
                      {M}
                    </Fragment>
                  )}
                </ModalV2>
              )
            }}
          />
          <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
        </RNAnimated.View>
        <StatusBarRN
          animated
          translucent={true}
          barStyle={`${this.state.statusBarTheme}-content`}
        />
      </Container>
    )
  }
}

Submission.propTypes = {
  navigateTo: PropTypes.func,
}

Submission.defaultProps = {
  navigateTo: () => {},
}

export default _enhancedNavigation(
  connect(mapStateToProps, { setDataSubmission })(Submission),
  mapFromNavigationParam,
)
