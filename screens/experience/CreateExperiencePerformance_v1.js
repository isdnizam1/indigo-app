import React from 'react'
import { isEmpty, map, pick } from 'lodash-es'
import { BackHandler, Image, ImageBackground, Platform, StatusBar, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import { LinearGradient } from 'expo-linear-gradient'
import { _enhancedNavigation, Container, Form, Icon, InputDate, InputModal, InputTextLight, ListItem, Modal, Text } from '../../components'
import { GREY, SILVER_CALM, WHITE, WHITE_MILK } from '../../constants/Colors'
import { ExperienceSteps } from '../../components/StepCircle'
import { BORDER_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { getCompany, getJob, postPerformanceJourney } from '../../actions/api'
import { FONT_SIZE, HP1, WP1, WP10, WP100, WP2, WP24, WP4, WP5 } from '../../constants/Sizes'
import { invalidButtonStyle, validButtonStyle } from '../../utils/helper'
import HeaderNormal from '../../components/HeaderNormal'
import RequiredMark from '../../components/RequiredMark'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  refreshProfile: getParam('refreshProfile', () => {
  })
})

class CreateExperiencePerformance extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  state = {
    isRefreshing: false,
    images: [],
    imagesName: [],
    imagesBase64: []
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  _selectPhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCameraRoll === 'granted') {
      return await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
    }
  }

  _takePhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
      return await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
    }
  }

  _deleteImage = (index) => {
    const {
      images,
      imagesBase64
    } = this.state
    images.splice(index, 1)
    imagesBase64.splice(index, 1)
    this.setState({
      images,
      imagesBase64
    })
  }

  _handlePhoto = async (result) => {
    if (Platform.OS === 'ios') StatusBar.setHidden(false)
    const {
      uri,
      base64,
      cancelled
    } = result

    if (!result) {
      return
    }

    if (!cancelled) this.setState((state) => {
      let name = result.uri.split('/')
      name = name[name.length - 1]
      return ({
        images: [...state.images, uri],
        imagesBase64: [...state.imagesBase64, base64],
        imagesName: [...state.imagesName, name]
      })
    })
  }

  _onPerformanceJourney = () => {
    const {
      userData,
      dispatch,
      refreshProfile,
      navigateTo
    } = this.props
    const formMusicJourney = pick(this.state, [
      'title', 'companyName', 'date', 'description', 'imagesBase64', 'jobTitle', 'city'
    ])
    dispatch(postPerformanceJourney, {
      id_user: userData.id_user,
      title: formMusicJourney.title,
      company: formMusicJourney.companyName,
      event_date: formMusicJourney.date,
      description: formMusicJourney.description,
      media: formMusicJourney.imagesBase64,
      role: formMusicJourney.jobTitle,
      event_location: formMusicJourney.city
    }, null, true)
      .then(() => {
        navigateTo('CreateExperienceFinish', { experienceType: 'performance', refreshProfile })
      })
  }

  _isValid = (state = this.state) => (
    state.title
    && state.jobTitle
    && state.companyName
    && state.date
    && state.city
    && state.description
    && !isEmpty(state.images)
  )

  render() {
    const {
      isLoading
    } = this.props
    const {
      images
    } = this.state
    const isValidInput = this._isValid() ? validButtonStyle : invalidButtonStyle
    return (
      <Container
        scrollable
        borderedHeader
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._backHandler}
            text='Create Performance Experience'
            centered
          />
        )}
        outsideScrollContent={() => (
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            disabled={isValidInput.disabled}
            onPress={this._onPerformanceJourney}
          >
            <LinearGradient
              colors={isValidInput.buttonColor}
              start={[0, 0]} end={[1, 0]}
              style={{ width: WP100, padding: WP4, flexGrow: 0 }}
            >
              <Text color={WHITE} centered weight={500}>
                NEXT
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        )}
        outsideScrollContentTop={() => <ExperienceSteps style={{ ...BORDER_STYLE['bottom'] }} index={2}/>}
      >
        <Form
          onChangeInterceptor={({ key, text }) => this.setState({ [key]: text })}
        >
          {
            ({ onChange, formData }) => (
              <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
                <View style={{ paddingHorizontal: WP10, paddingVertical: WP2 }}>
                  <InputTextLight
                    required
                    label='Event Title'
                    value={formData.title}
                    size='mini'
                    placeholder='Javajazz Vol.2'
                    onChangeText={onChange('title')}
                  />
                  <InputModal
                    triggerComponent={(
                      <InputTextLight
                        required
                        bold
                        size='mini'
                        value={formData.jobTitle}
                        label='What is your role?'
                        placeholder='Vocalist'
                        editable={false}
                      />
                    )}
                    header='Select Role'
                    placeholder='Search role here...'
                    suggestion={getJob}
                    suggestionKey='job_title'
                    suggestionPathResult='job_title'
                    onChange={onChange('jobTitle')}
                    selected={formData.jobTitle}
                  />
                  <InputModal
                    triggerComponent={(
                      <InputTextLight
                        required
                        bold
                        size='mini'
                        value={formData.companyName}
                        label='Band/Artist/Company Name'
                        placeholder='Self Employed'
                        editable={false}
                      />
                    )}
                    header='Select Band/Artist/Company Name'
                    placeholder='Search here...'
                    suggestion={getCompany}
                    suggestionKey='company_name'
                    suggestionPathResult='company_name'
                    onChange={onChange('companyName')}
                    selected={formData.companyName}
                  />
                  <InputTextLight
                    required
                    label='Event Location'
                    value={formData.city}
                    size='mini'
                    placeholder='Mega Kuningan, DKI Jakarta'
                    onChangeText={onChange('city')}
                  />
                  <InputDate
                    required
                    size='mini'
                    value={formData.date}
                    dateFormat='DD/MM/YYYY'
                    mode='date'
                    placeholder='DD/MM/YYYY'
                    label='Event Date'
                    onChangeText={onChange('date')}
                  />
                  <InputTextLight
                    required
                    label='Tell us about this event!'
                    value={formData.description}
                    size='mini'
                    placeholder='Genjreng Band is a Jazz Pop band who loves to make covers. '
                    multiline
                    maxLength={160}
                    onChangeText={onChange('description')}
                  />
                  <View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text size={'mini'} color={GREY} weight={500}>Media</Text>
                      <RequiredMark/>
                    </View>
                    <Modal
                      renderModalContent={({ toggleModal }) => (
                        <View>
                          <ListItem
                            inline onPress={() => {
                              this._selectPhoto().then((result) => {
                                toggleModal()
                                this._handlePhoto(result)
                              })
                            }}
                          >
                            <Icon type='MaterialIcons' size='large' name='photo-library'/>
                            <Text style={{ paddingLeft: WP2 }}>Select from library</Text>
                          </ListItem>
                          <ListItem
                            inline onPress={() => {
                              this._takePhoto().then((result) => {
                                toggleModal()
                                this._handlePhoto(result)
                              })
                            }}
                          >
                            <Icon size='large' name='camera'/>
                            <Text style={{ paddingLeft: WP2 }}>Take a picture</Text>
                          </ListItem>
                        </View>
                      )}
                    >
                      {({ toggleModal }, M) => (
                        <View style={{ flexDirection: 'row', paddingVertical: HP1, flexWrap: 'wrap' }}>
                          {
                            map(images, (image, i) => (
                              <ImageBackground
                                key={`${i}${new Date()}`}
                                imageStyle={{ borderRadius: 5 }}
                                style={{
                                  alignItems: 'flex-end', backgroundColor: WHITE_MILK,
                                  width: WP24, height: undefined, aspectRatio: 1,
                                  marginVertical: WP1, marginHorizontal: WP1
                                }}
                                source={{ uri: image }}
                              >
                                <TouchableOpacity
                                  onPress={() => this._deleteImage(i)}
                                  activeOpacity={TOUCH_OPACITY}
                                  style={{
                                    margin: WP1,
                                    padding: WP1, width: FONT_SIZE['tiny'] + WP2, height: FONT_SIZE['tiny'] + WP2,
                                    borderRadius: WP100, backgroundColor: GREY
                                  }}
                                >
                                  <Icon size='tiny' color={WHITE} name='close'/>
                                </TouchableOpacity>
                              </ImageBackground>
                            ))
                          }
                          <TouchableOpacity
                            onPress={toggleModal}
                          >
                            <View style={{
                              width: WP24,
                              height: WP24,
                              marginVertical: WP1, marginHorizontal: WP1,
                              backgroundColor: SILVER_CALM,
                              borderRadius: 5,
                              justifyContent: 'center',
                              alignItems: 'center'
                            }}
                            >
                              <Image
                                source={require('../../assets/icons/iconCamera.png')}
                                style={{ width: WP5, height: undefined, aspectRatio: 222 / 183 }}
                              />
                            </View>
                          </TouchableOpacity>
                          {M}
                        </View>
                      )}
                    </Modal>
                  </View>
                </View>
              </KeyboardAwareScrollView>
            )
          }
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateExperiencePerformance),
  mapFromNavigationParam
)
