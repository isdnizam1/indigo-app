import React from 'react'
import { BackHandler, View } from 'react-native'
import { connect } from 'react-redux'
import { _enhancedNavigation, Container, Image, Text } from '../../components'
import { DEFAULT_PAGING } from '../../constants/Routes'
import { GREY_WARM } from '../../constants/Colors'
import { ExperienceSteps } from '../../components/StepCircle'
import { BORDER_STYLE } from '../../constants/Styles'
import { HP1, HP2, WP20, WP30 } from '../../constants/Sizes'
import HeaderNormal from '../../components/HeaderNormal'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  refreshProfile: getParam('refreshProfile', () => {})
})

class CreateExperience extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  state = {
    isReady: false,
    isRefreshing: false
  }

  async componentDidMount() {
    await this._getInitialScreenData()
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _getInitialScreenData = async (...args) => {
    await this._getData(...args)
    this.setState({
      isReady: true,
      isRefreshing: false
    })
  }

  _getData = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {}

  _backHandler = async () => {
    this.props.refreshProfile()
    this.props.navigateBack()
  }

  render() {
    const {
      isLoading,
      navigateTo,
      refreshProfile
    } = this.props
    const {
      isReady
    } = this.state
    return (
      <Container
        scrollable
        borderedHeader
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._backHandler}
            text='Add Experience'
            centered
          />
        )}
        outsideScrollContentTop={() => <ExperienceSteps style={{ ...BORDER_STYLE['bottom'] }} index={1}/>}
      >
        <View style={{ alignItems: 'center', paddingVertical: HP2, paddingHorizontal: WP20 }}>
          <Text weight={500} type='SansPro' size='large' centered>Which one do you need?</Text>
          <Text size='xtiny' color={GREY_WARM} centered>Tap the one you choose!</Text>
          <Image
            onPress={() => navigateTo('CreateExperienceMusic', { refreshProfile })}
            source={require('../../assets/images/ex_music.png')}
            style={{ marginBottom: HP1, marginTop: HP2 }}
            imageStyle={{ width: WP30, aspectRatio: 1 }}
          />
          <Text weight={500} type='SansPro' style={{ marginVertical: HP1 }} centered>Music Experience</Text>
          <Text size='tiny' centered>You are able to keep track your career journey in music industry!</Text>
          <Text size='large' type='SansPro' weight={500} style={{ marginTop: HP2 }}>or</Text>
          <Image
            onPress={() => navigateTo('CreateExperiencePerformance', { refreshProfile })}
            source={require('../../assets/images/ex_performace.png')}
            style={{ marginBottom: HP1, marginTop: HP2 }}
            imageStyle={{ width: WP30, aspectRatio: 1 }}
          />
          <Text weight={500} type='SansPro' style={{ marginVertical: HP1 }} centered>Performance Journey</Text>
          <Text size='tiny' centered>You are able to keep track and show your performance journey!</Text>
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateExperience),
  mapFromNavigationParam
)
