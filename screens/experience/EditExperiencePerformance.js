import React from 'react'
import { forEach, isEmpty, map, pick } from 'lodash-es'
import { View, TouchableOpacity, ImageBackground, Platform, StatusBar, Image, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import { BackHandler } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import {
  _enhancedNavigation,
  Container,
  Form,
  Header,
  Icon,
  InputTextLight,
  Text,
  InputModal,
  InputDate,
  ListItem, Modal
} from '../../components'
import { BLACK, GREY, SILVER_CALM, WHITE, WHITE_MILK, ORANGE_BRIGHT, GREY_BACKGROUND, PURE_RED } from '../../constants/Colors'
import { getCompany, getJob, postEditPerformanceExperiences, deleteJourney } from '../../actions/api'
import {
  FONT_SIZE,
  HP1,
  WP1,
  WP10,
  WP100,
  WP2,
  WP24,
  WP5
} from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { alertDelete } from '../../utils/alert'
import { fetchAsBlob, convertBlobToBase64 } from '../../utils/helper'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  refreshData: getParam('refreshData', () => { }),
  experience: getParam('experience', {})
})

class EditExperiencePerformance extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  state = {
    isRefreshing: false,
    images: [],
    imagesName: [],
    imagesBase64: []
  }

  async componentDidMount() {
    const {
      experience
    } = this.props
    const data = JSON.parse(experience.additional_data)
    if (!isEmpty(data.media)) {
      const images = []
      const imagesBase64 = []
      const imagesName = []
      forEach(data.media, async (media, i) => {
        const [name] = media.split('_').slice(-1)
        images.push(media)
        imagesName.push(name)
        imagesBase64.push((await fetchAsBlob(media).then(convertBlobToBase64)).replace('data:application/octet-stream;base64,', ''))
      })
      this.setState({
        imagesBase64,
        imagesName,
        images
      })
    }

    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  _selectPhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCameraRoll === 'granted') {
      return await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
    }
  }

  _takePhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
      return await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
    }
  }

  _deleteJourney = () => {
    alertDelete(this._delete)
  }

  _delete = () => {
    const {
      dispatch,
      refreshData,
      navigateBack,
      experience
    } = this.props
    dispatch(deleteJourney, experience.id_journey)
      .then(() => {
        refreshData()
        navigateBack()
      })
  }

  _deleteImage = (index) => {
    const {
      images,
      imagesBase64
    } = this.state
    images.splice(index, 1)
    imagesBase64.splice(index, 1)
    this.setState({
      images,
      imagesBase64
    })
  }

  _handlePhoto = async (result) => {
    if (Platform.OS === 'ios') StatusBar.setHidden(false)
    const {
      uri,
      base64,
      cancelled
    } = result

    if (!result) {
      return
    }

    if (!cancelled) this.setState((state) => {
      let name = result.uri.split('/')
      name = name[name.length - 1]
      return ({
        images: [...state.images, uri],
        imagesBase64: [...state.imagesBase64, base64],
        imagesName: [...state.imagesName, name]
      })
    })
  }

  _onPerformanceJourney = async ({ formData, isDirty }) => {
    const {
      userData,
      dispatch,
      refreshData,
      navigateBack,
      experience
    } = this.props
    const {
      imagesBase64
    } = this.state
    const formPerformanceJourney = pick(formData, ['title', 'companyName', 'date', 'description', 'imagesBase64', 'jobTitle', 'city'])
    dispatch(postEditPerformanceExperiences, {
      id_journey: experience.id_journey,
      id_user: userData.id_user,
      title: formPerformanceJourney.title,
      company: formPerformanceJourney.companyName,
      event_date: formPerformanceJourney.date,
      description: formPerformanceJourney.description,
      role: formPerformanceJourney.jobTitle,
      event_location: formPerformanceJourney.city,
      media: imagesBase64,
    }, null, true)
      .then(() => {
        refreshData()
        navigateBack()
      })
  }

  _isValid = (state = this.state) => (
    state.title
    && state.jobTitle
    && state.companyName
    && state.date
    && state.city
    && state.description
    && !isEmpty(this.state.images)
  )

  render() {
    const {
      isLoading,
      experience
    } = this.props
    const {
      images
    } = this.state
    if (!experience)return <View/>

    const additionalData = JSON.parse(experience.additional_data)
    return (
      <Container
        borderedHeader
        isLoading={isLoading}
      >
        <Form
          initialValue={{
            title: experience.title,
            jobTitle: additionalData ? additionalData.role : '',
            companyName: additionalData ? additionalData.company : '',
            city: additionalData ? additionalData.event_location : '',
            date: additionalData ? additionalData.event_date : '',
            description: experience.description
          }}
          onSubmit={this._onPerformanceJourney}
        >
          {
            ({ onChange, onSubmit, formData }) => (
              <View style={{ flex: 1, backgroundColor: GREY_BACKGROUND }}>
                <Header style={{ backgroundColor: WHITE }}>
                  <Icon
                    size='huge' name='chevron-left' type='Entypo' centered
                    onPress={this._backHandler} color={BLACK}
                  />
                  <Text size='large' type='SansPro' weight={500}>Edit Performance Journey</Text>
                  <TouchableOpacity
                    onPress={() => {
                      Keyboard.dismiss()
                      onSubmit()
                    }} activeOpacity={TOUCH_OPACITY}
                    disabled={!this._isValid(formData)}
                  >
                    <Text size='medium' color={ORANGE_BRIGHT}>Save</Text>
                  </TouchableOpacity>

                </Header>
                <KeyboardAwareScrollView
                  showsVerticalScrollIndicator={false}
                  keyboardShouldPersistTaps='handled'
                  style={{ marginTop: WP1, backgroundColor: WHITE }}
                >
                  <View style={{ paddingHorizontal: WP10, paddingVertical: WP2 }}>

                    <InputTextLight
                      label='Event Title'
                      value={formData.title}
                      size='mini'
                      placeholder='Javajazz Vol.2'
                      onChangeText={onChange('title')}
                    />
                    <InputModal
                      triggerComponent={(
                        <InputTextLight
                          bold
                          size='mini'
                          value={formData.jobTitle}
                          label='What is your role?'
                          placeholder='Vocalist'
                          editable={false}
                        />
                      )}
                      header='Select Role'
                      placeholder='Search role here...'
                      suggestion={getJob}
                      suggestionKey='job_title'
                      suggestionPathResult='job_title'
                      onChange={onChange('jobTitle')}
                      selected={formData.jobTitle}
                    />
                    <InputModal
                      triggerComponent={(
                        <InputTextLight
                          bold
                          size='mini'
                          value={formData.companyName}
                          label='Band/Artist/Company Name'
                          placeholder='Self Employed'
                          editable={false}
                        />
                      )}
                      header='Select Band/Artist/Company Name'
                      placeholder='Search here...'
                      suggestion={getCompany}
                      suggestionKey='company_name'
                      suggestionPathResult='company_name'
                      onChange={onChange('companyName')}
                      selected={formData.companyName}
                    />
                    <InputTextLight
                      label='Event Location'
                      value={formData.city}
                      size='mini'
                      placeholder='Mega Kuningan, DKI Jakarta'
                      onChangeText={onChange('city')}
                    />
                    <InputDate
                      size='mini'
                      value={formData.date}
                      dateFormat='DD/MM/YYYY'
                      mode='date'
                      placeholder='DD/MM/YYYY'
                      label='Event Date'
                      onChangeText={onChange('date')}
                    />
                    <InputTextLight
                      label='Tell us about this event!'
                      value={formData.description}
                      size='mini'
                      placeholder='Genjreng Band is a Jazz Pop band who loves to make covers. '
                      multiline
                      maxLength={160}
                      onChangeText={onChange('description')}
                    />
                    <View>
                      <Text size={'mini'} color={GREY} weight={500}>Media</Text>
                      <Modal
                        renderModalContent={({ toggleModal }) => (
                          <View>
                            <ListItem
                              inline onPress={() => {
                                this._selectPhoto().then((result) => {
                                  toggleModal()
                                  this._handlePhoto(result)
                                })
                              }}
                            >
                              <Icon type='MaterialIcons' size='large' name='photo-library' />
                              <Text style={{ paddingLeft: WP2 }}>Select from library</Text>
                            </ListItem>
                            <ListItem
                              inline onPress={() => {
                                this._takePhoto().then((result) => {
                                  toggleModal()
                                  this._handlePhoto(result)
                                })
                              }}
                            >
                              <Icon size='large' name='camera' />
                              <Text style={{ paddingLeft: WP2 }}>Take a picture</Text>
                            </ListItem>
                          </View>
                        )}
                      >
                        {({ toggleModal }, M) => (
                          <View style={{ flexDirection: 'row', paddingVertical: HP1, flexWrap: 'wrap' }}>
                            {
                              map(images, (image, i) => (
                                <ImageBackground
                                  key={`${i}${new Date()}`}
                                  imageStyle={{ borderRadius: 5 }}
                                  style={{
                                    alignItems: 'flex-end', backgroundColor: WHITE_MILK,
                                    width: WP24, height: undefined, aspectRatio: 1,
                                    marginVertical: WP1, marginHorizontal: WP1
                                  }}
                                  source={{ uri: image }}
                                >
                                  <TouchableOpacity
                                    onPress={() => this._deleteImage(i)}
                                    activeOpacity={TOUCH_OPACITY}
                                    style={{
                                      margin: WP1,
                                      padding: WP1, width: FONT_SIZE['tiny'] + WP2, height: FONT_SIZE['tiny'] + WP2,
                                      borderRadius: WP100, backgroundColor: GREY
                                    }}
                                  >
                                    <Icon size='tiny' color={WHITE} name='close' />
                                  </TouchableOpacity>
                                </ImageBackground>
                              ))
                            }
                            <TouchableOpacity
                              onPress={toggleModal}
                            >
                              <View style={{
                                width: WP24,
                                height: WP24,
                                marginVertical: WP1, marginHorizontal: WP1,
                                backgroundColor: SILVER_CALM,
                                borderRadius: 5,
                                justifyContent: 'center',
                                alignItems: 'center'
                              }}
                              >
                                <Image
                                  source={require('../../assets/icons/iconCamera.png')}
                                  style={{ width: WP5, height: undefined, aspectRatio: 222 / 183 }}
                                />
                              </View>
                            </TouchableOpacity>
                            {M}
                          </View>
                        )}
                      </Modal>
                    </View>
                  </View>

                  <TouchableOpacity
                    onPress={() => {
                      this._deleteJourney()
                    }} activeOpacity={TOUCH_OPACITY}
                  >
                    <Text size='large' style={{ margin: WP10, alignSelf: 'center' }} color={PURE_RED}>Delete Performance Journey</Text>
                  </TouchableOpacity>
                </KeyboardAwareScrollView>
              </View>
            )
          }
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(EditExperiencePerformance),
  mapFromNavigationParam
)
