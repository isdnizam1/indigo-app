import { Buffer } from 'buffer'
import React from 'react'
import { isEmpty, map, pick, isUndefined, isString } from 'lodash-es'
import { BackHandler, Image, Keyboard, Platform, ScrollView, StatusBar, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import { CheckBox } from 'react-native-elements'
import moment from 'moment'
import { _enhancedNavigation, Container, Form, InputModal, InputTextLight, Text } from 'sf-components'
import { GUN_METAL, ORANGE_BRIGHT, PALE_BLUE, PALE_GREY, PALE_LIGHT_BLUE_TWO, PALE_SALMON, PALE_WHITE, REDDISH, SHIP_GREY, SHIP_GREY_CALM, SILVER_TWO, WHITE, SHADOW_GRADIENT } from 'sf-constants/Colors'
import { SHADOW_STYLE, TEXT_INPUT_STYLE, TOUCH_OPACITY } from 'sf-constants/Styles'
import { getJob, postPerformanceJourney, postUpdatePerformanceJourney } from 'sf-actions/api'
import {
  HP1,
  HP2,
  WP1,
  WP10,
  WP100,
  WP15,
  WP2,
  WP205,
  WP24,
  WP25,
  WP3,
  WP305,
  WP308,
  WP4,
  WP5,
  WP6,
  WP8,
  WP12,
  WP105,
} from 'sf-constants/Sizes'
import HeaderNormal from 'sf-components/HeaderNormal'
import { FONTS } from 'sf-constants/Fonts'
import ButtonV2 from 'sf-components/ButtonV2'
import ListItem from 'sf-components/ListItem'
import Icon from 'sf-components/Icon'
import Modal from 'sf-components/Modal'
import SelectDate from 'sf-components/SelectDate'
import { bytesToSize } from 'sf-utils/helper'
import * as ImageManipulator from 'expo-image-manipulator'
import { HEADER } from 'sf-constants/Styles'
import { LinearGradient } from 'expo-linear-gradient'
import { setClearBlueMessage } from 'sf-services/messagebar/actionDispatcher'

import Spacer from 'sf-components/Spacer'

import Divider from 'sf-components/Divider'
import style from '../profile/v2/ProfileScreen/TabComponents/style'
import { setAddProfile } from '../../utils/review'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  setClearBlueMessage
}

const SOLO_PERFORMANCE = 'Solo Performance'

const styles = {
  section: {
    paddingHorizontal: WP4,
    paddingVertical: WP6,
    marginBottom: WP4,
    borderBottomColor: PALE_BLUE,
    borderBottomWidth: 1
  }
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  experience: getParam('experience', {
    additional_data: {
      media: []
    }
  }),
  refreshProfile: getParam('refreshProfile', () => {})
})

class CreateExperiencePerformance extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
      images: (props.experience.additional_data.media || []).map((uri) => ({ uri })),
      isUploading: false,
      event_date: new Date(),
      ...props.experience.additional_data,
      ...props.experience,
    }
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  _selectPhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCameraRoll === 'granted') {
      const { uri } = await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
      return await ImageManipulator.manipulateAsync(uri, [{
        resize: {
          width: 620,
          height: 620
        }
      }], {
        base64: true,
        compress: 0.9,
        format: ImageManipulator.SaveFormat.JPG
      })
    }
  }

  _takePhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
      const { uri } = await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
      return await ImageManipulator.manipulateAsync(uri, [{
        resize: {
          width: 620,
          height: 620
        }
      }], {
        base64: true,
        compress: 0.9,
        format: ImageManipulator.SaveFormat.JPG
      })
    }
  }

  _deleteImage = (index) => {
    const {
      images
    } = this.state
    images.splice(index, 1)
    this.setState({
      images
    })
  }

  _transformNewImage = (rawImage) => {
    const {
      uri,
      base64,
    } = rawImage

    const fileSize = Buffer.byteLength(base64, 'utf8')
    let name = rawImage.uri.split('/')
    name = name[name.length - 1]

    return {
      uri,
      base64,
      name,
      size: bytesToSize(fileSize)
    }
  }

  _editImage = (rawImage, index) => {
    const {
      images
    } = this.state

    const newImage = this._transformNewImage(rawImage)

    const newImages = [...images]
    newImages[index] = newImage

    this.setState({
      images: newImages
    })
  }

  _handlePhoto = async (result) => {
    if (Platform.OS === 'ios') StatusBar.setHidden(false)
    if (!result) {
      return
    }

    const newImage = this._transformNewImage(result)

    if (!result.cancelled) this.setState((state) => {
      return ({
        images: [...state.images, newImage]
      })
    })
  }

  _onPerformanceJourney = async () => {
    const {
      userData,
      dispatch,
      refreshProfile,
      navigateBack,
      experience: {
        id_journey
      }
    } = this.props
    const formMusicJourney = pick(this.state, [
      'title', 'company', 'event_date', 'description', 'images', 'role', 'event_location'
    ])

    this.setState({ isUploading: true })

    const body = {
      id_user: userData.id_user,
      title: formMusicJourney.title,
      company: formMusicJourney.company,
      event_date: moment(isString(formMusicJourney.event_date) ? new Date(formMusicJourney.event_date.replace(' ', 'T')) : formMusicJourney.event_date).format('MM/YYYY'),
      media: formMusicJourney.images.map((image) => (image.base64 || image.uri)),
      role: formMusicJourney.role,
      event_location: formMusicJourney.event_location
    }

    if(!isUndefined(id_journey)) body.id_journey = id_journey

    try {
      await dispatch(isUndefined(id_journey) ? postPerformanceJourney : postUpdatePerformanceJourney, body, null, true)
    } catch (e) {
      // keep silent
    } finally {
      this.props.setClearBlueMessage(isUndefined(id_journey) ? 'Experience berhasil ditambahkan' : 'Experience berhasil diperbarui')
      isEmpty(id_journey) && await setAddProfile()
      this.setState({ isUploading: false }, () => {
        navigateBack()
        refreshProfile()
      })
    }
  }

  _isValid = (state = this.state) => {
    return state.title
    && state.role
    && state.company
    && state.event_date
    && state.event_location
  }

  _renderUploadImageModal = (children) => {
    return (
      <Modal
        renderModalContent={({ toggleModal }) => (
          <View>
            <Spacer size={WP105} />
            <ListItem
              inline onPress={() => {
                this._selectPhoto().then((result) => {
                  toggleModal()
                  this._handlePhoto(result)
                })
              }}
              style={{ paddingVertical: WP305 }}
            >
              <Text type={'Circular'} centered style={{ paddingLeft: WP2 }}>Select from library</Text>
            </ListItem>
            <ListItem
              style={{ paddingVertical: WP305 }}
              inline onPress={() => {
                this._takePhoto().then((result) => {
                  toggleModal()
                  this._handlePhoto(result)
                })
              }}
            >
              <Text type={'Circular'} centered style={{ paddingLeft: WP2 }}>Take a picture</Text>
            </ListItem>
            <Spacer size={WP105} />
            <Divider />
            <ListItem
              inline
              onPress={toggleModal}
              style={{ paddingVertical: WP4 + WP1, paddingLeft: WP4 }}
            >
              <Icon centered type={'MaterialIcons'} size={'large'} name={'close'}/>
              <Text type={'Circular'} centered style={{ paddingLeft: WP2 }}>Tutup</Text>
            </ListItem>
          </View>
        )}
      >
        {children}
      </Modal>
    )
  }

  _renderEmptyMedia = ({ toggleModal }, M) => {
    return (
      <View style={{ alignItems: 'center', paddingHorizontal: WP10 }}>
        <Image
          style={{
            width: WP15,
            height: WP15,
            marginBottom: WP5,
          }}
          source={require('sf-assets/icons/v3/journeyArtWorkIcon.png')}
        />
        <View>
          <Text centered color={GUN_METAL} type={'Circular'} weight={500}>
            Upload foto bermusik
          </Text>
          <Text
            centered
            style={style.emptyStateDescription}
            size={'mini'}
            color={SHIP_GREY_CALM}
            type={'Circular'}
            weight={300}
          >
            Bagikan foto pengalamanmu bermain pada acara ini
          </Text>
        </View>
        <ButtonV2
          onPress={toggleModal}
          style={style.emptyStateCta}
          color={REDDISH}
          textColor={WHITE}
          textSize={'mini'}
          text={'Upload Foto'}
          disabled={this.state.isUploading}
        />
        {M}
      </View>
    )
  }

  _imageButtonAction = ({ icon, label, action }) => {
    return (
      <TouchableOpacity
        onPress={action}
        style={{ flexDirection: 'row', marginRight: WP4, alignItems: 'center' }}
        disabled={this.state.isUploading}
      >
        <Image source={icon} style={{ marginRight: WP1, width: WP4, aspectRatio: 1 }} />
        <Text type='Circular' size='xmini' color={SHIP_GREY_CALM}>{label}</Text>
      </TouchableOpacity>
    )
  }

  _renderMedia = ({ toggleModal }, M) => {
    const {
      images
    } = this.state

    return (
      <View style={{ paddingVertical: HP1 }}>
        {
          map(images, (image, i) => (
            <View style={{ marginBottom: WP2, ...SHADOW_STYLE.shadow, shadowOpacity: 0.1 }}>
              <View style={{ flexDirection: 'row', backgroundColor: WHITE, paddingVertical: WP3, paddingHorizontal: WP3 }}>
                <Image
                  source={{ uri: image.uri }}
                  style={{ width: WP24, aspectRatio: 1, borderRadius: 10, marginRight: WP3 }}
                />
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <Text type='Circular' size='mini' color={GUN_METAL} style={{ marginBottom: WP2 }} weight={400}>
                    {image.name}
                  </Text>
                  <Text type='Circular' size='xmini' color={SHIP_GREY_CALM}>{image.size}</Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row', paddingVertical: WP205, backgroundColor: PALE_GREY, paddingHorizontal: WP3 }}>
                {
                  this._imageButtonAction({
                    icon: require('sf-assets/icons/mdi_edit.png'),
                    label: 'Edit',
                    action: () => {
                      this._selectPhoto().then((result) => {
                        this._editImage(result, i)
                      })
                    }
                  })
                }
                {
                  this._imageButtonAction({
                    icon: require('sf-assets/icons/mdi_delete.png'),
                    label: 'Delete',
                    action: () => this._deleteImage(i)
                  })
                }
              </View>
            </View>
          ))
        }

        <ButtonV2
          onPress={toggleModal}
          style={style.emptyStateCta}
          color={REDDISH}
          textColor={WHITE}
          textSize={'mini'}
          text={'Upload Foto'}
          disabled={this.state.isUploading}
        />
        {M}
      </View>
    )
  }

  _divider = () => (
    <View style={{ width: WP100, height: 1, backgroundColor: PALE_BLUE }} />
  )

  render() {
    let {
      navigateBack,
      experience
    } = this.props

    const {
      isUploading,
      images
    } = this.state

    return (
      <Container colors={[WHITE, WHITE]}>
        <Form
          onChangeInterceptor={({ key, text }) => this.setState({ [key]: text })}
          initialValue={{
            event_date: new Date(),
            ...experience.additional_data,
            ...experience,
          }}
        >
          {({ onChange, formData }) => (
            <View style={{ overflow: 'hidden' }}>
              <View>
                <HeaderNormal
                  iconLeftOnPress={() => navigateBack()}
                  iconLeftWrapperStyle={{ width: WP8, marginRight: WP12 }}
                  textType='Circular'
                  textColor={SHIP_GREY}
                  textWeight={400}
                  text='Tambah Experiences'
                  textSize={'slight'}
                  centered
                  rightComponent={(
                    <View style={{ width: WP25, alignItems: 'flex-end', paddingRight: WP5 }}>
                      {!isUploading &&
                    <TouchableOpacity
                      onPress={() => {
                        Keyboard.dismiss()
                        this._onPerformanceJourney()
                      }}
                      activeOpacity={TOUCH_OPACITY}
                      disabled={!this._isValid()}
                    >
                      <Text size='slight' weight={400} color={this._isValid() ? ORANGE_BRIGHT : PALE_SALMON}>Simpan</Text>
                    </TouchableOpacity>
                      }
                      {isUploading &&
                    <Image
                      spinning
                      source={require('sf-assets/icons/ic_loading.png')}
                      imageStyle={{ width: WP8, aspectRatio: 1 }}
                    />
                      }
                    </View>
                  )}
                />
                <LinearGradient
                  colors={SHADOW_GRADIENT}
                  style={HEADER.shadow}
                />
              </View>

              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: WP25, backgroundColor: PALE_WHITE }}
              >
                <View style={styles.section}>
                  <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>EVENT EXPERIENCES</Text>

                  <InputTextLight
                    withLabel={false}
                    placeholder='Nama event kamu bermain. Contoh: Soundfren Festival'
                    value={formData.title}
                    onChangeText={onChange('title')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Nama Event'
                    editable={!isUploading}
                  />

                  <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
                    Bulan & Tahun Event Tersebut
                  </Text>

                  <SelectDate
                    value={formData.event_date}
                    dateFormat='YYYY-MM-DD'
                    maximumDate={new Date()}
                    mode='date'
                    onChangeDate={onChange('event_date')}
                    style={{ marginBottom: WP4 }}
                    withDate={false}
                    disabled={isUploading}
                  />

                  <InputTextLight
                    withLabel={false}
                    placeholder='Tuliskan lokasi event yang kamu ikuti. Contoh: ICE BSD'
                    value={formData.event_location}
                    onChangeText={onChange('event_location')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Lokasi Event'
                    editable={!isUploading}
                  />
                </View>

                <View style={styles.section}>
                  <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>ROLES INFORMATIONS</Text>
                  <InputModal
                    triggerComponent={(
                      <InputTextLight
                        withLabel={false}
                        placeholder='Tuliskan posisimu di group itu. Contoh: Back Singer'
                        value={formData.role}
                        onChangeText={onChange('role')}
                        placeholderTextColor={SILVER_TWO}
                        color={SHIP_GREY}
                        bordered
                        size='mini'
                        type='Circular'
                        returnKeyType={'next'}
                        wording=' '
                        maxLengthFocus
                        lineHeight={1}
                        style={{ marginTop: 0, marginBottom: 0 }}
                        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                        labelv2='Role / Posisi'
                        disabled
                        editable={false}
                      />
                    )}
                    header='Select Role'
                    placeholder='Search role here...'
                    suggestion={getJob}
                    suggestionKey='job_title'
                    suggestionPathResult='job_title'
                    onChange={onChange('role')}
                    selected={formData.jobTitle}
                    disabled={isUploading}
                  />
                  {
                    formData.company !== SOLO_PERFORMANCE && (
                      <InputTextLight
                        withLabel={false}
                        placeholder='Tuliskan nama band / group / team kamu'
                        value={formData.company}
                        onChangeText={onChange('company')}
                        placeholderTextColor={SILVER_TWO}
                        color={SHIP_GREY}
                        bordered
                        size='mini'
                        type='Circular'
                        returnKeyType={'next'}
                        wording=' '
                        maxLengthFocus
                        lineHeight={1}
                        style={{ marginTop: 0, marginBottom: 0 }}
                        textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                        labelv2='Nama Band / Group / Team'
                        editable={!isUploading}
                      />
                    )
                  }
                  <CheckBox
                    disabled={isUploading}
                    checked={formData.company === SOLO_PERFORMANCE}
                    containerStyle={{
                      backgroundColor: WHITE, borderWidth: 0, margin: 0,
                      paddingHorizontal: -WP2, marginBottom: WP2
                    }}
                    onPress={() => {
                      onChange('company')(formData.company === SOLO_PERFORMANCE ? undefined : SOLO_PERFORMANCE)
                    }}
                    title='Saya pemain solo (Solo Performance)'
                    textStyle={{ fontSize: WP308, color: formData.company === SOLO_PERFORMANCE ? REDDISH : SHIP_GREY }}
                    fontFamily={FONTS.Circular['200']}
                    wrapperStyle={{ marginHorizontal: -WP2, marginTop: -WP3 }}
                    checkedIcon={(
                      <Image
                        imageStyle={{ width: WP305, aspectRatio: 1 }}
                        source={require('sf-assets/icons/icCheckboxActive.png')}
                      />
                    )}
                    uncheckedIcon={(
                      <Image
                        imageStyle={{ width: WP305, aspectRatio: 1 }}
                        source={require('sf-assets/icons/icCheckboxInactive.png')}
                      />
                    )}
                  />
                </View>

                <View style={{ ...styles.section, borderBottomWidth: 0 }}>
                  <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>
                    {'MEDIA '}
                    <Text type='Circular' size='xmini' color={PALE_LIGHT_BLUE_TWO}>(Opsional)</Text>
                  </Text>
                  {
                    isEmpty(images) ?
                      this._renderUploadImageModal(this._renderEmptyMedia) :
                      this._renderUploadImageModal(this._renderMedia)
                  }
                </View>
              </ScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateExperiencePerformance),
  mapFromNavigationParam
)
