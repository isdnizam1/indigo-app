import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Image, View } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { Container, Text, HeaderNormal } from '../../components'
import { HP1, WP100, WP4, WP50, WP6, WP1, HP3 } from '../../constants/Sizes'
import { GREY_CALM_SEMI, TOMATO } from '../../constants/Colors'
import { WORKSPAGE } from '../../components/works/WorksConstant'
import { toMonthDate, fullDateToMonthDate } from '../../utils/date'
// import 'moment/locale/id'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  data: getParam('data', null)
})

const type = {
  performance_journey: WORKSPAGE.performance,
  music_journey: WORKSPAGE.journey
}

class DetailExperienceScreen extends Component {

  state = {
    isLoading: true,
    isReady: false,
    activeSlider: 0,
  }

  render() {
    const { navigateBack, data } = this.props
    const { activeSlider } = this.state
    const page = type[data.type]
    const additionalData = typeof data.additional_data === 'string' ? JSON.parse(data.additional_data) : data.additional_data
    let title,
      second,
      third,
      desc
    if (data.type === 'performance_journey') {
      title = data.title
      second = `at ${additionalData.event_location}`
      third = `${fullDateToMonthDate(additionalData.event_date)}`
      desc = data.description
    }
    if (data.type === 'music_journey') {
      title = additionalData.role
      second = `at ${additionalData.company}`
      third = `${toMonthDate(additionalData.date_join.start)} - ${toMonthDate(additionalData.date_join?.until)}`
      desc = data.description
    }
    return (
      <Container
        scrollable
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text={page.title}
          />
        )}
      >
        <View>
          <Carousel
            onSnapToItem={(i) => this.setState({ activeSlider: i })}
            data={additionalData.media}
            firstItem={activeSlider}
            inactiveSlideScale={1}
            activeSlideAlignment='start'
            sliderWidth={WP100}
            itemWidth={WP100}
            containerCustomStyle={{ }}
            renderItem={({ item, index }) => {
              return (<View style={{ backgroundColor: '#000' }} key={index}>
                <Image style={{ width: WP100, height: WP50 }} source={{ uri: item }} />
              </View>)
            }}
          />
          <Pagination
            dotsLength={additionalData.media.length}
            activeDotIndex={activeSlider}
            dotColor={TOMATO}
            inactiveDotColor={GREY_CALM_SEMI}
            inactiveDotScale={1}
            containerStyle={{
              justifyContent: 'center',
              paddingVertical: 0,
              marginTop: WP4 * -1,
              marginBottom: HP1
            }}
            dotContainerStyle={{ marginHorizontal: 1 }}
            dotStyle={{
              width: 8,
              height: 8,
              borderRadius: 4,
            }}
          />
          <View style={{ padding: WP6 }}>
            <Text weight={'500'} type='NeoSans' size='large'>{title}</Text>
            <View style={{ marginTop: WP1 }}>
              <Text size='mini'>{second}</Text>
            </View>
            <View style={{ marginTop: WP1 }}>
              <Text size='mini'>{third}</Text>
            </View>
            <View style={{ marginTop: HP3 }}>
              <Text weight={'500'} size='mini'>Description</Text>
            </View>
            <View style={{ marginTop: WP1 }}>
              <Text size='mini'>{desc}</Text>
            </View>
          </View>
        </View>
      </Container>
    )
  }

}

DetailExperienceScreen.propTypes = {}
DetailExperienceScreen.defaultProps = {}
export default _enhancedNavigation(
  connect(mapStateToProps, {})(DetailExperienceScreen),
  mapFromNavigationParam
)
