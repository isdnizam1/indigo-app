import React from 'react'
import { isEmpty, map, pick, forEach } from 'lodash-es'
import { View, TouchableOpacity, ImageBackground, Platform, StatusBar, Image, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import { BackHandler } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import moment from 'moment'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import {
  _enhancedNavigation,
  Container,
  Form,
  Header,
  Icon,
  InputTextLight,
  Text,
  InputModal,
  InputDate,
  ListItem, Modal
} from '../../components'
import { BLACK, GREY, SILVER_CALM, WHITE, WHITE_MILK, ORANGE_BRIGHT, GREY_BACKGROUND, PURE_RED } from '../../constants/Colors'
import { getCompany, getJob, postEditMusicExperiences, deleteJourney } from '../../actions/api'
import {
  FONT_SIZE,
  HP1,
  HP105,
  WP1,
  WP10,
  WP100,
  WP2,
  WP24,
  WP5
} from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { alertDelete } from '../../utils/alert'
import { convertBlobToBase64, fetchAsBlob } from '../../utils/helper'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  refreshData: getParam('refreshData', () => { }),
  experience: getParam('experience', {})
})

class EditExperienceMusic extends React.Component {
    static navigationOptions = ({ navigation }) => ({
      gesturesEnabled: !navigation.getParam('initialLoaded', false)
    })

    state = {
      isRefreshing: false,
      images: [],
      imagesName: [],
      imagesBase64: []
    }

    async componentDidMount() {
      const {
        experience
      } = this.props
      const data = JSON.parse(experience.additional_data)
      if (!isEmpty(data.media)) {
        const images = []
        const imagesBase64 = []
        const imagesName = []
        forEach(data.media, async (media, i) => {
          const [name] = media.split('_').slice(-1)
          images.push(media)
          imagesName.push(name)
          imagesBase64.push((await fetchAsBlob(media).then(convertBlobToBase64)).replace('data:application/octet-stream;base64,', ''))
        })
        this.setState({
          imagesBase64,
          imagesName,
          images
        })
      }

      BackHandler.addEventListener('hardwareBackPress', this._backHandler)
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
    }

    _backHandler = async () => {
      this.props.navigateBack()
    }

    _selectPhoto = async () => {
      if (Platform.OS === 'ios') StatusBar.setHidden(true)
      const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
      if (statusCameraRoll === 'granted') {
        return await ImagePicker.launchImageLibraryAsync({
          base64: true,
          allowsEditing: true,
          aspect: [1, 1]
        })
      }
    }

    _takePhoto = async () => {
      if (Platform.OS === 'ios') StatusBar.setHidden(true)
      const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
      const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
      if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
        return await ImagePicker.launchCameraAsync({
          base64: true,
          allowsEditing: true,
          aspect: [1, 1]
        })
      }
    }

    _deleteJourney = () => {
      alertDelete(this._delete)
    }

    _delete = () => {
      const {
        dispatch,
        refreshData,
        navigateBack,
        experience
      } = this.props
      dispatch(deleteJourney, experience.id_journey)
        .then(() => {
          refreshData()
          navigateBack()
        })
    }

    _deleteImage = (index) => {
      const {
        images,
        imagesBase64
      } = this.state
      images.splice(index, 1)
      imagesBase64.splice(index, 1)
      this.setState({
        images,
        imagesBase64
      })
    }

    _handlePhoto = async (result) => {
      if (Platform.OS === 'ios') StatusBar.setHidden(false)
      const {
        uri,
        base64,
        cancelled
      } = result

      if (!result) {
        return
      }

      if (!cancelled) this.setState((state) => {
        let name = result.uri.split('/')
        name = name[name.length - 1]
        return ({
          images: [...state.images, uri],
          imagesBase64: [...state.imagesBase64, base64],
          imagesName: [...state.imagesName, name]
        })
      })
    }

  _onMusicJourney = async ({ formData, isDirty }) => {
    const {
      userData,
      dispatch,
      refreshData,
      navigateBack,
      experience
    } = this.props
    const {
      imagesBase64
    } = this.state
    const formMusicJourney = pick(formData, ['jobTitle', 'companyName', 'startDate', 'endDate', 'description', 'isCurrentlyHere', 'imagesBase64'])
    dispatch(postEditMusicExperiences, {
      id_journey: experience.id_journey,
      id_user: userData.id_user,
      role: formMusicJourney.jobTitle,
      company: formMusicJourney.companyName,
      description: formMusicJourney.description,
      media: imagesBase64,
      startJoin: formMusicJourney.startDate,
      until: formMusicJourney.isCurrentlyHere ? 'Now' : formMusicJourney.endDate
    }, null, true)
      .then(() => {
        refreshData()
        navigateBack()
      })
  }

    _isValid = (state) => (
      state.jobTitle
        && state.companyName
        && state.startDate
        && state.endDate
        && state.description
        && !isEmpty(this.state.images)
    )

    render() {
      const {
        isLoading,
        experience
      } = this.props
      const {
        images
      } = this.state
      const additionalData = JSON.parse(experience.additional_data)
      return (
        <Container
          borderedHeader
          isLoading={isLoading}
        >
          <Form
            initialValue={{
              jobTitle: additionalData ? additionalData.role : '',
              companyName: additionalData ? additionalData.company : '',
              startDate: additionalData ? additionalData.date_join.start : '',
              endDate: additionalData ? additionalData.date_join.until : '',
              isCurrentlyHere: additionalData ? additionalData.date_join.until === 'Now' : '',
              description: experience.description
            }}
            onSubmit={this._onMusicJourney}
          >
            {
              ({ onChange, onSubmit, formData }) => (
                <View style={{ flex: 1, backgroundColor: GREY_BACKGROUND }}>
                  <Header style={{ backgroundColor: WHITE }}>
                    <Icon
                      size='huge' name='chevron-left' type='Entypo' centered
                      onPress={this._backHandler} color={BLACK}
                    />
                    <Text size='large' type='SansPro' weight={500}>Edit Music Experience</Text>
                    <TouchableOpacity
                      onPress={() => {
                        Keyboard.dismiss()
                        onSubmit()
                      }} activeOpacity={TOUCH_OPACITY}
                      disabled={!this._isValid(formData)}
                    >
                      <Text size='medium' color={ORANGE_BRIGHT}>Save</Text>
                    </TouchableOpacity>

                  </Header>
                  <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps='handled'
                    style={{ marginTop: WP1, backgroundColor: WHITE }}
                  >
                    <View style={{ paddingHorizontal: WP10, paddingVertical: WP2 }}>
                      <InputModal
                        triggerComponent={(
                          <InputTextLight
                            bold
                            size='mini'
                            value={formData.jobTitle}
                            label='What is your role?'
                            placeholder='Vocalist'
                            editable={false}
                          />
                        )}
                        header='Select Role'
                        placeholder='Search role here...'
                        suggestion={getJob}
                        suggestionKey='job_title'
                        suggestionPathResult='job_title'
                        onChange={onChange('jobTitle')}
                        selected={formData.jobTitle}
                      />
                      <InputModal
                        triggerComponent={(
                          <InputTextLight
                            bold
                            size='mini'
                            value={formData.companyName}
                            label='Band/Artist/Company Name'
                            placeholder='Self Employed'
                            editable={false}
                          />
                        )}
                        header='Select Band/Artist/Company Name'
                        placeholder='Search here...'
                        suggestion={getCompany}
                        suggestionKey='company_name'
                        suggestionPathResult='company_name'
                        onChange={onChange('companyName')}
                        selected={formData.companyName}
                      />
                      <View style={{ marginTop: HP105, marginBottom: HP1 }}>
                        <Text size={'mini'} color={GREY} weight={500}>How long were you joined?</Text>
                        <View style={{ flexDirection: 'row' }}>
                          <View style={{ flex: 1, marginRight: WP2 }}>
                            <InputDate
                              size='mini'
                              value={formData.startDate}
                              dateFormat='MM/YYYY'
                              mode='date'
                              maximumDate={formData.maxStartDate}
                              placeholder='Start Date (MM/YYYY)'
                              onChangeText={(text) => {
                                onChange('startDate')(text)
                                onChange('minEndDate')(text)
                              }}
                            />
                          </View>
                          <View style={{ flex: 1, marginLeft: WP2 }}>
                            <InputDate
                              size='mini'
                              value={formData.endDate}
                              dateFormat='MM/YYYY'
                              mode='date'
                              minimumDate={formData.minEndDate}
                              placeholder='End Date (MM/YYYY)'
                              onChangeText={(text) => {
                                onChange('endDate')(text)
                                onChange('maxStartDate')(text)
                                onChange('isCurrentlyHere')(false)
                              }}
                            />
                            <TouchableOpacity
                              activeOpacity={TOUCH_OPACITY}
                              style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}
                              onPress={() => {
                                const isCurrentlyHere = !formData.isCurrentlyHere
                                onChange('isCurrentlyHere')(isCurrentlyHere)
                                if (isCurrentlyHere) {
                                  onChange('endDate')('Present')
                                  const maxStartDate = moment().format('MM/YYYY')
                                  onChange('maxStartDate')(maxStartDate)
                                  if (formData.startDate > maxStartDate) onChange('startDate')(maxStartDate)
                                } else {
                                  onChange('endDate')(undefined)
                                  onChange('maxStartDate')(null)
                                }
                              }}
                            >
                              <Icon
                                name={`check-box${!formData.isCurrentlyHere ? '-outline-blank' : ''}`}
                                type='MaterialIcons'
                              />
                              <Text size='xtiny'>I currently work in this role</Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                      <InputTextLight
                        label='Tell us about this journey!'
                        value={formData.description}
                        size='mini'
                        placeholder='Genjreng Band is a Jazz Pop band who loves to make covers. '
                        multiline
                        maxLength={160}
                        onChangeText={onChange('description')}
                      />
                      <View>
                        <Text size={'mini'} color={GREY} weight={500}>Media</Text>
                        <Modal
                          renderModalContent={({ toggleModal }) => (
                            <View>
                              <ListItem
                                inline onPress={() => {
                                  this._selectPhoto().then((result) => {
                                    toggleModal()
                                    this._handlePhoto(result)
                                  })
                                }}
                              >
                                <Icon type='MaterialIcons' size='large' name='photo-library' />
                                <Text style={{ paddingLeft: WP2 }}>Select from library</Text>
                              </ListItem>
                              <ListItem
                                inline onPress={() => {
                                  this._takePhoto().then((result) => {
                                    toggleModal()
                                    this._handlePhoto(result)
                                  })
                                }}
                              >
                                <Icon size='large' name='camera' />
                                <Text style={{ paddingLeft: WP2 }}>Take a picture</Text>
                              </ListItem>
                            </View>
                          )}
                        >
                          {({ toggleModal }, M) => (
                            <View style={{ flexDirection: 'row', paddingVertical: HP1, flexWrap: 'wrap' }}>
                              {
                                map(images, (image, i) => (
                                  <ImageBackground
                                    key={`${i}${new Date()}`}
                                    imageStyle={{ borderRadius: 5 }}
                                    style={{
                                      alignItems: 'flex-end', backgroundColor: WHITE_MILK,
                                      width: WP24, height: undefined, aspectRatio: 1,
                                      marginVertical: WP1, marginHorizontal: WP1
                                    }}
                                    source={{ uri: image }}
                                  >
                                    <TouchableOpacity
                                      onPress={() => this._deleteImage(i)}
                                      activeOpacity={TOUCH_OPACITY}
                                      style={{
                                        margin: WP1,
                                        padding: WP1, width: FONT_SIZE['tiny'] + WP2, height: FONT_SIZE['tiny'] + WP2,
                                        borderRadius: WP100, backgroundColor: GREY
                                      }}
                                    >
                                      <Icon size='tiny' color={WHITE} name='close' />
                                    </TouchableOpacity>
                                  </ImageBackground>
                                ))
                              }
                              <TouchableOpacity
                                onPress={toggleModal}
                              >
                                <View style={{
                                  width: WP24,
                                  height: WP24,
                                  marginVertical: WP1, marginHorizontal: WP1,
                                  backgroundColor: SILVER_CALM,
                                  borderRadius: 5,
                                  justifyContent: 'center',
                                  alignItems: 'center'
                                }}
                                >
                                  <Image
                                    source={require('../../assets/icons/iconCamera.png')}
                                    style={{ width: WP5, height: undefined, aspectRatio: 222 / 183 }}
                                  />
                                </View>
                              </TouchableOpacity>
                              {M}
                            </View>
                          )}
                        </Modal>
                      </View>
                    </View>

                    <TouchableOpacity
                      onPress={() => {
                        this._deleteJourney()
                      }} activeOpacity={TOUCH_OPACITY}
                    >
                      <Text size='large' style={{ margin: WP10, alignSelf: 'center' }} color={PURE_RED}>Delete Music Experience</Text>
                    </TouchableOpacity>
                  </KeyboardAwareScrollView>
                </View>
              )
            }
          </Form>
        </Container>
      )
    }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(EditExperienceMusic),
  mapFromNavigationParam
)
