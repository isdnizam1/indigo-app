import React, { Fragment } from 'react'
import { View, ImageBackground, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { BackHandler } from 'react-native'
import { isEmpty, noop } from 'lodash-es'
import {
  _enhancedNavigation,
  Container,
  Header,
  Icon,
  Text
} from '../../components'
import { DEFAULT_PAGING } from '../../constants/Routes'
import { BLACK, GREY_CALM, WHITE, WHITE_MILK, GREY_WARM } from '../../constants/Colors'
import { HP2, WP2, WP5, WP105, WP05, WP3, WP6, WP1, WP28, WP100 } from '../../constants/Sizes'
import ListItem from '../../components/ListItem'
import Modal from '../../components/Modal'
import { getExperiences, postRemoveExperiences } from '../../actions/api'
import { fullDateToMonthDate, toMonthDate } from '../../utils/date'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const styles = {
  container: {
    backgroundColor: GREY_CALM
  },
  editIcon: {
    position: 'absolute',
    right: WP5,
    top: WP2
  },
  header: {
    wrapper: {
      backgroundColor: WHITE,
      marginTop: WP105,
      marginBottom: WP05,
      paddingVertical: WP2,
      paddingHorizontal: WP5,
      flexDirection: 'row',
      alignItems: 'center'
    },
    iconWrapper: {
      marginRight: WP3
    },
    icon: {
      height: WP6
    },
    text: {},
    seeAll: {
      backgroundColor: WHITE,
      marginTop: WP105,
      marginBottom: WP05,
      paddingVertical: WP2,
      paddingHorizontal: WP5,
      alignItems: 'center'
    }
  },
  experience: {
    wrapper: {
      backgroundColor: WHITE,
      paddingVertical: WP105,
      paddingHorizontal: WP5,
      marginVertical: WP05
    },
    title: {
      marginBottom: WP1
    },
    info: {
      marginTop: WP05,
      marginBottom: WP1
    },
    description: {
      marginBottom: WP1
    },
    subHeader: {
      weight: 400,
      size: 'mini'
    },
    infoText: {
      weight: 500,
      size: 'mini'
    },
    mediaWrapper: {
      flex: 1, flexDirection: 'row', flexWrap: 'wrap',
      marginTop: WP105
    },
    media: {
      alignItems: 'flex-end', backgroundColor: WHITE_MILK,
      width: WP28, height: undefined, aspectRatio: 1,
      marginVertical: WP1, marginHorizontal: WP1
    }
  }
}

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  refreshProfile: getParam('refreshProfile', () => { }),
  title: getParam('title', ''),
  idUser: getParam('idUser', 0),
  category: getParam('category', '')
})

class ListExperienceScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
      gesturesEnabled: !navigation.getParam('initialLoaded', false)
    })

    state = {
      isReady: false,
      isRefreshing: false,
      experiences: []
    }

    async componentDidMount() {
      await this._getInitialScreenData()
      BackHandler.addEventListener('hardwareBackPress', this._backHandler)
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
    }

    _getInitialScreenData = async (...args) => {
      await this._getData(...args)
      this.setState({
        isReady: true,
        isRefreshing: false,
      })
    }

    _getData = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
      const {
        dispatch,
        idUser
      } = this.props
      this.setState({
        isReady: false,
        isRefreshing: true,
      })
      try {
        const dataResponse = await dispatch(getExperiences, { id_user: idUser }, noop, true, isLoading)
        this.setState({ experiences: dataResponse.result || {} })
      } catch (e) {
        //
      }
      this.setState({
        isReady: true,
        isRefreshing: false,
      })
    }

  _deleteExperience = async (experience) => {
    const {
      dispatch,
    } = this.props

    try {
      await dispatch(postRemoveExperiences, { id_journey: experience.id_journey })
    } catch (e) {
      //
    }
    await this._getData()

  }

    _backHandler = async () => {
      this.props.refreshProfile()
      this.props.navigateBack()
    }

    _renderExperiences = (category) => {
      const {
        experiences,
      } = this.state
      return (
        <View style={{ width: WP100, ...styles.container }}>
          <Modal
            renderModalContent={({ toggleModal, payload }) => (
              <View>
                <ListItem
                  inline onPress={() => {
                    toggleModal()
                    this._deleteExperience(payload)
                  }
                  }
                >
                  <Icon size='large' name='delete' />
                  <Text style={{ paddingLeft: WP2 }}>Delete</Text>
                </ListItem>
              </View>
            )}
          >
            {({ toggleModal }, M) => (
              <View>
                {
                  experiences[category].map((experience, index) => (
                    this._renderJourney(experience, this.experienceRenderOption(experience), toggleModal)
                  ))
                }
                {M}
              </View>
            )}
          </Modal>
        </View>
      )
    }

  _editExperience = async (experience) => {
    const {
      navigateTo
    } = this.props

    if (experience.type === 'music_journey') {
      navigateTo('EditExperienceMusic', { refreshData: this._getData, experience })
    } else if (experience.type === 'performance_journey') {
      navigateTo('EditExperiencePerformance', { refreshData: this._getData, experience })
    } else {
      // wrong
    }
  }

  _renderJourney = (experience, experienceConstant, toggleModal) => {
    const additionalData = JSON.parse(experience.additional_data)
    const isMine = experience.id_user === this.props.userData.id_user
    const {
      navigateTo
    } = this.props
    return (
      <View style={styles.experience.wrapper}>
        <Text weight={500} style={styles.experience.title}>{experienceConstant.title}</Text>
        <View style={{ flexDirection: 'row' }}>
          {experienceConstant.subHeader}
        </View>
        <Text style={styles.experience.info} size='slight'>
          {experienceConstant.info}
        </Text>
        <Text style={styles.experience.description} size='mini'>{experience.description}</Text>
        {
          !isEmpty(additionalData.media) && (
            <View style={styles.experience.mediaWrapper}>
              {
                additionalData.media.map((item, index) => (
                  <TouchableOpacity
                    key={`${index}${new Date()}`}
                    onPress={() => navigateTo('ImagePreviewScreen', { images: [{ url: item }] })}
                  >
                    <ImageBackground
                      style={styles.experience.media}
                      source={{ uri: item }}
                    />
                  </TouchableOpacity>
                ))
              }
            </View>
          )
        }
        {isMine && (
          <Icon
            style={styles.editIcon}
            centered type='MaterialCommunityIcons'
            name='pencil'
            onPress={() => this._editExperience(experience)}
          />
        )}
      </View>
    )
  }

  experienceRenderOption = (experience) => {
    const additionalData = JSON.parse(experience.additional_data)
    if (experience.type === 'music_journey') {
      return {
        title: additionalData.role,
        subHeader: (
          <Fragment>
            <Text {...styles.experience.subHeader}>{toMonthDate(additionalData.date_join.start)}</Text>
            <Icon centered color={GREY_WARM} type='Entypo' name='dot-single' />
            <Text {...styles.experience.subHeader}>{toMonthDate(additionalData.date_join.until)}</Text>
          </Fragment>
        ),
        info: <Text {...styles.experience.infoText}>{additionalData.company}</Text>
      }
    }

    if (experience.type === 'performance_journey')
      return {
        title: experience.title,
        subHeader: (
          <Fragment>
            <Text {...styles.experience.subHeader}>{additionalData.event_location}</Text>
            <Icon centered color={GREY_WARM} type='Entypo' name='dot-single' />
            <Text {...styles.experience.subHeader}>{fullDateToMonthDate(additionalData.event_date)}</Text>
          </Fragment>
        ),
        info: (
          <Fragment>
            <Text {...styles.experience.infoText}>{additionalData.role}</Text>
            {' at '}
            <Text {...styles.experience.infoText}>{additionalData.company}</Text>
          </Fragment>
        )
      }

    return {}
  }
  render() {
    const {
      title,
      category } = this.props
    const {
      isReady,
      isRefreshing,
      experiences
    } = this.state
    return (
      <Container
        scrollable
        borderedHeader
        isLoading={isRefreshing}
        isReady={isReady}
        onPullDownToRefresh={this._getData}
        renderHeader={() => (
          <Header style={{ justifyContent: 'flex-start' }}>
            <Icon
              size='huge' name='chevron-left' type='Entypo' centered
              onPress={this._backHandler} color={BLACK}
            />
            <Text size='large' type='SansPro' weight={500}>{title}</Text>
          </Header>
        )}
      >
        <View style={{ alignItems: 'center', paddingVertical: HP2 }}>
          {
            !isEmpty(experiences[category]) && this._renderExperiences(category)
          }
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ListExperienceScreen),
  mapFromNavigationParam
)
