import React from 'react'
import { connect } from 'react-redux'
import { BackHandler } from 'react-native'
import {
  _enhancedNavigation,
  Container,
  Header,
  Icon,
  Text,
  Button, Empty
} from '../../components'
import { BLACK } from '../../constants/Colors'
import { ExperienceSteps } from '../../components/StepCircle'
import { BORDER_STYLE } from '../../constants/Styles'
import { WP65 } from '../../constants/Sizes'

const EXPERIENCE_TYPE = {
  music: {
    title: 'Music Experience',
    message: (
      <Text size='mini' centered>
        You
        <Text size='mini' centered weight={500}> music experience </Text>
        has been added to your profile!
      </Text>
    )
  },
  performance: {
    title: 'Performance Journey',
    message: (
      <Text size='mini' centered>
        You
        <Text size='mini' centered weight={500}> performance journey </Text>
        has been added to your profile!
      </Text>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  experienceType: getParam('experienceType', 'music'),
  refreshProfile: getParam('refreshProfile', () => {})
})

class CreateExperienceFinish extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  render() {
    const {
      popToTop,
      refreshProfile,
      experienceType,
    } = this.props
    const currentJourney = EXPERIENCE_TYPE[experienceType]
    return (
      <Container
        scrollable
        borderedHeader
        renderHeader={() => (
          <Header style={{ justifyContent: 'flex-start' }}>
            <Icon
              size='huge' name='chevron-left' type='Entypo' centered
              onPress={this._backHandler} color={BLACK}
            />
            <Text size='large' type='SansPro' weight={500}>Create {currentJourney.title}</Text>
          </Header>
        )}
        outsideScrollContentTop={() => <ExperienceSteps style={{ ...BORDER_STYLE['bottom'] }} index={3}/>}
      >
        <Empty
          title='Hurray!'
          message={currentJourney.message}
          image={require('../../assets/images/promoteArtistPreview.png')}
          aspectRatio={200/96}
          imageWidth={WP65}
          actions={(
            <Button
              onPress={() => {
                refreshProfile()
                popToTop()
              }}
              rounded
              centered
              soundfren
              width={WP65}
              text='Check Your Profile!'
              shadow='none'
            />
          )}
        />
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateExperienceFinish),
  mapFromNavigationParam
)
