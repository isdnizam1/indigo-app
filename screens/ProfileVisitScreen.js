import React from 'react'
import { BackHandler, ScrollView, View } from 'react-native'
import { noop } from 'lodash-es'
import { connect } from 'react-redux'
import { _enhancedNavigation, Container } from '../components'
import { PALE_WHITE } from '../constants/Colors'
import { getVisitedProfile, postProfileFollow, postProfileUnfollow } from '../actions/api'
import { WP3, WP6, } from '../constants/Sizes'
import HeaderNormal from '../components/HeaderNormal'
import FollowItem from '../components/follow/FollowItem'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  id_notif: getParam('id_notif', undefined),
})

class ProfileVisitScreen extends React.Component {
  state = {
    users: [],
  }

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  async componentDidMount() {
    this._getVisitedProfile()
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  _getVisitedProfile = async () => {
    const { dispatch, userData, id_notif } = this.props
    const users = await dispatch(getVisitedProfile, { id_user: userData.id_user, id_notif })
    this.setState({ users })
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _onFollowed = async (follow, destinationId, isLoading = true) => {
    const {
      userData: { id_user },
      dispatch
    } = this.props

    const { users } = this.state

    const currentAction = follow ? postProfileFollow : postProfileUnfollow
    const currentPayload = follow ? { id_user: destinationId, followed_by: id_user } : {
      id_user,
      id_user_following: destinationId
    }

    dispatch(currentAction, currentPayload, noop, true, isLoading)
    const updatedUsers = users.map((user) => {
      if (user.id_user == destinationId) {
        user.is_followed_by_viewer = !user.is_followed_by_viewer
      }
      return user
    })
    this.setState({ users: updatedUsers })
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  render() {
    const {
      navigateTo,
      isLoading,
    } = this.props
    const {
      users,
      isReady
    } = this.state
    return (
      <Container
        scrollable={false}
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._backHandler}
            text='Mengunjungi Profilemu'
            centered
            shadow
          />
        )}
        scrollBackgroundColor={PALE_WHITE}
      >
        <ScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flexGrow: 1 }}
          style={{ backgroundColor: PALE_WHITE, paddingTop: WP3, paddingBottom: WP6 }}
        >
          <View>
            {
              users.map((item, index) => (
                <FollowItem
                  idUser={item.id_user}
                  onPressItem={() => {
                    navigateTo('ProfileScreenNoTab', { idUser: item.id_user }, 'push')
                  }}
                  onFollow={() => {
                    const isFollowed = item.is_followed_by_viewer
                    this._onFollowed(!isFollowed, item.id_user, false)
                  }}
                  key={`${index}${item.id_user}`}
                  user={item}
                />
              ))
            }
          </View>
        </ScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(ProfileVisitScreen),
  mapFromNavigationParam
)
