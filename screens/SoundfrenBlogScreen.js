import React from 'react'
import { FlatList, View } from 'react-native'
import { withNavigation } from '@react-navigation/compat'
import { connect } from 'react-redux'
import { concat, isEmpty, noop } from 'lodash-es'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { _enhancedNavigation, Container } from '../components'
import { getAds } from '../actions/api'
import { authDispatcher } from '../services/auth'
import { WHITE } from '../constants/Colors'
import AdsNewsItem from '../components/ads/AdsNewsItem'
import HeaderNormal from '../components/HeaderNormal'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class SoundfrenBlogScreen extends React.Component {
    state = {
      listNews: [],
      isReady: false,
      isLoading: false,
    }

    async componentDidMount() {
      await this._getInitialScreenData({ limit: 20 })
    }

    _getInitialScreenData = async (...args) => {
      await this._getNews(...args)
    }

    _getNews = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
      const {
        dispatch
      } = this.props
      const {
        listNews
      } = this.state

      const dataResponse = await dispatch(getAds, {
        status: 'active',
        category: 'news_update', ...params }, noop, true, isLoading)

      let newNews = loadMore ? listNews : []
      if (!isEmpty(dataResponse.result)) newNews = concat(newNews, dataResponse.result)

      await this.setState((state) => {
        return {
          isReady: true,
          listNews: newNews,
        }
      })
    }

    render() {
      const {
        navigateTo,
        navigateBack
      } = this.props
      const {
        listNews,
        isReady,
        isLoading
      } = this.state
      return (
        <Container
          scrollable
          isLoading={isLoading}
          isReady={isReady}
          scrollBackgroundColor={WHITE}
          type='horizontal'
          onPullDownToRefresh={async () => {
            await this._getInitialScreenData(null, null, false)
          }}
          onPullUpToLoad={async () => {
            await this._getNews({ start: listNews.length, limit: 10 }, true, false)
          }}
          borderedHeader
          style={{ flex: 1 }}
          renderHeader={() => (
            <HeaderNormal
              iconLeftOnPress={navigateBack}
              text='Soundfren Blog'
              centered
            />
          )}
        >
          <View style={{
            marginVertical: wp('2%')
          }}
          >
            <FlatList
              data={listNews}
              keyExtractor={(item, index) => `key${ item.id_ads}`}
              renderItem={({ item, index }) => (
                <AdsNewsItem
                  navigateTo={navigateTo}
                  index={index}
                  ads={item}
                />
              )}
            />
          </View>
        </Container>
      )
    }
}

export default withNavigation(_enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SoundfrenBlogScreen),
  mapFromNavigationParam
))
