import React from 'react'
import { View } from 'react-native'
import { noop } from 'lodash-es'
import { connect } from 'react-redux'
import { LinearGradient } from 'expo-linear-gradient'
import { _enhancedNavigation, Container } from '../components'
import { DEFAULT_PAGING } from '../constants/Routes'
import { getFeedLikes, postProfileFollow, postProfileUnfollow } from '../actions/api'
import HeaderNormal from '../components/HeaderNormal'
import { SHIP_GREY, SHADOW_GRADIENT } from '../constants/Colors'
import ProfilePeopleListTab from './ProfilePeopleListTab'
import style from './profile/v2/ProfileScreen/style'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  idTimeline: getParam('idTimeline', 0)
})

class LikesScreen extends React.Component {
  state = {
    people: [],
    isReady: false,
    isRefreshing: false
  }

  async componentDidMount() {
    await this._getInitialScreenData({ limit: 100 })
  }

  _getInitialScreenData = async (...args) => {
    await this._getLikes(...args)
    this.setState({
      isReady: true,
      isRefreshing: false
    })
  }

  _getLikes = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const {
      userData: { id_user },
      idTimeline,
      dispatch
    } = this.props
    const dataResponse = await dispatch(getFeedLikes, {
      id_timeline: idTimeline,
      id_viewer: id_user, ...params
    }, noop, false, isLoading)
    await this.setState({ people: dataResponse.user_like })
  }

  _onFollowed = async (follow, destinationId, isLoading = true) => {
    const {
      userData: { id_user },
      dispatch
    } = this.props
    const currentAction = follow ? postProfileFollow : postProfileUnfollow
    const currentPayload = follow ? { id_user: destinationId, followed_by: id_user } : {
      id_user,
      id_user_following: destinationId
    }
    await dispatch(currentAction, currentPayload, noop, true, isLoading)
  }

  render() {
    const {
      userData: { id_user: idViewer },
      navigateTo,
      isLoading,
      navigateBack,
    } = this.props
    const {
      people,
      isReady
    } = this.state
    return (
      <Container
        scrollable
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={async () => {
                navigateBack()
              }}
              withExtraPadding
              text='Menyukai Post ini'
              centered
            />
            <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
          </View>
        )}
      >
        <ProfilePeopleListTab
          isEmpty={false}
          emptyMessage={'No likes available'}
          dataSource={people}
          navigateTo={navigateTo}
          type={'following'}
          isMine={true}
          idViewer={idViewer}
          onFollow={this._onFollowed}
        />
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(LikesScreen),
  mapFromNavigationParam
)
