import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { split, isEmpty } from 'lodash-es'
import { View, BackHandler } from 'react-native'
import * as FileSystem from 'expo-file-system'
import { connect } from 'react-redux'
import ImageViewer from 'react-native-image-zoom-viewer'
import moment from 'moment'
import { BLACK, WHITE } from '../constants/Colors'
import Header from '../components/Header'
import Icon from '../components/Icon'
import Container from '../components/Container'
import _enhancedNavigation from '../navigation/_enhancedNavigation'
import Text from '../components/Text'
import Modal from '../components/Modal'
import { WP2, WP20, WP3, WP5 } from '../constants/Sizes'
import { ListItem } from '../components'
import { saveToCameraRoll } from '../utils/media'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  images: getParam('images', []),
  withIndicator: getParam('withIndicator', true)
})

class ImagePreviewScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 0,
      labelVisible: true
    }
  }

  componentDidMount = () => {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
    setTimeout(() => this.setState({ labelVisible: false }), 5000)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  _onChange = (index) => {
    this.setState({ index })
  }

  _onClick = async () => {
    await this.setState({ labelVisible: !this.state.labelVisible })
    setTimeout(() => this.setState({ labelVisible: false }), 5000)
  }

  _saveToCameraRoll = async (data) => {
    const { url } = data
    // eslint-disable-next-line no-useless-escape
    const base64 = url.split(/^(data\:image.+)base64\,/)
    try {
      if (base64.length > 1) {
        const content = base64[base64.length - 1]
        const fileName = data.fileName || `${content.substr(content.length - 5)}.jpg`
        const cacheUri = `${FileSystem.cacheDirectory}${fileName}`
        await FileSystem.writeAsStringAsync(cacheUri, content, { encoding: FileSystem.EncodingType.Base64 })
        await saveToCameraRoll(cacheUri)
      } else {
        const names = split(url, '/')
        const fileName = names[names.length - 1]
        const { uri } = await FileSystem.downloadAsync(url, `${FileSystem.cacheDirectory}${fileName}`)
        await saveToCameraRoll(uri)
      }
    } catch (e) {
      //
    }
  }

  _noIndicator = () => null

  render() {
    const {
      images,
      withIndicator
    } = this.props

    const {
      index,
      labelVisible
    } = this.state
    const imagesValid = images.filter((item) => !isEmpty(item.url))

    return (
      <Container
        colors={[BLACK, BLACK]}
        isReady={true}
        isLoading={false}
        theme='light'
      >
        <Modal
          renderModalContent={({ toggleModal }) => (
            <View>
              <ListItem
                inline onPress={() => {
                  toggleModal()
                  this._saveToCameraRoll(images[index])
                }}
              >
                <Icon size='large' name='download' />
                <Text style={{ paddingLeft: WP2 }}>Save to phone</Text>
              </ListItem>
            </View>
          )}
        >
          {({ toggleModal }, M) => (
            <View style={{ flex: 1, paddingBottom: WP20 }}>
              <Header>
                <Icon onPress={this._backHandler} size='small' color={WHITE} name='close' />
                {
                  imagesValid.length > 0 && (
                    <Icon
                      size='small'
                      style={{ alignSelf: 'center' }} onPress={toggleModal} type='Entypo' color={WHITE}
                      name='dots-three-vertical'
                    />
                  )
                }
              </Header>
              {
                imagesValid.length > 0 ? (
                  <ImageViewer
                    imageUrls={images}
                    index={index}
                    onChange={this._onChange}
                    saveToLocalByLongPress={false}
                    onClick={this._onClick}
                    renderIndicator={withIndicator ? undefined : this._noIndicator}
                  />
                ) : (
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text size='mini' color={WHITE} weight={400}>Images Not Found</Text>
                  </View>
                )
              }
              {
                labelVisible &&
                <View
                  style={{ position: 'absolute', left: 0, bottom: 0, paddingHorizontal: WP5, paddingVertical: WP3 }}
                >
                  {images[index].label && <Text color={WHITE} weight={400}>{images[index].label}</Text>}
                  {images[index].timeStamp &&
                    <Text color={WHITE} weight={200}>{moment(images[index].timeStamp).format('HH:mm')}</Text>}
                </View>
              }
              {M}
            </View>
          )}
        </Modal>
      </Container>
    )
  }
}

ImagePreviewScreen.propTypes = {
  images: PropTypes.array,
  withIndicator: PropTypes.bool
}

ImagePreviewScreen.defaultProps = {
  images: [],
  withIndicator: true
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(ImagePreviewScreen),
  mapFromNavigationParam
)
