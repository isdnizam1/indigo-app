import React from 'react'
import { View, KeyboardAvoidingView, Platform } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { isEmpty } from 'lodash-es'
import { connect } from 'react-redux'
import { Container, Header, Icon, Text, InputText, Button, Form, _enhancedNavigation } from '../components'
import { PINK_RED_DARK, ORANGE_BRIGHT_DARK, WHITE, WHITE20 } from '../constants/Colors'
import { HP5, WP5, HP4 } from '../constants/Sizes'
import { alertMessage } from '../utils/alert'
import { setUserId } from '../utils/storage'
import { postRegister } from '../actions/api'
import { GET_USER_DETAIL } from '../services/auth/actionTypes'
import { GET_NOTIFICATION } from '../services/notification/actionTypes'
import { registerUser } from '../utils/registerUser'

const mapDispatchToProps = {
  setUserDetailDispatcher: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  }),
  setNewNotification: (notification) => ({
    type: `${GET_NOTIFICATION}_PUSHER`,
    response: notification
  })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class RegisterPersonalScreen extends React.Component {

  _onRegisterPersonal = ({ formData }) => {
    const {
      dispatch,
      navigateTo,
      setUserDetailDispatcher
    } = this.props

    if (
      isEmpty(formData.full_name) ||
      isEmpty(formData.email) ||
      isEmpty(formData.password) ||
      isEmpty(formData.password_confirmation)
    ) alertMessage(null, 'Please fill the form correctly')
    else {
      dispatch(postRegister, formData, async (dataResponse) => {
        await setUserId(dataResponse.id_user.toString())
        await registerUser(dataResponse.id_user.toString(), this.props)
        setUserDetailDispatcher(dataResponse)
      })
        .then(async (dataResponse) => {
          navigateTo('RegisterJobScreen', { initialState: true })
        })
    }
  }

  render() {
    const {
      isLoading,
      navigateBack
    } = this.props

    return (
      <Container isLoading={isLoading} colors={[ORANGE_BRIGHT_DARK, PINK_RED_DARK]} theme='light'>
        <Header>
          <Icon
            onPress={() => {
              navigateBack()
            }} size='massive' color={WHITE} name='left'
          />
        </Header>
        <KeyboardAwareScrollView keyboardShouldPersistTaps='handled'>
          <View style={{ marginHorizontal: WP5 }}>
            <Text centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>Register</Text>
            <Form onSubmit={this._onRegisterPersonal}>
              {({ onChange, onSubmit }) => (

                <KeyboardAvoidingView
                  behavior={Platform.OS === 'ios' ? 'padding' : null}
                  style={{ marginVertical: HP4 }}
                >
                  <InputText
                    label='Full Name'
                    onChangeText={onChange('full_name')}
                    returnKeyType='next'
                  />
                  <InputText
                    label='Email'
                    onChangeText={onChange('email')}
                    returnKeyType='next'
                  />
                  <InputText
                    label='Password'
                    onChangeText={onChange('password')}
                    returnKeyType='next'
                    secureTextEntry toggleSecureTextEntry
                  />
                  <InputText
                    label='Confirm Password'
                    onChangeText={onChange('password_confirmation')}
                    secureTextEntry toggleSecureTextEntry
                  />
                  <Button
                    onPress={onSubmit}
                    style={{ marginTop: HP5 }}
                    rounded
                    centered
                    shadow='none'
                    backgroundColor={WHITE20}
                    textColor={WHITE}
                    text='Next'
                  />
                </KeyboardAvoidingView>
              )}
            </Form>
          </View>
        </KeyboardAwareScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(null, mapDispatchToProps)(RegisterPersonalScreen),
  mapFromNavigationParam
)
