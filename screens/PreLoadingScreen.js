import React from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import { _enhancedNavigation } from 'sf-components'
import InteractionManager from 'sf-utils/InteractionManager'

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({})

// Somehow this empty screen needed to prevent blank screen on the next (LoadingScreen) screen
class PreLoadingScreen extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this._showLoading()
  }

  _showLoading = () => {
    this.props.navigateTo('LoadingScreen')
  };

  render() {
    return <View />
  }
}

export default _enhancedNavigation(
  connect(null, mapDispatchToProps)(PreLoadingScreen),
  mapFromNavigationParam,
)
