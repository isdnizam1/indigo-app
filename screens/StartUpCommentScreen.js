import React, { createRef } from 'react'
import { RefreshControl, View } from 'react-native'
import { noop, get } from 'lodash-es'
import { connect } from 'react-redux'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { _enhancedNavigation, Container, FeedItem } from '../components'
import { WHITE, REDDISH } from '../constants/Colors'
import { deleteJourney, getAdsDetail } from '../actions/api'
import HeaderNormal from '../components/HeaderNormal'
import CommentSection from '../components/comment/CommentSection'
import { WP5 } from '../constants/Sizes'
import StartUpCommentSection from '../components/comment/StartUpCommentSection'

const mapStateToProps = ({ auth, song: { playback, soundObject } }) => ({
  userData: auth.user,
  playback,
  soundObject,
})

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam('id_ads', 0),
  onRefresh: getParam('onRefresh', () => {}),
})

class StartUpCommentScreen extends React.Component {
  static navigationOptions = () => ({
    gesturesEnabled: true,
  });

  state = {
    feed: {
      type: 'post',
    },
    isReady: false,
    comments: [],
    isRefreshing: false,
    bottomReached: false,
  };

  // scrollRef = createRef();
  _commentOffset = 0;

  componentDidMount() {
    this._getInitialScreenData()
  }

  componentWillUnmount() {
    const { soundObject, playback } = this.props
    playback.isLoaded && soundObject.pauseAsync()
  }

  _getInitialScreenData = () => {
    this._getAdsDetail()
    this.setState({
      isReady: true,
      isRefreshing: false,
    })
  };

  _getAdsDetail = async () => {
    const {
      dispatch,
      id_ads,
      userData,
      navigateTo,
      navigateBack,
      isStartup,
      onRefresh
    } = this.props
    const { isLoading } = this.state
    await dispatch(
      getAdsDetail,
      { id_user: userData.id_user, id_ads },
      noop,
      false,
      isLoading,
    ).then(async (ads) => {
      console.log(`ads ${JSON.stringify(ads.comment)}`)
      this.setState({
        feed: ads,
        comments: ads.comment
      })
    })
    onRefresh()
  }

  _onPressTopic = (topic) => {};

  render() {
    const {
      isLoading,
      userData,
      dispatch,
      navigateBack,
      id_ads,
      is1000Startup,
    } = this.props
    const { feed, comments, isReady, isRefreshing } = this.state
    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={() => navigateBack()}
            text={'Comments'}
            centered
          />
        )}
      >
        <View style={{ flex: 1 }}>
          <StartUpCommentSection
            bottomReached={this.state.bottomReached}
            releaseBottomReachedState={() =>
              this.setState({ bottomReached: false })
            }
            navigateTo={this.props.navigateTo}
            userData={userData}
            dispatch={dispatch}
            comments={comments}
            getAdsDetail={this._getAdsDetail}
            commentCount={feed.total_comment}
            feedId={id_ads}
            onDeleteComment={this._getFeed}
            onGetFeed={this._getFeed}
            totalComment={get(feed, 'total_comment') || 0}
          />
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(StartUpCommentScreen),
  mapFromNavigationParam,
)
