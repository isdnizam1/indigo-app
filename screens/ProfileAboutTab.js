import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { withNavigation } from '@react-navigation/compat'
import { isEmpty, startCase, omit, toPairs, camelCase } from 'lodash-es'
import { Text, ProfileCard, Modal, Icon, Loader } from '../components'
import { GREY20, GREY80, GREY_WARM, GREY_WHITE, NO_COLOR, PURPLE, WHITE } from '../constants/Colors'
import { HP08, HP1, HP105, HP2, WP05, WP2, WP4 } from '../constants/Sizes'
import { TOUCH_OPACITY } from '../constants/Styles'

const NAV_PROFILE_PROGRESS = {
  'about_me': 'ProfileUpdateAboutScreen',
  'interest': 'RegisterGenreInterestScreen',
  'basic_info': 'ProfileUpdateBasicScreen'
}

class ProfileAboutTab extends React.Component {
  _isNotEmptyBasicInfo = () => {
    const { user } = this.props
    return (
      !isEmpty(user.gender) ||
      !isEmpty(user.birthdate) ||
      !isEmpty(user.location) ||
      !isEmpty(user.email) ||
      !isEmpty(user.website)
    )
  }

  _renderProfileProgress = (onPress) => {
    const { userProgress } = this.props
    const totalUserProgressComplete = Number(userProgress.completed)
    const totalUserProgressPaused = Number(userProgress.not_completed)
    const totalProgress = totalUserProgressComplete + totalUserProgressPaused
    const progress = []
    for (let i = 1; i <= totalProgress; i++) {
      progress.push(
        <View
          key={i}
          style=
            {{
              flex: 1 / totalProgress,
              height: HP105,
              marginHorizontal: WP05,
              backgroundColor: i <= totalUserProgressComplete ? PURPLE : WHITE,
              borderColor: PURPLE,
              borderWidth: 1,
              borderTopEndRadius: i === totalProgress ? 5 : 0,
              borderBottomEndRadius: i === totalProgress ? 5 : 0,
              borderTopStartRadius: i === 1 ? 5 : 0,
              borderBottomStartRadius: i === 1 ? 5 : 0
            }}
        />
      )
    }
    const Container = onPress ? TouchableOpacity : View
    return (
      <Container
        activeOpacity={TOUCH_OPACITY}
        onPress={onPress}
        style={{
          borderRadius: 5, backgroundColor: WHITE,
          marginHorizontal: WP2, marginVertical: HP1,
          paddingHorizontal: WP4, paddingTop: HP2
        }}
      >
        <Text type='KotoriRose'>Complete your profile</Text>
        <View
          style={{ marginHorizontal: -WP05, flexDirection: 'row', justifyContent: 'space-between', marginTop: HP1 }}
        >
          {progress}
        </View>
        <Icon centered color={PURPLE} name='chevron-down' type='Entypo'/>
      </Container>
    )
  }

  _renderProfileProgressDetail = (toggleModal) => {
    const { userProgress, navigateTo, refreshProfile } = this.props
    const totalUserProgressComplete = Number(userProgress.completed)
    const totalUserProgressPaused = Number(userProgress.not_completed)
    const totalProgress = totalUserProgressComplete + totalUserProgressPaused
    const progressDetailValue = omit(userProgress, ['completed', 'not_completed'])
    const progressDetailEntries = toPairs(progressDetailValue)
    const progressDetail = []
    for (let i = 0; i < totalProgress; i++) {
      const currentProgressEntry = progressDetailEntries[i]
      const title = startCase(camelCase(currentProgressEntry[0]))
      const isComplete = currentProgressEntry[1]
      progressDetail.push(
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY} key={i}
          style={{
            flexDirection: 'row', alignItems: 'center', marginVertical: HP2
          }}
          onPress={() => {
            toggleModal()
            navigateTo(NAV_PROFILE_PROGRESS[currentProgressEntry[0]], { isEditing: true, refreshProfile })
          }}
        >
          <Icon
            color={isComplete ? PURPLE : GREY20} size='massive' centered name='ios-checkmark-circle-outline'
            type='Ionicons'
          />
          <View style={{ alignItems: 'flex-start', flexGrow: 1, marginHorizontal: WP4 }}>
            <Text color={isComplete ? PURPLE : GREY80} type='KotoriRose' size='large'>{title}</Text>
          </View>
          <Icon color={GREY_WARM} centered name='right'/>
        </TouchableOpacity>
      )
    }

    return (
      <View
        style={{
          borderRadius: 5, backgroundColor: WHITE,
          marginHorizontal: WP2, marginVertical: HP1,
          paddingHorizontal: WP4, paddingVertical: HP1
        }}
      >
        {progressDetail}
      </View>
    )
  }

  render() {
    const {
      user,
      userProgress,
      isMine,
      navigateTo,
      refreshProfile,
      isTabReady,
      isActiveTab
    } = this.props
    return (
      !isActiveTab ? null :
        isTabReady ? (
          <View style={{ backgroundColor: GREY_WHITE, flexGrow: 1, paddingVertical: HP1 }}>
            {
              Number(userProgress.not_completed) > 0 && isMine && (
                <Modal
                  position='center'
                  style={{
                    backgroundColor: NO_COLOR
                  }}
                  modalStyle={{ margin: 0 }}
                  renderModalContent={({ toggleModal, payload }) => (
                    <View>
                      {
                        this._renderProfileProgress()
                      }
                      {
                        this._renderProfileProgressDetail(toggleModal)
                      }
                    </View>
                  )}
                >
                  {({ toggleModal }, M) => (
                    <View>
                      {
                        this._renderProfileProgress(toggleModal)
                      }
                      {M}
                    </View>
                  )}
                </Modal>
              )
            }
            <ProfileCard
              header='About me'
              action={() => navigateTo('ProfileUpdateAboutScreen', { refreshProfile })}
              isMine={isMine}
              content={
                !isEmpty(user.about_me)
                  ? (
                    <View style={{ paddingHorizontal: WP4, paddingVertical: HP2 }}>
                      <Text>{user.about_me}</Text>
                    </View>
                  ) : null
              }
            />
            <ProfileCard
              header='Interest'
              action={() => navigateTo('RegisterGenreInterestScreen', { isEditing: true, refreshProfile })}
              isMine={isMine}
              content={
                !isEmpty(user.interest)
                  ? (
                    <View style={{
                      paddingHorizontal: WP2,
                      paddingVertical: HP1,
                      flexDirection: 'row',
                      flexWrap: 'wrap',
                      width: '100%'
                    }}
                    >
                      {user.interest.map((interest, i) => {
                        return (
                          <View
                            key={i} style={{
                              borderWidth: 1,
                              borderColor: '#fd451a',
                              marginVertical: HP1,
                              marginHorizontal: WP2,
                              borderRadius: 100
                            }}
                          >
                            <Text style={{
                              color: '#fd451a',
                              paddingHorizontal: WP2,
                              paddingVertical: HP1
                            }}
                            >{interest.interest_name}</Text>
                          </View>
                        )
                      })}
                    </View>
                  ) : null
              }
            />
            <ProfileCard
              header='Basic Info'
              action={() => navigateTo('ProfileUpdateBasicScreen', { refreshProfile })}
              isMine={isMine}
              content={
                this._isNotEmptyBasicInfo() && (
                  <View style={{ paddingHorizontal: WP4, paddingVertical: HP2 }}>
                    {
                      !isEmpty(user.gender) && (
                        <View>
                          <Text size='mini'>Gender</Text>
                          <Text>{user.gender}</Text>
                          <View style={{ paddingVertical: HP08 }}/>
                        </View>
                      )
                    }
                    <View>
                      <Text size='mini'>Birthdate</Text>
                      <Text>{isEmpty(user.birthdate) ? '-' : user.birthdate}</Text>
                      <View style={{ paddingVertical: HP08 }}/>
                    </View>
                    {
                      !isEmpty(user.email) && (
                        <View>
                          <Text size='mini'>Email</Text>
                          <Text>{user.email}</Text>
                          <View style={{ paddingVertical: HP08 }}/>
                        </View>
                      )
                    }
                    <View>
                      <Text size='mini'>Website</Text>
                      <TouchableOpacity activeOpacity={TOUCH_OPACITY} disabled={true}>
                        <Text>{isEmpty(user.website) ? '-' : user.website}</Text>
                      </TouchableOpacity>
                      <View style={{ paddingVertical: HP08 }}/>
                    </View>
                  </View>
                )
              }
            />
          </View>
        ) : (
          <View style={{ flexGrow: 1, backgroundColor: WHITE }}>
            <Loader isLoading/>
          </View>
        )
    )
  }
}

export default withNavigation(ProfileAboutTab)
