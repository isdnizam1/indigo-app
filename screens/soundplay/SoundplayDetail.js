import React, { Component, Fragment } from 'react'
import { get, noop } from 'lodash-es'
import { connect } from 'react-redux'
import {
  AppState,
  BackHandler,
  Image as RNImage,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  View,
} from 'react-native'
import HTMLElement from 'react-native-render-html'
import { LinearGradient } from 'expo-linear-gradient'
import * as Notifications from 'expo-notifications'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import {
  getAdsDetail,
  getSubscriptionDetail,
  postLogSoundfenPlayEpisode,
  getListEpisodePodcast,
  postEpisodePodcastStatus,
} from 'sf-actions/api'
import { isIOS } from 'sf-utils/helper'
import Audio from 'sf-utils/audio'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import { Container, PremiumPopup, SoundplayPlayer, Text } from 'sf-components'
import {
  HP1,
  HP2,
  HP6,
  WP100,
  WP308,
  WP4,
  WP90,
  WP5,
  WP6,
  WP2,
  WP3,
} from 'sf-constants/Sizes'
import {
  GREY_CALM,
  GREY_WARM,
  GUN_METAL,
  NAVY_DARK,
  REDDISH,
  SHIP_GREY_CALM,
  WHITE,
} from 'sf-constants/Colors'
import style from 'sf-styles/soundplay/SoundplayDetail'
import {
  actions,
  localNotification,
  notificationCategories,
} from 'sf-utils/soundplay'
import HeaderNormal from 'sf-components/HeaderNormal'
import { paymentDispatcher } from 'sf-services/payment'
import Icon from 'sf-components/Icon'
import SoundfrenExploreOptions from 'sf-components/explore/SoundfrenExploreOptions'
import Modal from 'sf-components/Modal'
import { speakerDisplayName } from 'sf-utils/transformation'
import { ScrollView } from 'react-native'
import Badge from 'sf-components/Badge'
import moment from 'moment'
import learnOptions from '../learn/LearnOptionMenus'
import { BLUE_CHAT, BLUE_CHAT20, GREEN_20, GREEN_30, PALE_GREY, PALE_GREY_THREE, TRANSPARENT } from '../../constants/Colors'
import { WP1, WP7, HP5, HP100 } from '../../constants/Sizes'
import { postScreenTimeAdsHelper } from '../../utils/helper'
import { postLike } from '../../actions/api'
import Touchable from '../../components/Touchable'
import CounterButton from '../../components/feed/CounterButton'


const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
  paymentSourceIdAdsSet: paymentDispatcher.paymentSourceIdAdsSet,
}

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam('id_ads', null),
  id_episode: getParam('id_episode', null),
  isStartup: getParam('isStartup', false),
  refreshListLearn: getParam('refreshListLearn', () => {}),
})

const dateFormat = 'YYYY-MM-DD HH:mm:ss'

if (Platform.OS === 'android') {
  Notifications.setNotificationChannelAsync('soundfrenplay', {
    name: 'Soundfren Podcast',
    sound: false,
    priority: 'max',
    vibrate: [0, 0, 0, 0],
    android: {
      sound: false,
      vibrate: [0, 0, 0, 0],
    },
  })
}

const soundObject = new Audio.Sound()

class SoundplayDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isPremium: false,
      accountType: null,
      sp: null,
      showFullDescription: false,
      showPlayer: false,
      theme: 'light',
      statusBarBackground: '#3b659c',
      episodeIndex: null,
      showPremiumPopup: false,
      played: [],
      isPlaybackPlaying: false,
      showMiniPlayer: false,
      latestEpisodeIndex: null,
      viewForm: null,
      appState: AppState.currentState
    }
    this._didFocusSubscription = props.navigation.addListener('focus', () => {
      // console.log('isfocus')
      this.setState({ viewFrom: moment().format(dateFormat) })
      AppState.addEventListener('change', this._handleAppStateChange)
    }
    )
    this.getCurrentSubscription = this.getCurrentSubscription.bind(this)
    this.getAlbumDetail = this.getAlbumDetail.bind(this)
    this.renderDescription = this.renderDescription.bind(this)
    this.renderEpisode = this.renderEpisode.bind(this)
    this.onLinkPress = this.onLinkPress.bind(this)
    this.togglePlayer = this.togglePlayer.bind(this)
    this.playByIndex = this.playByIndex.bind(this)
    this.onUpgrade = this.onUpgrade.bind(this)
    this.addNotificationListener = this.addNotificationListener.bind(this)
    Notifications.dismissAllNotificationsAsync()
  }

  // eslint-disable-next-line react/sort-comp
  addNotificationListener = async (notification) => {
    const actionId = notification.actionIdentifier
    const { sp, episodeIndex } = this.state
    if (actionId && sp) {
      if (actions.map((action) => action.identifier).indexOf(actionId) >= 0) {
        if (actionId.indexOf('spPause') === 0) {
          await soundObject.pauseAsync()
          localNotification(sp, episodeIndex, 'spCategory3')
          setIsPlaybackPlaying(false)
        }
        if (actionId.indexOf('spPlay') === 0) {
          await soundObject.playAsync()
          localNotification(
            sp,
            episodeIndex,
            episodeIndex == 0 ? 'spCategory1' : 'spCategory2',
          )
          setIsPlaybackPlaying(true)
        }
        if (actionId.indexOf('spPrev') === 0) {
          let prevEpisode = episodeIndex - 1
          if (prevEpisode < 0) prevEpisode = 0
          let episode = sp.additional_data.episodeList[prevEpisode]
          if (!episode.is_premium || isPremium) this.playByIndex(prevEpisode)
          else
            this.setState(
              { showPremiumPopup: true, episodeIndex: null, showPlayer: false },
              () => {
                soundObject.pauseAsync()
                setIsPlaybackPlaying(false)
              },
            )
        } else if (actionId.indexOf('spNext') === 0) {
          let nextEpisode = episodeIndex + 1
          if (nextEpisode === sp.additional_data.episodeList.length) nextEpisode = 0
          let episode = sp.additional_data.episodeList[nextEpisode]
          if (!episode.is_premium || isPremium) this.playByIndex(nextEpisode)
          else
            this.setState(
              { showPremiumPopup: true, episodeIndex: null, showPlayer: false },
              () => {
                soundObject.pauseAsync()
                setIsPlaybackPlaying(false)
              },
            )
        }
      }
    }
  };

  async componentDidMount() {
    const {
      userData: { id_user },
      navigateBack,
      navigateTo,
    } = this.props
    if (!id_user) {
      navigateBack()
      navigateTo('AuthNavigator')
    } else {
      try {
        await notificationCategories.map(async (category) => {
          await Notifications.deleteNotificationCategoryAsync(category.id)
        })
      } finally {
        await notificationCategories.map(async (category) => {
          await Notifications.setNotificationCategoryAsync(
            category.id,
            category.actions,
          )
        })
        if (!isIOS()) {
          Notifications.addNotificationResponseReceivedListener(
            this.addNotificationListener,
          )
        }
        await this.getCurrentSubscription()
        await this.getAlbumDetail()
      }
    }

    this._willBlurSubscription = this.props.navigation.addListener('blur', () => {
      // console.log('isBlur')
      this._postScreenTimeAds()
      if (this.props.refreshListLearn) {
        this.props.refreshListLearn()
      }
      AppState.removeEventListener('change', this._handleAppStateChange)
    }
    )
  }

  async componentWillUnmount() {
    try {
      soundObject.unloadAsync()
    } catch (err) {
      /*silent is gold*/
    } finally {
      Notifications.dismissAllNotificationsAsync()
    }
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _didFocusSubscription;
  _willBlurSubscription;


  _handleAppStateChange = (nextAppState) => {
    // console.log(AppState.currentState)
    if (AppState.currentState === 'background') {
      soundObject && soundObject.pauseAsync()
      this._postScreenTimeAds()
    }
    if ((this.state.appState === 'inactive' || this.state.appState === 'background') && nextAppState === 'active') {
      this.setState({ viewFrom: moment().format(dateFormat) })
      // console.log('back to foreground', this.state.viewFrom)
    }
    this.setState({ appState: nextAppState })
    // console.log({ appState: this.state.appState })
  }

  _postScreenTimeAds = () => {
    const { isLoading, viewFrom } = this.state
    const { id_ads, userData: { id_user }, dispatch, isStartup } = this.props
    // console.log({ id_ads, id_user, viewFrom })
    postScreenTimeAdsHelper({ viewFrom, id_ads, id_user, dispatch, isLoading, isStartup })
  }

  async onUpgrade() {
    await this.togglePlayer(false)
    this.props.paymentSourceSet('soundfren_play')
    this.props.paymentSourceIdAdsSet(this.props.id_ads)
    await this.props.navigateTo('MembershipScreen')
  }

  playByIndex = async (episodeIndex) => {
    const { sp, isPremium, played, showMiniPlayer } = this.state
    const { isStartup } = this.props
    const episode = sp.additional_data.episodeList[episodeIndex]
    if (
      !(isPremium || sp.additional_data.episodeList[episodeIndex].is_premium != 1) &&
      sp.additional_data.episodeList[episodeIndex].is_premium == 1 &&
      !isPremium
    ) {
      this.setState({ showPremiumPopup: true })
      return
    }
    // console.log('played_status', sp.additional_data.episodeList[episodeIndex].played_status)
    if (isStartup) {
      this.actionPodcastPlaying(episode.id_episode, 'playing')
    }
    await this.togglePlayer(true)
    if (!showMiniPlayer)
      this.setState({ showMiniPlayer: true })
    const {
      dispatch,
      id_ads,
      userData: { id_user },
    } = this.props
    const episodeList = get(sp, 'additional_data.episodeList')
    const id_episode = episodeList[episodeIndex].id_episode
    dispatch(postLogSoundfenPlayEpisode, { id_ads, id_episode, id_user })
    this.setState({ episodeIndex, played: [...played, id_episode] })
  };

  togglePlayer = async (showPlayer) => {
    setTimeout(() => {
      this.setState({ showPlayer })
    }, 200)
  };

  onLinkPress = (event, href) =>
    this.props.navigateTo('BrowserScreenNoTab', { url: href });

  renderDescription() {
    let {
      sp: { description },
      showFullDescription,
    } = this.state
    if (showFullDescription) {
      return (
        <TouchableOpacity
          onPress={() =>
            this.setState({ showFullDescription: !this.state.showFullDescription })
          }
          activeOpacity={TOUCH_OPACITY}
        >
          <HTMLElement
            textSelectable
            containerStyle={{
              width: WP90,
            }}
            html={`<div class="description">${description
              .trim()
              .replace(/(\r\n|\n|\r|)/gm, '')
              .replace(
                /<[/]([a-z])> /g,
                ' </$1>',
              )} <b class="readless">Read Less</b></div>`}
            classesStyles={{
              readless: {
                fontWeight: '400',
                fontFamily: 'CircularMedium',
                color: REDDISH,
                fontSize: WP308,
              },
            }}
            onLinkPress={this.onLinkPress}
            baseFontStyle={{
              fontWeight: '300',
              fontFamily: 'CircularBook',
              color: SHIP_GREY_CALM,
              fontSize: WP308,
            }}
          />
        </TouchableOpacity>
      )
    } else {
      let plainDescription = description
        .replace(/<\/?[^>]+(>|$)/g, '')
        .replace(/\s+/g, ' ')
        .trim()
      return (
        <Text
          color={SHIP_GREY_CALM}
          type={'Circular'}
          style={{ width: WP90 }}
          onPress={() =>
            this.setState({ showFullDescription: !this.state.showFullDescription })
          }
          size={'mini'}
        >
          {plainDescription.substring(0, 140)}
          {plainDescription.length > 140 ? '... ' : null}
          {plainDescription.length > 140 && (
            <Text type={'Circular'} color={REDDISH} weight={400} size={'mini'}>
              {'\n\n'}Read More
            </Text>
          )}
        </Text>
      )
    }
  }

  renderEpisode = ({ title, link, speaker_name, length, is_premium, played_status }, index) => {
    const { isPremium, sp, episodeIndex, isPlaybackPlaying } = this.state
    const { isStartup } = this.props
    const isSelected = episodeIndex === index
    return (
      <TouchableOpacity
        onPress={() => {
          if (isPremium || is_premium != 1) this.playByIndex(index)
          else if (!isPremium && is_premium == 1)
            this.setState({ showPremiumPopup: true })
        }}
        style={[style.episodeList, index > 0 ? style.episodeBorder : null]}
        key={Math.random()}
        activeOpacity={TOUCH_OPACITY}
      >
        <View style={{ flexDirection: 'row' }}>
          <View
            style={{
              width: sp.additional_data.episodeList.length >= 10 ? WP6 + 1 : WP5,
            }}
          >
            <Text color={isSelected ? REDDISH : GUN_METAL} type={'Circular'} weight={500} size={'mini'}>
              {index + 1}.
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text
              numberOfLines={1}
              color={isSelected ? REDDISH : GUN_METAL}
              type={'Circular'}
              weight={500}
              size={'mini'}
              style={style.episodeTitle}
            >
              {title.trim()}
            </Text>

            <View style={{ flexDirection: 'row', marginTop: WP1 }} >
              {isStartup && this._renderBadgePodcastStatus(played_status)}
              <Text style={{ alignSelf: 'center' }} color={GREY_WARM} type={'Circular'} size={'xmini'}>
                {parseInt(length)} mins
              </Text>
            </View>
            {is_premium == 1 && (
              <View style={style.premiumBadge}>
                <Text
                  color={SHIP_GREY_CALM}
                  weight={400}
                  type={'Circular'}
                  size={'xmini'}
                >
                  Premium
                </Text>
              </View>
            )}
          </View>
          <View style={{ alignSelf: 'center', width: WP7, height: WP7, borderRadius: WP7 * 2, backgroundColor: isSelected ? REDDISH : PALE_GREY, justifyContent: 'center' }}>
            <Icon
              color={isSelected ? WHITE : SHIP_GREY_CALM}
              size='mini'
              name={isSelected ? isPlaybackPlaying ? 'pause' : 'play' : 'play'}
              type='Ionicons'
              style={{ alignSelf: 'center' }}
            />
          </View>
        </View>
      </TouchableOpacity>
    )
  };

  async getCurrentSubscription() {
    const { userData, dispatch } = this.props

    const { package_type } = await dispatch(getSubscriptionDetail, {
      id_user: userData.id_user,
    })
    let isPremium = package_type && package_type !== 'free'
    this.setState({
      isPremium,
      accountType: isPremium ? package_type.toLowerCase() : 'free',
    })
  }

  async getAlbumDetail() {
    const {
      userData: { id_user },
      id_ads,
      dispatch,
      id_episode,
      isStartup,
    } = this.props

    // console.log({ id_user, id_ads })

    if (isStartup) {
      let spStartup = await dispatch(getListEpisodePodcast, { id_ads, id_user })
      spStartup = {
        ...spStartup,
        additional_data: {
          episodeList: spStartup.episodeList
        }
      }

      if (spStartup.latestPlay) {
        const latestEpisodeIndex = spStartup.additional_data.episodeList
          .map((episode) => episode.id_episode)
          .indexOf(spStartup.latestPlay.id_episode)
        this.setState({ latestEpisodeIndex })

      }
      this.setState({
        sp: spStartup, // record time when ads successfully loaded
      })

      return
    }
    // not startup
    let sp = await dispatch(getAdsDetail, { id_ads, id_user })
    sp.additional_data = JSON.parse(sp.additional_data)
    this.setState({ sp })
    if (id_episode) {
      const findIndex = sp.additional_data.episodeList
        .map((episode) => episode.id_episode)
        .indexOf(id_episode)
      findIndex >= 0 && this.playByIndex(findIndex)
    }
  }

  // actionPodcastPlaying = (status, id_episode) =>{
  actionPodcastPlaying = async (id_episode, status) => {
    // const status = 'playing'
    // const id_episode = 4
    const {
      userData: { id_user },
      id_ads,
      dispatch,
    } = this.props
    const body = { id_user, id_ads, id_episode, status }

    let podcastStatus = await dispatch(postEpisodePodcastStatus, body)
    // console.log({ podcastStatus })

    this.getAlbumDetail()
  }

  _renderBadgePodcastStatus(played_status) {
    // console.log({played_status})
    const podcastStatus = (played_status.replace('-', ' ')).replace(/(^\w|\s\w)/g, (m) => m.toUpperCase())
    const textColor = podcastStatus === 'Playing' ? BLUE_CHAT : podcastStatus === 'Played' ? GREEN_30 : SHIP_GREY_CALM
    const color = podcastStatus === 'Playing' ? BLUE_CHAT20 : podcastStatus === 'Played' ? GREEN_20 : PALE_GREY_THREE
    return (
      <View style={{ alignSelf: 'flex-start', flexDirection: 'row' }}>
        <Badge
          textSize='xmini'
          radius={6}
          textColor={textColor}
          color={color}
        >
          {podcastStatus}
        </Badge>
        <View style={{
          width: 4,
          height: 4,
          borderRadius: 8,
          backgroundColor: GREY_WARM,
          alignSelf: 'center',
          marginHorizontal: 7
        }} />
      </View>
    )
  }

  setIsPlaybackPlaying = (isPlaying) => {
    this.setState({ isPlaybackPlaying: isPlaying })
    // console.log('isPlaybackPlaying', this.state.isPlaybackPlaying)
  }

  _onPressLike = () => {
    const {
      userData: { id_user },
      isStartup
    } = this.props
    const {
      sp
    } = this.state

    postLike({
      id_user,
      related_to: 'id_ads',
      id_related_to: sp.id_ads
    }).then(async (result) => {
      // console.log(`result ${JSON.stringify(result.data.result)}`)
      await this.getAlbumDetail()
    })
  }

  _renderHeaderRightModal = () => {
    const {
      userData: { id_user },
      isStartup,
    } = this.props
    const {
      sp
    } = this.state
    // console.log(`ads ${JSON.stringify(sp)}`)
    if (isStartup) {
      if (sp !== null) {
        return (
          <TouchableOpacity onPress={this._onPressLike}>
            <View style={{ padding: 15, justifyContent: 'center', alignItems: 'center' }}>
              <RNImage
                source={sp.is_liked
                  ? require('sf-assets/icons/active_heart.png')
                  : require('sf-assets/icons/outlineHeartWhite.png')
                }
                style={{ width: 24, height: 24 }}
              />
            </View>
          </TouchableOpacity>
        )
      }
    } else {
      return (
        <Modal
          position='bottom'
          swipeDirection='none'
          renderModalContent={({ toggleModal }) => {
            return (
              <SoundfrenExploreOptions
                menuOptions={learnOptions}
                userData={this.props.userData}
                id={this.props.id_ads}
                navigateTo={this.props.navigateTo}
                onClose={toggleModal}
                onShare={() =>
                  navigateTo('ShareScreen', {
                    id: this.props.id_ads,
                    type: 'explore',
                    title: 'Bagikan Podcast',
                  })
                }
              />
            )
          }}
        >
          {({ toggleModal }, M) => (
            <Fragment>
              <Icon
                style={{ marginRight: WP4, marginVertical: WP4 }}
                onPress={isStartup ? noop : toggleModal}
                background='dark-circle'
                size='large'
                color={isStartup ? TRANSPARENT : WHITE}
                name='dots-three-horizontal'
                type='Entypo'
              />
              {M}
            </Fragment>
          )}
        </Modal>
      )
    }
  }

  _renderCommentButton = () => {
    const { isLoading } = this.props
    const {
      sp
    } = this.state
    return (
      <Touchable
        onPress={this._onPressComment}
        style={{
          backgroundColor: WHITE,
          borderTopWidth: 1,
          borderTopColor: '#E4E6E7',
          padding: WP2,
        }}>
        <View style={{
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
          <CounterButton
            onPress={this._onPressComment}
            images={[null, require('sf-assets/icons/icComment.png')]}
            value={counterLabel(
              sp?.total_comment,
              'Komentar',
              'Komentar',
            )}
            disable={isLoading}
          />
          <Icon
            centered
            name={'chevron-thin-right'}
            type='Entypo'
            color={SHIP_GREY_CALM}
          />
        </View>
      </Touchable>
    )
  };

  _onPressComment = () => {
    const { navigateTo, onRefresh } = this.props
    const {
      sp
    } = this.state
    navigateTo('StartUpCommentScreen', {
      id_ads: sp.id_ads,
      onRefresh: onRefresh
    })
  }

  render() {
    const { navigateTo, navigateBack, userData, isStartup } = this.props
    const {
      theme,
      statusBarBackground,
      sp,
      showPremiumPopup,
      episodeIndex,
      accountType,
      isPremium,
      showPlayer,
      showMiniPlayer,
      latestEpisodeIndex,
    } = this.state
    return (
      <Container
        type={'vertical'}
        colors={['#3b659c', '#3b659c']}
        scrollBackgroundColor={WHITE}
        isLoading={!sp || !accountType}
        isReady={sp != null && accountType != null}
        scrollable={false}
        statusBarBackground={statusBarBackground}
        theme={theme}
        outsideContent={() => (
          <PremiumPopup
            title={'Opps...!'}
            message={'This episode is available for premium users.'}
            onClose={() => this.setState({ showPremiumPopup: false })}
            onPress={() =>
              this.setState({ showPremiumPopup: false }, this.onUpgrade)
            }
            visible={showPremiumPopup}
          />
        )}
        renderHeader={() => (
          <HeaderNormal
            textColor={WHITE}
            style={{ paddingVertical: 0, alignItems: 'center' }}
            iconLeftOnPress={() => {
              navigateBack()
            }}
            iconLeftColor={WHITE}
            iconLeftBackground={'dark-circle'}
            iconLeftBackgroundColor={'#FFFFFF22'}
            text='Album'
            textType='Circular'
            textWeight={700}
            noRightPadding
            rightComponent={this._renderHeaderRightModal()}
            centered
          />
        )}
      >
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior={Platform.OS === 'ios' ? 'padding' : null}
        >
          <ScrollView>
            {sp && (
              <View>
                <View style={style.wrapper}>
                  <View style={{ paddingBottom: 24 }}>
                    <LinearGradient
                      style={{
                        width: WP100,
                        alignItems: 'center',
                        paddingBottom: HP6,
                        paddingTop: HP2,
                      }}
                      colors={['#3b659c', '#1b252f', '#1a222b']}
                      start={[0, 0]}
                      end={[0, 1.4]}
                    >
                      <RNImage
                        resizeMode={'cover'}
                        style={style.poster}
                        source={{ uri: sp.image }}
                      />
                      <View style={{ width: WP90, alignItems: 'center' }}>
                        <Text
                          centered
                          type={'Circular'}
                          weight={500}
                          size={'small'}
                          color={WHITE}
                        >
                          {sp.title.split(': ').join(':\n')}
                        </Text>
                        {/* <Text
                          centered
                          type={'Circular'}
                          color={SHIP_GREY_CALM}
                          size={'mini'}
                          weight={400}
                          style={style.spSpeaker}
                        >
                          {speakerDisplayName(sp.speaker)}
                        </Text> */}
                      </View>
                      {/*<TouchableOpacity onPress={() => this.playByIndex(episodeIndex || 0)} activeOpacity={TOUCH_OPACITY} style={style.buttonWrapper}>
                    <LinearGradient
                      colors={['#292b6f', '#181a4d', '#090a2c']}
                      style={style.buttonPlay}
                    >
                      <RNImage resizeMode={'contain'} style={style.playIcon} source={require('sf-assets/icons/spPlay.png')} />
                      <Text weight={500} size={'xmini'} color={WHITE}>Play Album</Text>
                    </LinearGradient>
                  </TouchableOpacity>*/}
                    </LinearGradient>
                    <TouchableOpacity
                      onPress={() => this.playByIndex(episodeIndex || latestEpisodeIndex || 0)}
                      activeOpacity={TOUCH_OPACITY}
                      style={style.playButton}
                    >
                      <Text color={WHITE} type={'Circular'} weight={700}>
                        Play
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={[style.episodeWrapper]}>
                    <Text
                      centered
                      weight={700}
                      type={'Circular'}
                      size={'slight'}
                      color={NAVY_DARK}
                    >
                      Episode List
                    </Text>
                    <View style={{ marginBottom: HP1 }} />
                    {sp.additional_data.episodeList.map(this.renderEpisode)}
                    {/* {sp.additional_data.episodeList.map((item, index) => console.log({item}))} */}
                  </View>

                </View>
                <View
                  style={{
                    borderTopWidth: 1,
                    borderTopColor: GREY_CALM,
                    alignItems: 'center',
                    paddingBottom: showPremiumPopup ? 100 : 30,
                    backgroundColor: WHITE,
                  }}
                >
                  <View style={{ width: WP90 }}>
                    <Text
                      style={{ marginVertical: WP4 }}
                      type={'Circular'}
                      weight={700}
                      color={NAVY_DARK}
                    >
                      Tentang Podcast
                    </Text>
                    {this.renderDescription()}
                  </View>
                </View>
              </View>

            )}
          </ScrollView>
        </KeyboardAvoidingView>
        <View
          style={{
            // width: WP100,
          }}>
          {this._renderCommentButton()}

          <SoundplayPlayer
            idUser={this.props.userData.id_user}
            playByIndex={this.playByIndex}
            isPremium={isPremium}
            onUpgrade={this.onUpgrade}
            soundIndex={episodeIndex}
            soundObject={soundObject}
            sp={sp}
            setIsPlaybackPlaying={this.setIsPlaybackPlaying}
            togglePlayer={this.togglePlayer}
            visible={showPlayer}
            navigateTo={navigateTo}
            isStartup={isStartup}
            showMiniPlayer={showMiniPlayer}
            actionPodcastPlaying={this.actionPodcastPlaying}
          />
        </View>
      </Container>
    )
  }
}
const counterLabel = (value, label, empty) => {
  let labelText = value < 1 ? empty : label
  let valueText = value < 1 ? '' : `${value} `
  return `${valueText}${labelText}`
}
// const userBadge = {
//   free: require('sf-assets/icons/userFreeBadge.png'),
//   basic: require('sf-assets/icons/userBasicBadge.png'),
//   pro: require('sf-assets/icons/userProBadge.png'),
//   maestro: require('sf-assets/icons/userMaestroBadge.png')
// }

SoundplayDetail.propTypes = {}
SoundplayDetail.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SoundplayDetail),
  mapFromNavigationParam,
)
