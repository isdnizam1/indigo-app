import React from 'react'
import { View } from 'react-native'
import { WP05, WP375, WP3, WP105, WP9, WP44 } from '../../constants/Sizes'
import Image from '../../components/Image'
import Text from '../../components/Text'
import { WHITE_MILK, GREY_WARM } from '../../constants/Colors'
import { speakerDisplayName } from '../../utils/transformation'

const SoundplayItem = ({ ads, loading = true, badge = false, centered = false }) => {
  const title = loading ? 'Soundfren Learn Title' : ads.title,
    speaker = loading ? 'Speaker Name' : speakerDisplayName(ads.speaker),
    sizeItem = centered ? WP44 - WP3 : WP375
  return (
    <View style={{
      marginRight: WP3,
      marginLeft: 1,
      width: sizeItem,
    }}
    >
      <View style={{ height: sizeItem, width: sizeItem, backgroundColor: WHITE_MILK, borderRadius: 12 }}>
        {
          !loading && (
            <View>
              <View style={{
                borderRadius: 12,
                overflow: 'hidden',
              }}
              >
                <Image
                  source={{ uri: ads.image }}
                  imageStyle={{ height: sizeItem, aspectRatio: 1 }}
                />
                {badge && <Image size={WP9} style={{ position: 'absolute', right: WP105, top: WP105 }} aspectRatio={1} source={require('../../assets/icons/icHeadphone.png')} />}
              </View>
            </View>
          )
        }
      </View>
      <View style={{ marginTop: WP105, paddingHorizontal: WP05 }}>
        <Text centered={centered} numberOfLines={centered ? 2 : 1} size='tiny' type='NeoSans' weight={500}>{title}</Text>
        <Text centered={centered} numberOfLines={1} ellipsizeMode='tail' size='tiny' color={GREY_WARM}>{speaker}</Text>
      </View>
    </View>
  )
}

SoundplayItem.propTypes = {}

SoundplayItem.defaultProps = {}

export default SoundplayItem
