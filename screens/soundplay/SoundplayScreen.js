import React, { Component } from 'react'
import { connect } from 'react-redux'
import { FlatList, Image as RNImage, ScrollView, TouchableOpacity, View } from 'react-native'
import { isEmpty, noop } from 'lodash-es'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { getAds, getSettingDetail, getSubscriptionDetail } from '../../actions/api'
import { isIOS } from '../../utils/helper'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { Container, Image, Text } from '../../components'
import { WP135, WP2, WP3, WP44, WP6 } from '../../constants/Sizes'
import { GREY, WHITE } from '../../constants/Colors'
import HeaderNormal from '../../components/HeaderNormal'
import { paymentDispatcher } from '../../services/payment'
import SoundplayItem from './SoundplayItem'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
}

const mapFromNavigationParam = (getParam) => ({})

class SoundplayScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: false,
      accountType: null,
      categoryList: [],
      newestList: [],
    }
  }

  async componentDidMount() {
    await this._getDataNewest()
    await this._getDataCategory()
    await this._getCurrentSubscription()
  }

  _getCurrentSubscription = async () => {
    const { userData, dispatch } = this.props

    const { package_type } = await dispatch(
      getSubscriptionDetail,
      { id_user: userData.id_user },
      noop,
      false,
      true,
    )
    let isPremium = package_type && package_type !== 'free'
    this.setState({
      accountType: isPremium ? package_type.toLowerCase() : 'free',
    })
  };

  _getDataNewest = async () => {
    const { dispatch } = this.props
    try {
      const data = await dispatch(
        getAds,
        {
          status: 'active',
          category: 'sp_album',
          start: 0,
          limit: 5,
        },
        noop,
        true,
        true,
      )
      this.setState({ isReady: true })
      if (data.code === 200) {
        this.setState({ newestList: data.result })
      }
    } catch (e) {
      this.setState({ isReady: true })
    }
  };

  _getDataCategory = async (isLoading = true) => {
    const { dispatch } = this.props

    const dataResponse = await dispatch(
      getSettingDetail,
      {
        setting_name: 'sp_album',
        key_name: 'category',
      },
      noop,
      false,
      isLoading,
    )
    const category = JSON.parse(dataResponse.value)
    this.setState({ categoryList: category })
  };

  _onPullDownToRefresh = () => {
    this.setState(
      {
        categoryList: [],
        newestList: [],
      },
      this.componentDidMount,
    )
  };

  _sectionNewestAdd = () => {
    const { navigateTo } = this.props
    const data = this.state.newestList
    return (
      <View style={{ marginBottom: WP6 }}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingHorizontal: WP6,
            paddingTop: WP3,
          }}
        >
          <Text centered size='mini' color={GREY} weight={500} type='NeoSans'>
            Newest Added
          </Text>
        </View>
        <ScrollView
          horizontal
          style={{ flexGrow: 0 }}
          contentContainerStyle={{ paddingLeft: WP6, marginTop: WP3 }}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        >
          {data.map((ads, i) => (
            <TouchableOpacity
              style={{ paddingVertical: WP2 }}
              key={`${i}${new Date()}`}
              activeOpacity={TOUCH_OPACITY}
              onPress={() => navigateTo('SoundplayDetail', { id_ads: ads.id_ads })}
            >
              <SoundplayItem ads={ads} loading={false} />
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    )
  };

  _sectionCategory = () => {
    const { navigateTo } = this.props
    const data = this.state.categoryList
    return (
      <View style={{ marginBottom: WP6 }}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            paddingHorizontal: WP6,
          }}
        >
          <Text centered size='mini' color={GREY} weight={500} type='NeoSans'>
            Category
          </Text>
        </View>
        <FlatList
          contentContainerStyle={{
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'flex-start',
            paddingHorizontal: WP6,
          }}
          data={data}
          numColumns={2}
          keyExtractor={(item, index) => `key${index}`}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              onPress={() => navigateTo('SoundplayListAlbum', { category: item })}
              activeOpacity={TOUCH_OPACITY}
              style={{
                marginTop: WP6,
                marginLeft: index % 2 == 1 ? WP3 : 0,
              }}
            >
              <Image
                imageStyle={{ width: WP44 - WP3 }}
                aspectRatio={130 / 72}
                source={
                  !isEmpty(item.bg_image)
                    ? { uri: item.bg_image }
                    : require('../../assets/images/bgHeadphone.png')
                }
              />
              <View
                style={{
                  position: 'absolute',
                  left: 0,
                  top: 0,
                  bottom: 0,
                  right: 0,
                  flex: 1,
                  justifyContent: 'center',
                  paddingVertical: WP3,
                  paddingLeft: WP3,
                  paddingRight: WP135,
                }}
              >
                <Text size='tiny' type='NeoSans' weight={500} color={WHITE}>
                  {item.category_name}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    )
  };

  render() {
    const { navigateTo, navigateBack, isLoading, paymentSourceSet } = this.props
    const { isReady, accountType, newestList, categoryList } = this.state
    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={this._onPullDownToRefresh}
        isReady={isReady}
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            iconStyle={{ width: 94, flexDirection: 'row' }}
            text='Soundfren Learn'
            rightComponent={
              !isIOS() &&
              accountType && (
                <TouchableOpacity
                  activeOpacity={TOUCH_OPACITY}
                  onPress={() => {
                    if (accountType === 'free') {
                      paymentSourceSet('soundfren_play')
                      navigateTo('MembershipScreen')
                    } else {
                      navigateTo('MembershipScreen')
                    }
                  }}
                >
                  <RNImage
                    style={{ width: 94, height: 28 }}
                    resizeMode={'contain'}
                    source={userBadge[accountType]}
                  />
                </TouchableOpacity>
              )
            }
            centered
          />
        )}
      >
        <View>
          {!isEmpty(newestList) && this._sectionNewestAdd()}
          {!isEmpty(categoryList) && this._sectionCategory()}
        </View>
      </Container>
    )
  }
}

const userBadge = {
  free: require('../../assets/icons/userFreeBadge.png'),
  basic: require('../../assets/icons/userBasicBadge.png'),
  pro: require('../../assets/icons/userProBadge.png'),
  maestro: require('../../assets/icons/userMaestroBadge.png'),
}

SoundplayScreen.propTypes = {}

SoundplayScreen.defaultProps = {
  paymentSourceSet: noop,
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SoundplayScreen),
  mapFromNavigationParam,
)
