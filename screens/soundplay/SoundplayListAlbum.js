import React, { Component } from 'react'
import { connect } from 'react-redux'
import { FlatList, TouchableOpacity, View } from 'react-native'
import { noop } from 'lodash-es'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { getAds, getSettingDetail } from '../../actions/api'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { Container, Text } from '../../components'
import { HP1, HP40, WP100, WP3, WP6 } from '../../constants/Sizes'
import { GREY, WHITE } from '../../constants/Colors'
import HeaderNormal from '../../components/HeaderNormal'
import SoundplayItem from './SoundplayItem'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  category: getParam('category', {})
})

class SoundplayListAlbum extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isReady: false,
      albumList: [],
      albumLoading: true,
      category: this.props.category
    }
  }

  async componentDidMount() {
    if (!this.props.category.category_name) {
      await this._getCategory()
    }
    await this._getDataSPByCategory()
  }

  _getCategory = async () => {
    const {
      dispatch
    } = this.props

    const {
      category
    } = this.state

    const dataResponse = await dispatch(getSettingDetail, {
      'setting_name': 'sp_album',
      'key_name': 'category'
    }, noop, false, true)
    const categories = JSON.parse(dataResponse.value)
    const activeCategory = categories.find((item) => item.id_sp_album_category == category.id_sp_album_category)
    await this.setState({ category: activeCategory || {} })
  }

  _getDataSPByCategory = async () => {
    const {
      dispatch
    } = this.props

    const {
      category
    } = this.state

    this.setState({ albumLoading: true })
    try {
      const data = await dispatch(getAds, {
        status: 'active',
        category: 'sp_album',
        id_sp_album_category: category.id_sp_album_category
      }, noop, true, true)
      this.setState({ albumLoading: false, isReady: true })
      if (data.code === 200) {
        this.setState({ albumList: data.result })
      }
    } catch (e) {
      this.setState({ albumLoading: false, isReady: true })
    }
  }

  _onPullDownToRefresh = () => {
    this.setState({
      albumList: [],
    }, this.componentDidMount)
  }

  _sectionData = () => {
    const {
      navigateTo
    } = this.props

    const {
      category
    } = this.state

    const { albumList, albumLoading } = this.state
    return (
      <View style={{ marginBottom: WP6 }}>
        <View style={{
          flex: 1,
          alignItems: 'center',
          paddingHorizontal: WP6,
          paddingTop: WP3
        }}
        >
          <Text centered size='mini' color={GREY} weight={500} type='NeoSans'>{category.category_name}</Text>
        </View>
        <FlatList
          contentContainerStyle={{
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'flex-start',
          }}
          data={albumList}
          numColumns={2}
          keyExtractor={(item, index) => `key${index}`}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              onPress={() => navigateTo('SoundplayDetail', { id_ads: item.id_ads })}
              activeOpacity={TOUCH_OPACITY}
              style={{
                marginTop: WP6,
                marginLeft: WP6
              }}
            >
              <SoundplayItem ads={item} loading={albumLoading} centered />
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            !albumLoading && (
              <View style={{ width: WP100, alignItems: 'center', justifyContent: 'center', paddingHorizontal: WP6, paddingVertical: HP40 }}>
                <Text centered size='mini' color={GREY} type='NeoSans'>{'It’s empty here,\n coming soon audio content.'}</Text>
              </View>
            )
          }
        />
      </View>
    )
  }

  render() {
    const { navigateBack, isLoading } = this.props
    const {
      isReady,
    } = this.state
    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={this._onPullDownToRefresh}
        isReady={isReady}
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            style={{ paddingVertical: HP1 }}
            iconLeftOnPress={navigateBack}
            text='Daftar Album'
            centered
          />
        )}
      >
        {this._sectionData()}
      </Container>
    )
  }

}

SoundplayListAlbum.propTypes = {}

SoundplayListAlbum.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(SoundplayListAlbum),
  mapFromNavigationParam
)
