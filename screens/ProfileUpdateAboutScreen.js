import React from 'react'
import { ScrollView, TextInput, View, TouchableOpacity, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash-es'
import { _enhancedNavigation, Container, Header, Icon, Text, Form } from '../components'
import { ORANGE_BRIGHT, WHITE } from '../constants/Colors'
import { FONT_SIZE, WP4 } from '../constants/Sizes'
import { TOUCH_OPACITY } from '../constants/Styles'
import { alertMessage } from '../utils/alert'
import { postProfileUpdateAbout } from '../actions/api'
import { authDispatcher } from '../services/auth'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', () => {})
})

class ProfileUpdateAboutScreen extends React.Component {
  _onUpdateAbout = ({ formData }) => {
    const {
      userData: { id_user },
      navigateBack,
      refreshProfile,
      dispatch,
      getUserDetailDispatcher,
    } = this.props
    if(
      isEmpty(formData.aboutMe)
    ) alertMessage(null, 'Please fill the form correctly')
    else {
      dispatch(postProfileUpdateAbout, { 'about_me': formData.aboutMe, id_user })
        .then(() => {
          getUserDetailDispatcher({ id_user })
          refreshProfile()
          navigateBack()
        })
    }
  }

  render() {
    const {
      userData,
      isLoading,
      navigateBack
    } = this.props
    return (
      <Container isLoading={isLoading} colors={[WHITE, WHITE]}>
        <Form initialValue={{ aboutMe: userData.about_me }} onSubmit={this._onUpdateAbout}>
          {({ onChange, onSubmit, formData, isDirty }) => (
            <View style={{ flex: 1 }}>
              <Header>
                <Icon onPress={() => navigateBack()} size='massive' color={ORANGE_BRIGHT} name='close'/>
                <Text size='massive' type='SansPro' weight={500}>About Me</Text>
                <TouchableOpacity
                  disabled={!isDirty} onPress={() => {
                    Keyboard.dismiss()
                    onSubmit()
                  }} activeOpacity={TOUCH_OPACITY}
                >
                  <Text size='large' color={ORANGE_BRIGHT}>Save</Text>
                </TouchableOpacity>
              </Header>
              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1, paddingHorizontal: WP4 }}
              >
                <TextInput
                  multiline
                  style={{
                    fontSize: FONT_SIZE['small'],
                  }}
                  value={formData.aboutMe}
                  autoFocus
                  onChangeText={onChange('aboutMe')}
                  placeholder='Write yours...'
                />
              </ScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProfileUpdateAboutScreen),
  mapFromNavigationParam
)
