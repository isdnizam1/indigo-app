import React from 'react'
import { connect } from 'react-redux'
import { BackHandler } from 'react-native'
import { _enhancedNavigation, Container, Header, Icon, Text } from '../components'
import { DEFAULT_PAGING } from '../constants/Routes'
import { authDispatcher } from '../services/auth'
import { GET_USER_DETAIL } from '../services/auth/actionTypes'
import { ORANGE_BRIGHT } from '../constants/Colors'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setUserDetail: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class _blueprint extends React.Component {
  state = {
    isReady: false,
    isRefreshing: false,
  }

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false),
  })

  async componentDidMount() {
    await this._getInitialScreenData()
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _getInitialScreenData = async (...args) => {
    await this._getData(...args)
    this.setState({
      isReady: true,
      isRefreshing: false,
    })
  }

  _getData = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {}

  _backHandler = async () => {
    this.props.onRefresh()
    //this.props.navigateTo('TimelineScreen')
  }

  render() {
    const {
      isLoading,
    } = this.props
    const {
      isReady
    } = this.state
    return (
      <Container
        scrollable
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <Header>
            <Icon onPress={this._backHandler} size='massive' color={ORANGE_BRIGHT} name='left'/>
          </Header>
        )}
      >
        <Text>_blueprint</Text>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(_blueprint),
  mapFromNavigationParam
)
