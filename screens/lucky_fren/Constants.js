export const REWARD = {
  point: {
    id: [11, 12, 13],
    message: (point) => (
      [
        { style: 'normal', text: 'Selamat! kamu mendapatkan ' },
        { style: 'bold', text: `${point} Poin Tambahan ` },
        { style: 'normal', text: 'dari ' },
        { style: 'bold', text: 'Hadiah Lucky Box. ' },
        { style: 'normal', text: 'Ambil & kumpulkan pointnya!' }
      ]
    )
  },
  voucher: {
    id: [9, 10],
    message: (name) => (
      [
        { style: 'normal', text: 'Selamat! kamu mendapatkan ' },
        { style: 'bold', text: name },
        { style: 'normal', text: 'dari ' },
        { style: 'bold', text: 'Hadiah Lucky Box. ' },
        { style: 'normal', text: 'Ambil & kumpulkan pointnya!' }
      ]
    )
  }
}

export const COIN_REWARD_VALUE = {
  11: 15,
  12: 25,
  13: 50
}

export const CONFIRMATION_MESSAGE = {
  INSUFFICIENT_POINT: {
    title: 'Poin kamu masih kurang nih..',
    content: 'Yuk ikuti misi untuk mendapatkan poin tambahan!',
    confirmLabel: 'Ikuti Misi',
    cancelLabel: 'Kembali'
  },
  CLAIM_REWARD: {
    title: 'Dapatkan hadiah ini',
    content: 'Apakah kamu yakin ingin menukarkan pointmu untuk mendapatkan hadiah ini?',
    confirmLabel: 'Ya, Tukar Sekarang',
    cancelLabel: 'Nanti Saja'
  },
}

export const VIDEO_CREATIVE_CONTENT = [
  'Buat video kreatif dengan durasi minimal 1 menit dengan tema Soundfren. Kamera yang digunakan untuk membuat video bebas (HP, Mirrorless, DSLR, dsb).',
  'Video di upload di Instagram pribadi kamu dan pastikan akun kamu tidak dalam kondisi “private” agar dapat di repost oleh tim Soundfren.',
  'Post video kamu dengan caption bebas dan dengan hastag #Soundfren #Findyourfren'
]

export const TnC_CONTENT = [
  'Satu ID pengguna hanya dapat mengklaim satu hadiah.',
  'Jumlah hadiah terbatas atau selama persedian masih ada.',
  'Pengguna dapat mengklaim dan share hadiah jika ingin menukarkan star dengan hadiah yang ada.',
  'Pemenang yang berhasil mengklaim hadiah akan dihubungi oleh pihak Soundfren untuk mengkonfirmasi nama, alamat (digunakan sebagai tujuan pengiriman hadiah), nomor handphone dan alamat e-mail dalam waktu 3x24 jam.',
  'Nomor yang akan menghubungi adalah nomor official Soundfren dan email official Soundfren (contact center: 081310957673 / contact service: hello@soundfren.id).',
  'Apabila dalam 3 hari pemenang tidak dapat dihubungi, maka hadiah dianggap batal/hangus.',
  'Hadiah akan dikirimkan kepada pemenang dalam waktu maksimal 14 hari setelah melakukan konfirmasi data diri.',
  'Hadiah digital dikirim maksimal 3x24 jam.',
  'Program ini hanya bisa diikuti oleh pengguna yang telah terdaftar di aplikasi Soundfren.',
  'Pengguna punya kesempatan untuk mengumpulkan Star dengan mengikuti login harian, misi harian dan misi spesial.'
]
