import React, { Component } from 'react'
import { View, FlatList } from 'react-native'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import { connect } from 'react-redux'
import Container from 'sf-components/Container'
import Text from 'sf-components/Text'
import InteractionManager from 'sf-utils/InteractionManager'
import { get, isEmpty, noop, sortBy } from 'lodash-es'
import { WP105, WP5, WP4, WP6, WP3, WP2, WP1 } from 'sf-constants/Sizes'
import { GUN_METAL, SHADOW_GRADIENT } from 'sf-constants/Colors'
import Spacer from 'sf-components/Spacer'
import HeaderNormal from 'sf-components/HeaderNormal'
import { LinearGradient } from 'expo-linear-gradient'
import { HEADER } from 'sf-constants/Styles'
import Touchable from 'sf-components/Touchable'

import produce from 'immer'
import style from '../../styles/register'
import RankingItem from '../../components/lucky_fren/RankingItem'
import { getLuckyFrenLeaderBoard } from '../../actions/api'
import { Loader } from '../../components'

class ListRankingScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: { leaderboard: [] },
      loading: false,
      shouldShowLoading: true,
    }
    this._getContent = this._getContent.bind(this)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._getContent)
  }

  _getContent() {
    const {
      is1000Startup,
      userData: { id_user },
    } = this.props
    const { loading } = this.state
    !loading &&
      this.setState(
        {
          loading: true,
        },
        () => {
          getLuckyFrenLeaderBoard({
            id_user,
            start: this.state.data.leaderboard.length,
            limit: 10,
            ...(is1000Startup ? { platform: '1000Startup' } : {}),
          })
            .then(({ data: { result: data } }) => {
              this.setState(
                produce((draft) => {
                  draft.shouldShowLoading = !isEmpty(data.leaderboard)
                  data.leaderboard.map((item) => {
                    draft.data.leaderboard.push(item)
                  })
                  draft.data.full_name = data.full_name
                  draft.data.leaderboard_position = data.leaderboard_position
                  draft.data.profile_picture = data.profile_picture
                  draft.data.total_point = data.total_point
                  draft.data.id_user = data.id_user
                }),
              )
            })
            .catch((err) => {
              __DEV__ && console.log({ err })
            })
            .then(() => {
              this.setState({ loading: false })
            })
        },
      )
  }

  _renderLeaderBoardItem = ({ item, index }) => {
    return (
      <Touchable
        onPress={() =>
          this.props.navigateTo('StartUpProfileScreen', { idUser: item.id_user })
        }
      >
        <View
          style={{ paddingVertical: WP105, paddingHorizontal: WP6 }}
          activeOpacity={0.75}
          onPress={noop}
        >
          <RankingItem
            loading={false}
            image={item?.profile_picture}
            name={item?.full_name}
            position={item?.position}
            point={item?.total_point}
            latest_position={item?.latest_position}
          />
        </View>
      </Touchable>
    )
  };

  _renderHeader = () => {
    return (
      <View>
        <HeaderNormal
          style={style.headerNormal}
          centered
          iconLeftOnPress={this.props.navigateBack}
          textType={'Circular'}
          textSize={'mini'}
          iconStyle={{ marginRight: WP5 }}
          text={'Perolehan Poin'}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
      </View>
    )
  };

  render() {
    const { data, loading, shouldShowLoading } = this.state

    return (
      <Container renderHeader={this._renderHeader} scrollable={false}>
        <FlatList
          ListHeaderComponent={() => {
            return (
              <View>
                <Spacer size={WP4} />
                <View
                  style={{ paddingVertical: WP105, paddingHorizontal: WP6 }}
                  activeOpacity={0.75}
                  onPress={noop}
                >
                  <RankingItem
                    loading={!get(data, 'full_name')}
                    image={get(data, 'profile_picture')}
                    name={get(data, 'full_name')}
                    position={get(data, 'leaderboard_position')}
                    point={get(data, 'total_point')}
                    latest_position={'constant'}
                  />
                </View>
                <Spacer size={WP1} />
                <View style={{ paddingHorizontal: WP6, paddingVertical: WP3 }}>
                  <Text type={'Circular'} color={GUN_METAL} weight={600}>
                    Peringkat Sementara
                  </Text>
                </View>
              </View>
            )
          }}
          ListFooterComponent={() => (
            <View>
              <Spacer size={WP2} />
              {shouldShowLoading && loading && (
                <Loader isLoading={loading} size={'mini'} />
              )}
              <Spacer size={WP4} />
            </View>
          )}
          renderItem={this._renderLeaderBoardItem}
          onEndReached={this._getContent}
          data={sortBy(get(data, 'leaderboard'), (item) => parseInt(item.position))}
        />
      </Container>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  is1000Startup: getParam('is1000Startup', false),
})

export default _enhancedNavigation(
  connect(mapStateToProps, null)(ListRankingScreen),
  mapFromNavigationParam,
)
