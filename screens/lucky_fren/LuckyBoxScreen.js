import React, { Component } from 'react'
import { connect } from 'react-redux'
import { get, isEmpty, noop } from 'lodash-es'
import { TouchableOpacity, View } from 'react-native'
import { SHIP_GREY } from 'sf-constants/Colors'
import { WP2, WP1 } from 'sf-constants/Sizes'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { setData } from '../../services/screens/lv1/lucky-fren/actionDispatcher'
import Text from '../../components/Text'
import Container from '../../components/Container'
import HeaderNormal from '../../components/HeaderNormal'
import { getGiftBox, postOpenLuckyBox } from '../../actions/api'
import Image from '../../components/Image'
import Icon from '../../components/Icon'
import {
  GUN_METAL,
  PALE_BLUE_TWO,
  REDDISH,
  SHIP_GREY_CALM,
  WHITE,
} from '../../constants/Colors'
import { WP12, WP3, WP4, WP5, WP6, WP75 } from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'
import PopUpInfoTrigger from '../../components/popUp/PopUpInfoTrigger'
import ButtonV2 from '../../components/ButtonV2'
import Point from '../../components/lucky_fren/Point'
import { COIN_REWARD_VALUE, REWARD } from './Constants'

const STYLES = {
  listWrapper: {
    padding: WP5,
  },
  itemWrapper: {
    ...SHADOW_STYLE.shadow,
    shadowOpacity: 0.07,
    backgroundColor: WHITE,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: WP3,
    marginBottom: WP4,
    borderRadius: 10,
  },
  itemImage: {
    marginRight: WP3,
  },
}

class LuckyBoxScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      rewards: [],
      templatePopUp: {},
    }
  }

  componentDidMount = () => {
    this.getLuckyBox()
  }

  getLuckyBox = async () => {
    const {
      dispatch,
      userData: { id_user },
      is1000Startup,
    } = this.props
    const rewards = await dispatch(
      getGiftBox,
      is1000Startup ? { id_user, platform: '1000Startup' } : { id_user },
    )
    this.setState({ rewards })
  }

  emptyScreen = () => {
    const { navigateTo } = this.props
    return (
      <View style={{ alignItems: 'center', paddingHorizontal: WP6 }}>
        <Image
          source={require('sf-assets/images/empty-lucky-box.png')}
          size={WP75}
          aspectRatio={0.94}
          style={{ marginBottom: WP12 }}
        />
        <View style={{ marginBottom: WP6, paddingHorizontal: WP6 }}>
          <Text
            type='Circular'
            weight={400}
            color={GUN_METAL}
            centered
            style={{ marginBottom: WP3 }}
          >
            Kotak Hadiah Kosong
          </Text>
          <Text
            type='Circular'
            weight={300}
            color={SHIP_GREY_CALM}
            centered
            size='mini'
          >
            Ikuti misi dan kumpulkan poin untuk medapatkan hadiah yang kamu inginkan!
          </Text>
        </View>
        <ButtonV2
          text='Lihat Misi'
          color={REDDISH}
          borderColor={REDDISH}
          textColor={WHITE}
          onPress={() => navigateTo('MissionScreen', { is1000Startup: true })}
        />
      </View>
    )
  }

  _openLuckyBox = async (id_reward_claimed) => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props
    const result = await dispatch(postOpenLuckyBox, { id_user, id_reward_claimed })

    const isVoucher = REWARD.voucher.id.includes(Number(result.id_reward))

    let onPress = noop
    if (isVoucher) {
      onPress = () => this._onPressVoucher(result)
    }

    const point = COIN_REWARD_VALUE[Number(result.id_reward)]
    let templatePopUp = {
      closeColor: PALE_BLUE_TWO,
      image: { uri: result.image },
      aspectRatio: 296 / 184,
      title: isVoucher ? result.reward_name : `Mendapat ${point} Poin Tambahan`,
      multipleContent: true,
      content: isVoucher
        ? REWARD.voucher.message(result.reward_name)
        : REWARD.point.message(point),
      cta: isVoucher ? 'Lihat Voucher' : 'Ambil Poin',
      onPress,
    }
    this.setState({ templatePopUp }, this.popUp._show())
    this.getLuckyBox()
  }

  _onPressVoucher = (reward) => {
    const { navigateTo } = this.props
    const addData = JSON.parse(reward.additional_data)
    const { id_voucher_code } = addData
    navigateTo('VoucherDetailScreen', { id_voucher_code })
  }

  _onPressReward = (reward) => () => {
    const { navigateTo } = this.props
    const id_reward = Number(reward.id_reward)

    if (id_reward === 14) {
      // lucky box
      this._openLuckyBox(reward.id_reward_claimed)
    } else if (REWARD.voucher.id.includes(id_reward)) {
      // voucher
      this._onPressVoucher(reward)
    } else {
      // reward
     navigateTo('RewardDetailScreen', { id_reward, is1000Startup: this.props.is1000Startup })
    }
  }

  rewardList = () => {
    const { rewards } = this.state
    return (
      <View style={STYLES.listWrapper}>
        {rewards.map((item, idx) => {
          let desc = null
          if (item.type === 'lucky_box') desc = 'Buka & dapatkan hadiahnya'
          if (item.type === 'gift_box') desc = 'Klik untuk gunakan voucher'
          return (
            <TouchableOpacity
              style={STYLES.itemWrapper}
              key={idx}
              onPress={this._onPressReward(item)}
            >
              <Image source={{ uri: item.image }} style={STYLES.itemImage} />
              <View style={{ flex: 1, paddingLeft: WP2 }}>
                <Text
                  size={'slight'}
                  numberOfLines={1}
                  type='Circular'
                  weight={500}
                  color={GUN_METAL}
                >
                  {item.reward_name}
                </Text>
                {item.type === 'reward' && (
                  <View style={{ flexDirection: 'row' }}>
                    <Image
                      size='xtiny'
                      style={{ marginRight: WP1 }}
                      source={require('sf-assets/icons/icPoint.png')}
                    />
                    <Text
                      type={'Circular'}
                      size={'xmini'}
                      color={SHIP_GREY}
                      style={{ marginTop: WP1 }}
                    >
                      {get(item, 'point')} Poin
                    </Text>
                  </View>
                )}
                {!!desc && (
                  <Text
                    type={'Circular'}
                    size={'xmini'}
                    color={SHIP_GREY}
                    style={{ marginTop: WP1 }}
                  >
                    {desc}
                  </Text>
                )}
              </View>
              <Icon
                type='Entypo'
                name='chevron-right'
                centered
                color={SHIP_GREY_CALM}
              />
            </TouchableOpacity>
          )
        })}
      </View>
    )
  }

  render() {
    const { isLoading, navigateBack } = this.props
    const { rewards, templatePopUp } = this.state
    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={() => navigateBack()}
            text='Kotak Hadiah'
            centered
            shadow
          />
        )}
        scrollable
      >
        {isEmpty(rewards) ? this.emptyScreen() : this.rewardList()}
        <PopUpInfoTrigger
          ref={(ref) => {
            this.popUp = ref
          }}
          template={templatePopUp}
        />
      </Container>
    )
  }
}

LuckyBoxScreen.propTypes = {}

LuckyBoxScreen.defaultProps = {}

const mapStateToProps = ({
                           auth,
                           screen: {
                             lv1: { luckyfren },
                           },
                         }) => ({
  userData: auth.user,
  luckyfren,
})

const mapDispatchToProps = {
  setData,
}

const mapFromNavigationParam = (getParam) => ({
  is1000Startup: getParam('is1000Startup', false),
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(LuckyBoxScreen),
  mapFromNavigationParam,
)
