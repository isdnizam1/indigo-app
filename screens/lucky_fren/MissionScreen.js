/* eslint-disable react/sort-comp */
import React, { Component } from 'react'
import { View, Animated } from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import Touchable from 'sf-components/Touchable'
import { noop } from 'lodash-es'
import { setData } from '../../services/screens/lv1/lucky-fren/actionDispatcher'
import {
  _enhancedNavigation,
  Container,
  Text,
  HeaderNormal,
} from '../../components'
import { NO_COLOR, PALE_WHITE, NAVY_DARK, SHIP_GREY } from '../../constants/Colors'
import { WP5, WP6, HP3, WP40, HP2 } from '../../constants/Sizes'
import InteractionManager from '../../utils/InteractionManager'
import CustomTabController from '../../components/CustomTabController'
import MessageBarClearBlue from '../../components/messagebar/MessageBarClearBlue'
import { getLuckyFrenCategory } from '../../actions/api'
import MissionItem from '../../components/lucky_fren/MissionItem'

const missionConfig = {
  daily: {
    title: 'Misi Harian',
  },
  special: {
    title: 'Misi Spesial',
  },
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  setData,
}

const mapFromNavigationParam = (getParam) => ({
  category: getParam('category', 'daily'),
  isExpired: getParam('isExpired', false),
  is1000Startup: getParam('is1000Startup', false),
})

class MissionScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
      scrollY: new Animated.Value(0),
      isLoading: false,
      templatePopUp: {},
      daily: null,
      dailyLoading: true,
      special: null,
      specialLoading: true,
      index: props.category === 'daily' ? 0 : 1,
      routes: [
        { key: 'daily', title: 'MISI HARIAN' },
        { key: 'special', title: 'MISI SPESIAL' },
      ],
    }
  }

  componentDidMount = () => {
    InteractionManager.runAfterInteractions(() => {
      this._getContent()
    })
  };

  _getContent = async () => {
    const { dispatch, is1000Startup } = this.props
    dispatch(
      getLuckyFrenCategory,
      is1000Startup
        ? { category: 'special', platform: '1000startup' }
        : { category: 'special' },
      noop,
      true,
      false,
    ).then(({ result }) => {
      if (result) {
        this.setState({ special: result, specialLoading: false })
      }
    })
    dispatch(
      getLuckyFrenCategory,
      is1000Startup
        ? { category: 'daily', platform: '1000startup' }
        : { category: 'daily' },
      noop,
      true,
      false,
    ).then(({ result }) => {
      if (result) {
        this.setState({ daily: result, dailyLoading: false })
      }
    })
  };

  _headerSection = () => {
    const { navigateBack } = this.props
    return (
      <HeaderNormal
        iconLeftSize='small'
        iconLeftOnPress={navigateBack}
        centered
        text='Misi'
      />
    )
  };

  _renderScene = ({ route }) => {
    const { navigateTo, isExpired } = this.props
    const data = this.state[route.key] || [1, 2]
    const totalPoint = data.reduce(function (a, b) {
      return Number(a || 0) + Number(b.free_point || 0)
    }, 0)
    const loading = this.state[`${route.key}Loading`]

    return (
      <View style={{ marginHorizontal: WP5, marginVertical: HP3 }}>
        <Text
          lineHeight={WP6}
          type='Circular'
          size='small'
          color={NAVY_DARK}
          weight={600}
        >
          {missionConfig[route.key].title}
        </Text>
        <Text
          lineHeight={WP6}
          type='Circular'
          size='mini'
          color={SHIP_GREY}
          style={{ marginTop: HP2 }}
        >
          Dapatkan tambahan hingga
          <Text
            lineHeight={WP6}
            type='Circular'
            size='mini'
            color={SHIP_GREY}
            weight={500}
          >{` ${totalPoint} point `}</Text>
          dengan menyelesaikan misi dibawah ini
        </Text>
        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
          {data.map((item) => (
            <Touchable
              key={item.id_mission_category}
              onPress={() =>
                navigateTo('MissionCategoryDetailScreen', {
                  idMission: item.id_mission_category,
                  isExpired,
                })
              }
            >
              <MissionItem
                loading={loading}
                isExpired={this.props.isExpired}
                style={{ marginTop: HP2 }}
                width={WP40}
                image={item.image}
              />
            </Touchable>
          ))}
        </View>
      </View>
    )
  };

  render() {
    const { isLoading, index, routes } = this.state

    return (
      <Container isLoading={isLoading} statusBarBackground={NO_COLOR}>
        <View style={{ flex: 1, backgroundColor: PALE_WHITE }}>
          <NavigationEvents onDidFocus={this._getContent} />
          {this._headerSection()}
          <View>
            <CustomTabController
              animationDuration={100}
              onPress={this._onChangeTab}
              activeIndex={index}
              routes={routes}
            />
            <MessageBarClearBlue />
          </View>
          {this._renderScene({ route: routes[index] })}
        </View>
      </Container>
    )
  }

  _onChangeTab = (index) => {
    this.setState({ index })
  };
}

MissionScreen.propTypes = {}

MissionScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(MissionScreen),
  mapFromNavigationParam,
)
