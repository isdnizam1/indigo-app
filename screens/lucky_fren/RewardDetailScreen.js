import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Animated, StatusBar as StatusBarRN, View } from 'react-native'
import { isEmpty, noop } from 'lodash-es'
import * as Animatable from 'react-native-animatable'
import ExpoConstants from 'expo-constants'
import { WP35 } from 'sf-constants/Sizes'
import { LinearGradient } from 'expo-linear-gradient'
import { setData } from '../../services/screens/lv1/lucky-fren/actionDispatcher'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { getRewardDetail } from '../../actions/api'
import Container from '../../components/Container'
import Text from '../../components/Text'
import Image from '../../components/Image'
import {
  FONT_SIZE,
  WP05,
  WP1,
  WP100,
  WP15,
  WP2,
  WP20,
  WP3,
  WP305,
  WP4,
  WP5,
  WP6,
  WP7,
} from '../../constants/Sizes'
import {
  GREEN_30,
  GUN_METAL,
  NO_COLOR,
  PALE_BLUE,
  PALE_GREY_THREE,
  PALE_WHITE,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  TOMATO,
  WHITE,
} from '../../constants/Colors'
import Point from '../../components/lucky_fren/Point'
import Icon from '../../components/Icon'
import ButtonV2 from '../../components/ButtonV2'
import { SHADOW_STYLE } from '../../constants/Styles'
import { daysRemaining } from '../../utils/date'
import MissionItemCard from '../../components/lucky_fren/MissionItemCard'
import Touchable from '../../components/Touchable'
import { getNotifMappingAction } from '../../components/notification/NotifAction'
import ConfirmationPopup from '../../components/popUp/ConfirmationPopUp'
import FollowItem from '../../components/follow/FollowItem'
import { getStatusBarHeight } from '../../utils/iphoneXHelper'
import { FONTS } from '../../constants/Fonts'
import ContentPopUp from '../../components/popUp/ContentPopUp'
import { CONFIRMATION_MESSAGE, VIDEO_CREATIVE_CONTENT } from './Constants'

const StatusBar = Animatable.createAnimatableComponent(StatusBarRN)
const STYLES = {
  section: {
    borderBottomColor: PALE_BLUE,
    borderBottomWidth: 0.75,
    paddingVertical: WP6,
    paddingHorizontal: WP5,
  },
  headerSection: {
    marginBottom: WP2,
  },
  winnerContainer: {
    ...SHADOW_STYLE.shadowSoft,
    backgroundColor: WHITE,
  },
  winnerSection: {
    borderBottomColor: PALE_GREY_THREE,
    borderBottomWidth: 0.75,
  },
  winnerHeaderWrapper: {
    paddingHorizontal: WP5,
    paddingVertical: WP4,
    flexDirection: 'row',
    alignItems: 'center',
  },
}

class RewardDetailScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      reward: {},
      user: {},
      isReady: false,
      claimRewardPopUp: false,
      insufficientPointPopUp: false,
      scrollY: new Animated.Value(0),
    }
    this.videoCreatifContent = React.createRef()
  }

  componentDidMount = () => {
    this._getRewardDetail()
  }

  _getRewardDetail = async () => {
    const {
      dispatch,
      id_reward,
      userData: { id_user },
    } = this.props
    const reward = await dispatch(getRewardDetail, { id_reward, id_user })
    // reward.user.claimed = true
    this.setState({
      reward: reward.detail,
      user: reward.user,
      isReady: true,
    })
  }

  _onClaim = () => {
    const { reward, user } = this.state
    if (Number(user.total_point) >= Number(reward.total_point) && this._isMissionTargetComplete()) {
      this.setState({ claimRewardPopUp: true })
    } else {
      this.setState({ insufficientPointPopUp: true })
    }
  }

  _isMissionTargetComplete = () => {
    const {
      reward: { mission_target },
    } = this.state
    if (!mission_target) return true

    for (let i = 0; i < mission_target.length; i++) {
      if (Number(mission_target[i].completed_quantity) < Number(mission_target[i].quantity)) {
        return false
      }
    }

    return true
  }

  _onPressMissionItem = async (item) => {
    const { navigateTo } = this.props
    if (item.link === 'video_creative') {
      this.videoCreatifContent.openModal()
    } else {
      const notifAction = await getNotifMappingAction({
        url_mobile: 'internal_link',
        url: item.link,
      })
      if (notifAction) {
        navigateTo(notifAction.to, notifAction.payload)
      }
    }
  }

  _renderMissionTarget = () => {
    const { reward } = this.state
    const { isExpired } = this.props
    return (
      <View style={STYLES.section}>
        <Text type='Circular' color={GUN_METAL} size='slight'>
          Ikuti langkah ini & dapatkan hadiahmu
        </Text>
        {reward.mission_target.map((item) => (
          <Touchable
            key={item.id_mission}
            onPress={isExpired ? noop : () => this._onPressMissionItem(item)}
            style={{ ...SHADOW_STYLE.shadowSoft }}
          >
            <MissionItemCard isExpired={isExpired} loading={false} item={item} withPoint={false} />
          </Touchable>
        ))}
      </View>
    )
  }

  _renderWinner = () => {
    const {
      reward: { winner },
      user,
    } = this.state
    return (
      <View style={STYLES.winnerContainer}>
        <View style={[STYLES.winnerHeaderWrapper, STYLES.winnerSection]}>
          <Image
            style={{ marginRight: WP2 }}
            size='xtiny'
            source={require('../../assets/icons/icGift.png')}
          />
          <Text type='Circular' weight={400} color={SHIP_GREY} size='mini'>
            Daftar Pemenang
          </Text>
        </View>
        <View>
          {winner.map((user, index) => (
            <FollowItem
              key={index}
              imageSize='xsmall'
              isMine={false}
              onPressItem={noop}
              user={user}
              style={{ marginHorizontal: 0, paddingHorizontal: WP5 }}
            />
          ))}
        </View>
        {user.claimed && (
          <View style={{ paddingHorizontal: WP5, paddingVertical: WP4 }}>
            <Text type='Circular' weight={300} color={SHIP_GREY_CALM} size='mini'>
              Selamat kamu telah mendapatkan hadiah ini
            </Text>
          </View>
        )}
      </View>
    )
  }

  _headerSection = () => {
    const { user, reward } = this.state
    const { isExpired } = this.props
    const daysLeft = daysRemaining(reward.endDate)

    // eslint-disable-next-line no-unused-vars
    const { navigateBack } = this.props
    const bgColor = this.state.scrollY.interpolate({
      inputRange: [0, WP5, WP15],
      outputRange: [NO_COLOR, NO_COLOR, WHITE],
      extrapolate: 'clamp',
    })
    const iconColor = this.state.scrollY.interpolate({
      inputRange: [0, WP5, WP20],
      outputRange: [WHITE, WHITE, SHIP_GREY_CALM],
      extrapolate: 'clamp',
    })
    const iconBgColor = this.state.scrollY.interpolate({
      inputRange: [0, WP5, WP20],
      outputRange: ['rgba(100,100,100, 0.16)', WHITE, WHITE],
      extrapolate: 'clamp',
    })
    const textColor = this.state.scrollY.interpolate({
      inputRange: [0, WP5, WP20],
      outputRange: [NO_COLOR, SHIP_GREY, SHIP_GREY],
      extrapolate: 'clamp',
    })

    return (
      <View style={{ width: WP100, position: 'absolute', zIndex: 999, top: 0 }}>
        <StatusBar
          animated
          translucent={true}
          backgroundColor={bgColor}
          barStyle={'light-content'}
        />
        <LinearGradient
          colors={['rgba(0,0,0,0.7)', 'rgba(0,0,0,0.2)', 'rgba(0,0,0,0.0)']}
          style={{
            position: 'absolute',
            height: WP35,
            width: WP100,
            top: 0,
            left: 0,
          }}
        />
        <Animated.View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: user.claimed ? WHITE : bgColor,
            paddingTop: getStatusBarHeight(true),
          }}
        >
          <Touchable
            onPress={() => navigateBack()}
            style={{ padding: WP2, paddingHorizontal: WP4 }}
          >
            <Icon
              centered
              background='dark-circle'
              backgroundColor={user.claimed ? WHITE : iconBgColor}
              size='huge'
              color={user.claimed ? SHIP_GREY_CALM : iconColor}
              name='chevron-left'
              type='Entypo'
            />
          </Touchable>

          <Animated.View
            style={{
              flex: 1,
              borderRadius: 6,
              alignItems: 'center',
              padding: WP2,
              marginHorizontal: WP2,
            }}
          >
            <Animated.Text
              style={{
                color: user.claimed ? SHIP_GREY : textColor,
                fontFamily: FONTS['Circular'][400],
                fontSize: FONT_SIZE['mini'],
                lineHeight: WP5,
              }}
              centered
            >
              Hadiah
            </Animated.Text>
          </Animated.View>
          <View style={{ padding: WP2, paddingHorizontal: WP4 + WP3 }} />
          {!user.claimed && (
            <View
              style={{
                backgroundColor: TOMATO,
                borderRadius: 5,
                paddingHorizontal: WP2,
                paddingVertical: WP05,
                position: 'absolute',
                right: WP5,
                top: getStatusBarHeight(true) + WP3,
              }}
            >
              <Text type='Circular' weight={400} size='mini' color={WHITE}>
                {isExpired
                  ? 'Telah Berakhir'
                  : daysLeft === 0
                    ? 'Hari Terakhir'
                    : `${daysLeft} Hari Lagi`}
              </Text>
            </View>
          )}
        </Animated.View>
        {user.claimed && (
          <View
            style={{
              backgroundColor: GREEN_30,
              paddingVertical: WP3,
              paddingHorizontal: WP5,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Text type='Circular' color={WHITE} size='slight'>
              Hadiah telah kamu dapatkan
            </Text>
            <Image source={require('../../assets/icons/check_green.png')} size={'tiny'} />
          </View>
        )}
      </View>
    )
  }

  render() {
    const { isLoading, navigateTo, isExpired } = this.props
    const { reward, user, isReady, claimRewardPopUp, insufficientPointPopUp } = this.state
    const isStockEmpty = reward.available_stock == 0
    const add_data = !isEmpty(reward) ? JSON.parse(reward.additional_data) : {}
    return (
      <Container
        isLoading={isLoading}
        noStatusBarPadding
        statusBarBackground={NO_COLOR}
        isReady={isReady}
      >
        <View
          style={{
            flexGrow: 1,
            backgroundColor: PALE_WHITE,
            paddingTop: user.claimed ? ExpoConstants.statusBarHeight : 0,
          }}
        >
          {this._headerSection()}

          <Animated.ScrollView
            ref={(ref) => (this.scrollViewRef = ref)}
            showsVerticalScrollIndicator={false}
            scrollEventThrottle={16}
            style={{ flex: 1, flexGrow: 1 }}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: { contentOffset: { y: this.state.scrollY } },
                },
              ],
              { useNativeDriver: false },
            )}
          >
            <View style={{ backgroundColor: PALE_WHITE }}>
              <Image source={{ uri: reward.image }} size={WP100} aspectRatio={1} />
              <View style={STYLES.section}>
                <Text type='Circular' weight={500} color={GUN_METAL}>
                  {reward.reward_name}
                </Text>
                <View style={{ flexDirection: 'row', marginTop: WP1, alignItems: 'center' }}>
                  <Point value={reward.total_point} iconSize='tiny' textSize='slight' />
                  {!isExpired && (
                    <Text type='Circular' color={SHIP_GREY} weight={600} size='slight'>
                      {'   .   '}
                    </Text>
                  )}
                  {!isExpired && (
                    <Text
                      type='Circular'
                      weight={400}
                      size='slight'
                      color={isStockEmpty ? REDDISH : SHIP_GREY}
                      style={{ marginTop: WP2 }}
                    >
                      {isStockEmpty
                        ? 'Stock telah habis'
                        : `Tersisa ${reward.available_stock} lagi`}
                    </Text>
                  )}
                </View>
              </View>

              {reward.mission_target && this._renderMissionTarget()}

              <View style={STYLES.section}>
                <Text
                  type='Circular'
                  weight={400}
                  color={GUN_METAL}
                  size='mini'
                  style={STYLES.headerSection}
                >
                  Informasi Hadiah
                </Text>
                <Text type='Circular' color={SHIP_GREY} size='mini' lineHeight={WP6}>
                  {add_data.description}
                </Text>
              </View>

              <View style={STYLES.section}>
                <Text
                  type='Circular'
                  weight={400}
                  color={GUN_METAL}
                  size='mini'
                  style={STYLES.headerSection}
                >
                  Spesifikasi Barang
                </Text>
                {add_data.specification &&
                add_data.specification.map((spec, index) => (
                  <View
                    key={index}
                    style={{ flexDirection: 'row', marginBottom: WP1, paddingRight: WP7 }}
                  >
                    <Icon
                      color={SHIP_GREY_CALM}
                      type='Entypo'
                      name='dot-single'
                      style={{ marginLeft: -WP1, marginRight: WP2, marginTop: WP05 }}
                    />
                    <Text type='Circular' color={SHIP_GREY} size='mini'>
                      {`${spec}`}
                    </Text>
                  </View>
                ))}
              </View>

              <View style={STYLES.section}>
                <Text
                  type='Circular'
                  weight={400}
                  color={GUN_METAL}
                  size='mini'
                  style={STYLES.headerSection}
                >
                  Penyedia Hadiah
                </Text>
                <Text type='Circular' color={SHIP_GREY} size='mini' lineHeight={WP6}>
                  {add_data.reward_by}
                </Text>
              </View>

              {!isEmpty(reward.winner) && (
                <View style={STYLES.section}>
                  <Text
                    type='Circular'
                    weight={400}
                    color={GUN_METAL}
                    size='mini'
                    style={STYLES.headerSection}
                  >
                    Pemenang Hadiah
                  </Text>
                  {this._renderWinner()}
                </View>
              )}
            </View>
          </Animated.ScrollView>

          <ConfirmationPopup
            isVisible={claimRewardPopUp}
            template={CONFIRMATION_MESSAGE.CLAIM_REWARD}
            onCancel={() => this.setState({ claimRewardPopUp: false })}
            onConfirm={() => {
              this.setState({ claimRewardPopUp: false })
              navigateTo('ClaimRewardScreen', { reward })
            }}
          />
          <ConfirmationPopup
            isVisible={insufficientPointPopUp}
            template={CONFIRMATION_MESSAGE.INSUFFICIENT_POINT}
            onCancel={() => this.setState({ insufficientPointPopUp: false })}
            onConfirm={() => {
              this.setState({ insufficientPointPopUp: false })
              navigateTo('MissionScreen', { is1000Startup: this.props.is1000Startup })
            }}
          />

          <ContentPopUp
            ref={(ref) => (this.videoCreatifContent = ref)}
            isVisible={false}
            content={VIDEO_CREATIVE_CONTENT}
            title={'Video Creative'}
          />

          {!user.claimed && !isExpired && (
            <View
              style={{
                paddingHorizontal: WP6,
                paddingVertical: WP305,
                backgroundColor: WHITE,
                ...SHADOW_STYLE.shadowBold,
              }}
            >
              <ButtonV2
                onPress={this._onClaim}
                style={{ paddingVertical: WP4 }}
                textSize={'slight'}
                text='Tukar Poinmu'
                textColor={WHITE}
                color={REDDISH}
                disabled={isStockEmpty}
              />
            </View>
          )}
        </View>
      </Container>
    )
  }
}

RewardDetailScreen.propTypes = {}

RewardDetailScreen.defaultProps = {}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  setData,
}

const mapFromNavigationParam = (getParam) => ({
  id_reward: getParam('id_reward', null),
  isExpired: getParam('isExpired', false),
  is1000Startup: getParam('is1000Startup', false),
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(RewardDetailScreen),
  mapFromNavigationParam,
)
