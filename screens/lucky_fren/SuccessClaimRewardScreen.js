import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BackHandler, TouchableOpacity, View } from 'react-native'
import ExpoConstants from 'expo-constants'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import Container from '../../components/Container'
import Image from '../../components/Image'
import Text from '../../components/Text'
import { WP10, WP3, WP6, WP60 } from '../../constants/Sizes'
import { NAVY_DARK, REDDISH, SHIP_GREY, WHITE } from '../../constants/Colors'
import ButtonV2 from '../../components/ButtonV2'
import { showReviewIfNotYet } from '../../utils/review'

const STYLE = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: WP10,
    paddingBottom: ExpoConstants.statusBarHeight
  }
}

class SuccessClaimRewardScreen extends Component {
  componentDidMount = () => {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this._onBackToLuckyBox)
    setTimeout(() => showReviewIfNotYet(), 1000)
  }

  componentWillUnmount() {
    typeof this.backHandler === 'object' && typeof this.backHandler.remove === 'function' && this.backHandler.remove()
  }

  _onBackToLuckyBox = () => {
    this.props.popToTop()
    this.props.navigateTo('LuckyFrenScreen')
    this.props.navigateTo('LuckyBoxScreen')
  }

  _onBackToHome = () => {
    this.props.popToTop()
    this.props.navigateTo('LuckyFrenScreen')
  }

  render() {
    return (
      <Container
        bounces={false}
      >
        <View style={STYLE.container}>
          <Image
            source={require('../../assets/icons/v3/congratulationCheck.png')}
            size={WP60}
            style={{ marginBottom: WP6 }}
          />
          <Text type='Circular' weight={600} size='large' color={NAVY_DARK} centered style={{ marginBottom: WP6 }}>
            Selamat menikmati hadiahmu!
          </Text>

          <Text type='Circular' weight={300} size='slight' color={SHIP_GREY} centered>
            Hadiah akan kami kirim paling lambat
            <Text type='Circular' weight={400} size='slight' color={SHIP_GREY}>{' 3 x 24 Jam '}</Text>
            setelah proses verifikasi oleh tim kami
          </Text>

          <ButtonV2
            onPress={this._onBackToLuckyBox}
            style={{ paddingVertical: WP3, paddingHorizontal: WP6, marginVertical: WP6 }}
            textSize={'slight'}
            text='Kembali ke Kotak Hadiah'
            textColor={WHITE}
            color={REDDISH}
          />
          <TouchableOpacity onPress={this._onBackToHome}>
            <Text type='Circular' weight={300} size='slight' color={SHIP_GREY} centered>
              Kembali ke halaman utama
            </Text>
          </TouchableOpacity>
        </View>
      </Container>
    )
  }
}

SuccessClaimRewardScreen.propTypes = {}

SuccessClaimRewardScreen.defaultProps = {}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {

}

const mapFromNavigationParam = (getParam) => ({
  reward: getParam('reward', {})
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SuccessClaimRewardScreen),
  mapFromNavigationParam
)
