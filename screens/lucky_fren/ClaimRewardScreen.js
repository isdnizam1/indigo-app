import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import * as yup from 'yup'
import { CheckBox } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { setData } from '../../services/screens/lv1/lucky-fren/actionDispatcher'
import Text from '../../components/Text'
import Container from '../../components/Container'
import HeaderNormal from '../../components/HeaderNormal'
import Image from '../../components/Image'
import {
  GUN_METAL,
  NAVY_DARK,
  PALE_BLUE_TWO,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SILVER_TWO,
  WHITE,
} from '../../constants/Colors'
import { WP17, WP2, WP3, WP4, WP5, WP6, WP7, WP9 } from '../../constants/Sizes'
import { HEADER, SHADOW_STYLE, TEXT_INPUT_STYLE } from '../../constants/Styles'
import ButtonV2 from '../../components/ButtonV2'
import Form from '../../components/Form'
import InputTextLight from '../../components/InputTextLight'
import Point from '../../components/lucky_fren/Point'
import { FONTS } from '../../constants/Fonts'
import { postClaimReward } from '../../actions/api'
import { isEmailValid, isValidPhoneNumber } from '../../utils/helper'
import ContentPopUp from '../../components/popUp/ContentPopUp'
import { TnC_CONTENT } from './Constants'

const FORM_VALIDATION = yup.object().shape({
  full_name: yup.string().required(),
  email: yup.string().required(),
  phone: yup.string().required(),
  address: yup.string().required(),
})

const STYLES = {
  rewardContainer: {
    paddingTop: WP9,
    paddingBottom: WP7,
    paddingHorizontal: WP5,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE_TWO,
  },
  rewardWrapper: {
    ...SHADOW_STYLE.shadow,
    shadowOpacity: 0.06,
    padding: WP3,
    flexDirection: 'row',
    backgroundColor: WHITE,
    borderRadius: 10,
    alignItems: 'center',
    marginTop: WP4,
  },
}

class ClaimRewardScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      rewards: [],
      templatePopUp: {},
      isUploading: false,
      validPhone: true,
      validEmail: true,
    }
    this.tncContent = React.createRef()
  }
  componentDidMount = () => {};

  _onWhatsappChange = (onChange) => (whatsapp) => {
    let validPhone = false
    if (isValidPhoneNumber(whatsapp)) {
      validPhone = true
    }
    this.setState({ validPhone })
    onChange(whatsapp)
  };

  _onEmailChange = (onChange) => (email) => {
    let validEmail = false
    if (isEmailValid(email)) {
      validEmail = true
    }
    this.setState({ validEmail })
    onChange(email)
  };

  _onSubmit = async ({ formData }) => {
    const { dispatch, navigateTo, userData, reward } = this.props
    const { full_name, email, address, phone } = formData
    try {
      const body = {
        id_user: userData.id_user,
        id_reward: reward.id_reward,
        full_name,
        email,
        address,
        phone,
      }
      await dispatch(postClaimReward, body)
      navigateTo('SuccessClaimRewardScreen')
    } catch (e) {}
  };

  _renderReward = () => {
    const { reward } = this.props
    return (
      <View style={STYLES.rewardContainer}>
        <Text type='Circular' color={NAVY_DARK} size='medium' weight={600}>
          Selangkah lagi untuk memiliki:
        </Text>
        <View style={STYLES.rewardWrapper}>
          <Image source={{ uri: reward.image }} size={WP17} />
          <View style={{ flex: 1, marginLeft: WP4 }}>
            <Text
              color={GUN_METAL}
              weight={500}
              numberOfLines={1}
              ellipsizeMode='tail'
            >
              {reward.reward_name}
            </Text>
            <Point value={reward.total_point} />
          </View>
        </View>
      </View>
    )
  };

  render() {
    const { isLoading, navigateBack } = this.props
    const { isUploading, validEmail, validPhone } = this.state
    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={() => navigateBack()}
            text='Kotak Hadiah'
            centered
            shadow
          />
        )}
        scrollable
        bounces={false}
      >
        <KeyboardAwareScrollView
          keyboardDismissMode='interactive'
          keyboardShouldPersistTaps='always'
          extraScrollHeight={HEADER.height}
        >
          <Form validation={FORM_VALIDATION} onSubmit={this._onSubmit}>
            {({ onChange, onSubmit, formData, isValid }) => (
              <View>
                {this._renderReward()}
                <View style={{ paddingHorizontal: WP5 }}>
                  <View style={{ marginVertical: WP6 }}>
                    <Text
                      type='Circular'
                      color={NAVY_DARK}
                      size='medium'
                      weight={600}
                    >
                      Data Pribadi
                    </Text>
                    <Text type='Circular' color={SHIP_GREY} size='mini' weight={300}>
                      Isi data dibawah ini untuk proses pengiriman
                    </Text>
                  </View>

                  <InputTextLight
                    withLabel={false}
                    placeholder='Pastikan nama pemenang dan penerima sama'
                    value={formData.full_name}
                    onChangeText={onChange('full_name')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Nama Lengkap'
                    editable={!isUploading}
                  />
                  <InputTextLight
                    withLabel={false}
                    placeholder='Masukan email aktifmu'
                    value={formData.email}
                    onChangeText={this._onEmailChange(onChange('email'))}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={{
                      ...TEXT_INPUT_STYLE['inputV2'],
                      borderColor: validEmail ? PALE_BLUE_TWO : REDDISH,
                    }}
                    labelv2='Email'
                    editable={!isUploading}
                  />
                  <InputTextLight
                    withLabel={false}
                    placeholder='Tenang saja, alamatmu tidak akan tersebar'
                    value={formData.address}
                    onChangeText={onChange('address')}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                    labelv2='Alamat'
                    multiline
                    maxLine={4}
                    lineHeight={3}
                    editable={!isUploading}
                  />
                  <InputTextLight
                    withLabel={false}
                    placeholder='Pastikan tersambung dengan Whatsapp'
                    keyboardType={'numeric'}
                    value={formData.phone}
                    onChangeText={this._onWhatsappChange(onChange('phone'))}
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    wording=' '
                    maxLengthFocus
                    lineHeight={1}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={{
                      ...TEXT_INPUT_STYLE['inputV2'],
                      borderColor: validPhone ? PALE_BLUE_TWO : REDDISH,
                    }}
                    labelv2='No. Telephone'
                    subLabel='(Whatsapp)'
                    editable={!isUploading}
                  />

                  <CheckBox
                    disabled={isUploading}
                    checked={formData.acceptTnC}
                    containerStyle={{
                      backgroundColor: WHITE,
                      borderWidth: 0,
                      margin: 0,
                      paddingHorizontal: -WP2,
                      paddingRight: WP2,
                      marginBottom: WP2,
                      flex: 1,
                      marginTop: WP3,
                    }}
                    onPress={() => {
                      onChange('acceptTnC')(!formData.acceptTnC)
                    }}
                    title={
                      <Text
                        type='Circular'
                        color={formData.acceptTnC ? REDDISH : SHIP_GREY_CALM}
                        size='mini'
                        style={{ marginLeft: WP3 }}
                      >
                        {'Saya menyetujui '}
                        <Text
                          type='Circular'
                          color={formData.acceptTnC ? REDDISH : SHIP_GREY_CALM}
                          weight={600}
                          size='mini'
                          onPress={() => this.tncContent.openModal()}
                          style={{ textDecorationLine: 'underline' }}
                        >
                          {'Syarat & Ketentuan'}
                        </Text>
                        {' yang berlaku'}
                      </Text>
                    }
                    fontFamily={FONTS.Circular['200']}
                    wrapperStyle={{ marginHorizontal: -WP2, marginTop: -WP3 }}
                    checkedIcon={
                      <Image
                        imageStyle={{ width: WP6, aspectRatio: 1 }}
                        source={require('sf-assets/icons/icCheckboxActive.png')}
                      />
                    }
                    uncheckedIcon={
                      <Image
                        imageStyle={{ width: WP6, aspectRatio: 1 }}
                        source={require('sf-assets/icons/icCheckboxInactive.png')}
                      />
                    }
                  />

                  <ButtonV2
                    onPress={onSubmit}
                    style={{
                      paddingVertical: WP4,
                      marginVertical: WP4,
                      marginBottom: WP6,
                    }}
                    textSize={'slight'}
                    text='Kirim'
                    textColor={WHITE}
                    color={REDDISH}
                    disabled={
                      !isValid || !formData.acceptTnC || !validEmail || !validPhone
                    }
                  />
                </View>
              </View>
            )}
          </Form>
          <ContentPopUp
            ref={(ref) => (this.tncContent = ref)}
            content={TnC_CONTENT}
            contentStyle={{ paddingBottom: WP7 }}
            title={'Syarat & Ketentuan: Hadiah & Peserta'}
            list
          />
        </KeyboardAwareScrollView>
      </Container>
    )
  }
}

ClaimRewardScreen.propTypes = {}

ClaimRewardScreen.defaultProps = {}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  setData,
}

const mapFromNavigationParam = (getParam) => ({
  reward: getParam('reward', {}),
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ClaimRewardScreen),
  mapFromNavigationParam,
)
