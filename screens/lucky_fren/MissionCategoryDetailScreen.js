import React, { Component } from 'react'
import {
  Animated,
  RefreshControl,
  StatusBar as StatusBarRN,
  View,
} from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import * as Animatable from 'react-native-animatable'
import Touchable from 'sf-components/Touchable'
import { noop } from 'lodash-es'
import moment from 'moment'
import { setData } from '../../services/screens/lv1/lucky-fren/actionDispatcher'
import { _enhancedNavigation, Container, Icon, Image, Text } from '../../components'
import {
  NAVY_DARK,
  NO_COLOR,
  PALE_BLUE,
  PALE_GREY_THREE,
  PALE_WHITE,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from '../../constants/Colors'
import {
  FONT_SIZE,
  HP1,
  HP10,
  HP2,
  WP1,
  WP100,
  WP15,
  WP2,
  WP20,
  WP4,
  WP5,
  WP6,
} from '../../constants/Sizes'
import { getStatusBarHeight } from '../../utils/iphoneXHelper'
import { FONTS } from '../../constants/Fonts'
import InteractionManager from '../../utils/InteractionManager'
import { getLuckyFrenMission } from '../../actions/api'
import { SHADOW_STYLE } from '../../constants/Styles'
import HorizontalLine from '../../components/HorizontalLine'
import MissionItemCard from '../../components/lucky_fren/MissionItemCard'
import { getNotifMappingAction } from '../../components/notification/NotifAction'

const StatusBar = Animatable.createAnimatableComponent(StatusBarRN)

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  setData,
}

const mapFromNavigationParam = (getParam) => ({
  category: getParam('category', 'daily'),
  idMission: getParam('idMission', 0),
  isExpired: getParam('isExpired', false),
})

class MissionCategoryDetailScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
      scrollY: new Animated.Value(0),
      isLoading: true,
      list: [],
      detail: {},

      days: '00',
      hours: '00',
      minutes: '00',
      seconds: '00',
    }
  }

  componentDidMount = () => {
    InteractionManager.runAfterInteractions(() => {
      this._getContent()
    })
  };

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval)
    }
  }

  _getContent = async () => {
    const { dispatch, idMission, userData } = this.props
    const { result } = await dispatch(
      getLuckyFrenMission,
      { id_mission_category: idMission, id_user: userData.id_user },
      noop,
      true,
      true,
    )
    if (result) {
      this.setState({ ...result, isLoading: false })

      if (result.detail) {
        const timeTillDate = result.detail.endDate
        const then = moment(timeTillDate).unix()
        const now = moment().unix()
        const diffTime = then - now
        let duration = moment.duration(diffTime * 1000, 'milliseconds')
        this._setDuration(duration)

        this.interval = setInterval(() => {
          duration = moment.duration(duration - 1000, 'milliseconds')
          this._setDuration(duration)
        }, 1000)
      }
    }
  };

  _setDuration = (duration) => {
    const days = duration.days()
    const hours = duration.hours()
    const minutes = duration.minutes()
    const seconds = duration.seconds()
    this.setState({ days, hours, minutes, seconds })
  };
  _headerSection = () => {
    const { navigateBack } = this.props
    const { detail } = this.state
    const bgColor = this.state.scrollY.interpolate({
      inputRange: [-10, 0, WP5, WP15],
      outputRange: [WHITE, NO_COLOR, NO_COLOR, WHITE],
      extrapolate: 'clamp',
    })
    const iconColor = this.state.scrollY.interpolate({
      inputRange: [-10, 0, WP5, WP20],
      outputRange: [SHIP_GREY_CALM, WHITE, WHITE, SHIP_GREY_CALM],
      extrapolate: 'clamp',
    })
    return (
      <View style={{ width: WP100, position: 'absolute', zIndex: 999, top: 0 }}>
        <StatusBar
          animated
          translucent={true}
          backgroundColor={bgColor}
          barStyle={'dark-content'}
        />
        <Animated.View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: bgColor,
            paddingTop: getStatusBarHeight(true),
          }}
        >
          <Touchable
            onPress={() => navigateBack()}
            style={{ padding: WP2, paddingHorizontal: WP4 }}
          >
            <Icon
              centered
              background='dark-circle'
              backgroundColor='rgba(255,255,255, 0.16)'
              size='huge'
              color={iconColor}
              name='chevron-left'
              type='Entypo'
            />
          </Touchable>

          <Animated.View
            style={{
              flex: 1,
              borderRadius: 6,
              alignItems: 'center',
              padding: WP2,
              marginHorizontal: WP2,
            }}
          >
            <Animated.Text
              style={{
                color: iconColor,
                fontFamily: FONTS['Circular'][400],
                fontSize: FONT_SIZE['mini'],
                lineHeight: WP5,
              }}
              centered
            >
              {detail.name}
            </Animated.Text>
          </Animated.View>
          <View style={{ width: WP15 }} />
        </Animated.View>
      </View>
    )
  };
  _cardMission = (detail, list, remaining) => {
    const totalPoint = list.reduce(function (a, b) {
      return Number(a) + Number(b.total_point)
    }, 0)
    return (
      <View>
        <Image
          tint={'black'}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 156}
          source={{ uri: detail.banner }}
        />
        <View
          style={{
            width: '100%',
            paddingHorizontal: WP4,
            marginTop: -((WP100 * 156) / 360 / 3.25),
            marginBottom: WP6,
          }}
        >
          <View
            style={{
              borderRadius: 6,
              backgroundColor: WHITE,
              ...SHADOW_STYLE['shadowThin'],
            }}
          >
            <View style={{ width: '100%', overflow: 'hidden', padding: WP4 }}>
              <Text
                lineHeight={WP6}
                type='Circular'
                size='small'
                color={NAVY_DARK}
                weight={600}
              >
                {detail.name}
              </Text>
              <Text
                lineHeight={WP6}
                type='Circular'
                size='mini'
                color={SHIP_GREY}
                style={{ marginTop: HP1 }}
              >
                {detail.description}
              </Text>
            </View>
            <HorizontalLine width='100%' color={PALE_GREY_THREE} />
            <View style={{ width: '100%', overflow: 'hidden', padding: WP4 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                  size='xtiny'
                  style={{ marginRight: WP1 }}
                  source={require('../../assets/icons/icPoint.png')}
                />
                <Text
                  numberOfLines={1}
                  lineHeight={WP5}
                  type='Circular'
                  size='mini'
                  color={SHIP_GREY}
                >
                  Total Point:
                  <Text
                    lineHeight={WP6}
                    type='Circular'
                    size='mini'
                    color={SHIP_GREY}
                    weight={500}
                  >{` ${totalPoint} poin `}</Text>
                </Text>
              </View>
            </View>
            <HorizontalLine width='100%' color={PALE_GREY_THREE} />
            <View style={{ width: '100%', overflow: 'hidden', padding: WP4 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon
                  centered
                  backgroundColor='rgba(255,255,255, 0.16)'
                  size='small'
                  color={REDDISH}
                  name='clock-outline'
                  type='MaterialCommunityIcons'
                  style={{ marginRight: WP1 }}
                />
                {!this.props.isExpired && (
                  <Text
                    numberOfLines={1}
                    lineHeight={WP5}
                    type='Circular'
                    size='mini'
                    color={SHIP_GREY}
                  >
                    Berakhir dalam
                    <Text
                      lineHeight={WP6}
                      type='Circular'
                      size='mini'
                      color={SHIP_GREY}
                      weight={500}
                    >{`${remaining}  `}</Text>
                  </Text>
                )}
                {this.props.isExpired && (
                  <Text
                    numberOfLines={1}
                    lineHeight={WP5}
                    type='Circular'
                    size='mini'
                    color={SHIP_GREY}
                  >
                    Program telah berakhir
                  </Text>
                )}
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  };

  _missionList = (list, isLoading) => {
    return (
      <View style={{ marginHorizontal: WP4, marginTop: HP2 }}>
        <Text
          lineHeight={WP6}
          type='Circular'
          size='small'
          color={NAVY_DARK}
          weight={600}
        >
          Misi yang tersedia
        </Text>
        <View style={{ marginTop: HP1, marginBottom: HP10 }}>
          {list.map((item) => (
            <Touchable
              key={item.id_mission}
              onPress={
                this.props.isExpired ? noop : () => this._onPressMissionItem(item)
              }
            >
              <MissionItemCard
                isExpired={this.props.isExpired}
                loading={isLoading}
                item={item}
              />
            </Touchable>
          ))}
        </View>
      </View>
    )
  };

  _onPressMissionItem = async (item) => {
    const { navigateTo } = this.props

    const notifAction = await getNotifMappingAction({
      url_mobile: 'internal_link',
      url: item.link,
    })
    if (notifAction) {
      navigateTo(notifAction.to, notifAction.payload)
    }
    // if (item.id_mission === '109') navigateTo('StartupSubmissionForm', { idAds: 7 })
  };

  _getRemainingText = (days, hours, minutes, seconds) => {
    let text = ''
    let count = 0
    if (days > 0 && count < 2) {
      text += ` ${days} Hari`
      count++
    }
    if (hours > 0 && count < 2) {
      text += ` ${hours} Jam`
      count++
    }
    if (minutes > 0 && count < 2) {
      text += ` ${minutes} Menit`
      count++
    }
    if (seconds > 0 && count < 2) {
      text += ` ${seconds} Detik`
      count++
    }

    return text
  };

  render() {
    const {
      isLoading,
      list,
      detail,
      isRefreshing,
      days,
      hours,
      minutes,
      seconds,
    } = this.state

    const remaining = this._getRemainingText(days, hours, minutes, seconds)

    return (
      <Container
        isLoading={isLoading}
        noStatusBarPadding
        statusBarBackground={NO_COLOR}
      >
        <View style={{ flex: 1, backgroundColor: PALE_WHITE }}>
          <NavigationEvents onDidFocus={this._getContent} />
          {this._headerSection()}

          <Animated.ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={isRefreshing}
                onRefresh={() => {
                  this.setState({ isRefreshing: true }, async () => {
                    await this._getContent()
                    this.setState({
                      isRefreshing: false,
                    })
                  })
                }}
              />
            }
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: { contentOffset: { y: this.state.scrollY } },
                },
              ],
              { useNativeDriver: false },
            )}
          >
            {this._cardMission(detail, list, remaining)}
            <HorizontalLine
              width='100%'
              color={PALE_BLUE}
              style={{ marginVertical: HP2 }}
            />
            {this._missionList(list, isLoading)}
          </Animated.ScrollView>
        </View>
      </Container>
    )
  }

  _onChangeTab = (index) => {
    this.setState({ index })
  };
}

MissionCategoryDetailScreen.propTypes = {}

MissionCategoryDetailScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(MissionCategoryDetailScreen),
  mapFromNavigationParam,
)
