import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { SHIP_GREY, SHIP_GREY_CALM } from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import InteractionManager from 'sf-utils/InteractionManager'
import { Container, _enhancedNavigation } from '../../components'
import HeaderNormal from '../../components/HeaderNormal'
import { SHADOW_GRADIENT } from '../../constants/Colors'
import { WP100, WP12, WP3 } from '../../constants/Sizes'

const style = StyleSheet.create({
  headerShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
})

class NameOfScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this._backHandler = this._backHandler.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
    this._getData = this._getData.bind(this)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._getData)
  }

  _getData = () => {};

  _backHandler = () => {
    this.props.navigateBack()
  };

  _renderHeader = () => {
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftOnPress={this._backHandler}
          centered
          textType={'Circular'}
          iconLeft={'ios-arrow-back'}
          iconLeftSize={'large'}
          textSize={'slight'}
          iconLeftType={'Ionicons'}
          text={'Premium Membership'}
          textColor={SHIP_GREY}
          iconLeftColor={SHIP_GREY_CALM}
          rightComponent={<View style={{ width: WP12 }} />}
        />
        <LinearGradient
          colors={SHADOW_GRADIENT}
          style={style.headerShadow}
        />
      </View>
    )
  };

  render() {
    return <Container renderHeader={this._renderHeader} />
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  package: getParam('package', null),
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(NameOfScreen),
  mapFromNavigationParam,
)
