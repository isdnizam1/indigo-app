import React from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import { isEmpty } from 'lodash-es'
import SkeletonContent from 'react-native-skeleton-content'
import { Container, Text, Avatar, Icon } from '../../components'
import {
  WHITE,
  PALE_BLUE,
  NAVY_DARK,
  REDDISH,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  GUN_METAL,
  SHIP_GREY_CALM,
  PALE_GREY,
  SHIP_GREY,
  WHITE_MILK,
} from '../../constants/Colors'
import {
  WP4,
  WP6,
  WP40,
  WP2,
  WP105,
  WP35,
  WP3,
  WP12,
  WP50,
  WP1,
  WP05,
  WP20,
  WP10,
  WP80,
  WP405,
  HP50,
} from "../../constants/Sizes";
import { TOUCH_OPACITY, SHADOW_STYLE } from '../../constants/Styles'
import Layout from '../../components/home/HomeSkeletonLayout'
import SoundplayItemV2 from '../../components/home/items/SoundplayItemV2'
import { speakerDisplayName } from '../../utils/transformation'
import { restrictedAction } from '../../utils/helper'
import EmptyState from '../../components/EmptyState'

const podcastConfig = {
  popular: {
    title: "Album Terpopuler",
    route: "",
    key_data: "popular_album",
    line: true,
    horizontal: true,
    numColumns: undefined,
    empty_message_title: "Belum ada album podcast tersedia saat ini",
    empty_message_desc: "Tunggu podcast menarik hanya untuk kamu",
  },
  category: {
    title: "Kategori Pilihan",
    route: "PodcastListScreen",
    key_data: "category",
    line: true,
    horizontal: true,
    numColumns: undefined,
    empty_message_title: "Belum ada kategori podcast saat ini",
    empty_message_desc: "Tunggu podcast menarik hanya untuk kamu",
  },
  recomendation: {
    title: "Rekomendasi Episode",
    route: "",
    key_data: "recommend_episode",
    line: false,
    horizontal: false,
    numColumns: undefined,
  },
};

const EpisodeItem = ({ image, size, imageSize, title, subtitle, loading }) => {
  return (
    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start' }}>
      <View style={{ marginRight: WP4 }}>
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[{ width: size, height: size, borderRadius: 6 }]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          {!loading && (
            <Avatar
              style={{ borderRadius: 6 }}
              imageStyle={{ borderRadius: 6 }}
              size={imageSize}
              image={image}
            />
          )}
        </SkeletonContent>
      </View>
      <SkeletonContent
        containerStyle={{ flex: 1 }}
        layout={[
          { width: WP50, height: WP4, marginBottom: WP105 },
          { width: WP35, height: WP3 },
        ]}
        isLoading={loading}
        boneColor={SKELETON_COLOR}
        highlightColor={SKELETON_HIGHLIGHT}
      >
        <View>
          <Text
            numberOfLines={1}
            type='Circular'
            color={GUN_METAL}
            size='mini'
            weight={500}
            style={{ marginBottom: WP05 }}
          >
            {title}
          </Text>
          <Text
            numberOfLines={1}
            type='Circular'
            color={SHIP_GREY_CALM}
            size='xmini'
            weight={300}
          >
            {subtitle}
          </Text>
        </View>
      </SkeletonContent>
    </View>
  )
}

const RecomdationPodcastItem = ({ ads, loading, line }) => {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginRight: WP4,
        paddingTop: line ? WP4 : 0,
        borderTopColor: PALE_BLUE,
        borderTopWidth: line ? 1 : 0,
      }}
    >
      <EpisodeItem
        image={ads.image}
        imageSize={'xsmall'}
        size={WP10}
        title={ads.title}
        subtitle={ads.speaker_name}
        loading={loading}
      />
      <View style={{ marginLeft: WP4, width: WP20 }}>
        {ads.is_premium == 1 && (
          <View
            style={{
              width: WP20,
              borderRadius: 6,
              paddingVertical: WP1,
              paddingHorizontal: WP105,
              backgroundColor: PALE_GREY,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text type='Circular' color={SHIP_GREY_CALM} size='xmini' weight={400}>
              Premium
            </Text>
          </View>
        )}
      </View>
    </View>
  )
}

const CategoryPodcastItem = ({ ads, loading, navigateTo, isStartup }) => {
  const dataEpisode = loading
    ? [1, 2, 3]
    : !isEmpty(ads.data) && ads.data.slice(0, 3)
  return (
    <View
      style={{
        width: WP80,
        backgroundColor: WHITE,
        marginRight: WP4,
        borderRadius: 6,
        ...SHADOW_STYLE['shadowThin'],
      }}
    >
      <View
        style={{
          paddingHorizontal: WP4,
          paddingVertical: WP405,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          borderBottomColor: PALE_GREY,
          borderBottomWidth: 1,
        }}
      >
        <SkeletonContent
          containerStyle={{ flex: 1, justifyContent: 'center' }}
          layout={[{ width: WP50, height: WP6 }]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <Text
            numberOfLines={1}
            type='Circular'
            color={SHIP_GREY}
            size='mini'
            weight={500}
          >
            {ads.title}
          </Text>
        </SkeletonContent>
        <View
          style={{
            width: WP6,
            height: WP6,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Icon
            size='small'
            name='chevron-right'
            type='Entypo'
            color={loading ? WHITE_MILK : REDDISH}
            centered
          />
        </View>
      </View>
      <View style={{ paddingVertical: WP2 }}>
        {dataEpisode.map((item, index) => {
           var speaker = 'Speaker Name';
          if(item.additional_data){
            var additional_data=JSON.parse(item.additional_data);
            if(additional_data.episodeList[0]?.speaker_name){
              var speaker = additional_data.episodeList[0]?.speaker_name;
            }
          }
          return (
            <TouchableOpacity
              onPress={() => navigateTo('SoundplayDetail', { id_ads: item.id_ads, isStartup })}
              key={index}
              style={{
                flex: 1,
                paddingHorizontal: WP4,
                paddingVertical: WP2,
                borderTopWidth: index > 0 ? 1 : 0,
                borderTopColor: PALE_GREY,
              }}
            >
              <EpisodeItem
                image={item.image}
                imageSize={'small'}
                size={WP12}
                title={item.title}
                subtitle={speaker}
                loading={loading}
              />
            </TouchableOpacity>
          )
        })}
      </View>
    </View>
  )
}

const PodcastTab = ({ data, getData, navigateTo, loading, userData, isStartup = false }) => {
  const _onPressItem = (ads, section) => () => {
    if (section === 'popular') {
      navigateTo('SoundplayDetail', { id_ads: ads.id_ads, isStartup })
    } else if (section === 'recomendation') {
      navigateTo('SoundplayDetail', {
        id_ads: ads.id_ads,
        id_episode: ads.id_episode,
        isStartup
      })
    } else if (section === 'category') {
      navigateTo('PodcastListScreen', { category: ads.title, isStartup })
    }
  }

  const _adItem = (ads, i, section) => {
    if (loading && section === 'popular') {
      const skeleton = Layout.item(section)
      return (
        <View
          key={`${Math.random()}`}
          style={{
            borderRadius: 6,
            marginRight: WP4,
            width: WP40,
            paddingVertical: WP2,
            ...skeleton.wrapperStyle,
          }}
        >
          <SkeletonContent
            containerStyle={{
              ...Layout.container,
              alignItems: 'flex-start',
            }}
            layout={skeleton.layout}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          />
        </View>
      )
    }

    return (
      <TouchableOpacity
        disabled={loading}
        style={{ paddingVertical: WP2 }}
        key={`${i}${Math.random()}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={restrictedAction({
          action: _onPressItem(ads, section),
          userData,
          navigateTo,
        })}
      >
        {section === 'popular' && <SoundplayItemV2 ads={ads} loading={loading} />}
        {section === 'category' && (
          <CategoryPodcastItem ads={ads} loading={loading} navigateTo={navigateTo} isStartup={isStartup} />
        )}
        {/* {section === 'recomendation' && (
          <RecomdationPodcastItem ads={ads} loading={loading} line={i !== 0} />
        )} */}
      </TouchableOpacity>
    )
  }

  const _sectionPodcast = (section) => {
    const config = podcastConfig[section]
    const dummyData = [1, 2, 3]
    const dataList = loading
      ? dummyData
      : !isEmpty(data)
        ? data[config.key_data]
        : []
    return (
      (!isEmpty(data) || loading) && (
        <View
          style={{
            borderBottomColor: PALE_BLUE,
            borderBottomWidth: config.line ? 1 : 0,
          }}
        >
          <View style={{ paddingVertical: WP6 }}>
            {/* HEADER */}
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                paddingHorizontal: WP4,
              }}
            >
              <Text
                type="Circular"
                size="medium"
                color={NAVY_DARK}
                weight={600}
              >
                {config.title}
              </Text>
              {!isEmpty(config.route) && !isEmpty(dataList) && (
                <TouchableOpacity
                  activeOpacity={TOUCH_OPACITY}
                  onPress={() => {
                    navigateTo(config.route, { isStartup });
                  }}
                >
                  <Text
                    type="Circular"
                    size="xmini"
                    color={REDDISH}
                    weight={400}
                  >
                    Lihat Semua
                  </Text>
                </TouchableOpacity>
              )}
            </View>

            {/* ITEM */}
            {!isEmpty(dataList) ? (
              <FlatList
                bounces={false}
                bouncesZoom={false}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                horizontal={config.horizontal}
                numColumns={config.numColumns}
                style={{ flexGrow: 0 }}
                contentContainerStyle={{
                  paddingLeft: WP4,
                  paddingTop: WP4,
                }}
                data={dataList}
                extraData={dataList}
                renderItem={({ item, index }) =>
                  _adItem(item, index, section, loading)
                }
              />
            ) : (
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  height: HP50,
                }}
              >
                <EmptyState
                  image={
                    config.title == "Album Terpopuler"
                      ? require(`sf-assets/icons/ic_media_album_emptystate.png`)
                      : require(`sf-assets/icons/ic_media_category_emptystate.png`)
                  }
                  title={config.empty_message_title}
                  message={config.empty_message_desc}
                />
              </View>
            )}
          </View>
        </View>
      )
    );
  }

  return (
    <Container
      isReady={true}
      isLoading={false}
      theme='light'
      noStatusBarPadding
      scrollable
      scrollBackgroundColor={WHITE}
      onPullDownToRefresh={() => getData()}
    >
      <View>
        {_sectionPodcast('popular')}
        {_sectionPodcast('category')}
        {/* {_sectionPodcast('recomendation')} */}
      </View>
    </Container>
  )
}

export default PodcastTab
