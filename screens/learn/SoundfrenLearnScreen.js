import React, { Component, Fragment } from 'react'
import { Animated as RNAnimated, StatusBar, View } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import { TabBar, TabView } from 'react-native-tab-view'
import { noop } from 'lodash-es'
import SkeletonContent from 'react-native-skeleton-content'
import Animated from 'react-native-reanimated'
import { HeaderNormal } from 'sf-components'
import { SHADOW_GRADIENT, SHIP_GREY } from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import MessageBarClearBlue from 'sf-components/messagebar/MessageBarClearBlue'
import Touchable from 'sf-components/Touchable'
import { WP305 } from 'sf-constants/Sizes'
import {
  BLACK20,
  BLACK90,
  NO_COLOR,
  REDDISH,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  TRANSPARENT,
  WHITE,
} from '../../constants/Colors'
import { Container, Icon, Image, Text } from '../../components'
import {
  WP1,
  WP10,
  WP100,
  WP105,
  WP3,
  WP4,
  WP50,
  WP6,
  WP70,
  WP8,
} from '../../constants/Sizes'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { getAdsSFLearn, postLogMenuVideo } from '../../actions/api'
import { BORDER_STYLE, SHADOW_STYLE } from '../../constants/Styles'
import { getBottomSpace, getStatusBarHeight } from '../../utils/iphoneXHelper'
import Modal from '../../components/Modal'
import SoundfrenExploreOptions from '../../components/explore/SoundfrenExploreOptions'
import style from '../profile/v2/ProfileScreen/style'
import { restrictedAction } from '../../utils/helper'
import PodcastTab from './PodcastTab'
import VideoTab from './VideoTab'
import LearnOptionMenus from './LearnOptionMenus'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  category: getParam('category', ''),
  tabToShow: getParam('tabToShow', 0),
  isStartup: getParam('isStartup', false),
})

class SoundfrenLearnScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      result: {},
      index: this.props.tabToShow || 0,
      routes: [
        { key: 'podcast', title: 'PODCAST' },
        { key: 'video', title: 'VIDEOS' },
      ],
      headerOpacity: new RNAnimated.Value(0),
      statusBarTheme: 'light',
    }
  }

  async componentDidMount() {
    await this._getAdsSFLearn()
  }

  _getAdsSFLearn = async () => {
    const { dispatch, userData, isStartup } = this.props
    let param = isStartup ? { id_user: userData.id_user, platform: '1000startup' } : { id_user: userData.id_user }
    try {
      const data = await dispatch(
        getAdsSFLearn,
        param,
        noop,
        true,
        false,
      )
      this.setState({ isLoading: false })
      if (data.code === 200) {
        this.setState({ result: data.result })
      }
    } catch (e) {
      this.setState({ isLoading: false })
    }
  };

  _onPullDownToRefresh = () => {
    this.setState({ result: {}, isLoading: true }, () => {
      this._getAdsSFLearn()
    })
  };

  _headerSection = (about) => {
    const { isStartup } = this.props
    // let bgHeader = require('sf-assets/images/bgSoundfrenLearn.png')
    // if(isStartup) {
    //   require('sf-assets/images/bgSoundfrenLearn.png')
    // }
    const startupSubtitle = 'Edukasi kamu melalui podcast & video'
    return (
      <View>
        <Image
          tint={'black'}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 198}
          source={require('sf-assets/images/bgSoundfrenLearn.png')}
        />
        <View
          style={{
            position: 'absolute',
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            backgroundColor: BLACK20,
          }}
        >
          <View
            style={{
              paddingTop: getStatusBarHeight(false) + WP3,
              paddingHorizontal: WP4,
              paddingBottom: WP4,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                ...SHADOW_STYLE['shadowBold'],
              }}
            >
              <Icon
                background='dark-circle'
                backgroundColor={WHITE}
                size='large'
                color={BLACK90}
                name='chevron-left'
                type='Entypo'
              />
              {!isStartup && <Icon
                background='dark-circle'
                size='large'
                color={WHITE}
                name='dots-three-horizontal'
                type='Entypo'
                             />}

            </View>
            <SkeletonContent
              containerStyle={{
                alignItems: 'center',
                paddingHorizontal: WP8,
                paddingVertical: WP3,
              }}
              layout={[
                { width: WP50, height: WP6, marginBottom: WP105 },
                { width: WP70, height: WP4 },
              ]}
              isLoading={this.state.isLoading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View style={{ ...SHADOW_STYLE['shadowBold'] }}>
                <Text
                  type='Circular'
                  size='large'
                  weight={600}
                  color={WHITE}
                  centered
                  style={{ marginBottom: WP1 }}
                >
                  {about?.title}
                </Text>
                <Text
                  type='Circular'
                  size='mini'
                  weight={300}
                  color={WHITE}
                  centered
                >
                  {isStartup ? startupSubtitle : about?.subtitle}
                </Text>
              </View>
            </SkeletonContent>
          </View>
        </View>
      </View>
    )
  };

  _onChangeTab = async (index) => {
    const { dispatch, userData } = this.props
    this.setState({ index })
    if (index == 1) {
      await dispatch(postLogMenuVideo, {
        id_user: userData.id_user,
        value: 'video',
      })
    }
  };

  _outsideContent = () => {
    return (
      <StatusBar
        animated={false}
        translucent={true}
        backgroundColor={'transparent'}
        barStyle={`${this.state.statusBarTheme}-content`}
      />
    )
  };

  _onScroll = ({
    nativeEvent: {
      contentOffset: { y: scrollPosition },
    },
  }) => {
    const { statusBarTheme: theme, headerOpacity } = this.state
    if (scrollPosition > 15 && theme === 'light') {
      RNAnimated.timing(headerOpacity, {
        toValue: 1,
        useNativeDriver: true,
        duration: 250,
        delay: 0,
        isInteraction: false,
      }).start()
      this.setState({
        statusBarTheme: 'dark',
      })
    }
    if (scrollPosition <= 15 && theme === 'dark') {
      RNAnimated.timing(headerOpacity, {
        toValue: 0,
        useNativeDriver: true,
        duration: 250,
        delay: 0,
        isInteraction: false,
      }).start()
      this.setState({
        statusBarTheme: 'light',
      })
    }
  };

  render() {
    const { navigateTo, userData, navigation, isStartup } = this.props
    const { isLoading, result } = this.state

    return (
      <Container
        scrollBackgroundColor={WHITE}
        isReady={true}
        isLoading={false}
        noStatusBarPadding
        outsideContent={this._outsideContent}
        statusBarBackground={NO_COLOR}
      >
        <NavigationEvents onWillFocus={this._onPullDownToRefresh} />
        <Animated.ScrollView
          onScroll={this._onScroll}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        >
          <View style={{ flex: 1, paddingBottom: getBottomSpace() }}>
            {this._headerSection(result?.about)}
            <View style={{ flex: 1, marginTop: -WP10 }}>
              <TabView
                navigationState={this.state}
                initialLayout={{ height: 0 }}
                swipeEnabled={false}
                renderScene={({ route }) => {
                  switch (route.key) {
                    case 'podcast':
                      return (
                        <PodcastTab
                          isStartup={isStartup}
                          data={result?.podcast}
                          loading={isLoading}
                          navigateTo={navigateTo}
                          userData={this.props.userData}
                          getData={this._onPullDownToRefresh}
                        />
                      )
                    case 'video':
                      return (
                        <VideoTab
                          isStartup={isStartup}
                          data={result?.video}
                          loading={isLoading}
                          navigateTo={navigateTo}
                          userData={this.props.userData}
                          getData={this._onPullDownToRefresh}
                        />
                      )
                  }
                }}
                renderTabBar={(props) => (
                  <TabBar
                    {...props}
                    style={[
                      {
                        backgroundColor: WHITE,
                        elevation: 0,
                        borderTopLeftRadius: 12,
                        borderTopRightRadius: 12,
                        ...BORDER_STYLE['bottom'],
                      },
                    ]}
                    indicatorStyle={{ backgroundColor: REDDISH }}
                    renderLabel={({ route, focused }) => {
                      return (
                        <Text
                          type='Circular'
                          centered
                          size='xmini'
                          color={focused ? REDDISH : SHIP_GREY_CALM}
                          weight={600}
                        >
                          {route.title}
                        </Text>
                      )
                    }}
                  />
                )}
                onIndexChange={this._onChangeTab}
              />
            </View>
          </View>
        </Animated.ScrollView>
        <RNAnimated.View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            backgroundColor: WHITE,
            paddingTop: getStatusBarHeight(true),
            opacity: this.state.headerOpacity,
          }}
        >
          <HeaderNormal
            withExtraPadding
            iconLeftOnPress={this.props.navigateBack}
            centered
            textType={'Circular'}
            textSize={'slight'}
            text={'Podcasts & Videos'}
            rightComponent={() => {
              return (
                  <Modal
                    position='bottom'
                    swipeDirection='none'
                    renderModalContent={({ toggleModal }) => {
                      return (
                        <SoundfrenExploreOptions
                          menuOptions={LearnOptionMenus}
                          userData={this.props.userData}
                          navigateTo={this.props.navigateTo}
                          onClose={toggleModal}
                        />
                      )
                    }}
                  >
                    {({ toggleModal }, M) => (
                      <Fragment>
                        <Touchable
                          style={{
                            paddingVertical: WP3,
                            paddingHorizontal: WP305,
                          }}
                          onPress={isStartup ? noop : restrictedAction({
                            action: toggleModal,
                            userData,
                            navigation,
                          })}
                          disabled={!!isStartup}
                        >
                          <Icon
                            background='dark-circle'
                            size='large'
                            color={isStartup ? TRANSPARENT : SHIP_GREY}
                            name='dots-three-horizontal'
                            type='Entypo'
                          />
                        </Touchable>
                        {M}
                      </Fragment>
                    )}
                  </Modal>
              )
            }}
          />
          <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
          <View>
            <MessageBarClearBlue />
          </View>
        </RNAnimated.View>
      </Container>
    )
  }
}

SoundfrenLearnScreen.propTypes = {
  navigateTo: PropTypes.func,
}

SoundfrenLearnScreen.defaultProps = {
  navigateTo: () => { },
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(SoundfrenLearnScreen),
  mapFromNavigationParam,
)
