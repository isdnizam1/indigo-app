import React, { Component, Fragment } from 'react'
import { FlatList, ImageBackground, TouchableOpacity, View } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import { noop } from 'lodash-es'
import { WP105 } from 'sf-constants/Sizes'
import Spacer from 'sf-components/Spacer'
import { BLACK90, GREY, GREY_CALM, NO_COLOR, PALE_GREY, SHIP_GREY_CALM, TRANSPARENT, WHITE } from '../../constants/Colors'
import { Container, Icon, Text } from '../../components'
import { HP5, WP05, WP1, WP12, WP2, WP205, WP3, WP30, WP4, WP5, WP6 } from '../../constants/Sizes'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { getAds, getPodcastCategory } from '../../actions/api'
import { SHADOW_STYLE } from '../../constants/Styles'
import { getBottomSpace, getStatusBarHeight } from '../../utils/iphoneXHelper'
import Image from '../../components/Image'
import Modal from '../../components/Modal'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { mapListWithAdditionalData } from '../../utils/mapper'
import SoundfrenExploreOptions from '../../components/explore/SoundfrenExploreOptions'
import { speakerDisplayName } from '../../utils/transformation'
import PodcastFilterCategory from './PodcastFilterCategory'
import LearnOptionMenus from './LearnOptionMenus'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  category: getParam('category', ''),
  isStartup: getParam('isStartup', false)
})

class PodcastListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      categories: [],
      selectedCategories: [],
      albums: []
    }
  }

  async componentDidMount() {
    await this._getCategories()
    await this._getAlbums()
  }

  _getCategories = async () => {
    const {
      dispatch, category,userData: { id_user }
    } = this.props

    const dataResponse = await dispatch(getPodcastCategory, {
      'id_user': id_user
    }, noop, false, true)
    const categories = dataResponse;
    const selectedCategories = categories.filter((item) => item.category_name === category)
    await this.setState({ categories, selectedCategories })
  }

  _getAlbums = async () => {
    const {
      dispatch,
      isStartup,
      userData: { id_user },
    } = this.props
    try {
      let params = { status: 'active', category: 'sp_album', id_viewer:id_user,id_sp_album_category: this._selectedCategoryIds() }
      if (isStartup) {
        params = { status: 'active', category: 'sp_album', id_viewer:id_user, platform: '1000startup', id_sp_album_category: this._selectedCategoryIds() }
      }
      const data = await dispatch(getAds, params, noop, true, false)
      if (data.code === 200) {
        this.setState({ albums: mapListWithAdditionalData(data.result), isLoading: false })
      } else {
        this.setState({ albums: [], isLoading: false })
      }
    } catch (e) {
      this.setState({ isLoading: false })
    }
  }

  _onSelectCategories = (selectedCategories) => {
    this.setState({ selectedCategories }, this._getAlbums)
  }

  _selectedCategoryIds = () => {
    const { selectedCategories } = this.state
    if (selectedCategories.length === 0) return undefined
    return selectedCategories.map((category) => category.id_sp_album_category)
  }

  _onPullDownToRefresh = () => {
    this.setState({ isLoading: true }, () => {
      return this._getAlbums()
    })
  }

  _headerSection = () => {
    const { navigateBack, navigateTo, isStartup } = this.props
    return (
      <View>
        <ImageBackground
          source={require('../../assets/images/bgSoundfrenLearn.png')}
          style={{ paddingTop: getStatusBarHeight(false) + WP3, paddingHorizontal: WP4, paddingBottom: WP12 }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', ...SHADOW_STYLE['shadowBold'] }}>
            <Icon
              onPress={() => navigateBack()}
              background='dark-circle'
              backgroundColor={WHITE}
              size='large'
              color={BLACK90}
              name='chevron-left'
              type='Entypo'
            />
            <Text weight={500} color={WHITE} size='medium'>
              Podcast
            </Text>
            <Modal
              position='bottom'
              swipeDirection='none'
              renderModalContent={({ toggleModal }) => {
                return (
                  <SoundfrenExploreOptions
                    menuOptions={LearnOptionMenus}
                    userData={this.props.userData}
                    navigateTo={navigateTo}
                    onClose={toggleModal}
                  />
                )
              }}
            >
              {
                ({ toggleModal }, M) => (
                  <Fragment>
                    <Icon
                      onPress={isStartup ? noop : toggleModal}
                      background='dark-circle'
                      size='large'
                      color={isStartup ? TRANSPARENT : WHITE}
                      name='dots-three-horizontal'
                      type='Entypo'
                    />
                    {M}
                  </Fragment>
                )
              }
            </Modal>
          </View>
        </ImageBackground>
      </View>
    )
  }

  _renderFilterCategory = () => {
    const {
      selectedCategories,
      categories
    } = this.state
    return (
      <Modal
        position='bottom'
        swipeDirection='none'
        renderModalContent={({ toggleModal }) => {
          return (
            <PodcastFilterCategory
              onClose={toggleModal}
              onSelect={this._onSelectCategories}
              categories={categories}
              selectedCategories={selectedCategories}
            />
          )
        }}
      >
        {
          ({ toggleModal }, M) => (
            <Fragment>
              {this._renderSectionHeader(toggleModal)}
              {M}
            </Fragment>
          )
        }
      </Modal>
    )
  }

  _getTitle = () => {
    const { selectedCategories } = this.state
    if (selectedCategories.length === 0) return 'Semua Album'
    if (selectedCategories.length === 1) return selectedCategories[0].category_name
    return 'Albums'
  }

  _renderSectionHeader = (toggleModal) => {
    const { selectedCategories } = this.state
    const categoryFilterCounter = selectedCategories.length === 0 ? '' : `(${selectedCategories.length})`
    return (
      <View style={{ borderBottomWidth: 1, borderBottomColor: GREY_CALM, marginBottom: WP3 }}>
        <View style={{
          flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
          paddingHorizontal: WP6, paddingVertical: WP5
        }}
        >
          <Text type='Circular' weight={600}>{this._getTitle()}</Text>
          <TouchableOpacity
            style={{
              borderWidth: 1, borderColor: SHIP_GREY_CALM, borderRadius: 8,
              paddingHorizontal: WP205,
              backgroundColor: PALE_GREY, flexDirection: 'row', alignItems: 'center',
              paddingVertical: WP105
            }}
            onPress={() => { toggleModal() }}
          >
            <View>
              <Icon
                color={SHIP_GREY_CALM}
                name={'filter-variant'}
                type={'MaterialCommunityIcons'}
              />
            </View>
            <Spacer size={WP1} horizontal />
            <View>
              <Text type='Circular' weight={400} color={SHIP_GREY_CALM} size='mini'>{`Category ${categoryFilterCounter}`}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _renderAlbum = ({ item: album, index }) => {
    const { categories } = this.state
    const category = categories.find((item) => item.id_sp_album_category == album.additionalData.id_sp_album_category)
    const { isStartup } = this.props;
    var speaker = 'Speaker Name';
    if(album.additional_data){
      var additional_data=JSON.parse(album.additional_data);
      if(additional_data.episodeList[0]?.speaker_name){
        var speaker = additional_data.episodeList[0]?.speaker_name;
      }
    }
    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY} onPress={() => this.props.navigateTo('SoundplayDetail', {
          id_ads: album.id_ads,
          isStartup
        })}
      >
        <View
          key={index}
          style={{
            flexDirection: 'row', paddingHorizontal: WP6, paddingVertical: WP3
          }}
        >
          <Image
            source={{ uri: album.image }}
            centered={false}
            imageStyle={{ width: WP30, borderRadius: 10, marginRight: WP4 }}
          />
          <View style={{ flex: 1 }}>
            {
              category && (
                <View style={{ flexDirection: 'row', marginBottom: WP2 }}>
                  <View style={{ paddingHorizontal: WP205, paddingVertical: WP05, backgroundColor: PALE_GREY, borderRadius: 6 }}>
                    <Text type='Circular' size='xmini' weight={400} color={SHIP_GREY_CALM}>{category.category_name}</Text>
                  </View>
                </View>
              )
            }
            <Text lineHeight={WP5 + 1.5} type='Circular' weight={500} size='slight' style={{ marginBottom: WP2 }}>{album.title}</Text>
            <Text type='Circular' size='mini' color={SHIP_GREY_CALM}>{speaker}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const {
      albums, isLoading
    } = this.state
    return (
      <Container
        theme='light'
        scrollBackgroundColor={WHITE}
        isReady={true}
        isLoading={false}
        noStatusBarPadding
        statusBarBackground={NO_COLOR}
      >
        <NavigationEvents
          onWillFocus={this._onPullDownToRefresh}
        />
        <View style={{ flex: 1, paddingBottom: getBottomSpace() }}>
          {this._headerSection()}
          <View style={{
            flex: 1, elevation: 0, marginTop: -12,
            borderTopLeftRadius: 12, borderTopRightRadius: 12, backgroundColor: WHITE
          }}
          >
            {this._renderFilterCategory()}
            <FlatList
              data={albums}
              renderItem={this._renderAlbum}
              keyExtractor={(item) => item.id_ads.toString()}
              initialNumToRender={albums.length}
              ListEmptyComponent={
                !isLoading && (
                  <View style={{ paddingHorizontal: WP6, paddingVertical: HP5 }}>
                    <Text centered size='mini' color={GREY} type='NeoSans'>{'It’s empty here,\n coming soon audio content.'}</Text>
                  </View>
                )
              }
            />
          </View>
        </View>
      </Container>
    )
  }
}

PodcastListScreen.propTypes = {
  navigateTo: PropTypes.func
}

PodcastListScreen.defaultProps = {
  navigateTo: () => {
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(PodcastListScreen),
  mapFromNavigationParam
)
