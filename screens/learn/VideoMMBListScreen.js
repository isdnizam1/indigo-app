import React, { Component } from 'react'
import { View, SafeAreaView, ScrollView, Image, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { noop } from 'lodash-es'
import { Alert } from 'react-native'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { HeaderNormal, Text } from '../../components'

import { getVideoList } from '../../actions/api'
import styles from './styles'

const mapStateToProps = ({ auth }) => ({
	userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
	category: getParam('category', 'Growth'),
	subCategory: getParam('subCategory', ''),
})

class VideoMMBListScreen extends Component {
    constructor(props) {
        super(props)

        this.state = {
			isLoading: false,
			data: null,
        }
    }

	async componentDidMount() {
        await this._getData()
    }

    _getData = async () => {
		const { category, subCategory, userData } = this.props
        this.setState({ isLoading: true })
        const { dispatch } = this.props

        try {
            const data = await dispatch(
				getVideoList,
				{
					content_category: category,
					content_sub_category: subCategory,
					id_user: userData.id_user,
				},
				noop,
				true,
				false,
            )
            this.setState({ isLoading: false })
            if (data.code === 200) {
				this.setState({
					data: data.result
				},
				() => {
					// data[0]
					// "created_by": "test",
					// "description": "content description",
					// "id_ads": "1083",
					// "image": "https://apidev.soundfren.id/userfiles/images/ads/1082_banner_picture_1584021834.png",
					// "locked": false,
					// "platform": "soundfren",
					// "title": "Highlights Soundfren Connect - Januari 2021",
					// "total_view": 48,
					// "video_status": "not-played",

					// JSON.parse(this.state.data[0].additional_data)
					// "category": Array [
						// 	"Copyright & Related",
						// 	"Strategi & Pemasaran Musik",
						// 	"Publikasi Musik",
					//   ],
					//   "learn": Array [
						// 	"Pengenalan",
						// 	"Isi",
						// 	"Penutup",
					//   ],
					//   "thumbnail": "https://webdev.soundfren.id/assets/image/event/sf_meetup_vol3.jpeg",
					//   "url_video": "https://s3.soundfren.id/converted/SC%20Exclusive%20Content%20-%20January%202020.m3u8",
					//   "url_video_teaser": "https://s3.soundfren.id/converted/SC%20Exclusive%20Content%20-%20January%202020.m3u8",
					//   "video_duration": "37:31",
				})
            }
        } catch (e) {
            Alert.alert('Error', e)
            this.setState({ isLoading: false })
        }
    }

	_itemVideo = (item, index) => {
		const additionalData = JSON.parse(item.additional_data)

		return (
			<View key={index}>
				<View style={styles.itemVideoContainer} key={index}>
					<View style={styles.itemVideoThumbnailContainer}>
						<Image
							style={styles.itemVideoThumbnail}
							// aspectRatio={1}
							source={
								item.image ? { uri: item.image } :
								require('../../assets/icons/icon_category.png')
							}
						/>
					</View>
					<View>
						{/* title */}
						<Text
							style={{ width: 205 }}
							type='Circular'
							size={'slight'}
							lineHeight={20}
							weight={700}
							color={'#454F5B'}
						>
							{item.title}
						</Text>
						{/* duration, total views */}
						<View style={{ flexDirection: 'row' }}>
							<Text
								type='Circular'
								size={'xmini'}
								lineHeight={18}
								weight={400}
								color={'#919EAB'}
							>
								{additionalData.video_duration}
							</Text>
							<Text
								style={{ bottom: 3, marginHorizontal: 3 }}
								type='Circular'
								size={'xmini'}
								lineHeight={18}
								weight={400}
								color={'#919EAB'}
							>
								.
							</Text>
							<Text
								type='Circular'
								size={'xmini'}
								lineHeight={18}
								weight={400}
								color={'#919EAB'}
							>
								{`${item.total_view} Views`}
							</Text>
						</View>
						{/* action, status */}
						<View style={styles.itemVideoActionContainer}>
							{!item.locked ?
								item.video_status === 'not-played' || item.video_status ==='playing' ?
								(
									<TouchableOpacity
									style={styles.itemVideoActionButtonPlay}
									onPress={() => {
										this.props.navigateTo('PremiumVideoTeaser', { id_ads: item.id_ads, isStartup: true, refreshListVideo: () => { this._getData() } })
									}}
									>
										<Text
											type='Circular'
											size={'xmini'}
											lineHeight={18}
											weight={400}
											color={'#FFFFFF'}
										>
											Lihat
										</Text>
									</TouchableOpacity>
								)
								:
								item.video_status === 'completed' &&
								(
									<TouchableOpacity
									style={styles.itemVideoActionButtonPlayAgain}
									onPress={() => {
										this.props.navigateTo('PremiumVideoTeaser', { id_ads: item.id_ads, isStartup: true, refreshListVideo: () => { this._getData() } })
									}}
									>
										<Text
											type='Circular'
											size={'xmini'}
											lineHeight={18}
											weight={400}
											color={'#919EAB'}
										>
											Lihat lagi
										</Text>
									</TouchableOpacity>
								)
							:
								(
									<View style={styles.itemVideoActionButtonLocked}>
										<Text
											type='Circular'
											size={'xmini'}
											lineHeight={15}
											weight={400}
											color={'#919EAB'}
										>
											Terkunci
										</Text>
									</View>
								)
							}
							{!item.locked && item.video_status === 'playing' ?
							(
								<View style={styles.itemVideoStatusContainer}>
									<Image source={require('../../assets/icons/ic_progress.png')}/>
									<Text
										style={{ marginLeft: 4 }}
										type='Circular'
										size={'xmini'}
										lineHeight={18}
										weight={400}
										color={'#3287EC'}
									>
										In Progress
									</Text>
								</View>
							)
							:
							item.video_status === 'completed' &&
							(
								<View style={styles.itemVideoStatusContainer}>
									<Image source={require('../../assets/icons/ic_done.png')}/>
									<Text
										style={{ marginLeft: 4 }}
										type='Circular'
										size={'xmini'}
										lineHeight={18}
										weight={400}
										color={'#50B83C'}
									>
										Selesai
									</Text>
								</View>
							)
							}
						</View>
					</View>
				</View>
				<View style={styles.separatorLight}/>
			</View>
		)
	}

	render() {
		const { subCategory } = this.props
		const { data } = this.state
		let dataCurrentVideos = []
		let dataNextVideos = []

		if (data !== null) {
			dataCurrentVideos = data.filter(function (d) { return !d.locked })
			dataNextVideos = data.filter(function (d) { return d.locked })
		}

		// console.log({data})

		return (
			<SafeAreaView style={styles.container}>
				<HeaderNormal shadow centered={true} text={subCategory} iconLeftOnPress={() => this.props.navigation.goBack()}/>
				<ScrollView>
					<View style={styles.contentContainer}>
						{(data && data.length === 0) || data === null && (
							<View style={styles.emptyStateContainer}>
								<Image source={require('../../assets/icons/ic_empty_video.png')}/>
								<Text
									style={{ marginTop: 16 }}
									type='Circular'
									size={'small'}
									lineHeight={18}
									weight={400}
									color={'#454F5B'}
								>
									Siapkan Dirimu dengan Videos
								</Text>
								<Text
									style={{ marginTop: 3, marginBottom: 16 }}
									type='Circular'
									size={'mini'}
									lineHeight={15}
									weight={400}
									color={'#919EAB'}
								>
									Kembangkan dirimu dengan menonton video
								</Text>
							</View>
						)}
						{dataCurrentVideos.length > 0 && (
							<View style={styles.videoListContainer}>
								<Text
									style={{ marginVertical: 16 }}
									type='Circular'
									size={'small'}
									lineHeight={24}
									weight={700}
									color={'#212B36'}
								>
									Mulai pembelajaran kamu
								</Text>
								{dataCurrentVideos.map(this._itemVideo)}
							</View>
						)}
						<View style={styles.videoListContainer}>
							{data && data.length > 0 && (
								<Text
									style={{ marginBottom: 16 }}
									type='Circular'
									size={'small'}
									lineHeight={24}
									weight={700}
									color={'#212B36'}
								>
									Pembelajaran selanjutnya
								</Text>
							)}
							{dataNextVideos.length > 0 ?
								dataNextVideos.map(this._itemVideo)
								:
								data && data.length > 0 && (
									<View style={styles.learnFinishedContainer}>
										<Image source={require('../../assets/icons/ic_graduate.png')}/>
										<Text
											style={{ marginBottom: 3, marginTop: 12 }}
											type='Circular'
											size={'slight'}
											lineHeight={18}
											weight={400}
											color={'#454F5B'}
										>
											Materi sudah selesai
										</Text>
										<Text
											style={{ marginBottom: 10, textAlign: 'center' }}
											type='Circular'
											size={'mini'}
											lineHeight={20}
											weight={400}
											color={'#919EAB'}
										>
											Silahkan pelajari materi lain untuk menambah pengetahuan kamu
										</Text>
									</View>
								)
							}
						</View>
					</View>
				</ScrollView>
			</SafeAreaView>
		)
	}
}

VideoMMBListScreen.propTypes = {
  navigateTo: PropTypes.func,
}

VideoMMBListScreen.defaultProps = {
  navigateTo: () => {},
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(VideoMMBListScreen),
  mapFromNavigationParam,
)