import { WP1, WP3, WP4, WP40, WP6 } from '../../constants/Sizes'
import { NAVY_DARK, PALE_BLUE, PALE_BLUE_TWO, REDDISH, SHIP_GREY_CALM } from '../../constants/Colors'

export default {
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 20
  },
  contentContainer: {
    marginBottom: 20,
  },
  titleText: {
    color: NAVY_DARK,
    fontSize: 16,
    lineHeight: 24,
    marginHorizontal: 20,
    marginVertical: 5
  },
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: '#E4E6E7',
    marginTop: 5,
    marginBottom: 24,
  },
  separatorSmall: {
    width: '100%',
    height: 1,
    backgroundColor: '#E4E6E7',
    marginTop: 5,
    marginBottom: 20,
  },
  separatorLight: {
    width: '100%',
    height: 1,
    backgroundColor: '#F4F6F8',
    marginBottom: 15,
  },
  itemCategoryContainer: {
    flexDirection: 'row',
    marginHorizontal: 16,
    padding: 12,
    backgroundColor: '#FFFFFF',
    borderRadius: 6,
    elevation: 2,
    marginBottom: 16,
  },
  itemCategoryTextContainer: {
    width: 220,
    marginHorizontal: 10,
    alignSelf: 'center'
  },
  iconRightContainer: {
    alignSelf: 'center'
  },
  itemSubCategoryContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
  videoListContainer: {
    marginHorizontal: 20,
    marginBottom: 24
  },
  itemVideoContainer: {
    flexDirection: 'row',
    marginBottom: 20
  },
  itemVideoThumbnailContainer: {
    marginRight: 16
  },
  itemVideoThumbnail: {
    width: 99,
    height: 99,
    resizeMode: 'cover',
    borderRadius: 10
  },
  itemVideoActionContainer: {
    flexDirection: 'row'
  },
  itemVideoActionButtonPlay: {
    backgroundColor: '#FF651F',
    alignItems: 'center',
    justifyContent: 'center',
    width: 65,
    height: 25,
    borderRadius: 8,
    marginTop: 5
  },
  itemVideoActionButtonPlayAgain: {
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#919EAB55',
    alignItems: 'center',
    justifyContent: 'center',
    width: 65,
    height: 25,
    borderRadius: 8,
    marginTop: 5
  },
  itemVideoActionButtonLocked: {
    backgroundColor: '#F4F6F8',
    alignItems: 'center',
    justifyContent: 'center',
    width: 65,
    height: 25,
    borderRadius: 8,
    marginTop: 5
  },
  itemVideoStatusContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 8,
    marginTop: 8,
    flexDirection: 'row'
  },
  learnFinishedContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20
  },
  emptyStateContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 24
  }
}
