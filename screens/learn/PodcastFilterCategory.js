import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { FlatList, TouchableOpacity, View } from 'react-native'
import { CheckBox } from 'react-native-elements'
import noop from 'lodash-es/noop'
import Text from 'sf-components/Text'
import { HP100, WP20, WP3, WP4, WP5, WP6, WP8 } from 'sf-constants/Sizes'
import { getStatusBarHeight } from 'sf-utils/iphoneXHelper'
import { GREY_CALM, GUN_METAL, REDDISH, WHITE } from 'sf-constants/Colors'
import Image from 'sf-components/Image'
import Button from 'sf-components/Button'

class PodcastFilterCategory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selected: this.props.selectedCategories || [],
      isDirty: false
    }
  }

  _onClose = () => {
    this.setState({ selected: this.props.selectedCategories, isDirty: false })
    this.props.onClose()
  }

  _onSelect = () => {
    this.setState({ isDirty: false })
    this.props.onSelect(this.state.selected)
    this.props.onClose()
  }

  _isCategorySelected = (category) => {
    const { selected } = this.state
    return selected.find((item) => item.id_sp_album_category === category.id_sp_album_category)
  }

  _onPressCategory = (category) => async () => {
    const { selected } = this.state
    let selectedCategories = []

    if (this._isCategorySelected(category)) {
      selectedCategories = selected.filter((item) => Number(item.id_sp_album_category) !== Number(category.id_sp_album_category) )
    } else {
      selectedCategories = [...selected, category]
    }

    await this.setState({ selected: selectedCategories, isDirty: true })
  }

  _renderSectionHeader = () => {
    return (
      <View style={{ borderBottomWidth: 1, borderBottomColor: GREY_CALM, marginBottom: WP3 }}>
        <View style={{
          flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',
          paddingHorizontal: WP6, paddingVertical: WP5
        }}
        >
          <Text type='Circular' weight={600}>Pilih Category</Text>
          <TouchableOpacity
            onPress={this._onClose}
          >
            <Image
              source={require('sf-assets/icons/close.png')}
              imageStyle={{ width: WP8 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _renderCategory = ({ item: category, index }) => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row', justifyContent: 'space-between',
          paddingHorizontal: WP6, paddingVertical: WP5, borderBottomColor: GREY_CALM, borderBottomWidth: 1
        }}
        onPress={this._onPressCategory(category)}
      >
        <Text type='Circular' size='slight' weight={300} color={GUN_METAL}>{category.category_name}</Text>
        <CheckBox
          containerStyle={{ padding: 0, margin: 0 }}
          checked={this._isCategorySelected(category)}
          onPress={this._onPressCategory(category)}
          checkedColor={REDDISH}
        />
      </TouchableOpacity>
    )
  }

  render() {
    const { categories } = this.props
    const { isDirty } = this.state
    return (
      <View style={{
        height: HP100 - (getStatusBarHeight(false) + WP20)
      }}
      >
        { this._renderSectionHeader() }
        <FlatList
          data={categories}
          renderItem={this._renderCategory}
          keyExtractor={(item, index) => index.toString()}
        />
        <Button
          contentStyle={{ marginHorizontal: WP6, paddingVertical: WP4, borderRadius: 10 }}
          backgroundColor={REDDISH}
          text='Cari Podcast'
          textColor={WHITE}
          centered
          textType='Circular'
          textWeight={500}
          textSize='medium'
          disable={!isDirty}
          onPress={this._onSelect}
        />
      </View>
    )
  }
}
PodcastFilterCategory.propTypes = {
  selectedCategories: PropTypes.arrayOf(PropTypes.any),
  categories: PropTypes.arrayOf(PropTypes.any),
  onClose: PropTypes.func,
  onSelect: PropTypes.func
}

PodcastFilterCategory.defaultProps = {
  selectedCategories: [],
  categories: [],
  onClose: noop,
  onSelect: noop,
}

export default PodcastFilterCategory
