import React from 'react'
import { View, TouchableOpacity, FlatList, Image } from 'react-native'
import { isEmpty, upperFirst } from 'lodash-es'
import moment from 'moment'
import { Container, Text } from '../../components'
import {
  PALE_BLUE,
  NAVY_DARK,
  GUN_METAL,
  SHIP_GREY_CALM,
  PALE_WHITE,
} from '../../constants/Colors'
import { WP4, WP6, WP2, WP1, HP50 } from "../../constants/Sizes";
import { TOUCH_OPACITY } from '../../constants/Styles'
import Spacer from '../../components/Spacer'
import { getYoutubeId } from '../../utils/helper'
import EmptyState from '../../components/EmptyState'

const VideoTab = ({ data, getData, navigateTo, userData, isStartup=false }) => {
  const _renderVideo = ({ item: video, index }) => {
    const additional = JSON.parse(video.additional_data)
    return (
      <TouchableOpacity
        onPress={() => {
          if (!userData.id_user) navigateTo('AuthNavigator')
          else {
            const youtubeId = getYoutubeId(additional.url_video)
              navigateTo('PremiumVideoTeaser', { id_ads: video.id_ads, isStartup })
          }
        }}
        activeOpacity={TOUCH_OPACITY}
        style={{ marginBottom: WP6 }}
      >
        <View style={{ backgroundColor: PALE_BLUE, borderRadius: WP2 }}>
          <Image
            source={{ uri: additional.thumbnail }}
            style={{ width: '100%', aspectRatio: 1.75, borderRadius: WP2 }}
          />
        </View>
        <Spacer size={WP2} />
        <Text type='Circular' size='small' color={GUN_METAL} weight={500}>
          {upperFirst(video.title)}
        </Text>
        <Spacer size={WP1} />
        <Text type='Circular' size='xmini' color={SHIP_GREY_CALM}>
          {upperFirst(
            `${moment(video.created_at).locale('id').format('DD MMM Y')}  ·  ${
              video.total_view
            } Ditonton`,
          )}
        </Text>
      </TouchableOpacity>
    )
  }
  return (
    <Container
      isReady={true}
      isLoading={false}
      theme="light"
      noStatusBarPadding
      scrollable
      scrollBackgroundColor={PALE_WHITE}
      onPullDownToRefresh={() => getData()}
    >
      <View style={{ padding: WP4, paddingTop: WP6, paddingBottom: WP6 }}>
        <Text type="Circular" size="medium" color={NAVY_DARK} weight={600}>
          Video Pilihan
        </Text>
        <Spacer size={WP6} />
        {!isEmpty(data) ? (
          <FlatList
            data={data || []}
            keyExtractor={(video) => video.id_ads.toString()}
            renderItem={_renderVideo}
          />
        ) : (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              height: HP50,
            }}
          >
        <EmptyState
          image={require(`sf-assets/icons/ic_media_video_emptystate.png`)}
          title={'Belum ada video saat ini'}
          message={'Tunggu video-video menarik hanya untuk kamu'}
        />
          </View>
        )}
      </View>
    </Container>
  );
}

export default VideoTab
