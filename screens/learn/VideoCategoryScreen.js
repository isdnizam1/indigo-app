import React, { Component } from 'react'
import { View, SafeAreaView, ScrollView, TouchableOpacity, ImageBackground } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { LinearGradient } from 'expo-linear-gradient'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { noop } from 'lodash-es'
import { Alert } from 'react-native'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { HeaderNormal, Icon, Image, Text } from '../../components'

import {
SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
} from '../../constants/Colors'
import {
  WP100,
  WP105,
  WP3,
  WP4,
  WP50,
  WP6,
  WP70,
  WP8,
  WP2,
	WP1,
} from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'
import { getStatusBarHeight } from '../../utils/iphoneXHelper'
import { getVideoCategory } from '../../actions/api'
import styles from './styles'
import { restrictedAction } from '../../utils/helper'

const mapStateToProps = ({ auth }) => ({
	userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
    //
})

class VideoCategoryScreen extends Component {
    constructor(props) {
        super(props)

        this.state = {
			isLoading: false,
			data: null,
        }
    }

    async componentDidMount() {
        await this._getData()
    }

    _getData = async () => {
        this.setState({ isLoading: true })
        const { dispatch } = this.props

        try {
            const data = await dispatch(
				getVideoCategory,
				{ platform: '1000startup' },
				noop,
				true,
				false,
            )
            this.setState({ isLoading: false })
            if (data.code === 200) {
				this.setState({
					data: JSON.parse(data.result.value.replace(/\\"n/g, ' '))
				})
            }
        } catch (e) {
            Alert.alert('Error', e)
            this.setState({ isLoading: false })
        }
    }

	_onPullDownToRefresh = () => {
		this.setState({ isLoading: true }, () => {
			this._getData()
		})
	};

	_headerSection = () => {
		const image = this.state.data ? this.state.data.image : null

		return (
			<View>
				<Image
					tint={'black'}
					imageStyle={{ width: WP100, height: undefined }}
					aspectRatio={360 / 180}
					source={
						image ? { uri: image } : require('../../assets/images/bgStartupVideoPodcast.png')
					}
				/>
				<LinearGradient
					style={{
						position: 'absolute',
						left: 0,
						top: 0,
						right: 0,
						bottom: 0,
					}}
					colors={[
						'rgba(0, 0, 0, 0)',
						'rgba(0, 0, 0, 0.5)',
						'rgba(0, 0, 0, 1)',
					]}
				>
					<View
						style={{
							paddingTop: getStatusBarHeight(false) + WP3,
							paddingHorizontal: WP4,
							paddingBottom: WP4,
						}}
					>
						<SkeletonContent
							containerStyle={{
								alignItems: 'center',
								paddingHorizontal: WP8,
								paddingVertical: WP3,
							}}
							layout={[
								{ width: WP50, height: WP6, marginBottom: WP105 },
								{ width: WP70, height: WP4 },
							]}
							isLoading={this.state.isLoading}
							boneColor={SKELETON_COLOR}
							highlightColor={SKELETON_HIGHLIGHT}
						>
							<View style={{ ...SHADOW_STYLE['shadowBold'] }}>
								<View style={{ marginTop: WP2 }} />
								<Text
									style={{ marginBottom: WP2 }}
									type='Circular'
									size='medium'
									weight={700}
									color={WHITE}
									centered
								>
									Videos
								</Text>
								<Text
									style={{ width: 250 }}
									type='Circular'
									size={12}
									lineHeight={21}
									weight={400}
									color={WHITE}
									centered
								>
									Edukasi untuk kamu melalui video kapan saja dimana saja
								</Text>
							</View>
						</SkeletonContent>
					</View>
				</LinearGradient>
			</View>
		)
	};

	_itemCategory = (item, index) => {
		return (
			<TouchableOpacity
				style={styles.itemCategoryContainer} onPress={() => {
					this.props.navigateTo('VideoSubCategoryScreen', {
						title: item.title,
						data: item.sub_category
					})
				}}
				key={index}
			>
				{item.icon ? (
				<Image
					imageStyle={{ width: 48, height: undefined }}
					aspectRatio={1}
					source={
						item.icon ? { uri: item.icon } :
						require('../../assets/icons/icon_category.png')
					}
				/>
				)
				:
				(
					<ImageBackground style={{ width: 48, height: 48, justifyContent: 'center' }} source={require('../../assets/icons/icon_category.png')}>
						<Image
							imageStyle={{ width: 38, height: undefined }}
							aspectRatio={1}
							source={
								item.icon ? { uri: item.icon } :
								require('../../assets/icons/ic_item_category.png')
							}
						/>
					</ImageBackground>
				)
				}
				<View style={styles.itemCategoryTextContainer}>
					<Text
						type='Circular'
						size={'small'}
						lineHeight={17}
						weight={700}
						color={'#454F5B'}
					>
						{item.title}
					</Text>
					<Text
						type='Circular'
						size={12}
						lineHeight={15}
						weight={400}
						color={'#919EAB'}
					>
						{item.description}
					</Text>
				</View>
				<View style={styles.iconRightContainer}>
					<Icon
						size={'large'}
						background={'none'}
						name={'chevron-right'}
						type={'Entypo'}
						centered
						color={SHIP_GREY_CALM}
					/>
				</View>
			</TouchableOpacity>
		)
	}

	_renderHeader = () => {
		const { navigateTo } = this.props
		return (
			<View
				style={{
					flexDirection: 'row',
					justifyContent: 'space-between',
					paddingHorizontal: WP4,
					paddingVertical: WP2,
				}}
			>
				<Icon
					centered
					onPress={() => this.props.navigation.goBack()}
					background='dark-circle'
					size='large'
					color={SHIP_GREY_CALM}
					name='chevron-left'
					type='Entypo'
				/>

				<TouchableOpacity
					onPress={restrictedAction({
						action: () => navigateTo('StartupSearchScreen', {searchType: 'video'}),
						userData: this.props.userData,
						navigation: this.props.navigation,
					})}
					activeOpacity={0.75}
					style={{
						flex: 1,
						backgroundColor: 'rgba(145, 158, 171, 0.1)',
						borderRadius: 6,
						flexDirection: 'row',
						alignItems: 'center',
						paddingHorizontal: WP2,
						paddingVertical: WP1,
						marginHorizontal: WP2,
					}}
				>
					<Icon
						centered
						background='dark-circle'
						size='large'
						color={SHIP_GREY_CALM}
						name='magnify'
						type='MaterialCommunityIcons'
						style={{ marginRight: WP1 }}
					/>
					<Text
						type='Circular'
						size='mini'
						weight={300}
						color={SHIP_GREY_CALM}
						centered
					>
						Coba cari Video
          </Text>
				</TouchableOpacity>
			</View>
		)
	};

	render() {
		const { data } = this.state

		return (
			<SafeAreaView style={styles.container}>
				{/* Todo: Header Search */}
				{/* <HeaderNormal centered={true} text={'Kategori Video'} iconLeftOnPress={() => this.props.navigation.goBack()}/> */}
				{this._renderHeader()}
				<ScrollView>
					<View style={styles.contentContainer}>
						{this._headerSection()}
						<Text type={'Circular'} weight={700} style={styles.titleText}>{'Pilih Kategori'}</Text>
						<View style={styles.separator}/>
						{data !== null && data.map(this._itemCategory)}
					</View>
				</ScrollView>
			</SafeAreaView>
		)
	}
}

VideoCategoryScreen.propTypes = {
  navigateTo: PropTypes.func,
}

VideoCategoryScreen.defaultProps = {
  navigateTo: () => {},
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(VideoCategoryScreen),
  mapFromNavigationParam,
)