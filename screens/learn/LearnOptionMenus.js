import { ORANGE_BRIGHT } from '../../constants/Colors'
import { FONTS } from '../../constants/Fonts'

export const learnOptions = (props) => {
  let menu = [
    // {
    //   type: 'menu',
    //   image: require('../../assets/icons/badgeSoundfrenPremium.png'),
    //   imageStyle: {},
    //   name: 'Premium',
    //   textStyle: { color: ORANGE_BRIGHT, fontFamily: FONTS.Circular['500'] },
    //   onPress: () => { },
    //   joinButton: false,
    //   isPremiumMenu: true
    // },
    {
      type: 'menu',
      image: require('../../assets/icons/icAboutShipGrey.png'),
      imageStyle: {},
      name: 'Tentang Media Streaming',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('AboutFeatureScreen', {
          content: {
            image: require('../../assets/images/bgSoundfrenLearn.png'),
            imageRatio: 360 / 198,
            title: 'Media Streaming',
            subtitle: 'Edukasi  melalui podcasts & videos',
            header: 'Tentang Media Streaming',
            paragraph: [
              'Platform streaming digital bagi para music enthusiast untuk mendapatkan pengetahuan seputar musik dari para pelaku industri musik berpengalaman dengan bentuk podcast dan video.',
              'Media Streaming menyediakan topik tentang wawasan tentang industri musik, produksi musik, sampai dengan manajemen musik yang tentunya dapat menjadi referensi utama bagi kalian music enthusiast mendapatkan pengetahuan dan wawasan di bidang musik.',
              'Podcast dan video di bawakan oleh para pelaku industri musik berpengalaman seperti Danilla Riyadi, Marcello Tahitoe, dan nama nama lain yang tentunya akan memberikan inspirasi bagi kalian para music enthusiast.',
              'Para music enthusiast dapat mengakses konten podcast dan video yang telah tersedia dari Media Streaming langsung dari aplikasi Eventeer.'
            ],
            route: 'SoundfrenLearnScreen',
            buttonTitle: 'Cari Podcast & Video'
          }
        })
      }
    },
    {
      type: 'menu',
      image: require('../../assets/icons/mdi_chat.png'),
      imageStyle: {},
      name: 'Feedback & Report',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('FeedbackScreen')
      }
    },
    {
      type: 'separator'
    },
    {
      type: 'menu',
      image: require('../../assets/icons/close.png'),
      imageStyle: {},
      name: 'Tutup',
      textStyle: {},
      onPress: props.onClose
    },
  ]
  if (props.onShare) menu.splice(1, 0,
    {
      type: 'menu',
      image: require('../../assets/icons/mdi_share.png'),
      imageStyle: {},
      name: 'Bagikan',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.onShare()
      },
      joinButton: false,
      isPremiumMenu: false
    })
  return {
    withPromoteUser: true,
    menus: menu
  }
}

export default learnOptions
