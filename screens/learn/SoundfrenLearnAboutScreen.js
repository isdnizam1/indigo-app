import React, { Component } from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { BLACK20, NAVY_DARK, NO_COLOR, REDDISH, SHIP_GREY_CALM, WHITE } from '../../constants/Colors'
import { Container, Image, Text } from '../../components'
import { WP100, WP105, WP2, WP3, WP4, WP6, WP8 } from '../../constants/Sizes'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { SHADOW_STYLE } from '../../constants/Styles'
import { getBottomSpace, getStatusBarHeight } from '../../utils/iphoneXHelper'
import Button from '../../components/Button'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
})

const content = {
  title: 'Soundfren Learn',
  subTittle: 'Nikmati konten video & podcast',
  header: 'Tentang Soundfren Learn',
  paragraph: [
    'Platform streaming digital bagi para music enthusiast untuk mendapatkan pengetahuan seputar musik dari para pelaku industri musik berpengalaman dengan bentuk podcast dan video.',
    'Soundfren Learn menyediakan topik tentang wawasan tentang industri musik, produksi musik, sampai dengan manajemen musik yang tentunya dapat menjadi referensi utama bagi kalian music enthusiast mendapatkan pengetahuan dan wawasan di bidang musik.',
    'Podcast dan video di bawakan oleh para pelaku industri musik berpengalaman seperti Danilla Riyadi, Marcello Tahitoe, dan nama nama lain yang tentunya akan memberikan inspirasi bagi kalian para music enthusiast.',
    'Para music enthusiast dapat mengakses konten podcast dan video yang telah tersedia dari Soundfren Learn langsung dari aplikasi Soundfren.'
  ]
}

class SoundfrenLearnAboutScreen extends Component {
  _headerSection = () => {
    return (
      <View>
        <Image
          tint={'black'}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 198}
          source={require('../../assets/images/bgSoundfrenLearn.png')}
        />
        <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0, backgroundColor: BLACK20 }}>
          <View style={{ paddingTop: getStatusBarHeight(false) + WP3, paddingHorizontal: WP4, paddingBottom: WP4 }}>
            <View style={{ ...SHADOW_STYLE['shadowBold'], alignItems: 'center', paddingHorizontal: WP8, paddingVertical: WP3 }}>
              <Text type='Circular' size='huge' weight={600} color={WHITE} centered style={{ marginBottom: WP105 }}>{content.title}</Text>
              <Text type='Circular' size='mini' weight={300} color={WHITE} centered >{content.subTittle}</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }

  _renderParagraph = (text, index) => {
    return (
      <Text
        key={index} type='Circular' color={SHIP_GREY_CALM} weight={300} size='mini'
        style={{ marginBottom: WP3 }}
      >
        {text}
      </Text>
    )
  }

  render() {
    const {
      navigateTo
    } = this.props

    return (
      <Container
        theme='light'
        scrollBackgroundColor={WHITE}
        isReady={true}
        isLoading={false}
        noStatusBarPadding
        statusBarBackground={NO_COLOR}
        scrollable
        outsideScrollContent={() => (
          <View style={{ backgroundColor: WHITE, paddingHorizontal: WP4, paddingVertical: WP2, ...SHADOW_STYLE['shadow'] }}>
            <Button
              shadow='none'
              contentStyle={{ paddingVertical: WP4, borderRadius: 6 }}
              backgroundColor={REDDISH}
              text='Cari Podcast & Video'
              textColor={WHITE}
              centered
              textType='Circular'
              textWeight={600}
              textSize='small'
              onPress={() => navigateTo('SoundfrenLearnScreen')}
            />
          </View>
        )}
      >
        <View style={{ flex: 1, paddingBottom: getBottomSpace() }}>
          {this._headerSection()}
          <View style={{
            paddingHorizontal: WP6, paddingTop: WP6
          }}
          >
            <Text
              type='Circular' weight={600} color={NAVY_DARK} size='small'
              style={{ marginBottom: WP4 }}
            >
              {content.header}
            </Text>
            {
              content.paragraph.map((text, index) => (
                this._renderParagraph(text, index)
              ))
            }
          </View>
        </View>
      </Container>
    )
  }
}

SoundfrenLearnAboutScreen.propTypes = {
  navigateTo: PropTypes.func
}

SoundfrenLearnAboutScreen.defaultProps = {
  navigateTo: () => {
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(SoundfrenLearnAboutScreen),
  mapFromNavigationParam
)
