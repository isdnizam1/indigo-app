import React, { Component } from 'react'
import { View, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { HeaderNormal, Icon, Text } from '../../components'

import {
	SHIP_GREY_CALM,
} from '../../constants/Colors'
import styles from './styles'

const mapStateToProps = ({ auth }) => ({
	userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
	title: getParam('title', 'Sub Category'),
	data: getParam('data', [])
})

class VideoSubCategoryScreen extends Component {
    constructor(props) {
        super(props)

        this.state = {
			isLoading: false,
			data: null,
        }
    }

	_itemSubCategory = (item, index) => {
		return (
			<TouchableOpacity
				onPress={() => {
					this.props.navigateTo('VideoMMBListScreen', {
						category: this.props.title,
						subCategory: item.title
					})
				}}
				key={index}
			>
				<View style={styles.itemSubCategoryContainer}>
					<Text
						type='Circular'
						size={'small'}
						lineHeight={20}
						weight={400}
						color={'#454F5B'}
					>
						{item.title}
					</Text>
					<View style={styles.iconRightContainer}>
						<Icon
							size={'large'}
							background={'none'}
							name={'chevron-right'}
							type={'Entypo'}
							centered
							color={SHIP_GREY_CALM}
						/>
					</View>
				</View>
				<View style={styles.separatorSmall}/>
			</TouchableOpacity>
		)
	}

	render() {
		const { title, data } = this.props

		return (
			<SafeAreaView style={styles.container}>
				<HeaderNormal shadow centered={true} text={title} iconLeftOnPress={() => this.props.navigation.goBack()}/>
				<ScrollView>
					<View style={styles.contentContainer}>
						<Text type={'Circular'} weight={700} style={styles.titleText}>{'Pilih Modul'}</Text>
					</View>
					{data.map(this._itemSubCategory)}
				</ScrollView>
			</SafeAreaView>
		)
	}
}

VideoSubCategoryScreen.propTypes = {
  navigateTo: PropTypes.func,
}

VideoSubCategoryScreen.defaultProps = {
  navigateTo: () => {},
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(VideoSubCategoryScreen),
  mapFromNavigationParam,
)