import React from 'react'
import { connect } from 'react-redux'
import {
  View,
  TouchableOpacity,
  Dimensions,
  Image as RNImage
} from 'react-native'
import { noop, isEmpty } from 'lodash-es'
import {
  _enhancedNavigation,
  Container,
  Image,
  HeaderNormal,
  Text
} from '../../components'
import {
  WHITE,
  GREY_WARM,
  colors
} from '../../constants/Colors'
import {
  WP6,
  WP100,
  HP1,
  HP2,
  HP3,
  HP10
} from '../../constants/Sizes'
import { BORDER_STYLE } from '../../constants/Styles'
import { initiateRoom } from '../../utils/helper'
import { getCollaborationDetailProject } from '../../actions/api'
import { authDispatcher } from '../../services/auth'
const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam('id_ads')
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher
}

const propsDefault = {}

class ProjectDashboard extends React.Component {

  constructor(props) {
    super(props)
  }

  state = {
    isLoading: true,
    isReady: false,
    ad: {
      list_user: []
    }
  }

  async componentDidMount() {
    const { dispatch, id_ads } = this.props
    try {
      const { result, code } = await dispatch(getCollaborationDetailProject, { id_ads }, noop, true, true)
      code == 200 && this.setState({ ad: result })
    } catch(err) { /*silent is gold*/ } finally {
      this.setState({ isLoading: false, isReady: true })
    }
  }

  async openChatRoom(id_user) {
    const { userData, navigateTo } = this.props
    const chatRoomDetail = await initiateRoom(userData, { id_user })
    await navigateTo('ChatRoomScreen', chatRoomDetail)
  }

  render() {
    const { navigateBack, isLoading, navigateTo } = this.props
    const {
      isReady,
      ad
    } = this.state
    const { title, image, list_user, active_duration } = ad
    return (
      <Container
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <HeaderNormal
            style={{ paddingVertical: HP2 }}
            iconLeftOnPress={navigateBack}
            textSize={'xmini'}
            text='Activity Dashboard'
          />
        )}
        isAvoidingView
        scrollable
        scrollBackgroundColor={WHITE}
        scrollContentContainerStyle={{ justifyContent: 'space-between' }}
      >
        {isReady ? <View style={{
          width: WP100,
          ...BORDER_STYLE['top'],
        }}
                   >
          <RNImage style={{ width: WP100, height: Dimensions.get('window').width / 2.46, resizeMode: 'cover' }} source={{ uri: image }} />
          <View style={{
            paddingVertical: HP2,
            width: WP100,
            paddingHorizontal: WP6 }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'flex-end', marginBottom: HP1 }}>
              <Text color={colors.grey50} size={'tiny'} style={{ marginRight: 10 }}>Post active duration</Text>
              <Text color={colors.grey80} weight={500} size={'tiny'}>{active_duration}</Text>
            </View>
            <Text style={{ marginBottom: HP3 }} weight={500} size={'slight'}>{title}</Text>
            {!isEmpty(list_user) ? <View>
              {list_user.map(({ profile_picture, full_name, id_user, city_name, job_title }) => {
                return (<View key={id_user} style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: HP1 }}>
                  <Image onPress={() => navigateTo('ProfileScreenNoTab', { idUser: id_user })} imageStyle={{ borderRadius: 30 }} size={'large'} source={{ uri: profile_picture }} />
                  <TouchableOpacity activeOpacity={0.5} onPress={() => navigateTo('ProfileScreenNoTab', { idUser: id_user })} style={{ paddingVertical: HP2, paddingHorizontal: HP3, flex: 1 }}>
                    <Text color={colors.grey80} weight={500} size={'mini'}>{full_name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                      <Text color={colors.grey50} size={'tiny'} style={{ marginRight: 5 }}>{job_title}</Text>
                      <Text color={colors.grey20} size={'tiny'} style={{ marginRight: 5 }}>•</Text>
                      <Text color={colors.grey50} size={'tiny'}>{city_name}</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity activeOpacity={0.5} onPress={this.openChatRoom.bind(this, id_user)} style={{ paddingVertical: HP2, paddingHorizontal: HP1 }}>
                    <Image size={'mini'} source={require('../../assets/icons/chatInsideCircle.png')} />
                  </TouchableOpacity>
                </View>)
              })}
            </View> : <View style={{ marginVertical: HP10 }}>
              <Text centered size={'slight'} color={GREY_WARM}>Still empty, no requests yet</Text>
            </View>}
          </View>
        </View> : null}
      </Container>
    )
  }

}

ProjectDashboard.defaultProps = propsDefault
export default _enhancedNavigation(connect(mapStateToProps, mapDispatchToProps)(ProjectDashboard), mapFromNavigationParam)
