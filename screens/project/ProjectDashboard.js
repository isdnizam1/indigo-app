import React from 'react'
import { connect } from 'react-redux'
import { TabBar, TabView } from 'react-native-tab-view'
import { Dimensions, Keyboard, TouchableOpacity, View } from 'react-native'
import { isEmpty, noop } from 'lodash-es'
import moment from 'moment'
import {
  _enhancedNavigation,
  Button,
  Container,
  CreateAddButton,
  HeaderNormal,
  Icon,
  Image,
  Text,
} from '../../components'
import {
  colors,
  GREEN,
  GREY_CALM_SEMI,
  GREY_WARM,
  ORANGE_BRIGHT,
  TEAL,
  TOMATO,
  WHITE,
} from '../../constants/Colors'
import { HP1, HP6, WP1, WP2, WP3, WP4, WP6, WP90 } from '../../constants/Sizes'
import {
  getCreatedProject,
  getJoinedProject,
  getVoucherUser,
} from '../../actions/api'
import Empty from '../../components/Empty'
import { getSpeaker } from '../../utils/helper'

const mapFromNavigationParam = (getParam) => ({})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const propsDefault = {}

class ProjectDashboard extends React.Component {
  constructor(props) {
    super(props)
  }

  state = {
    isLoading: true,
    isReady: false,
    isKeyboardOpen: false,
    joined: [],
    created: [],
    sumCreated: 0,
    createdDataFetched: false,
    tabState: {
      index: 0,
      routes: [
        { key: 'joined', title: 'JOINED' },
        { key: 'created', title: 'CREATED' },
      ],
    },
    vouchers: [],
  };

  async getJoinedProject() {
    const { userData, dispatch, navigateTo } = this.props
    try {
      const { result, code } = await dispatch(
        getJoinedProject,
        { id_user: userData.id_user },
        noop,
        true,
        true,
      )
      if (code == 200) {
        const joined = [
          {
            heading: 'Soundfren Connect',
            data: result
              .filter((ads) => ads.category == 'soundconnect')
              .map((ads) => {
                let additional_data
                if (ads.additional_data && typeof ads.additional_data !== 'object') {
                  additional_data = JSON.parse(ads.additional_data)
                }
                return { ...ads, additional_data }
              }),
            clickHandler: ({ id_ads }) =>
              navigateTo('SessionDetail', { id_ads, joined: true }),
          },
          {
            heading: 'Submission',
            data: result
              .filter((ads) => ads.category.indexOf('submission') >= 0)
              .map((ads) => {
                let additional_data
                if (ads.additional_data && typeof ads.additional_data !== 'object') {
                  additional_data = JSON.parse(ads.additional_data)
                }
                return { ...ads, additional_data }
              }),
            clickHandler: ({ id_ads }) =>
              navigateTo('AdsDetailScreenNoTab', { id_ads }),
          },
        ]

        this.setState({ joined })
      }
    } catch (err) {
      /*silent is gold*/
    }
  }

  async getCreatedProject() {
    const { userData, dispatch, navigateTo } = this.props
    try {
      const {
        result: { collaboration, submission, band },
        code,
      } = await dispatch(
        getCreatedProject,
        { id_user: userData.id_user },
        noop,
        true,
        true,
      )
      if (code == 200) {
        const created = [
          {
            heading: 'Collaboration',
            data: collaboration,
            clickHandler: ({ id_ads }) => navigateTo('AdRequests', { id_ads }),
          },
          {
            heading: 'Submission',
            data: submission,
            clickHandler: () =>
              navigateTo('BrowserScreenNoTab', {
                title: 'Soundfren Business',
                url: 'https://business.soundfren.id/',
              }),
          },
          {
            heading: 'Artist Spotlight',
            data: band,
            clickHandler: ({ id_ads }) =>
              navigateTo('AdsDetailScreenNoTab', { id_ads }),
          },
        ]
        const sumCreated = created
          .map((item) => item.data.length)
          .reduce((a, b) => a + b, 0)
        this.setState({ sumCreated, created })
      }
    } catch (err) {
      /*silent is gold*/
    } finally {
      this.setState({ createdDataFetched: true })
    }
  }

  _getVouchers = async () => {
    const { dispatch, userData } = this.props

    try {
      const { result, code } = await dispatch(
        getVoucherUser,
        { id_user: userData.id_user },
        noop,
        true,
        true,
      )
      if (code == 200) {
        this.setState({ vouchers: result })
      }
    } catch (error) {}
  };

  async componentDidMount() {
    await this.getJoinedProject()
    this._getVouchers()
    this.setState({
      isReady: true,
      isLoading: false,
    })
  }

  componentWillUnmount() {}

  render() {
    const { navigateBack, isLoading, navigateTo } = this.props
    const {
      isReady,
      tabState,
      // createdDataFetched,
      sumCreated,
      tabState: { index },
      vouchers,
    } = this.state
    return (
      <Container
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text='Activity Dashboard'
            rightComponent={
              <Button
                backgroundColor={TEAL}
                shadow='none'
                marginless
                textColor={WHITE}
                text={`My Voucher (${vouchers.length})`}
                onPress={() => navigateTo('VoucherListScreen')}
                contentStyle={{ paddingVertical: WP1, paddingHorizontal: WP3 }}
                rounded
                textSize='xmini'
              />
            }
          />
        )}
        isAvoidingView
        scrollable={index == 0 || sumCreated > 0}
        scrollBackgroundColor={WHITE}
        scrollContentContainerStyle={{ justifyContent: 'space-between' }}
      >
        <TabView
          navigationState={tabState}
          initialLayout={{ height: 0 }}
          swipeEnabled={false}
          renderScene={({ route }) => this.renderScene(route.key)}
          renderTabBar={(props) => (
            <TabBar
              {...props}
              style={[
                {
                  backgroundColor: WHITE,
                  elevation: 0,
                  // ...BORDER_STYLE['bottom']
                },
              ]}
              indicatorStyle={{ backgroundColor: ORANGE_BRIGHT }}
              renderLabel={({ route, focused }) => {
                return (
                  <Text
                    centered
                    size='xmini'
                    color={focused ? ORANGE_BRIGHT : GREY_WARM}
                    weight={500}
                  >
                    {route.title}
                  </Text>
                )
              }}
            />
          )}
          onIndexChange={this.onSwitchTab.bind(this)}
        />
      </Container>
    )
  }

  emptyScene(routeKey) {}

  renderScene(routeKey) {
    const { navigateTo, navigateBack } = this.props
    const {
      joined,
      created,
      sumCreated,
      isKeyboardOpen,
      createdDataFetched,
    } = this.state
    const { width, height } = Dimensions.get('window')
    if (routeKey === 'joined') {
      return isEmpty(joined) ? (
        <Empty
          style={{ justifyContent: 'flex-start' }}
          imageWidth={width}
          imageHeight={height}
          image={require('../../assets/images/joinedEmptyState.jpg')}
          hideTitle
          hideMessage
          actions={
            <View>
              <Text centered size='mini' style={{ marginBottom: 3, marginTop: 0 }}>
                You haven’t joined any project yet.
              </Text>
              <Text centered size='mini' weight={500}>
                Let’s find something awesome for you.
              </Text>
              <Button
                onPress={() => {
                  navigateBack()
                  navigateTo('ExploreScreen')
                }}
                style={{ width: WP90, marginTop: 30 }}
                backgroundColor={TOMATO}
                centered
                bottomButton
                radius={WP4}
                shadow='none'
                textType='NeoSans'
                textSize='small'
                textColor={WHITE}
                textWeight={500}
                text='Go to Explore'
              />
            </View>
          }
        />
      ) : (
        <View>
          {joined.map(({ data, heading, clickHandler }) => {
            return data.length > 0 ? (
              <View
                style={{
                  paddingHorizontal: WP6,
                  paddingTop: WP6,
                  paddingBottom: WP2,
                }}
                key={heading}
              >
                <Text size={'mini'} color={colors.grey50}>
                  {heading}
                </Text>
                {data.map((item) => {
                  const {
                    image,
                    title,
                    created_at,
                    status,
                    speaker,
                    submission_by,
                    additional_data,
                  } = item
                  const is_paid = status === 'confirm'
                  return (
                    <View key={Math.random()}>
                      <TouchableOpacity
                        onPress={clickHandler.bind(this, item)}
                        style={{ flexDirection: 'row', paddingVertical: WP3 }}
                        activeOpacity={0.5}
                      >
                        <Image
                          centered={false}
                          imageStyle={{ borderRadius: 12 }}
                          size={'huge'}
                          source={{ uri: image }}
                        />
                        <View style={{ flex: 1, paddingLeft: 20 }}>
                          <Text color={colors.grey80} size={'mini'} weight={500}>
                            {title}
                          </Text>
                          {heading === 'Soundfren Connect' ? (
                            <View>
                              <Text>
                                <Text color={GREY_WARM} size={'tiny'}>
                                  Connect with{' '}
                                </Text>
                                <Text
                                  style={{ marginLeft: 5 }}
                                  weight={500}
                                  size={'tiny'}
                                >
                                  {getSpeaker(speaker)}
                                </Text>
                              </Text>
                              {additional_data.date ? (
                                <Text size={'tiny'}>
                                  Schedule:{' '}
                                  {moment(additional_data.date.start)
                                    .format('ddd, LL | HH:mm')
                                    .replace('pukul', '|')}{' '}
                                  WIB
                                </Text>
                              ) : null}
                              {['confirm', 'pending'].indexOf(status) >= 0 ? (
                                <View
                                  style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    marginTop: HP1,
                                  }}
                                >
                                  <Icon
                                    color={is_paid ? GREEN : GREY_CALM_SEMI}
                                    style={{ marginRight: 5 }}
                                    type='MaterialIcons'
                                    name='check-circle'
                                  />
                                  <Text
                                    color={is_paid ? GREEN : GREY_CALM_SEMI}
                                    size={'tiny'}
                                  >
                                    {is_paid ? 'Payment Done' : 'Payment Process'}
                                  </Text>
                                </View>
                              ) : null}
                            </View>
                          ) : null}
                          {heading === 'Submission' ? (
                            <View>
                              <Text
                                style={{ marginVertical: 8 }}
                                size={'tiny'}
                                color={colors.grey80}
                              >
                                Submission by: {submission_by}
                              </Text>
                              <Text size={'tiny'} color={colors.grey50}>
                                Registered: {moment(created_at).format('LL')}
                              </Text>
                            </View>
                          ) : null}
                        </View>
                      </TouchableOpacity>
                    </View>
                  )
                })}
              </View>
            ) : null
          })}
        </View>
      )
    }
    if (routeKey === 'created') {
      return (
        <View style={{ paddingTop: sumCreated == 0 ? HP6 : 0 }}>
          {createdDataFetched && sumCreated == 0 ? (
            <Empty
              imageWidth={width / 1.6}
              imageHeight={width / 1.6}
              image={require('../../assets/images/empty_experience.png')}
              hideTitle
              hideMessage
              actions={
                <View>
                  <Text
                    centered
                    size='mini'
                    style={{ marginBottom: 3, marginTop: 0 }}
                  >
                    You don’t have any created project,
                  </Text>
                  <Text centered size='mini' weight={500}>
                    Let’s make new one.
                  </Text>
                  <View style={{ width: WP90 }}>
                    <CreateAddButton
                      showButtonsOnly
                      isKeyboardOpen={isKeyboardOpen}
                      navigateTo={navigateTo}
                      showButtonCreate={this._showButtonCreate.bind(this)}
                    />
                  </View>
                </View>
              }
            />
          ) : (
            <View>
              {created.map(({ data, heading, clickHandler }) => {
                return data.length > 0 ? (
                  <View
                    style={{
                      paddingHorizontal: WP6,
                      paddingTop: WP6,
                      paddingBottom: WP2,
                    }}
                    key={heading}
                  >
                    <Text size={'mini'} color={colors.grey50}>
                      {heading}
                    </Text>
                    {data.map((item) => {
                      const {
                        image,
                        title,
                        total_request,
                        total_viewers,
                        created_at,
                        status,
                      } = item
                      return (
                        <View key={Math.random()}>
                          <TouchableOpacity
                            onPress={clickHandler.bind(this, item)}
                            style={{ flexDirection: 'row', paddingVertical: WP3 }}
                            activeOpacity={0.5}
                          >
                            <Image
                              centered={false}
                              imageStyle={{ borderRadius: 12 }}
                              size={'massive'}
                              source={{ uri: image }}
                            />
                            <View style={{ flex: 1, paddingLeft: 20 }}>
                              <Text color={colors.grey80} size={'mini'} weight={500}>
                                {title}
                              </Text>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'flex-start',
                                  marginVertical: 8,
                                }}
                              >
                                <Image
                                  size={'tiny'}
                                  source={require('../../assets/icons/userCircle.png')}
                                />
                                <Text
                                  size={'tiny'}
                                  weight={500}
                                  color={colors.orangeBright}
                                  style={{ paddingLeft: 8, marginTop: 2.5 }}
                                >
                                  {typeof total_viewers === 'number'
                                    ? total_viewers
                                    : total_request}{' '}
                                  {heading === 'Artist Spotlight'
                                    ? 'viewers'
                                    : 'requests'}
                                </Text>
                              </View>
                              <Text size={'tiny'} color={colors.grey50}>
                                Created: {moment(created_at).format('LLL')}{' '}
                                {status !== 'active' ? `(${status})` : null}
                              </Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      )
                    })}
                  </View>
                ) : null
              })}
            </View>
          )}
        </View>
      )
    }
  }

  _showButtonCreate = () => {
    this.setState({
      isKeyboardOpen: false,
    })
    Keyboard.dismiss()
  };

  onSwitchTab(index) {
    const { tabState, createdDataFetched } = this.state
    this.setState({ tabState: { ...tabState, index } })
    !createdDataFetched && this.getCreatedProject()
  }
}

ProjectDashboard.defaultProps = propsDefault
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProjectDashboard),
  mapFromNavigationParam,
)
