import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { SHIP_GREY, SHIP_GREY_CALM } from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import InteractionManager from 'sf-utils/InteractionManager'
import { Container, _enhancedNavigation } from 'sf-components'
import HeaderNormal from 'sf-components/HeaderNormal'
import { SHADOW_GRADIENT } from 'sf-constants/Colors'
import { WP100, WP12, WP3 } from 'sf-constants/Sizes'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { noop } from 'lodash-es'
import YoutubePlayer from 'react-native-youtube-iframe'
import { Icon } from '../components'
import { WP50 } from '../constants/Sizes'
import { getAdsSFLearn } from '../actions/api'
import { getYoutubeId } from '../utils/helper'

const style = StyleSheet.create({
  headerShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
})

class YoutubePreviewScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      id_ads: null,
    }
    this._backHandler = this._backHandler.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
    this._changeScreenOrientation = this._changeScreenOrientation.bind(this)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._changeScreenOrientation)
    getAdsSFLearn({ id_user: this.props.userData.id_user }).then((result) => {
      result.data.result.video.map((video) => {
        const additional = JSON.parse(video.additional_data)
        const youtubeId = getYoutubeId(additional.url_video)
        if (youtubeId) {
          if (youtubeId === this.props.youtubeId) {
            if (!this.state.id_ads) {
              this.setState({
                id_ads: video.id_ads,
              })
            }
          }
        }
      })
    })
  }

  componentWillUnmount() {
    const { onBackPress = noop } = this.props
    onBackPress()
  }

  _backHandler = () => {
    this.props.navigateBack()
  };

  _renderHeader = () => {
    const { isStartup } = this.props
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftOnPress={this._backHandler}
          centered
          textType={'Circular'}
          iconLeft={'ios-close'}
          iconLeftSize={'large'}
          textSize={'slight'}
          iconLeftType={'Ionicons'}
          text={'Eventeer'}
          textColor={SHIP_GREY}
          iconLeftColor={SHIP_GREY_CALM}
          rightComponent={
            !isStartup && <TouchableOpacity
              onPress={() => {
                this.props.navigateTo('ShareScreen', {
                  id: this.state.id_ads,
                  type: 'explore',
                  title: 'Video',
                })
              }}
                          >
              <View style={{ width: WP12 }}>
                {this.state.id_ads && (
                  <Icon
                    type='MaterialCommunityIcons'
                    color={SHIP_GREY_CALM}
                    name='share-variant'
                  />
                )}
              </View>
            </TouchableOpacity>
          }
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
      </View>
    )
  };

  async _changeScreenOrientation() {
    // await ScreenOrientation.lockAsync(
    //   ScreenOrientation.OrientationLock.LANDSCAPE,
    // )
  }

  render() {
    return (
      <Container scrollable={false} renderHeader={this._renderHeader}>
        <View style={{ width: WP100, height: WP50 }}>
          <YoutubePlayer
            height={300}
            play={true}
            videoId={this.props.youtubeId}
            onChangeState={noop}
          />
        </View>
      </Container>
    )
    // return (
    //   <View style={{ flex: 1 }}>
    //     <View style={{ flex: 1 }}>

    //     </View>
    //     {/* <TouchableOpacity
    //       style={{
    //         position: 'absolute',
    //         top: 0,
    //         left: 0,
    //         backgroundColor: 'red',
    //         width: 400,
    //         height: 400,
    //       }}
    //     >
    //       <Icon type={'Ionicons'} name={'ios-arrow-back'} color={WHITE} />
    //     </TouchableOpacity> */}
    //   </View>
    // )
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  youtubeId: getParam('youtubeId', null),
  onBackPress: getParam('onBackPress', noop),
  isStartup: getParam('isStartup', false),
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(YoutubePreviewScreen),
  mapFromNavigationParam,
)
