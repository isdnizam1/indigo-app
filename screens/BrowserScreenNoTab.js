import React, { Component } from 'react'
import { connect } from 'react-redux'
import { noop } from 'lodash-es'
import _enhancedNavigation from '../navigation/_enhancedNavigation'
import BrowserScreen from './BrowserScreen'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  title: getParam('title', null),
  forceTitle: getParam('forceTitle', null),
  url: getParam('url', {}),
  onNavigationStateChange: getParam('onNavigationStateChange', noop),
  handleBack: getParam('handleBack', false),
  onLoadStart: getParam('onLoadStart', noop),
  onLoadEnd: getParam('onLoadEnd', noop),
})

class BrowserScreenNoTab extends Component {
  render() {
    return <BrowserScreen {...this.props} />
  }
}

BrowserScreenNoTab.propTypes = {}
BrowserScreenNoTab.defaultProps = {}
export default _enhancedNavigation(
  connect(mapStateToProps, {})(BrowserScreenNoTab),
  mapFromNavigationParam,
)
