import React from 'react'
import {
  Animated,
  TextInput,
  View,
  TouchableOpacity,
  Modal,
  Dimensions,
  Keyboard
} from 'react-native'
import { connect } from 'react-redux'
import includes from 'lodash-es/includes'
import { LinearGradient } from 'expo-linear-gradient'
import { Alert } from 'react-native'
import { Text, _enhancedNavigation, Container } from '../components'
import Dialog from '../components/Dialog'
import { GREY_CALM_SEMI, NAVY_DARK, REDDISH, SHADOW_GRADIENT, SHIP_GREY, TOMATO_CALM, WHITE } from '../constants/Colors'
import { WP4, WP5, HP5, WP2, WP1, FONT_SIZE, WP8 } from '../constants/Sizes'
import style from '../styles/register'
import { postCheckRecoveryCode, postForgotPassword } from '../actions/api'
import { isEmailValid } from '../utils/helper'
import { KEYBOARD_VISIBLE } from '../services/helper/actionTypes'
import HeaderNormal from '../components/HeaderNormal'
import { setLocalStorage } from '../utils/storage'
import { KEY_FORGOT_PASSWROD_EMAIL } from '../constants/Storage'
import ButtonV2 from '../components/ButtonV2'
import { HEADER } from '../constants/Styles'
import Spacer from '../components/Spacer'

const { height } = Dimensions.get('window')

const mapStateToProps = () => ({

})

const mapDispatchToProps = {
  keyboardVisible: (response) => ({
    type: KEYBOARD_VISIBLE,
    response
  })
}

class ForgotPasswordScreen extends React.Component {

  fadeIn = new Animated.Value(0);

  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
      errorMessage: null,
      showPassword: false,
      marginTop: 0,
      email: null,
      password: null,
      isLoading: false,
      isConfirmation: false,
      recoveryCode: ''
    }
    this.onChangeText = this.onChangeText.bind(this)
    this.toggleModal = this.toggleModal.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.onLayout = this.onLayout.bind(this)
    this.onSubmitCode = this.onSubmitCode.bind(this)
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this.props.keyboardVisible(true))
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.props.keyboardVisible(false))
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
    this.props.keyboardVisible(false)
  }

  onLayout({ nativeEvent }) {
    const viewHeight = nativeEvent.layout.height
    this.setState(
      {
        marginTop: height - viewHeight - viewHeight / 2,
      },
      () => {
        Animated.timing(this.fadeIn, {
          toValue: 1,
          duration: 50,
          useNativeDriver: false
        }).start()
      }
    )
  }

  toggleModal() {
    this.setState({ modalVisible: !this.state.modalVisible })
  }

  async onSubmitCode() {
    const { navigateTo } = this.props
    const { recoveryCode, email } = this.state
    if (!isEmailValid(email)) {
      this.setState({
        modalVisible: true,
        errorMessage: 'Incorrect email address',
        isConfirmation: false,
      })
    } else {
      this.setState(
        {
          isLoading: true,
        },
        () => {
          postCheckRecoveryCode({ email, forgotten_password_code: recoveryCode })
            .then(({ data }) => {
              const { code } = data
              if(code == 200) navigateTo('ResetPasswordScreen')
              if(code == 500) {
                this.setState({
                  modalVisible: true,
                  errorMessage: 'Incorrect recovery code',
                  isConfirmation: false,
                  recoveryCode: ''
                })
                this.recoveryInput.focus()
              }
            })
            .catch((e) => {})
            .then(() => this.setState({ isLoading: false }))
        }
      )
    }
  }

  onSubmit() {
    const { email } = this.state
    if (!isEmailValid(email)) {
      this.setState({
        modalVisible: true,
        errorMessage: 'Incorrect email address',
        isConfirmation: false,
      })
    } else {
      this.setState(
        {
          isLoading: true,
        },
        () => {
          postForgotPassword({ email })
            .then(({ data }) => {
              const { code, message } = data
              if (includes(message || '', 'not found')) {
                this.setState({
                  modalVisible: true,
                  errorMessage: 'Sorry, can’t find your account',
                  isConfirmation: true,
                })
              } else if (code == 200) {
                Promise.resolve(
                  setLocalStorage(KEY_FORGOT_PASSWROD_EMAIL, this.state.email))
                  .then(async () => {
                    this.setState({ emailSent: true })
                  })
              } else if (code === 500) {
                Alert.alert('Terjadi Kesalahan', message)
              }
            })
            .catch((e) => {})
            .then(() => this.setState({ isLoading: false }))
        }
      )
    }
  }

  onChangeText(value, callback) {
    Promise.all([this.setState({ errorMessage: null }), callback(value)])
  }

  render() {
    const { navigateBack, navigation, navigateTo } = this.props
    const {
      recoveryCode,
      modalVisible,
      errorMessage,
      email,
      emailSent,
      isLoading,
      isConfirmation,
    } = this.state
    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <View>
            <HeaderNormal
              style={style.headerNormal}
              iconLeftOnPress={navigateBack}
              textType='Circular'
              textColor={SHIP_GREY}
              textWeight={400}
              text={'Lupa Password'}
              centered
            />
            <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
          </View>)}
      >
        <View style={{ paddingHorizontal: WP5 }}>
          {emailSent && (
            <View style={{ alignItems: 'stretch' }}>
              <Text style={{ marginVertical: HP5 }} color={NAVY_DARK} weight={600} size='large' >Silakan cek email kamu</Text>
              <Text
                style={style.label}
                size={'mini'}
                weight={500}
                color={SHIP_GREY}
              >
                Input Recovery Code
              </Text>
              <View>

                <TouchableOpacity activeOpacity={1} onPress={() => this.recoveryInput.focus()}>
                  <View style={{ flexDirection: 'row', marginTop: WP2, marginHorizontal: -WP2 }}>
                    {
                      [...Array(6).keys()].map((n) => (<Text
                        key={n}
                        size={'large'}
                        color={recoveryCode[n] ? SHIP_GREY : WHITE}
                        weight={400}
                        centered
                        style={{
                          paddingVertical: (FONT_SIZE['large'] / 2) + WP1,
                          flex: 1,
                          marginHorizontal: WP1,
                          borderColor: GREY_CALM_SEMI,
                          borderWidth: 1,
                          borderRadius: WP1,
                        }}
                      >
                        {recoveryCode[n] || '-'}
                      </Text>))
                    }
                  </View>
                </TouchableOpacity>
                <TextInput
                  ref={(ref) => { this.recoveryInput = ref }}
                  value={recoveryCode}
                  selectionColor={WHITE}
                  onChangeText={(code) => {
                    if (code.length <= 6) this.setState({ recoveryCode: code.trim().replace(/\D/g, '') })
                  }}
                  keyboardType={'number-pad'}
                  style={{ ...style.input, opacity: 0, position: 'absolute', width: '100%' }}
                />
              </View>
              <Spacer size={WP8} />
              <ButtonV2
                style={{ paddingVertical: WP4 }}
                textSize={'slight'}
                text='Next'
                onPress={() => this.onSubmitCode()}
                disabled={recoveryCode.length !== 6 || isLoading}
                textColor={WHITE}
                color={REDDISH}
              />
            </View>
          )}
          {!emailSent && (
            <View style={{ alignItems: 'stretch' }}>
              <Text style={{ marginVertical: HP5 }} color={NAVY_DARK} weight={600} size='large' >Lupa Password?</Text>
              <Text
                style={style.label}
                size={'mini'}
                weight={500}
                color={SHIP_GREY}
              >
                Email
              </Text>
              <TextInput
                defaultValue={email}
                onChangeText={(email) => this.setState({ email })}
                keyboardType={'email-address'}
                autoCompleteType={'email'}
                placeholder={'Tuliskan email'}
                style={style.input}
              />
              <Spacer size={WP8} />
              <ButtonV2
                style={{ paddingVertical: WP4 }}
                textSize={'slight'}
                text='Send Recovery Email'
                onPress={() => this.onSubmit()}
                disabled={!isEmailValid(email)}
                textColor={WHITE}
                color={REDDISH}
              />
            </View>
          )}
        </View>
        <Modal
          transparent
          onRequestClose={this.toggleModal}
          visible={modalVisible}
        >
          <Dialog
            width={250}
            isConfirmation={isConfirmation}
            height={isConfirmation ? 128 : 120}
            cancelText={'Sign Up'}
            confirmText={'Try Again'}
            onCancel={() =>
              this.setState({ modalVisible: false }, () => {
                Promise.resolve(navigation.popToTop()).then(() => {
                  navigateTo('RegisterV3')
                })
              })
            }
            onConfirm={this.toggleModal}
          >
            <Text
              centered
              style={{ marginTop: isConfirmation ? 0 : WP4 }}
              size={'xmini'}
            >
              {errorMessage}
            </Text>
            {!isConfirmation && (
              <TouchableOpacity
                style={{ paddingVertical: WP4 }}
                onPress={this.toggleModal}
                activeOpacity={0.8}
              >
                <Text
                  centered
                  color={TOMATO_CALM}
                  weight={500}
                  type={'NeoSans'}
                  size={'xmini'}
                >
                  Try Again
                </Text>
              </TouchableOpacity>
            )}
          </Dialog>
        </Modal>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordScreen),
  null
)
