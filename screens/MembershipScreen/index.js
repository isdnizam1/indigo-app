/* eslint-disable react/sort-comp */
import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import {
  SHIP_GREY,
  SHIP_GREY_CALM,
  SHADOW_GRADIENT,
  PALE_BLUE,
  PALE_BLUE_TWO,
  PALE_GREY_THREE,
  PALE_WHITE,
  WHITE,
} from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import {
  WP100,
  WP1,
  WP2,
  WP205,
  WP3,
  WP4,
  WP12,
  WP6,
  WP5,
  WP50,
} from 'sf-constants/Sizes'
import InteractionManager from 'sf-utils/InteractionManager'
import moment from 'moment'
import { Text, _enhancedNavigation, Container, HeaderNormal } from 'sf-components'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { get, isEmpty, isNil } from 'lodash-es'
import Spacer from 'sf-components/Spacer'
import { FlatList } from 'react-native-gesture-handler'
import Icon from 'sf-components/Icon'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import { noop } from 'lodash-es/noop'
import SkeletonContent from 'react-native-skeleton-content'
import { getMembershipScreen, postLogPremium } from '../../actions/api'
import { isIOS, osText, restrictedAction } from '../../utils/helper'
import {
  GUN_METAL,
  REDDISH,
  GREEN_30,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  TRANSPARENT,
} from '../../constants/Colors'
import { paymentDispatcher } from '../../services/payment'
import { WP10, WP15 } from '../../constants/Sizes'

const style = StyleSheet.create({
  headerShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
})

class MembershipScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: {
        benefit: [null, null, null, null],
        package: [null, null, null, null],
      },
      activeBenefitIndex: 0,
    }
    this._renderHeader = this._renderHeader.bind(this)
    this._backHandler = this._backHandler.bind(this)
  }

  componentDidMount = () => {
    InteractionManager.runAfterInteractions(this._getData)
  };

  _sendLogPremium() {
    const {
      userData,
      dispatch,
      paymentSource,
      idAds,
      paymentSourceSet,
    } = this.props
    if (!isEmpty(paymentSource) && !isNil(paymentSource)) {
      paymentSourceSet(paymentSource)
      dispatch(
        postLogPremium,
        { id_user: userData.id_user, previous_screen: paymentSource, id_ads: idAds },
        noop,
        true,
        true,
      ).then((result) => {})
    } else {
      bugsnagClient.notify(
        new Error('Untracked paymentSource (this warning doesn\'t cause app crash)'),
      )
    }
  }

  _getData = async () => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props
    const data = await dispatch(getMembershipScreen, {
      id_user,
    })
    this.setState({ data })
    this._sendLogPremium()
  };

  get _pagination() {
    const { data, activeBenefitIndex } = this.state
    return (
      <View style={{ paddingVertical: WP4, paddingLeft: WP1 * 1.5 }}>
        <Pagination
          dotsLength={data.benefit.length}
          activeDotIndex={activeBenefitIndex}
          containerStyle={{
            justifyContent: 'flex-start',
            paddingVertical: 0,
            paddingLeft: WP4 + 1,
            paddingRight: 0,
          }}
          dotContainerStyle={{
            marginLeft: 0,
            marginRight: WP1,
          }}
          inactiveDotScale={1}
          dotStyle={{
            width: WP5,
            height: WP2,
            borderRadius: WP2 / 2,
            backgroundColor: REDDISH,
            marginLeft: 0,
            marginRight: 0,
          }}
          inactiveDotStyle={{
            width: WP2,
            height: WP2,
            borderRadius: WP2 / 2,
            backgroundColor: PALE_BLUE_TWO,
            marginLeft: 0,
            marginRight: 0,
          }}
          inactiveDotOpacity={1}
        />
      </View>
    )
  }

  get _currentPackage() {
    const {
      data: { package_type, valid_until },
    } = this.state
    return (
      <SkeletonContent
        containerStyle={{ flexGrow: 1, flex: 1, flexDirection: 'row' }}
        isLoading={!package_type}
        boneColor={SKELETON_COLOR}
        highlightColor={SKELETON_HIGHLIGHT}
      >
        {!package_type ? (
          <View
            style={{ height: WP15 - WP2, flex: 1, borderRadius: WP2, elevation: 3 }}
          />
        ) : (
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: WP4,
              backgroundColor: !package_type ? TRANSPARENT : WHITE,
              borderRadius: WP2,
              elevation: !package_type ? 0 : 3,
            }}
          >
            <Text type={'Circular'} weight={'700'} color={GUN_METAL}>
              {package_type}
            </Text>
            <Text type={'Circular'} size={'mini'} color={SHIP_GREY_CALM}>
              {valid_until
                ? `s/d ${moment(valid_until).locale('id').format('LL')}`
                : 'Paket kamu saat ini'}
            </Text>
          </View>
        )}
      </SkeletonContent>
    )
  }

  _renderBenefit = ({ item, index }) => {
    return (
      <View
        style={{
          width: WP50 * 1.1 + WP3,
          height: (WP50 * 1.1 * 102) / 180,
        }}
      >
        <SkeletonContent
          containerStyle={{
            width: WP50 * 1.1,
            height: (WP50 * 1.1 * 102) / 180,
          }}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
          isLoading={!item}
        >
          <Image
            style={{
              width: WP50 * 1.1,
              height: (WP50 * 1.1 * 102) / 180,
            }}
            source={{ uri: item?.image }}
          />
        </SkeletonContent>
      </View>
    )
  };

  _renderPackage = ({ item, index }) => {
    const { navigateTo, userData } = this.props
    const Wrapper = isIOS() ? View : TouchableOpacity
    const wrapperProps =
      isIOS() || !item
        ? {}
        : {
            onPress: restrictedAction({
              action: () =>
                navigateTo('MembershipDurationScreen', {
                  package: item.package_name,
                  package_count: index == 0 ? 1 : 3,
                }),
              userData,
              navigateTo,
            }),
            activeOpacity: TOUCH_OPACITY,
          }
    return (
      <View
        style={{
          paddingHorizontal: WP2,
          paddingTop: WP1,
          paddingBottom: WP2,
        }}
      >
        <SkeletonContent
          containerStyle={{ flexGrow: 1, flex: 1 }}
          isLoading={!item}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          {!item ? (
            <View
              style={{
                flex: 1,
                height: WP6 * 5,
                width: WP100 - WP6 * 2,
                elevation: 3,
                borderRadius: WP2,
              }}
            />
          ) : (
            <Wrapper {...wrapperProps}>
              <View
                style={{
                  backgroundColor: WHITE,
                  padding: WP5,
                  paddingEnd: WP4,
                  borderRadius: WP2,
                  elevation: 3,
                  flexDirection: 'row',
                  alignItems: 'center',
                  flex: 1,
                }}
              >
                <View style={{ flex: 1 }}>
                  <Spacer size={WP1 / 2} />
                  <Text
                    type={'Circular'}
                    size={'small'}
                    weight={500}
                    color={GUN_METAL}
                  >
                    {item.package_name.toUpperCase()}
                  </Text>
                  <Spacer size={WP2} />
                  {item.benefit.map((benefit) => (
                    <View
                      key={benefit}
                      style={{ flexDirection: 'row', paddingTop: WP1 }}
                    >
                      <View>
                        <Image
                          style={{ width: WP4, height: WP4 }}
                          source={require('sf-assets/icons/reddishCircledTick.png')}
                        />
                      </View>
                      <View style={{ flex: 1, paddingLeft: WP2 + WP1 }}>
                        <Text
                          type={'Circular'}
                          size={'mini'}
                          weight={300}
                          color={SHIP_GREY_CALM}
                        >
                          {benefit}
                        </Text>
                      </View>
                    </View>
                  ))}
                  <Spacer size={WP1} />
                </View>
                {!isIOS() && (
                  <View>
                    <Icon
                      size={'large'}
                      color={REDDISH}
                      type={'MaterialCommunityIcons'}
                      name={'chevron-right'}
                    />
                  </View>
                )}
                {item.is_best_deal && !isIOS() && (
                  <View
                    style={{
                      backgroundColor: GREEN_30,
                      paddingVertical: WP1,
                      paddingHorizontal: WP2 + WP1,
                      position: 'absolute',
                      right: 0,
                      top: 0,
                      borderTopRightRadius: WP2,
                      borderBottomLeftRadius: WP2,
                    }}
                  >
                    <Text type={'Circular'} size={'tiny'} weight={500} color={WHITE}>
                      BEST DEAL
                    </Text>
                  </View>
                )}
              </View>
            </Wrapper>
          )}
        </SkeletonContent>
      </View>
    )
  };

  render() {
    const { data } = this.state
    return (
      <Container
        scrollBackgroundColor={PALE_GREY_THREE}
        scrollable
        renderHeader={this._renderHeader}
      >
        <View style={{ backgroundColor: PALE_WHITE }}>
          <View
            style={{
              paddingHorizontal: WP6,
              paddingTop: WP6,
              paddingBottom: WP4,
            }}
          >
            <Text type={'Circular'} weight={'700'} size={'medium'} color={SHIP_GREY}>
              {osText('Benefits of Premium', 'Benefits')}
            </Text>
          </View>
          <View>
            <Carousel
              containerCustomStyle={{ paddingLeft: WP6 }}
              sliderWidth={WP100}
              itemWidth={WP50 * 1.1 + WP3}
              activeSlideAlignment='start'
              inactiveSlideScale={1}
              data={get(data, 'benefit')}
              renderItem={this._renderBenefit}
              onSnapToItem={(index) => this.setState({ activeBenefitIndex: index })}
            />
            {this._pagination}
            <View style={{ padding: WP6, marginBottom: WP4 }}>
              {this._currentPackage}
            </View>
          </View>
        </View>
        <Spacer color={PALE_BLUE} size={1} />
        <View style={{ backgroundColor: PALE_GREY_THREE, padding: WP4 }}>
          <View
            style={{
              paddingHorizontal: WP2,
              marginVertical: WP2,
              paddingBottom: WP1,
            }}
          >
            <Text type={'Circular'} weight={'700'} size={'medium'} color={SHIP_GREY}>
              Pilihan Paket
            </Text>
          </View>
          <FlatList
            keyExtractor={(item) => (!item ? Math.random() : item.package_name)}
            renderItem={this._renderPackage}
            data={get(data, 'package')}
          />
        </View>
      </Container>
    )
  }

  _backHandler = () => {
    this.props.navigateBack()
  };

  _renderHeader = () => {
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftOnPress={this._backHandler}
          centered
          textType={'Circular'}
          textSize={'slight'}
          text={osText('Premium Membership', 'Information')}
          textColor={SHIP_GREY}
          iconLeftColor={SHIP_GREY_CALM}
          rightComponent={<View style={{ width: WP12 }} />}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
      </View>
    )
  };
}

const mapFromNavigationParam = (getParam) => ({})

export default _enhancedNavigation(
  connect(
    ({ auth: { user: userData }, payment }) => ({
      userData,
      paymentSource: payment.paymentSource,
      idAds: payment.idAds,
    }),
    {
      paymentSourceSet: paymentDispatcher.paymentSourceSet,
    },
  )(MembershipScreen),
  mapFromNavigationParam,
)
