import React from 'react'
import { View, StyleSheet, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import { SHIP_GREY, SHIP_GREY_CALM } from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import InteractionManager from 'sf-utils/InteractionManager'
import { Text } from 'sf-components'
import { WP6 } from 'sf-constants/Sizes'
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native-gesture-handler'
import { get, isNil } from 'lodash-es'
import moment from 'moment'
import { TextInput } from 'react-native'
import { ActivityIndicator } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { Container, Icon, _enhancedNavigation } from '../../components'
import HeaderNormal from '../../components/HeaderNormal'
import {
  GREEN_30,
  GUN_METAL,
  PALE_BLUE_TWO,
  PALE_GREY_THREE,
  PALE_WHITE,
  REDDISH,
  RED_20,
  SHADOW_GRADIENT,
  SILVER_TWO,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  TRANSPARENT,
  WHITE,
} from '../../constants/Colors'
import {
  WP1,
  WP100,
  WP10,
  WP12,
  WP2,
  WP3,
  WP20,
  WP4,
  WP5,
  WP25,
} from '../../constants/Sizes'
import {
  getMembershipDetail,
  getSingleVoucherDetail,
  postMembershipPlan,
} from '../../actions/api'
import Spacer from '../../components/Spacer'
import { TOUCH_OPACITY } from '../../constants/Styles'
import ButtonV2 from '../../components/ButtonV2'
import ConfirmationModalV3 from '../../components/ConfirmationModalV3'

const style = StyleSheet.create({
  headerShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
})

class MembershipDurationScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      package: Array(props.package_count).fill({ duration: 0 }),
      checkingVoucher: false,
      voucher_code: '',
      token_voucher: null,
      voucher_status: null,
      discount_amount: 0,
      invalidVoucherDuration: false,
      invalidVoucherCode: false,
    }
    this._backHandler = this._backHandler.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._getData)
  }

  _getData = () => {
    this.props
      .dispatch(getMembershipDetail, {
        id_user: this.props.userData.id_user,
        package_name: this.props.package,
      })
      .then((result) => {
        this.setState({
          ...result,
          duration: get(
            result.package.filter((is_best_deal) => is_best_deal),
            '0',
          )?.duration,
        })
      })
  };

  _backHandler = () => {
    this.props.navigateBack()
  };

  _renderHeader = () => {
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftOnPress={this._backHandler}
          centered
          textSize={'slight'}
          text={'Premium Membership'}
          rightComponent={<View style={{ width: WP12 }} />}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
      </View>
    )
  };

  _renderPackage = ({ item, index }) => {
    const { duration } = this.state
    const isSelected = !item.normal_price ? null : item.duration === duration
    return (
      <View
        style={{
          paddingHorizontal: WP4,
          paddingTop: WP1,
        }}
      >
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          onPress={() =>
            this.setState({ duration: item.duration || 0 }, this._useVoucher)
          }
        >
          <SkeletonContent
            containerStyle={{ flexGrow: 1, flex: 1 }}
            isLoading={!item.normal_price}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            {!item.normal_price ? (
              <View
                style={{
                  height: WP20 + WP5,
                  marginBottom: WP2,
                  marginHorizontal: WP2,
                  marginTop: WP1 / 2,
                  flex: 1,
                  width: WP100 - WP6 * 2,
                  elevation: 3,
                  borderRadius: WP2,
                }}
              />
            ) : (
              <View
                style={{
                  backgroundColor: WHITE,
                  padding: WP5,
                  paddingEnd: WP4,
                  borderRadius: WP2,
                  marginBottom: WP2,
                  marginHorizontal: WP2,
                  marginTop: WP1 / 2,
                  elevation: 3,
                  flexDirection: 'row',
                  alignItems: 'center',
                  height: WP20 + WP5,
                }}
              >
                <View>
                  <Icon
                    size={'huge'}
                    type={'MaterialIcons'}
                    color={isSelected ? REDDISH : SHIP_GREY_CALM}
                    name={isSelected ? 'radio-button-on' : 'radio-button-off'}
                  />
                </View>
                <View style={{ flex: 1, paddingLeft: WP4 }}>
                  <Text
                    type={'Circular'}
                    size={'small'}
                    weight={500}
                    color={GUN_METAL}
                  >
                    {item.label}
                  </Text>
                  <Text
                    type={'Circular'}
                    size={'small'}
                    weight={400}
                    color={isSelected ? REDDISH : SHIP_GREY_CALM}
                  >
                    {this.state.currency}
                    {item.discount_price
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                  </Text>
                  {!!item.discount && (
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}
                    >
                      <Text
                        type={'Circular'}
                        size={'mini'}
                        weight={400}
                        color={SHIP_GREY_CALM}
                        style={{ textDecorationLine: 'line-through' }}
                      >
                        {this.state.currency}
                        {item.normal_price
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
                      </Text>
                      <Text
                        type={'Circular'}
                        size={'slight'}
                        weight={400}
                        color={SHIP_GREY_CALM}
                      >
                        {' · '}
                      </Text>
                      <Text
                        type={'Circular'}
                        size={'mini'}
                        weight={400}
                        color={GREEN_30}
                      >
                        Hemat {item.discount}
                      </Text>
                    </View>
                  )}
                </View>
                {item.is_best_deal && (
                  <View
                    style={{
                      backgroundColor: GREEN_30,
                      paddingVertical: WP1,
                      paddingHorizontal: WP2 + WP1,
                      position: 'absolute',
                      right: 0,
                      top: 0,
                      borderTopRightRadius: WP2,
                      borderBottomLeftRadius: WP2,
                    }}
                  >
                    <Text type={'Circular'} size={'tiny'} weight={500} color={WHITE}>
                      BEST DEAL
                    </Text>
                  </View>
                )}
              </View>
            )}
          </SkeletonContent>
        </TouchableOpacity>
      </View>
    )
  };

  _useVoucher = () => {
    !!this.state.voucher_code &&
      this.setState({ checkingVoucher: true }, () => {
        getSingleVoucherDetail({
          type: 'premium_user',
          id_user: this.props.userData.id_user,
          voucher_code: this.state.voucher_code,
          premium_type: this.props.package,
          package_duration: this.state.duration,
        })
          .then(({ data: voucherResult }) => {
            this.setState({
              voucher_status: get(voucherResult, 'status') === 'success',
              token_voucher: get(voucherResult, 'result.token_voucher'),
              discount_amount: get(voucherResult, 'result.amount_discount') || 0,
              invalidVoucherDuration:
                (get(voucherResult, 'message') || '').replace(/\s+/g, ' ') ===
                'Voucher cannot be used',
              invalidVoucherCode:
                (get(voucherResult, 'message') || '').replace(/\s+/g, ' ') ===
                'Voucher Code is not found',
            })
          })
          .catch((error) => {})
          .then(() => {
            this.setState({ checkingVoucher: false })
            Keyboard.dismiss()
          })
      })
  };

  _onRequestPay = () => {
    postMembershipPlan({
      duration: this.state.duration,
      package_type: this.props.package,
      token_voucher: this.state.token_voucher,
      id_user: this.props.userData.id_user,
      payment_channel: null,
    })
      .then(({ data: result }) => {
        this.props.navigation.popToTop()
        this.props.navigateTo('ProfileScreen')
      })
      .catch((error) => {})
      .then(() => {})
  };

  _onNext = () => {
    const { discount_amount, token_voucher, duration } = this.state
    if (discount_amount == 100) {
      this._onRequestPay()
    } else {
      this.props.navigateTo('PaymentMethodScreen', {
        payment_type: 'membership',
        body: {
          package_type: this.props.package,
          token_voucher,
          duration,
        },
      })
    }
  };

  render() {
    const { package: packages, voucher_status, discount_amount } = this.state
    const finalPrice = (
      parseInt(
        get(
          packages.filter((item) => item.duration == this.state.duration),
          '0.discount_price',
        ) || '0',
      ) -
      (parseInt(
        get(
          packages.filter((item) => item.duration == this.state.duration),
          '0.discount_price',
        ) || '0',
      ) *
        discount_amount) /
        100
    )
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, '.')

    const normalPrice = parseInt(
      get(
        packages.filter((item) => item.duration == this.state.duration),
        '0.discount_price',
      ) || '0',
    )
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    return (
      <Container scrollable={false} renderHeader={this._renderHeader}>
        <ScrollView
          keyboardShouldPersistTaps={'always'}
          style={{
            backgroundColor: PALE_GREY_THREE,
          }}
        >
          <View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                padding: WP6,
                paddingBottom: WP3,
              }}
            >
              <Text type={'Circular'} color={SHIP_GREY_CALM} size={'slight'}>
                Pilihan Membership:{' '}
              </Text>
              <Text type={'Circular'} color={REDDISH} size={'slight'} weight={500}>
                {this.props.package.toUpperCase()}
              </Text>
            </View>
            <FlatList
              keyExtractor={(item) =>
                !item.normal_price ? Math.random() : item.duration
              }
              renderItem={this._renderPackage}
              data={packages}
            />
            <Spacer size={WP4 + WP2} />
          </View>
          <View style={{ padding: WP6, backgroundColor: PALE_WHITE }}>
            {discount_amount > 0 && (
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}
                >
                  <Text type={'Circular'} color={SHIP_GREY_CALM} size={'mini'}>
                    Voucher (diskon {discount_amount}%)
                  </Text>
                  <View style={{ flex: 1 }} />
                  <Text
                    type={'Circular'}
                    color={PALE_BLUE_TWO}
                    size={'mini'}
                    style={{ textDecorationLine: 'line-through' }}
                  >
                    {this.state.currency}
                    {normalPrice}
                  </Text>
                  <Text type={'Circular'} color={SHIP_GREY_CALM} size={'mini'}>
                    {' '}
                    {this.state.currency}
                    {finalPrice}
                  </Text>
                </View>
                <Spacer size={WP3} />
                <Spacer size={1} color={PALE_GREY_THREE} />
                <Spacer size={WP5} />
              </View>
            )}
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Text type={'Circular'} color={SHIP_GREY} size={'small'} weight={600}>
                Total pembayaran
              </Text>
              <Text type={'Circular'} color={REDDISH} size={'large'} weight={600}>
                {this.state.currency}
                {finalPrice}
              </Text>
            </View>
            <Spacer size={WP5} />
            <Spacer size={1} color={PALE_GREY_THREE} />
            <Spacer size={WP2} />
            <Text
              type={'Circular'}
              color={SHIP_GREY_CALM}
              size={'xmini'}
              weight={300}
            >
              {`Paket ${this.props.package} kamu akan aktif s/d ${moment()
                .locale('id')
                .add(parseInt((this.state.duration || '0').split(' ')[0]), 'months')
                .format('LL')}`}
            </Text>
            <Spacer size={WP2} />
            <Spacer size={1} color={PALE_GREY_THREE} />
            <Spacer size={WP2} />
            <Spacer size={WP5} />
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text type={'Circular'} color={SHIP_GREY} size={'slight'} weight={500}>
                Kode Voucher
              </Text>
              <Text
                type={'Circular'}
                color={PALE_BLUE_TWO}
                size={'slight'}
                weight={400}
              >
                {' '}
                (opsional)
              </Text>
            </View>
            <Spacer size={WP2} />
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'flex-start',
              }}
            >
              <View style={{ flex: 1, paddingRight: WP3 }}>
                <TextInput
                  autoCapitalize='characters'
                  onChangeText={(voucher_code) =>
                    this.setState({
                      voucher_code,
                      voucher_status: null,
                      token_voucher: null,
                      discount_amount: 0,
                    })
                  }
                  value={this.state.voucher_code}
                  placeholder={'Tulis kode vouchermu'}
                  placeholderTextColor={SILVER_TWO}
                  style={{
                    color: SHIP_GREY,
                    borderWidth: 1.25,
                    borderColor: !isNil(voucher_status)
                      ? voucher_status
                        ? GREEN_30
                        : RED_20
                      : PALE_BLUE_TWO,
                    backgroundColor: !isNil(voucher_status)
                      ? voucher_status
                        ? 'rgb(235,245,233)'
                        : 'rgb(255,219,232)'
                      : TRANSPARENT,
                    height: WP12,
                    borderRadius: WP2,
                    paddingLeft: WP3,
                    paddingRight: WP2,
                  }}
                />
                {!isNil(voucher_status) && (
                  <View>
                    <Spacer size={WP2} />
                    {!voucher_status && (
                      <Text type={'Circular'} color={RED_20} size={'mini'}>
                        Kode voucher tidak valid
                      </Text>
                    )}
                    {!!voucher_status && (
                      <Text type={'Circular'} color={GREEN_30} size={'mini'}>
                        Voucher kamu telah aktif
                      </Text>
                    )}
                  </View>
                )}
              </View>
              <View>
                <ButtonV2
                  onPress={this._useVoucher}
                  style={{
                    height: WP12,
                    paddingHorizontal: WP5,
                    width: WP25,
                  }}
                  textColor={REDDISH}
                  textSize={'slight'}
                  borderColor={REDDISH}
                  borderWidth={1.25}
                  text={this.state.checkingVoucher ? null : 'Pakai'}
                >
                  {this.state.checkingVoucher && (
                    <ActivityIndicator color={REDDISH} />
                  )}
                </ButtonV2>
              </View>
            </View>
            <Spacer size={WP10} />
          </View>
        </ScrollView>
        <LinearGradient
          colors={SHADOW_GRADIENT}
          style={[style.headerShadow, { transform: [{ rotate: '180deg' }] }]}
        />
        <View style={{ padding: WP5 }}>
          <ButtonV2
            disabled={this.state.checkingVoucher}
            style={{ height: WP12, paddingHorizontal: WP5 }}
            textColor={WHITE}
            color={REDDISH}
            textSize={'slight'}
            borderColor={REDDISH}
            borderWidth={1.25}
            onPress={this._onNext}
            text={
              discount_amount == 100 ? 'Daftar Premium' : 'Pilih Metode Pembayaran'
            }
          />
        </View>
        <ConfirmationModalV3
          visible={
            this.state.invalidVoucherDuration || this.state.invalidVoucherCode
          }
          title={
            this.state.invalidVoucherDuration
              ? 'Voucher tidak dapat digunakan'
              : 'Kode voucher tidak valid'
          }
          subtitle={
            this.state.invalidVoucherDuration
              ? 'Kode voucher yang kamu pakai tidak dapat digunakan pada pembelian ini.'
              : 'Kode voucher yang kamu masukan tidak valid'
          }
          primary={{
            text: this.state.invalidVoucherDuration ? 'Ubah Pembelian' : 'Ulangi',
            onPress: () => {
              this.setState({
                invalidVoucherDuration: false,
                invalidVoucherCode: false,
                token_voucher: null,
                voucher_code: '',
                voucher_status: null,
              })
            },
          }}
        />
      </Container>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  package: getParam('package', null),
  package_count: getParam('package_count', null),
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(MembershipDurationScreen),
  mapFromNavigationParam,
)
