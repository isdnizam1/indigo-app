import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { SHIP_GREY } from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import InteractionManager from 'sf-utils/InteractionManager'
import { get } from 'lodash-es'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import { Image } from 'react-native'
import { WP20 } from 'sf-constants/Sizes'
import { Container, Icon, _enhancedNavigation } from '../../components'
import HeaderNormal from '../../components/HeaderNormal'
import Text from '../../components/Text'
import {
  PALE_GREY_THREE,
  WHITE,
  REDDISH,
  SHADOW_GRADIENT,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
} from '../../constants/Colors'
import { WP100, WP12, WP3, WP2, WP4, WP15 } from '../../constants/Sizes'
import {
  getSettingDetail,
  postMembershipPlan,
  postJoinSession,
} from '../../actions/api'
import Spacer from '../../components/Spacer'
import { TOUCH_OPACITY } from '../../constants/Styles'
import SkeletonContent from '../../local_modules/react-native-skeleton-content/lib/index.d'

const style = StyleSheet.create({
  headerShadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
})

class PaymentMethodScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      channels: {
        gopay: Array(1).fill({}),
        bank_transfer: Array(4).fill({}),
      },
    }
    this._backHandler = this._backHandler.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
    this._getData = this._getData.bind(this)
  }

  _getData = () => {};

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._getData)
    getSettingDetail({
      setting_name: 'payment',
      key_name: 'payment_channel',
    }).then(({ data }) => {
      const channels = JSON.parse(get(data, 'result.value'))
      this.setState({
        channels: {
          gopay: get(channels, 'gopay'),
          bank_transfer: get(channels, 'bank_transfer'),
        },
      })
    })
  }

  _backHandler = () => {
    this.props.navigateBack()
  };

  _onRequestPay = (payment_channel) => {
    const { body, payment_type } = this.props
    if (payment_type == 'membership') {
      postMembershipPlan({
        ...body,
        id_user: this.props.userData.id_user,
        payment_channel,
      })
        .then(({ data: result }) => {
          this.props.navigation.popToTop()
          this.props.navigateTo('PaymentHistory')
          this.props.navigateTo('PaymentWebView', {
            url: get(result, 'result.redirect_payment'),
          })
        })
        .catch((error) => {})
        .then(() => {})
    } else if (payment_type == 'join_session') {
      postJoinSession({
        ...body,
        payment_channel,
      })
        .then(({ data: result }) => {
          this.props.navigation.popToTop()
          this.props.navigateTo('PaymentHistory')
          this.props.navigateTo('PaymentWebView', {
            url: get(result, 'result.redirect_payment'),
          })
        })
        .catch((error) => {})
        .then(() => {})
    }
  };

  _renderHeader = () => {
    return (
      <View>
        <HeaderNormal
          withExtraPadding
          iconLeftOnPress={this._backHandler}
          centered
          textSize={'slight'}
          text={'Metode Pembayaran'}
          rightComponent={<View style={{ width: WP12 }} />}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={style.headerShadow} />
      </View>
    )
  };

  _section = (channels, index) => {
    return (
      <View key={index}>
        <Spacer size={WP2} />
        <View style={{ padding: WP4 }}>
          <View style={{ paddingHorizontal: WP2 }}>
            <Text type={'Circular'} weight={500} color={SHIP_GREY} size={'small'}>
              {index == 0 ? 'Dompet Digital' : 'ATM/Transfer Bank'}
            </Text>
          </View>
          <Spacer size={WP3} />
          <FlatList
            data={channels}
            keyExtractor={(item) => item.payment_channel || Math.random()}
            renderItem={({ item: channel }) => {
              const isLoading = !channel.payment_channel
              return (
                <SkeletonContent
                  containerStyle={{ flexGrow: 1, flex: 1 }}
                  isLoading={isLoading}
                  boneColor={SKELETON_COLOR}
                  highlightColor={SKELETON_HIGHLIGHT}
                >
                  {isLoading ? (
                    <View
                      style={{
                        width: WP100 - WP2 * 6,
                        height: WP20,
                        borderRadius: WP2,
                        margin: WP2,
                        elevation: 3,
                      }}
                    />
                  ) : (
                    <TouchableOpacity
                      activeOpacity={TOUCH_OPACITY}
                      onPress={() => this._onRequestPay(channel.payment_channel)}
                    >
                      <View style={{ padding: WP2 }}>
                        <View
                          style={{
                            backgroundColor: WHITE,
                            elevation: 3,
                            borderRadius: WP2,
                          }}
                        >
                          <View
                            style={{
                              flexDirection: 'row',
                              padding: WP4,
                              alignItems: 'center',
                              justifyContent: 'space-between',
                            }}
                          >
                            <View style={{ paddingRight: WP4 }}>
                              <Image
                                style={{
                                  width: WP15,
                                  height: WP12,
                                  resizeMode: 'cover',
                                }}
                                source={{
                                  uri: channel.logo,
                                }}
                              />
                            </View>
                            <Text
                              type={'Circular'}
                              weight={400}
                              color={SHIP_GREY}
                              size={'small'}
                            >
                              {channel.bank
                                ? channel.bank
                                : (channel.payment_channel || '').toUpperCase()}
                            </Text>
                            <View style={{ flex: 1 }} />
                            <View>
                              <Icon
                                type={'MaterialCommunityIcons'}
                                name={'chevron-right'}
                                size={'large'}
                                color={REDDISH}
                              />
                            </View>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  )}
                </SkeletonContent>
              )
            }}
          />
        </View>
        <Spacer size={WP2} />
        {index == 0 && <Spacer size={WP3} color={PALE_GREY_THREE} />}
      </View>
    )
  };

  render() {
    const {
      channels: { gopay, bank_transfer },
    } = this.state
    return (
      <Container scrollable renderHeader={this._renderHeader}>
        <View>{[gopay, bank_transfer].map(this._section)}</View>
      </Container>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  body: getParam('body', {}),
  payment_type: getParam('payment_type', null),
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(PaymentMethodScreen),
  mapFromNavigationParam,
)
