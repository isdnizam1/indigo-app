import React from 'react'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { noop, upperCase, map, isEmpty, findIndex, toLower } from 'lodash-es'
import { connect } from 'react-redux'
import { Dimensions } from 'react-native'
import { BackHandler, View } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { _enhancedNavigation, Button, Container, Header, Icon, Text } from '../components'
import { DEFAULT_PAGING } from '../constants/Routes'
import {
  GREY,
  GREY_WARM,
  WHITE,
  NO_COLOR,
  DARK_INDIGO,
  DARK_SLATE_BLUE,
  TOMATO,
  MANILA,
  MANGO,
  GREY_CALM_SEMI,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
} from '../constants/Colors'
import { getSettingDetail } from '../actions/api'
import { HP1, HP2, WP100, WP15, WP4, WP6 } from '../constants/Sizes'
import { currencyFormatter } from '../utils/helper'
import { BORDER_COLOR } from '../constants/Styles'
import PromoteUserSkeleton from './promote_user/PromoteUserSkeletonConfig'

const { width: viewportWidth } = Dimensions.get('window')

function wp(percentage) {
  const value = (percentage * viewportWidth) / 100
  return Math.round(value)
}

const slideWidth = wp(80)
const itemHorizontalMargin = wp(2)

export const sliderWidth = viewportWidth
export const itemWidth = slideWidth + itemHorizontalMargin * 2

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', noop),
  onSelectedPackage: getParam('onSelectedPackage', noop),
  selectedPackage: getParam('selectedPackage', {})
})

const dummyPackage = {
  benefit: [1, 2, 3, 4, 5, 6]
}

const dummyPackages = [
  dummyPackage, dummyPackage, dummyPackage
]

class PackageScreen extends React.Component {
  state = {
    packages: dummyPackages,
    selectedPackage: {},
    activeSlider: 0,
    isRefreshing: false,
    isLoading: true
  }

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  async componentDidMount() {
    await this._getInitialScreenData()
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _getInitialScreenData = async (...args) => {
    await this._getData(...args)
    this.setState({
      isRefreshing: false
    })
  }

  _getData = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const {
      dispatch
    } = this.props
    //getting data
    const dataResponse = await dispatch(getSettingDetail, {
      'setting_name': 'premium_member',
      'key_name': 'package'
    }, noop, false, isLoading)
    const packages = JSON.parse(dataResponse.value)
    this.setState((state, props) => ({
      packages,
      selectedPackage: !isEmpty(props.selectedPackage) ? props.selectedPackage : packages[0],
      activeSlider: !isEmpty(props.selectedPackage) ? findIndex(packages, ['package_name', props.selectedPackage.package_name]) : 0,
      isLoading: false
    }))
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  _renderItem = ({ item, index }) => {
    const isMaestro = toLower(item.package_name) == 'maestro'
    const { isLoading } = this.state
    return (
      <View>
        <View style={{
          backgroundColor: isMaestro ? GREY : WHITE,
          marginBottom: HP1,
          justifyContent: 'center',
          paddingVertical: HP2,
          borderWidth: 2.5,
          borderColor: isMaestro ? MANILA : WHITE
        }}
        >
          <SkeletonContent
            containerStyle={{ alignItems: 'center' }}
            layout={[
              PromoteUserSkeleton.layouts.title
            ]}
            isLoading={isLoading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <Text type='NeoSans' size='large' weight={500} color={isMaestro ? MANILA : GREY} centered>{`${upperCase(item.package_name)}${isMaestro ? ' Limited Offer' : ''}`}</Text>
          </SkeletonContent>
        </View>
        <View style={{ backgroundColor: isMaestro ? GREY : WHITE, paddingVertical: HP2, borderWidth: 2.5, borderColor: isMaestro ? MANILA : WHITE }}>
          <View style={{ paddingHorizontal: WP4 }}>
            {
              !isLoading && (
                <View style={{ flexDirection: 'row', marginBottom: HP1 }}>
                  <Text size='tiny' style={{ flex: 1 }} color={isMaestro ? WHITE : GREY_WARM} weight={400}>Features</Text>
                  <Text centered size='tiny' style={{ width: WP15 }} color={isMaestro ? WHITE : GREY_WARM} weight={400}>Free</Text>
                  <Text centered size='tiny' style={{ width: WP15 }} color={isMaestro ? WHITE : GREY_WARM} weight={400}>Premium</Text>
                </View>
              )
            }
            {
              map(item.benefit, (benefit, i) => (
                <View key={`bene${i}`}>
                  <SkeletonContent
                    containerStyle={{ flexDirection: 'row', paddingVertical: HP2, borderTopWidth: 1, borderTopColor: isMaestro ? GREY_CALM_SEMI : BORDER_COLOR }}
                    layout={[
                      PromoteUserSkeleton.layouts.benefit
                    ]}
                    isLoading={isLoading}
                    boneColor={SKELETON_COLOR}
                    highlightColor={SKELETON_HIGHLIGHT}
                  >
                    <Text weight={isMaestro && item.benefit.length == i + 1 ? 500 : 300} size='tiny' color={isMaestro ? WHITE : GREY} style={{ flex: 1 }}>{benefit.name}</Text>
                    <View style={{ width: WP15, justifyContent: 'center', alignItems: 'center' }} >
                      {
                        isMaestro && item.benefit.length == i + 1 ? (
                          <Text
                            centered
                            weight={500}
                            size='xtiny'
                            color={WHITE}
                            style={{
                              textDecorationColor: WHITE,
                              textDecorationLine: 'line-through',
                              textDecorationStyle: 'solid'
                            }}
                          >
                            IDR 350.000
                          </Text>
                        ) : (
                          <Icon centered type='Entypo' name='minus' color={isMaestro ? GREY_CALM_SEMI : GREY} />
                        )
                      }
                    </View>
                    <Icon centered style={{ width: WP15 }} type='Entypo' name='check' color={isMaestro ? MANILA : TOMATO} />
                  </SkeletonContent>
                </View>
              ))
            }
          </View>
          <View style={{ borderTopWidth: 1, borderTopColor: isMaestro ? GREY_CALM_SEMI : BORDER_COLOR, paddingTop: HP2, marginTop: HP2 }}>
            <SkeletonContent
              containerStyle={{ alignItems: 'center' }}
              layout={[
                PromoteUserSkeleton.layouts.title
              ]}
              isLoading={isLoading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <Text type='NeoSans' color={isMaestro ? WHITE : GREY} size='massive' weight={500} centered>
                {`${item.currency_price} ${currencyFormatter(Number(item.price))}`}
                <Text type='NeoSans' color={isMaestro ? WHITE : GREY} size='massive'>/</Text>
                <Text type='NeoSans' color={isMaestro ? WHITE : GREY} size='large'>month</Text>
              </Text>
            </SkeletonContent>
          </View>
        </View>
      </View>
    )
  }

  render() {
    const {
      isLoading
    } = this.props
    const {
      packages,
      selectedPackage,
      activeSlider
    } = this.state
    const isMaestro = !isEmpty(selectedPackage) ? toLower(selectedPackage.package_name) == 'maestro' : false
    return (
      <Container
        theme='light'
        scrollable
        scrollBackgroundColor='transparent'
        colors={[DARK_SLATE_BLUE, DARK_INDIGO]}
        type='vertical'
        renderHeader={() => (
          <Header>
            <Icon
              onPress={this._backHandler} background='dark-circle' size='large' color={WHITE}
              name='chevron-left' type='Entypo'
            />
            <Text size='mini' type='NeoSans' weight={500} color={WHITE}>Premium Membership</Text>
            <Icon size='large' color={NO_COLOR} name='left' />
          </Header>
        )}
        outsideScrollContent={() => (
          !isEmpty(selectedPackage) && <View style={{
            width: WP100,
            alignItems: 'center',
            paddingHorizontal: WP6,
            paddingVertical: WP4,
            elevation: 20
          }}
                                       >
            <Button
              onPress={async () => {
                this.props.onSelectedPackage(selectedPackage)
                this.props.navigateBack()
              }}
              width='100%'
              contentStyle={{ paddingVertical: WP4 }}
              disable={isLoading}
              colors={isMaestro ? [MANILA, MANGO] : [TOMATO, TOMATO]}
              start={[0, 0]} end={[0, 1]}
              centered
              marginless
              shadow='none'
              textColor={isMaestro ? GREY : WHITE}
              radius={12}
              textType='NeoSans'
              textSize='small'
              textWeight={500}
              text={`Choose ${(selectedPackage.package_name || 'This')} Plan`}
            />
          </View>
        )}
      >
        <Carousel
          onSnapToItem={(i) => this.setState({ selectedPackage: packages[i], activeSlider: i })}
          data={packages}
          firstItem={activeSlider}
          contentContainerCustomStyle={{ alignItems: 'center' }}
          renderItem={this._renderItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
        />
        <Pagination
          dotsLength={packages.length}
          activeDotIndex={activeSlider}
          dotColor={WHITE}
          inactiveDotColor={WHITE}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(PackageScreen),
  mapFromNavigationParam
)
