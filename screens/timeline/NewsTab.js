import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TouchableOpacity, View } from 'react-native'
import moment from 'moment'
import { LinearGradient } from 'expo-linear-gradient'
import SkeletonContent from 'react-native-skeleton-content'
import EmptyV3 from 'sf-components/EmptyV3'
import Spacer from 'sf-components/Spacer'
import { WP105 } from 'sf-constants/Sizes'
import { NAVY_DARK, PALE_GREY_TWO, SKELETON_COLOR, SKELETON_HIGHLIGHT, WHITE } from '../../constants/Colors'
import { HP05, WP1, WP20, WP3, WP30, WP40, WP43, WP5, WP6, WP67, WP7, WP90 } from '../../constants/Sizes'
import Container from '../../components/Container'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import { getNews, getNews1000Startup } from '../../actions/api'
import Text from '../../components/Text'
import AllNewsItem from '../news/components/AllNewsItem'
import NewsItem from '../news/components/NewsItem'
import Image from '../../components/Image'
import Icon from '../../components/Icon'
import { LINEAR_TYPE } from '../../constants/Styles'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
}

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam('onRefresh', () => {
  }),
})

class NewsTab extends Component {
  constructor(props) {
    super(props)
    this.state = {
      latestNews: [],
      allNews: [],
      start: 0,
      limit: 15,
      isLoading: true
    }
  }

  componentDidMount = async () => {
    this._getNews()
  }

  _getNews = async (loadMore = false) => {
    const {
      dispatch,
      is1000Startup
    } = this.props
    const { allNews, limit } = this.state
    const start = loadMore ? this.state.start : 0
    let URL = is1000Startup ? getNews1000Startup : getNews
    const news = await dispatch(URL, { start, limit })

    await this.setState({
      latestNews: news.latest_news,
      allNews: loadMore ? [...allNews, ...news.all_news] : news.all_news,
      start: start + limit,
      isLoading: false
    })
  }

  _renderLatestNewsSection = () => {
    const {
      navigateTo,
      is1000Startup,
    } = this.props

    const {
      latestNews
    } = this.state

    if (latestNews.length === 0) {
      return null
    }

    return (
      <View style={{ marginTop: WP5 }}>
        <Text type='Circular' weight={600} color={NAVY_DARK} size='medium'>Latest News</Text>
        <View style={{ marginVertical: WP5, flexDirection: 'row', flex: 1, flexWrap: 'wrap', justifyContent: 'space-between' }}>
          {
            latestNews.map((item, index) => {
              if (index === 0) {
                return (
                  <TouchableOpacity
                    style={{ width: WP90, height: WP67, marginBottom: WP5, justifyContent: 'flex-end' }}
                    onPress={() => {
                      navigateTo('NewsDetailScreen', { id_ads: item.id_ads, is1000Startup }, 'push')
                    }}
                  >
                    <Image
                      aspectRatio={WP90/WP67}
                      source={{ uri: item.image }}
                      imageStyle={{ width: WP90, height: WP67, borderRadius: 5 }}
                      style={{ position: 'absolute' }}
                    />
                    <LinearGradient
                      colors={['rgba(33, 43, 54, 0)', 'rgba(33, 43, 54, 1)']}
                      {...LINEAR_TYPE.vertical}
                      style={{ position: 'absolute', top: '50%', bottom: 0, left: 0, right: 0, borderBottomLeftRadius: WP105, borderBottomRightRadius: WP105 }}
                    />
                    <View style={{ padding: WP5 }}>
                      <Text numberOfLines={3} ellipsizeMode='tail' color={WHITE} weight={500} size='slight' style={{ marginBottom: WP1 }}>
                        {item.title}
                      </Text>
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text color={PALE_GREY_TWO} size='xmini' type='Circular'>{moment(item.created_at).format('D MMMM YYYY')}</Text>
                        <Icon centered color={PALE_GREY_TWO} size='tiny' type='Entypo' name='dot-single'/>
                        <Text color={PALE_GREY_TWO} weight={200} size='xmini'>
                          {item.mins_read}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                )
              }
              return (
                <NewsItem
                  withViewCount
                  key={index}
                  ads={item}
                  navigateTo={navigateTo}
                  is1000Startup={is1000Startup}
                />
              )
            })
          }
        </View>
      </View>
    )
  }

  _renderAllNewsSection = () => {
    const {
      navigateTo,
      is1000Startup,
    } = this.props

    const {
      allNews
    } = this.state

    return (
      <View style={{ }}>
        <Text type='Circular' weight={600} color={NAVY_DARK} size='medium'>All News</Text>
        <View style={{ marginVertical: WP5 }}>
          {
            allNews.length === 0 ?
            (
              <EmptyV3
                imageWidth={WP20}
                aspectRatio={1 / 1}
                image={require('sf-assets/icons/v3/mdiComment.png')}
                title='Belum ada artikel'
                message='Belum ada artikel untuk saat ini'
                actions={<Spacer size={WP40} />}
              />
            )
            :
            allNews.map((item, index) => (
              <AllNewsItem
                key={index}
                ads={item}
                navigateTo={navigateTo}
                is1000Startup={is1000Startup}
              />
            ))
          }
        </View>
      </View>
    )
  }

  _renderSkeleton = () => {
    const ItemSkeleton = (
      <SkeletonContent
        containerStyle={{ marginBottom: WP5 }}
        layout={[
          { width: WP43, height: WP30, marginBottom: WP3 },
          { width: WP40, height: WP6 }
        ]}
        isLoading={true}
        boneColor={SKELETON_COLOR}
        highlightColor={SKELETON_HIGHLIGHT}
      />
    )

    return (
      <View style={{ marginTop: WP5 }}>
        <SkeletonContent
          layout={[
            { width: WP30, height: WP7, marginBottom: WP3 },
            { width: WP90, height: WP67 }
          ]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
        <View style={{ marginVertical: WP5, flexDirection: 'row', flex: 1, flexWrap: 'wrap', justifyContent: 'space-between' }}>
          {ItemSkeleton}
          {ItemSkeleton}
          {ItemSkeleton}
          {ItemSkeleton}
        </View>
      </View>
    )
  }

  render() {
    const {
      isLoading
    } = this.state

    return (
      <Container
        scrollable
        noStatusBarPadding
        type='horizontal'
        scrollBackgroundColor={WHITE}
        scrollContentContainerStyle={{ marginTop: HP05 }}
        hasBottomNavbar
        onPullDownToRefresh={async () => {
          this._getNews(false)
        }}
        onPullUpToLoad={async () => {
          this._getNews(true)
        }}
      >
        <View style={{ paddingHorizontal: WP5 }}>
          {
            isLoading ?
              this._renderSkeleton() :
            <>
              {this._renderLatestNewsSection()}
              {this._renderAllNewsSection()}
            </>
          }
        </View>
      </Container>
    )
  }
}

NewsTab.propTypes = {}

NewsTab.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(NewsTab),
  mapFromNavigationParam
)
