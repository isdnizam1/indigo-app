import React, { Component } from 'react'
import { View, ImageBackground } from 'react-native'
import Text from '../../components/Text'
import { HP05, WP3, WP4, WP40, WP6 } from '../../constants/Sizes'
import { NO_COLOR, PURPLE, WHITE } from '../../constants/Colors'
import Button from '../../components/Button'
import { postFeedback } from '../../actions/api'
import Modal from '../../components/Modal'
import { isIOS } from '../../utils/helper'
import { modalContent } from './FeedbackModalContent'

const style = {
  wrapper: {
    paddingHorizontal: WP6,
    paddingVertical: WP4,
    paddingBottom: WP3,
    marginHorizontal: HP05,
    borderRadius: 10,
    marginBottom: HP05
  },
  text: {
    color: WHITE
  },
  optionWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  btn: {
    borderColor: WHITE,
    borderWidth: 2
  },
  btnOutline: {
    backgroundColor: NO_COLOR
  },
  btnFilled: {
    backgroundColor: PURPLE
  },
  modalContent: {
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    backgroundColor: '#e3e3e3'
  }
}

const feedbackOption = {
  YES: '1',
  NO: '-1'
}

class FeedbackFeed extends Component {
  state = {
    feedback: 'YES'
  }

  _handleButtonPress = (feedbackValue, toggleModal) => async () => {
    const { userData, dispatch } = this.props
    const body = {
      id_user: userData.id_user,
      is_feedback: feedbackOption[feedbackValue]
    }
    await dispatch(postFeedback, body)
    this.setState({ feedback: feedbackValue }, toggleModal)
  }

  render() {
    const {
      dispatch,
      userData,
      getUserDetailDispatcher
    } = this.props

    if (isIOS()) {
      return <View />
    }

    return <View />

    return (
      <ImageBackground
        source={require('../../assets/images/feedback-background-m.png')}
        imageStyle={{ borderRadius: 10 }}
        style={[style.wrapper, { display: 'none' }]}
      >
        <Text centered color={WHITE} weight={500}>
          Enjoying Soundfren?
        </Text>
        <Modal
          position='center'
          closeOnBackdrop={true}
          renderModalContent={({ toggleModal, payload }) => modalContent[this.state.feedback](toggleModal, getUserDetailDispatcher, userData.id_user, dispatch)}
          style={style.modalContent}
          swipeDirection={null}
        >
          {({ toggleModal }, M) => (
            <View>
              <View style={style.optionWrapper}>
                <Button
                  width={WP40}
                  colors={[NO_COLOR, NO_COLOR]}
                  contentStyle={[style.btn, style.btnOutline]}
                  rounded
                  text='Not Really'
                  textWeight={400}
                  textColor={WHITE}
                  textSize='mini'
                  centered
                  onPress={this._handleButtonPress('NO', toggleModal)}
                />
                <Button
                  width={WP40}
                  contentStyle={[style.btn, style.btnFilled]}
                  rounded
                  text='Yes, of course!'
                  textWeight={400}
                  textColor={PURPLE}
                  textSize='mini'
                  centered
                  onPress={this._handleButtonPress('YES', toggleModal)}
                />
              </View>
              {M}
            </View>
          )}
        </Modal>
      </ImageBackground>
    )
  }
}

FeedbackFeed.propTypes = {}

FeedbackFeed.defaultProps = {}

export default FeedbackFeed
