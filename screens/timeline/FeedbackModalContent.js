import React from 'react'
import { TextInput, View } from 'react-native'
import * as WebBrowser from 'expo-web-browser'
import { isEmpty } from 'lodash-es'
import Text from '../../components/Text'
import { HP30, WP4, WP401 } from '../../constants/Sizes'
import { GREY, WHITE } from '../../constants/Colors'
import Button from '../../components/Button'
import { postFeedbackReason } from '../../actions/api'

const style = {
  container: {
    borderRadius: 15,
    backgroundColor: '#e3e3e3',
    flexWrap: 'wrap',
  },
  wrapper: {
    padding: WP4,
  },
  reasonText: {
    paddingLeft: WP4,
    paddingRight: WP4,
    paddingTop: WP4,
    paddingBottom: WP4,
    height: HP30,
    backgroundColor: WHITE,
    textAlignVertical: 'top',
    borderRadius: 5,
    color: GREY,
    fontSize: WP401
  },
  buttonWrapper: {
    flexDirection: 'row',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  button: {
    flexGrow: 1,
    marginVertical: 0,
    overflow: 'hidden'
  },
  title: {
    marginBottom: 20,
  }
}

export class FeedbackComment extends React.Component {
  state = {
    description: ''
  }

  _handleTextChange = (text) => {
    this.setState({ description: text })
  }

  render() {
    const { toggleModal, userId, dispatch, getUserDetailDispatcher } = this.props

    return (
      <View style={style.container}>
        <View style={style.wrapper}>
          <Text centered weight={600} color={GREY} style={style.title}>
            Would you tell us why?
          </Text>
          <TextInput
            style={style.reasonText}
            placeholder='Type here...'
            multiline
            onChangeText={this._handleTextChange}
          />
        </View>
        <View style={style.buttonWrapper}>
          <Button
            style={[style.button, { borderBottomLeftRadius: 15 }]}
            colors={['#e3e3e3', '#e3e3e3']}
            text='No, Thanks'
            textWeight={600}
            textColor={GREY}
            textSize='slight'
            centered
            onPress={async () => {
              toggleModal()
              await getUserDetailDispatcher({ id_user: userId })
            }}
          />
          <Button
            style={[style.button, { borderBottomRightRadius: 15 }]}
            disable={isEmpty(this.state.description)}
            soundfren
            text='Submit!'
            textWeight={600}
            textColor={WHITE}
            textSize='slight'
            centered
            onPress={async () => {
              const { description } = this.state
              try {
                await dispatch(postFeedbackReason, { id_user: userId, description })
              } catch (e) {
                //
              }
              toggleModal()
              await getUserDetailDispatcher({ id_user: userId })
            }}
          />
        </View>
      </View>
    )
  }
}

export class FeedbackRating extends React.Component {
  render() {
    const { toggleModal, getUserDetailDispatcher, userId } = this.props
    return (
      <View style={[style.container, { backgroundColor: WHITE }]}>
        <View style={[style.wrapper, { marginVertical: WP4 }]}>
          <Text centered weight={600} color={GREY} size='slight'>
            Would you rate us on Play Store?
          </Text>
        </View>
        <View style={style.buttonWrapper}>
          <Button
            style={[style.button, { borderBottomLeftRadius: 15 }]}
            colors={[WHITE, WHITE]}
            text='No, Thanks'
            textWeight={600}
            textColor={GREY}
            textSize='slight'
            centered
            onPress={async () => {
              toggleModal()
              await getUserDetailDispatcher({ id_user: userId })
            }}
          />
          <Button
            style={[style.button, { borderBottomRightRadius: 15 }]}
            soundfren
            text='Sure!'
            textWeight={600}
            textColor={WHITE}
            textSize='slight'
            centered
            onPress={async () => {
              WebBrowser.openBrowserAsync('https://bit.ly/SoundfrenApps')
              toggleModal()
              await getUserDetailDispatcher({ id_user: userId })
            }}
          />
        </View>
      </View>
    )
  }
}

export const modalContent = {
  // eslint-disable-next-line react/display-name
  YES: (toggleModal, getUserDetailDispatcher, userId) =>
    <FeedbackRating toggleModal={toggleModal} getUserDetailDispatcher={getUserDetailDispatcher} userId={userId} />,
  // eslint-disable-next-line react/display-name
  NO: (toggleModal, getUserDetailDispatcher, userId, dispatch) =>
    <FeedbackComment toggleModal={toggleModal} getUserDetailDispatcher={getUserDetailDispatcher} userId={userId} dispatch={dispatch} />
}
