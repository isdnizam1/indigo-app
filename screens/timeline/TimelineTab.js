import React, { Fragment } from 'react'
import { RefreshControl, View, FlatList } from 'react-native'
import { withNavigation } from '@react-navigation/compat'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash-es'
import {
  _enhancedNavigation,
  Button,
  Container,
  DeletePost,
  EmptyV3,
  FeedItem,
  Icon,
  Modal,
} from '../../components'
import {
  GREY_WHITE,
  PALE_LIGHT_BLUE_TWO,
  REDDISH,
  WHITE,
} from '../../constants/Colors'
import { HP05, HP1, WP10, WP100, WP2, WP20, WP5 } from '../../constants/Sizes'
import { constructTimeline } from '../../utils/feed'
import { authDispatcher } from '../../services/auth'
import Loader from '../../components/Loader'
import { SET_TIMELINE_SCROLL_DIRECTION } from '../../services/timeline/actionTypes'
import NewsTab from './NewsTab'

const mapStateToProps = ({ auth, timeline: { scrollDirection } }) => ({
  userData: auth.user,
  scrollDirection,
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setTimelineScrollDirection: (response) => ({
    type: SET_TIMELINE_SCROLL_DIRECTION,
    response,
  }),
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
})

class TimelineTab extends React.Component {
  constructor(props) {
    super(props)
    this.onTopicLayout = this.onTopicLayout.bind(this)
  }
  state = {
    topicIsLoading: false,
    isRefreshing: false,
  };

  shouldComponentUpdate(nextProps, nextState) {
    const { isRefreshing, topicIsLoading } = this.state
    const { feeds, deletededFeeds, selectedTopicId, isTrendingShown } = this.props
    const result =
      ((isRefreshing == !nextState.isRefreshing ||
        topicIsLoading == !nextState.topicIsLoading ||
        feeds.length != nextProps.feeds.length ||
        deletededFeeds.length != nextProps.deletededFeeds.length ||
        selectedTopicId != nextProps.selectedTopicId) &&
        nextProps.isActive) ||
      isTrendingShown

    return result || true
  }

  scrollDirection = null;
  topicScrollView = React.createRef();
  topicButtonPos = {};

  onTopicLayout({ nativeEvent }, topicId) {
    const { x, y } = nativeEvent.layout
    this.topicButtonPos[topicId] = { x, y, animated: true }
  }

  // componentDidUpdate(prevProps) {
  //   const { selectedTopicId } = this.props
  //   selectedTopicId && typeof this.topicScrollView != 'undefined' && typeof this.topicScrollView.scrollTo === 'function' && this.topicScrollView.scrollTo(this.topicButtonPos[selectedTopicId])
  // }

  _renderTrendingSection = () => {};

  putTrending = (list) => {
    return [this._renderTrendingSection(), list]
  };

  render() {
    const {
      feeds,
      deletededFeeds,
      userData,
      navigateTo,
      isActive,
      getFeeds,
      isTabReady,
      onDeletePost,
      topics,
      isTrendingShown,
      trending,
      onFeedScroll,
      selectedTopicId,
      navigation,
      is1000Startup,
      type,
    } = this.props
    const { topicIsLoading, isRefreshing } = this.state

    // const timelineData = constructTimeline(feeds, isTrendingShown && !isEmpty(trending), true)
    const timelineData = constructTimeline(
      feeds,
      isTrendingShown && !isEmpty(trending),
      true,
    )

    if (selectedTopicId == 13) {
      return <NewsTab navigation={navigation} />
    }

    return (
      <Container
        noStatusBarPadding
        type='horizontal'
        scrollBackgroundColor={WHITE}
        scrollContentContainerStyle={{ marginTop: !isEmpty(topics) ? HP05 : 0 }}
        onPullDownToRefresh={async () => {
          if (!isTabReady) return
          getFeeds()
        }}
      >
        <Fragment>
          <Modal
            unstyled
            renderModalContent={({ toggleModal, payload }) => (
              <DeletePost
                onCancel={toggleModal}
                onConfirm={() => {
                  toggleModal()
                  onDeletePost(Number(payload.id_journey))
                }}
              />
            )}
          >
            {({ toggleModal }, M) => (
              <View style={{ flexGrow: 1 }}>
                {!isEmpty(feeds) ? (
                  <FlatList
                    style={{ backgroundColor: GREY_WHITE, flex: 1 }}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                      <RefreshControl
                        refreshing={isRefreshing}
                        onRefresh={async () => {
                          if (!isTabReady) return
                          await this.setState({ isRefreshing: true })
                          getFeeds()
                          this.setState({ isRefreshing: false })
                        }}
                      />
                    }
                    onScroll={onFeedScroll}
                    ListFooterComponent={() => (
                      <>
                        {topicIsLoading && <Loader size='mini' isLoading />}
                        <View style={{ marginBottom: WP20 }} />
                      </>
                    )}
                    onEndReached={
                      isEmpty(feeds)
                        ? null
                        : async () => {
                            if (!isTabReady) return
                            if (topicIsLoading) return
                            this.setState({ topicIsLoading: true })
                            await getFeeds(
                              { start: feeds.length, limit: 5 },
                              true,
                              false,
                            )
                            this.setState({ topicIsLoading: false })
                          }
                    }
                    onEndReachedThreshold={0.1}
                    data={timelineData}
                    keyExtractor={(item, index) =>
                      `timeline-${index}-${item.id_timeline}`
                    }
                    renderItem={({ item, index }) =>
                      deletededFeeds.indexOf(parseInt(item.id_journey)) == -1 ? (
                        <Fragment key={index}>
                          <FeedItem
                            {...this.props}
                            is1000Startup={is1000Startup}
                            navigateTo={navigateTo}
                            onRefresh={getFeeds}
                            key={index}
                            onPressOption={
                              item.id_user === userData.id_user ? toggleModal : null
                            }
                            feed={item}
                            showCommentField
                            isLoading={!isTabReady}
                            ownFeed={item.id_user === userData.id_user}
                            onDeleteFeed={onDeletePost}
                          />
                        </Fragment>
                      ) : null
                    }
                  />
                ) : isActive && type === 'circle' ? (
                  <EmptyV3
                    title='Buat lingkaranmu'
                    message='Terhubung lebih dekat bersama teman-teman kamu disini'
                    icon={
                      <Icon
                        centered
                        style={{ borderRadius: WP100, marginBottom: HP1 }}
                        padding={WP5}
                        type='MaterialIcons'
                        name='group'
                        size={WP10}
                        color={WHITE}
                        background='pale-light-circle'
                        backgroundColor={PALE_LIGHT_BLUE_TWO}
                      />
                    }
                    actions={
                      <Button
                        onPress={() =>
                          navigateTo('FindFriendScreen', {
                            isStartup: this.props.is1000Startup,
                            onRefresh: async () => {
                              if (!isTabReady) return
                              await this.setState({ isRefreshing: true })
                              getFeeds()
                              this.setState({ isRefreshing: false })
                            },
                          })
                        }
                        radius={WP2}
                        centered
                        backgroundColor={REDDISH}
                        text='Cari teman'
                        textWeight={400}
                        textSize='mini'
                        textColor={WHITE}
                        shadow='none'
                      />
                    }
                  />
                ) : null}
                {M}
              </View>
            )}
          </Modal>
        </Fragment>
      </Container>
    )
  }
}

export default withNavigation(
  _enhancedNavigation(
    connect(mapStateToProps, mapDispatchToProps)(TimelineTab),
    mapFromNavigationParam,
  ),
)
