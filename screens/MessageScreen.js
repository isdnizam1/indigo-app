import React from 'react'
import { ScrollView, Image, View, BackHandler } from 'react-native'
import { isNull } from 'lodash-es'
import { Container, Header, Icon, Text, Button, _enhancedNavigation } from '../components'
import { WP5, WP30, HP2 } from '../constants/Sizes'
import { ORANGE_BRIGHT_DARK, PINK_RED_DARK, WHITE, WHITE20 } from '../constants/Colors'
import { NavigateToInternalBrowser } from '../utils/helper'

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', true),
  isBack: getParam('isBack', false),
  iconHeader: getParam('iconHeader', null),
  title: getParam('title', null),
  message: getParam('message', null),
  toScreen: getParam('toScreen', null),
  toScreenParams: getParam('toScreenParams', {}),
  toReset: getParam('toReset', {}),
  linking: getParam('linking', null),
  buttonText: getParam('buttonText', 'Continue')
})

const defaultProps = {
  iconHeader: null,
  initialLoaded: true,
  isLoading: true
}

class MessageScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam('initialLoaded', false)
  })

  attachBackAction() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  releaseBackAction() {
    try {
      this.backHandler.remove()
    } catch(err) { /*silent is gold*/ }
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('focus', this.attachBackAction.bind(this))
    this.blurListener = this.props.navigation.addListener('blur', this.releaseBackAction.bind(this))
  }

  componentWillUnmount() {
    try {
      this.releaseBackAction()
      typeof this.focusListener !== 'undefined' && this.focusListener()
      typeof this.blurListener !== 'undefined' && this.blurListener()
    } catch(err) { /*silent is gold*/ }
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  render() {
    const {
      navigateTo,
      navigateBack,
      resetNavigation,
      isBack,
      isLoading,
      iconHeader,
      title,
      message,
      toScreen,
      linking,
      buttonText,
      toReset,
      toScreenParams
    } = this.props

    return (
      <Container isLoading={isLoading} colors={[ORANGE_BRIGHT_DARK, PINK_RED_DARK]} theme='light'>
        <Header>
          {
            isBack && (
              <Icon onPress={this._backHandler} size='massive' color={WHITE} name='left'/>
            )
          }
        </Header>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ marginHorizontal: WP5, flex: 1, justifyContent: 'center' }}
          keyboardShouldPersistTaps='handled'
        >
          {
            iconHeader || (
              <Image
                source={require('../assets/icons/successWhite.png')}
                style={{ width: WP30, height: undefined, aspectRatio: 1, alignSelf: 'center' }}
              />
            )
          }
          <Text style={{ marginTop: HP2 }} centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>
            {
              title
            }
          </Text>
          <View>
            <View style={{ marginVertical: HP2 }}>
              {
                <Text centered color={WHITE}>{message}</Text>
              }
            </View>
            <Button
              onPress={() => {
                if (!isNull(linking)) {
                  NavigateToInternalBrowser({
                    url: linking
                  })
                } else if (!isNull(toScreen)) navigateTo(toScreen, toScreenParams)
                else if (!isNull(toReset)) resetNavigation(toReset)
                else navigateBack()
              }}
              rounded
              centered
              shadow='none'
              backgroundColor={WHITE20}
              textColor={WHITE}
              text={buttonText}
            />
          </View>
        </ScrollView>
      </Container>
    )
  }
}

MessageScreen.defaultProps = defaultProps
export default _enhancedNavigation(
  MessageScreen,
  mapFromNavigationParam,
  true
)
