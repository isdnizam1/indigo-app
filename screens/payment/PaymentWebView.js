import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BackHandler } from 'react-native'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import ConfirmationModalV3 from '../../components/ConfirmationModalV3'
import BrowserScreen from '../BrowserScreen'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  url: getParam('url', ''),
})

class PaymentWebView extends Component {
  state = {
    showExitWarning: false,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _backHandler = () => {
    this.setState({ showExitWarning: true })
    return true
  };

  _navigateToPaymentHistory = () => {
    const { popToTop, navigateTo } = this.props

    popToTop()
    navigateTo('ProfileScreen')
    navigateTo('SettingScreen')
    navigateTo('PaymentHistory')
  };

  render() {
    const { url } = this.props
    const { showExitWarning } = this.state

    return (
      <>
        <BrowserScreen
          {...this.props}
          title='Pembayaran'
          url={url}
          backHandler={() => this.setState({ showExitWarning: true })}
          onNavigationStateChange={async (state) => {
            if ((state?.url || '').includes('payment/finish')) {
              this._navigateToPaymentHistory()
            }

            // TODO Navigate to NotificationScreen
          }}
        />
        <ConfirmationModalV3
          visible={showExitWarning}
          title='Pembayaran Belum Selesai'
          subtitle='Yakin kamu ingin keluar dari halaman ini? Proses Pembayaran belum selesai'
          primary={{
            text: 'Lanjutkan Pembayaran',
            onPress: () => {
              this.setState({ showExitWarning: false })
            },
          }}
          secondary={{
            text: 'Keluar',
            onPress: () => {
              this.setState({ showExitWarning: false })
              this._navigateToPaymentHistory()
            },
          }}
        />
      </>
    )
  }
}

PaymentWebView.propTypes = {}
PaymentWebView.defaultProps = {}
export default _enhancedNavigation(
  connect(mapStateToProps, {})(PaymentWebView),
  mapFromNavigationParam,
)
