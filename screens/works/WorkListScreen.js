import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import { ImageBackground, View, FlatList } from 'react-native'
import { isEmpty, noop, startCase, groupBy, get } from 'lodash-es'
import {
  _enhancedNavigation,
  Container,
  Button,
  HeaderNormal,
  Text, Icon,
  Player, ListItem, Modal
} from '../../components'
import { WHITE, GREY_CALM_SEMI, BLUE_LIGHT } from '../../constants/Colors'
import { WP1, WP100, WP2, WP4, WP30, WP3, HP5 } from '../../constants/Sizes'
import { getProfileSongs, getProfileVideos, getProfileArts, getExperiences } from '../../actions/api'
import { WORKSPAGE } from '../../components/works/WorksConstant'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  actionEdit: getParam('actionEdit', () => {}),
  actionDelete: getParam('actionDelete', () => {}),
  actionAdd: getParam('actionAdd', () => {}),
  category: getParam('category', 'null'),
  refreshProfile: getParam('refreshProfile', () => {}),
  user: getParam('user', {}),
})

const propsDefault = {}

class WorkListScreen extends React.Component {

  constructor(props) {
    super(props)
  }

  state = {
    isReady: true,
    data: []
  }

  async componentDidMount() {
    await this._getInitialScreenData()
  }

  _getInitialScreenData = async () => {
    const {
      dispatch,
      user,
      category
    } = this.props
    this.setState({
      isReady: false,
    })
    const idUser = user.id_user
    let dataResponse

    switch (category) {
    case 'song':
      dataResponse = await dispatch(getProfileSongs, { id_user: idUser }, noop, true, true)
      break
    case 'video':
      dataResponse = await dispatch(getProfileVideos, { id_user: idUser }, noop, true, true)
      break
    case 'picture':
      dataResponse = await dispatch(getProfileArts, { id_user: idUser }, noop, true, true)
      dataResponse.result = Object.values(groupBy(dataResponse.result, 'id_journey'))
      break
    case 'performance':
    case 'journey':
      dataResponse = await dispatch(getExperiences, { id_user: idUser }, noop, true, true)
      break
    default:
      break
    }
    this.setState({
      isReady: true,
      data: dataResponse.result
    })
  }

  _renderModalContent = ({ toggleModal, payload }) => {
    const {
      actionEdit,
      actionDelete,
      navigateBack,
      navigateTo,
      category
    } = this.props
    return (
      <View>
        <ListItem
          inline
          onPress={async () => {
            toggleModal()
            navigateTo('ShareScreen', {
              id: get(payload, WORKSPAGE[category].idPerItem),
              type: 'profile',
              title: `My ${WORKSPAGE[category].title}`
            })
          }}
        >
          <Icon size='large' name='form' />
          <Text style={{ paddingLeft: WP2 }}>Share</Text>
        </ListItem>
        <ListItem
          inline
          onPress={async () => {
            toggleModal()
            navigateBack()
            actionEdit(payload)
          }}
        >
          <Icon size='large' name='edit' />
          <Text style={{ paddingLeft: WP2 }}>Edit</Text>
        </ListItem>
        <ListItem
          inline
          onPress={async () => {
            toggleModal()
            navigateBack()
            actionDelete(get(payload, WORKSPAGE[category].idPerItem))
          }}
        >
          <Icon size='large' name='delete' />
          <Text style={{ paddingLeft: WP2 }}>Delete</Text>
        </ListItem>
      </View>
    )
  }

  render() {
    const { isLoading, userData, navigateBack, category, user, navigateTo,
      actionAdd } = this.props
    const { isReady, data } = this.state

    const idUser = user.id_user
    const isMine = userData.id_user === idUser

    const page = WORKSPAGE[category]
    const WorkItem = page.item || View

    return (
      <Container
        isLoading={isLoading}
        isReady={isReady}
        scrollable
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text={page.title}
            rightComponent={isMine && (
              <Button
                style={{
                  alignSelf: 'flex-start'
                }}
                onPress={() => {
                  navigateBack()
                  actionAdd()
                }}
                marginless
                contentStyle={{ width: WP30, paddingHorizontal: WP1, paddingLeft: WP3, justifyContent: 'space-between' }}
                iconName='add-circle-outline'
                iconType='MaterialIcons'
                iconSize='mini'
                iconPosition='right'
                iconColor={WHITE}
                compact='center'
                rounded
                backgroundColor={BLUE_LIGHT}
                shadow='none'
                text={page.addCaption}
                textColor={WHITE}
                textType='NeoSans'
                textSize='xtiny'
                textWeight={500}
              />)}
          />
        )}
      >
        <View style={{ alignItems: 'center' }}>
          {
            page.bannerImage &&
            <ImageBackground
              source={page.bannerImage}
              style={{ width: WP100, backgroundColor: GREY_CALM_SEMI, aspectRatio: 32 / 16, paddingLeft: WP4, justifyContent: 'center' }}
            >
              <Text type='NeoSans' color={WHITE} size='extraMassive'>{startCase(category)}</Text>
              <Text type='NeoSans' color={WHITE} size='mini'>{`of ${user.full_name}`}</Text>
            </ImageBackground>
          }
          {
            category === 'song' ?
              <Player>
                {
                  (playerOpts) => {
                    return (
                      <FlatList
                        contentContainerStyle={{
                          justifyContent: 'center',
                          flexDirection: 'column',
                          alignItems: 'flex-start'
                        }}
                        ListEmptyComponent={
                          <View style={{ marginTop: HP5, width: '100%', alignItems: 'center' }}>
                            <Text centered size='small'>
                              <Text type='NeoSans' size='mini'>Hey, it`s empty here, </Text>
                              <Text type='NeoSans' size='mini' weight={500} >come back soon  </Text>
                            </Text>
                          </View>
                        }
                        data={!isEmpty(data) && !isEmpty(page.itemSelector) ? data[page.itemSelector] : data}
                        numColumns={page.numColumns || 1}
                        scrollEnabled={false}
                        keyExtractor={(item, index) => `key${ index}`}
                        renderItem={({ item }) => (
                          <WorkItem
                            data={item}
                            playerOpts={playerOpts}
                            renderAction={isMine ? (
                              <Modal
                                renderModalContent={this._renderModalContent}
                              >
                                {
                                  ({ toggleModal }, M) => (
                                    <Fragment>
                                      <Button
                                        onPress={() => toggleModal(item)}
                                        imageIcon={require('../../assets/icons/buttonEditProfile.png')}
                                        imageSize='badge'
                                        contentStyle={{ padding: WP2 }}
                                        shadow='shadow'
                                        backgroundColor={GREY_CALM_SEMI}
                                        rounded
                                      />
                                      {M}
                                    </Fragment>
                                  )
                                }
                              </Modal>) : <View/>}
                          />
                        )}
                      />
                    )
                  }
                }
              </Player> :
              <FlatList
                contentContainerStyle={{
                  justifyContent: 'center',
                  flexDirection: 'column',
                  alignItems: 'flex-start'
                }}
                ListEmptyComponent={
                  <View style={{ marginTop: HP5, width: '100%', alignItems: 'center' }}>
                    <Text centered size='small'>
                      <Text type='NeoSans' size='mini'>Hey, it`s empty here, </Text>
                      <Text type='NeoSans' size='mini' weight={500} >come back soon  </Text>
                    </Text>
                  </View>
                }
                data={!isEmpty(page.itemSelector) ? data[page.itemSelector] : data}
                numColumns={page.numColumns || 1}
                scrollEnabled={false}
                keyExtractor={(item, index) => `key${ index}`}
                renderItem={({ item }) => (
                  <WorkItem
                    data={item}
                    navigateTo={navigateTo}
                    renderAction={isMine ? (
                      <Modal
                        renderModalContent={this._renderModalContent}
                      >
                        {
                          ({ toggleModal }, M) => (
                            <Fragment>
                              <Button
                                onPress={() => toggleModal(item)}
                                imageIcon={require('../../assets/icons/buttonEditProfile.png')}
                                imageSize='badge'
                                contentStyle={{ padding: WP2 }}
                                shadow='shadow'
                                backgroundColor={GREY_CALM_SEMI}
                                rounded
                              />
                              {M}
                            </Fragment>
                          )
                        }
                      </Modal>) : <View/>}
                  />
                )}
              />
          }
        </View>
      </Container>
    )
  }
}

WorkListScreen.defaultProps = propsDefault
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(WorkListScreen),
  mapFromNavigationParam
)
