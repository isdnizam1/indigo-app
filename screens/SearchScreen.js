import React from 'react'
import { connect } from 'react-redux'
import { BackHandler } from 'react-native'
import { KeyboardAwareScrollView as ScrollView } from 'react-native-keyboard-aware-scroll-view'
import {
  startCase,
  isObject,
  isNumber,
  uniqWith,
  isEqual,
  isEmpty,
  isNil,
  get,
  pick,
} from 'lodash-es'
import { CancelToken } from 'axios'
import { _enhancedNavigation, Container } from 'sf-components'
import { Peoples, Activities, SearchBox, Recents } from 'sf-components/search'
import { getAdvancedSearch } from 'sf-actions/api'
import { getRecent, pushRecent } from 'sf-utils/storage'
import {
  setCurrentTab,
  setRecentSearch,
  setSearchingState,
  setQuery,
} from 'sf-services/search/actionDispatcher'
import { validateQuery, filterAdListing, isEqualObject } from 'sf-utils/helper'
import { KEY_RECENT_SEARCH } from 'sf-constants/Storage'
import bugsnagClient from 'sf-utils/bugsnag'
import VisibleView from 'sf-components/VisibleView'
import InteractionManager from 'sf-utils/InteractionManager'
import produce from 'immer'

const mapStateToProps = ({
  auth,
  search: { currentTab, query, peopleFilters },
}) => ({
  userData: auth.user,
  currentTab,
  query,
  peopleFilters,
})

const mapDispatchToProps = {
  setCurrentTab,
  setRecentSearch,
  setSearchingState,
  setQuery,
}

const mapFromNavigationParam = (getParam) => ({})

class SearchScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSearching: false,
      loadMore: false,
      query: '',
      all: {
        lastQuery: null,
        people: { artist: [], people: [] },
        activity: [],
      },
      people: {
        lastQuery: null,
        data: [],
        band: [],
        loadMorePeople: false,
        loadMoreArtist: false,
      },
      activity: { lastQuery: null, data: [] },
      video: { lastQuery: null, data: [] },
      podcast: { lastQuery: null, data: [] },
      news: { lastQuery: null, data: [] },
      activityTab: null,
      peopleSuggestions: null,
      activitySuggestions: null,
      videoSuggestions: null,
      podcastSuggestions: null,
      newsSuggestions: null,
      avoidScreenUpdate: false,
      networkError: false,
    }
    this.cancelToken = null
    this._populateTab = this._populateTab.bind(this)
    this._releaseLoader = this._releaseLoader.bind(this)
    this._attachBackAction = this._attachBackAction.bind(this)
    this._releaseBackAction = this._releaseBackAction.bind(this)
    this._getSuggestion = this._getSuggestion.bind(this)
    this._onBack = this._onBack.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true
  }

  static getDerivedStateFromProps(props, state) {
    const { query } = props
    return { query }
  }

  _onBack = () => {
    this.props.setCurrentTab(null)
    this.props.setQuery('')
    this.props.navigateTo('ExploreScreen')
    return true
  };

  _attachBackAction = (e) => {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this._onBack
    )
  };

  _releaseBackAction() {
    typeof this.backHandler !== 'undefined' && this.backHandler.remove()
  }

  async _releaseLoader() {
    this.setState(
      produce((draft) => {
        draft.isSearching = false
        draft.loadMore = false
        draft.avoidScreenUpdate = false
      })
    )
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener(
      'focus',
      this._attachBackAction
    )
    this.blurListener = this.props.navigation.addListener(
      'blur',
      this._releaseBackAction
    )
    InteractionManager.runAfterInteractions(() => {
      Promise.all([
        this._getSuggestion('people'),
        this._getSuggestion('activity'),
        this._getSuggestion('video'),
        this._getSuggestion('podcast'),
        this._getSuggestion('news'),
      ]).then((results) => {
        const [
          peopleSuggestions,
          activitySuggestions,
          videoSuggestions,
          podcastSuggestions,
          newsSuggestions,
        ] = results
        this.setState(
          produce((draft) => {
            draft.peopleSuggestions = peopleSuggestions
            draft.activitySuggestions = activitySuggestions
            draft.videoSuggestions = videoSuggestions
            draft.podcastSuggestions = podcastSuggestions
            draft.newsSuggestions = newsSuggestions
          })
        )
      })
    })
    const { query } = this.props
    !!query && this._onQueryChange(query)
  }

  _getSuggestion = (type) => {
    const {
      userData: { id_user },
    } = this.props
    return new Promise((resolve, reject) => {
      getAdvancedSearch({
        type,
        action: 'suggestion',
        offset: 0,
        limit: 4,
        id_user,
      }).then(({ data: { result: suggestions } }) => {
        if (type == 'people') resolve(suggestions)
        else
          resolve(
            Object.keys(suggestions).map((type) => {
              return {
                heading: startCase(type.replace('sc_session', 'Webinar').replace('Submission', 'Audition')),
                id: type,
                data: !isEmpty(suggestions[type])
                  ? suggestions[type].slice(0, 1)
                  : [],
              }
            })
          )
      })
    })
  };

  _populateTab() {
    const { query } = this.props
    validateQuery(query) && this.search(query)
    return true
  }

  componentDidUpdate(prevProps, prevState) {
    const { query, currentTab, setSearchingState } = this.props
    const { isSearching } = this.state
    !!prevProps.query && !query && this.search(query, false, true)
    prevProps.isSearching != isSearching && setSearchingState(isSearching)
    prevProps.query != query &&
      this._onQueryChange(
        query,
        prevProps.currentTab != currentTab ? 100 : 850
      )
    !isEqualObject(prevProps.peopleFilters, this.props.peopleFilters) &&
      this.search(query, false, true)
    prevProps.currentTab != currentTab &&
      this._populateTab() &&
      this._onQueryChange(query, 100)
  }

  search(query, loadMore, forceSearch, show = null) {
    const { activityTab, all, activity, isSearching } = this.state
    const { currentTab, peopleFilters } = this.props
    const {
      userData: { id_user },
    } = this.props
    if (
      (!isSearching || !loadMore) &&
      (forceSearch ||
        (!currentTab && query != all.lastQuery) ||
        (currentTab && query != this.state[currentTab].lastQuery) ||
        loadMore)
    ) {
      this.setState(
        produce((draft) => {
          draft.loadMore = loadMore
        }),
        async () => {
          const start =
              !currentTab || !loadMore ? 0 : this.state[currentTab].data.length,
            limit =
              !currentTab || (currentTab === 'people' && !loadMore)
                ? 3
                : currentTab === 'people'
                  ? 5
                  : 10,
            type = currentTab,
            search = query || this.state.query
          try {
            this.cancelToken = CancelToken.source()
            this.requestTimeout = setTimeout(
              () =>
                this.cancelToken.cancel(
                  `Advanced search time exceed 10s, user has been offered to retry the request. Keyword : "${query}"`
                ),
              10000
            )
            const params = {
              id_user,
              search: (search || '').toLowerCase(),
              type,
              start,
              limit,
              ...(currentTab === 'people' ? peopleFilters : {}),
              ...(currentTab === 'activity' && activityTab
                ? { category: activityTab }
                : {}),
            }
            let result = {}
            if (currentTab == 'people') {
              result = pick(this.state.people, [
                'loadMorePeople',
                'loadMoreArtist',
              ])
              if (!loadMore || show == 'people') {
                const {
                  data: {
                    result: { people: peopleData },
                  },
                } = await getAdvancedSearch(
                  produce(params, (draft) => {
                    draft.show = 'people'
                  }),
                  this.cancelToken.token
                )
                result.people = peopleData
                result.loadMorePeople = peopleData.length >= limit
              }
              if (!loadMore || show == 'artist') {
                const {
                  data: {
                    result: { artist: artistData },
                  },
                } = await getAdvancedSearch(
                  produce(params, (draft) => {
                    draft.show = 'artist'
                  }),
                  this.cancelToken.token
                )
                result.artist = artistData
                result.loadMoreArtist = artistData.length >= limit
              }
            } else {
              const { data } = await getAdvancedSearch(
                params,
                this.cancelToken.token
              )
              result = data.result
            }
            if (Array.isArray(result) && result.length === 0) {
              return
            }
            const parent = currentTab || 'all'
            const parentDataKey = currentTab ? 'data' : 'activity'
            let saveToHistory = true
            if (isObject(result)) {
              switch (currentTab) {
              case 'people':
                if (isObject(result[currentTab])) {
                  saveToHistory = result[currentTab].length > 0
                  this.setState(
                    produce((draft) => {
                      draft[currentTab] = {
                        lastQuery: query,
                        loadMorePeople: result.loadMorePeople,
                        loadMoreArtist: result.loadMoreArtist,
                        [parentDataKey]: uniqWith(
                          loadMore && !!result.people
                            ? [
                              ...this.state[currentTab].data,
                              ...result[currentTab],
                            ]
                            : result[currentTab],
                          isEqual
                        ),
                        artist: uniqWith(
                          loadMore && !!result.artist
                            ? [
                              ...this.state[currentTab].artist,
                              ...result.artist,
                            ]
                            : result.artist,
                          isEqual
                        ),
                      }
                    }),
                    this._releaseLoader
                  )
                } else {
                  this._releaseLoader()
                }
                break
              default:
                // eslint-disable-next-line no-case-declarations
                const dataExplore = [
                  {
                    id: 'band',
                    heading: 'Artist Spotlight',
                    data: filterAdListing(result.band),
                  },
                  {
                    id: 'collaboration',
                    heading: 'Collaboration',
                    data: filterAdListing(result.collaboration),
                  },
                  {
                    id: 'event',
                    heading: 'Event',
                    data: filterAdListing(result.event),
                  },
                  {
                    id: 'sc_session',
                    heading: 'Webinar',
                    data: filterAdListing(result.sc_session),
                  },
                  {
                    id: 'submission',
                    heading: 'Submission',
                    data: filterAdListing(result.submission),
                  },
                  {
                    id: 'video',
                    heading: 'Video',
                    data: filterAdListing(result.video),
                  },
                  {
                    id: 'podcast',
                    heading: 'Podcast',
                    data: filterAdListing(result.sp_album),
                  },
                  {
                    id: 'news',
                    heading: 'News',
                    data: filterAdListing(result.news_update),
                  },
                ].filter((explore) => {
                  return explore.data.length > 0
                })
                saveToHistory =
                    dataExplore.length > 0 ||
                    (isObject(result.people) && !isEmpty(result.people))
                if (!currentTab) {
                  this.setState(
                    produce((draft) => {
                      draft[parent] = {
                        lastQuery: query,
                        people: {
                          people: result.people,
                          artist: result.artist,
                        },
                        activity: dataExplore,
                      }
                    }),
                    this._releaseLoader
                  )
                }
                if (!isEmpty(currentTab)) {
                  this.setState(
                    produce((draft) => {
                      draft[parent] = {
                        lastQuery: query,
                        [parentDataKey]: dataExplore.map((item) => {
                          const recentActivity =
                              activity.data[
                                activity.data
                                  .map((act) => act.id)
                                  .indexOf(item.id)
                              ]
                          const res = {
                            ...item,
                            data: uniqWith(
                              loadMore
                                ? [...recentActivity.data, ...item.data]
                                : item.data,
                              isEqual
                            ),
                          }
                          return res
                        }),
                      }
                    }),
                    this._releaseLoader
                  )
                }
                break
              }
            }
            saveToHistory && !!query && pushRecent(KEY_RECENT_SEARCH, query)
            isNumber(this.requestTimeout) && clearTimeout(this.requestTimeout)
          } catch (e) {
            !isNil(e.message) && bugsnagClient.notify(new Error(e.message))
            this.setState(
              produce((draft) => {
                draft.networkError = !isNil(e.message)
                draft.isSearching = false
              }),
              this._releaseLoader
            )
            isNumber(this.requestTimeout) && clearTimeout(this.requestTimeout)
          }
        }
      )
    }
  }

  _onQueryChange(query, timeout) {
    isObject(this.cancelToken) && this.cancelToken.cancel(null)
    isNumber(this.requestTimeout) && clearTimeout(this.requestTimeout)
    if (validateQuery(query)) {
      isNumber(this.typingTimeout) && clearTimeout(this.typingTimeout)
      this.typingTimeout = setTimeout(
        () =>
          this.setState(
            produce((draft) => {
              draft.query = query
              draft.networkError = false
              draft.avoidScreenUpdate = true
              draft.isSearching = true
            }),
            () => {
              this.search(query, false, true)
            }
          ),
        timeout ? timeout : 850
      )
    } else {
      this._releaseLoader()
    }
    return true
  }

  _renderHeader = () => {
    return <SearchBox onBack={this._onBack} />
  };

  _updateRecents = () => {
    getRecent(KEY_RECENT_SEARCH).then((recents) => {
      this.props.setRecentSearch(recents.filter((recent) => !!recent))
    })
  };

  _onLoadMorePeople = (show = 'people') => {
    this.search(this.props.query, true, true, show)
  };

  render() {
    const {
      isSearching,
      all,
      people,
      activity,
      video,
      podcast,
      news,
      peopleSuggestions,
      activitySuggestions,
      videoSuggestions,
      podcastSuggestions,
      newsSuggestions,
    } = this.state
    const {
      navigateTo,
      navigation,
      currentTab,
      query,
      peopleFilters,
    } = this.props
    const validQuery = validateQuery(query)
    const peopleFiltered =
      Object.values(peopleFilters).filter((value) => !!value).length > 0
    return (
      <Container
        scrollable={false}
        renderHeader={this._renderHeader}
        isLoading={false}
        isReady={true}
      >
        <ScrollView keyboardShouldPersistTaps='handled'>
          <VisibleView
            key={`all-${query}`}
            visible={!currentTab && validQuery && !isSearching}
          >

            <Peoples
              navigateTo={navigateTo}
              peoples={get(all, 'people.people')}
              artists={get(all, 'people.artist')}
            />
            <Activities navigation={navigation} data={get(all, 'activity')} />
          </VisibleView>
          <VisibleView
            onVisible={this._updateRecents}
            visible={!currentTab && !validQuery && !isSearching}
          >
            <Recents />
          </VisibleView>
          <VisibleView
            key={`people-${JSON.stringify(
              validQuery || peopleFiltered ? people : peopleSuggestions
            )}`}
            visible={currentTab === 'people'}
          >
            <Peoples
              loadMorePeople={get(people, 'loadMorePeople')}
              loadMoreArtist={get(people, 'loadMoreArtist')}
              onMore={this._onLoadMorePeople}
              navigateTo={navigateTo}
              peoples={
                validQuery || peopleFiltered
                  ? get(people, 'data')
                  : get(peopleSuggestions, 'people')
              }
              artists={
                validQuery || peopleFiltered
                  ? get(people, 'artist') || get(people, 'band')
                  : get(peopleSuggestions, 'artist')
              }
            />
          </VisibleView>
          <VisibleView
            key={`activity-${JSON.stringify(
              query ? activity : activitySuggestions
            )}`}
            visible={currentTab === 'activity'}
          >
            <Activities
              navigation={navigation}
              data={
                validQuery
                  ? get(activity, 'data') || activity
                  : activitySuggestions
              }
            />
          </VisibleView>
          <VisibleView
            key={`video-${JSON.stringify(query ? video : videoSuggestions)}`}
            visible={currentTab === 'video'}
          >
            <Activities
              navigation={navigation}
              data={validQuery ? get(video, 'data') || video : videoSuggestions}
            />
          </VisibleView>
          <VisibleView
            key={`podcast-${JSON.stringify(
              query ? podcast : podcastSuggestions
            )}`}
            visible={currentTab === 'podcast'}
          >
            <Activities
              navigation={navigation}
              data={
                validQuery ? get(podcast, 'data') || podcast : podcastSuggestions
              }
            />
          </VisibleView>
          <VisibleView
            key={`news-${JSON.stringify(query ? news : newsSuggestions)}`}
            visible={currentTab === 'news'}
          >
            <Activities
              navigation={navigation}
              data={validQuery ? get(news, 'data') || news : newsSuggestions}
            />
          </VisibleView>
        </ScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SearchScreen),
  mapFromNavigationParam
)
