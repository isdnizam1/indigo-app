import React, { Component } from 'react'
import { SafeAreaView, ScrollView, StatusBar } from 'react-native'
import { View } from 'react-native'
import { TeamUpSearchBox, Filters, Empty } from 'sf-components/team_up/index'
import { GUN_METAL, SHIP_GREY_CALM, REDDISH, WHITE } from 'sf-constants/Colors'
import { ItemTeamUp } from 'sf-components/team_up'
import { REDDISH_LIGHT } from 'sf-constants/Colors'
import { isEmpty, noop } from 'lodash-es'
import { connect } from 'react-redux'
import { Loader, ModalMessageView, _enhancedNavigation } from 'sf-components'
import { WP6 } from 'sf-constants/Sizes'
import { getTeamUpSearch } from 'sf-actions/api'
import PersonModal from 'sf-components/team_up/PersonModal'
import { postTeamUpInvite } from '../../actions/api'
import { Alert } from '../../components/team_up'

// import { Empty, PersonModal } from '../../components/team_up'
// import { Avatar } from '../../components'

const mapStateToProps = ({
  auth,
}) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  roleParam: getParam('roleParam', 'Hacker'),
})

const mapDispatchToProps = {
}

export class TeamUpSearch extends Component {

  constructor(props) {
    super(props)
  }

  state = {
    selectedCity: {},
    isModalInviteVisible: false,
    isModalPersonVisible: false,
    isLoading: false,
    listTeamUp: null,
    selectedPerson: {},
    searchPerson: '',
    showAlert: false,
    textError: '',
  }

  componentDidMount() {
    this._actionSearch()
  }

  _renderItemTeamUp = (selectedPerson, index) => {
    const { full_name, city_name, profile_picture, is_invited } = selectedPerson
    // console.log(selectedPerson)
    const button = [
      is_invited ? {
        text: 'Invited',
        backgroundColor: REDDISH_LIGHT,
        borderColor: REDDISH,
        onPress: noop,
        textColor: REDDISH,
      } : {
        text: 'Invite',
        backgroundColor: REDDISH,
        borderColor: REDDISH,
        onPress: () => {
          this.setState({ selectedPerson }, () => {
            this._toggleModalInvite()
          })
        },
      },
    ]
    return (
      <ItemTeamUp
        onPressItem={() => {
          this.setState({ selectedPerson }, () => this._toggleModalPerson())
        }}
        key={Math.random()}
        title={(full_name)}
        subtitle={(city_name)}
        button={button}
        profilePicture={profile_picture}
      />
    )
  }

  _onChangeCity = (selectedCity) => {
    this.setState({ selectedCity }, this._actionSearch)
  }

  _toggleModalInvite = () => {
    this.setState({ isModalInviteVisible: !this.state.isModalInviteVisible })
  }

  _toggleModalPerson = () => {
    this.setState({ isModalPersonVisible: !this.state.isModalPersonVisible })
  }

  _runAlert = (textError) => {
    this.setState({ showAlert: true, textError })
    setTimeout(() => {
      if (isEmpty(textError))
        this.props.navigateBack()
      else
        this.setState({ showAlert: false })
    }, 2000)
  }

  _renderModalInvite = () => {
    const { selectedPerson } = this.state
    const { roleParam } = this.props
    return (
      <ModalMessageView
        toggleModal={this._toggleModalInvite}
        isVisible={this.state.isModalInviteVisible}
        title={`Kamu yakin untuk mengajak\n${selectedPerson?.full_name} sebagai ${roleParam}?`}
        titleLineHeight={WP6}
        titleType='Circular'
        titleSize={'small'}
        titleColor={GUN_METAL}
        subtitle={
          `Invitation untuk Team Up dengan ${selectedPerson.full_name} akan dikirimkan setelah ini.`
        }
        subtitleType='Circular'
        subtitleSize={'xmini'}
        subtitleWeight={400}
        subtitleColor={SHIP_GREY_CALM}
        image={null}
        buttonPrimaryText={'Invite'}
        buttonPrimaryTextType='Circular'
        buttonPrimaryTextWeight={400}
        buttonPrimaryBgColor={REDDISH}
        buttonPrimaryAction={this._actionInvite}
        buttonSecondaryText={'Batalkan'}
        buttonSecondaryTextType='Circular'
        buttonSecondaryTextWeight={400}
        buttonSecondaryTextColor={SHIP_GREY_CALM}
      />
    )
  }

  _actionSearch = async () => {
    this.setState({ isLoading: true })
    const { userData: { id_user }, roleParam } = this.props
    const { selectedCity, searchPerson } = this.state
    try {
      const params = {
        id_user,
        // start: 5,
        // limit: 15,
        search: searchPerson,
        id_city: selectedCity?.id_city,
        role: roleParam
      }

      let listTeamUp = await getTeamUpSearch(params)
      listTeamUp = listTeamUp.data.result
      if (listTeamUp) {
        // console.log(listTeamUp.data.result)
        listTeamUp = listTeamUp.filter(
          (item) => item.id_user !== id_user,
        )
      }
      this.setState({ listTeamUp, isLoading: false })

    } catch (e) {
      this.setState({ isLoading: false })
    }

  }

  _actionInvite = async () => {
    const { userData: { id_user } } = this.props
    const { selectedPerson } = this.state
    try {
      const body = {
        id_user: selectedPerson?.id_user,
        invited_by: id_user
      }
      // console.log(body)
      const response = await postTeamUpInvite(body)
      // console.log(response.data)
      if (response.data.code === 200) {
        // console.log(response.data)
        this._runAlert()
      } else {
        //handle error/ another code
        this._runAlert(response.data.message)
      }
      // this._actionSearch()

    } catch (e) {
      // console.log(e)
      //
    }
  }

  _onChangeText = (searchPerson) => {
    this.setState({ searchPerson }, this._actionSearch)
  }

  _onClearText = () => {
    this.setState({ searchPerson: '' }, this._actionSearch)
  }

  render() {

    const { navigateBack, roleParam } = this.props
    const { selectedCity, textError, isLoading, listTeamUp, searchPerson, isModalPersonVisible, selectedPerson, showAlert } = this.state
    return (

      <View>
        <SafeAreaView style={{ paddingTop: StatusBar.currentHeight, backgroundColor: WHITE }}>
          <View style={{ backgroundColor: WHITE, width: '100%', height: '100%', }} >
            <TeamUpSearchBox
              onChangeText={this._onChangeText}
              text={searchPerson}
              onClearText={this._onClearText}
              onPressBack={() => navigateBack()}
            />
            <Filters
              isActive={!isEmpty(selectedCity?.city_name)}
              onChangeCity={this._onChangeCity}
              selectedCity={selectedCity?.city_name}
              roleType={roleParam}
            />
            <View style={{ flex: 1 }}>
              <ScrollView>
                {isLoading ?
                  <Loader isLoading /> :
                  isEmpty(listTeamUp) ?
                    <Empty query={searchPerson} /> :
                    listTeamUp && listTeamUp.map(this._renderItemTeamUp)}
              </ScrollView>
              {showAlert && <Alert textError={textError} />}
            </View>

          </View>
        </SafeAreaView>
        {this._renderModalInvite()}
        <PersonModal
          isVisible={isModalPersonVisible}
          onPressClose={this._toggleModalPerson}
          role={roleParam}
          selectedPerson={selectedPerson}
          onPressInvite={() => {
            this._toggleModalPerson()
            this._toggleModalInvite()
          }}
        />
      </View>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(TeamUpSearch),
  mapFromNavigationParam,
)