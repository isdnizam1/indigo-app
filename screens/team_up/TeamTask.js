import React, { Component } from 'react'
import { View, TouchableOpacity, Text, Image } from 'react-native'
import { connect } from 'react-redux'

import { _enhancedNavigation, HeaderNormal, Container } from '../../components'

import styles from './styles/TeamDiscussionStyle'

const mapStateToProps = ({
  auth
}) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({

})

const mapDispatchToProps = {
}

class TeamTask extends Component {
  constructor(props) {
    super(props)
    this.state = {
        //
    }
  }

  componentDidMount() {

  }

  render () {
    const { navigateBack } = this.props
    const isEmpty = true

    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={() => navigateBack()}
            textType={'Circular'}
            textSize={'slight'}
            text={'Team Task'}
            centered
          />
        )}
      >
          {isEmpty && (
              <View style={styles.emptyContainer}>
                  <Image style={styles.emptyIcon} source={require('../../assets/icons/ic_task.png')}/>
                  <Text style={styles.emptyTitleText}>Belum ada tugas</Text>
                  <Text style={styles.emptyDescriptionText}>Ikuti terus tugas-tugas menarik untuk pengembangan tim Startup kamu</Text>
              </View>
          )}
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(TeamTask),
  mapFromNavigationParam
)
