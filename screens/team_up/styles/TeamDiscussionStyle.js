import { Dimensions } from 'react-native'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  emptyContainer: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyIcon: {
    width: 50,
    height: 50
  },
  emptyTitleText: {
    fontFamily: 'CircularMedium',
    fontSize: 16,
    color: '#454F5B',
    marginTop: 20,
    marginBottom: 10
  },
  emptyDescriptionText: {
    fontFamily: 'CircularBook',
    fontSize: 14,
    color: '#919EAB',
    textAlign: 'center',
    marginHorizontal: 50,
    lineHeight: 20
  },
  scrollContainer: {
    flex: 1
  },
  contentContainer: {
    //
  },
  chatContainer: {
    flexDirection: 'row',
    marginHorizontal: 16,
    marginBottom: 12,
    alignItems: 'center'
  },
  dateText: {
    fontFamily: 'CircularBook',
    fontSize: 11,
    color: '#919EAB',
    marginHorizontal: 8,
    alignSelf: 'flex-end'
  },
  selfChatContainer: {
    backgroundColor: '#3287EC',
    borderRadius: 8,
    maxWidth: Dimensions.get('screen').width * 0.65
  },
  selfChatText: {
    fontFamily: 'CircularBook',
    fontSize: 14,
    lineHeight: 20,
    color: '#F9FAFB',
    textAlign: 'left',
    paddingHorizontal: 8,
    paddingVertical: 6
  },
  teamChatContainer: {
    backgroundColor: '#F4F6F8',
    borderRadius: 8,
    maxWidth: Dimensions.get('screen').width * 0.65
  },
  teamChatText: {
    fontFamily: 'CircularBook',
    fontSize: 14,
    lineHeight: 20,
    color: '#637381',
    textAlign: 'left',
    paddingHorizontal: 8,
    paddingVertical: 6
  },
  senderText: {
    fontFamily: 'CircularMedium',
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'left',
    paddingHorizontal: 8,
    paddingTop: 6
  }
})
