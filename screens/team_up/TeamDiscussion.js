import React, { Component } from 'react'
import { View, ScrollView, Text, Image } from 'react-native'
import { connect } from 'react-redux'

import ChatInput from '../../components/team_up/ChatInput'
import { _enhancedNavigation, HeaderNormal, Container } from '../../components'

import { getDiscussion, postDiscussion } from '../../actions/api'

import styles from './styles/TeamDiscussionStyle'

const mapStateToProps = ({
  auth
}) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
    active: getParam('active', true),
    id_startup_team: getParam('id_startup_team', -1)
  })

const mapDispatchToProps = {
}

let _chatRefresher = null

class TeamDiscussion extends Component {
  constructor(props) {
    super(props)
    this.state = {
        chats: []
    }
  }

  componentDidMount() {
    this._getDiscussion()

    _chatRefresher = setInterval(() => {
      this._getDiscussion()
    }, 3000)
  }

  componentWillUnmount() {
    if (_chatRefresher !== null) {
      clearInterval(_chatRefresher)
    }
  }

  _getDiscussion = () => {
    const { dispatch } = this.props

    const id_startup_team =
        this.props && this.props.route &&
            this.props.route.params &&
                this.props.route.params.id_startup_team ?
                    this.props.route.params.id_startup_team : -1

    const params = {
        id_startup_team
    }

    dispatch(getDiscussion, params)
      .then((result) => {
          console.log(result)
        this.setState({ chats: result })
        resolve(result)
      })
      .catch((e) => {
        // this.setState({ isLoading: false })
      })
  }

  _postDiscussion = (chat) => {
    const { userData, dispatch } = this.props

    const id_startup_team =
        this.props && this.props.route &&
            this.props.route.params &&
                this.props.route.params.id_startup_team ?
                    this.props.route.params.id_startup_team : -1

    const params = {
        id_user: userData.id_user,
        id_startup_team,
        chat
    }

    dispatch(postDiscussion, params)
      .then((result) => {
          this._getDiscussion()
        resolve(result)
      })
      .catch((e) => {
        // this.setState({ isLoading: false })
      })
  }

  _renderSelfChat = (data) => {
    return (
        <View style={{ ...styles.chatContainer, alignSelf: 'flex-end' }}>
            <Text style={styles.dateText}>{data.created_at.slice(11, 16)}</Text>
            <View style={styles.selfChatContainer}>
                <Text style={styles.selfChatText}>{data.comment}</Text>
            </View>
        </View>
    )
  }

  _renderTeamChat = (data) => {
    return (
        <View style={{ ...styles.chatContainer, alignSelf: 'flex-start' }}>
            <View style={styles.teamChatContainer}>
                <Text style={{
                    ...styles.senderText,
                    color:
                        data.role === 'Hustler' ? '#2b9ee5' :
                        data.role === 'Hipster' ? '#E5422B' : '#50B83C'
                }}
                >{data.full_name}</Text>
                <Text style={styles.teamChatText}>{data.comment}</Text>
            </View>
            <Text style={styles.dateText}>{data.created_at.slice(11, 16)}</Text>
        </View>
    )
  }

  render () {
    const { navigateBack, userData } = this.props
    const { chats } = this.state
    const active = this.props && this.props.route && this.props.route.params && this.props.route.params.active

    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={() => navigateBack()}
            textType={'Circular'}
            textSize={'slight'}
            text={'Team Discussion'}
            centered
          />
        )}
      >
          {!active && (
              <View style={styles.emptyContainer}>
                  <Image style={styles.emptyIcon} source={require('../../assets/icons/ic_discussion.png')}/>
                  <Text style={styles.emptyTitleText}>Anda belum melakukan Team Up</Text>
                  <Text style={styles.emptyDescriptionText}>Segera Team Up dengan founder lain untuk bisa melakukan diskusi disini</Text>
              </View>
          )}
          {active && (
              <View style={{ flex: 1 }}>
                  <ScrollView>
                      <View style={styles.contentContainer}>
                        {chats.map((data, index) => {
                            if (data.id_user === userData.id_user) {
                                return this._renderSelfChat(data)
                            } else {
                                return this._renderTeamChat(data)
                            }
                        })}
                      </View>
                  </ScrollView>
                  <ChatInput
                    placeholder={'Enter a message'}
                    onSend={(text) => {
                        this._postDiscussion(text)
                    }}
                  />
              </View>
          )}
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(TeamDiscussion),
  mapFromNavigationParam
)
