import React, { Component } from 'react'
import { View, TouchableOpacity, ImageBackground, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { _enhancedNavigation, HeaderNormal, Container, Text, Avatar, Image, ModalMessageView } from '../../components'
import { WP100, WP5, HP100, HP05, HP1, WP20, WP25, WP6, WP2, WP4, WP15, WP3, WP1, WP205, WP105, WP10, WP80, WP8, WP305, WP7, WP12, HP80, HP20, HP10, HP5, HP7 } from '../../constants/Sizes'
import { WHITE, NAVY_DARK, PALE_BLUE, GREY_CALM_SEMI, GUN_METAL, SHIP_GREY_CALM, PALE_GREY_THREE, REDDISH, REDDISH_DISABLED, TRANSPARENT, PALE_GREY } from '../../constants/Colors'
import style from '../startup_profile/v2/ProfileScreen/TabDetail/style'
import { getTeamUpInfo, postLockRole } from '../../actions/api'
import Spacer from '../../components/Spacer'
import { TOUCH_OPACITY } from '../../constants/Styles'

const mapStateToProps = ({
  auth
}) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({

})

const mapDispatchToProps = {
}

class TeamUpScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      teamUpInfo: null,
      showModalInfoRole: false,
      confirmModal: false,
      buttonType: '',
      role: ''
    }
    this._didFocusSubscription = props.navigation.addListener('focus', () => {
      this.componentDidMount()
    }
    )
  }

  componentDidMount() {
    this._getTeamUpInfo()
  }

  _didFocusSubscription;
  
  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
  }

  _toggleModalInfoRole = () => {
    this.setState({
      showModalInfoRole: !this.state.showModalInfoRole
    })
  }

  _toggleConfirmModal = (type, role) => {
    this.setState({
      confirmModal: !this.state.confirmModal,
      buttonType: type,
      role
    })
  }

  _getTeamUpInfo = () => {
    const { userData, dispatch } = this.props
    const params = { id_user: userData.id_user }
    dispatch(getTeamUpInfo, params)
      .then((result) => {
        this.setState({ teamUpInfo: result })
        // console.log(result)
        resolve(result)
      })
      .catch((e) => {
        // this.setState({ isLoading: false })
      })
  }

  _postLockRole = () => {
    const { userData, dispatch, navigateTo } = this.props
    const params = { id_user: userData.id_user }
    const { buttonType } = this.state
    dispatch(postLockRole, params)
      .then((result) => {
        this._getTeamUpInfo()
        if (buttonType === 'invitation') {
          navigateTo('TeamUpInvitation')
        } else {
          navigateTo('TeamUpSearch', { roleParam: this.state.role })
        }
      })
      .catch((e) => {
        // this.setState({ isLoading: false })
      })
    // this.componentDidMount()
  }

  _renderButtonInvitationList = () => {
    const { navigateTo } = this.props
    const { teamUpInfo } = this.state
    return (
      <TouchableOpacity
        style={{ justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'row' }}
        onPress={teamUpInfo?.user.is_locked_role ? () => this.props.navigateTo('TeamUpInvitation') : () => this._toggleConfirmModal('invitation', '')}
      >
        <View>
          <Image size={'massive'} source={require('sf-assets/icons/invitationList.png')} />
        </View>
        <View>
          <Text
            size={'small'}
            color={'#454F5B'}
            type={'Circular'}
            weight={700}
          >
            {`Invitation List (${teamUpInfo?.totalInvitation})`}
          </Text>
          <Text
            size={'mini'}
            color={'#919EAB'}
            type={'Circular'}
            weight={400}
          >
            {'Cek undangan dari founder lain'}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  _renderTeam = () => {
    const { teamUpInfo } = this.state
    let finalTeamData = []
    if (teamUpInfo?.user.role === null) {
      finalTeamData = [
        {
          ...teamUpInfo?.user
        },
        // {
        //   "id_user": "0",
        //   "role": 'Undang Hipster ke tim kamu',
        //   "is_locked_role": false,
        //   "full_name": "Hipster",
        //   "profile_picture": null,
        // },
        // {
        //   "id_user": "0",
        //   "role": 'Undang Hacker ke tim kamu',
        //   "is_locked_role": false,
        //   "full_name": "Hacker",
        //   "profile_picture": null,
        // },
      ]
    } else {
      const indexHustler = teamUpInfo?.teamMember.findIndex((x) => x.role === 'Hustler')
      const indexHipster = teamUpInfo?.teamMember.findIndex((x) => x.role === 'Hipster')
      const indexHacker = teamUpInfo?.teamMember.findIndex((x) => x.role === 'Hacker')

      if (indexHustler !== -1) {
        finalTeamData.push({ ...teamUpInfo?.teamMember[indexHustler] })
      }
      if (indexHustler === -1) {
        finalTeamData.push({
          'id_user': '0',
          'role': 'Undang Hustler ke tim kamu',
          'is_locked_role': false,
          'full_name': 'Hustler',
          'profile_picture': null,
        })
      }
      if (indexHipster !== -1) {
        finalTeamData.push({ ...teamUpInfo?.teamMember[indexHipster] })
      }
      if (indexHipster === -1) {
        finalTeamData.push({
          'id_user': '0',
          'role': 'Undang Hipster ke tim kamu',
          'is_locked_role': false,
          'full_name': 'Hipster',
          'profile_picture': null,
        })
      }
      if (indexHacker !== -1) {
        finalTeamData.push({ ...teamUpInfo?.teamMember[indexHacker] })
      }
      if (indexHacker === -1) {
        finalTeamData.push({
          'id_user': '0',
          'role': 'Undang Hacker ke tim kamu',
          'is_locked_role': false,
          'full_name': 'Hacker',
          'profile_picture': null,
        })
      }
    }
    const indexUser = finalTeamData.findIndex((x) => x.id_user === teamUpInfo?.user.id_user)
    finalTeamData.splice(0, 0, finalTeamData.splice(indexUser, 1)[0])
    return (
      <FlatList
        data={finalTeamData}
        scrollEnabled={false}
        renderItem={this._renderItem}
      />
    )
  }

  _renderItem = ({ item, index }) => {
    const { teamUpInfo } = this.state
    return (
      <View style={style.contentWithPadding}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <View style={style.teamItem}>
            {item.profile_picture !== null ? <Avatar
              noPlaceholder={false}
              shadow={false}
              size='regular'
              imageStyle={[{ borderWidth: 1, borderColor: GREY_CALM_SEMI }]}
              image={item.profile_picture}
            />
              : <Image
                size={'regular'}
                source={require('../../assets/icons/addTeam.png')}
              />}
            <View style={style.teamInfo}>
              <Text
                size={'small'}
                color={GUN_METAL}
                type={'Circular'}
                weight={500}
              >
                {item.id_user === teamUpInfo?.user.id_user ? 'You' : item.full_name}
              </Text>
              {item.role !== null && <Text
                size={'mini'}
                color={SHIP_GREY_CALM}
                type={'Circular'}
                weight={400}
              >
                {item.role}
              </Text>}
            </View>
          </View>
          <View>
            {item.id_user !== teamUpInfo?.user.id_user && item.id_user === '0' ? <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onPress={teamUpInfo?.user.is_locked_role ? () => {
                this.setState({ role: item.full_name }, () => {
                  this.props.navigateTo('TeamUpSearch', { roleParam: this.state.role })

                })
              } : () => this._toggleConfirmModal('invite', item.full_name)}
            >
              <View style={{
                alignItems: 'center',
                justifyContent: 'center',
                paddingHorizontal: WP3,
                paddingVertical: WP1,
                borderRadius: 5,
                alignSelf: 'center',
                backgroundColor: teamUpInfo?.user.role !== null ? REDDISH : REDDISH_DISABLED,
                borderWidth: 1,
                borderColor: TRANSPARENT,
                marginLeft: WP1,
                borderStyle: 'solid'
              }}
              >
                <Text
                  type='Circular'
                  size={'xmini'}
                  // lineHeight={10}
                  weight={400}
                  color={WHITE}
                >
                  {'Invite'}
                </Text>
              </View>
            </TouchableOpacity>
              : teamUpInfo?.user.role === null && <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() => this.props.navigateTo('StartUpProfileForm', {
                  onSuccessEditProfile: () => this._getTeamUpInfo()
                })}
              >
                <Text
                  type='Circular'
                  size={'xmini'}
                  // lineHeight={10}
                  weight={400}
                  color={REDDISH}
                >
                  {'Isi Role'}
                </Text>

              </TouchableOpacity>}
          </View>
        </View>
      </View>
    )
  };

  _renderTaskButton = () => {
    return (
      <TouchableOpacity onPress={() => {
        this.props.navigateTo('TeamTask')
      }}
      >
        <ImageBackground
          source={require('../../assets/images/bgTeamTask.png')}
          style={{
            height: WP25,
            width: '100%',
            resizeMode: 'cover',
            elevation: 2,
          }}
          imageStyle={{ borderRadius: WP3 }}
        >
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingHorizontal: WP4,
              width: '100%',
              height: WP25,
            }}
          >
            <View style={{ flex: 1, paddingLeft: WP4, justifyContent: 'center' }}>
              <Text color={WHITE} size={'medium'} weight={700}>
                Team Task
              </Text>
              <Text color={PALE_GREY_THREE} size={'xmini'}>
                Kembangkan team kamu dengan berbagai tugas!
              </Text>
              <Spacer size={WP1} />
            </View>
            <View style={{ paddingHorizontal: WP4 }}>
              <Image
                size={'xmini'}
                source={require('../../assets/icons/arrowWhiteTransparent.png')}
              />
              {false && <View
                style={{
                  width: WP205,
                  height: WP205,
                  backgroundColor: '#FF1515',
                  borderRadius: WP105,
                  position: 'absolute',
                  right: WP3,
                  top: -WP1,
                }}
              />}
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    )
  }

  render() {
    const { userData, navigateBack } = this.props
    const { teamUpInfo } = this.state
    const userRoleText = teamUpInfo?.user.role === null ? 'Kamu belum memilih\nRole kamu!' : `Kamu adalah ${teamUpInfo?.user.role}!`
    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={() => navigateBack()}
            textType={'Circular'}
            textSize={'slight'}
            text={'Team Up'}
            centered
          />
        )}
      >
        <ImageBackground
          source={
            require('sf-assets/images/bgStartup.png')
          }
          style={{ width: WP100 }}
        >
          <View
            style={{
              width: WP100,
              height: 202,
              alignItems: 'flex-start',
              justifyContent: 'center',
              padding: WP5
            }}
          >
            <Text size={'huge'} color={WHITE} type={'Circular'} weight={700}>
              {`Hello, ${teamUpInfo?.user.full_name}\n${userRoleText}`}
            </Text>
            <TouchableOpacity onPress={() => this._toggleModalInfoRole()} style={{ flexDirection: 'row' }}>
              <Image
                size={'tiny'}
                source={require('../../assets/icons/clarityMapWhite.png')}
              />
              <Spacer size={WP2} horizontal />
              <Text
                size={'small'}
                color={'#BDBDBD'}
                type={'Circular'}
                weight={400}
                style={{ marginTop: HP05 }}
              >
                {'Pelajari tentang pengertian role'}
              </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
        <View>
          <View style={{ marginTop: 15, marginLeft: 15 }}>
            <Text size={'medium'} color={NAVY_DARK} type={'Circular'} weight={600}>
              {'Your Triangle Team'}
            </Text>
          </View>
          {this._renderButtonInvitationList()}
          <View
            style={{
              height: 1,
              backgroundColor: PALE_BLUE,
              marginBottom: WP4,
              marginHorizontal: WP4,
            }}
          />
          {this._renderTeam()}
          {this._renderTaskButton()}
        </View>
        <ModalMessageView
        // aspectRatio={2245 / 1587}
          fullImage
          toggleModal={this._toggleModalInfoRole}
          style={{ backgroundColor: TRANSPARENT  }}
          imageStyle={{ width: '80%', height: '85%', resizeMode: 'contain' }}
          isVisible={this.state.showModalInfoRole}
          image={{uri: `https://apidev.soundfren.id/assets/img/role-info-grafis.png`}}
          buttonPrimaryAction={this._toggleModalInfoRole}
          titleSize={'massive'}
          buttonPrimaryTextWeight={500}
          buttonPrimaryTextSize={'small'}
          buttonPrimaryBgColor={REDDISH}
          buttonPrimaryStyle={{
            marginTop: -HP7
          }}
          buttonPrimaryText={'Baik, saya paham'}
        />
        <ModalMessageView
          style={{ width: WP100 - WP8 }}
          contentStyle={{ paddingHorizontal: WP4 }}
          toggleModal={() => this.setState({ confirmModal: false })}
          isVisible={this.state.confirmModal}
          title={'Apa kamu yakin dengan Role kamu sekarang?'}
          titleType='Circular'
          titleSize={'small'}
          titleColor={GUN_METAL}
          titleStyle={{ marginBottom: WP4 }}
          subtitle={
            'Kamu tidak akan bisa mengubah Role kamu setelah ini, tekan “Lanjutkan” jika kamu yakin'
          }
          subtitleType='Circular'
          subtitleSize={'xmini'}
          subtitleWeight={400}
          subtitleColor={SHIP_GREY_CALM}
          subtitleStyle={{ marginBottom: WP3 }}
          image={null}
          buttonPrimaryText={'Lanjutkan'}
          buttonPrimaryContentStyle={{ borderRadius: 8, paddingVertical: WP305 }}
          buttonPrimaryTextType='Circular'
          buttonPrimaryTextWeight={400}
          buttonPrimaryBgColor={REDDISH}
          buttonPrimaryAction={() => {
            // this.state.buttonType === 'invitation'
            //   ? this.props.navigateTo('TeamUpInvitation')
            //   : this._postLockRole()
            this._postLockRole()
            this.setState({ confirmModal: false })
          }}
          buttonSecondaryText={'Batalkan'}
          buttonSecondaryStyle={{
            backgroundColor: PALE_GREY,
            marginTop: WP1,
            borderRadius: 8,
            paddingVertical: WP305,
          }}
          buttonSecondaryTextType='Circular'
          buttonSecondaryTextWeight={400}
          buttonSecondaryTextColor={SHIP_GREY_CALM}
          buttonSecondaryAction={() => this.setState({ confirmModal: false })}
        />
        <TouchableOpacity
          onPress={() => {
            const indexSelf = teamUpInfo?.teamMember.findIndex((x) => x.id_user === userData.id_user)
            let id_startup_team = teamUpInfo.teamMember[indexSelf].id_startup_team

            this.props.navigateTo('TeamDiscussion', { active: !!(teamUpInfo && teamUpInfo.teamMember && teamUpInfo.teamMember.length >= 3), id_startup_team })
          }}
          style={{
            height: WP12,
            width: WP12,
            borderRadius: WP6,
            backgroundColor: REDDISH,
            position: 'absolute',
            right: WP5,
            bottom: WP5,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Image
            size={'xmini'}
            source={require('../../assets/icons/whiteDiscussion.png')}
          />
        </TouchableOpacity>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(TeamUpScreen),
  mapFromNavigationParam
)
