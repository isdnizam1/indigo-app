import { LinearGradient } from 'expo-linear-gradient'
import { isEmpty, noop } from 'lodash-es'
import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { getTeamUpInvitation, postTeamUpStatus } from '../../actions/api'
import { Container, HeaderNormal, ModalMessageView, Text, _enhancedNavigation } from '../../components'
import Spacer from '../../components/Spacer'
import { ItemTeamUp, PersonModal, Empty } from '../../components/team_up/index'
import { GUN_METAL, PALE_GREY, REDDISH, SHADOW_GRADIENT, SHIP_GREY, SHIP_GREY_CALM, TRANSPARENT } from '../../constants/Colors'
import { WP2, WP4, WP6 } from '../../constants/Sizes'
import { HEADER } from '../../constants/Styles'

const mapStateToProps = ({
  auth
}) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({

})

const mapDispatchToProps = {
}

class TeamUpInvitation extends Component {

  state = {
    selectedPerson: {},
    isModalPersonVisible: false,
    isModalErrorVisible: false,
    listInvitation: null,

  }

  componentDidMount() {
    this._getInvitationList()
  }
  _getInvitationList = async () => {
    this.setState({ isLoading: true })
    const { userData: { id_user } } = this.props
    try {
      const params = {
        id_user,
      }
  
      const listInvitation = await getTeamUpInvitation(params)
      // console.log(listInvitation.data)
      if (listInvitation) {
        // console.log(listInvitation.data.result.invitations[0])
      }
      this.setState({ listInvitation: listInvitation.data.result.invitations, isLoading: false })

    } catch (e) {
      this.setState({ isLoading: false })
    }

  }

  _actionAcceptReject = async ({ isAccept = false }) => {
    const { userData: { id_user }, navigateBack } = this.props
    const { selectedPerson } = this.state
    try {
      const body = {
        id_user,
        id_startup_team: selectedPerson?.id_startup_team,
        status: isAccept? 'accepted' : 'rejected'
      }
      
      // console.log(id_user)
      // console.log(body)
      const response = await postTeamUpStatus(body)
      // console.log(response.data)
      if (response.data.code === 200) {
        // console.log(response.data)
        // this._runAlert()
        // navigateBack()
      } else {
        //handle error/ another code
        // console.log(response.data)
        this._toggleModalErrorVisible()
        // this._runAlert(`${selectedPerson.full_name} sudah memiliki tim`)
      }
      this.componentDidMount()
      // this._actionSearch()

    } catch (e) {
      // console.log(e)
      //
    }
  }

  _toggleModalErrorVisible = () => {
    this.setState({ isModalErrorVisible: !this.state.isModalErrorVisible })
  }

  _renderModalAccept = () => {
    const { isModalErrorVisible } = this.state
    return (
      <ModalMessageView
        toggleModal={this._toggleModalErrorVisible}
        isVisible={isModalErrorVisible}
        title={'Uups, Sorry'}
        titleLineHeight={WP6}
        titleType='Circular'
        titleSize={'small'}
        titleColor={GUN_METAL}
        subtitle={'Maaf, Invitation ini telah expired. Role kamu tidak sesuai dengan Invitation.'}
        subtitleType='Circular'
        subtitleSize={'xmini'}
        subtitleWeight={400}
        subtitleColor={SHIP_GREY_CALM}
        image={null}
        buttonPrimaryText={'Oke, saya mengerti'}
        buttonPrimaryTextType='Circular'
        buttonPrimaryTextWeight={400}
        buttonPrimaryBgColor={REDDISH}
        buttonPrimaryAction={noop}
      />
    )
  }

  _renderItemTeamUp = (selectedPerson, index) => {
    const { full_name, city_name, profile_picture, role, province_name } = selectedPerson
    // console.log(selectedPerson)
    const button = [
      {
        text: 'Accept',
        backgroundColor: REDDISH,
        borderColor: REDDISH,
        onPress: () => {
          this.setState({ selectedPerson }, () => {
            this._actionAcceptReject({ isAccept: true })
          })
          // this._toggleModalErrorVisible()
        },
      },
      {
        text: 'Reject',
        backgroundColor: PALE_GREY,
        borderColor: TRANSPARENT,
        onPress: () => {
          this.setState({ selectedPerson }, () => {
            this._actionAcceptReject({ isAccept: false })
          })
          // this._toggleModalErrorVisible()
        },
        textColor: SHIP_GREY_CALM,
      },
    ]
    return (
      <ItemTeamUp
        onPressItem={() => {
          this.setState({ selectedPerson }, () => this._toggleModalPerson())
        }}
        key={Math.random()}
        title={(full_name)}
        subtitle={role}
        extraSubtitle={city_name}
        button={button}
        profilePicture={profile_picture}
      />
    )
  }

  _toggleModalPerson = () => {
    this.setState({ isModalPersonVisible: !this.state.isModalPersonVisible })
  }

  render() {
    const { selectedPerson, isModalPersonVisible, isLoading, listInvitation } = this.state
    return (
      <Container
        isReady={!isLoading}
        isLoading={isLoading}
        renderHeader={() => (
          <>
            <HeaderNormal
              iconLeftOnPress={() => this.props.navigateBack()}
              textType={'Circular'}
              textSize={'slight'}
              text={'Invitation List'}
              centered
            />
            <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
          </>
        )}
        scrollable={true}
      >
        <View
          style={{ backgroundColor: null, height: '100%' }}
        >
          <Spacer size={WP2} />
          <Text size={'medium'} color={SHIP_GREY} type={'Circular'} weight={600} style={{ paddingHorizontal: WP4 }}>
            {'Bangun Startup dengan mereka'}
          </Text>
          {isEmpty(listInvitation) ?
            <Empty isInvitation={true} />
            : listInvitation.map(this._renderItemTeamUp)}
        </View>
        <PersonModal
          isVisible={isModalPersonVisible}
          onPressClose={this._toggleModalPerson}
          role={selectedPerson?.role}
          selectedPerson={selectedPerson}
          name={selectedPerson?.full_name}
          city={selectedPerson?.city_name}
          province={selectedPerson?.province_name}
          image={selectedPerson?.profile_picture}
          followers={selectedPerson?.followers}
          followings={selectedPerson?.followings}
          description={selectedPerson?.about_me}

          // onPressInvite={}
        />
        {this._renderModalAccept()}

      </Container >
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(TeamUpInvitation),
  mapFromNavigationParam
)
