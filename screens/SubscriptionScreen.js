import React, { Component } from 'react'
import { find, map, noop, toLower, upperFirst } from 'lodash-es'
import { connect } from 'react-redux'
import {
  Alert,
  Dimensions,
  ImageBackground,
  Platform,
  TouchableOpacity,
  View,
} from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import moment from 'moment'
import { Button, Container, LinearGradient, Text } from '../components'
import { BLACK, GREY, GREY20, PURPLE, WHITE } from '../constants/Colors'
import { getSettingDetail, getSubscriptionDetail } from '../actions/api'
import _enhancedNavigation from '../navigation/_enhancedNavigation'
import { DEFAULT_PAGING } from '../constants/Routes'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../constants/Styles'
import { HP1, HP15, HP2, HP3, WP2, WP4, WP5 } from '../constants/Sizes'
import { initiateRoom, NavigateToInternalBrowser } from '../utils/helper'
import HeaderNormal from '../components/HeaderNormal'

const { width: viewportWidth } = Dimensions.get('window')

const IS_IOS = Platform.OS === 'ios'

function wp(percentage) {
  const value = (percentage * viewportWidth) / 100
  return Math.round(value)
}

const slideWidth = wp(80)
const itemHorizontalMargin = wp(2)

export const sliderWidth = viewportWidth
export const itemWidth = slideWidth + itemHorizontalMargin * 2

const mapUrlBenefit = async (benefit, navigateTo, userFrom, userTo) => {
  const { internal_link, internal_browser, url, title } = benefit
  if (internal_link) {
    switch (url) {
      case 'messaging':
        // eslint-disable-next-line no-case-declarations
        const chatRoomDetail = await initiateRoom(userFrom, userTo)
        await navigateTo('ChatRoomScreen', chatRoomDetail)
        break
      case 'promote_artist':
        navigateTo('PromoteBandScreen')
        break
      case 'promote_collab':
        navigateTo('Collaboration')
        break
      case 'popup':
        Alert.alert('Stay Tuned', 'We will inform this feature to you soon.')
    }
  } else if (internal_browser) {
    navigateTo('BrowserScreenNoTab', {
      title,
      url,
    })
  } else {
    NavigateToInternalBrowser({
      url,
    })
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({})

class SubscriptionScreen extends Component {
  state = {
    subscriptionDetail: {
      benefit: [],
    },
    isReady: false,
    activeSlider: 0,
    packages: [],
  };

  async componentDidMount() {
    await this._getInitialScreenData()
  }

  _getInitialScreenData = async (...args) => {
    await this._getSubscriptionDetail(...args)
    await this._getPackages(...args)
    this.setState({ isReady: true })
  };

  _getPackages = async (
    params = DEFAULT_PAGING,
    loadMore = false,
    isLoading = true,
  ) => {
    const { dispatch } = this.props
    //getting data
    const dataResponse = await dispatch(
      getSettingDetail,
      {
        setting_name: 'premium_member',
        key_name: 'package',
      },
      noop,
      false,
      isLoading,
    )
    const packages = JSON.parse(dataResponse.value)
    this.setState({
      packages,
    })
  };

  _getTextBanefit = (plan) => {
    const { subscriptionDetail } = this.state
    let text = ''
    map(subscriptionDetail.benefit, (benefit, i) => {
      text += i == 0 ? benefit.title : ` • ${benefit.title}`
      if (i == 0 && toLower(plan) == toLower('Pro')) {
        text += ' (With Online Ads!)'
      }
    })
    return text
  };

  _renderItem = ({ item, index }) => {
    return (
      <LinearGradient style={{ padding: HP2 }}>
        <View style={{ justifyContent: 'center', paddingVertical: HP1 }}>
          <Text type='SansPro' size='large' weight={500} centered color={WHITE}>
            {upperFirst(item.package_name)} Membership
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            paddingHorizontal: WP4,
            marginBottom: HP1,
          }}
        >
          <Text size='mini' centered color={WHITE}>
            {this._getTextBanefit(item.package_name)}
          </Text>
        </View>
      </LinearGradient>
    )
  };

  _getSubscriptionDetail = async (
    params = DEFAULT_PAGING,
    loadMore = false,
    isLoading = true,
  ) => {
    const { userData, dispatch } = this.props

    const dataResponse = await dispatch(
      getSubscriptionDetail,
      { id_user: userData.id_user },
      noop,
      loadMore,
      isLoading,
    )
    this.setState({
      subscriptionDetail: dataResponse,
    })
  };

  _onPressRenew = (packages, subscriptionDetail) => () => {
    const { navigateTo } = this.props
    const currentPackage = find(
      packages,
      (packageDetail) =>
        toLower(packageDetail.package_name) ===
        toLower(subscriptionDetail.package_type),
    )
    navigateTo('PromoteUser3', {
      paymentStatus: 'renew',
      selectedPackage: currentPackage,
      startDate: moment(subscriptionDetail.valid_until).format('DD/MM/YYYY'),
    })
  };

  _onPressDetailBenefit = (sub) => async () => {
    const { navigateTo, userData } = this.props
    await mapUrlBenefit(sub, navigateTo, userData, { id_user: 960 })
  };

  render() {
    const { userData, navigateTo, navigateBack, isLoading } = this.props

    const { isReady, packages, subscriptionDetail, activeSlider } = this.state
    return (
      <Container
        scrollable
        isReady={isReady}
        isLoading={isLoading}
        borderedHeader
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text='Premium Member Benefit'
            centered
          />
        )}
      >
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          disabled={IS_IOS}
          onPress={this._onPressRenew(packages, subscriptionDetail)}
          style={{
            borderColor: GREY,
            borderWidth: 1,
            marginHorizontal: WP4,
            marginTop: WP4,
            marginBottom: WP2,
            paddingHorizontal: WP4,
            paddingTop: WP4,
            borderRadius: 4,
            paddingBottom: IS_IOS ? WP4 : 0,
          }}
        >
          <Text
            centered={IS_IOS}
            size='large'
            type='SansPro'
            style={{ marginBottom: WP4 }}
            color={PURPLE}
            weight={600}
          >
            {upperFirst(toLower(subscriptionDetail.package_type))} Membership
          </Text>
          <Text size='mini' centered={IS_IOS}>
            Valid until :
            <Text size='mini' weight={500}>
              {' '}
              {moment(subscriptionDetail.valid_until).format('DD MMMM YYYY')}
            </Text>
          </Text>
          {!IS_IOS && (
            <Button
              onPress={this._onPressRenew(packages, subscriptionDetail)}
              style={{ alignSelf: 'flex-end', alignItems: 'flex-end' }}
              disable={isLoading}
              soundfren
              compact
              centered
              rounded
              shadow='none'
              textColor={WHITE}
              textSize='mini'
              textWeight={500}
              text='Renew Membership!'
            />
          )}
        </TouchableOpacity>

        <Text
          style={{ marginVertical: HP2 }}
          centered
          type='SansPro'
          weight={500}
          size='large'
        >
          Try Our Premium Membership
        </Text>

        <Carousel
          onSnapToItem={(i) => this.setState({ activeSlider: i })}
          data={packages}
          firstItem={activeSlider}
          contentContainerCustomStyle={{ alignItems: 'center' }}
          renderItem={this._renderItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
        />
        <Pagination
          dotsLength={packages.length}
          activeDotIndex={activeSlider}
          dotColor={BLACK}
          inactiveDotColor={GREY}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />

        {IS_IOS && (
          <Text
            style={{ marginBottom: HP2, marginHorizontal: WP5 }}
            centered
            size='mini'
          >
            Unfortunately, you cannot upgrade your membership directly on our apps.
            But, you can upgrade it by send a message to administrator.
          </Text>
        )}

        <Text
          style={{ marginVertical: HP2 }}
          centered
          type='SansPro'
          weight={500}
          size='large'
        >
          Enjoy Your Benefit!
        </Text>

        {map(subscriptionDetail.benefit, (sub, i) => (
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            onPress={this._onPressDetailBenefit(sub)}
          >
            <ImageBackground
              onPress={async () => {
                await mapUrlBenefit(sub, navigateTo, userData, { id_user: 1 })
              }}
              key={`benefit${i}`}
              style={[
                {
                  padding: WP5,
                  borderRadius: 4,
                  marginHorizontal: WP4,
                  marginVertical: WP2,
                  minHeight: HP15,
                  justifyContent: 'space-between',
                },
                SHADOW_STYLE['shadowThin'],
              ]}
              imageStyle={{ borderRadius: 4 }}
              source={{ uri: sub.background_img }}
            >
              <Text type='SansPro' weight={500} size='medium' color={WHITE}>
                {sub.title}
              </Text>
              <Button
                onPress={this._onPressDetailBenefit(sub)}
                compact='flex-end'
                style={{ marginVertical: 0, marginTop: HP3, alignSelf: 'flex-end' }}
                colors={[WHITE, GREY20]}
                start={[0, 0]}
                end={[0, 1]}
                rounded
                centered
                width='40%'
                text='Try now'
                textType='SansPro'
                textSize='mini'
                textColor={PURPLE}
                textWeight={500}
                shadow='none'
              />
            </ImageBackground>
          </TouchableOpacity>
        ))}
        {!IS_IOS && toLower(subscriptionDetail.package_type) === 'basic' && (
          <View style={{ paddingHorizontal: WP4 }}>
            <Button
              onPress={() => {
                const currentPackage = find(
                  packages,
                  (packageDetail) => toLower(packageDetail.package_name) === 'pro',
                )
                navigateTo('PromoteUser3', {
                  paymentStatus: 'upgrade',
                  selectedPackage: currentPackage,
                  startDate: moment().format('DD/MM/YYYY'),
                })
              }}
              disable={isLoading}
              width='100%'
              soundfren
              centered
              rounded
              shadow='none'
              textColor={WHITE}
              text='Upgrade Your Plan!'
            />
          </View>
        )}
      </Container>
    )
  }
}

SubscriptionScreen.propTypes = {}

SubscriptionScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(SubscriptionScreen),
  mapFromNavigationParam,
)
