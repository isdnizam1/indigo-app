import React from 'react'
import { connect } from 'react-redux'
import { Clipboard, Share, TouchableWithoutFeedback, View } from 'react-native'
import moment from 'moment'
import { _enhancedNavigation, Button, Container, HeaderNormal, Image, Text } from '../components'
import { BLUE_LIGHT, GREY, GREY_CALM_SEMI, GREY_DARK, GREY_WARM, ICE_BLUE, WHITE } from '../constants/Colors'
import { WP10, WP105, WP12, WP2, WP205, WP3, WP4, WP6, WP70, WP8 } from '../constants/Sizes'
import env from '../utils/env'
import InviteFriendTnC from '../components/InviteFriendTnC'

export const isAfterFebruary2021 = () => moment().isSameOrAfter('2021-03-01')

const getHeading = () => {
  if (isAfterFebruary2021()) {
    return {
      heading: 'Ajak temanmu bergabung di Soundfren dan dapatkan hadiahnya',
      subheading: 'Dapatkan paket Premium Basic dari Soundfren selama 1 bulan gratis untuk setiap 5 teman yang berhasil kamu ajak bergabung di aplikasi Soundfren.'
    }
  }

  return {
    heading: 'Dapatkan hadiah menarik tiap mengajak temanmu!',
    subheading: 'Ajak 5 teman untuk mendapatkan paket Premium membership selama 1 bulan, 10 teman untuk mendapatkan voucher discount 50% Music Webinar, dan 25 teman untuk mendapatkan 1 Tiket Music Webinar gratis selama periode bulan February 2021.'
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = () => ({})

class InviteFriendsScreen extends React.Component {

  _backHandler = async () => {
    this.props.navigateBack()
  }

  _copyReferral = async () => {
    const { userData } = this.props
    await Clipboard.setString(userData.referral_code)
    alert(`${userData.referral_code} Copied!`)
  }

  _shareReferral = async () => {
    try {
      const { userData } = this.props
      await Share.share({
        title: 'Share your Referral Code!',
        message: `Hi Fren, sudah saatnya kamu harus gabung dengan teman-teman profesional musik di Soundfren. Kita bisa berbagi pengalaman bermusik hingga berkolaborasi!\n\nAyo gabung Soundfren Sekarang!\n${env.webUrl}/authentication/login?referral_code=${userData.referral_code}`
      })
    } catch (error) {
      alert(error.message)
    }
  }

  render() {
    const { userData } = this.props
    const heading = getHeading() || {}

    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftSize='small'
            iconLeftOnPress={this._backHandler}
            text='Invite Friends'
          />
        )}
        scrollable
        bounces={false}
      >
        <Image
          source={require('../assets/images/promoteArtist.png')}
          style={{ marginTop: WP12, marginBottom: WP6 }}
          imageStyle={{ width: WP70 }}
          aspectRatio={196 / 100.4}
        />
        <View style={{ paddingHorizontal: WP8 }}>
          <Text centered color={GREY} weight={500} size='huge' style={{ marginBottom: WP6 }}>
            {heading.heading}
          </Text>
          <View style={{ marginBottom: WP6, paddingHorizontal: WP3 }}>
            <Text centered color={GREY_DARK} size='mini'>
              {heading.subheading}
            </Text>
          </View>
          <View style={{ marginBottom: WP4 }}>
            <Text centered color={GREY_WARM} size='xtiny' style={{ marginBottom: WP105 }}>Share your referral code</Text>
            <TouchableWithoutFeedback onLongPress={this._copyReferral}>
              <View style={{
                backgroundColor: ICE_BLUE,
                borderRadius: WP6,
                paddingHorizontal: WP6,
                paddingVertical: WP4,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center'
              }}
              >
                <Text color={GREY} size='tiny' weight={500}>{userData.referral_code}</Text>
                <Text color={BLUE_LIGHT} size='tiny' type='NeoSans' weight={500} onPress={this._copyReferral}>Salin</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={{ marginBottom: WP6 }}>
            <View style={{
              paddingVertical: WP3,
              paddingLeft: WP205,
              paddingRight: WP2,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              borderBottomColor: GREY_CALM_SEMI,
              borderBottomWidth: .7
            }}
            >
              <Text color={GREY_WARM} size='tiny' type='NeoSans' weight={500}>{`${userData.total_referral} Teman diajak`}</Text>
              <Image
                imageStyle={{ width: WP3, aspectRatio: 1 }}
                source={require('../assets/icons/icPeople.png')}
              />
            </View>

            <InviteFriendTnC>
              <View style={{
                paddingVertical: WP3,
                paddingLeft: WP205,
                paddingRight: WP2,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                borderBottomColor: GREY_CALM_SEMI,
                borderBottomWidth: .7
              }}
              >
                <Text color={GREY_WARM} size='tiny' type='NeoSans' weight={500}>Baca syarat & ketentuan</Text>
                <Image
                  imageStyle={{ width: WP3, aspectRatio: 12 / 13 }}
                  source={require('../assets/icons/icInfo.png')}
                />
              </View>
            </InviteFriendTnC>

          </View>

          <Button
            onPress={this._shareReferral}
            backgroundColor={BLUE_LIGHT}
            contentStyle={{ paddingVertical: WP4, marginBottom: WP10 }}
            centered
            radius={8}
            shadow='none'
            textType='NeoSans'
            textSize='slight'
            textColor={WHITE}
            textWeight={500}
            text='Bagikan kode referalku'
          />
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(InviteFriendsScreen),
  mapFromNavigationParam
)
