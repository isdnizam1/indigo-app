import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { get, noop } from 'lodash-es'
import HeaderNormal from 'sf-components/HeaderNormal'
import Container from 'sf-components/Container'
import Image from 'sf-components/Image'
import Text from 'sf-components/Text'
import Spacer from 'sf-components/Spacer'
import EmptyV3 from 'sf-components/EmptyV3'
import { HP1, WP105, WP2, WP5, WP20 } from 'sf-constants/Sizes'
import { PALE_BLUE, GUN_METAL, SHIP_GREY_CALM } from 'sf-constants/Colors'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import { getVoucherUser } from 'sf-actions/api'
import { Card } from 'react-native-shadow-cards'
import produce from 'immer'
import { SHADOW_STYLE } from '../../constants/Styles'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

class VoucherListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      vouchers: [],
    }
  }

  componentDidMount = () => {
    this._getVouchers()
  };

  _getVouchers = async () => {
    const { dispatch, userData } = this.props

    try {
      const { result, code } = await dispatch(
        getVoucherUser,
        { id_user: this.props.userData.id_user },
        noop,
        true,
        true,
      )
      if (code == 200) {
        const vouchers = result.map((voucher) =>
          produce(voucher, (draft) => {
            draft.settings = JSON.parse(draft.settings)
            // if(__DEV__) draft.settings.banner = 'https://i.pinimg.com/originals/a8/e0/ae/a8e0aea35187e07cd115cfdcec7288fa.png'
          }),
        )
        this.setState({ vouchers })
      }
    } catch (error) {}
  };

  _renderEmpty = () => (
    <EmptyV3
      imageWidth={WP20}
      aspectRatio={1 / 1}
      image={require('sf-assets/icons/v3/emptyVoucher.png')}
      title='Belum ada Voucher'
      message='Ikuti beragam aktivitas di Eventeer dan dapatkan berbagai voucher menarik'
      actions={<Spacer size={WP20} />}
    />
  );

  _renderList = () => {
    const { vouchers } = this.state

    const { navigateTo } = this.props

    return (
      <View style={{ paddingBottom: HP1 }}>
        <Spacer size={WP5} />
        {vouchers.map((item, index) => (
          <View
            key={`voucher-${index}`}
            style={{ justifyContent: 'center', alignItems: 'center' }}
          >
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => navigateTo('VoucherDetailScreen', { voucher: item })}
              style={{ ...SHADOW_STYLE.shadowSoft }}
            >
              <Card opacity={0.04}>
                <Image
                  imageStyle={{
                    borderTopLeftRadius: WP2,
                    borderTopRightRadius: WP2,
                    width: '100%',
                  }}
                  aspectRatio={328 / 185}
                  source={{ uri: get(item, 'settings.banner') }}
                />
                <View style={{ padding: WP5 }}>
                  <Text
                    lineHeight={WP5 + 2}
                    color={GUN_METAL}
                    weight={500}
                    size={'slight'}
                    type={'Circular'}
                  >
                    {get(item, 'settings.title')}
                  </Text>
                  <Spacer size={WP105} />
                  <Text
                    lineHeight={WP5 - 2}
                    color={SHIP_GREY_CALM}
                    size={'xmini'}
                    type={'Circular'}
                  >
                    {get(item, 'settings.subtitle')}
                  </Text>
                </View>
              </Card>
            </TouchableOpacity>
            <Spacer size={WP5} />
          </View>
        ))}
      </View>
    )
  };

  render() {
    const { navigateBack, isLoading } = this.props
    const { vouchers } = this.state

    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <View style={{ borderBottomWidth: 1, borderBottomColor: PALE_BLUE }}>
            <HeaderNormal
              iconLeftOnPress={navigateBack}
              text={'My Voucher'}
              textSize={'mini'}
              centered
            />
          </View>
        )}
        isAvoidingView
        scrollable
      >
        {vouchers.length === 0 ? this._renderEmpty() : this._renderList()}
      </Container>
    )
  }
}

VoucherListScreen.propTypes = {
  userData: PropTypes.objectOf(PropTypes.any),
}

VoucherListScreen.defaultProps = {
  userData: {},
}

export default _enhancedNavigation(connect(mapStateToProps, {})(VoucherListScreen))
