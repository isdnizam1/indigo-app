export const voucherInfo = [
  'Voucher ini hanya berlaku memalui satu pembelian dari akun ID Soundfren.',
  'Voucher hanya aktif selama 14 hari setelah pembelian.',
  'Voucher tidak dapat dipindah tangankan.',
  'Voucher berlaku untuk satu Session di Soundfren Connect',
  'Voucher hanya valid untuk satu pemakaian.',
  'Dengan menggunakan voucher ini anda menyetujui syarat dan ketentuan yang berlaku.'
]

export const voucherTnC = [
  'Voucher ini hanya berlaku memalui satu pembelian dari akun ID Soundfren.',
  'Voucher hanya aktif selama 14 hari setelah pembelian.',
  'Voucher berlaku untuk satu Session di Soundfren Connect',
  'Voucher tidak dapat dipindah tangankan.',
  'Voucher tidak dapat digunakan dengan promosi lain.',
  'Voucher hanya valid untuk satu pemakaian.',
  'Voucher tidak dapat ditukar dengan voucher lain.',
  'Soundfren berhak melakukan pembatalan penggunaan voucher yang terindikasi penipuan atau penyalahgunaan, dengan atau tanpa pemberitahuan berlanjut kepada pengguna.',
  'Soundfren berhak mengubah syarat dan ketentuan dengan atau tanpa pemberitahuan berlanjut.',
  'Dengan menggunakan voucher ini, Anda telan menyetujui semua syarat dan ketentuan yang berlaku.'
]
