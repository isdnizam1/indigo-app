import React, { Component } from 'react'
import { Easing, ScrollView, View } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getVoucherDetail } from 'sf-actions/api'
import { get, isFunction, isObject, noop } from 'lodash'
import HeaderNormal from 'sf-components/HeaderNormal'
import Container from 'sf-components/Container'
import Text from 'sf-components/Text'
import { WP1, WP100, WP2, WP3, WP305, WP4, WP5, WP50, WP6, } from 'sf-constants/Sizes'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import { GUN_METAL, PALE_BLUE, PALE_GREY_THREE, REDDISH, SHIP_GREY, SHIP_GREY_CALM, WHITE } from 'sf-constants/Colors'
import Icon from 'sf-components/Icon'
import ButtonV2 from 'sf-components/ButtonV2'
import Image from 'sf-components/Image'
import Spacer from 'sf-components/Spacer'
import Divider from 'sf-components/Divider'
import Touchable from 'sf-components/Touchable'
import { voucherDispatcher } from 'sf-services/voucher'
import Collapsible from 'react-native-collapsible'
import { NavigationEvents } from '@react-navigation/compat'
import { isArray } from 'lodash-es'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  voucher: getParam('voucher', {}),
  id_voucher_code: getParam('id_voucher_code', undefined),

})

const mapDispatchToProps = {
  voucherSet: voucherDispatcher.voucherSet,
  voucherClear: voucherDispatcher.voucherClear,
}

class VoucherDetailScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      voucher: this.props.voucher || {},
      viewTerms: false,
      onCtaPressed: noop
    }
  }

  componentDidMount = async () => {
    const { dispatch, userData: { id_user }, id_voucher_code } = this.props
    if (id_voucher_code) {
      const result = await dispatch(getVoucherDetail, {
        id_user,
        id_voucher_code
      })
      const voucher = result[0]
      if(!isObject(voucher.settings)) voucher.settings = JSON.parse(voucher.settings)
      await this.setState({ voucher })
    }
  }

  static getDerivedStateFromProps(props, state) {
    const voucherCode = get(props, 'voucher.voucher_category') || get(state, 'voucher.voucher_category')
    const onCtaPressed = {
      'maestro_voucher': () => {
        props.voucherSet(state.voucher)
        props.navigateTo('SoundconnectScreen')
      },
      'soundfren_connect': () => {
        props.voucherSet(state.voucher)
        props.navigateTo('SoundconnectScreen')
      },
      'free_trial': () => {
        props.voucherSet(state.voucher)
        props.navigateTo('ProfileScreenNoTab', { shouldShowTrialModal: true })
      }
    }[voucherCode]
    return { onCtaPressed: isFunction(onCtaPressed) ? onCtaPressed : noop }
  }

  _onFocus = () => {
    const { voucherClear, userData: { id_user }, voucher: { voucher_code }, id_voucher_code } = this.props
    voucherClear()
    getVoucherDetail({
      id_user,
      [voucher_code ? 'voucher_code' : 'id_voucher_code']: voucher_code || id_voucher_code
    }).then(({ data: { result: res } }) => {
      let result = isArray(res) ? res[0] : res
      if(!isObject(result.settings)) result.settings = JSON.parse(result.settings)
      this.setState({ voucher: result })
    })
  }

  render() {
    const { navigateBack, isLoading } = this.props
    const { voucher, viewTerms } = this.state
    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <View style={{ paddingVertical: WP1 }}>
            <HeaderNormal
              iconLeftOnPress={navigateBack}
              textSize={'mini'}
              centered
              text={'My Voucher'}
            />
          </View>
        )}
        isAvoidingView
        scrollable={false}
      >
        <ScrollView>
          <View style={{ height: WP50, width: WP100 }}>
            <Image
              source={{ uri: get(voucher, 'settings.banner') }}
              style={{ position: 'absolute' }}
              imageStyle={{ width: WP100 }}
              aspectRatio={2}
            />
          </View>

          <View>
            <View
              style={{
                borderBottomWidth: 1,
                borderBottomColor: PALE_BLUE,
                padding: WP5,
              }}
            >
              <Text
                type={'Circular'}
                color={GUN_METAL}
                size={'slight'}
                weight={500}
              >
                {get(voucher, 'settings.title')}
              </Text>
            </View>
            <View style={{ padding: WP5 }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginBottom: WP3,
                }}
              >
                <Text
                  type={'Circular'}
                  weight={400}
                  color={SHIP_GREY}
                  size='mini'
                >
                  {get(voucher, 'settings.heading')}
                </Text>
              </View>

              <View style={{ marginBottom: WP2 }}>
                {(get(voucher, 'settings.description') || []).map(
                  (item, index) => (
                    <View
                      key={`voucher-desc-${index}`}
                      style={{
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                        marginBottom: WP2,
                      }}
                    >
                      <Text
                        color={SHIP_GREY_CALM}
                        type={'Circular'}
                        lineHeight={WP5}
                        size='mini'
                      >
                        {'•'}
                      </Text>
                      <Spacer horizontal size={WP2} />
                      <View style={{ flex: 1 }}>
                        <Text
                          color={SHIP_GREY}
                          type={'Circular'}
                          lineHeight={WP5}
                          size='mini'
                        >
                          {item}
                        </Text>
                      </View>
                    </View>
                  )
                )}
              </View>
            </View>
            {!!get(voucher, 'settings.term_conditions') && <View>
              <Divider />
              <View style={{ backgroundColor: PALE_GREY_THREE }}>
                <Collapsible
                  easing={Easing.ease}
                  collapsed={!viewTerms}
                >
                  <View style={{ paddingHorizontal: WP5, paddingTop: WP5 }}>
                    <Text
                      type={'Circular'}
                      weight={400}
                      color={SHIP_GREY}
                      size='mini'
                    >
                    Syarat & Ketentuan
                    </Text>
                    <Spacer size={WP3} />
                    {(get(voucher, 'settings.term_conditions') || []).map(
                      (item, index) => (
                        <View
                          key={`voucher-term-${index}`}
                          style={{
                            flexDirection: 'row',
                            alignItems: 'flex-start',
                            marginBottom: WP2,
                          }}
                        >
                          <Text
                            color={SHIP_GREY_CALM}
                            type={'Circular'}
                            lineHeight={WP5}
                            size='mini'
                          >
                            {'•'}
                          </Text>
                          <Spacer horizontal size={WP2} />
                          <View style={{ flex: 1 }}>
                            <Text
                              color={SHIP_GREY}
                              type={'Circular'}
                              lineHeight={WP5}
                              size='mini'
                            >
                              {item}
                            </Text>
                          </View>
                        </View>
                      )
                    )}
                  </View>
                </Collapsible>
                <Touchable onPress={() => this.setState({ viewTerms: !viewTerms })}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingHorizontal: WP5,
                      paddingVertical: WP4
                    }}
                  >
                    <Text
                      color={REDDISH}
                      weight={400}
                      size={'mini'}
                      type={'Circular'}
                    >
                      {viewTerms ? 'Sembunyikan' : 'Lihat Syarat & Ketentuan'}
                    </Text>
                    <View>
                      <Icon
                        size={'large'}
                        color={REDDISH}
                        type={'MaterialCommunityIcons'}
                        name={viewTerms ? 'chevron-up' : 'chevron-down'}
                      />
                    </View>
                  </View>
                </Touchable>
              </View>
              <Divider />
            </View>}
            {/*<Button
            colors={['rgb(255, 254, 142)', 'rgb(255, 162, 33)']}
            start={[0, 0]} end={[0, 1]}
            textCentered
            centered
            text='Click to use'
            textWeight={500}
            contentStyle={{ borderRadius: 10 }}
            onPress={() => {
              voucherSet(voucher)
              navigateTo('ScListScreen', { category: 'session' })
            }}
          />*/}
          </View>
        </ScrollView>
        <View style={{ paddingHorizontal: WP6, paddingVertical: WP305, borderTopWidth: 0.75, borderTopColor: PALE_BLUE }}>
          <ButtonV2
            onPress={this.state.onCtaPressed}
            style={{ paddingVertical: WP4 }}
            textSize={'slight'}
            text='Gunakan Sekarang'
            textColor={WHITE}
            color={REDDISH}
          />
        </View>
        <NavigationEvents onDidFocus={this._onFocus} />
      </Container>
    )
  }
}

VoucherDetailScreen.propTypes = {
  voucher: PropTypes.objectOf(PropTypes.any),
}

VoucherDetailScreen.defaultProps = {
  voucher: {},
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(VoucherDetailScreen),
  mapFromNavigationParam
)
