import React from 'react'
import { View, TouchableOpacity, StyleSheet, BackHandler, Keyboard, Modal } from 'react-native'
import * as yup from 'yup'
import { connect } from 'react-redux'
import { isEmpty, split, noop } from 'lodash-es'
import { _enhancedNavigation, Container, Text, Icon, Form, ModalMessageView, InputTextLight, MenuOptions, Image } from '../components'
import { WHITE, PALE_GREY_TWO, SHIP_GREY, SHIP_GREY_CALM, PALE_GREY, GUN_METAL, SILVER_TWO, PALE_SALMON, REDDISH, PALE_BLUE, PALE_BLUE_TWO } from '../constants/Colors'
import { WP4, WP5, WP15, WP2, WP12, WP7, WP100, WP8, WP3, WP305, WP1, WP6, WP105, WP16, WP40 } from '../constants/Sizes'
import { TOUCH_OPACITY, SHADOW_STYLE } from '../constants/Styles'
import { selectPhoto, takePhoto } from '../utils/upload'
import { postFeedbackAlbum } from '../actions/api'

const validation = yup.object().shape({
  description: yup.string().required(),
  attachmentb64: yup.string().required(),
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {}

const mapFromNavigationParam = () => ({})

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: '100%',
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 6,
  }
})

const ButtonIcon = ({ onPress = () => { }, title, iconName, iconType = 'MaterialCommunityIcons', iconPosition = 'left', textColor = SHIP_GREY_CALM, iconColor = SHIP_GREY_CALM }) => (
  <TouchableOpacity onPress={onPress} activeOpacity={TOUCH_OPACITY} style={{ flexDirection: 'row', alignItems: 'center' }}>
    {iconPosition == 'right' ? (<Text type='Circular' size='xmini' weight={400} color={textColor} style={{ marginRight: WP105 }}>{title}</Text>) : null}
    <Icon
      centered
      size='small'
      color={iconColor}
      name={iconName}
      type={iconType}
    />
    {iconPosition == 'left' ? (<Text type='Circular' size='xmini' weight={400} color={textColor} style={{ marginLeft: WP105 }}>{title}</Text>) : null}
  </TouchableOpacity>
)

class FeedbackScreen extends React.Component {
  _didFocusSubscription
  _willBlurSubscription

  constructor(props) {
    super(props)
    this._didFocusSubscription = props.navigation.addListener('focus', () =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler)
    )
  }

  state = {
    backModal: false,
    popUpVisible: false
  }

  async componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('blur', () =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
    )
  }

  componentWillUnmount() {
    this._didFocusSubscription()
    this._willBlurSubscription()
  }

  _backHandler = async () => {
    const { navigateBack } = this.props
    const { backModal } = this.state

    if (this.form && this.form.isDirtyState()) {
      this.setState({ backModal: !backModal })
    } else {
      navigateBack()
    }
  }

  _postFeedbacReport = async ({ formData }) => {
    const { userData, dispatch } = this.props
    try {
      const params = {
        id_user: userData.id_user,
        feedback: 'report',
        description: formData.description,
        attachment: formData.attachmentb64,
      }
      const result = await dispatch(postFeedbackAlbum, params, noop, true, true)

      if (result.code == 200) {
        this.setState({ popUpVisible: true })
      }
    } catch (error) {
      // silent
    }
  }

  render() {
    const {
      navigateBack,
      isLoading,
    } = this.props
    const { backModal, popUpVisible } = this.state
    return (
      <Form
        ref={(form) => this.form = form}
        validation={validation}
        initialValue={{ description: '', attachmentUri: '', attachmentb64: '' }}
        onSubmit={this._postFeedbacReport}
      >
        {({ onChange, onSubmit, formData, isValid, isDirty }) => {
          const splits = split(formData.attachmentUri, '/')
          const filename = !isEmpty(formData.attachmentUri) ? splits[splits.length - 1] : ''
          return (
            <Container
              theme='dark'
              isLoading={isLoading}
              renderHeader={() => (
                <View style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  paddingHorizontal: WP5,
                  paddingVertical: WP2,
                  backgroundColor: WHITE,
                }}
                >
                  <View style={{ width: WP15, alignItems: 'flex-start' }}>
                    <Icon
                      onPress={() => this._backHandler()}
                      background='dark-circle'
                      size='large'
                      color={SHIP_GREY_CALM}
                      name='chevron-left'
                      type='Entypo'
                    />
                  </View>
                  <Text type='Circular' size='mini' weight={400} color={SHIP_GREY} centered>{'Feedback & Report'}</Text>
                  <View style={{ width: WP15, alignContent: 'flex-end' }}>

                    {
                      isLoading && isValid ? (
                        <Image
                          spinning
                          source={require('../assets/icons/ic_loading.png')}
                          style={{ alignSelf: 'flex-end' }}
                          imageStyle={{ width: WP5, aspectRatio: 1 }}
                        />
                      ) : (
                        <TouchableOpacity
                          style={{ width: WP15, alignContent: 'flex-end' }}
                          disabled={!isValid || !isDirty}
                          onPress={() => {
                            Keyboard.dismiss()
                            onSubmit()
                          }}
                        >
                          <Text type='Circular' size='mini' weight={400} color={(!isValid || !isDirty) ? PALE_SALMON : REDDISH} style={{ textAlign: 'right' }}>Kirim</Text>
                        </TouchableOpacity>
                      )
                    }
                  </View>
                </View>
              )}
              isAvoidingView
              scrollable
              scrollBackgroundColor={PALE_GREY_TWO}
            >
              <ModalMessageView
                style={{ width: WP100 - WP8 }}
                contentStyle={{ paddingHorizontal: WP4 }}
                toggleModal={() => this.setState({ backModal: false })}
                isVisible={backModal}
                title={'Perubahan belum tersimpan'}
                titleType='Circular'
                titleSize={'small'}
                titleColor={GUN_METAL}
                titleStyle={{ marginBottom: WP4 }}
                subtitle={'Kamu memiliki beberapa perubahan yang belum tersimpan, apakah kamu yakin untuk keluar?'}
                subtitleType='Circular'
                subtitleSize={'xmini'}
                subtitleWeight={400}
                subtitleColor={SHIP_GREY_CALM}
                subtitleStyle={{ marginBottom: WP3 }}
                image={null}
                buttonPrimaryText={'Tidak'}
                buttonPrimaryContentStyle={{ borderRadius: 8, paddingVertical: WP305 }}
                buttonPrimaryTextType='Circular'
                buttonPrimaryTextWeight={400}
                buttonPrimaryBgColor={REDDISH}
                buttonSecondaryText={'Ya'}
                buttonSecondaryStyle={{ backgroundColor: PALE_GREY, marginTop: WP1, borderRadius: 8, paddingVertical: WP305 }}
                buttonSecondaryTextType='Circular'
                buttonSecondaryTextWeight={400}
                buttonSecondaryTextColor={SHIP_GREY_CALM}
                buttonSecondaryAction={() => navigateBack()}
              />

              <View style={{ backgroundColor: PALE_GREY_TWO }}>
                <View style={{ width: '100%', paddingTop: WP7, paddingBottom: WP8, paddingHorizontal: WP4, borderBottomWidth: 1, borderBottomColor: PALE_BLUE }}>
                  <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>Jelaskan masalah yang kamu alami</Text>
                  <InputTextLight
                    onChangeText={onChange('description')}
                    value={formData.description}
                    placeholder='Tuliskan masalah yang kamu hadapi agar kami dapat membantumu dan semua pengguna Eventeer'
                    placeholderTextColor={SILVER_TWO}
                    color={SHIP_GREY_CALM}
                    bordered
                    size='mini'
                    type='Circular'
                    returnKeyType={'next'}
                    multiline
                    lineHeight={4}
                    style={{ marginTop: 0, marginBottom: 0 }}
                    textInputStyle={styles.input}
                  />
                </View>
                <View style={{ padding: WP4, paddingTop: WP6 }}>
                  {
                    !isEmpty(formData.attachmentUri) && (
                      <View style={{ marginBottom: WP6 }}>
                        <Text weight={600} type='Circular' size={'tiny'} color={GUN_METAL} style={{ marginBottom: WP4 }}>UPLOADED FILES</Text>
                        <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', backgroundColor: WHITE, borderRadius: 6, padding: WP4, paddingRight: WP6, ...SHADOW_STYLE['shadowThin'] }}>
                          <Image
                            style={{ marginRight: WP4 }}
                            imageStyle={{ width: WP12, height: WP12, aspectRatio: 1, borderRadius: 6 }}
                            source={{ uri: formData.attachmentUri }}
                          />
                          <View style={{ flex: 1 }}>
                            <Text weight={400} type='Circular' size={'mini'} color={GUN_METAL} style={{ marginBottom: WP105 }} numberOfLines={1}>{filename}</Text>
                            <ButtonIcon
                              iconName='delete'
                              title='Delete'
                              onPress={() => {
                                onChange('attachmentUri')('')
                                onChange('attachmentb64')('')
                              }}
                            />
                          </View>
                        </View>
                      </View>
                    )
                  }
                  <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>Lengkapi laporanmu dengan foto</Text>
                  <Text weight={300} type='Circular' size={'xmini'} color={SHIP_GREY_CALM} style={{ marginBottom: WP4 }}>Laporkan kendala yang kamu hadapi didalam aplikasi Eventeer dan bantu kami untuk menjadi lebih baik.</Text>
                  <View style={{ flexWrap: 'wrap', marginBottom: WP4 }}>
                    <MenuOptions
                      options={[
                        {
                          onPress: selectPhoto(onChange, 'attachment', [1, 1], { allowsEditing: false }),
                          title: 'Choose from Library',
                          withBorder: true
                        },
                        {
                          onPress: takePhoto(onChange, 'attachment', [1, 1], { allowsEditing: false }),
                          title: 'Take Photo',
                        }
                      ]}
                      triggerComponent={(toggleModal) => (
                        <TouchableOpacity
                          activeOpacity={TOUCH_OPACITY}
                          onPress={toggleModal}
                          style={{
                            paddingHorizontal: WP6,
                            paddingVertical: WP2,
                            borderRadius: 6,
                            backgroundColor: REDDISH,
                          }}
                        >
                          <Text type='Circular' size='mini' weight={400} color={WHITE}>Upload Foto</Text>
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                  <Text type='Circular' size='xmini' weight={300} color={SILVER_TWO}>Upload file max. 10 mb (jpg, jpeg, png)</Text>
                </View>
              </View>

              <Modal
                transparent
                onRequestClose={() => { }}
                animationType='slide'
                visible={popUpVisible}
              >
                <TouchableOpacity activeOpacity={1} disabled={true} style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.75)', justifyContent: 'center', alignItems: 'center', }}>
                  <View style={{ width: WP100 - WP16 }}>
                    <View style={{ width: '100%', backgroundColor: WHITE, paddingHorizontal: WP4, paddingTop: WP6, paddingBottom: WP8, borderRadius: 6, justifyContent: 'center', alignItems: 'center', }}>
                      <Image
                        source={require('../assets/icons/icSuccesLarge.png')}
                        imageStyle={[{ width: WP40, marginBottom: WP4 }]}
                        aspectRatio={1}
                      />
                      <Text centered weight={600} type='Circular' size={'small'} color={GUN_METAL} style={{ marginBottom: WP4 }}>{'Feedback & Report Terkirim'}</Text>
                      <Text centered weight={300} type='Circular' size={'mini'} color={SHIP_GREY}>{'Terima kasih atas partisipasinya,\nlaporanmu sudah kami terima.'}</Text>
                    </View>
                    <View style={{ position: 'absolute', top: WP2, right: WP2 }}>
                      <Icon
                        onPress={() => {
                          this.setState({ popUpVisible: false }, navigateBack)
                        }}
                        background='dark-circle'
                        size='large'
                        color={SHIP_GREY_CALM}
                        name='close'
                        type='MaterialCommunityIcons'
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </Modal>
            </Container>
          )
        }}
      </Form>
    )
  }
}

FeedbackScreen.navigationOptions = () => ({
  gesturesEnabled: false
})

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(FeedbackScreen),
  mapFromNavigationParam
)
