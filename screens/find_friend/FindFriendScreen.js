/* eslint-disable react/sort-comp */
import React from "react";
import { BackHandler, FlatList, View } from "react-native";
import { noop, isEmpty, concat } from "lodash-es";
import { connect } from "react-redux";
import { NavigationActions, NavigationEvents } from "@react-navigation/compat";
import Empty from "sf-components/search/Empty";
import Spacer from "sf-components/Spacer";
import { _enhancedNavigation, Container, FollowItem } from "../../components";
import { FOLLOW_PAGING } from "../../constants/Routes";
import { PALE_BLUE } from "../../constants/Colors";
import {
  postProfileFollow,
  postProfileUnfollow,
  getContact,
} from "../../actions/api";
import { HP5, HP12 } from "../../constants/Sizes";
import HeaderNormal from "../../components/HeaderNormal";
import Loader from "../../components/Loader";
import { initiateRoom } from "../../utils/helper";
import { resetAction } from "../../utils/backhandler";

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam("initialLoaded", false),
  onRefresh: getParam("onRefresh", () => {}),
  initialTab: getParam("initialTab", 0),
  isMessage: getParam("isMessage", false),
  isStartup: getParam("isStartup", true),
  fromScreen: getParam("fromScreen", "ExploreScreen"),
});

class FindFriendScreen extends React.PureComponent {
  state = {
    people: [],
    artists: [],
    isReady: false,
    isRefreshing: false,
    isLoadMore: false,
    endPeople: false,
    endArtist: false,
    hasScroll: false,
    index: this.props.initialTab,
    routes: [
      // { key: 'artist', title: 'ARTISTS' },
      { key: "people", title: "PEOPLE" },
    ],
  };

  static navigationOptions = ({ navigation }) => ({
    gesturesEnabled: !navigation.getParam("initialLoaded", false),
  });

  componentDidMount() {
    this._getInitialScreenData({ start: 0, limit: 10 });
    BackHandler.addEventListener("hardwareBackPress", this._backHandler);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this._backHandler);
  }

  _getInitialScreenData = async (...args) => {
    await Promise.all([this._getPeople(...args)]);
    this.setState({ isReady: true });
  };

  _refreshScreen = () => {
    this.setState(
      {
        people: [],
        artists: [],
        isReady: false,
      },
      () => {
        this._getInitialScreenData();
      }
    );
  };

  _getPeople = async (
    params = FOLLOW_PAGING,
    loadMore = false,
    isLoading = true
  ) => {
    const {
      userData: { id_user },
      dispatch,
    } = this.props;
    const { people } = this.state;

    if (!params) params = FOLLOW_PAGING;

    const dataResponse = await dispatch(
      getContact,
      { id_user: id_user, ...params },
      noop,
      true,
      isLoading
    );

    let newPeople = loadMore ? people : [];
    if (!isEmpty(dataResponse.result))
      newPeople = concat(newPeople, dataResponse.result);
    this.setState({
      people: newPeople,
      endPeople: dataResponse.result.length < 10,
    });
  };

  _onFollowed = async (
    follow,
    with_id_user,
    isLoading = true,
    user_fullname,
    user_profile_picture,
    id_groupmessage
  ) => {
    const {
      userData,
      dispatch,
      isMessage,
      navigateTo,
      navigation,
      fromScreen,
      navigateBack,
    } = this.props;
    const { id_user } = userData;

    if (isMessage && !follow) {
      const user = { id_user: with_id_user };
      const chatRoomDetail = await initiateRoom(userData, user);
      const onRefresh = async () => {};
      const navigateBackCallback = async () => {
        if (chatRoomDetail.room.comments.length == 0) navigateBack();
        else
          resetAction(
            navigation,
            1,
            [
              NavigationActions.navigate({
                routeName: "ChatScreen",
                params: { fromScreen },
              }),
            ],
            fromScreen
          );
      };

      await navigateTo("ChatRoomScreen", {
        with_id_user,
        user_fullname,
        user_profile_picture,
        id_groupmessage,
        onRefresh,
        navigateBackCallback,
      });
    } else {
      const currentAction = follow ? postProfileFollow : postProfileUnfollow;
      const currentPayload = follow
        ? { id_user: with_id_user, followed_by: id_user }
        : {
            id_user,
            id_user_following: with_id_user,
          };
      await dispatch(currentAction, currentPayload, noop, true, isLoading);
    }
  };

  _backHandler = async () => {
    this.props.onRefresh();
    this.props.navigateBack();
  };

  render() {
    const { navigateTo, isLoading, isMessage } = this.props;
    const { people, endPeople, isReady, isLoadMore } = this.state;
    return (
      <Container
        onPullDownToRefresh={() => {
          this._getInitialScreenData(null, null, false);
        }}
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <View style={{ borderBottomWidth: 1, borderBottomColor: PALE_BLUE }}>
            <HeaderNormal
              iconLeftOnPress={this._backHandler}
              withExtraPadding
              textWeight={400}
              text={isMessage ? "Find Contact" : "Find People"}
              textSize="slight"
              textType="Circular"
              centered
            />
          </View>
        )}
      >
        <NavigationEvents onWillFocus={() => this._refreshScreen()} />
        <FlatList
          bounces={true}
          bouncesZoom={false}
          initialNumToRender={10}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: HP5 }}
          style={{ flexGrow: 0 }}
          data={people}
          keyExtractor={(item) => item.id}
          renderItem={({ item, index }) => (
            <FollowItem
              isMine={false}
              onPressItem={() =>
                navigateTo("ProfileScreen", {
                  idUser: item.id,
                  navigateBackOnDone: true,
                })
              }
              onFollow={this._onFollowed}
              key={`${index}${item.id}`}
              user={item}
              isMessage={isMessage}
            />
          )}
          ListEmptyComponent={
            <View>
              <Spacer size={HP12} />
              <Empty />
            </View>
          }
          onMomentumScrollBegin={() => this.setState({ hasScroll: true })}
          onMomentumScrollEnd={() => this.setState({ hasScroll: false })}
          onEndReachedThreshold={0.01}
          onEndReached={
            endPeople
              ? null
              : async () => {
                  if (!isReady) return;
                  this.setState({ isLoadMore: true });
                  await this._getPeople(
                    { start: people.length, limit: 10 },
                    true,
                    false
                  );
                  this.setState({ isLoadMore: false });
                }
          }
          ListFooterComponent={isLoadMore && <Loader size="mini" isLoading />}
        />
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(FindFriendScreen),
  mapFromNavigationParam
);
