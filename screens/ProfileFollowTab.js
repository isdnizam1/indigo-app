import React from "react";
import { TouchableOpacity, View } from "react-native";
import { isEmpty, map, noop, isUndefined } from "lodash-es";
import { withNavigation } from "@react-navigation/compat";
import {
  InputSearch,
  FollowItem,
  _enhancedNavigation,
  Container,
  Text,
} from "../components";
import { WP100, WP105, WP14, WP2, WP3, WP4, WP6 } from "../constants/Sizes";

import { DEFAULT_PAGING } from "../constants/Routes";
import { getFriendSuggestion } from "../actions/api";
import { SHADOW_STYLE } from "../constants/Styles";
import {
  PALE_BLUE,
  PALE_GREY_TWO,
  REDDISH,
  SHIP_GREY_CALM,
  WHITE,
} from "../constants/Colors";
import EmptyV3 from "../components/EmptyV3";
import Image from "../components/Image";
import ProfilePeopleListTab from "./ProfilePeopleListTab";

class ProfileFollowTab extends React.Component {
  state = {
    friendSuggestion: undefined,
    isRefreshing: false,
    isSearch: false,
  };

  async componentDidMount() {
    await this._getInitialScreenData({ limit: 5 });
  }

  _getInitialScreenData = async (...args) => {
    await this._getFriendSuggest(...args);
    this.setState({
      isReady: true,
      isRefreshing: false,
    });
  };

  _getFriendSuggest = async (
    params = DEFAULT_PAGING,
    loadMore = false,
    isLoading = true
  ) => {
    const { idViewer, dispatch } = this.props;
    try {
      const dataResponse = await dispatch(
        getFriendSuggestion,
        { id_user: idViewer, ...params },
        noop,
        true,
        isLoading
      );
      if (dataResponse.code == 200) {
        this.setState({ friendSuggestion: dataResponse.result });
      } else {
        this.setState({ friendSuggestion: undefined });
      }
    } catch (error) {
      this.setState({ friendSuggestion: undefined });
      //
    }
  };

  _renderEmptyState = () => {
    const { navigateTo } = this.props;
    return (
      <View style={{ width: WP100, height: WP100 }}>
        <EmptyV3
          backgroundColor={WHITE}
          title="Tambahkan teman"
          message={"Temukan  pengguna lainnya di\nEventeer"}
          icon={
            <Image
              imageStyle={{
                width: WP14,
                height: WP14,
                marginBottom: WP4,
              }}
              source={require("sf-assets/icons/v3/icBgGroupAdd.png")}
            />
          }
          actions={
            <TouchableOpacity
              onPress={() =>
                navigateTo("FindFriendScreen", {
                  isMessage: false,
                  fromScreen: "ProfileScreen",
                })
              }
              style={{
                marginTop: WP4,
                paddingHorizontal: WP4,
                paddingVertical: WP105,
                borderRadius: 6,
                backgroundColor: REDDISH,
              }}
            >
              <Text type="Circular" size="xmini" weight={400} color={WHITE}>
                Cari Teman
              </Text>
            </TouchableOpacity>
          }
        />
      </View>
    );
  };

  render() {
    const {
      idViewer,
      isMine,
      navigateTo,
      refresh,
      dataSource,
      onFollow,
      type,
      user,
      isLoading,
      withEmptyScreen,
    } = this.props;
    const { friendSuggestion, isSearch } = this.state;
    const count =
      type == "followers" ? user.total_followers : user.total_following;

    const _renderSuggestion = (isEmpty = true) => (
      <ProfilePeopleListTab
        dataSource={friendSuggestion}
        navigateTo={navigateTo}
        type={type}
        isMine={isMine}
        idViewer={idViewer}
        onFollow={onFollow}
        user={user}
        isEmpty={isEmpty}
      />
    );
    const _renderData = () => (
      <View style={{ paddingTop: WP2 }}>
        {map(dataSource, (data, i) => (
          <FollowItem
            type={type}
            isMine={
              type == "followers"
                ? idViewer === data.followed_by
                : idViewer === data.id_user
            }
            onPressItem={() => {
              const idUser =
                type === "followers" ? data.followed_by : data.id_user;
              navigateTo(
                "ProfileScreenNoTab",
                { idUser },
                idViewer === idUser ? undefined : "push"
              );
            }}
            onFollow={
              idViewer ===
              (type === "followers" ? data.followed_by : data.id_user)
                ? null
                : onFollow
            }
            key={`${i}${data.id_user}${type}`}
            user={data}
          />
        ))}
      </View>
    );

    if (withEmptyScreen && isEmpty(dataSource) && isMine) {
      return this._renderEmptyState();
    }

    return (
      <Container
        scrollable={!isEmpty(dataSource)}
        noStatusBarPadding
        onPullUpToLoad={
          !isEmpty(dataSource)
            ? async () => {
                await refresh(
                  { start: dataSource.length, limit: 15 },
                  true,
                  false
                );
              }
            : () => {}
        }
        scrollBackgroundColor={PALE_GREY_TWO}
      >
        <View style={{ flexGrow: 1, backgroundColor: PALE_GREY_TWO }}>
          {((isMine && count > 0) || (!isMine && count >= 0)) && (
            <View
              style={{
                backgroundColor: WHITE,
                paddingVertical: WP3,
                paddingHorizontal: WP4,
                ...SHADOW_STYLE["shadowThin"],
              }}
            >
              <InputSearch
                onSearch={(searchKey) => {
                  this.setState({ isSearch: true }, () =>
                    refresh({ name: searchKey })
                  );
                }}
              />
            </View>
          )}
          {isEmpty(dataSource) ? (
            isMine && count == 0 ? (
              _renderSuggestion()
            ) : isSearch && !isLoading ? (
              <View style={{ paddingTop: WP6, paddingHorizontal: WP4 }}>
                <Text
                  color={SHIP_GREY_CALM}
                  type={"Circular"}
                  size="xmini"
                  weight={400}
                >
                  Pencarian tidak ditemukan
                </Text>
              </View>
            ) : null
          ) : dataSource.length > 1 ? (
            _renderData()
          ) : (
            <View>
              {_renderData()}
              {!isUndefined(friendSuggestion) && friendSuggestion.length > 0 && (
                <View
                  style={{
                    borderTopWidth: 1,
                    borderTopColor: PALE_BLUE,
                    paddingTop: WP4,
                  }}
                >
                  <Text
                    color={SHIP_GREY_CALM}
                    type={"Circular"}
                    size="xmini"
                    weight={400}
                    style={{ paddingHorizontal: WP4 }}
                  >
                    Saran pertemanan
                  </Text>
                  {_renderSuggestion(false)}
                </View>
              )}
            </View>
          )}
        </View>
      </Container>
    );
  }
}

export default withNavigation(_enhancedNavigation(ProfileFollowTab));
