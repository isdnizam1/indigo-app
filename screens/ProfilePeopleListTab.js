import React, { isValidElement } from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'
import { map, isUndefined } from 'lodash-es'
import { FollowItem, Text, Image } from '../components'
import { WP1, WP105, WP15, WP4, WP5, WP6 } from '../constants/Sizes'
import { GUN_METAL, NAVY_DARK, PALE_BLUE, PALE_GREY_TWO, REDDISH, SHIP_GREY_CALM, WHITE } from '../constants/Colors'
import ButtonV2 from '../components/ButtonV2'

const styles = StyleSheet.create({
  emptyStateIcon: {
    width: WP15,
    height: WP15,
    marginBottom: WP4
  },
  emptyStateDescription: {
    lineHeight: WP5,
    marginTop: WP1
  },
  emptyStateCta: {
    marginTop: WP4,
    paddingVertical: WP105,
    paddingHorizontal: WP4
  },
  suggestionHeader: {
    paddingTop: WP6,
    paddingBottom: WP1,
    paddingHorizontal: WP4,
    borderTopWidth: 1,
    borderTopColor: PALE_BLUE
  }
})

const configEmpty = {
  following: (isMine, username) => ({
    title: isMine ? 'Tambahkan Teman' : 'Belum Ada yang Diikuti',
    subtitle: isMine ? 'Temukan pengguna lainnya di Eventeer' : `Kamu akan melihat semua yang diikuti oleh ${username} disini`
  }),
  followers: (isMine, username) => ({
    title: 'Belum ada pengikut',
    subtitle: isMine ? 'Kamu akan melihat akun yang mengikutimu disini' : `Kamu akan melihat akun yang mengikuti ${username} disini`
  }),
}

const EmptyState = ({
  bottomComponent,
  title,
  subtitle,
  image = require('../assets/icons/v3/icBgGroupAdd.png'),
  lessPadding
}) => {
  return (
    <View style={{
      alignItems: 'center',
      paddingHorizontal: WP15,
      paddingVertical: lessPadding ? WP6 : WP15
    }}
    >
      <Image
        imageStyle={styles.emptyStateIcon}
        source={image}
      />
      <View>
        <Text centered color={GUN_METAL} type={'Circular'} weight={500}>{title}</Text>
        <Text centered style={styles.emptyStateDescription} size={'xmini'} color={SHIP_GREY_CALM} type={'Circular'} weight={300}>{subtitle}</Text>
      </View>
      {isValidElement(bottomComponent) && bottomComponent}
    </View>
  )
}

class ProfilePeopleListTab extends React.Component {
  state = {
    isRefreshing: false
  }

  render() {
    const {
      idViewer,
      isMine,
      navigateTo,
      dataSource,
      onFollow,
      type,
      emptyMessage,
      user,
      isEmpty: _isEmpty
    } = this.props
    return (
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: _isEmpty ? WHITE : PALE_GREY_TWO }}
        contentContainerStyle={{ flexGrow: 1 }}
      >
        {
          _isEmpty && (
            <EmptyState
              title={configEmpty[type](isMine, user?.full_name).title}
              subtitle={configEmpty[type](isMine, user?.full_name).subtitle}
              bottomComponent={
                type == 'following' && isMine && (
                  <ButtonV2
                    onPress={() => navigateTo('ArtistSpotlightScreen')}
                    style={styles.emptyStateCta}
                    color={REDDISH}
                    textColor={WHITE}
                    textSize={'xmini'}
                    text={'Cari Teman'}
                  />
                )
              }
            />
          )
        }
        {
          _isEmpty && isMine && (
            <View style={styles.suggestionHeader}>
              <Text color={NAVY_DARK} type={'Circular'} size='medium' weight={600}>Saran Pertemanan</Text>
            </View>
          )
        }
        {
          isUndefined(dataSource) && isMine && (
            <EmptyState
              title={'Data Tidak Tersedia'}
              subtitle={emptyMessage}
              lessPadding
            />
          )
        }
        {
          isMine && (<View>
            {
              map(dataSource, (data, i) => (
                <FollowItem
                  type={type}
                  isMine={idViewer === data.id_user}
                  onPressItem={() => {
                    const idUser = type === 'followers' && data.followed_by ? data.followed_by : data.id_user
                    navigateTo('ProfileScreenNoTab', { idUser }, idViewer === idUser ? undefined : 'push')
                  }}
                  idUser={data.id_user}
                  onFollow={idViewer === (type === 'followers' ? data.followed_by : data.id_user) ? null : onFollow}
                  key={`${i}${data.id_user}${type}`}
                  user={data}
                />
              ))
            }
          </View>)
        }
      </ScrollView>
    )
  }
}

ProfilePeopleListTab.defaultProps = {
  isEmpty: true,
  emptyMessage: 'Untuk saat ini saran pertemanan belum ada.'
}
export default ProfilePeopleListTab
