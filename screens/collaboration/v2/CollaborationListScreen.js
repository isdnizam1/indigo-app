import React, { Component, Fragment } from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { isEmpty, noop } from 'lodash-es'
import SkeletonContent from 'react-native-skeleton-content'
import { _enhancedNavigation, Text, Icon, Container, Button, BottomSheet, Image } from '../../../components'
import { getAdsCollab, postLogCreateCollab } from '../../../actions/api'
import { WHITE, SHIP_GREY_CALM, SHIP_GREY, GREY_BORDER, NAVY_DARK, GREEN30, CLEAR_BLUE, SKELETON_COLOR, SKELETON_HIGHLIGHT } from '../../../constants/Colors'
import { WP4, WP2, WP100, WP6, WP3, HP5, HP2, HP100, HP80, WP5, WP90, WP8, WP1 } from '../../../constants/Sizes'
import { TOUCH_OPACITY } from '../../../constants/Styles'
import CollaborationItem from '../../../components/collaboration/CollaborationItem'
import SoundfrenExploreOptions from '../../../components/explore/SoundfrenExploreOptions'
import Modal from '../../../components/Modal'
import { CollaborationOptionMenus } from './CollaborationOptionMenus'

const collabConfig = {
  empty_message: 'Maaf, untuk saat ini belum ada\ncollaboration yang tersedia',
  title: 'Mulai Kolaborasimu',
  subtitle: 'Cari teman kolaborasimu sekarang'
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = () => ({
})

class CollaborationListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: true,
      isLoading: true,
      result: {},
      sort: 'new',
      filter: 'all',
      isFilterOpen: false,
    }

    this.sortFilterSheet = React.createRef()
  }

  componentDidMount = () => {
    this._getContent({ start: 0, limit: 10 }, false, false)
  }

  _getContent = async (params, loadMore = false, isLoading = false) => {
    const {
      dispatch,
      userData: {
        id_user
      }
    } = this.props
    const {
      sort,
      filter
    } = this.state

    if (!loadMore) {
      this.setState({
        isLoading: true
      })
    }

    try {
      if (!params) params = { start: 0, limit: 10 }

      const { result } = await dispatch(getAdsCollab, {
        id_user,
        show: 'collaboration',
        sort_by: sort,
        filter,
        ...params
      }, noop, true, isLoading)

      const dataResult = result ? result.collaboration || [] : []

      if (!loadMore) {
        this.setState({
          result: dataResult,
          isLoading: false
        })
      } else {
        this.setState({
          result: concat(this.state.result, dataResult || []),
          isLoading: false
        })
      }
    } catch (e) {
      this.setState({ isLoading: false })
    }
  }

  _createCollab = () => {
    const { navigateTo } = this.props
    Promise.resolve(navigateTo('CollaborationForm')).then(() => {
      postLogCreateCollab({ id_user: this.props.userData.id_user })
    })
  }

  _adItem = (ads, i, section, loading) => {
    const { navigateTo } = this.props

    return (
      <TouchableOpacity
        disabled={loading}
        style={{ paddingLeft: WP3, paddingVertical: WP2, width: WP100 }}
        activeOpacity={TOUCH_OPACITY}
        onPress={() => navigateTo('CollaborationPreview', { id: ads.id_ads })}
      >
        <CollaborationItem ads={ads} loading={loading} navigateTo={navigateTo} />
      </TouchableOpacity>
    )
  }

  _headerSection = (about) => {
    return (
      <TouchableOpacity activeOpacity={TOUCH_OPACITY} onPress={() => this._createCollab()} >
        <Image
          tint={'black'}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 75}
          source={require('../../../assets/images/bgCollaboration2.png')}
        />
        <View style={{
          position: 'absolute', left: 0, top: 0, right: 0, bottom: 0,
          justifyContent: 'space-between', alignItems: 'flex-start', flexDirection: 'row', paddingVertical: WP6, paddingHorizontal: WP8
        }}
        >
          <SkeletonContent
            containerStyle={{ alignItems: 'center' }}
            isLoading={this.state.isLoading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <View style={{ alignItems: 'flex-start' }}>
              <Text type='Circular' size='slight' weight={600} color={WHITE} centered style={{ marginBottom: WP1 }}>{about?.title}</Text>
              <Text type='Circular' size='mini' weight={300} color={WHITE} centered >{about?.subtitle}</Text>
            </View>
          </SkeletonContent>
          <Image
            tint={'black'}
            size='mini'
            aspectRatio={1}
            source={require('../../../assets/icons/icArrowNext.png')}
            onPress={() => this._createCollab()}
          />
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const {
      isReady,
      isLoading,
      sort,
      filter,
      result,
      isFilterOpen
    } = this.state
    const {
      navigateBack,
      navigateTo
    } = this.props
    const section = 'collaboration'

    const dummyData = [1, 2, 3]
    const dataList = isLoading ? dummyData : !isEmpty(result) ? result : []

    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        onPullUpToLoad={async () => {
          await this._getContent({ start: result.length, limit: 10 }, true, false)
        }}
        isReady={isReady}
        isLoading={false}
        outsideScrollContent={() => (
          <>
            {!isFilterOpen && <Button
              rounded
              style={{ position: 'absolute', bottom: HP5, alignSelf: 'center' }}
              shadow='shadow'
              backgroundColor={WHITE}
              textColor={SHIP_GREY}
              textType='Circular'
              textSize='mini'
              iconName='sort-variant'
              iconType='MaterialCommunityIcons'
              iconSize='mini'
              text='Sort & Filter'
              onPress={() => {
                this.setState({ isFilterOpen: true }, () => {
                  this.sortFilterSheet.current?.snapTo(0)
                })
              }}
                              />}
            <BottomSheet
              innerRef={this.sortFilterSheet}
              snapPoints={[HP80, -HP100]}
              showPanel
              onCloseEnd={() => this.setState({ isFilterOpen: false })}
              borderRadius={WP5}
              renderContent={({ closeBottomSheet }) => (
                <View style={{ height: HP80, zIndex: 9999 }}>
                  <View style={{ justifyContent: 'space-between', paddingHorizontal: WP6, paddingVertical: HP2, flexDirection: 'row' }}>
                    <Text type='Circular' weight={600} size='large' color={NAVY_DARK}>Sort & Filter</Text>
                    <Icon
                      onPress={() => closeBottomSheet()}
                      centered
                      color={SHIP_GREY_CALM}
                      type='EvilIcons'
                      name='close'
                      size='large'
                    />
                  </View>
                  <View style={{ width: WP100, height: 1, backgroundColor: GREY_BORDER }} />
                  <Text style={{ paddingHorizontal: WP6, paddingTop: HP2 }} type='Circular' weight={400} size='mini' color={SHIP_GREY}>Urutkan Kolaborasi berdasarkan</Text>
                  <View style={{ paddingHorizontal: WP6, flexDirection: 'row' }}>
                    <Button
                      onPress={() => {
                        this.setState({ sort: 'new' })
                      }}
                      toggle
                      compact='center'
                      borderRadius={WP2}
                      toggleActive={sort === 'new'}
                      toggleActiveColor={WHITE}
                      toggleInactiveColor={WHITE}
                      toggleInactiveTextColor={SHIP_GREY}
                      backgroundColor={sort === 'new' ? GREEN30 : GREY_BORDER}
                      shadow='none'
                      text='Terbaru'
                      textType='Circular'
                      textSize='mini'
                      textWeight={500}
                    />
                    <Button
                      onPress={() => {
                        this.setState({ sort: 'popular' })
                      }}
                      toggle
                      compact='center'
                      borderRadius={WP2}
                      toggleActive={sort === 'popular'}
                      toggleActiveColor={WHITE}
                      toggleInactiveColor={WHITE}
                      toggleInactiveTextColor={SHIP_GREY}
                      backgroundColor={sort === 'popular' ? GREEN30 : GREY_BORDER}
                      shadow='none'
                      text='Terpopular'
                      textType='Circular'
                      textSize='mini'
                      textWeight={500}
                    />
                  </View>
                  <View style={{ width: WP100, height: 1, backgroundColor: GREY_BORDER }} />
                  <Text style={{ paddingHorizontal: WP6, paddingTop: HP2 }} type='Circular' weight={400} size='mini' color={SHIP_GREY}>Filter Kolaborasi berdasarkan </Text>
                  <View style={{ paddingHorizontal: WP6, flexDirection: 'row' }}>
                    <Button
                      onPress={() => {
                        this.setState({ filter: 'all' })
                      }}
                      toggle
                      compact='center'
                      borderRadius={WP2}
                      toggleActive={filter === 'all'}
                      toggleActiveColor={WHITE}
                      toggleInactiveColor={WHITE}
                      toggleInactiveTextColor={SHIP_GREY}
                      backgroundColor={filter === 'all' ? GREEN30 : GREY_BORDER}
                      shadow='none'
                      text='Semua'
                      textType='Circular'
                      textSize='mini'
                      textWeight={500}
                    />
                    <Button
                      onPress={() => {
                        this.setState({ filter: 'location' })
                      }}
                      toggle
                      compact='center'
                      borderRadius={WP2}
                      toggleActive={filter === 'location'}
                      toggleActiveColor={WHITE}
                      toggleInactiveColor={WHITE}
                      toggleInactiveTextColor={SHIP_GREY}
                      backgroundColor={filter === 'location' ? GREEN30 : GREY_BORDER}
                      shadow='none'
                      text='Kota'
                      textType='Circular'
                      textSize='mini'
                      textWeight={500}
                    />
                    <Button
                      onPress={() => {
                        this.setState({ filter: 'profession' })
                      }}
                      toggle
                      compact='center'
                      borderRadius={WP2}
                      toggleActive={filter === 'profession'}
                      toggleActiveColor={WHITE}
                      toggleInactiveColor={WHITE}
                      toggleInactiveTextColor={SHIP_GREY}
                      backgroundColor={filter === 'profession' ? GREEN30 : GREY_BORDER}
                      shadow='none'
                      text='Profesi'
                      textType='Circular'
                      textSize='mini'
                      textWeight={500}
                    />
                  </View>

                  <Button
                    style={{ position: 'absolute', bottom: HP5, width: WP90, marginHorizontal: WP5 }}
                    onPress={() => {
                      this.setState({ result: [] }, async () => {
                        await this._getContent({ start: 0, limit: 10 }, false, false)
                        closeBottomSheet()
                      })
                    }}
                    backgroundColor={GREEN30}
                    centered
                    bottomButton
                    radius={WP2}
                    shadow='none'
                    textType='Circular'
                    textSize='small'
                    textColor={WHITE}
                    textWeight={500}
                    text='Lihat Kolaborasi'
                  />
                </View>
              )}
            />
          </>)}
        renderHeader={() => (
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: WP4,
            paddingVertical: WP2,
            backgroundColor: WHITE,
          }}
          >
            <Icon
              centered
              onPress={() => navigateBack()}
              background='dark-circle'
              size='large'
              color={SHIP_GREY_CALM}
              name='chevron-left'
              type='Entypo'
            />
            <Text type='Circular' size='mini' weight={400} color={SHIP_GREY} centered>Collaboration List</Text>
            <Modal
              position='bottom'
              swipeDirection='none'
              renderModalContent={({ toggleModal }) => {
                return (
                  <SoundfrenExploreOptions
                    menuOptions={CollaborationOptionMenus}
                    userData={this.props.userData}
                    navigateTo={navigateTo}
                    onClose={toggleModal}
                    onCreate={() => this._createCollab()}
                  />
                )
              }}
            >
              {
                ({ toggleModal }, M) => (
                  <Fragment>
                    <Icon
                      centered
                      onPress={toggleModal}
                      background='dark-circle'
                      size='large'
                      color={SHIP_GREY_CALM}
                      name='dots-three-horizontal'
                      type='Entypo'
                    />
                    {M}
                  </Fragment>
                )
              }
            </Modal>
          </View>
        )}
      >
        <View>
          {this._headerSection(collabConfig)}
          <FlatList
            bounces={false}
            bouncesZoom={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: HP5 }}
            style={{ flexGrow: 0 }}
            data={dataList}
            extraData={dataList}
            keyExtractor={(item) => item.id_ads}
            scrollEnabled={false}
            renderItem={({ item, index }) => (
              this._adItem(item, index, section, isLoading)
            )}
            ListEmptyComponent={(
              <View style={{ alignItems: 'center', marginTop: HP2 }}>
                <Text type='Circular' centered size='small' weight={400} color={SHIP_GREY_CALM}>{collabConfig.empty_message}</Text>
              </View>
            )}
          />
        </View>
      </Container>
    )
  }
}

CollaborationListScreen.propTypes = {}

CollaborationListScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(CollaborationListScreen),
  mapFromNavigationParam
)
