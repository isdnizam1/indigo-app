import React, { Component, Fragment } from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { isEmpty, noop } from 'lodash-es'
import { _enhancedNavigation, Text, Icon, Container } from '../../../components'
import { getAdsCollab, postLogCreateCollab } from '../../../actions/api'
import { WHITE, SHIP_GREY_CALM, SHIP_GREY } from '../../../constants/Colors'
import { WP4, WP2, WP100, WP3, HP5 } from '../../../constants/Sizes'
import { TOUCH_OPACITY } from '../../../constants/Styles'
import ColalboratorItem from '../../../components/collaboration/ColalboratorItem'
import SoundfrenExploreOptions from '../../../components/explore/SoundfrenExploreOptions'
import Modal from '../../../components/Modal'
import { CollaborationOptionMenus } from './CollaborationOptionMenus'

const collabConfig = {
  empty_message: 'Maaf, untuk saat ini belum ada\ncollaborator yang tersedia'
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = () => ({
})

class CollaboratorListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: true,
      isLoading: true,
      result: {},
    }
  }

  componentDidMount = () => {
    this._getContent({ start: 0, limit: 10 }, false, false)
  }

  _getContent = async (params, loadMore = false, isLoading = false) => {
    const {
      dispatch,
      userData: {
        id_user
      }
    } = this.props

    if (!loadMore) {
      this.setState({
        isLoading: true
      })
    }

    try {
      if (!params) params = { start: 0, limit: 10 }

      const { result } = await dispatch(getAdsCollab, {
        id_user,
        show: 'collaborator',
        ...params
      }, noop, true, isLoading)

      const dataResult = result ? result.collaborator || [] : []

      if (!loadMore) {
        this.setState({
          result: dataResult,
          isLoading: false
        })
      } else {
        this.setState({
          result: concat(this.state.result, dataResult || []),
          isLoading: false
        })
      }
    } catch (e) {
      this.setState({ isLoading: false })
    }
  }

  _createCollab = () => {
    const { navigateTo } = this.props
    Promise.resolve(navigateTo('CollaborationForm')).then(() => {
      postLogCreateCollab({ id_user: this.props.userData.id_user })
    })
  }

  _adItem = (ads, i, section, loading) => {
    const { navigateTo } = this.props

    return (
      <TouchableOpacity
        disabled={loading}
        style={{ paddingLeft: WP3, paddingVertical: WP2, width: WP100 }}
        activeOpacity={TOUCH_OPACITY}
        onPress={() => navigateTo('ProfileScreen', { idUser: ads.id_user, navigateBackOnDone: true })}
      >
        <ColalboratorItem myId={this.props.userData.id_user} ads={ads} loading={loading} simpleListItem={true} />
      </TouchableOpacity>
    )
  }

  render() {
    const {
      isReady,
      isLoading,
      result
    } = this.state
    const {
      navigateBack,
      navigateTo
    } = this.props
    const section = 'collaborator'

    const dummyData = [1, 2, 3]
    const dataList = isLoading ? dummyData : !isEmpty(result) ? result : []

    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={() => this._getContent({ start: 0, limit: 10 }, false, false)}
        onPullUpToLoad={async () => {
          await this._getContent({ start: result.length, limit: 10 }, true, false)
        }}
        isReady={isReady}
        isLoading={false}
        renderHeader={() => (
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: WP4,
            paddingVertical: WP2,
            backgroundColor: WHITE,
          }}
          >
            <Icon
              centered
              onPress={() => navigateBack()}
              background='dark-circle'
              size='large'
              color={SHIP_GREY_CALM}
              name='chevron-left'
              type='Entypo'
            />
            <Text type='Circular' size='mini' weight={400} color={SHIP_GREY} centered>Top Collaborators</Text>
            <Modal
              position='bottom'
              swipeDirection='none'
              renderModalContent={({ toggleModal }) => {
                return (
                  <SoundfrenExploreOptions
                    menuOptions={CollaborationOptionMenus}
                    userData={this.props.userData}
                    navigateTo={navigateTo}
                    onClose={toggleModal}
                    onCreate={() => this._createCollab()}
                  />
                )
              }}
            >
              {
                ({ toggleModal }, M) => (
                  <Fragment>
                    <Icon
                      centered
                      onPress={toggleModal}
                      background='dark-circle'
                      size='large'
                      color={SHIP_GREY_CALM}
                      name='dots-three-horizontal'
                      type='Entypo'
                    />
                    {M}
                  </Fragment>
                )
              }
            </Modal>
          </View>
        )}
      >
        <View>
          <FlatList
            bounces={false}
            bouncesZoom={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: HP5 }}
            style={{ flexGrow: 0 }}
            data={dataList}
            extraData={dataList}
            keyExtractor={(item) => item.id_user}
            renderItem={({ item, index }) => (
              this._adItem(item, index, section, isLoading)
            )}
            scrollEnabled={false}
            ListEmptyComponent={(
              <View style={{ alignItems: 'center' }}>
                <Text type='Circular' size='small' weight={400} color={SHIP_GREY_CALM}>{collabConfig.empty_message}</Text>
              </View>
            )}
          />
        </View>
      </Container>
    )
  }
}

CollaboratorListScreen.propTypes = {}

CollaboratorListScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(CollaboratorListScreen),
  mapFromNavigationParam
)
