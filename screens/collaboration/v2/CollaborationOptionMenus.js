import { isFunction } from 'lodash-es'
import { ORANGE_BRIGHT } from '../../../constants/Colors'
import { FONTS } from '../../../constants/Fonts'

export const CollaborationOptionMenus = (props) => {
  let menu = [
    // {
    //   type: 'menu',
    //   image: require('../../../assets/icons/badgeSoundfrenPremium.png'),
    //   imageStyle: {},
    //   name: 'Premium',
    //   textStyle: { color: ORANGE_BRIGHT, fontFamily: FONTS.Circular['500'] },
    //   onPress: () => { },
    //   joinButton: false,
    //   isPremiumMenu: true
    // },
    {
      type: 'menu',
      image: require('../../../assets/icons/icAboutShipGrey.png'),
      imageStyle: {},
      name: 'Tentang Collaboration',
      textStyle: {},
      onPress: () => {
        props.onClose()
      }
    },
    {
      type: 'menu',
      image: require('../../../assets/icons/mdi_chat.png'),
      imageStyle: {},
      name: 'Feedback & Report',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('FeedbackScreen')
      }
    },
    {
      type: 'separator'
    },
    {
      type: 'menu',
      image: require('../../../assets/icons/close.png'),
      imageStyle: {},
      name: 'Tutup',
      textStyle: {},
      onPress: props.onClose
    },
  ]
  if (isFunction(props.onShare)) menu.splice(1, 0, {
    type: 'menu',
    image: require('../../../assets/icons/mdi_share.png'),
    imageStyle: {},
    name: 'Share',
    textStyle: {},
    onPress: () => {
      props.onClose()
      props.onShare()
    }
  })
  if (isFunction(props.onCreate)) menu.splice(1, 0, {
    type: 'menu',
    image: require('../../../assets/icons/icPlusShipGrey.png'),
    imageStyle: {},
    name: 'Mulai Kolaborasi',
    textStyle: {},
    onPress: () => {
      props.onClose()
      props.onCreate()
    }
  })
  return {
    withPromoteUser: true,
    menus: menu
  }

}

export default CollaborationOptionMenus
