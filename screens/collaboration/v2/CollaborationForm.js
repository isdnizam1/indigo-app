import React from 'react'
import { connect } from 'react-redux'
import {
  Alert,
  BackHandler,
  Image as RNImage,
  Clipboard,
  TouchableOpacity,
  View,
  Animated,
  StyleSheet,
  Easing,
  ScrollView,
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { map, noop, startCase, toLower, isEmpty } from 'lodash-es'
import { setClearBlueMessage } from 'sf-services/messagebar/actionDispatcher'
import Touchable from 'sf-components/Touchable'
import {
  _enhancedNavigation,
  Container,
  Form,
  Icon,
  Image,
  InputTextLight,
  InputDate,
  MenuOptions,
  Text,
  ModalMessageView,
  SelectModalV3,
  Avatar,
} from '../../../components'
import {
  WHITE,
  GUN_METAL,
  SHIP_GREY_CALM,
  REDDISH,
  PALE_GREY,
  REDDISH_LIGHT,
  SHIP_GREY,
  PALE_SALMON,
  PALE_BLUE_TWO,
  PALE_LIGHT_BLUE_TWO,
  PALE_BLUE,
  SILVER_TWO,
  PALE_GREY_TWO,
} from '../../../constants/Colors'
import {
  FONT_SIZE,
  WP1,
  WP100,
  WP3,
  WP4,
  WP6,
  WP8,
  WP305,
  WP2,
  WP5,
  WP05,
  WP205,
  WP7,
  WP15,
} from '../../../constants/Sizes'
import { selectPhoto, takePhoto } from '../../../utils/upload'
import {
  convertBlobToBase64,
  fetchAsBlob,
  isIOS,
  isPremiumUser,
  isValidUrl,
} from '../../../utils/helper'
import {
  getAdsDetail,
  getCity,
  getJob,
  getSettingDetail,
  putCollabPost,
  getSearchSuggestion,
  postCollabPost,
} from '../../../actions/api'
import { objectMapper } from '../../../utils/mapper'
import { TOUCH_OPACITY } from '../../../constants/Styles'
import { paymentDispatcher } from '../../../services/payment'
import { FORM_VALIDATION, MAPPER_FOR_API, MAPPER_FROM_API } from '../_constants'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
  setClearBlueMessage,
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  id: getParam('id', 0),
})

const propsDefault = {}

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: '100%',
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 6,
  },
})

const Card = ({ children, maxLength, border = true, backgroundColor = WHITE, style }) => (
  <View
    style={{
      backgroundColor,
      paddingHorizontal: WP4,
      paddingTop: WP4,
      paddingBottom: maxLength ? WP4 : WP6,
      borderBottomColor: PALE_BLUE,
      borderBottomWidth: border ? 1 : 0,
      ...style,
    }}
  >
    {children}
  </View>
)

const ButtonIcon = ({
  onPress = () => {},
  title,
  iconName,
  iconType = 'MaterialCommunityIcons',
}) => (
  <Touchable
    onPress={onPress}
    activeOpacity={TOUCH_OPACITY}
    style={{ flexDirection: 'row', alignItems: 'center' }}
  >
    <Icon
      centered
      background='dark-circle'
      size='small'
      color={REDDISH}
      name={iconName}
      type={iconType}
    />
    <Text type='Circular' size='xmini' weight={400} color={REDDISH}>
      {title}
    </Text>
  </Touchable>
)

class CollaborationForm extends React.Component {
  _didFocusSubscription;
  _willBlurSubscription;

  specificationRef = React.createRef();
  scrollView = React.createRef();
  constructor(props) {
    super(props)
    this._didFocusSubscription = props.navigation.addListener('focus', (payload) =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler),
    )
  }

  state = {
    collaboration: {},
    collaborationDurations: [],
    isReady: !this.props.id,
    backModal: false,
    suggestionProfesi: [],
    totalStep: 3,
    step: 0,
    progress: new Animated.Value(0),
    scrollEnabled: false,
    isSubmiting: false,
  };

  async componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('blur', (payload) =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler),
    )
    await this._animatedProgress(1)
    await this.setState({ step: this.state.step + 1 })
    await this._getSearchSuggestion()
    await this._getInitialScreenData()
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _getSearchSuggestion = async () => {
    const { dispatch } = this.props
    try {
      let params = { setting_name: 'collaboration', key_name: 'search_suggestion' }
      const { result } = await dispatch(getSearchSuggestion, params, noop, true, false)
      await this.setState({
        suggestionProfesi: JSON.parse(result.value),
      })
    } catch (e) {
      // silent
    }
  };

  _getInitialScreenData = async (...args) => {
    await this._getCollaborationDuration(...args)
    if (this.props.id) {
      await this._getCollaborationDetail(...args)
    }
    await this.setState({
      isReady: true,
    })
  };

  _getCollaborationDetail = async (isLoading = true) => {
    const {
      dispatch,
      userData: { id_user },
      id,
    } = this.props
    if (id) {
      try {
        const dataResponse = await dispatch(
          getAdsDetail,
          { id_user, id_ads: id },
          noop,
          false,
          isLoading,
        )
        const bannerPictureb64 = !isEmpty(dataResponse.image)
          ? await fetchAsBlob(dataResponse.image).then(convertBlobToBase64)
          : ''
        const collaborationDataFromApi = {
          ...dataResponse,
          bannerPictureb64,
          additional_data: JSON.parse(dataResponse.additional_data),
        }
        await this.setState({
          collaboration: objectMapper(collaborationDataFromApi, MAPPER_FROM_API),
        })
      } catch (error) {
        // silend
      }
    }
  };

  _getCollaborationDuration = async (isLoading = true) => {
    const { dispatch } = this.props
    const dataResponse = await dispatch(
      getSettingDetail,
      {
        setting_name: 'ads',
        key_name: 'post_duration',
      },
      noop,
      false,
      isLoading,
    )
    let collaborationDurations = JSON.parse(dataResponse.value)
    this.setState({
      collaborationDurations,
    })
  };

  _backHandler = async () => {
    const { backModal, step } = this.state
    if (step <= 1) {
      this.setState({ backModal: !backModal })
    } else {
      this.setState({ step: step - 1 }, this.startScroll(step - 1))
    }
  };

  _postCollaboration = ({ formData }) => {
    const { id, userData, dispatch, popToTop, navigateTo } = this.props
    const { isSubmiting } = this.state
    !isSubmiting &&
      this.setState({ isSubmiting: true }, () => {
        if (id) {
          dispatch(putCollabPost, {
            ...objectMapper(formData, MAPPER_FOR_API),
            id_user: userData.id_user,
            id_ads: id,
          }).then(() => {
            this.props.setClearBlueMessage('Project kolaborasi berhasil diperbarui')
            Promise.all()
            popToTop()
            navigateTo('NotificationScreen', { defaultTab: 'activities' }, 'push')
          })
        } else {
          const params = {
            ...objectMapper(formData, MAPPER_FOR_API),
            banner_picture: !isEmpty(formData.bannerPictureb64) ? formData.bannerPictureb64 : '',
            id_user: userData.id_user,
          }
          dispatch(postCollabPost, params).then((data) => {
            this.props.setClearBlueMessage('Project kolaborasi berhasil dibuat')
            popToTop()
            navigateTo('NotificationScreen', { defaultTab: 'activities', review: true }, 'push')
          })
        }
      })
  };

  _getSelectedOption = (selectedValue, value) =>
    toLower(selectedValue) === value
      ? {
          container: {
            backgroundColor: REDDISH_LIGHT,
            borderColor: REDDISH,
          },
          text: {
            color: WHITE,
          },
        }
      : {
          container: {
            backgroundColor: WHITE,
            borderColor: PALE_BLUE_TWO,
          },
          text: {
            color: SHIP_GREY,
          },
        };

  _headerSection = (onSubmit, isDisable) => {
    const { step, isSubmiting } = this.state
    const { id } = this.props
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: WP5,
            paddingVertical: WP2,
            backgroundColor: WHITE,
          }}
        >
          <View style={{ width: WP15 }}>
            <Icon
              onPress={() => this._backHandler()}
              background='dark-circle'
              size='large'
              color={SHIP_GREY_CALM}
              name='chevron-left'
              type='Entypo'
            />
          </View>
          <Text type='Circular' size='mini' weight={400} color={SHIP_GREY} centered>
            {id ? 'Edit Project' : 'Create Project'}
          </Text>
          <Touchable
            style={{ width: WP15, alignContent: 'flex-end' }}
            disabled={isDisable || isSubmiting}
            onPress={() => {
              if (step <= 1 && !id) {
                this.setState({ step: step + 1 }, this.startScroll(step + 1))
              } else {
                !isSubmiting && onSubmit()
              }
            }}
          >
            <Text
              type='Circular'
              size='mini'
              weight={400}
              color={isDisable || isSubmiting ? BLUE10 : REDDISH}
              style={{ textAlign: 'right' }}
            >
              {id ? 'Save' : step <= 1 ? 'Next' : 'Publish'}
            </Text>
          </Touchable>
        </View>
        {this._progress()}
      </View>
    )
  };

  _progress = () => {
    return (
      !this.props.id && (
        <View style={{ width: WP100, height: WP05, backgroundColor: PALE_GREY }}>
          <Animated.View
            style={{ height: WP05, backgroundColor: REDDISH, width: this.state.progress }}
          />
        </View>
      )
    )
  };

  _animatedProgress = (step) => {
    Animated.timing(this.state.progress, {
      toValue: (WP100 / this.state.totalStep) * step,
      duration: 350,
      delay: 500,
      easing: Easing.ease,
      useNativeDriver: false,
    }).start()
  };

  _customLabel = (title, subtitle, marginBottom = WP1) => {
    return (
      <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom }}>
        {title}
        {subtitle && (
          <Text weight={300} type='Circular' size={'xmini'} color={PALE_LIGHT_BLUE_TWO}>
            {' '}
            {subtitle}
          </Text>
        )}
      </Text>
    )
  };

  _bannerSection = (onChange, formData) => {
    const { id } = this.props
    return (
      <Card maxLength backgroundColor={PALE_GREY_TWO} style={{ paddingTop: id ? WP6 : WP7 }}>
        <Image
          source={
            isEmpty(formData.bannerPictureUri)
              ? require('../../../assets/images/bgDefaultCollab.png')
              : { uri: formData.bannerPictureUri }
          }
          size={WP100 - WP8}
          aspectRatio={328 / 184}
          imageStyle={{ borderRadius: 8 }}
          style={{ backgroundColor: SHIP_GREY_CALM, borderRadius: 8, marginBottom: WP4 }}
        />
        <MenuOptions
          options={[
            {
              onPress: selectPhoto(onChange, 'bannerPicture', [328, 184]),
              title: 'Select from library',
              iconName: 'image',
            },
            {
              onPress: takePhoto(onChange, 'bannerPicture', [328, 184]),
              title: 'Take a picture',
              iconName: 'camera',
            },
       
          ]}
          triggerComponent={(toggleModal) => (
            <ButtonIcon onPress={toggleModal} iconName={'camera-plus'} title={'Ubah Banner'} />
          )}
        />
      </Card>
    )
  };

  _titleSection = (onChange, formData) => {
    const { userData, id } = this.props
    return (
      <Card maxLength>
        {id ? this._customLabel('Judul Kolaborasi') : null}
        <View style={{ flexDirection: 'row' }}>
          {!id && (
            <Avatar
              shadow={false}
              image={userData.profile_picture}
              size='xsmall'
              style={{ marginRight: WP2 }}
            />
          )}
          <InputTextLight
            onChangeText={onChange('title')}
            value={formData.title}
            placeholder='Tulis judul kolaborasimu'
            placeholderTextColor={SILVER_TWO}
            color={SHIP_GREY}
            bordered
            size='mini'
            type='Circular'
            returnKeyType={'next'}
            wording=' '
            maxLength={70}
            maxLengthFocus
            multiline
            maxLine={2}
            lineHeight={1}
            style={{ marginTop: 0, marginBottom: 0 }}
            textInputStyle={styles.input}
          />
        </View>
      </Card>
    )
  };

  _profesiCatatanSection = (onChange, formData) => {
    const { id } = this.props
    const { suggestionProfesi } = this.state
    return (
      <Card maxLength>
        {/* profesi section */}
        <View>
          {this._customLabel('Cari Profesi:', '', WP2)}
          {/* selection profesi */}
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {map(formData.professions, (profession, index) => (
              <View
                style={{
                  marginRight: WP2,
                  marginBottom: WP2,
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingHorizontal: WP3,
                  paddingVertical: WP2,
                  borderRadius: 6,
                  backgroundColor: WHITE,
                  borderWidth: 1,
                  borderColor: PALE_BLUE_TWO,
                }}
              >
                <Text
                  size='xmini'
                  type='Circular'
                  weight={400}
                  color={SHIP_GREY}
                  style={{ marginRight: WP205 }}
                >
                  {profession?.name}
                </Text>
                <Icon
                  onPress={() => {
                    const newProfessions = formData.professions
                    newProfessions.splice(index, 1)
                    onChange('professions')(newProfessions)
                  }}
                  centered
                  size='small'
                  color={PALE_BLUE_TWO}
                  name={'close'}
                  type='MaterialCommunityIcons'
                />
              </View>
            ))}
          </View>
          {/* button profesi */}
          <SelectModalV3
            refreshOnSelect
            triggerComponent={
              <ButtonIcon
                iconName={id ? 'pencil' : 'plus'}
                title={id ? 'Ubah Profesi' : 'Tambahkan Profesi'}
              />
            }
            header='Cari Profesi'
            suggestion={getJob}
            suggestionKey='job_title'
            suggestionPathResult='job_title'
            onChange={(value) => {
              const newProfessions = formData.professions
              newProfessions.push({ name: value })
              onChange('professions')(newProfessions)
            }}
            createNew={false}
            placeholder='Coba cari Profesi'
            selection={formData.professions}
            selectionKey='name'
            selectionWording='Posisi Terpilih'
            showSelection
            onRemoveSelection={(index) => {
              const newProfessions = formData.professions
              newProfessions.splice(index, 1)
              onChange('professions')(newProfessions)
            }}
            quickSearchTitle={'Saran Pencarian Profesi'}
            quickSearchList={suggestionProfesi}
            actionNext={() => this.specificationRef.current.focus()}
          />
        </View>
        {/* line section */}
        <View
          style={{ width: '100%', height: 1, marginVertical: WP4, backgroundColor: PALE_GREY }}
        />
        {/* catatan section */}
        {this._customLabel('Catatan')}
        <InputTextLight
          innerRef={this.specificationRef}
          onChangeText={onChange('specification')}
          value={formData.specification}
          placeholder={'Contoh: UI/UX Designer'}
          placeholderTextColor={SILVER_TWO}
          color={SHIP_GREY}
          bordered
          size='mini'
          type='Circular'
          returnKeyType={'next'}
          wording=' '
          maxLength={100}
          maxLengthFocus
          multiline
          lineHeight={1}
          style={{ marginTop: 0, marginBottom: 0 }}
          textInputStyle={styles.input}
        />
      </Card>
    )
  };

  _locationSection = (onChange, formData) => {
    return (
      <Card>
        {this._customLabel('Kota/Lokasi')}
        <SelectModalV3
          refreshOnSelect
          triggerComponent={
            <View style={styles.input}>
              <Text
                type='Circular'
                size='mini'
                weight={300}
                color={isEmpty(formData.locationName) ? SILVER_TWO : SHIP_GREY_CALM}
              >
                {isEmpty(formData.locationName) ? 'Pilih Kota/Lokasi' : formData.locationName}
              </Text>
            </View>
          }
          header='Kota/Lokasi'
          suggestion={getCity}
          suggestionKey='city_name'
          suggestionPathResult='city_name'
          onChange={onChange('locationName')}
          createNew={false}
          reformatFromApi={(text) => startCase(toLower(text))}
          placeholder='Coba cari Kota'
          selection={formData.locationName}
          showSelection
          onRemoveSelection={() => onChange('locationName')('')}
        />
      </Card>
    )
  };

  _durationSection = (onChange, formData) => {
    const { userData, id, navigateTo, paymentSourceSet } = this.props
    const { collaborationDurations } = this.state
    return (
      !id && (
        <Card border={false}>
          {this._customLabel('Durasi Project')}
          <View style={{ flexDirection: 'row' }}>
            {map(collaborationDurations, (duration, i) => (
              <Touchable
                activeOpacity={TOUCH_OPACITY}
                onPress={() => {
                  if (formData.postDuration == duration.duration) {
                    onChange('postDuration')('')
                  } else {
                    if (duration.is_premium && !isPremiumUser(userData.account_type)) {
                      if (isIOS())
                        Alert.alert('Sorry, this option cannot be used on iOS. Please stay tuned.')
                      else {
                      onChange('postDuration')(duration.duration)
                        // paymentSourceSet('collaboration')
                        // navigateTo('MembershipScreen')
                      }
                    } else {
                      onChange('postDuration')(duration.duration)
                    }
                  }
                }}
                style={{
                  paddingVertical: WP2,
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderColor: PALE_BLUE_TWO,
                  borderWidth: 1,
                  borderTopStartRadius: i === 0 ? 6 : undefined,
                  borderBottomStartRadius: i === 0 ? 6 : undefined,
                  borderTopEndRadius: i === collaborationDurations.length - 1 ? 6 : undefined,
                  borderBottomEndRadius: i === collaborationDurations.length - 1 ? 6 : undefined,
                  ...this._getSelectedOption(formData.postDuration, duration.duration).container,
                }}
              >
                {/* {Boolean(duration.is_premium) && (
                  <RNImage
                    style={{
                      width: FONT_SIZE['mini'],
                      height: FONT_SIZE['mini'],
                      marginRight: WP1,
                    }}
                    source={require('../../../assets/icons/badge.png')}
                  />
                )} */}
                <Text
                  centered
                  weight={400}
                  size='xmini'
                  type='Circular'
                  {...this._getSelectedOption(formData.postDuration, duration.duration).text}
                >
                  {duration.duration}
                </Text>
              </Touchable>
            ))}
          </View>
        </Card>
      )
    )
  };



  _durationSection2 = (onChange, formData) => {
    const { userData, id, navigateTo, paymentSourceSet } = this.props
    const { collaborationDurations } = this.state
    return (
      !id && (
        <Card border={false}>
          {this._customLabel('Durasi Project')}
          <InputDate
              labelWeight={400}
              placeholder='Start Date'
              onChangeText={onChange('startDate')}
              value={formData.startDate}
              label=''
              placeholderTextColor={SILVER_TWO}
              color={SHIP_GREY}
              bordered
              size='mini'
              type='Circular'
              lineHeight={1}
              style={{ marginTop: 0, marginBottom: 0 }}
              textInputStyle={styles.input}
            />
            <InputDate
              labelWeight={400}
              placeholder='End Date'
              label=''
              onChangeText={onChange('endDate')}
              value={formData.endDate}
              placeholderTextColor={SILVER_TWO}
              color={SHIP_GREY}
              bordered
              size='mini'
              type='Circular'
              lineHeight={1}
              style={{ marginTop: 10, marginBottom: 0 }}
              textInputStyle={styles.input}
            />
        </Card>
      )
    )
  };

  _aboutSection = (onChange, formData) => {
    return (
      <Card maxLength>
        {this._customLabel('Tentang Kolaborasi')}
        <InputTextLight
          onChangeText={onChange('description')}
          value={formData.description}
          placeholder={'Tuliskan deskripsi tentang kolaborasimu'}
          placeholderTextColor={SILVER_TWO}
          color={SHIP_GREY}
          bordered
          size='mini'
          type='Circular'
          returnKeyType={'next'}
          wording=' '
          maxLength={250}
          maxLengthFocus
          multiline
          lineHeight={1}
          style={{ marginTop: 0, marginBottom: 0 }}
          textInputStyle={styles.input}
        />
      </Card>
    )
  };

  _referenceSection = (onChange, formData) => {
    return (
      <Card border={false}>
        {this._customLabel('Referensi kolaborasi', '(opsional)')}
        <InputTextLight
          onChangeText={onChange('collaborationReference')}
          value={formData.collaborationReference}
          placeholder='Masukkan link/url referensi kolaborasimu'
          placeholderTextColor={SILVER_TWO}
          color={SHIP_GREY}
          bordered
          size='mini'
          type='Circular'
          returnKeyType={'done'}
          style={{ marginTop: 0, marginBottom: 0 }}
          textInputStyle={styles.input}
          containerStyle={{ height: WP5 }}
        />
        <View
          style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
        >
          <ButtonIcon
            onPress={async () => {
              const text = await Clipboard.getString()
              onChange('collaborationReference')(text)
            }}
            iconName={'link'}
            title={'Paste link'}
          />
          {!isValidUrl(formData.collaborationReference) &&
            !isEmpty(formData.collaborationReference) && (
              <Text type='Circular' size='tiny' weight={300} color={SHIP_GREY_CALM}>
                URL Tidak Valid
              </Text>
            )}
        </View>
      </Card>
    )
  };
  _memberQuotaSection = (onChange, formData) => {
    return (
      <Card border={false}>
        {this._customLabel('Member Quota', '')}
        <InputTextLight
          onChangeText={onChange('memberQuota')}
          value={formData.memberQuota}
          keyboardType='numeric'
          placeholder='Enter the number of member'
          placeholderTextColor={SILVER_TWO}
          color={SHIP_GREY}
          bordered
          size='mini'
          type='Circular'
          returnKeyType={'done'}
          style={{ marginTop: 0, marginBottom: 0 }}
          textInputStyle={styles.input}
          containerStyle={{ height: WP5 }}
        />
      </Card>
    )
  };

  lockScrollview() {
    this.setState({ scrollEnabled: false })
  }

  startScroll = (step) => {
    this.setState(
      {
        scrollEnabled: true,
      },
      () => {
        try {
          setTimeout(() => {
            try {
              this.scrollView.scrollTo({
                x: WP100 * (step - 1),
                y: 0,
                animated: true,
              })
              this._animatedProgress(step)
            } catch (e) {
              // Keep silent
            } finally {
              setTimeout(() => this.lockScrollview(), 500)
            }
          }, 250)
        } catch (e) {
          // keep silent
        }
      },
    )
  };

  render() {
    const { id, navigateBack } = this.props
    const { isReady, collaboration, backModal, step, scrollEnabled, isSubmiting } = this.state
    return (
      <Form
        ref={(form) => (this.form = form)}
        validation={FORM_VALIDATION}
        initialValue={{ professions: [], ...collaboration }}
        onSubmit={this._postCollaboration}
      >
        {({ onChange, onSubmit, formData, isValid, isDirty }) => {
          const isDisable =
            step <= 1 && !id
              ? !isValid ||
                !isDirty ||
                (!isEmpty(formData.collaborationReference) &&
                  !isValidUrl(formData.collaborationReference))
              : !isValid ||
                !isDirty ||
                isEmpty(formData.description) ||
                (!isEmpty(formData.collaborationReference) &&
                  !isValidUrl(formData.collaborationReference))
          return (
            <Container
              isReady={isReady}
              theme='dark'
              scrollable={false}
              renderHeader={() => (
                <View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingHorizontal: WP5,
                      paddingVertical: WP2,
                      backgroundColor: WHITE,
                    }}
                  >
                    <View style={{ width: WP15 }}>
                      <Icon
                        onPress={() => this._backHandler()}
                        background='dark-circle'
                        size='large'
                        color={SHIP_GREY_CALM}
                        name='chevron-left'
                        type='Entypo'
                      />
                    </View>
                    <Text type='Circular' size='mini' weight={400} color={SHIP_GREY} centered>
                      {id ? 'Edit Project' : 'Create Project'}
                    </Text>
                    <Touchable
                      style={{ width: WP15, alignContent: 'flex-end' }}
                      disabled={isDisable || isSubmiting}
                      onPress={() => {
                        if (step <= 1 && !id) {
                          this.setState({ step: step + 1 }, this.startScroll(step + 1))
                        } else {
                          !isSubmiting && onSubmit()
                        }
                      }}
                    >
                      <Text
                        type='Circular'
                        size='mini'
                        weight={400}
                        color={isDisable || isSubmiting ? PALE_SALMON : REDDISH}
                        style={{ textAlign: 'right' }}
                      >
                        {id ? 'Save' : step <= 1 ? 'Next' : 'Publish'}
                      </Text>
                    </Touchable>
                  </View>
                  {this._progress()}
                </View>
              )}
            >
              <ModalMessageView
                style={{ width: WP100 - WP8 }}
                contentStyle={{ paddingHorizontal: WP4 }}
                toggleModal={() => this.setState({ backModal: false })}
                isVisible={backModal}
                title={id ? 'Edit Project' : 'Project Collaboration'}
                titleType='Circular'
                titleSize={'small'}
                titleColor={GUN_METAL}
                titleStyle={{ marginBottom: WP4 }}
                subtitle={
                  id
                    ? isDirty
                      ? 'Beberapa informasi project telah diubah.\nSimpan perubahan?'
                      : 'Kamu belum mengubah informasi project.\nLanjutkan untuk mengubah?'
                    : 'Pembuatan projekmu sedang berlangsung saat ini.\nYakin untuk melanjutkan?'
                }
                subtitleType='Circular'
                subtitleSize={'xmini'}
                subtitleWeight={400}
                subtitleColor={SHIP_GREY_CALM}
                subtitleStyle={{ marginBottom: WP3 }}
                image={null}
                buttonPrimaryText={
                  id ? (isDirty ? 'Simpan Perubahan' : 'Ubah Informasi') : 'Lanjutkan Project'
                }
                buttonPrimaryContentStyle={{ borderRadius: 8, paddingVertical: WP305 }}
                buttonPrimaryTextType='Circular'
                buttonPrimaryTextWeight={400}
                buttonPrimaryBgColor={REDDISH}
                buttonPrimaryAction={() => {
                  if (id && !isDisable) onSubmit()
                }}
                buttonSecondaryText={'Batalkan'}
                buttonSecondaryStyle={{
                  backgroundColor: PALE_GREY,
                  marginTop: WP1,
                  borderRadius: 8,
                  paddingVertical: WP305,
                }}
                buttonSecondaryTextType='Circular'
                buttonSecondaryTextWeight={400}
                buttonSecondaryTextColor={SHIP_GREY_CALM}
                buttonSecondaryAction={() => navigateBack()}
              />

              <ScrollView
                scrollEnabled={scrollEnabled}
                showsHorizontalScrollIndicator={false}
                ref={(ref) => {
                  this.scrollView = ref
                }}
                keyboardShouldPersistTaps={'handled'}
                keyboardDismissMode={'none'}
                horizontal
              >
                {id ? (
                  <View style={{ width: WP100, backgroundColor: WHITE }}>
                    <KeyboardAwareScrollView
                      keyboardShouldPersistTaps={'handled'}
                      showsVerticalScrollIndicator={false}
                      bounces={false}
                    >
                      {this._bannerSection(onChange, formData)}
                      {this._titleSection(onChange, formData)}
                      {this._profesiCatatanSection(onChange, formData)}
                      {this._locationSection(onChange, formData)}
                      {this._aboutSection(onChange, formData)}
                      {this._referenceSection(onChange, formData)}
                    </KeyboardAwareScrollView>
                  </View>
                ) : null}

                <View style={{ width: WP100, backgroundColor: WHITE }}>
                  <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={'handled'}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                    contentContainerStyle={{ paddingTop: WP3 }}
                  >
                    {this._titleSection(onChange, formData)}
                    {this._profesiCatatanSection(onChange, formData)}
                    {this._locationSection(onChange, formData)}
                    {this._memberQuotaSection(onChange, formData)}
                    {this._durationSection2(onChange, formData)}
                  </KeyboardAwareScrollView>
                </View>

                <View style={{ width: WP100, backgroundColor: WHITE }}>
                  <KeyboardAwareScrollView
                    keyboardShouldPersistTaps={'handled'}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                  >
                    {this._bannerSection(onChange, formData)}
                    {this._aboutSection(onChange, formData)}
                    {this._referenceSection(onChange, formData)}
                  </KeyboardAwareScrollView>
                </View>
              </ScrollView>
            </Container>
          )
        }}
      </Form>
    )
  }
}

CollaborationForm.navigationOptions = () => ({
  gesturesEnabled: false,
})

CollaborationForm.defaultProps = propsDefault
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CollaborationForm),
  mapFromNavigationParam,
)
