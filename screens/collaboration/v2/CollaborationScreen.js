import React, { Component, Fragment } from "react";
import {
  View,
  TouchableOpacity,
  FlatList,
  Animated,
  Easing,
} from "react-native";
import { connect } from "react-redux";
import { NavigationEvents } from "@react-navigation/compat";
import { isEmpty, noop, startCase, toLower, get } from "lodash-es";
import SkeletonContent from "react-native-skeleton-content";
import {
  _enhancedNavigation,
  Text,
  Icon,
  Container,
  Image,
  SelectModalV2,
} from "sf-components";
import { getAdsCollab, getCity, postLogCreateCollab } from "sf-actions/api";
import {
  WHITE,
  SHIP_GREY_CALM,
  REDDISH,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  PALE_GREY_TWO,
  SHIP_GREY,
  PALE_BLUE,
  NAVY_DARK,
  PALE_GREY,
  PALE_SALMON20,
} from "sf-constants/Colors";
import {
  WP4,
  WP2,
  WP1,
  WP100,
  WP12,
  WP50,
  WP6,
  WP105,
  WP70,
  WP40,
  WP8,
  WP3,
  WP305,
  WP05,
  HP50,
} from "sf-constants/Sizes";
import { TOUCH_OPACITY, SHADOW_STYLE } from "sf-constants/Styles";
import ColalboratorItem from "sf-components/collaboration/ColalboratorItem";
import Layout from "sf-components/collaboration/CollaborationSkeleton";
import CollaborationItem from "sf-components/collaboration/CollaborationItem";
import SoundfrenExploreOptions from "sf-components/explore/SoundfrenExploreOptions";
import Modal from "sf-components/Modal";
import { setCollabData } from "sf-services/screens/lv1/collab/actionDispatcher";
import { setCollabLv1Cache } from "sf-utils/storage";
import { restrictedAction } from "../../../utils/helper";
import { CollaborationOptionMenus } from "./CollaborationOptionMenus";
import EmptyState from "../../../components/EmptyState";

const collabConfig = {
  collaborator: {
    title: "Top Collaborators",
    route: "CollaboratorListScreen",
    key_data: "collaborator",
    line: true,
    horizontal: true,
    numColumns: undefined,
    empty_message: "Maaf, untuk saat ini belum ada\ncollaborator yang tersedia",
  },
  collaboration: {
    title: "Kolaborasi saat ini",
    route: "CollaborationListScreen",
    key_data: "collaboration",
    line: true,
    horizontal: false,
    numColumns: undefined,
    empty_message: "Maaf, untuk saat ini belum ada\nkolaborasi yang tersedia",
    empty_message_title: "Belum ada kolaborasi saat ini",
    empty_message_desc:
      "Mulai kolaborasi dengan partisipan lain sekarang juga !",
  },
};

const mapStateToProps = ({
  auth,
  screen: {
    lv1: { collab },
  },
}) => ({
  userData: auth.user,
  collab,
});

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam("id_ads", 0),
});

class CollaborationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      result: props.collab,
      location: "",
      totalStep: 3,
      step: 2,
      progress: new Animated.Value((WP100 / 3) * 2),
      fadeAnim: new Animated.Value(1),
      scrollEnabled: true,
    };
  }

  componentDidMount = () => {
    if (this.props.id_ads) {
      this.setState({ scrollEnabled: false }, () =>
        this._animatedProgress(this.state.step + 1)
      );
    }
  };

  _getContent = async (location = "") => {
    const {
      dispatch,
      userData: { id_user },
      setCollabData,
    } = this.props;
    try {
      let params = { id_user, show: "all" };
      if (!isEmpty(location)) params.location_name = location;

      const { result } = await dispatch(
        getAdsCollab,
        params,
        noop,
        true,
        false
      );
      console.log("Result", result);
      this.setState(
        {
          result,
        },
        () => {
          setCollabLv1Cache(result);
          setCollabData(result);
        }
      );
    } catch (e) {
      // keep silent
    }
  };

  _onChangeLocation = (value) => {
    this.setState(
      {
        location: value,
      },
      () => this._getContent(value)
    );
  };

  _createCollab = () => {
    const { navigateTo } = this.props;
    Promise.resolve(navigateTo("CollaborationForm")).then(() => {
      postLogCreateCollab({ id_user: this.props.userData.id_user });
    });
  };

  _headerSearch = () => {
    const { navigateBack, navigateTo, userData, navigation } = this.props;
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingHorizontal: WP4,
            paddingVertical: WP2,
            backgroundColor: WHITE,
          }}
        >
          <Icon
            centered
            onPress={() => navigateBack()}
            background="dark-circle"
            size="large"
            color={SHIP_GREY_CALM}
            name="chevron-left"
            type="Entypo"
          />
          <TouchableOpacity
            onPress={restrictedAction({
              action: () => navigateTo("SearchCollaborationScreen"),
              userData,
              navigation,
            })}
            activeOpacity={TOUCH_OPACITY}
            style={{
              flex: 1,
              backgroundColor: "rgba(145, 158, 171, 0.1)",
              borderRadius: 6,
              flexDirection: "row",
              alignItems: "center",
              paddingHorizontal: WP2,
              paddingVertical: WP1,
              marginHorizontal: WP2,
            }}
          >
            <Icon
              centered
              background="dark-circle"
              size="large"
              color={SHIP_GREY_CALM}
              name="magnify"
              type="MaterialCommunityIcons"
              style={{ marginRight: WP1 }}
            />
            <Text
              type="Circular"
              size="mini"
              weight={300}
              color={SHIP_GREY_CALM}
              centered
            >
              Coba cari kolaborasimu
            </Text>
          </TouchableOpacity>
          <Modal
            position="bottom"
            swipeDirection={null}
            renderModalContent={({ toggleModal }) => {
              return (
                <SoundfrenExploreOptions
                  menuOptions={CollaborationOptionMenus}
                  userData={this.props.userData}
                  navigateTo={navigateTo}
                  onClose={toggleModal}
                  onCreate={() => this._createCollab()}
                />
              );
            }}
          >
            {({ toggleModal }, M) => (
              <Fragment>
                <Icon
                  centered
                  onPress={restrictedAction({
                    action: toggleModal,
                    userData,
                    navigation,
                  })}
                  background="dark-circle"
                  size="large"
                  color={SHIP_GREY_CALM}
                  name="dots-three-horizontal"
                  type="Entypo"
                />
                {M}
              </Fragment>
            )}
          </Modal>
        </View>
        {this._progress()}
      </View>
    );
  };

  _progress = () => {
    if (this.props.id_ads && !this.state.scrollEnabled) {
      return (
        <View
          style={{ width: WP100, height: WP05, backgroundColor: PALE_GREY }}
        >
          <Animated.View
            style={{
              height: WP05,
              backgroundColor: REDDISH,
              width: this.state.progress,
            }}
          />
        </View>
      );
    }
  };

  _headerSection = (about) => {
    const { navigateTo, id_ads, userData, navigation } = this.props;
    const { step, totalStep, fadeAnim, result } = this.state;
    const isLoading = !get(result, "about.title");
    return (
      <View>
        <Image
          tint={"black"}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 182}
          source={require("sf-assets/images/bgCollaboration.png")}
        />
        <View
          style={{
            position: "absolute",
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <SkeletonContent
            containerStyle={{ alignItems: "center", paddingHorizontal: WP12 }}
            layout={[
              { width: WP50, height: WP6, marginBottom: WP2 },
              { width: WP70, height: WP4, marginBottom: WP105 },
              { width: WP40, height: WP4, marginBottom: WP6 },
              { width: WP50, height: WP8, borderRadius: 6 },
            ]}
            isLoading={isLoading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <View style={{ alignItems: "center" }}>
              <Text
                type="Circular"
                size="large"
                weight={600}
                color={WHITE}
                centered
                style={{ marginBottom: WP1 }}
              >
                {about?.title}
              </Text>
              <Text
                type="Circular"
                size="mini"
                weight={300}
                color={WHITE}
                centered
              >
                {about?.subtitle}
              </Text>
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={restrictedAction({
                  action: this._createCollab,
                  userData,
                  navigation,
                })}
                style={{
                  width: WP50,
                  backgroundColor: WHITE,
                  borderRadius: 6,
                  justifyContent: "center",
                  alignItems: "center",
                  marginTop: WP6,
                  paddingVertical: WP2,
                  ...SHADOW_STYLE["shadowThin"],
                }}
              >
                <Text
                  type="Circular"
                  size="mini"
                  weight={500}
                  color={REDDISH}
                  centered
                >
                  Mulai Kolaborasi
                </Text>
              </TouchableOpacity>
            </View>
          </SkeletonContent>
        </View>
        {id_ads && !this.state.scrollEnabled ? (
          <Animated.View
            style={{
              position: "absolute",
              left: 0,
              top: 0,
              right: 0,
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              backgroundColor: WHITE,
              paddingVertical: WP305,
              paddingHorizontal: WP4,
              opacity: fadeAnim,
            }}
          >
            <View
              style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            >
              <Text
                weight={300}
                type="Circular"
                size={"mini"}
                color={SHIP_GREY_CALM}
              >
                {step == totalStep
                  ? "Publish Berhasil"
                  : "Publish Project Kolaborasi..."}
              </Text>
              {step == totalStep && (
                <Icon
                  style={{ marginLeft: WP2 }}
                  background="dark-circle"
                  backgroundColor={PALE_SALMON20}
                  size="xtiny"
                  color={REDDISH}
                  name="check-bold"
                  type="MaterialCommunityIcons"
                />
              )}
            </View>
            {step == totalStep ? (
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() => {
                  this._animatedFade(this.state.fadeAnim);
                  navigateTo("CollaborationPreview", { id: id_ads });
                }}
              >
                <Text
                  weight={300}
                  type="Circular"
                  size={"mini"}
                  color={REDDISH}
                >
                  Lihat
                </Text>
              </TouchableOpacity>
            ) : null}
          </Animated.View>
        ) : null}
      </View>
    );
  };

  _locationSection = () => {
    const { location } = this.state;
    return (
      <SelectModalV2
        extraResult
        refreshOnSelect
        triggerComponent={
          <View
            style={{
              width: "100%",
              backgroundColor: PALE_GREY_TWO,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingHorizontal: WP4,
              paddingTop: WP4,
              paddingBottom: WP3,
            }}
          >
            <View
              style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            >
              <Icon
                centered
                background="dark-circle"
                size="small"
                color={REDDISH}
                name="location-on"
                type="MaterialIcons"
              />
              <Text numberOfLines={1} style={{ flex: 1 }}>
                <Text
                  type="Circular"
                  size="mini"
                  weight={300}
                  color={SHIP_GREY}
                >
                  Kolaborasi di:
                </Text>
                <Text
                  type="Circular"
                  size="mini"
                  weight={300}
                  color={REDDISH}
                >{` ${
                  isEmpty(this.state.location)
                    ? "Semua Kota"
                    : this.state.location
                }`}</Text>
              </Text>
            </View>
            <Text type="Circular" size="xmini" weight={400} color={REDDISH}>
              Edit
            </Text>
          </View>
        }
        header="Select City"
        suggestion={getCity}
        suggestionKey="city_name"
        suggestionPathResult="city_name"
        onChange={this._onChangeLocation}
        selected={location}
        reformatFromApi={(text) => startCase(toLower(text))}
        createNew={false}
        placeholder="Search city here..."
      />
    );
  };

  _onPressItem = (data, section) => () => {
    const { navigateTo } = this.props;
    if (section === "collaborator") {
      navigateTo("ProfileScreen", {
        idUser: data.id_user,
        navigateBackOnDone: true,
      });
    } else if (section === "collaboration") {
      navigateTo("CollaborationPreview", { id: data.id_ads });
    }
  };

  _adItem = (ads, i, section, loading) => {
    const { navigateTo, userData, navigation } = this.props;
    const { result } = this.state;
    const isLoading = !get(result, "about.title");
    if (loading && section === "collaborator") {
      const skeleton = Layout.item(section);

      return (
        <View
          key={`${Math.random()}`}
          style={{
            borderRadius: 6,
            marginRight: WP4,
            width: WP40,
            paddingVertical: WP2,
            ...skeleton.wrapperStyle,
          }}
        >
          <SkeletonContent
            containerStyle={{ ...Layout.container, alignItems: "center" }}
            layout={skeleton.layout}
            isLoading={isLoading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          />
        </View>
      );
    }

    return (
      <TouchableOpacity
        disabled={isLoading}
        style={{ paddingVertical: WP2 }}
        key={`${i}${Math.random()}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={restrictedAction({
          action: this._onPressItem(ads, section),
          userData,
          navigation,
        })}
      >
        {section === "collaborator" && (
          <ColalboratorItem
            myId={this.props.userData.id_user}
            ads={ads}
            loading={isLoading}
          />
        )}
        {section === "collaboration" && (
          <CollaborationItem
            ads={ads}
            loading={isLoading}
            navigateTo={navigateTo}
          />
        )}
      </TouchableOpacity>
    );
  };

  _collabSection = (section) => {
    const { result } = this.state;
    const { navigateTo } = this.props;
    const config = collabConfig[section];
    const dummyData = [1, 2, 3];
    const isLoading = !get(result, "about.title");
    const dataList = isLoading
      ? dummyData
      : !isEmpty(result)
      ? result[config.key_data]
      : [];
    if (config.key_data == "collaborator" && isEmpty(dataList)) return null;
    return (
      (!isEmpty(result) || isLoading) && (
        <View
          style={{
            borderTopColor: PALE_BLUE,
            borderTopWidth: config.line ? 1 : 0,
          }}
        >
          <View style={{ paddingVertical: WP6 }}>
            {/* HEADER */}
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                paddingHorizontal: WP4,
              }}
            >
              <Text type="Circular" size="small" color={NAVY_DARK} weight={600}>
                {config.title}
              </Text>
              {!isEmpty(config.route) && (
                <TouchableOpacity
                  activeOpacity={TOUCH_OPACITY}
                  onPress={() => {
                    navigateTo(config.route);
                  }}
                >
                  <Text
                    type="Circular"
                    size="xmini"
                    color={REDDISH}
                    weight={400}
                  >
                    Lihat Semua
                  </Text>
                </TouchableOpacity>
              )}
            </View>

            {/* ITEM */}
            <FlatList
              bounces={false}
              bouncesZoom={false}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              horizontal={config.horizontal}
              numColumns={config.numColumns}
              style={{ flexGrow: 0 }}
              contentContainerStyle={{
                paddingLeft: WP4,
                paddingTop: section == "collaborator" ? WP6 : WP4,
              }}
              data={dataList}
              extraData={dataList}
              renderItem={({ item, index }) => {
                console.log("item => ", item);
                return this._adItem(item, index, section, false);
              }}
              ListEmptyComponent={
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    height: HP50 * 0.85,
                  }}
                >
                  <EmptyState
                    image={require(`sf-assets/icons/ic_collab_collaboration_emptystate.png`)}
                    title={config.empty_message_title}
                    message={config.empty_message_desc}
                  />
                </View>
              }
            />
          </View>
        </View>
      )
    );
  };

  _animatedProgress = (step) => {
    Animated.timing(this.state.progress, {
      toValue: (WP100 / this.state.totalStep) * step,
      duration: 350,
      delay: 500,
      easing: Easing.ease,
      useNativeDriver: false,
    }).start(() => {
      this.setState({ step }, () => {
        setTimeout(() => {
          this._animatedFade(this.state.fadeAnim);
        }, 5000);
      });
    });
  };

  _animatedFade = (fadeAnim) => {
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 500,
      delay: 100,
      useNativeDriver: true,
    }).start(() => {
      setTimeout(() => {
        this.setState({ scrollEnabled: true });
      }, 500);
    });
  };

  render() {
    const { location, result, scrollEnabled } = this.state;
    return (
      <Container
        theme="dark"
        scrollable={scrollEnabled}
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={() => this._getContent(location)}
        isReady={true}
        renderHeader={this._headerSearch}
      >
        <View>
          <NavigationEvents onDidFocus={() => this._getContent(location)} />
          {this._headerSection(result?.about)}
          {this._locationSection()}
          {this._collabSection("collaborator")}
          {this._collabSection("collaboration")}
        </View>
      </Container>
    );
  }
}

CollaborationScreen.propTypes = {};

CollaborationScreen.defaultProps = {};

export default _enhancedNavigation(
  connect(mapStateToProps, { setCollabData })(CollaborationScreen),
  mapFromNavigationParam
);
