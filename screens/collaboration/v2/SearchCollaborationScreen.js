import React, { Component } from "react";
import { View, TouchableOpacity, FlatList, TextInput } from "react-native";
import { connect } from "react-redux";
import { isEmpty, startCase, isObject, toLower, isNumber } from "lodash-es";
import {
  _enhancedNavigation,
  Text,
  Icon,
  Container,
  SelectModalV2,
  Image,
} from "../../../components";
import { getAdsCollab, getCity, getSettingDetail } from "../../../actions/api";
import {
  WHITE,
  SHIP_GREY_CALM,
  SHIP_GREY,
  PALE_WHITE,
  REDDISH,
  PALE_GREY,
  TRANSPARENT,
} from "../../../constants/Colors";
import {
  WP4,
  WP6,
  WP305,
  WP3,
  WP2,
  WP1,
  WP10,
  WP100,
} from "../../../constants/Sizes";
import { TOUCH_OPACITY } from "../../../constants/Styles";
import CollaboratorItem from "../../../components/collaboration/ColalboratorItem";
import CollaborationItem from "../../../components/collaboration/CollaborationItem";
import ButtonV2 from "../../../components/ButtonV2";
import Loader from "../../../components/Loader";

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapFromNavigationParam = (getParam) => ({
  location: getParam("location", ""),
  onSubmitCallback: getParam("onSubmitCallback", () => {}),
});

const limit = 5;

class SearchCollaborationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      isLoading: true,
      collaboration: [],
      collaborator: [],
      collaborationSeeMore: true,
      collaboratorSeeMore: true,
      q: "",
      location: this.props.location,
      searchSuggestions: [],
      loadMoreKey: "",
    };
    this._headerSearch = this._headerSearch.bind(this);
    this._locationSection = this._locationSection.bind(this);
    this._renderSuggestion = this._renderSuggestion.bind(this);
    this._performSearch = this._performSearch.bind(this);
    this._loadMoreButton = this._loadMoreButton.bind(this);
    this._onCancel = this._onCancel.bind(this);
    this._onChangeLocation = this._onChangeLocation.bind(this);
    this._handleAutoSearch = this._handleAutoSearch.bind(this);
  }

  _onCancel() {
    this.setState({
      q: "",
      location: "",
      collaboration: [],
      collaborator: [],
      loadMoreKey: "",
    });
  }

  _handleAutoSearch = () => {
    if (this.state.q.length >= 3) {
      isNumber(this.timeout) && clearTimeout(this.timeout);
      this.setState({ isSearching: true });
      this.timeout = setTimeout(
        () =>
          this._performSearch({
            show: "all",
            loadMore: false,
          }),
        850
      );
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    const { q, location, isSearching, loadMoreKey, searchSuggestions } =
      this.state;
    return (
      // (
      //   isEmpty(q) && !isEmpty(nextState.q) ||
      //   !isEmpty(q) && isEmpty(nextState.q)
      // ) ||
      q != nextState.q ||
      location != nextState.location ||
      isSearching != nextState.isSearching ||
      loadMoreKey != nextState.loadMoreKey ||
      searchSuggestions.length != nextState.searchSuggestions.length
    );
  }

  _loadMoreButton = (searchParam) => {
    const isLoading = this.state.loadMoreKey === searchParam.show;
    return (
      <View
        style={{
          paddingVertical: isLoading ? 1 : 0,
          marginBottom: WP3,
        }}
      >
        <Loader size={"mini"} isLoading={isLoading}>
          <TouchableOpacity
            onPress={() =>
              this.setState(
                {
                  loadMoreKey: searchParam.show,
                },
                () => this._performSearch(searchParam)
              )
            }
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                paddingVertical: WP3,
              }}
            >
              <View style={{ marginRight: WP1 }}>
                <Icon
                  size={"small"}
                  color={REDDISH}
                  type={"MaterialCommunityIcons"}
                  name="plus"
                />
              </View>
              <Text
                weight={400}
                color={REDDISH}
                size={"mini"}
                type={"Circular"}
              >
                Lihat lainnya
              </Text>
            </View>
          </TouchableOpacity>
        </Loader>
      </View>
    );
  };

  _performSearch = ({ show, loadMore }) => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props;
    const { q: search, location: location_name } = this.state;
    show == "all" &&
      this.setState({
        collaboration: [],
        collaborator: [],
        isSearching: true,
      });
    console.log("search", search);
    dispatch(getAdsCollab, {
      search,
      id_user,
      show,
      location_name,
      start: loadMore ? this.state[show].length : 0,
      limit,
    }).then(({ collaboration, collaborator }) => {
      isObject(collaboration) &&
        this.setState({
          collaboration: [...this.state.collaboration, ...collaboration],
          collaborationSeeMore: collaboration.length >= limit,
          loadMoreKey: "",
        });
      isObject(collaborator) &&
        this.setState({
          collaborator: [...this.state.collaborator, ...collaborator],
          collaboratorSeeMore: collaborator.length >= limit,
          loadMoreKey: "",
        });
      this.setState({ isSearching: false });
    });
  };

  componentDidMount = () => {
    const { dispatch } = this.props;
    dispatch(getSettingDetail, {
      setting_name: "collaboration",
      key_name: "search_suggestion",
    }).then((result) => {
      this.setState({
        searchSuggestions: JSON.parse(result.value),
      });
    });
  };

  _renderSuggestion = (q, index) => {
    return (
      <TouchableOpacity
        onPress={() =>
          this.setState({ q, isSearching: true }, () => {
            setTimeout(
              () =>
                this._performSearch({
                  show: "all",
                  loadMore: false,
                }),
              500
            );
          })
        }
        key={`suggest-${q}`}
        style={{
          paddingVertical: WP2,
          paddingHorizontal: WP3,
          marginRight: WP2,
          marginTop: WP3,
          borderWidth: 1,
          borderColor: "rgb(223, 227, 232)",
          borderRadius: WP2,
        }}
      >
        <Text size={"xmini"} color={SHIP_GREY} weight={500} type={"Circular"}>
          {q}
        </Text>
      </TouchableOpacity>
    );
  };

  _headerSearch = () => {
    const { navigateBack, onSubmitCallback } = this.props;
    const { q } = this.state;
    return (
      <View style={{ overflow: "hidden", paddingBottom: 4.5 }}>
        <View
          style={{
            backgroundColor: WHITE,
            elevation: 4.5,
          }}
        >
          <View
            style={{
              paddingHorizontal: WP4,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              paddingVertical: WP2,
            }}
          >
            <Icon
              centered
              onPress={() => navigateBack()}
              background="dark-circle"
              size="large"
              color={SHIP_GREY_CALM}
              name="chevron-left"
              type="Entypo"
            />
            <View
              style={{
                flex: 1,
                backgroundColor: "rgba(145, 158, 171, 0.1)",
                borderRadius: 6,
                flexDirection: "row",
                alignItems: "center",
                paddingLeft: WP3,
                paddingVertical: WP1,
                marginHorizontal: WP2,
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  navigateBack();
                  onSubmitCallback(q, this.state.location);
                }}
                activeOpacity={TOUCH_OPACITY}
              >
                <Icon
                  centered
                  background="dark-circle"
                  size="large"
                  color={SHIP_GREY_CALM}
                  name="magnify"
                  type="MaterialCommunityIcons"
                  style={{ marginRight: WP1 }}
                />
              </TouchableOpacity>
              <TextInput
                value={q}
                // onKeyPress={(q) => console.log("search: ", q)}
                // onSubmitEditing={(q) => console.log("search: ", q)}
                onChangeText={(q) =>
                  this.setState({ q }, this._handleAutoSearch)
                }
                placeholder={"Try to search Collaboration"}
                autoFocus
                style={{
                  flex: 1,
                  fontFamily: "CircularBook",
                  color: SHIP_GREY_CALM,
                }}
              />
              <TouchableOpacity
                onPress={() => {
                  !isEmpty(q) && this.setState({ q: "" });
                }}
                style={{
                  paddingVertical: WP1,
                  paddingHorizontal: WP2,
                }}
              >
                {!isEmpty(q) && (
                  <Icon
                    centered
                    background="dark-circle"
                    size="mini"
                    color={SHIP_GREY_CALM}
                    name="close"
                    type="MaterialCommunityIcons"
                  />
                )}
              </TouchableOpacity>
            </View>
            {!isEmpty(q) && (
              <Text
                onPress={navigateBack}
                style={{
                  paddingVertical: WP2,
                  paddingHorizontal: WP1 + 2,
                }}
                type="Circular"
                size="xmini"
                weight={400}
                color={REDDISH}
              >
                Cancel
              </Text>
            )}
          </View>
          {this._locationSection()}
        </View>
      </View>
    );
  };

  _locationSection = () => {
    const { location } = this.state;
    return (
      <SelectModalV2
        extraResult
        refreshOnSelect
        triggerComponent={
          <View
            style={{
              paddingHorizontal: WP4,
              width: "100%",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingTop: WP3,
              paddingBottom: WP3,
              borderTopWidth: 1,
              borderTopColor: PALE_GREY,
            }}
          >
            <View
              style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            >
              <Icon
                centered
                background="dark-circle"
                size="small"
                color={REDDISH}
                name="location-on"
                type="MaterialIcons"
              />
              <Text numberOfLines={1} style={{ flex: 1 }}>
                <Text
                  type="Circular"
                  size="mini"
                  weight={300}
                  color={SHIP_GREY}
                >
                  Collaboration at:
                </Text>
                <Text
                  type="Circular"
                  size="mini"
                  weight={300}
                  color={REDDISH}
                >{` ${
                  isEmpty(this.state.location)
                    ? "All City"
                    : this.state.location
                }`}</Text>
              </Text>
            </View>
            <Text type="Circular" size="xmini" weight={400} color={REDDISH}>
              Edit
            </Text>
            <View style={{ width: WP2 }} />
          </View>
        }
        header="Select City"
        suggestion={getCity}
        suggestionKey="city_name"
        suggestionPathResult="city_name"
        onChange={this._onChangeLocation}
        selected={location}
        reformatFromApi={(text) => startCase(toLower(text))}
        createNew={false}
        placeholder="Search city here..."
      />
    );
  };

  _onChangeLocation = (value) => {
    this.setState(
      {
        location: value,
      },
      this._performSearch({
        show: "all",
        loadMore: false,
      })
    );
  };

  _adItem = (ads, i, loading) => {
    const {
      navigateTo,
      userData: { id_user },
    } = this.props;
    const isAds = typeof ads.id_ads != "undefined";
    return (
      <TouchableOpacity
        disabled={loading}
        style={{
          width: WP100,
          alignItems: "center",
          paddingLeft: WP305,
          paddingVertical: WP2,
        }}
        activeOpacity={TOUCH_OPACITY}
        onPress={() => {
          isAds &&
            navigateTo("CollaborationPreview", {
              id: ads.id_ads,
            });
          !isAds &&
            navigateTo("ProfileScreenNoTab", {
              idUser: ads.id_user,
              navigateBackOnDone: true,
            });
        }}
      >
        {isAds && (
          <CollaborationItem
            ads={ads}
            myId={id_user}
            loading={loading}
            navigateTo={navigateTo}
          />
        )}
        {!isAds && (
          <CollaboratorItem
            ads={ads}
            loading={loading}
            navigateTo={navigateTo}
            simpleListItem={true}
          />
        )}
      </TouchableOpacity>
    );
  };

  render() {
    const {
      isReady,
      location,
      q,
      collaboration,
      collaborator,
      collaborationSeeMore,
      collaboratorSeeMore,
      isSearching,
    } = this.state;
    const { navigateTo } = this.props;
    return (
      <Container
        theme="dark"
        scrollable
        scrollBackgroundColor={PALE_WHITE}
        isReady={isReady}
        isLoading={false}
        renderHeader={this._headerSearch}
      >
        <View style={{ paddingVertical: WP6, paddingHorizontal: WP4 }}>
          <Text type={"Circular"} size={"medium"} weight={600}>
            Last Searched
          </Text>
          <View
            style={{ flexDirection: "row", flexWrap: "wrap", marginTop: WP2 }}
          >
            {this.state.searchSuggestions.map(this._renderSuggestion)}
          </View>
        </View>
        {isEmpty(location) && isEmpty(q) && (
          <View style={{ paddingVertical: WP6, paddingHorizontal: WP4 }}>
            <Text type={"Circular"} size={"medium"} weight={600}>
              Top Search
            </Text>
            <View
              style={{ flexDirection: "row", flexWrap: "wrap", marginTop: WP2 }}
            >
              {this.state.searchSuggestions.map(this._renderSuggestion)}
            </View>
          </View>
        )}
        {/* {isSearching && (
          <View style={{ paddingVertical: WP3 }}>
            <Loader size={"mini"} isLoading={isSearching} />
          </View>
        )}
        {(!isEmpty(location) || !isEmpty(q)) && !isSearching && (
          <View>
            {!isEmpty(collaboration) && (
              <View>
                <View
                  style={{
                    paddingTop: WP6,
                    paddingBottom: WP2,
                    paddingHorizontal: WP4,
                  }}
                >
                  <Text type={"Circular"} size={"medium"} weight={600}>
                    Collaboration
                  </Text>
                </View>
                <FlatList
                  bounces={false}
                  bouncesZoom={false}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  horizontal={false}
                  numColumns={1}
                  data={collaboration}
                  extraData={collaboration}
                  renderItem={({ item, index }) =>
                    this._adItem(item, index, false)
                  }
                  ListFooterComponent={
                    collaborationSeeMore
                      ? this._loadMoreButton({
                          show: "collaboration",
                          loadMore: true,
                        })
                      : null
                  }
                />
              </View>
            )}
            {!isEmpty(collaboration) && !isEmpty(collaborator) && (
              <View style={{ height: WP1 }} />
            )}
            {!isEmpty(collaborator) && !isSearching && (
              <View>
                <View
                  style={{
                    paddingTop: WP6,
                    paddingBottom: WP2,
                    paddingHorizontal: WP4,
                  }}
                >
                  <Text type={"Circular"} size={"medium"} weight={600}>
                    Collaborator
                  </Text>
                </View>
                <FlatList
                  bounces={false}
                  bouncesZoom={false}
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  horizontal={false}
                  numColumns={1}
                  data={collaborator}
                  extraData={collaborator}
                  renderItem={({ item, index }) =>
                    this._adItem(item, index, false)
                  }
                  ListFooterComponent={
                    collaboratorSeeMore
                      ? this._loadMoreButton({
                          show: "collaborator",
                          loadMore: true,
                        })
                      : null
                  }
                />
                <View style={{ height: WP6 }} />
              </View>
            )}
          </View>
        )} */}
        {!isSearching &&
          isEmpty(collaboration) &&
          isEmpty(collaborator) &&
          !isEmpty(q) && (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Image
                style={{ marginBottom: WP6 }}
                size={"extraMassive"}
                source={require("../../../assets/images/emptyMagnifier.png")}
              />
              <Text
                style={{ lineHeight: WP6 }}
                color={SHIP_GREY}
                type={"Circular"}
                size={"slight"}
                centered
              >
                {"Maaf, untuk saat ini belum ada\nkolaborasi yang tersedia"}
              </Text>
              <ButtonV2
                textColor={WHITE}
                style={{ paddingHorizontal: WP10, marginTop: WP6 }}
                color={REDDISH}
                textSize={"slight"}
                onPress={() => navigateTo("CollaborationForm")}
                text={"Buat Kolaborasi"}
              />
              <ButtonV2
                textColor={SHIP_GREY_CALM}
                style={{ paddingHorizontal: WP10, marginVertical: WP3 }}
                color={TRANSPARENT}
                textSize={"slight"}
                onPress={this._onCancel}
                text={"Coba Cari Lagi"}
              />
            </View>
          )}
      </Container>
    );
  }
}

SearchCollaborationScreen.propTypes = {};

SearchCollaborationScreen.defaultProps = {};

export default _enhancedNavigation(
  connect(mapStateToProps, {})(SearchCollaborationScreen),
  mapFromNavigationParam
);
