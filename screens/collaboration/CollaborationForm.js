import React from 'react'
import { connect } from 'react-redux'
import {
  Alert,
  BackHandler,
  Image as RNImage,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'
import { map, noop, startCase, toLower } from 'lodash-es'
import {
  _enhancedNavigation,
  Button,
  Card,
  Container,
  Form,
  HeaderNormal,
  Icon,
  Image,
  InputModal,
  InputTextLight,
  Menu,
  Text,
} from '../../components'
import {
  BLUE_LIGHT,
  GREY50,
  GREY_CALM_SEMI,
  GREY_CHAT,
  GREY_DISABLE,
  ORANGE,
  TOMATO,
  WHITE,
} from '../../constants/Colors'
import { FONT_SIZE, HP1, HP4, WP1, WP100, WP3, WP4, WP6 } from '../../constants/Sizes'
import { selectPhoto, takePhoto } from '../../utils/upload'
import { convertBlobToBase64, fetchAsBlob, isIOS, isPremiumUser } from '../../utils/helper'
import { getAdsDetail, getCity, getJob, getSettingDetail, putCollabPost } from '../../actions/api'
import { objectMapper } from '../../utils/mapper'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { FONTS } from '../../constants/Fonts'
import BackModal from '../../components/BackModal'
import RequiredMark from '../../components/RequiredMark'
import { paymentDispatcher } from '../../services/payment'
import { FORM_VALIDATION, MAPPER_FOR_API, MAPPER_FROM_API } from './_constants'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  id: getParam('id', 0),
})

const propsDefault = {}

class CollaborationForm extends React.Component {
  _didFocusSubscription;
  _willBlurSubscription;

  constructor(props) {
    super(props)
    this._didFocusSubscription = props.navigation.addListener('focus', (payload) =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler),
    )
  }

  state = {
    collaboration: {},
    collaborationDurations: [],
    isReady: !this.props.id,
    backModal: false,
  };

  async componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('blur', (payload) =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler),
    )
    await this._getInitialScreenData()
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _getInitialScreenData = async (...args) => {
    await this._getCollaborationDuration(...args)
    if (this.props.id) {
      await this._getCollaborationDetail(...args)
    }
    await this.setState({
      isReady: true,
    })
  };

  _getCollaborationDetail = async (isLoading = true) => {
    const {
      dispatch,
      userData: { id_user },
      id,
    } = this.props
    if (id) {
      const dataResponse = await dispatch(
        getAdsDetail,
        { id_user, id_ads: id },
        noop,
        false,
        isLoading,
      )
      const bannerPictureb64 = await fetchAsBlob(dataResponse.image).then(convertBlobToBase64)
      const collaborationDataFromApi = {
        ...dataResponse,
        bannerPictureb64,
        additional_data: JSON.parse(dataResponse.additional_data),
      }
      await this.setState({
        collaboration: objectMapper(collaborationDataFromApi, MAPPER_FROM_API),
      })
    }
  };

  _getCollaborationDuration = async (isLoading = true) => {
    const { dispatch } = this.props
    const dataResponse = await dispatch(
      getSettingDetail,
      {
        setting_name: 'ads',
        key_name: 'post_duration',
      },
      noop,
      false,
      isLoading,
    )
    const collaborationDurations = JSON.parse(dataResponse.value)
    this.setState({
      collaborationDurations,
    })
  };

  _backHandler = async () => {
    const { navigateBack } = this.props
    const { backModal } = this.state

    if (this.form && this.form.isDirtyState()) {
      this.setState({ backModal: !backModal })
    } else {
      navigateBack()
    }
  };

  _postCollaboration = ({ formData }) => {
    const { id, userData, dispatch } = this.props
    if (id) {
      dispatch(putCollabPost, {
        ...objectMapper(formData, MAPPER_FOR_API),
        id_user: userData.id_user,
        id_ads: id,
      }).then(() => {
        this.props.navigateBack()
      })
    } else
      this.props.navigateTo('CollaborationPreview', {
        previewData: { ...formData, createdBy: userData.full_name },
      })
  };

  _getSelectedOption = (selectedValue, value) =>
    toLower(selectedValue) === value
      ? {
          container: {
            backgroundColor: BLUE_LIGHT,
          },
          text: {
            color: WHITE,
          },
        }
      : {
          container: {
            backgroundColor: WHITE,
          },
          text: {
            color: BLUE_LIGHT,
          },
        };

  render() {
    const { userData, navigateTo, id, navigateBack, paymentSourceSet } = this.props
    const { isReady, collaboration, collaborationDurations, backModal } = this.state
    return (
      <Form
        ref={(form) => (this.form = form)}
        validation={FORM_VALIDATION}
        initialValue={{ professions: [], ...collaboration }}
        onSubmit={this._postCollaboration}
      >
        {({ onChange, onSubmit, formData, isValid, isDirty }) => (
          <Container
            isReady={isReady}
            renderHeader={() => (
              <HeaderNormal
                iconLeftOnPress={this._backHandler}
                text={id ? 'Edit Collaboration Project' : 'Create Collaboration Project'}
              />
            )}
            isAvoidingView
            scrollable
            scrollBackgroundColor={WHITE}
            scrollContentContainerStyle={{ justifyContent: 'space-between' }}
          >
            <BackModal
              modalVisible={backModal}
              navigateBack={navigateBack}
              dismiss={() => this.setState({ backModal: false })}
            />

            <View style={{ backgroundColor: GREY_CALM_SEMI }}>
              <ImageBackground
                source={{ uri: formData.bannerPictureUri }}
                style={{ width: WP100, backgroundColor: GREY_CALM_SEMI, aspectRatio: 32 / 13 }}
              >
                <Menu
                  options={[
                    {
                      onPress: selectPhoto(onChange, 'bannerPicture', [32, 13]),
                      name: 'Select from library',
                      iconName: 'photo-library',
                    },
                    {
                      onPress: takePhoto(onChange, 'bannerPicture', [32, 13]),
                      name: 'Take a picture',
                      iconName: 'camera',
                    },
                  ]}
                  triggerComponent={(toggleModal) => (
                    <Image
                      onPress={toggleModal}
                      size='tiny'
                      source={require('../../assets/icons/elementAddPhotoBanner.png')}
                      style={{
                        aspectRatio: 23 / 21,
                        bottom: WP6,
                        right: WP6,
                        position: 'absolute',
                      }}
                    />
                  )}
                />
              </ImageBackground>
              <Card first>
                <InputTextLight
                  required
                  withLabel={false}
                  maxLength={40}
                  placeholder='I’m Looking for...'
                  wording='Title of Collaboration Project'
                  value={formData.title}
                  onChangeText={onChange('title')}
                />
              </Card>
              <Card style={{ paddingHorizontal: 0 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text size='xtiny' weight={500} style={{ paddingLeft: WP6 }}>
                    ROLE NEEDED
                  </Text>
                  <RequiredMark style={{ marginTop: 0 }} />
                </View>
                <InputModal
                  extraResult
                  refreshOnSelect
                  triggerComponent={
                    <Button
                      width='40%'
                      style={{
                        alignSelf: 'flex-start',
                        paddingHorizontal: WP6,
                        marginTop: HP1,
                      }}
                      marginless
                      contentStyle={{
                        paddingHorizontal: WP1,
                        paddingLeft: WP3,
                        justifyContent: 'space-between',
                      }}
                      iconName='add-circle-outline'
                      iconType='MaterialIcons'
                      iconSize='mini'
                      iconPosition='right'
                      iconColor={WHITE}
                      compact='center'
                      rounded
                      backgroundColor={BLUE_LIGHT}
                      shadow='none'
                      text='Profession'
                      textColor={WHITE}
                      textType='NeoSans'
                      textSize='xtiny'
                      textWeight={500}
                    />
                  }
                  header='Select Profession'
                  suggestion={getJob}
                  suggestionKey='job_title'
                  suggestionPathResult='job_title'
                  onChange={(value) => {
                    const newProfessions = formData.professions
                    newProfessions.push({
                      name: value,
                      notes: '',
                    })
                    onChange('professions')(newProfessions)
                  }}
                  createNew={false}
                  placeholder='Search profession here...'
                />
                <View style={{ marginTop: HP1 }}>
                  {map(formData.professions, (profession, index) => (
                    <View
                      key={`profession-${index}`}
                      style={{
                        paddingVertical: HP1,
                        flexDirection: 'row',
                        alignItems: 'center',
                        flex: 1,
                        backgroundColor: GREY_CHAT,
                        marginVertical: 2,
                        paddingHorizontal: WP6,
                      }}
                    >
                      <Button
                        rounded
                        colors={[TOMATO, TOMATO]}
                        compact='center'
                        style={{ width: '30%', marginRight: WP6 }}
                        marginless
                        toggle
                        toggleActive={false}
                        toggleInactiveTextColor={TOMATO}
                        toggleInactiveColor={GREY_CHAT}
                        text={profession?.name}
                        textSize='xtiny'
                        centered
                        shadow='none'
                      />
                      <View style={{ width: '50%', flexDirection: 'row', alignItems: 'center' }}>
                        <Text centered size='xtiny'>
                          {'Notes: '}
                        </Text>
                        <TextInput
                          placeholder='Ex: Wanita'
                          value={profession.notes}
                          onChangeText={(text) => {
                            const newProfessions = formData.professions
                            newProfessions[index].notes = text
                            onChange('professions')(newProfessions)
                          }}
                          style={{
                            flex: 1,
                            fontSize: FONT_SIZE['xtiny'],
                            fontFamily: FONTS['OpenSans'][500],
                            color: TOMATO,
                            padding: 0,
                            paddingTop: 0,
                            margin: 0,
                            borderWidth: 0,
                          }}
                        />
                      </View>
                      <View style={{ width: '10%' }}>
                        <Icon
                          background='dark-circle'
                          backgroundColor={GREY50}
                          style={{ alignSelf: 'flex-end' }}
                          color={WHITE}
                          name='close'
                          size='tiny'
                          onPress={() => {
                            const newProfessions = formData.professions
                            newProfessions.splice(index, 1)
                            onChange('professions')(newProfessions)
                          }}
                        />
                      </View>
                    </View>
                  ))}
                </View>
              </Card>
              <Card>
                {!id && (
                  <>
                    <View style={{ flexDirection: 'row' }}>
                      <Text size='xtiny' weight={500}>
                        POST DURATION
                      </Text>
                      <RequiredMark style={{ marginTop: 0 }} />
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginVertical: HP1,
                        borderRadius: 8,
                        borderColor: BLUE_LIGHT,
                        borderWidth: 1,
                        overflow: 'hidden',
                      }}
                    >
                      {map(collaborationDurations, (duration, i) => (
                        <TouchableOpacity
                          activeOpacity={TOUCH_OPACITY}
                          onPress={() => {
                            if (duration.is_premium && !isPremiumUser(userData.account_type)) {
                              if (isIOS())
                                Alert.alert(
                                  'Sorry, this option cannot be used on iOS. Please stay tuned.',
                                )
                              else {
                                paymentSourceSet('collaboration')
                                navigateTo('MembershipScreen')
                              }
                            } else {
                              onChange('postDuration')(duration.duration)
                            }
                          }}
                          style={{
                            paddingVertical: WP1,
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderLeftColor: BLUE_LIGHT,
                            borderLeftWidth: i > 0 ? 1 : 0,
                            ...this._getSelectedOption(formData.postDuration, duration.duration)
                              .container,
                          }}
                        >
                          <Text
                            centered
                            weight={500}
                            type='SansPro'
                            {...this._getSelectedOption(formData.postDuration, duration.duration)
                              .text}
                          >
                            {duration.duration}
                          </Text>
                          {Boolean(duration.is_premium) && (
                            <RNImage
                              style={{ width: FONT_SIZE['small'], height: FONT_SIZE['small'] }}
                              source={require('../../assets/icons/badge.png')}
                            />
                          )}
                        </TouchableOpacity>
                      ))}
                    </View>
                    <Text size='xtiny' color={GREY_DISABLE}>
                      Duration for your collaboration project will promote on explore
                    </Text>
                  </>
                )}
                <InputModal
                  extraResult
                  refreshOnSelect
                  triggerComponent={
                    <InputTextLight
                      required
                      style={{ marginTop: id ? 0 : HP4 }}
                      withLabel={false}
                      placeholder='ex: Jakarta'
                      wording='City of Event Collaboration'
                      value={formData.locationName}
                    />
                  }
                  header='Select City'
                  suggestion={getCity}
                  suggestionKey='city_name'
                  suggestionPathResult='city_name'
                  onChange={onChange('locationName')}
                  createNew={false}
                  reformatFromApi={(text) => startCase(toLower(text))}
                  placeholder='Search city here...'
                />
                <View style={{ marginTop: HP4 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text size='xtiny' weight={500}>
                      ABOUT ME
                    </Text>
                    <RequiredMark style={{ marginTop: 0 }} />
                  </View>
                  <InputTextLight
                    withLabel={false}
                    bordered
                    size='tiny'
                    maxLength={300}
                    multiline
                    placeholder='Describe about yourself'
                    value={formData.description}
                    onChangeText={onChange('description')}
                  />
                </View>
                <View style={{ marginTop: HP4 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text size='xtiny' weight={500}>
                      DESCRIPTION
                    </Text>
                    <RequiredMark style={{ marginTop: 0 }} />
                  </View>
                  <InputTextLight
                    withLabel={false}
                    bordered
                    size='tiny'
                    maxLength={500}
                    multiline
                    placeholder='Describe what are you needed'
                    value={formData.specification}
                    onChangeText={onChange('specification')}
                  />
                </View>
              </Card>
            </View>
            <View
              style={{
                paddingHorizontal: WP4,
                backgroundColor: WHITE,
              }}
            >
              <Button
                disable={!isValid || !isDirty}
                onPress={onSubmit}
                backgroundColor={ORANGE}
                centered
                bottomButton
                radius={WP4}
                shadow='none'
                textType='NeoSans'
                textSize='small'
                textColor={WHITE}
                textWeight={500}
                text={id ? 'Save' : 'Create'}
              />
            </View>
          </Container>
        )}
      </Form>
    )
  }
}

CollaborationForm.defaultProps = propsDefault
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CollaborationForm),
  mapFromNavigationParam,
)
