/* @flow */

import moment from "moment/min/moment-with-locales";
import { compact, isEmpty } from "lodash-es";
import * as yup from "yup";

export const DUMP_DATA_FROM_API = {
  category: "collaboration",
  created_at: "2019-12-10 08:49:52",
  created_by: "User 3",
  deleted_at: null,
  description: "content description",
  email: "ibnu.dev@gmail.com",
  id_ads: "262",
  id_user: "322",
  image:
    "https://apidev.soundfren.id/userfiles/images/ads/322_banner_picture_1575942927.png",
  is_premium: "0",
  profile_picture:
    "https://apidev.soundfren.id/userfiles/images/profile_picture/322_profile_picture.jpg",
  status: "active",
  title: "Collab With me 2",
  job_title: "Gitaris",
  company: "Soundfren",
  city_name: "JAKARTA SELATAN",
  updated_at: "2019-12-10 08:55:27",
  valid_until: "8 days",
  additional_data: {
    date: {
      end: "2019-12-18",
      start: "2019-12-10",
    },
    location_name: "ICE BSD",
    post_duration: "8 days",
    profession: [
      {
        name: "kabelis",
        notes: "Wanita",
      },
      {
        name: "Drummer",
        notes: "Wanita",
      },
    ],
    specification: "deskripisi spesifikasinya",
  },
  location: {
    city_name: "DKI JAKARTA",
  },
};

export const DUMP_DATA_FOR_API = {
  id_user: 322,
  title: "",
  description: "",
  profession: [{ name: "", notes: "" }],
  location_name: "",
  banner_picture: "",
  spesification: "",
  post_duration: "", // 07 days | 14 days | 1 month
};

export const DUMP_COLLABORATION_DATA = {
  id: "id_ads",
  title: "Title Tes For Collaboration",
  description:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
  professions: [{ name: "name", notes: "notes" }],
  locationName: "Jakarta Selatan",
  bannerPictureUri:
    "https://apidev.soundfren.id/userfiles/images/ads/322_banner_picture_1575942927.png",
  bannerPictureb64: "",
  specification:
    "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
  postDuration: "7 days",
  daysRemaining: "8",
  createdBy: "Ibnu Prayogi",
  createdAt: "4 hours ago",
  updatedBy: "Ibnu Prayogi",
  updatedAt: "2019-12-10",
  isNew: false,
  // Author Profile
  authorPicture:
    "https://apidev.soundfren.id/userfiles/images/profile_picture/322_profile_picture.jpg",
  authorId: 322,
  authorJobTitle: "Guitarist",
  authorCompany: "Soundfren",
  authorCityName: "Jakarta",
  authorIsPremium: false,
};

// COLLABORATION_DATA Validation
export const FORM_VALIDATION = yup.object().shape({
  title: yup.string().required(),
  // description: yup.string().required(),
  professions: yup
    .array()
    .test("min-1", "Minimal 1 value", (value) => !isEmpty(compact(value))),
  locationName: yup.string().required(),
  // bannerPictureb64: yup.string().required(),
  specification: yup.string().required(),
  startDate: yup.string().required(),
  endDate: yup.string().required(),
  memberQuota: yup.string().required(),
});

//POP UP MODAL
export const MESSAGE_POP_UP = {
  JOIN_COLLAB: {
    title: "Success",
    content: "Your collaboration request has been sent",
    confirmLabel: "View My Collaboration Status",
    cancelLabel: "See More Collaboration",
  },
  TAKE_COLLAB: {
    title: "Offer Confirmation",
    content:
      "Are you sure to follow this project? After confirmation, the application process join on other projects will be terminated and can't be canceled",
    confirmLabel: "Yes",
    cancelLabel: "No",
  },
};

// API_DATA to COLLABORATION_DATA
export const MAPPER_FROM_API = {
  id: "id_ads",
  title: "title",
  description: "description",
  professions: ["additional_data.profession", { name: "name", notes: "notes" }],
  locationName: "additional_data.location.name",
  bannerPictureUri: "image",
  bannerPictureb64: "bannerPictureb64",
  specification: "additional_data.specification",
  postDuration: "additional_data.post_duration",
  collaborationReference: "additional_data.collaboration_reference",
  daysRemaining: "day_left",
  createdBy: "created_by",
  createdAt: (source) =>
    moment(source.created_at).locale("id").fromNow(false).replace("yang ", ""),
  updatedBy: "updated_by",
  updatedAt: "updated_at",
  isNew: (source) => moment().diff(moment(source.update_at), "days") <= 3,
  // Author Profile
  authorPicture: "profile_picture",
  authorId: "id_user",
  authorJobTitle: "job_title",
  authorCompany: "company",
  authorCityName: "location.city_name",
  authorIsPremium: "is_premium",
  comment: "comment",
  totalComment: (source) => parseInt(source.total_comment),
  joined: "joined",
  members: "members.data",
};

// COLLABORATION_DATA to API_DATA
export const MAPPER_FOR_API = {
  title: "title",
  description: "description",
  profession: "professions",
  location_name: "locationName",
  banner_picture: "bannerPictureb64",
  spesification: "specification",
  post_duration: "postDuration",
  startDate: "startDate",
  endDate: "endDate",
  memberQuota: "memberQuota",
  collaboration_reference: "collaborationReference",
};

export type Collaboration = {
  id: string | number,
  title: string,
  description: string,
  professions: Array,
  comment: Array,
  total_comment: string | number,
  locationName: string,
  bannerPictureUri: string,
  bannerPictureb64: string,
  specification: string,
  postDuration: string,
  collaborationReference: string,
  daysRemaining: string,
  createdBy: string,
  createdAt: string,
  updatedBy: string,
  updatedAt: string,
  isNew: boolean,
  authorPicture: string,
  authorId: string | number,
  authorJobTitle: string,
  authorCompany: string,
  authorCityName: string,
  authorIsPremium: boolean,
};

export type PropsType = {
  initialLoaded?: boolean,
  id?: number | string,
  previewData?: Collaboration,
};

export type StateType = {
  isReady?: boolean,
  collaboration?: Collaboration,
  hasPreviewData?: boolean,
};
