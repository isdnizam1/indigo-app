import React from "react";
import { View, Image as RNImage } from "react-native";
import { connect } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";
import {
  NAVY_DARK,
  PALE_BLUE,
  PALE_WHITE,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from "../../../constants/Colors";
import Text from "../../../components/Text";
import Container from "../../../components/Container";
import _enhancedNavigation from "../../../navigation/_enhancedNavigation";
import HeaderNormal from "../../../components/HeaderNormal";
import headerStyle from "../../profile/v2/ProfileScreen/style";
import { WP2, WP3, WP4, WP5 } from "../../../constants/Sizes";
import {
  getProfileFollowing,
  postProfileFollow,
  postProfileUnfollow,
} from "../../../actions/api";
import { SHADOW_STYLE } from "../../../constants/Styles";
import { FollowItem } from "../../../components";
import ButtonV2 from "../../../components/ButtonV2";

const mapStateToProps = ({ auth }) => ({ userData: auth.user });

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam("onRefresh", () => {}),
  defaultTab: getParam("defaultTab", 0),
  participants: getParam("participants", {}),
});

class CollaborationGroupchatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      isLoading: true,
      friends: [],
      scrollEnabled: true,
      participants: this.props.participants,
    };
  }

  componentDidMount = () => {
    // this._getFriends();
  };

  // _getFriends = async () => {
  //   const { userData } = this.props;
  //   const params = {
  //     followed_by: userData.id_user,
  //     id_viewer: userData.id_user,
  //     start: 0,
  //     limit: 15,
  //   };
  //   this.setState({
  //     isReady: false,
  //     isLoading: true,
  //   });
  //   const dataResponse = await getProfileFollowing(params);
  //   if (dataResponse.data.code == 200) {
  //     this.setState({
  //       friends: dataResponse.data.result,
  //       isReady: true,
  //       isLoading: false,
  //     });
  //   }
  //   this.setState({
  //     isReady: true,
  //     isLoading: false,
  //   });
  //   console.log("friends: ", dataResponse.data.result);
  // };

  _onFollowed = async (follow, with_id_user, isLoading = true) => {
    const { userData, dispatch } = this.props;
    const { id_user } = userData;

    const currentAction = follow ? postProfileFollow : postProfileUnfollow;
    const currentPayload = follow
      ? { id_user: with_id_user, followed_by: id_user }
      : {
          id_user,
          id_user_following: with_id_user,
        };
    await dispatch(currentAction, currentPayload, noop, true, isLoading);
  };

  render() {
    const { navigateBack } = this.props;
    const { isReady, isLoading, scrollEnabled, friends, participants } =
      this.state;
    return (
      <Container
        isReady={isReady}
        // isLoading={isLoading}
        scrollable={scrollEnabled}
        isAvoidingView
        scrollBackgroundColor={PALE_WHITE}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={() => navigateBack()}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="My Collaboration Project"
              centered
            />
            <LinearGradient
              colors={SHADOW_GRADIENT}
              style={headerStyle.headerShadow}
            />
          </View>
        )}
      >
        <View
          style={{
            paddingLeft: WP4,
            paddingRight: WP2,
            marginTop: WP5,
          }}
        >
          <Text type="Circular" size="small" color={NAVY_DARK} weight={500}>
            Follow All Members
          </Text>
          <Text
            type="Circular"
            size="mini"
            color={SHIP_GREY}
            weight={300}
            style={{ marginVertical: WP2 }}
          >
            You must follow all members of your collaboration project before
            creat group chat
          </Text>

          {participants.map((item, index) => (
            <View
              key={index}
              style={{
                flexDirection: "row",
                flex: 1,
                marginRight: WP2,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <FollowItem
                onPressItem={() =>
                  navigateTo("ProfileScreenNoTab", {
                    idUser: item.id_user,
                  })
                }
                imageSize="small"
                isMine={false}
                key={`${index}${item.id_user}`}
                user={item}
                onCollab={true}
                style={{ marginHorizontal: 0 }}
                onFollow={this._onFollowed()}
              />
            </View>
          ))}
        </View>
        <View
          style={{
            backgroundColor: WHITE,
            padding: WP4,
            paddingVertical: WP5,
            ...SHADOW_STYLE["shadow"],
          }}
        >
          <ButtonV2
            // onPress={this.onWatchRequest.bind(this)}
            style={{ paddingVertical: WP3 }}
            color={REDDISH}
            textColor={WHITE}
            textSize={"mini"}
            text={"Create Group Chat"}
          />
        </View>
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps)(CollaborationGroupchatScreen),
  mapFromNavigationParam
);
