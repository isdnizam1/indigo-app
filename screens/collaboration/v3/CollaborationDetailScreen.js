import React, { Component } from "react";
import { View, Image as RNImage, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";
import {
  GREEN,
  PALE_GREY_THREE,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
} from "../../../constants/Colors";
import Container from "../../../components/Container";
import _enhancedNavigation from "../../../navigation/_enhancedNavigation";
import HeaderNormal from "../../../components/HeaderNormal";
import headerStyle from "../../profile/v2/ProfileScreen/style";
import CollaborationSkeleton from "../../../components/collaboration/CollaborationSkeleton";
import ChatSkeletonConfig from "../../../components/chat/ChatSkeletonConfig";
import {
  WP1,
  WP10,
  WP105,
  WP2,
  WP20,
  WP4,
  WP5,
  WP8,
  WP80,
} from "../../../constants/Sizes";
import MyCollaborationTab from "../../../components/collaboration/MyCollaborationTab";
import {
  getDetailCollab,
  postEditMemberStatus,
  getDetailProject,
} from "../../../actions/api";
import { Button, FollowItem, Image } from "../../../components";
import SkeletonContent from "react-native-skeleton-content";
import { TOUCH_OPACITY } from "../../../constants/Styles";
import { acc } from "react-native-reanimated";

const mapStateToProps = ({ auth }) => ({ userData: auth.user });

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam("onRefresh", () => {}),
  typeProject: getParam("typeProject", {}),
  id_ads: getParam("id_ads", {}),
});

class CollaborationDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: true,
      participants: [],
      dataCollab: [],
      accepted: false,
      acceptedMember: [],
      denied: false,
      scrollEnabled: true,
    };
  }

  componentDidMount = () => {
    this._getDetailCollab();
  };

  _renderSkeleton = () => {
    const dummy = [1, 2, 3, 4, 5, 6];
    return (
      <View>
        <SkeletonContent
          containerStyle={{
            ...CollaborationSkeleton.container,
            paddingBottom: WP5,
            alignItems: "center",
            justifyContent: "center",
          }}
          layout={[
            CollaborationSkeleton.layout.image,
            CollaborationSkeleton.layout.label,
            {
              ...CollaborationSkeleton.layout.button,
              alignItems: "center",
              justifyContent: "center",
            },
          ]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
        {dummy.map(() => this._renderSkeletonItem())}
        <SkeletonContent
          containerStyle={{
            ...CollaborationSkeleton.container,
            paddingVertical: WP5,
            alignItems: "center",
            justifyContent: "center",
          }}
          layout={[{ width: WP80, height: WP8, marginBottom: WP105 }]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
      </View>
    );
  };

  _renderSkeletonItem = () => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-start",
          marginHorizontal: WP4,
          borderBottomWidth: 1,
          borderBottomColor: PALE_GREY_THREE,
        }}
      >
        <SkeletonContent
          containerStyle={{ flexGrow: 0 }}
          layout={[ChatSkeletonConfig.layouts.avatar]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
        <SkeletonContent
          containerStyle={{ flexGrow: 1, paddingVertical: WP4 }}
          layout={[
            ChatSkeletonConfig.layouts.roomName,
            ChatSkeletonConfig.layouts.lastComment,
          ]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
      </View>
    );
  };

  _getDetailCollab = async () => {
    const { userData, id_ads } = this.props;
    const { typeProject } = this.state;

    const paramsProject = {
      id_ads: id_ads,
      limit: 3,
    };

    const responseDetailProject = await getDetailProject(paramsProject);
    this.setState({
      // isReady: false,
      isLoading: true,
    });
    console.log("response", responseDetailProject);
    if (responseDetailProject.data.code == 200) {
      this.setState({
        dataCollab: responseDetailProject.data.result.detail,
        participants:
          responseDetailProject.data.result.data !== null
            ? responseDetailProject.data.result.data
            : [],
        isLoading: false,
        isReady: true,
      });
    }
  };

  _editMemberStatus = async (id_user_target, shortlist) => {
    const { userData, id_ads } = this.props;

    const body = {
      id_user: userData.id_user,
      id_ads: id_ads,
      id_user_target: id_user_target,
      shortlist: shortlist,
    };
    const response = await postEditMemberStatus(body);
  };

  _renderContent = () => {
    const { navigateTo, typeProject } = this.props;
    const { participants, dataCollab, accepted } = this.state;

    return (
      <MyCollaborationTab
        isEmptyCollab={false}
        emptyMessageTitle={"No Collaborations You Created"}
        onPressGroup={() =>
          navigateTo("CollaborationGroupchatScreen", {
            participants: participants,
          })
        }
        navigateTo={navigateTo}
        label={"People who want to join your collaborative project"}
        data={dataCollab}
        content={
          participants.length > 0
            ? participants.map((item, index) => {
                let isAccepted = false;
                return (
                  <View
                    key={index}
                    style={{
                      flexDirection: "row",
                      flex: 1,
                      marginRight: WP2,
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <FollowItem
                      onPressItem={() => {
                        navigateTo("ProfileScreenNoTab", {
                          idUser: item.id_user,
                          isAcceptMember: true,
                          onSubmitCallback: (id_user, shortlist) => {
                            this.setState({ isLoading: true });
                            this._editMemberStatus(id_user, shortlist);
                            const members = this.state.participants.filter(
                              (user) => user.id_user != id_user
                            );
                            this.setState({
                              participants: members,
                              isLoading: false,
                            });
                          },
                        });
                      }}
                      onCollab={isAccepted}
                      imageSize="small"
                      isMine={false}
                      key={`${index}${item.id_user}`}
                      user={item}
                      style={{ marginHorizontal: 0, paddingVertical: WP2 }}
                    />

                    {typeProject === "myProject" && (
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            this._editMemberStatus(item.id_user, 2);
                            this.setState({ isLoading: true });
                            const members = this.state.participants.filter(
                              (user) => user.id_user != item.id_user
                            );
                            this.setState({
                              participants: members,
                              isLoading: false,
                            });
                          }}
                          activeOpacity={TOUCH_OPACITY}
                          style={{
                            height: WP10,
                            width: WP10,
                            borderWidth: 1,
                            borderRadius: WP5,
                            borderColor: SHIP_GREY_CALM,
                            alignItems: "center",
                            justifyContent: "center",
                            marginHorizontal: WP2,
                            marginVertical: WP4,
                            alignSelf: "center",
                          }}
                        >
                          <Image
                            style={{
                              width: "100%",
                              alignItems: "center",
                            }}
                            size="tiny"
                            centered
                            source={require("../../../assets/icons/v3/closeGrey.png")}
                          />
                        </TouchableOpacity>

                        <TouchableOpacity
                          onPress={() => {
                            this._editMemberStatus(item.id_user, 1);
                            this.setState({ isLoading: true });
                            isAccepted = true;
                            // const members = this.state.participants.filter(
                            //   (user) => user.id_user != item.id_user
                            // );
                            this.setState({
                              // participants: members,
                              isLoading: false,
                            });
                          }}
                          activeOpacity={TOUCH_OPACITY}
                          style={{
                            height: WP10,
                            width: WP10,
                            borderWidth: 1,
                            borderRadius: WP5,
                            borderColor: GREEN,
                            alignItems: "center",
                            justifyContent: "center",
                            marginVertical: WP4,
                          }}
                        >
                          <Image
                            style={{
                              width: "100%",
                              alignItems: "center",
                            }}
                            size="tiny"
                            centered
                            source={require("../../../assets/icons/v3/checkGreen.png")}
                          />
                        </TouchableOpacity>
                      </View>
                    )}
                  </View>
                );
              })
            : null
        }
      />
    );
  };

  render() {
    const { navigateBack } = this.props;
    const { isReady, scrollEnabled } = this.state;
    return (
      <Container
        // isLoading={isLoading}
        scrollable={scrollEnabled}
        scrollBackgroundColor={WHITE}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={() => navigateBack()}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="My Collaboration Project"
              centered
            />
            <LinearGradient
              colors={SHADOW_GRADIENT}
              style={headerStyle.headerShadow}
            />
          </View>
        )}
      >
        {isReady ? this._renderContent() : this._renderSkeleton()}
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps)(CollaborationDetailScreen),
  mapFromNavigationParam
);
