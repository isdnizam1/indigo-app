import React from "react";
import { View, Image as RNImage, FlatList } from "react-native";
import { connect } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";
import {
  NAVY_DARK,
  PALE_GREY_TWO,
  PALE_WHITE,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SILVER_TWO,
} from "../../../constants/Colors";
import ColalboratorItem from "sf-components/collaboration/ColalboratorItem";
import Text from "../../../components/Text";
import Container from "../../../components/Container";
import _enhancedNavigation from "../../../navigation/_enhancedNavigation";
import HeaderNormal from "../../../components/HeaderNormal";
import headerStyle from "../../profile/v2/ProfileScreen/style";
import {
  HP50,
  WP1,
  WP105,
  WP2,
  WP24,
  WP27,
  WP3,
  WP4,
  WP5,
  WP6,
  WP8,
} from "../../../constants/Sizes";
import {
  getCity,
  getJob,
  getListCollaborator,
  postProfileFollow,
  postProfileUnfollow,
} from "../../../actions/api";
import {
  BORDER_STYLE,
  SHADOW_STYLE,
  TOUCH_OPACITY,
} from "../../../constants/Styles";
import {
  FollowItem,
  Icon,
  Image,
  SelectModalFilter,
  SelectModalV2,
} from "../../../components";
import { difference, isEmpty, noop, startCase, toLower } from "lodash-es";
import { TouchableOpacity } from "react-native-gesture-handler";
import EmptyState from "../../../components/EmptyState";

const mapStateToProps = ({ auth }) => ({ userData: auth.user });

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam("onRefresh", () => {}),
});

const ButtonIcon = ({ onChange, filter, onPress = () => {} }) => (
  <TouchableOpacity
    onPress={onPress}
    activeOpacity={TOUCH_OPACITY}
    style={{
      width: filter !== 0 ? WP27 : WP24,
      backgroundColor: PALE_WHITE,
      borderWidth: 1,
      borderColor: onChange ? REDDISH : SILVER_TWO,
      borderRadius: 25,
      paddingVertical: WP2,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      ...SHADOW_STYLE["shadowThin"],
    }}
  >
    <Image
      source={
        filter !== 0
          ? require("../../../assets/icons/v3/icFilterBlue.png")
          : require("../../../assets/icons/v3/icFilter.png")
      }
      style={{ marginVertical: WP2, marginRight: WP2 }}
      imageStyle={{ height: WP4, paddingBottom: WP1 }}
      aspectRatio={7 / 8}
    />

    <Text
      centered
      type="Circular"
      size="mini"
      weight={400}
      color={onChange ? REDDISH : SHIP_GREY}
    >
      Filter
    </Text>
    {filter !== 0 && (
      <Text
        centered
        type="Circular"
        size="mini"
        weight={400}
        color={onChange ? REDDISH : SHIP_GREY}
        style={{ marginLeft: WP1 }}
      >
        ({filter})
      </Text>
    )}
  </TouchableOpacity>
);

class MyNetworkScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      isLoading: true,
      listCollaborator1: [],
      listCollaborator2: [],
      job: [],
      filter: false,
      location: [],
      scrollEnabled: true,
      participants: this.props.participants,
    };
  }

  componentDidMount = async () => {
    this._getListCollaborator();
  };

  _onChange = (key) => async (value) => {
    const { job } = this.state;
    if (key == "job") {
      if (job.length !== 0) {
        await this.setState({ [key]: job.concat(value) });
      } else await this.setState({ [key]: value });
    } else await this.setState({ [key]: value });
  };

  _getListCollaborator = async () => {
    const { dispatch, userData } = this.props;
    const { job, location } = this.state;

    let item;
    if (job.length !== 0) {
      item = job.map((job) => job.job_title);
    }

    const params =
      job.length == 0 && location.length == 0
        ? { id_user: userData.id_user }
        : location.length !== 0 && job.length !== 0
        ? { id_user: userData.id_user, proffesion: item, city_name: location }
        : location.length == 0 && job.length !== 0
        ? { id_user: userData.id_user, proffesion: item }
        : { city_name: location, id_user: userData.id_user };

    try {
      const response = await dispatch(
        getListCollaborator,
        params,
        noop,
        true,
        false
      );
      const dataLength = response.result.length;
      if (dataLength > 6) {
        const listCollaborator2 = response.result.splice(6, dataLength);
        const listCollaborator1 = response.result.splice(0, 6);
        this.setState({
          listCollaborator1: listCollaborator1,
          listCollaborator2: listCollaborator2,
          isLoading: false,
        });
      } else {
        this.setState({
          listCollaborator1: response.result,
          listCollaborator2: null,
          isLoading: false,
        });
      }
      this.setState({ isLoading: false });
    } catch (error) {
      //
    }
  };

  _onFollowed = async (follow, destinationId, isLoading = true) => {
    const {
      userData: { id_user },
      dispatch,
    } = this.props;

    const currentAction = follow ? postProfileFollow : postProfileUnfollow;
    const currentPayload = follow
      ? { id_user: destinationId, followed_by: id_user }
      : {
          id_user,
          id_user_following: destinationId,
        };

    dispatch(currentAction, currentPayload, noop, true, isLoading);
    const updatedUsers = users.map((user) => {
      if (user.id_user == destinationId) {
        user.is_followed_by_viewer = !user.is_followed_by_viewer;
      }
      return user;
    });
    this.setState({ users: updatedUsers });
  };

  _locationSection = () => {
    const { location } = this.state;
    return (
      <SelectModalV2
        extraResult
        refreshOnSelect
        triggerComponent={
          <View
            style={{
              width: "100%",
              backgroundColor: PALE_GREY_TWO,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between",
              paddingTop: WP4,
              paddingBottom: WP3,
            }}
          >
            <View
              style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            >
              <Icon
                centered
                background="dark-circle"
                size="small"
                color={REDDISH}
                name="location-on"
                type="MaterialIcons"
              />
              <Text numberOfLines={1} style={{ flex: 1 }}>
                <Text
                  type="Circular"
                  size="mini"
                  weight={300}
                  color={SHIP_GREY}
                >
                  Collaboration at:
                </Text>
                <Text
                  type="Circular"
                  size="mini"
                  weight={300}
                  color={REDDISH}
                >{` ${
                  isEmpty(this.state.location)
                    ? "All City"
                    : this.state.location
                }`}</Text>
              </Text>
            </View>
            <Text type="Circular" size="xmini" weight={400} color={REDDISH}>
              Edit
            </Text>
          </View>
        }
        header="Select City"
        suggestion={getCity}
        suggestionKey="city_name"
        suggestionPathResult="city_name"
        onChange={async (value) => {
          const city = [value];
          this._onChange("location")(city);
          await this.setState({ isLoading: true });
          this._getListCollaborator();
        }}
        selected={location}
        reformatFromApi={(text) => startCase(toLower(text))}
        createNew={false}
        placeholder="Search city here..."
      />
    );
  };

  _collaboratorSection = (ads) => {
    const {
      navigateTo,
      userData: { id_user },
    } = this.props;
    const { isLoading, collaborator, listCollaborator1 } = this.state;

    const dataList = isLoading ? [1, 2, 3, 4] : listCollaborator1;

    return (
      <FlatList
        bounces={false}
        bouncesZoom={false}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        style={{ flexGrow: 0 }}
        contentContainerStyle={{
          paddingLeft: WP4,
          paddingTop: WP6,
        }}
        key={(item, index) => item.key}
        keyExtractor={(item, index) => item.key}
        data={dataList}
        extraData={dataList}
        renderItem={({ item, index }) => {
          return (
            <ColalboratorItem
              myId={id_user}
              ads={item}
              loading={isLoading}
              onPressItem={() => {
                navigateTo(
                  "ProfileScreenNoTab",
                  { idUser: item.id_user },
                  "push"
                );
              }}
            />
          );
        }}
        ListEmptyComponent={
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              height: HP50 * 0.85,
            }}
          >
            <EmptyState
              image={require(`sf-assets/icons/ic_collab_collaboration_emptystate.png`)}
              title={"Belum ada collaborator saat ini"}
              message={
                "Maaf, untuk saat ini belum ada\n collaborator yang tersedia"
              }
            />
          </View>
        }
      />
    );
  };

  _listCollaborator = () => {
    const {
      navigateTo,
      userData: { id_user },
    } = this.props;
    const { isLoading, listCollaborator2 } = this.state;

    const dataList = isLoading ? [1, 2, 3, 4] : listCollaborator2;

    return (
      <FlatList
        bounces={false}
        bouncesZoom={false}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        style={{ flexGrow: 0, paddingHorizontal: WP4 }}
        contentContainerStyle={{
          paddingTop: WP4,
        }}
        key={(item, index) => item.key}
        keyExtractor={(item, index) => item.key}
        data={dataList}
        extraData={dataList}
        renderItem={({ item, index }) => {
          return (
            <FollowItem
              onPressItem={() => {
                navigateTo(
                  "ProfileScreenNoTab",
                  { idUser: item.id_user },
                  "push"
                );
              }}
              imageSize="small"
              isMine={false}
              key={`${index}${item.id_user}`}
              user={item}
              onCollab={true}
              onFollow={() => {
                const isFollowed = item.followed_by;
                this._onFollowed(!isFollowed, item.id_user, false);
              }}
              style={{
                marginHorizontal: 0,
                paddingVertical: WP3,
              }}
            />
          );
        }}
        ListEmptyComponent={
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              height: HP50 * 0.85,
            }}
          >
            <EmptyState
              image={require(`sf-assets/icons/ic_collab_collaboration_emptystate.png`)}
              title={"Belum ada collaborator saat ini"}
              message={
                "Maaf, untuk saat ini belum ada\n collaborator yang tersedia"
              }
            />
          </View>
        }
      />
    );
  };

  _renderContent = () => {
    const { listCollaborator2, isLoading, job } = this.state;

    return (
      <View>
        <View
          style={{
            width: "100%",
            paddingHorizontal: WP4,
            paddingTop: WP8,
            paddingBottom: WP3,
            ...BORDER_STYLE["bottom"],
          }}
        >
          <Text
            type="Circular"
            size="large"
            weight={600}
            color={NAVY_DARK}
            style={{ marginBottom: WP105 }}
          >
            Explore Opportunities
          </Text>
          <Text type="Circular" size="slight" weight={300} color={SHIP_GREY}>
            Expand your network based on interest
          </Text>
          {this._locationSection()}

          <SelectModalFilter
            refreshOnSelect
            triggerComponent={
              <ButtonIcon
                onChange={this.state.filter}
                filter={this.state.job.length}
                iconName={"plus"}
                title={"Filter"}
              />
            }
            header="Filter"
            suggestion={getJob}
            search={false}
            suggestionKey="job_title"
            suggestionPathResult="job_title"
            showSuggestion={true}
            onChange={async (value) => {
              const selection = difference(value, job);
              this._onChange("job")(selection);
              await this.setState({ filter: true, isLoading: true });
              this._getListCollaborator();
            }}
            createNew={true}
            selected={job}
            selectionWording="Interest Terpilih"
            showSelection
            onRemoveSelection={(index) => {}}
            quickSearchTitle={"Saran Pencarian Interest"}
          />

          <View style={{ paddingTop: WP8, paddingBottom: WP4 }}>
            <Text type="Circular" size="small" color={NAVY_DARK} weight={600}>
              Top Collaborators
            </Text>

            {this._collaboratorSection()}
          </View>
        </View>
        {!isLoading && !isEmpty(listCollaborator2) && this._listCollaborator()}
      </View>
    );
  };

  render() {
    const { navigateBack } = this.props;
    const { isReady, isLoading, scrollEnabled } = this.state;
    return (
      <Container
        isReady={isReady}
        // isLoading={isLoading}
        scrollable={scrollEnabled}
        isAvoidingView
        scrollBackgroundColor={PALE_WHITE}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={() => navigateBack()}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="My Network"
              centered
            />
            <LinearGradient
              colors={SHADOW_GRADIENT}
              style={headerStyle.headerShadow}
            />
          </View>
        )}
      >
        {this._renderContent()}
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(MyNetworkScreen),
  mapFromNavigationParam
);
