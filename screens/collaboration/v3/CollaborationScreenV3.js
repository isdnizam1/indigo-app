import React, { Component, Fragment } from "react";
import {
  View,
  TouchableOpacity,
  FlatList,
  Animated,
  Easing,
} from "react-native";
import { connect } from "react-redux";
import { NavigationEvents } from "@react-navigation/compat";
import { isEmpty, noop, startCase, toLower, get } from "lodash-es";
import SkeletonContent from "react-native-skeleton-content";
import {
  _enhancedNavigation,
  Text,
  Icon,
  Container,
  Image,
} from "sf-components";
import { postLogCreateCollab } from "sf-actions/api";
import {
  WHITE,
  SHIP_GREY_CALM,
  REDDISH,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  NAVY_DARK,
  PALE_GREY,
  PALE_SALMON20,
} from "sf-constants/Colors";
import {
  WP4,
  WP2,
  WP1,
  WP100,
  WP12,
  WP50,
  WP6,
  WP105,
  WP70,
  WP40,
  WP305,
  WP05,
  HP50,
} from "sf-constants/Sizes";
import { TOUCH_OPACITY } from "sf-constants/Styles";
import SoundfrenExploreOptions from "sf-components/explore/SoundfrenExploreOptions";
import Modal from "sf-components/Modal";
import { setCollabData } from "sf-services/screens/lv1/collab/actionDispatcher";
import { setCollabLv1Cache } from "sf-utils/storage";
import { restrictedAction } from "../../../utils/helper";
import { CollaborationOptionMenus } from "../v2/CollaborationOptionMenus";
import EmptyState from "../../../components/EmptyState";
import CollaborationCategory from "../../../components/collaboration/CollaborationCategory";
import {
  getListCollaboration,
  getListCollaborationLv1,
} from "../../../actions/api";
import { MyCollaborationItem } from "../../../components";

const collabConfig = {
  recent: {
    title: "Recently updates",
    route: "CollaboratorListScreen",
    key_data: "recent",
    line: true,
    horizontal: true,
    numColumns: undefined,
    empty_message: "Maaf, untuk saat ini belum ada\ncollaborator yang tersedia",
    empty_message_title: "Belum ada kolaborasi saat ini",
    empty_message_desc:
      "Mulai kolaborasi dengan partisipan lain sekarang juga !",
  },
  popular: {
    title: "Popular collaborations",
    route: "CollaborationListScreen",
    key_data: "popular",
    line: true,
    horizontal: true,
    numColumns: undefined,
    empty_message: "Maaf, untuk saat ini belum ada\nkolaborasi yang tersedia",
    empty_message_title: "Belum ada kolaborasi saat ini",
    empty_message_desc:
      "Mulai kolaborasi dengan partisipan lain sekarang juga !",
  },
  categorySection: [
    {
      category_name: "create_collab",
      label: "Create Collaboration",
      image: require("../../../assets/images/v3/icCreateCollab.png"),
    },
    {
      category_name: "explore_collab",
      label: "Explore Collaboration",
      image: require("../../../assets/images/v3/icExploreCollab.png"),
    },
    {
      category_name: "my_network",
      label: "My Network",
      image: require("../../../assets/images/v3/icMyNetwork.png"),
    },
    {
      category_name: "my_collab_project",
      label: "My Collaboration Project",
      image: require("../../../assets/images/v3/icMyCollabProject.png"),
    },
  ],
};

const mapStateToProps = ({
  auth,
  screen: {
    lv1: { collab },
  },
}) => ({
  userData: auth.user,
  collab,
});

const mapFromNavigationParam = (getParam) => ({
  id_ads: getParam("id_ads", 0),
});

class CollaborationScreenV3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      isLoading: true,
      result: { recent: [], popular: [] },
      totalStep: 3,
      step: 2,
      progress: new Animated.Value((WP100 / 3) * 2),
      fadeAnim: new Animated.Value(1),
      scrollEnabled: true,
    };
  }

  componentDidMount = () => {
    if (this.props.id_ads) {
      this.setState({ scrollEnabled: false }, () =>
        this._animatedProgress(this.state.step + 1)
      );
    }
    this._getContent();
  };

  _getContent = async () => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props;
    try {
      const recent = await dispatch(
        getListCollaboration,
        { id_user, order: "recent" },
        noop,
        true,
        false
      );
      const popular = await dispatch(
        getListCollaboration,
        { id_user, order: "popular" },
        noop,
        true,
        false
      );
      console.log("resultt", popular);
      this.setState({
        result: {
          recent: recent.result,
          popular: popular.result,
        },
        isLoading: false,
      });
    } catch (e) {
      // keep silent
    }
  };

  _createCollab = () => {
    const { navigateTo } = this.props;
    Promise.resolve(navigateTo("CollaborationForm")).then(() => {
      postLogCreateCollab({ id_user: this.props.userData.id_user });
    });
  };

  _progress = () => {
    if (this.props.id_ads && !this.state.scrollEnabled) {
      return (
        <View
          style={{ width: WP100, height: WP05, backgroundColor: PALE_GREY }}
        >
          <Animated.View
            style={{
              height: WP05,
              backgroundColor: WHITE,
              width: this.state.progress,
            }}
          />
        </View>
      );
    }
  };

  _headerSection = (about) => {
    const { navigateBack, navigateTo, id_ads, userData, navigation } =
      this.props;
    const { step, totalStep, fadeAnim, result, isLoading } = this.state;
    return (
      <View>
        <Image
          tint={"black"}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 182}
          source={require("sf-assets/images/bgCollaboration.png")}
        />
        <View
          style={{
            position: "absolute",
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
          }}
        >
          <View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                paddingHorizontal: WP4,
                paddingTop: WP2,
                marginBottom: WP6,
              }}
            >
              <Icon
                centered
                onPress={() => navigateBack()}
                background="dark-circle"
                size="large"
                color={WHITE}
                name="chevron-left"
                type="Entypo"
              />

              <Modal
                position="bottom"
                swipeDirection={null}
                renderModalContent={({ toggleModal }) => {
                  return (
                    <SoundfrenExploreOptions
                      menuOptions={CollaborationOptionMenus}
                      userData={this.props.userData}
                      navigateTo={navigateTo}
                      onClose={toggleModal}
                      onCreate={() => this._createCollab()}
                    />
                  );
                }}
              >
                {({ toggleModal }, M) => (
                  <Fragment>
                    <Icon
                      centered
                      onPress={restrictedAction({
                        action: toggleModal,
                        userData,
                        navigation,
                      })}
                      background="dark-circle"
                      size="large"
                      color={WHITE}
                      name="dots-three-horizontal"
                      type="Entypo"
                    />
                    {M}
                  </Fragment>
                )}
              </Modal>
            </View>
            {this._progress()}
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <SkeletonContent
              containerStyle={{
                alignItems: "center",
                justifyContent: "center",
                paddingHorizontal: WP12,
              }}
              layout={[
                { width: WP50, height: WP6, marginBottom: WP2 },
                { width: WP70, height: WP4, marginBottom: WP105 },
                { width: WP40, height: WP4, marginBottom: WP6 },
              ]}
              isLoading={isLoading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View style={{ alignItems: "center", justifyContent: "center" }}>
                <Text
                  type="Circular"
                  size="large"
                  weight={600}
                  color={WHITE}
                  centered
                  style={{ marginBottom: WP1 }}
                >
                  Collaboration
                </Text>
                <Text
                  type="Circular"
                  size="mini"
                  weight={300}
                  color={WHITE}
                  centered
                >
                  Collaborate your skills and create your dream team now
                </Text>
              </View>
            </SkeletonContent>
          </View>
        </View>
        {id_ads && !this.state.scrollEnabled ? (
          <Animated.View
            style={{
              position: "absolute",
              left: 0,
              top: 0,
              right: 0,
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              backgroundColor: WHITE,
              paddingVertical: WP305,
              paddingHorizontal: WP4,
              opacity: fadeAnim,
            }}
          >
            <View
              style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            >
              <Text
                weight={300}
                type="Circular"
                size={"mini"}
                color={SHIP_GREY_CALM}
              >
                {step == totalStep
                  ? "Publish Berhasil"
                  : "Publish Project Kolaborasi..."}
              </Text>
              {step == totalStep && (
                <Icon
                  style={{ marginLeft: WP2 }}
                  background="dark-circle"
                  backgroundColor={PALE_SALMON20}
                  size="xtiny"
                  color={REDDISH}
                  name="check-bold"
                  type="MaterialCommunityIcons"
                />
              )}
            </View>
            {step == totalStep ? (
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() => {
                  this._animatedFade(this.state.fadeAnim);
                  navigateTo("CollaborationPreview", {
                    id: id_ads,
                    previousScreen: "HomeCollaboration",
                  });
                }}
              >
                <Text
                  weight={300}
                  type="Circular"
                  size={"mini"}
                  color={REDDISH}
                >
                  Lihat
                </Text>
              </TouchableOpacity>
            ) : null}
          </Animated.View>
        ) : null}
      </View>
    );
  };

  _adItem = (ads, i, section, loading) => {
    const { navigateTo, userData, navigation } = this.props;
    const { result, isLoading } = this.state;
    const myAds = ads.id_user === userData.id_user;

    return (
      <TouchableOpacity
        disabled={isLoading}
        style={{ paddingVertical: WP2 }}
        key={`${i}${Math.random()}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={() =>
          navigateTo("CollaborationPreview", {
            id: ads.id_ads,
            previousScreen: "HomeCollaboration",
          })
        }
      >
        <MyCollaborationItem
          ads={ads}
          userData={userData}
          myAds={myAds}
          type={"home"}
          loading={isLoading}
          navigateTo={navigateTo}
        />
      </TouchableOpacity>
    );
  };

  _collabSection = (section) => {
    const { result, isLoading } = this.state;
    const { navigateTo } = this.props;
    const config = collabConfig[section];
    const dummyData = [1, 2, 3];
    const dataList = isLoading
      ? dummyData
      : !isEmpty(result.recent) && !isEmpty(result.popular)
      ? section === "recent"
        ? result.recent
        : result.popular
      : [];
    console.log("recenttt", result.popular);
    if (isEmpty(dataList)) return null;
    return (
      (!isEmpty(result) || isLoading) && (
        <View>
          <View style={{ paddingVertical: WP4 }}>
            {/* HEADER */}
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                paddingHorizontal: WP4,
              }}
            >
              <Text type="Circular" size="small" color={NAVY_DARK} weight={600}>
                {config.title}
              </Text>
              {!isEmpty(config.route) && (
                <TouchableOpacity
                  activeOpacity={TOUCH_OPACITY}
                  onPress={() => {
                    navigateTo(config.route);
                  }}
                >
                  <Text
                    type="Circular"
                    size="xmini"
                    color={REDDISH}
                    weight={400}
                  >
                    See All
                  </Text>
                </TouchableOpacity>
              )}
            </View>

            {/* ITEM */}
            <FlatList
              bounces={false}
              bouncesZoom={false}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              horizontal={config.horizontal}
              numColumns={config.numColumns}
              style={{ flexGrow: 0 }}
              contentContainerStyle={{
                paddingLeft: WP4,
                paddingTop: section == "collaborator" ? WP6 : WP4,
              }}
              data={dataList}
              extraData={dataList}
              renderItem={({ item, index }) => {
                return this._adItem(item, index, section, false);
              }}
              ListEmptyComponent={
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    height: HP50 * 0.85,
                  }}
                >
                  <EmptyState
                    image={require(`sf-assets/icons/ic_collab_collaboration_emptystate.png`)}
                    title={config.empty_message_title}
                    message={config.empty_message_desc}
                  />
                </View>
              }
            />
          </View>
        </View>
      )
    );
  };

  _animatedProgress = (step) => {
    Animated.timing(this.state.progress, {
      toValue: (WP100 / this.state.totalStep) * step,
      duration: 350,
      delay: 500,
      easing: Easing.ease,
      useNativeDriver: false,
    }).start(() => {
      this.setState({ step }, () => {
        setTimeout(() => {
          this._animatedFade(this.state.fadeAnim);
        }, 5000);
      });
    });
  };

  _animatedFade = (fadeAnim) => {
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 500,
      delay: 100,
      useNativeDriver: true,
    }).start(() => {
      setTimeout(() => {
        this.setState({ scrollEnabled: true });
      }, 500);
    });
  };

  render() {
    const { result, scrollEnabled } = this.state;
    const { navigateTo, navigation, userData, isLoading } = this.props;
    return (
      <Container
        theme="dark"
        isLoading={isLoading}
        scrollable={scrollEnabled}
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={() => this._getContent()}
        isReady={true}
      >
        <View>
          <NavigationEvents onDidFocus={() => this._getContent()} />
          {this._headerSection(result?.about)}
          <CollaborationCategory
            navigateTo={navigateTo}
            navigation={this.props.navigation}
            list={collabConfig.categorySection}
            loading={false}
            idUser={userData.id_user}
            userData={userData}
            create_collab={() =>
              restrictedAction({
                action: this._createCollab,
                userData,
                navigation,
              })
            }
          />
          {this._collabSection("recent")}
          {this._collabSection("popular")}
        </View>
      </Container>
    );
  }
}

CollaborationScreenV3.propTypes = {};

CollaborationScreenV3.defaultProps = {};

export default _enhancedNavigation(
  connect(mapStateToProps, { setCollabData })(CollaborationScreenV3),
  mapFromNavigationParam
);
