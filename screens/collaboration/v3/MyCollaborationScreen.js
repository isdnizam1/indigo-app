import React, { Component } from "react";
import { TouchableOpacity, View, Image as RNImage } from "react-native";
import { connect } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";
import {
  PALE_GREY_THREE,
  SHADOW_GRADIENT,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
} from "../../../constants/Colors";
import Container from "../../../components/Container";
import _enhancedNavigation from "../../../navigation/_enhancedNavigation";
import HeaderNormal from "../../../components/HeaderNormal";
import headerStyle from "../../profile/v2/ProfileScreen/style";
import CustomTabController from "../../../components/CustomTabController";
import CollaborationSkeleton from "../../../components/collaboration/CollaborationSkeleton";
import ChatSkeletonConfig from "../../../components/chat/ChatSkeletonConfig";
import produce from "immer";
import { HP50, WP105, WP4, WP5, WP8, WP80 } from "../../../constants/Sizes";
import { getMyCollaboration } from "../../../actions/api";
import { TOUCH_OPACITY } from "../../../constants/Styles";
import { MyCollaborationItem } from "../../../components";
import SkeletonContent from "react-native-skeleton-content";
import EmptyState from "../../../components/EmptyState";

const mapStateToProps = ({ auth }) => ({ userData: auth.user });

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam("onRefresh", () => {}),
  defaultTab: getParam("defaultTab", 0),
});

const tabIndex = {
  myProject: 0,
  joined: 1,
};

class MyCollaborationScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      isLoading: true,
      index: tabIndex[this.props.defaultTab] || 0,
      routes: [
        { key: "myProject", title: "My Project" },
        { key: "joined", title: "Joined" },
      ],
      myProject: [],
      joinedProject: [],
      isEmpty: false,
      scrollEnabled: true,
    };
  }

  componentDidMount = () => {
    this._getMyProject();
    this._getJoinedProject();
  };

  _renderSkeleton = () => {
    const dummy = [1, 2, 3, 4, 5, 6];
    return (
      <View>
        <SkeletonContent
          containerStyle={{
            ...CollaborationSkeleton.container,
            paddingBottom: WP5,
            alignItems: "center",
            justifyContent: "center",
          }}
          layout={[
            CollaborationSkeleton.layout.image,
            CollaborationSkeleton.layout.label,
            {
              ...CollaborationSkeleton.layout.button,
              alignItems: "center",
              justifyContent: "center",
            },
          ]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
        {dummy.map(() => this._renderSkeletonItem())}
        <SkeletonContent
          containerStyle={{
            ...CollaborationSkeleton.container,
            paddingVertical: WP5,
            alignItems: "center",
            justifyContent: "center",
          }}
          layout={[{ width: WP80, height: WP8, marginBottom: WP105 }]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
      </View>
    );
  };

  _renderSkeletonItem = () => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-start",
          marginHorizontal: WP4,
          borderBottomWidth: 1,
          borderBottomColor: PALE_GREY_THREE,
        }}
      >
        <SkeletonContent
          containerStyle={{ flexGrow: 0 }}
          layout={[ChatSkeletonConfig.layouts.avatar]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
        <SkeletonContent
          containerStyle={{ flexGrow: 1, paddingVertical: WP4 }}
          layout={[
            ChatSkeletonConfig.layouts.roomName,
            ChatSkeletonConfig.layouts.lastComment,
          ]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
      </View>
    );
  };

  _getMyProject = async () => {
    const { userData } = this.props;
    const { myProject } = this.state;
    const params = {
      id_user: userData.id_user,
      shortlist: "request",
      limit: 15,
    };

    const dataResponse = await getMyCollaboration(params);
    if (dataResponse.data.code == 200) {
      this.setState({
        myProject: dataResponse.data.result,
      });
    }
    this.setState({
      isReady: true,
      isLoading: false,
    });
    if (myProject.length == 0) {
      this.setState({
        isEmpty: true,
      });
    }
  };

  _getJoinedProject = async () => {
    const { userData } = this.props;
    const params = {
      id_user: userData.id_user,
      shortlist: "joined",
      limit: 15,
    };

    const dataResponse = await getMyCollaboration(params);
    if (dataResponse.data.code == 200) {
      this.setState({
        joinedProject: dataResponse.data.result,
      });
    }
    this.setState({
      isReady: true,
      isLoading: false,
    });
  };

  _onChangeTab = (index) => {
    this.setState(
      produce((draft) => {
        draft.index = index;
        draft.isTabReady = false;
      })
    );
  };

  _renderContent = ({ route }) => {
    const { navigateTo, userData } = this.props;
    const { joinedProject, isReady, myProject, isLoading } = this.state;

    switch (route.key) {
      case "myProject":
        return !isLoading && myProject.length == 0 ? (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              height: HP50 * 0.85,
            }}
          >
            <EmptyState
              image={require(`sf-assets/icons/ic_collab_collaboration_emptystate.png`)}
              title={"No Collaborations You Created"}
              message={"Start collaborating with other participants right now!"}
            />
          </View>
        ) : (
          <View
            style={{
              alignItems: "center",
              marginLeft: WP4,
              marginTop: WP4,
              flexGrow: 0,
            }}
          >
            {myProject.map((item, index) => (
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                style={{ marginBottom: WP4 }}
                key={item.id_ads}
                onPress={() =>
                  navigateTo("CollaborationDetailScreen", {
                    typeProject: "myProject",
                    id_ads: item.id_ads,
                    // user_request: item.
                  })
                }
              >
                <MyCollaborationItem
                  key={`${index}${item.id_ads}`}
                  ads={item}
                  myAds={true}
                  userData={userData}
                  type={"myProject"}
                  loading={!isReady}
                  navigateTo={navigateTo}
                />
              </TouchableOpacity>
            ))}
          </View>
        );
      case "joined":
        return !isLoading && joinedProject.length == 0 ? (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              height: HP50 * 0.85,
            }}
          >
            <EmptyState
              image={require(`sf-assets/icons/ic_collab_collaboration_emptystate.png`)}
              title={"No Collaborations You Joined"}
              message={"Start collaborating with other participants right now!"}
            />
          </View>
        ) : (
          <View
            style={{
              alignItems: "center",
              marginLeft: WP4,
              marginTop: WP4,
              flexGrow: 0,
            }}
          >
            {joinedProject.map((item, index) => (
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                key={item.id_ads}
                style={{ marginBottom: WP4 }}
                onPress={() =>
                  navigateTo("CollaborationDetailScreen", {
                    typeProject: "joined",
                    id_ads: item.id_ads,
                  })
                }
              >
                <MyCollaborationItem
                  key={`${index}${item.id_ads}`}
                  ads={item}
                  myAds={false}
                  type={"joined"}
                  loading={!isReady}
                  navigateTo={navigateTo}
                />
              </TouchableOpacity>
            ))}
          </View>
        );

      default:
        null;
    }
  };

  render() {
    const { navigateBack } = this.props;
    const { isReady, index, routes, isLoading, scrollEnabled } = this.state;
    return (
      <Container
        isLoading={isLoading}
        scrollable={scrollEnabled}
        scrollBackgroundColor={WHITE}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={() => navigateBack()}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="My Collaboration Project"
              centered
            />
            <LinearGradient
              colors={SHADOW_GRADIENT}
              style={headerStyle.headerShadow}
            />
          </View>
        )}
      >
        <View>
          <CustomTabController
            onPress={this._onChangeTab}
            activeIndex={index}
            routes={routes}
          />
        </View>
        {this._renderContent({ route: routes[index] })}
        {/* {isReady
          ? this._renderContent({ route: routes[index] })
          : this._renderSkeleton()} */}
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps)(MyCollaborationScreen),
  mapFromNavigationParam
);
