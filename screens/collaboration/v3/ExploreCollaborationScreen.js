import React, { Fragment } from "react";
import { View, Image as RNImage, FlatList } from "react-native";
import { connect } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";
import {
  PALE_WHITE,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SILVER_TWO,
  WHITE,
} from "../../../constants/Colors";
import headerStyle from "../../profile/v2/ProfileScreen/style";
import Text from "../../../components/Text";
import Container from "../../../components/Container";
import _enhancedNavigation from "../../../navigation/_enhancedNavigation";
import {
  WP1,
  WP100,
  WP2,
  WP24,
  WP27,
  WP3,
  WP4,
  WP5,
} from "../../../constants/Sizes";
import {
  getAdsCollab,
  getJob,
  getListCollaboration,
} from "../../../actions/api";
import { SHADOW_STYLE, TOUCH_OPACITY } from "../../../constants/Styles";
import {
  Icon,
  Image,
  MyCollaborationItem,
  SelectModalFilter,
} from "../../../components";
import { difference, isObject, noop } from "lodash-es";
import { TouchableOpacity } from "react-native-gesture-handler";
import { restrictedAction } from "../../../utils/helper";

const mapStateToProps = ({ auth }) => ({ userData: auth.user });

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam("onRefresh", () => {}),
});

const ButtonIcon = ({ onChange, filter, onPress = () => {} }) => (
  <TouchableOpacity
    onPress={onPress}
    activeOpacity={TOUCH_OPACITY}
    style={{
      width: filter !== 0 ? WP27 : WP24,
      backgroundColor: PALE_WHITE,
      borderWidth: 1,
      borderColor: onChange ? REDDISH : SILVER_TWO,
      borderRadius: 25,
      paddingVertical: WP2,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      ...SHADOW_STYLE["shadowThin"],
    }}
  >
    <Image
      source={
        onChange
          ? require("../../../assets/icons/v3/icFilterBlue.png")
          : require("../../../assets/icons/v3/icFilter.png")
      }
      style={{ marginVertical: WP2, marginRight: WP2 }}
      imageStyle={{ height: WP4, paddingBottom: WP1 }}
      aspectRatio={7 / 8}
    />

    <Text
      centered
      type="Circular"
      size="mini"
      weight={400}
      color={onChange ? REDDISH : SHIP_GREY}
    >
      Filter
    </Text>
    {filter !== 0 && (
      <Text
        centered
        type="Circular"
        size="mini"
        weight={400}
        color={onChange ? REDDISH : SHIP_GREY}
        style={{ marginLeft: WP1 }}
      >
        ({filter})
      </Text>
    )}
  </TouchableOpacity>
);

class ExploreCollaborationScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: true,
      isLoading: true,
      collaborations: [],
      job: [],
      filter: false,
      isSearching: false,
      location: "",
      scrollEnabled: true,
      participants: this.props.participants,
    };
  }

  componentDidMount = async () => {
    this._getListCollaboration();
  };

  _performSearch = ({ search, location_name, loadMore }) => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props;

    this.setState({
      collaborations: [],
      isSearching: true,
      isLoading: true,
    });
    console.log("search", search);
    dispatch(getListCollaboration, {
      title: search,
      id_user,
      city_name: location_name,
      start: loadMore ? this.state[show].length : 0,
      limit: 5,
    }).then((result) => {
      console.log("collaboration, ", result);
      isObject(result) &&
        this.setState({
          collaborations: [...this.state.collaborations, ...result],
        });

      this.setState({ isSearching: false, isLoading: false });
    });
  };

  _onChange = (key) => async (value) => {
    const { job } = this.state;
    if (key == "job") {
      if (job.length !== 0) {
        await this.setState({ [key]: job.concat(value) });
      } else await this.setState({ [key]: value });
    }
  };

  _getListCollaboration = async () => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props;
    const { job } = this.state;

    let item;
    if (job.length !== 0) {
      item = job.map((job) => job.job_title);
    }

    let params = job.length == 0 ? { id_user } : { id_user, proffesion: item };

    try {
      const response = await dispatch(
        getListCollaboration,
        params,
        noop,
        true,
        false
      );
      this.setState({
        collaborations: response.result,
        isLoading: false,
      });
    } catch (e) {
      // keep silent
    }
  };

  _headerSearch = () => {
    const { navigateBack, navigateTo, userData, navigation } = this.props;
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingHorizontal: WP4,
            paddingVertical: WP2,
            backgroundColor: WHITE,
          }}
        >
          <Icon
            centered
            onPress={() => navigateBack()}
            background="dark-circle"
            size="large"
            color={SHIP_GREY_CALM}
            name="chevron-left"
            type="Entypo"
          />
          <TouchableOpacity
            onPress={restrictedAction({
              action: () =>
                navigateTo("SearchCollaborationScreen", {
                  onSubmitCallback: (search, location) => {
                    console.log("from search", search, location);
                    this.setState({ isLoading: true });
                    this._performSearch({
                      search: search,
                      location_name: location,
                      loadMore: false,
                    });
                  },
                }),
              userData,
              navigation,
            })}
            activeOpacity={TOUCH_OPACITY}
            style={{
              flex: 1,
              minWidth: "85%",
              backgroundColor: "rgba(145, 158, 171, 0.1)",
              borderRadius: 6,
              flexDirection: "row",
              alignItems: "center",
              paddingHorizontal: WP2,
              paddingVertical: WP5,
              marginHorizontal: WP2,
            }}
          >
            <Icon
              centered
              background="dark-circle"
              size="large"
              color={SHIP_GREY_CALM}
              name="magnify"
              type="MaterialCommunityIcons"
              style={{ marginRight: WP1 }}
            />
            <Text
              type="Circular"
              size="mini"
              weight={300}
              color={SHIP_GREY_CALM}
              centered
            >
              Try to search Collaboration
            </Text>
          </TouchableOpacity>
        </View>
        <LinearGradient
          colors={SHADOW_GRADIENT}
          style={headerStyle.headerShadow}
        />
      </View>
    );
  };

  _renderContent = () => {
    const { navigateTo, userData } = this.props;
    const { collaborations, isLoading, job } = this.state;

    const data = isLoading ? [1, 2, 3] : collaborations;
    return (
      <View
        style={{
          width: "100%",
          marginHorizontal: WP4,
          marginTop: WP4,
        }}
      >
        <SelectModalFilter
          refreshOnSelect
          triggerComponent={
            <ButtonIcon
              onChange={this.state.filter}
              iconName={"plus"}
              title={"Filter"}
              filter={job.length}
            />
          }
          header="Filter"
          suggestion={getJob}
          search={false}
          suggestionKey="job_title"
          suggestionPathResult="job_title"
          showSuggestion={true}
          onChange={async (value) => {
            const selection = difference(value, job);
            this._onChange("job")(selection);
            await this.setState({ filter: true, isLoading: true });
            this._getListCollaboration();
          }}
          createNew={true}
          selected={job}
          selectionWording="Interest Terpilih"
          showSelection
          onRemoveSelection={(index) => {}}
          quickSearchTitle={"Saran Pencarian Interest"}
        />

        {data.map((item, index) => {
          const myAds = item.id_user === userData.id_user;
          return (
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              style={{
                marginBottom: WP4,
                marginTop: WP4,
                height: WP100,
              }}
              key={item.id_ads}
              onPress={() =>
                navigateTo("CollaborationPreview", {
                  id: item.id_ads,
                  previousScreen: "HomeCollaboration",
                })
              }
            >
              <MyCollaborationItem
                key={`${index}${item.id_ads}`}
                ads={item}
                myAds={myAds}
                userData={userData}
                image={item.image}
                type={"explore"}
                loading={isLoading}
                navigateTo={navigateTo}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  render() {
    const { navigateBack } = this.props;
    const { isReady, isLoading, scrollEnabled } = this.state;

    return (
      <Container
        isReady={isReady}
        // isLoading={isLoading}
        scrollable={scrollEnabled}
        isAvoidingView
        scrollBackgroundColor={"rgba(235,235,235)"}
        renderHeader={this._headerSearch}
      >
        <View>{this._renderContent()}</View>
      </Container>
    );
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(ExploreCollaborationScreen),
  mapFromNavigationParam
);
