import React, { createRef, Fragment } from "react";
import { connect } from "react-redux";
import {
  FlatList,
  ImageBackground,
  Keyboard,
  KeyboardAvoidingView,
  Modal as RNModal,
  Platform,
  ScrollView,
  TouchableOpacity,
  View,
  Clipboard
} from "react-native";
import {
  isEmpty,
  isNil,
  isObject,
  isUndefined,
  map,
  noop,
  isFunction,
} from "lodash-es";
import HTMLElement from "react-native-render-html";
import { IGNORED_TAGS } from "react-native-render-html/src/HTMLUtils";
import moment from "moment/min/moment-with-locales";
import {
  _enhancedNavigation,
  Avatar,
  Card,
  Container,
  HeaderNormal,
  Icon,
  Image,
  Text,
} from "sf-components";
import ButtonV2 from "sf-components/ButtonV2";
import DeletePost from "sf-components/DeletePost";
import ReadMore from "sf-components/ReadMore";
import MentionTextInput from "sf-components/MentionTextInput";
import SoundfrenExploreOptions from "sf-components/explore/SoundfrenExploreOptions";
import Modal from "sf-components/Modal";
import {
  GREY_CALM_SEMI,
  GUN_METAL,
  PALE_BLUE,
  PALE_GREY,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SILVER_TWO,
  WHITE,
} from "sf-constants/Colors";
import {
  FONT_SIZE,
  HP1,
  WP05,
  WP1,
  WP10,
  WP100,
  WP2,
  WP3,
  WP305,
  WP4,
  WP5,
  WP6,
} from "sf-constants/Sizes";
import { TOUCH_OPACITY } from "sf-constants/Styles";
import {
  getMentionSuggestions,
  getProfileFollowStatus,
  postAdsComment,
  postCollabPost,
  postDeleteAdsComment,
  postLikeAdsComment,
  postProfileFollow,
} from "sf-actions/api";
import { objectMapper } from "sf-utils/mapper";
import { initiateRoom } from "sf-utils/helper";
import SubmitModal from "sf-components/SubmitModal";
import MenuOptions from "sf-components/MenuOptions";
import ModalMessageView from "sf-components/ModalMessage/ModalMessageView";
import { HEADER, SHADOW_STYLE } from "../../constants/Styles";
import { linkify } from "../../utils/helper";
import { pushDeletedCollabIds } from "../../services/helper/actionDispatcher";
import {
  getDetailCollab,
  getDetailProject,
  postDeleteAds,
  postEditMemberStatus,
  postJoinCollaboration,
} from "../../actions/api";
import { CollaborationOptionMenus } from "./v2/CollaborationOptionMenus";
import {
  MAPPER_FOR_API,
  MAPPER_FROM_API,
  MESSAGE_POP_UP,
  PropsType,
  StateType,
} from "./_constants";
import { WP15, WP20, WP50, WP8, WP88 } from "../../constants/Sizes";
import {
  GREEN,
  LIGHT_TEAL,
  TEAL_GREEN,
  BLUE_LIGHT,
} from "../../constants/Colors";
import LinkPreviewCard from "../../components/LinkPreviewCard";
import { FollowItem } from "../../components";
import ConfirmationModalV3 from "../../components/ConfirmationModalV3";
import { PrivateValueStore } from "@react-navigation/native";

const mapStateToProps = ({ auth, helper: { deletedCollabIds } }) => ({
  userData: auth.user,
  deletedCollabIds,
});

const mapDispatchToProps = {
  pushDeletedCollabIds,
};

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam("initialLoaded", false),
  id: getParam("id", 0),
  previewData: getParam("previewData", {}), // should be like DUMP_COLLABORATION_DATA
  previousScreen: getParam("previousScreen", {}),
});

const propsDefault = {};

class CollaborationPreview extends React.Component {
  scrollOffset = 0;
  scrollContainer = createRef();

  constructor(props) {
    super(props);
    const hasPreviewData = !isEmpty(props.previewData);
    this.state = {
      modalVisible: false,
      joinModal: false,
      takeModal: false,
      backModal: false,
      collaboration: this.props.id ? {} : this.props.previewData,
      onDeletePost: null,
      comments: "",
      followedByMe: true,
      following: false,
      hasPreviewData,
      isReady: hasPreviewData,
      isShowKeyboard: false,
      mentionComponentKey: Math.random(),
      mentions: [],
      mentionSuggestions: [],
      onConfirmDelete: null,
      openPanel: false,
      reply: "",
      replyMentions: [],
      replyTo: null,
      readMoreAbout: false,
      readMoreAdditional: false,
      keyboardHeight: 0,
      isAccepted: false,
      isTakeJoin: false,
    };
    this._getCollaborationDetail = this._getCollaborationDetail.bind(this);
    this._getMentionSuggestion = this._getMentionSuggestion.bind(this);
    this._joinCollaboration = this._joinCollaboration.bind(this);
    this._onSelectSuggestion = this._onSelectSuggestion.bind(this);
    this._parseComment = this._parseComment.bind(this);
    this._postCollabComment = this._postCollabComment.bind(this);
    this._postReplyComment = this._postReplyComment.bind(this);
    this._renderComment = this._renderComment.bind(this);
    this._renderSuggestionsRow = this._renderSuggestionsRow.bind(this);
    this._renderViewMore = this._renderViewMore.bind(this);
    this._profileSreen = this._profileSreen.bind(this);
    this._onFollow = this._onFollow.bind(this);
    this._onPressOverlay = this._onPressOverlay.bind(this);
    this._onEdit = this._onEdit.bind(this);

  }

  _onEdit = () => {
    this.props.navigateTo("CollaborationForm", {
      id: this.state.collaboration.id,
    });
  };

  async componentDidMount() {
    if (!this.props.userData.id_user) {
      this.props.navigateBack();
      this.props.navigateTo("AuthNavigator");
    } else {
      await this._getInitialScreenData();
      this.keyboardDidShowListener = Keyboard.addListener(
        "keyboardDidShow",
        (e) => {
          this.setState({
            keyboardHeight: e.endCoordinates.height,
            isShowKeyboard: true,
          });
        }
      );
      this.keyboardDidHideListener = Keyboard.addListener(
        "keyboardDidHide",
        (e) => {
          this.setState({ isShowKeyboard: false });
        }
      );
    }
  }

  _getInitialScreenData = async (...args) => {
    // FIXME: change this to be interactive flow for the user (modal or alert)
    if (!this.state.hasPreviewData && !this.props.id) this._backHandler();

    await this._getCollaborationDetail(...args);
    await this.setState({
      isReady: true,
    });
  };

  _getCollaborationDetail = async (
    isLoading = true,
    scrollToEnd = false,
    scrollAmount = 0
  ) => {
    const {
      dispatch,
      userData: { id_user },
      id,
    } = this.props;
    if (id) {
      const dataResponse = await dispatch(
        getDetailCollab,
        { id_user, id_ads: id, limit: 3 },
        noop,
        false,
        isLoading
      );

      const responseDetailProject = await dispatch(
        getDetailProject,
        {
          shortlist: "joined",
          id_ads: id,
          id_user,
          limit: 13,
        },
        noop,
        false,
        isLoading
      );

      const collaborationDataFromApi = {
        ...dataResponse,
        additional_data: JSON.parse(dataResponse.additional_data),
        members: responseDetailProject,
      };
      this.setState(
        {
          collaboration: objectMapper(
            collaborationDataFromApi,
            MAPPER_FROM_API
          ),
        },
        async () => {
          const { status: followStatus } = await dispatch(
            getProfileFollowStatus,
            {
              id_user: this.state.collaboration.authorId,
              followed_by: id_user,
            },
            noop,
            false,
            false
          );
          const followedByMe = followStatus === "followed";
          this.setState({ followedByMe });
        }
      );
    }
  };

  _backHandler = async () => {
    this.props.navigateBack();
  };

  _postCollaboration = () => {
    const { navigateTo, dispatch, userData, popToTop } = this.props;
    const { collaboration } = this.state;
    dispatch(postCollabPost, {
      ...objectMapper(collaboration, MAPPER_FOR_API),
      id_user: userData.id_user,
    }).then((data) => {
      navigateTo("FinishScreen", {
        message:
          "We already receive your Collaboration project, It's time for you to take a break and wait for our screening.",
        buttonText: "Go to Collaboration Project",
        buttonBottom: false,
        buttonAction: () => {
          popToTop();
          navigateTo("CollaborationPreview", { id: data.id_ads });
        },
      });
    });
  };

  _joinCollaboration = async (collaboration) => {
    const { navigateTo, userData, dispatch, popToTop } = this.props;
    const { authorId, id } = collaboration;

    this.setState({ isLoading: true });
    const response = await dispatch(
      postJoinCollaboration,
      { id_user: userData.id_user, id_ads: id },
      noop,
      true,
      false
    );
    this.setState({ joinModal: true });
    // const chatRoomDetail = await initiateRoom(userData, { id_user: authorId })
    // chatRoomDetail.template = 'Hai, saya ingin collab dengan anda '
    // await navigateTo('ChatRoomScreen', chatRoomDetail)
  };

  _renderSpecificationCard = (collaboration) => {
    return (
      <Card style={{ marginTop: 1 }}>
        <Text size="xmini" weight={500}>
          Kebutuhan kolaborasi
        </Text>
        <View style={{ marginTop: HP1 }}>
          <Text size="xmini">{collaboration.specification}</Text>
        </View>
      </Card>
    );
  };

  _renderProfileCard = (collaboration, isMine) => {
    const { hasPreviewData } = this.state;
    return (
      <View>
        {!hasPreviewData && (
          <Card style={{ paddingTop: WP3, paddingBottom: WP5 }}>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                flex: 1,
                justifyContent: "space-between",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  flexShrink: 1,
                  alignItems: "center",
                }}
              >
                <View style={{ marginRight: WP4 }}>
                  <Avatar
                    onPress={this._profileSreen(collaboration.authorId)}
                    shadow={false}
                    image={collaboration.authorPicture}
                  />
                </View>
                <View style={{ flexShrink: 1 }}>
                  <Text
                    onPress={this._profileSreen(collaboration.authorId)}
                    style={{ marginBottom: WP05 }}
                    color={GUN_METAL}
                    weight={500}
                    size="mini"
                    type="Circular"
                  >
                    {collaboration.createdBy}
                  </Text>
                  <Text color={SHIP_GREY_CALM} size="xmini">
                    {collaboration.authorJobTitle}
                  </Text>
                </View>
              </View>
            </View>
          </Card>
        )}
      </View>
    );
  };

  _getHeaderText = () =>
    this.state.hasPreviewData
      ? "Create Collaboration Project"
      : "Collaboration Project";

  _getCtaText = () =>
    this.state.hasPreviewData
      ? "Post Collaboration"
      : this.props.userData.id_user === this.state.collaboration.authorId
      ? "Edit"
      : "Join";

  _getCtaAction = () => {
    this.state.hasPreviewData
      ? this._postCollaboration()
      : this.props.userData.id_user === this.state.collaboration.authorId
      ? this.props.navigateTo("CollaborationForm", {
          id: this.state.collaboration.id,
        })
      : this._joinCollaboration(this.state.collaboration);
  };

  _profileSreen = (idUser) => () => {
    this.props.navigateTo("ProfileScreenNoTab", {
      idUser,
      navigateBackOnDone: true,
    });
  };

  _onPressOverlay = () => {
    this.setState({ replyTo: null });
  };

  _cancelDeletePost = () => {
    this.setState({ onDeletePost: null });
  };

  _onFollow = () => {
    const { following, collaboration } = this.state;
    const { dispatch, userData: my } = this.props;
    !following &&
      this.setState({ following: true }, () => {
        dispatch(
          postProfileFollow,
          {
            id_user: collaboration.authorId,
            followed_by: my.id_user,
          },
          noop,
          false,
          false
        ).then(() => this._getCollaborationDetail(false));
      });
  };

  _renderNotification = () => {
    const { isAccepted, isTakeJoin } = this.state;
    const backgroundColor = isAccepted ? TEAL_GREEN : BLUE_LIGHT;
    const iconName = isAccepted
      ? "account-check-outline"
      : "information-outline";
    return (
      <View
        style={{
          backgroundColor: backgroundColor,
          width: WP100,
          height: WP20,
          paddingHorizontal: WP2,
          paddingVertical: WP3,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Icon
          background="dark-circle"
          size={WP10}
          color={WHITE}
          name={iconName}
          type="MaterialCommunityIcons"
          style={{ marginRight: isTakeJoin ? WP2 : null }}
        />
        <View>
          <Text
            size="slight"
            color={WHITE}
            weight={500}
            style={{ marginBottom: WP1, marginRight: WP3 }}
          >
            {isAccepted
              ? "Congrats! You are accepted for this project!"
              : "Currently you are joining this project"}
          </Text>
          {isAccepted && (
            <Text
              size="mini"
              color={WHITE}
              weight={300}
              style={{ marginBottom: WP3 }}
            >
              You can take this offer and follow it right now
            </Text>
          )}
        </View>
      </View>
    );
  };

  _renderConfirmationModal = () => {
    const { joinModal, takeModal, id } = this.state;
    const { previousScreen, navigateTo, popToTop } = this.props;
    const visible =
      previousScreen === "HomeCollaboration" ? joinModal : takeModal;
    const template = joinModal
      ? MESSAGE_POP_UP.JOIN_COLLAB
      : MESSAGE_POP_UP.TAKE_COLLAB;

    const onPressJoin = () => {
      popToTop();
      navigateTo("NotificationScreen", { defaultTab: "activities" }, "push");
    };

    const onPressTakeJoin = () => {
      this._takeAndJoin(id);
    };

    return (
      <ConfirmationModalV3
        visible={joinModal || takeModal}
        title={template.title}
        titleSize={"large"}
        titleWeight={600}
        subtitle={template.content}
        primary={{
          text: template.confirmLabel,
          onPress: joinModal ? onPressJoin : onPressTakeJoin,
        }}
        image={joinModal ? require("../../assets/icons/isSuccess.png") : null}
        secondary={{
          text: template.cancelLabel,
          onPress: () => this.setState({ joinModal: false, takeModal: false }),
          textColor: REDDISH,
          backgroundColor: WHITE,
          style: {
            borderWidth: 1,
            borderColor: REDDISH,
          },
        }}
      />
    );
  };

  _takeAndJoin = async (id_ads) => {
    const { userData, dispatch, popToTop, navigateTo } = this.props;
    const body = {
      id_user: userData.id_user,
      id_ads: id_ads,
      id_user_target: userData.id_user,
      shortlist: 3,
    };

    this.setState({ isLoading: true });
    const response = await dispatch(
      postEditMemberStatus,
      { body },
      noop,
      true,
      false
    );
    this.setState({ isLoading: false, takeModal: false });
    popToTop();
    navigateTo("MyCollaborationScreen", { defaultTab: "joined" }, "push");
  };

  render() {
    const { isLoading, userData: my, navigateTo, previousScreen } = this.props;
    const {
      backModal,
      collaboration,
      followedByMe,
      hasPreviewData,
      isReady,
      mentionComponentKey,
      mentionSuggestions,
      onConfirmDelete,
      replyTo,
      following,
      readMoreAdditional,
      readMoreAbout,
      isShowKeyboard,
      keyboardHeight,
      isAccepted,
      isTakeJoin,
    } = this.state;
    // const isMine = false;
    const isMine = my.id_user === collaboration.authorId;
    const collabMember = isLoading
      ? [1, 2, 3, 4]
      : !isEmpty(collaboration?.members)
      ? collaboration?.members
      : [];

    return (
      <Container
        isLoading={isLoading && isUndefined(collaboration.title)}
        isReady={(isReady && !isLoading) || !isUndefined(collaboration.title)}
        onRefocusScreen={this._getCollaborationDetail}
        onScroll={({ contentOffset: { y: scrollOffset } }) => {
          this.scrollOffset = scrollOffset;
        }}
        outsideScrollContent={() => (
          <View>
            <RNModal
              onRequestClose={() => this.setState({ onConfirmDelete: null })}
              animationType={"fade"}
              transparent
              visible={!isNil(onConfirmDelete)}
            >
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "rgba(0,0,0,.5)",
                }}
              >
                <DeletePost
                  onConfirm={onConfirmDelete}
                  onCancel={() => this.setState({ onConfirmDelete: null })}
                />
              </View>
            </RNModal>
            <RNModal
              onRequestClose={() => this.setState({ replyTo: null })}
              animationType={"slide"}
              transparent
              visible={!isNil(replyTo)}
            >
              <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                style={{ flex: 1, justifyContent: "flex-end" }}
              >
                <TouchableOpacity
                  onPress={this._onPressOverlay}
                  style={{ flex: 1 }}
                />
                {!isNil(replyTo) && (
                  <View
                    onLayout={({
                      nativeEvent: {
                        layout: { height: modalSuggestionOffsetBottom },
                      },
                    }) => this.setState({ modalSuggestionOffsetBottom })}
                    style={{
                      padding: WP5,
                      backgroundColor: WHITE,
                      elevation: 10,
                      paddingTop:
                        mentionSuggestions.length > 0 &&
                        this.state.replyFormPaddingTop
                          ? this.state.replyFormPaddingTop + WP2
                          : WP5,
                    }}
                  >
                    {mentionSuggestions.length == 0 && (
                      <Text
                        size="mini"
                        color={SHIP_GREY_CALM}
                        weight={400}
                        style={{ marginBottom: WP3 }}
                      >
                        Membalas {replyTo.name}
                      </Text>
                    )}
                    <View
                      style={{
                        flexDirection: "row",
                        alignItems: "center",
                      }}
                    >
                      <Avatar
                        size={"xsmall"}
                        shadow={false}
                        onPress={this._profileSreen(my.id_user)}
                        image={my.profile_picture}
                      />
                      <View
                        style={{ flex: 1, paddingLeft: WP3, paddingRight: WP2 }}
                      >
                        <KeyboardAvoidingView
                          behavior={Platform.OS === "ios" ? "padding" : null}
                        >
                          <MentionTextInput
                            key={`${mentionComponentKey}-reply`}
                            autoFocus
                            above
                            placeholder=""
                            textInputStyle={{
                              backgroundColor: PALE_GREY,
                              borderRadius: 5.5,
                              color: GUN_METAL,
                              fontSize: FONT_SIZE["mini"],
                              lineHeight: FONT_SIZE["mini"] * 1.28,
                              maxHeight: FONT_SIZE["mini"] * 5,
                              minHeight:
                                this.state.commentInputMinHeight ||
                                FONT_SIZE["mini"] * 2.75,
                              paddingHorizontal: WP3,
                              paddingVertical: WP1,
                            }}
                            suggestionsPanelStyle={{
                              backgroundColor: "rgba(100,100,100,0.1)",
                            }}
                            onChangedSelected={(selected) => {
                              this.setState({ mentions: selected });
                            }}
                            trigger={"@"}
                            triggerLocation={"new-word-only"} // 'new-word-only', 'anywhere'
                            value={this.state.reply}
                            onChangeText={(val) => {
                              this.setState({ reply: val });
                            }}
                            onOpenSuggestionsPanel={() => {
                              this.setState({ openPanel: true });
                            }}
                            onCloseSuggestionsPanel={() => {
                              this.setState({
                                openPanel: false,
                                mentionSuggestions: [],
                              });
                            }}
                            triggerDelay={2}
                            triggerCallback={this._getMentionSuggestion}
                            _renderSuggestionsRow={this._renderSuggestionsRow}
                            suggestionsData={mentionSuggestions}
                            keyExtractor={(item, index) => {
                              if (item != null) {
                                return item.id;
                              }
                            }}
                            suggestionRowHeight={45}
                            horizontal={false}
                            MaxVisibleRowCount={5}
                          />
                        </KeyboardAvoidingView>
                      </View>
                      <ButtonV2
                        disabled={this.state.reply.length == 0}
                        color={REDDISH}
                        textColor={WHITE}
                        style={{ paddingHorizontal: WP5 }}
                        onPress={this._postReplyComment}
                        onLayout={({
                          nativeEvent: {
                            layout: { height: commentInputMinHeight },
                          },
                        }) => this.setState({ commentInputMinHeight })}
                        text={"Post"}
                      />
                    </View>
                    <View
                      onLayout={({
                        nativeEvent: {
                          layout: { height: replyFormPaddingTop },
                        },
                      }) => this.setState({ replyFormPaddingTop })}
                      style={{
                        position: "absolute",
                        backgroundColor: WHITE,
                        top: 0,
                        width: WP100,
                        zIndex: 999,
                      }}
                    >
                      {map(mentionSuggestions, this._renderSuggestionsRow)}
                    </View>
                  </View>
                )}
              </KeyboardAvoidingView>
            </RNModal>
          </View>
        )}
        renderHeader={() => (
          <View style={{}}>
            <HeaderNormal
              textType={"Circular"}
              textSize={"slight"}
              iconLeftOnPress={this._backHandler}
              text={this._getHeaderText()}
              noRightPadding
              rightComponent={
                <Modal
                  position="bottom"
                  swipeDirection={null}
                  renderModalContent={({ toggleModal }) => {
                    return (
                      <SoundfrenExploreOptions
                        menuOptions={CollaborationOptionMenus}
                        userData={this.props.userData}
                        navigateTo={navigateTo}
                        onClose={toggleModal}
                        onShare={
                          !hasPreviewData
                            ? () => {
                                navigateTo("ShareScreen", {
                                  id: this.props.id,
                                  type: "explore",
                                  title: "Bagikan Collaboration",
                                });
                              }
                            : null
                        }
                      />
                    );
                  }}
                >
                  {({ toggleModal }, M) => (
                    <Fragment>
                      <TouchableOpacity
                        onPress={toggleModal}
                        style={[HEADER.rightIcon, { paddingVertical: WP2 }]}
                      >
                        <Icon
                          centered
                          onPress={toggleModal}
                          background="dark-circle"
                          size="large"
                          color={SHIP_GREY_CALM}
                          name="dots-three-horizontal"
                          type="Entypo"
                        />
                      </TouchableOpacity>
                      {M}
                    </Fragment>
                  )}
                </Modal>
              }
              centered
            />
          </View>
        )}
        onScrollViewRef={(ref) => {
          this.scrollContainer = ref;
        }}
        scrollable
        scrollBackgroundColor={WHITE}
        scrollContentContainerStyle={{ justifyContent: "space-between" }}
      >
        {/* <ScrollView style={{ marginBottom: isAccepted ? WP10 : null }}> */}
        <View
          style={{
            marginBottom: isAccepted ? WP15 : null,
            paddingBottom:
              isShowKeyboard && Platform.OS === "ios" ? keyboardHeight : 0,
          }}
        >
          {(isAccepted || isTakeJoin) && this._renderNotification()}
          <View style={{ backgroundColor: PALE_BLUE }}>
            <ImageBackground
              source={
                isEmpty(collaboration.bannerPictureUri)
                  ? require("../../assets/images/bgDefaultCollab.png")
                  : { uri: collaboration.bannerPictureUri }
              }
              style={{
                width: WP100,
                backgroundColor: GREY_CALM_SEMI,
                aspectRatio: 360 / 202,
              }}
            />
            <Card first style={{ paddingHorizontal: WP5 }}>
              {/*<Text type={'Circular'} size={'large'} weight={500}>{collaboration.title}</Text>*/}
              <Text
                type={"Circular"}
                size={"slight"}
                style={{ lineHeight: 24 }}
                weight={500}
              >
                {collaboration.title}
              </Text>
              <View style={{ flexDirection: "row", marginTop: WP3 }}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  {!hasPreviewData && (
                    <Icon
                      color={SILVER_TWO}
                      centered
                      name="calendar-today"
                      type="MaterialCommunityIcons"
                      style={{ marginRight: WP1 }}
                    />
                  )}
                  <Text
                    color={SHIP_GREY_CALM}
                    centered
                    type={"Circular"}
                    size="xmini"
                  >
                    {!hasPreviewData ? collaboration.createdAt : ""}
                    {!hasPreviewData ? "  ·  " : ""}
                    {collaboration.locationName}
                  </Text>
                </View>
              </View>
              {previousScreen !== "MyCollaboration" && !isMine && (
                <TouchableOpacity
                  activeOpacity={TOUCH_OPACITY}
                  onPress={() => this._joinCollaboration(collaboration)}
                  style={{
                    minWidth: WP50,
                    backgroundColor: REDDISH,
                    borderRadius: 6,
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: WP8,
                    marginBottom: WP4,
                    paddingVertical: WP3,
                    ...SHADOW_STYLE["shadowThin"],
                  }}
                >
                  <Text
                    type="Circular"
                    size="mini"
                    weight={500}
                    color={WHITE}
                    centered
                  >
                    Join
                  </Text>
                </TouchableOpacity>
              )}
            </Card>

            <Card
              style={{
                paddingHorizontal: 0,
                marginTop: 1,
                paddingBottom: WP6,
              }}
            >
              {!hasPreviewData && (
                <Text
                  size="xmini"
                  color={SHIP_GREY}
                  weight={400}
                  style={{ paddingHorizontal: WP5 }}
                >
                  Posted by:
                </Text>
              )}
              {this._renderProfileCard(collaboration, isMine)}
              {!isMine && !hasPreviewData && (
                <View style={{ flexDirection: "row", paddingHorizontal: WP5 }}>
                  {!followedByMe && (
                    <ButtonV2
                      onPress={this._onFollow}
                      icon={!following ? "account-plus" : null}
                      style={{ flex: 1, paddingVertical: 0 }}
                      forceBorderColor={following}
                      borderColor={following ? REDDISH : "rgb(223, 227, 232)"}
                      textColor={following ? REDDISH : SHIP_GREY}
                      color={following ? "rgba(255, 101, 31, 0.1)" : null}
                      text={following ? "Following" : "Follow"}
                    />
                  )}
                  {!followedByMe && <View style={{ width: WP4 }} />}
                  <ButtonV2
                    leftComponent={() => (
                      <Image
                        style={{ marginRight: WP1 + 1.25 }}
                        size={WP305}
                        aspectRatio={1 / 1}
                        source={require("../../assets/icons/send.png")}
                      />
                    )}
                    onPress={() => {
                      this._joinCollaboration.bind(this);
                    }}
                    style={{ flex: 1 }}
                    text={"Send Message"}
                  />
                </View>
              )}
              {isMine && !hasPreviewData && (
                <View style={{ paddingHorizontal: WP5 }}>
                  <View>
                    <ButtonV2
                      onPress={this._onEdit}
                      type={"MaterialIcons"}
                      icon={"pencil"}
                      iconColor={REDDISH}
                      textColor={REDDISH}
                      style={{
                        flex: 1,
                        paddingVertical: 0,
                        borderColor: REDDISH,
                      }}
                      key={Math.random()}
                      text={"Edit Project"}
                    />
                  </View>
                  <View style={{ width: WP3, height: WP4 }} />
                  <ButtonV2
                    onPress={() =>
                      this.setState({
                        onDeletePost: () => {
                          this.props.dispatch(postDeleteAds, {
                            id_ads: collaboration.id,
                            id_user: this.props.userData.id_user,
                          });
                          this.props.pushDeletedCollabIds(collaboration.id);
                          this.props.navigateBack();
                        },
                      })
                    }
                    icon={"check-underline"}
                    type={"MaterialCommunityIcons"}
                    iconColor={WHITE}
                    textColor={WHITE}
                    style={{
                      paddingHorizontal: WP3,
                      paddingVertical: 0,
                      backgroundColor: REDDISH,
                    }}
                    key={Math.random()}
                    text={"Close Project"}
                  />
                </View>
              )}
              {!hasPreviewData && (
                <View
                  style={{
                    height: 1.0,
                    marginVertical: WP5,
                    backgroundColor: PALE_GREY,
                    marginHorizontal: WP5,
                  }}
                />
              )}
              <View style={{ paddingHorizontal: WP5 }}>
                <Text size="xmini" color={SHIP_GREY} weight={400}>
                  Project status:
                </Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <View
                    style={{
                      backgroundColor: LIGHT_TEAL,
                      paddingVertical: WP1,
                      paddingHorizontal: WP2,
                      borderRadius: 5.5,
                      marginRight: WP2,
                      marginTop: WP3,
                    }}
                  >
                    <Text size="xmini" color={GREEN} weight={400}>
                      Opened
                    </Text>
                  </View>
                </View>
                <View style={{ height: WP4 }} />
                <Text size="xmini" color={SHIP_GREY} weight={400}>
                  Searched positions:
                </Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  {map(collaboration.professions, (profession, index) => (
                    <View
                      key={index}
                      style={{
                        backgroundColor: PALE_GREY,
                        paddingVertical: WP1,
                        paddingHorizontal: WP2,
                        borderRadius: 5.5,
                        marginRight: WP2,

                        marginTop: WP3,
                      }}
                    >
                      <Text size="xmini" color={SHIP_GREY_CALM} weight={400}>
                        {profession?.name}
                      </Text>
                    </View>
                  ))}
                </View>
                <View style={{ height: WP4 }} />
                <Text size="xmini" color={SHIP_GREY} weight={400}>
                  Member quota:
                </Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <View
                    style={{
                      backgroundColor: PALE_GREY,
                      paddingVertical: WP1,
                      paddingHorizontal: WP2,
                      borderRadius: 5.5,
                      marginRight: WP2,
                      marginTop: WP3,
                    }}
                  >
                    <Text size="xmini" color={SHIP_GREY_CALM} weight={400}>
                      {collaboration?.member_kuota || 0}
                    </Text>
                  </View>
                </View>
                <View style={{ height: WP4 }} />
                <Text size="xmini" color={SHIP_GREY} weight={400}>
                  Duration project:
                </Text>
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <View
                    style={{
                      backgroundColor: PALE_GREY,
                      paddingVertical: WP1,
                      paddingHorizontal: WP2,
                      borderRadius: 5.5,
                      marginRight: WP2,
                      marginTop: WP3,
                    }}
                  >
                    <Text size="xmini" color={SHIP_GREY_CALM} weight={400}>
                      {collaboration.postDuration}
                    </Text>
                  </View>
                </View>
                <View style={{ height: WP4 }} />
                <Text size="xmini" color={SHIP_GREY} weight={400}>
                  Notes:
                </Text>
                <View
                  style={{
                    backgroundColor: PALE_GREY,
                    paddingTop: WP3,
                    paddingBottom: WP3,
                    paddingHorizontal: WP4,
                    borderRadius: 5.5,
                    marginTop: WP3,
                  }}
                >
                  {readMoreAdditional ? (
                    <Text size="xmini" color={SHIP_GREY_CALM} weight={400}>
                      {collaboration?.specification}
                    </Text>
                  ) : (
                    <ReadMore
                      numberOfLines={3}
                      renderViewMore={this._renderViewMore(
                        "readMoreAdditional"
                      )}
                    >
                      <Text size="xmini" color={SHIP_GREY_CALM} weight={400}>
                        {collaboration?.specification}
                      </Text>
                    </ReadMore>
                  )}
                </View>
              </View>
            </Card>
            <Card
              style={{
                paddingHorizontal: 0,
                marginTop: 1,
                paddingBottom: WP6,
              }}
            >
              <Text
              selectable
                size="xmini"
                color={SHIP_GREY}
                weight={400}
                style={{ paddingHorizontal: WP5 }}
              >
                About this collaboration
              </Text>
              <View style={{ paddingHorizontal: WP5, marginTop: WP3 }}>
                {readMoreAbout ? (
                  <Text size="xmini" color={SHIP_GREY_CALM} weight={400}>
                    {collaboration?.description}
                  </Text>
                ) : (
                  <ReadMore
                    numberOfLines={5}
                    renderViewMore={this._renderViewMore("readMoreAbout")}
                  >
                    <Text size="xmini" color={SHIP_GREY_CALM} weight={400}>
                      {collaboration?.description}
                    </Text>
                  </ReadMore>
                )}
              </View>
              <View style={{ height: WP4 }} />
              {collaboration.collaborationReference !== undefined && (
                <View>
                  <Text
                    size="xmini"
                    color={SHIP_GREY}
                    weight={400}
                    style={{ paddingHorizontal: WP5 }}
                  >
                    Collaboration Reference
                  </Text>
                  <View style={{ paddingHorizontal: WP5, marginTop: WP3 }}>
                    {/* <LinkPreviewCard
                  text={collaboration?.collaboration_reference}
                  imageOnly
                  imageStyle={{
                    width: WP40,
                    height: WP30,
                    aspectRatio: 40 / 30,
                  }}
                  imageProps={{
                    centered: true,
                    resizeMode: "cover",
                  }}
                /> */}
                  </View>
                  <View style={{ height: WP4 }} />
                </View>
              )}
              {!isEmpty(collaboration?.members) && (
                <View>
                  <Text
                    size="xmini"
                    color={SHIP_GREY}
                    weight={400}
                    style={{ paddingHorizontal: WP5 }}
                  >
                    People who have joined
                  </Text>
                  {collabMember.map((item, index) => (
                    <View
                      key={index}
                      style={{ paddingHorizontal: WP5, marginTop: WP3 }}
                    >
                      <FollowItem
                        onPressItem={() => {}}
                        imageSize="small"
                        isMine={false}
                        key={`${index}${item}`}
                        keyExtractor={`${index}${item}`}
                        user={item}
                        onCollab={true}
                        style={{
                          marginHorizontal: 0,
                          paddingVertical: 0,
                        }}
                      />
                    </View>
                  ))}
                </View>
              )}
            </Card>
            {isNil(replyTo) && (
              <View>
                <View
                  style={{
                    position: "absolute",
                    backgroundColor: WHITE,
                    bottom: -37.5,
                    width: WP100 - WP10,
                    elevation: 5,
                    marginLeft: WP5,
                    borderRadius: 8,
                    zIndex: 999,
                  }}
                >
                  {map(mentionSuggestions, this._renderSuggestionsRow)}
                </View>
              </View>
            )}
            {!hasPreviewData && !isAccepted && (
              <Card
                onLayout={({ nativeEvent: { layout } }) => {
                  this.setState({ collabSuggesstionOffsetTop: layout.y });
                }}
                style={{
                  paddingHorizontal: 0,
                  marginTop: 1,
                  paddingBottom: WP6,
                }}
              >
                <Text
                  size="xmini"
                  color={SHIP_GREY}
                  weight={400}
                  style={{ paddingHorizontal: WP5, marginBottom: WP3 }}
                >
                  Komentar ({collaboration?.totalComment})
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    paddingHorizontal: WP5,
                    alignItems: "center",
                  }}
                >
                  <Avatar
                    size={"xsmall"}
                    shadow={false}
                    onPress={this._profileSreen(my.id_user)}
                    image={my.profile_picture}
                  />
                  <View
                    style={{ flex: 1, paddingLeft: WP3, paddingRight: WP2 }}
                  >
                    <MentionTextInput
                      key={mentionComponentKey}
                      above
                      placeholder=""
                      textInputStyle={{
                        fontSize: FONT_SIZE["mini"],
                        color: GUN_METAL,
                        backgroundColor: PALE_GREY,
                        paddingVertical: WP2,
                        paddingTop: WP2,
                        paddingHorizontal: WP3,
                        maxHeight: FONT_SIZE["mini"] * 5,
                        lineHeight: FONT_SIZE["mini"] * 1.28,
                        borderRadius: 5.5,
                        minHeight: FONT_SIZE["mini"] * 2.5,
                        justifyContent: "center",
                      }}
                      suggestionsPanelStyle={{
                        backgroundColor: "rgba(100,100,100,0.1)",
                      }}
                      onChangedSelected={(selected) => {
                        this.setState({ mentions: selected });
                      }}
                      trigger={"@"}
                      triggerLocation={"new-word-only"} // 'new-word-only', 'anywhere'
                      value={this.state.comments}
                      onChangeText={(val) => {
                        this.setState({ comments: val });
                      }}
                      onOpenSuggestionsPanel={() => {
                        this.setState({ openPanel: true });
                      }}
                      onCloseSuggestionsPanel={() => {
                        this.setState({
                          openPanel: false,
                          mentionSuggestions: [],
                        });
                      }}
                      triggerDelay={2}
                      triggerCallback={this._getMentionSuggestion}
                      _renderSuggestionsRow={this._renderSuggestionsRow}
                      suggestionsData={mentionSuggestions}
                      keyExtractor={(item, index) => {
                        if (item != null) {
                          return item.id;
                        }
                      }}
                      suggestionRowHeight={45}
                      horizontal={false}
                      MaxVisibleRowCount={5}
                      onFocus={() => {
                        setTimeout(() => {
                          this.scrollContainer.scrollTo({
                            x: 0,
                            y:
                              this.state.collabSuggesstionOffsetTop -
                              this.state.keyboardHeight,
                            animated: true,
                          });
                        }, 500);
                      }}
                    />
                  </View>
                  <ButtonV2
                    disabled={this.state.comments.length == 0}
                    color={REDDISH}
                    textColor={WHITE}
                    style={{ paddingHorizontal: WP5 }}
                    onPress={this._postCollabComment}
                    onLayout={({
                      nativeEvent: {
                        layout: { height: replyInputMinHeight },
                      },
                    }) => this.setState({ replyInputMinHeight })}
                    text={"Post"}
                  />
                </View>
                {collaboration?.totalComment != "0" && (
                  <View
                    style={{
                      height: 1.0,
                      marginVertical: WP5,
                      backgroundColor: PALE_GREY,
                      marginHorizontal: WP5,
                    }}
                  />
                )}
                {collaboration.totalComment > 0 && (
                  <View style={{ marginTop: WP3 }}>
                    {/*map(collaboration.comment, (comment, index) =>
                    this._renderComment(comment, index, comment.id_comment)
                  )*/}
                    <FlatList
                      data={collaboration.comment}
                      renderItem={({ item: comment }) =>
                        this._renderComment(comment, comment.id_comment)
                      }
                      keyExtractor={(item) => item.id_comment}
                      extraData={null}
                    />
                  </View>
                )}
              </Card>
            )}
            <SubmitModal
              modalVisible={backModal}
              submit={this._getCtaAction}
              dismiss={() => this.setState({ backModal: false })}
            />
          </View>
          {hasPreviewData && (
            <View
              style={{
                paddingHorizontal: WP5,
                backgroundColor: WHITE,
                flexDirection: "row",
                marginTop: WP3,
                marginBottom: WP6,
                justifyContent: "space-between",
              }}
            >
              <ButtonV2
                onPress={
                  hasPreviewData
                    ? () => this.setState({ backModal: true })
                    : () => {
                        this._getCtaAction();
                      }
                }
                color={REDDISH}
                textColor={WHITE}
                textSize={"small"}
                textWeight={500}
                text={this._getCtaText()}
                style={{ flex: 1, flexGrow: 1, paddingVertical: WP4 }}
              />
            </View>
          )}
          <ModalMessageView
            style={{}}
            subtitleStyle={{}}
            buttonSecondaryStyle={{}}
            toggleModal={this._cancelDeletePost}
            isVisible={isFunction(this.state.onDeletePost)}
            title={"Hapus Post"}
            titleType="Circular"
            titleSize={"small"}
            titleColor={GUN_METAL}
            subtitle={
              "Apakah kamu yakin untuk menghapus secara permanen post ini?"
            }
            subtitleType="Circular"
            subtitleSize={"xmini"}
            subtitleWeight={400}
            subtitleColor={SHIP_GREY_CALM}
            image={null}
            buttonPrimaryText={"Batalkan"}
            buttonPrimaryTextType="Circular"
            buttonPrimaryTextWeight={400}
            buttonPrimaryBgColor={REDDISH}
            buttonPrimaryAction={this._cancelDeletePost}
            buttonSecondaryText={"Hapus"}
            buttonSecondaryTextType="Circular"
            buttonSecondaryTextWeight={400}
            buttonSecondaryTextColor={SHIP_GREY_CALM}
            buttonSecondaryAction={this.state.onDeletePost}
          />
        </View>
        {/* </ScrollView> */}

        {isAccepted && (
          <View
            style={{
              bottom: 0,
              left: 0,
              right: 0,
              position: "absolute",
              width: "100%",
              height: WP20,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: WHITE,
              ...SHADOW_STYLE["shadow"],
            }}
          >
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onPress={() => this.setState({ takeModal: true })}
              style={{
                width: WP88,
                backgroundColor: REDDISH,
                borderRadius: 6,
                justifyContent: "center",
                alignItems: "center",
                marginVertical: WP4,
                paddingVertical: WP3,
                ...SHADOW_STYLE["shadowThin"],
              }}
            >
              <Text
                type="Circular"
                size="mini"
                weight={500}
                color={WHITE}
                centered
              >
                Take and Join Project
              </Text>
            </TouchableOpacity>
          </View>
        )}

        {this._renderConfirmationModal()}
      </Container>
    );
  }

  _renderViewMore = (stateField) => () => {
    return (
      <TouchableOpacity
        style={{ marginTop: WP3 }}
        onPress={() => this.setState({ [stateField]: true })}
      >
        <Text type={"Circular"} weight={300} size="mini" color={REDDISH}>
          Read More
        </Text>
      </TouchableOpacity>
    );
  };

  _getMentionSuggestion = async (full_name, triggerMatrix) => {
    if (!full_name) return;
    full_name = full_name.substring(1, full_name.length);
    const { dispatch } = this.props;

    const params = {
      start: 0,
      limit: 5,
      full_name,
    };

    const suggestions = await dispatch(
      getMentionSuggestions,
      params,
      noop,
      true,
      false
    );
    this.setState({ mentionSuggestions: suggestions.result || [] });
  };

  _renderSuggestionsRow(suggestion, index) {
    const { mentionSuggestions } = this.state;
    return (
      <TouchableOpacity
        key={`suggestion-${suggestion.id}`}
        activeOpacity={TOUCH_OPACITY}
        onPressIn={() => this._onSelectSuggestion(suggestion)}
      >
        <View
          style={[
            {
              paddingVertical: WP2,
              paddingHorizontal: WP4,
              flexDirection: "row",
              backgroundColor: WHITE,
              alignItems: "center",
              borderTopWidth: 1,
              borderTopColor: PALE_GREY,
            },
            index == 0
              ? {
                  borderTopLeftRadius: 8,
                  borderTopRightRadius: 8,
                }
              : {},
            mentionSuggestions.length - 1 == index
              ? {
                  borderBottomLeftRadius: 8,
                  borderBottomRightRadius: 8,
                }
              : {},
          ]}
        >
          <Avatar size={"mini"} image={suggestion.avatar} />
          <Text type={"Circular"} size={"xmini"} style={{ marginLeft: WP2 }}>
            {suggestion.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  _onSelectSuggestion = (suggestion) => {
    // hidePanel()
    let { comments, mentions, replyMentions, replyTo, reply } = this.state;
    if (!isNil(replyTo)) comments = reply;
    isNil(replyTo) && mentions.push(suggestion);
    !isNil(replyTo) && replyMentions.push(suggestion);
    this.setState({ mentionSuggestions: [] });
    const formattedComment = `${
      comments.substring(0, comments.lastIndexOf("@")) + suggestion.name.trim()
    } `;
    this.setState({
      [isNil(replyTo) ? "comments" : "reply"]: formattedComment,
      mentions,
      replyMentions,
      mentionComponentKey: Math.random(),
    });
    // updateText(formattedComment, suggestion)
  };

  _renderComment = (comment, id_parent) => {
    const { dispatch } = this.props;
    const isParent = isObject(comment.reply_comment);
    const liked = comment.total_like != "0";
    const myComment = comment.id_user == this.props.userData.id_user;
    return (
      <View
        key={`comment-${comment.id_comment}`}
        style={{
          flexDirection: "row",
          paddingHorizontal: isParent ? WP5 : 0,
          [isParent ? "marginBottom" : "marginTop"]: WP3,
        }}
      >
        <Avatar
          size={isParent ? "xsmall" : "mini"}
          shadow={false}
          onPress={this._profileSreen(comment.id_user)}
          image={comment.profile_picture}
        />
        <View style={{ flex: 1 }}>
          <View style={{ paddingLeft: WP3 }}>
            <View
              style={{
                paddingHorizontal: WP3,
                paddingTop: WP2,
                paddingBottom: WP3,
                borderRadius: 5.5,
                backgroundColor: PALE_GREY,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginBottom: 1.5,
                }}
              >
                <Text>
                  <Text
                    onPress={this._profileSreen(comment.id_user)}
                    size="xmini"
                    type={"Circular"}
                    color={GUN_METAL}
                    weight={500}
                  >
                    {comment?.full_name}
                  </Text>
                  <Text
                    type={"Circular"}
                    size="xmini"
                    color={SHIP_GREY_CALM}
                    weight={300}
                  >
                    {` · ${moment(comment.created_at)
                      .locale("id")
                      .fromNow(true)
                      .replace("beberapa detik", "baru saja")}`}
                  </Text>
                </Text>
                <View style={{ flex: 1 }} />
                {comment && (
                  <MenuOptions
                    options={[
                     
                      {
                        onPress: () => {
                          Clipboard.setString(comment.comment);
                          alert('Copied');
                        },
                        iconName: "clipboard",
                        iconColor: SHIP_GREY_CALM,
                        iconSize: "huge",
                        title: "Copy",
                      },
                    ]} 
                    customHeader={<View />}
                    triggerComponent={(toggleModal) => (
                      <Icon
                        onPress={toggleModal}
                        background="dark-circle"
                        size="mini"
                        color={SHIP_GREY_CALM}
                        name="dots-three-horizontal"
                        type="Entypo"
                      />
                    )}
                  />
                )  }
              </View>
              {this._parseComment(comment?.comment)}
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginVertical: WP1 + (isParent ? 3 : 0),
              }}
            >
              <Text
                onPress={() => {
                  this.props
                    .dispatch(
                      postLikeAdsComment,
                      {
                        related_to: "id_comment",
                        id_related_to: comment.id_comment,
                        id_user: this.props.userData.id_user,
                      },
                      noop,
                      false,
                      false
                    )
                    .then(() => this._getCollaborationDetail(false));
                }}
                size="xmini"
                type={"Circular"}
                color={
                  isNil(comment.is_liked) ? SHIP_GREY_CALM : "rgb(47, 128, 237)"
                }
                weight={400}
              >
                Like
              </Text>
              {liked && (
                <Text
                  size="xmini"
                  type={"Circular"}
                  color={SHIP_GREY_CALM}
                  weight={400}
                >
                  {"  ·  "}
                </Text>
              )}
              {liked && (
                <View style={{ paddingTop: 0.5 }}>
                  <Icon
                    color="rgb(47, 128, 237)"
                    size="xmini"
                    name={"thumb-up"}
                    type={"MaterialCommunityIcons"}
                  />
                </View>
              )}
              {liked && (
                <Text
                  style={{ color: "rgb(47, 128, 237)" }}
                  size="xmini"
                  type={"Circular"}
                  color={SHIP_GREY_CALM}
                  weight={400}
                >
                  {`  ${comment.total_like}`}
                </Text>
              )}
              <Text
                size="xmini"
                type={"Circular"}
                color={SHIP_GREY_CALM}
                weight={400}
              >
                {"   │   "}
              </Text>
              <Text
                onPress={() =>
                  this.setState(
                    {
                      reply: `${comment.full_name} `,
                      replyTo: {
                        id: comment.id_user,
                        name: comment.full_name,
                      },
                      id_parent,
                    },
                    () =>
                      this.setState({
                        replyMentions: [this.state.replyTo],
                      })
                  )
                }
                size="xmini"
                type={"Circular"}
                color={SHIP_GREY_CALM}
                weight={400}
              >
                {"Balas"}
                {!isEmpty(comment.reply_comment)
                  ? `   ·   ${comment.reply_comment.length} balasan`
                  : ""}
              </Text>
            </View>
            {isParent && (
              <FlatList
                data={comment.reply_comment}
                renderItem={({ item: reply }) =>
                  this._renderComment(reply, comment.id_comment)
                }
                keyExtractor={(item) => item.id_comment}
                extraData={null}
              />
            )}
          </View>
        </View>
      </View>
    );
  };

  _parseComment = (inputString) => {
    const textHtml = linkify(inputString);
    return (
      <HTMLElement
        textSelectable
        containerStyle={{ flexDirection: "row" }}
        html={textHtml}
        customWrapper={(children) => (
          <Text
          selectable
            weight={400}
            color={SHIP_GREY_CALM}
            type={"Circular"}
            size="xmini"
          >
            {children}
          </Text>
        )}
        baseFontStyle={{ fontFamily: "OpenSansRegular" }}
        ignoredTags={[...IGNORED_TAGS]}
        renderers={{
          mention: (attbs, children, convertedCSSStyles, passProps) => {
            return (
              <Text
                type={"Circular"}
                weight={500}
                onPress={this._profileSreen(attbs.id)}
                style={{ color: "rgb(47, 128, 237)" }}
                key={Math.random()}
                size="xmini"
              >
                {children}{" "}
              </Text>
            );
          },
          a: (attbs, children, convertedCSSStyles, passProps) => {
            return (
              <Text
                type={"Circular"}
                weight={400}
                onPress={() =>
                  this.props.navigateTo("BrowserScreenNoTab", {
                    url: attbs.href.replace(/\s/gim, ""),
                  })
                }
                style={{
                  color: "rgb(47, 128, 237)",
                  textDecorationLine: "none",
                  fontStyle: "normal",
                }}
                size="xmini"
              >
                {children}{" "}
              </Text>
            );
          },
        }}
      />
    );
  };

  _postCollabComment = () => {
    const { mentions, comments } = this.state;
    const {
      id,
      userData: { id_user },
    } = this.props;
    let comment = comments;
    mentions.forEach((element) => {
      comment = comment.replace(
        element.name,
        `<mention id=${element.id}>${element.name}</mention>`
      );
    });
    comment = comment.trim();
    this.setState(
      {
        mentions: [],
        comments: "",
        mentionComponentKey: Math.random(),
      },
      () => {
        this.props
          .dispatch(
            postAdsComment,
            {
              related_to: "id_ads",
              id_related_to: id,
              id_user,
              comment,
            },
            noop,
            false,
            false
          )
          .then(() => {
            this._getCollaborationDetail(false, true);
          });
      }
    );
  };

  _postReplyComment = () => {
    const { replyMentions, reply, id_parent } = this.state;
    const {
      userData: { id_user },
    } = this.props;
    let comment = reply;
    replyMentions.forEach((element) => {
      comment = comment.replace(
        element.name,
        `<mention id=${element.id}>${element.name}</mention>`
      );
    });
    comment = comment.trim();
    this.setState(
      {
        replyMentions: [],
        reply: "",
        replyTo: null,
        mentionComponentKey: Math.random(),
      },
      () => {
        this.props
          .dispatch(postAdsComment, {
            related_to: "id_comment",
            id_related_to: id_parent,
            id_user,
            comment,
          })
          .then(() => {
            this._getCollaborationDetail(false, false, 100);
          });
      }
    );
  };
}

CollaborationPreview.defaultProps = propsDefault;
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CollaborationPreview),
  mapFromNavigationParam
);
