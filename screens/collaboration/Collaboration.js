import React from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import { _enhancedNavigation, Container, Text, Image, Button, HeaderNormal } from '../../components'
import { authDispatcher } from '../../services/auth'
import { GET_USER_DETAIL } from '../../services/auth/actionTypes'
import { WHITE, BLACK, ORANGE, GREY_WARM } from '../../constants/Colors'
import { WP6, HP1, HP2, WP70, WP4 } from '../../constants/Sizes'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setUserDetail: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class Collaboration extends React.Component {
  state = {
    isReady: true,
    isRefreshing: false
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  render() {
    const { navigateTo } = this.props
    return (
      <Container
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={this._backHandler}
            text='Create Collaboration Project'
            centered
          />
        )}
      >
        <Image
          source={require('../../assets/images/collabsIllustration.png')}
          style={{ flexGrow: .5 }}
          imageStyle={{ width: WP70 }}
          aspectRatio={308 / 188}
        />
        <View style={{ paddingHorizontal: WP6, flexGrow: .5 }}>
          <Text color={BLACK} weight={500} type='NeoSans' size='extraMassive' style={{ marginVertical: HP2 }}>
            Hi There!
          </Text>
          <View style={{ marginVertical: HP1 }}>
            <Text color={GREY_WARM} size='xmini' style={{ marginBottom: HP2 }}>
              Welcome to our Collaboration Project! We’re excited to help you to find fellow artists to collaborate and create music together.
            </Text>
            <Text color={GREY_WARM} size='xmini'>
              So what are you waiting for? Click the button below to start your journey with us!
            </Text>
          </View>
        </View>
        <View style={{
          paddingHorizontal: WP4,
          backgroundColor: WHITE,
        }}
        >
          <Button
            onPress={() => navigateTo('CollaborationForm')}
            backgroundColor={ORANGE}
            centered
            bottomButton
            radius={WP4}
            shadow='none'
            textType='NeoSans'
            textSize='small'
            textColor={WHITE}
            textWeight={500}
            text='Let’s get Started'
          />
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(Collaboration),
  mapFromNavigationParam
)
