import React, { Component } from "react";
import {
  BackHandler,
  CheckBox,
  FlatList,
  ScrollView,
  RefreshControl,
  ScrollView as RNScrollView,
  StyleSheet,
  View,
} from "react-native";
import { get, isEmpty, isFunction, noop } from "lodash-es";
import { connect } from "react-redux";
import Container from "sf-components/Container";
import _enhancedNavigation from "sf-navigation/_enhancedNavigation";
import NotificationItem from "sf-components/notification/NotificationItem";
import { setNotificationFlagCache } from "sf-utils/storage";
import {
  getNotification,
  getVoucherUser,
  postReadNotification,
} from "sf-actions/api";
import {
  GUN_METAL,
  NAVY_DARK,
  PALE_BLUE,
  PALE_WHITE,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from "sf-constants/Colors";
import EmptyV3 from "sf-components/EmptyV3";
import { GET_NOTIFICATION } from "sf-services/notification/actionTypes";
import NavigationService from "sf-navigation/_serviceNavigation";
import { pusherNotificationListener } from "sf-utils/clientPusher";
import { Text } from "sf-components";
import {
  WP10,
  WP105,
  WP2,
  WP20,
  WP205,
  WP3,
  WP4,
  WP40,
  WP5,
  WP6,
  WP70,
  WP8,
} from "sf-constants/Sizes";
import produce from "immer";
import Image from "sf-components/Image";
import Divider from "sf-components/Divider";
import Icon from "sf-components/Icon";
import Spacer from "sf-components/Spacer";
import ButtonV2 from "sf-components/ButtonV2";
import Touchable from "sf-components/Touchable";
import CustomTabController from "sf-components/CustomTabController";
import MenuOptions from "sf-components/MenuOptions";

import MessageBarClearBlue from "sf-components/messagebar/MessageBarClearBlue";

import { emptyClearBlueMessage } from "sf-services/messagebar/actionDispatcher";
import {
  setActivityNotifications,
  setNotificationFlag,
  setUpdateNotifications,
} from "sf-services/notification/actionDispatcher";
import HeaderNormal from "sf-components/HeaderNormal";
import { showReviewIfNotYet } from "../utils/review";
import { getNotifMappingAction } from "../components/notification/NotifAction";

const mapStateToProps = ({
  auth,
  notification: {
    pusherNotification,
    updates_notifications,
    activities_notifications,
  },
  message,
}) => ({
  userData: auth.user,
  newMessageCount: message.newMessageCount,
  pusherNotification,
  updates_notifications,
  activities_notifications,
});

const mapDispatchToProps = {
  clearPusher: () => ({ type: `${GET_NOTIFICATION}_CLEAR_PUSHER` }),
  clearPusherFlag: () => ({ type: `${GET_NOTIFICATION}_CLEAR_PUSHER_FLAG` }),
  emptyClearBlueMessage,
  setActivityNotifications,
  setUpdateNotifications,
  setNotificationFlag,
};

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam("initialLoaded", false),
  review: getParam("review", false),
  defaultTab: getParam("defaultTab", 0),
});

const STYLE = StyleSheet.create({
  content: {
    backgroundColor: PALE_WHITE,
    flex: 1,
  },
  wrapper: {
    flex: 1,
  },
  header: {
    paddingLeft: WP5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  voucher: {
    paddingVertical: WP3,
    paddingHorizontal: WP5,
    justifyContent: "space-between",
  },
  voucherIcon: {
    marginRight: WP105,
  },
  item: {
    paddingHorizontal: WP4,
  },
});

const tabIndex = {
  updates: 0,
  activities: 1,
};

class NotificationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
      category: "All",
      sort_by: "new",
      updates_notifications: [],
      activities_notifications: [],
      voucherIcon: require("sf-assets/icons/v3/voucherDoesntExists.png"),
      index: tabIndex[this.props.defaultTab] || 0,
      vouchers: [],
      routes: [
        { key: "updates", title: "UPDATES" },
        { key: "activities", title: "ACTIVITIES" },
      ],
      refreshing: false,
    };
  }

  // eslint-disable-next-line react/sort-comp
  _focusListenerCupdatesback() {
    const { navigateTo, userData, navigateBack } = this.props;
    if (!userData.id_user) {
      navigateBack();
      this.props.navigation.navigate("AuthNavigator", {
        // screen: 'LoginScreen',
        screen: "LoginEmailScreen",
        params: {
          forceReload: true,
        },
      });
    } else {
      const { updates_notifications } = this.state;
      updates_notifications.length > 0 &&
        this.setState(
          produce((draft) => {
            draft.refreshing = true;
          })
        );
      this.props.setNotificationFlag(false);
      setNotificationFlagCache(false);
      this._getNotifications();
      this._getVouchers();
      setTimeout(this.props.emptyClearBlueMessage, 6000);
      this.backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        () => {
          navigateTo("ExploreScreen");
          return true;
        }
      );
    }
  }

  releaseBackAction() {
    typeof this.backHandler !== "undefined" && this.backHandler.remove();
  }

  componentWillUnmount() {
    this.focusListener();
    this.blurListener();
    typeof this.backHandler === "object" &&
      typeof this.backHandler.remove === "function" &&
      this.backHandler.remove();
  }

  async _getNotifications() {
    try {
      const updates_notifications = await this._fetchAllNotifications();
      const activities_notifications = await this._fetchProjectNotifications();
      this.props.setUpdateNotifications(updates_notifications);
      this.props.setActivityNotifications(activities_notifications);

      this.setState(
        produce((draft) => {
          // draft.updates_notifications = updates_notifications
          // draft.activities_notifications = activities_notifications
          draft.isReady = true;
          draft.refreshing = false;
        })
      );
    } catch (e) {
      this._getNotification();
    }
  }

  // eslint-disable-next-line react/sort-comp
  async componentDidMount() {
    const { userData, navigateTo, navigateBack } = this.props;
    this.focusListener = this.props.navigation.addListener(
      "focus",
      this._focusListenerCupdatesback.bind(this)
    );
    this.blurListener = this.props.navigation.addListener(
      "blur",
      this.releaseBackAction.bind(this)
    );
    const { review } = this.props;

    __DEV__ && this._getNotifications();
    __DEV__ && this._getVouchers();

    pusherNotificationListener(userData.id_user, this._appendNewNotification);

    if (review) setTimeout(() => showReviewIfNotYet(), 1000);
  }

  static getDerivedStateFromProps(props, state) {
    const { clearPusher, pusherNotification } = props;
    const { routes } = state;
    const { updates_notifications, activities_notifications } = props;
    let notifications = {
      updates_notifications,
      routes,
      activities_notifications,
    };
    if (!isEmpty(pusherNotification)) {
      try {
        notifications = produce((draft) => {
          draft.updates_notifications = produce(
            pusherNotification,
            (_draft) => {
              updates_notifications.map((notification) => {
                _draft.push(notification);
              });
            }
          );
        });
        clearPusher();
      } catch (e) {
        // keep silent beib
      }
    }
    return produce(notifications, (draft) => {
      try {
        let [unread_tab_0, unread_tab_1] = ([0, 0][
          (unread_tab_0, unread_tab_1)
        ] = [
          draft.updates_notifications.filter(
            (notif) => notif.status === "unread"
          ).length,
          draft.activities_notifications.filter(
            (notif) => notif.status === "unread"
          ).length,
        ]);
        draft.routes[0].title =
          unread_tab_0 > 0 ? `UPDATES (${unread_tab_0})` : "UPDATES";
        draft.routes[1].title =
          unread_tab_1 > 0 ? `ACTIVITIES (${unread_tab_1})` : "ACTIVITIES";
      } catch {
        // keep silent
      }
    });
  }

  async _fetchAllNotifications() {
    const { dispatch, userData, isLoading } = this.props;

    try {
      const response = await dispatch(
        getNotification,
        { id_user: userData.id_user, type: "Other" },
        noop,
        true,
        isLoading
      );
      return this._mapNotification(response.result);
    } catch (e) {
      return [];
    }
  }

  async _fetchProjectNotifications() {
    const { dispatch, userData, isLoading } = this.props;
    const { category, sort_by } = this.state;
    try {
      const response = await dispatch(
        getNotification,
        { id_user: userData.id_user, type: "Project", category, sort_by },
        noop,
        true,
        isLoading
      );
      return this._mapNotification(response.result);
    } catch (e) {
      return [];
    }
  }

  _getVouchers = async () => {
    const { dispatch, userData } = this.props;

    try {
      const { result, code } = await dispatch(
        getVoucherUser,
        { id_user: userData.id_user },
        noop,
        true,
        true
      );
      if (code == 200) {
        this.setState(
          produce((draft) => {
            draft.vouchers = result;
            if (result.length > 0) {
              draft.voucherIcon = require("sf-assets/icons/v3/voucherExists.png");
            }
          })
        );
      }
    } catch (error) {}
  };

  _mapNotification = (data) => {
    return data.map((notification) => ({
      id_notif: notification.id_notif,
      profilePicture: notification.profile_picture,
      username: notification["full_name"],
      content: notification.content || notification.content_mobile,
      timestamp: notification["created_at"],
      action: notification["url_mobile"],
      idRelatedTo: notification.id_related_to,
      relatedTo: notification.related_to,
      status: notification.status,
      from: notification.notif_from,
      url: notification.url,
      ads: notification.data_ads,
      push_to_platform: notification.push_to_platform,
      additional: JSON.parse(get(notification, "additional_data") || "{}"),
    }));
  };

  _appendNewNotification = async (newNotification) => {
    const notification = {
      id_notif: newNotification.id_notif,
      profilePicture: newNotification.profile_picture,
      username: newNotification["full_name"],
      content: newNotification.content,
      timestamp: new Date(),
      action: newNotification["url_mobile"],
      idRelatedTo: newNotification.id_related_to,
      relatedTo: newNotification.related_to,
      status: "unread",
      from: newNotification.notif_from,
      url: newNotification.url,
      ads: newNotification.data_ads,
      push_to_platform: notification.push_to_platform,
      additional: JSON.parse(get(newNotification, "additional_data") || "{}"),
    };
    this.setState(
      produce((draft) => {
        const newNotifications = [...draft.updates_notifications];
        newNotifications.unshift(notification);
        draft.updates_notifications = newNotifications;
      })
    );
  };

  _readNotification = (notification) => {
    const { id_notif } = notification;
    postReadNotification({
      id_notif,
    });
    this.props.clearPusherFlag();
  };

  _keyExtractor = ({ id_notif }) => id_notif;

  _renderItem = ({ item: notification }, index) => {
    const { navigateTo } = this.props;
    return (
      <Touchable
        style={STYLE.item}
        onPress={async () => {
          this._readNotification(notification);
          let { url, action } = notification;
          if (notification.action == "notification_activity") {
            this._onChangeTab(1);
          } else {
            const notifAction = await getNotifMappingAction({
              url_mobile: action,
              url: url || action,
            });
            if (!notifAction) return null;

            if (isFunction(notifAction.direct)) {
              notifAction.direct(notification, NavigationService);
            } else {
              navigateTo(notifAction.to, notifAction.payload);
            }
          }
        }}
      >
        <NotificationItem
          notification={notification}
          isActivity={this.state.index == 1}
          imageSrc={notification.profilePicture}
          username={notification.username}
          message={notification.content || notification.content_mobile}
          actions={<Spacer size={WP10} />}
          status={notification.status}
          timestamp={notification.timestamp}
          action={notification["url_mobile"]}
        />
      </Touchable>
    );
  };

  _sortOptions = () => {
    return [
      {
        title: "Paling baru",
        value: "new",
        description: "Urutkan berdasarkan waktu terbaru",
        onPress: () =>
          this.setState({ sort_by: "new" }, () =>
            Promise.all([this.toggleSortModal(), this._getNotifications()])
          ),
        icon: require("sf-assets/icons/v3/arrowUpCircle.png"),
      },
      null,
      {
        title: "Paling lama",
        value: "old",
        description: "Urutkan berdasarkan waktu terlama",
        onPress: () =>
          this.setState({ sort_by: "old" }, () =>
            Promise.all([this.toggleSortModal(), this._getNotifications()])
          ),
        icon: require("sf-assets/icons/v3/arrowUpCircle.png"),
        iconStyle: { transform: [{ rotate: "180deg" }] },
      },
    ].map((item) => {
      if (!item)
        return (
          <View style={{ paddingHorizontal: WP4 }}>
            <Divider size={0.75} />
          </View>
        );
      const { title, description, icon, iconStyle, onPress, value } = item;
      return (
        <Touchable key={title} onPress={onPress}>
          <View
            style={{ flexDirection: "row", padding: WP4, alignItems: "center" }}
          >
            <View>
              <Image imageStyle={iconStyle} source={icon} />
            </View>
            <View style={{ paddingLeft: WP4, paddingRight: WP2, flex: 1 }}>
              <Text type={"Circular"} size={"small"} color={GUN_METAL}>
                {title}
              </Text>
              <Spacer size={WP105} />
              <Text
                numberOfLines={1}
                type={"Circular"}
                size={"xmini"}
                color={SHIP_GREY_CALM}
              >
                {description}
              </Text>
            </View>
            <View>
              <CheckBox
                tintColors={{ true: REDDISH, false: SHIP_GREY_CALM }}
                value={value === this.state.sort_by}
                onValueChange={onPress}
              />
            </View>
          </View>
        </Touchable>
      );
    });
  };

  _listHeaderComponent = () => {
    const { index, category, sort_by } = this.state;
    const newFirst = sort_by == "new";
    return index == 1 ? (
      <RNScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{ paddingTop: WP5, paddingBottom: WP2 }}
      >
        <Spacer horizontal size={WP4} />
        <MenuOptions
          options={this._sortOptions()}
          triggerComponent={(toggleModal) => {
            this.toggleSortModal = toggleModal;
            return (
              <ButtonV2
                onPress={toggleModal}
                color={NAVY_DARK}
                forceBorderColor
                borderColor={NAVY_DARK}
                textColor={WHITE}
                rightComponent={
                  <View style={{ marginLeft: 3 }}>
                    <Icon type={"Entypo"} color={WHITE} name={"chevron-down"} />
                  </View>
                }
                style={{
                  paddingVertical: 0,
                  height: WP8,
                  paddingLeft: WP205,
                  paddingRight: WP2,
                }}
                text={newFirst ? "Paling baru" : "Paling lama"}
              />
            );
          }}
        />
        {["All", "Joined", "Created"].map((item) => {
          const isSelected = category == item;
          return (
            <View key={item} style={{ flexDirection: "row" }}>
              <Spacer horizontal size={WP2} />
              <ButtonV2
                onPress={() => {
                  this.setState({ category: item }, this._getNotifications);
                }}
                color={isSelected ? NAVY_DARK : WHITE}
                textColor={isSelected ? WHITE : SHIP_GREY}
                borderColor={isSelected ? NAVY_DARK : PALE_BLUE}
                forceBorderColor
                style={{
                  height: WP8,
                  paddingHorizontal: WP205,
                  paddingVertical: 0,
                  paddingTop: 0,
                  paddingBottom: 0,
                }}
                text={item == "All" ? "Semua" : item}
              />
            </View>
          );
        })}
      </RNScrollView>
    ) : (
      <View />
    );
  };

  _renderNotificationList = (notifications) => {
    return (
      <View>
        <FlatList
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          data={notifications}
          ListFooterComponent={<Spacer size={WP40} />}
        />
      </View>
    );
  };

  _viewVouchers = () => {
    this.props.navigateTo("VoucherListScreen");
  };

  _renderHeader = () => {
    const { voucherIcon } = this.state;
    const { navigateBack } = this.props;

    return (
      <View style={STYLE.header}>
        <Text color={SHIP_GREY} weight={400} type={"Circular"}>
          Notification
        </Text>
        <ButtonV2
          onPress={this._viewVouchers}
          style={STYLE.voucher}
          leftComponent={
            <Image style={STYLE.voucherIcon} size={WP6} source={voucherIcon} />
          }
          borderColor={"transparent"}
          text={"My Voucher"}
          textSize={"xmini"}
          textColor={SHIP_GREY}
        />
      </View>
    );
  };

  _onRefresh = () => {
    this.setState(
      produce((draft) => {
        draft.refreshing = true;
      }),
      this._getNotifications
    );
  };

  _renderScene = ({ route }) => {
    const { index } = this.state;
    let { updates_notifications, activities_notifications } = this.props;

    switch (route.key) {
      case "updates":
        return (
          <View
            style={[
              { display: index == 0 ? "flex" : "none" },
              isEmpty(updates_notifications) ? { alignSelf: "center" } : null,
            ]}
          >
            {isEmpty(updates_notifications) ? (
              <EmptyV3
                imageWidth={WP20}
                aspectRatio={1 / 1}
                image={require("sf-assets/icons/v3/emptyBell.png")}
                title="Belum ada pemberitahuan"
                message="Belum ada update & penawaran saat ini"
                actions={<Spacer size={WP40} />}
              />
            ) : (
              <ScrollView
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
                showsVerticalScrollIndicator={false}
                style={{ backgroundColor: "PALE_WHITE" }}
              >
                {!isEmpty(updates_notifications) &&
                  this._renderNotificationList(updates_notifications)}
              </ScrollView>
            )}
          </View>
        );
      case "activities":
        return (
          <View
            style={[
              {
                display: index == 1 ? "flex" : "none",
                backgroundColor: PALE_WHITE,
              },
              isEmpty(activities_notifications)
                ? { alignSelf: "center" }
                : null,
            ]}
          >
            {(activities_notifications.length > 0 ||
              this.state.category != "All") &&
              activities_notifications.length < 3 &&
              this._listHeaderComponent()}
            {isEmpty(activities_notifications) ? (
              <EmptyV3
                imageWidth={WP20}
                aspectRatio={1 / 1}
                image={require("sf-assets/icons/v3/emptyActivity.png")}
                title="Belum ada aktivitas"
                message={
                  "Temukan berbagai aktivitas yang dapat kamu ikuti di Eventeer"
                }
                actions={
                  <Spacer size={this.state.category != "All" ? WP70 : WP40} />
                }
              />
            ) : (
              <ScrollView
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
                showsVerticalScrollIndicator={false}
                style={{ backgroundColor: PALE_WHITE }}
              >
                {(activities_notifications.length > 0 ||
                  this.state.category != "All") &&
                  activities_notifications.length >= 3 &&
                  this._listHeaderComponent()}
                {this._renderNotificationList(activities_notifications)}
              </ScrollView>
            )}
          </View>
        );
    }
  };

  render() {
    const {
      isLoading,
      updates_notifications,
      activities_notifications,
      userData,
    } = this.props;

    const { isReady, index, routes } = this.state;

    return (
      <Container
        hasBottomNavbar
        scrollable={false}
        isLoading={
          isLoading &&
          updates_notifications.length == 0 &&
          activities_notifications.length == 0
        }
        isReady={
          isReady ||
          updates_notifications.length > 0 ||
          activities_notifications.length > 0
        }
        type="horizontal"
        backgroundColor={PALE_WHITE}
        renderHeader={this._renderHeader}
      >
        <View>
          {userData.id_user && (
            <CustomTabController
              onPress={this._onChangeTab}
              activeIndex={index}
              routes={routes}
            />
          )}
          <MessageBarClearBlue />
        </View>
        {userData.id_user && this._renderScene({ route: routes[index] })}
      </Container>
    );
  }

  _onChangeTab = (index) => {
    this.setState(
      produce((draft) => {
        draft.index = index;
        draft.isTabReady = false;
      })
    );
  };
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(NotificationScreen),
  mapFromNavigationParam
);
