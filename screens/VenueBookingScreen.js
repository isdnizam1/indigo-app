import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, View, TouchableOpacity, Keyboard, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import moment from 'moment'
import { isEmpty } from 'lodash-es'
import { _enhancedNavigation, Container, Header, Icon, Text, Form, InputTextLight } from '../components'

import { GREY05, ORANGE, ORANGE_BRIGHT, PINK_RED, WHITE } from '../constants/Colors'
import { HP2, HP4, WP100, WP105, WP6 } from '../constants/Sizes'
import InputDateTime from '../components/InputDateTime'
import { postBooking } from '../actions/api'
import { TOUCH_OPACITY } from '../constants/Styles'

const STYLE = StyleSheet.create({
  sectionHeader: {
    marginTop: HP4,
  },
  bookButton: {
    width: WP100,
    backgroundColor: ORANGE,
    paddingVertical: HP2,
  }
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  venue: getParam('venue', {})
})

class VenueBookingScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: true,
      invalidField: {}
    }
  }

  _validInputStyle = (inputKey) => {
    const {
      invalidField
    } = this.state

    if (invalidField[inputKey]) return { borderBottomColor: ORANGE, borderBottomWidth: 2 }
    return {}
  }

  _validate = (bookingRequest) => {
    const invalidField = {}
    if (isEmpty(bookingRequest.phone)) invalidField.phone = true
    if (isEmpty(bookingRequest.event_name)) invalidField.event_name = true
    if (isEmpty(bookingRequest.event_description)) invalidField.event_description = true
    if (isEmpty(bookingRequest.visitor_estimation)) invalidField.visitor_estimation = true
    if (!bookingRequest.start_date) invalidField.start_date = true
    if (!bookingRequest.end_date) invalidField.end_date = true

    this.setState({ invalidField })
    return Object.keys(invalidField).length === 0
  }

  _onSubmit = async ({ formData, isDirty }) => {
    const {
      userData,
      dispatch,
      navigateTo
    } = this.props

    const bookingRequest = { ...formData }
    if(!this._validate(bookingRequest)) return

    bookingRequest.id_user = userData.id_user
    const dateFormat = 'YYYY-MM-DD HH:mm:ss'
    bookingRequest.start_date = moment(formData.start_date).format(dateFormat)
    bookingRequest.end_date = moment(formData.end_date).format(dateFormat)

    await this.setState({ isReady: false })
    try {
      const bookingResult = await dispatch(postBooking, bookingRequest, () => {}, true)
      const status = bookingResult.code === 200 ? 'SUCCESS' : 'FAILED'
      await this.setState({ isReady: true })
      navigateTo('VenueBookingResultScreen', { status })
    } catch (e) {
      await this.setState({ isReady: true })
    }

  }

  render() {
    const {
      userData,
      isLoading,
      navigateBack,
      venue
    } = this.props

    const {
      isReady
    } = this.state

    return (
      <Container
        isLoading={isLoading}
        isReady={isReady}
        colors={[WHITE, WHITE]}
        type='horizontal'
        renderHeader={() => (
          <Header
            colors={[PINK_RED, ORANGE_BRIGHT]}
            style={{ justifyContent: 'flex-start' }}
          >
            <Icon onPress={() => { navigateBack() }} style={{ alignSelf: 'center', fontWeight: 600, marginRight: WP105 }} size='large' color={WHITE} name='left'/>
            <Text weight={500} size='medium' color={WHITE}>Venue Booking</Text>
          </Header>
        )}
      >
        <Form
          initialValue={{
            id_ads: venue.id_ads,
            full_name: userData.full_name,
            email: userData.email,
          }}
          onSubmit={this._onSubmit}
        >
          {({ onChange, onSubmit, formData, isDirty }) => (
            <View style={{ flex: 1 }}>
              <ScrollView
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps='handled'
              >
                <View style={{ width: WP100, flex: 1 }}>
                  <View style={{ flex: 1, marginVertical: HP2, marginHorizontal: WP6 }}>
                    <Text size='slight' weight={400}>Venue Information</Text>
                    <InputTextLight
                      disabled
                      editable={false}
                      value={venue.title}
                      label='Venue Name'
                      backgroundColor={GREY05}
                      onChangeText={() => {}}
                    />

                    <Text size='slight' weight={400} style={STYLE.sectionHeader}>Personal Data</Text>
                    <InputTextLight
                      disabled
                      editable={false}
                      value={formData.full_name}
                      label='Your Name'
                      backgroundColor={GREY05}
                      onChangeText={onChange('full_name')}
                    />
                    <InputTextLight
                      disabled
                      editable={false}
                      value={formData.email}
                      label='Email'
                      backgroundColor={GREY05}
                      onChangeText={onChange('email')}
                    />
                    <InputTextLight
                      value={formData.phone}
                      label='Phone Number'
                      textInputStyle={this._validInputStyle('phone')}
                      keyboardType='phone-pad'
                      onChangeText={onChange('phone')}
                    />

                    <Text size='slight' weight={400} style={STYLE.sectionHeader}>Event Information</Text>
                    <InputTextLight
                      value={formData.event_name}
                      label='Event Name'
                      textInputStyle={this._validInputStyle('event_name')}
                      onChangeText={onChange('event_name')}
                    />
                    <InputTextLight
                      value={formData.event_description}
                      label='Event Description'
                      multiline={true}
                      size='slight'
                      textInputStyle={this._validInputStyle('event_description')}
                      onChangeText={onChange('event_description')}
                    />
                    <InputTextLight
                      value={formData.visitor_estimation}
                      label='Estimated Visitor'
                      keyboardType='number-pad'
                      textInputStyle={this._validInputStyle('visitor_estimation')}
                      onChangeText={onChange('visitor_estimation')}
                    />

                    <Text size='slight' weight={400} style={STYLE.sectionHeader}>Booking Information</Text>

                    <InputDateTime
                      mode='datetime'
                      onChange={onChange('start_date')}
                      minimumDate={new Date()}
                    >
                      <InputTextLight
                        editable={false}
                        value={formData.start_date ? moment(formData.start_date).format('DD MMMM YYYY HH.mm') : ''}
                        label='Start Time'
                        textInputStyle={this._validInputStyle('start_date')}
                        onChangeText={onChange('start_date')}
                      />
                    </InputDateTime>

                    <InputDateTime
                      mode='datetime'
                      onChange={onChange('end_date')}
                      minimumDate={formData.start_date}
                    >
                      <InputTextLight
                        editable={false}
                        value={formData.end_date ? moment(formData.end_date).format('DD MMMM YYYY HH.mm') : ''}
                        label='End Time'
                        textInputStyle={this._validInputStyle('end_date')}
                        onChangeText={onChange('end_date')}
                      />
                    </InputDateTime>
                  </View>
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY} style={STYLE.bookButton} onPress={() => {
                      Keyboard.dismiss()
                      onSubmit()
                    }}
                  >
                    <Text centered weight={400} color={WHITE}>Book</Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </View>
          )}
        </Form>

      </Container>
    )
  }
}

VenueBookingScreen.propTypes = {
  userData: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  navigateBack: PropTypes.func,
  navigateTo: PropTypes.func,
  venue: PropTypes.objectOf(PropTypes.any),
}

VenueBookingScreen.defaultProps = {
  userData: {},
  isLoading: false,
  navigateBack: () => {},
  navigateTo: () => {},
  venue: PropTypes.objectOf(PropTypes.any)
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(VenueBookingScreen),
  mapFromNavigationParam
)
