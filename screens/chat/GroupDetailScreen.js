import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  TouchableOpacity,
  View,
  Image as RNImage,
  StyleSheet,
  TextInput,
} from "react-native";
import { connect } from "react-redux";
import { concat, isEmpty } from "lodash-es";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import noop from "lodash-es/noop";
import { NavigationActions } from "@react-navigation/compat";
import { LinearGradient } from "expo-linear-gradient";
import moment from "moment";
import Icon from "../../components/Icon";
import {
  GUN_METAL,
  NAVY_DARK,
  PALE_BLUE,
  PALE_BLUE_TWO,
  PALE_GREY,
  PALE_WHITE,
  PURPLE_DARKER,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SILVER_CALMER,
  SILVER_TWO,
  WHITE,
} from "../../constants/Colors";
import style from "sf-styles/register";
import Text from "../../components/Text";
import Container from "../../components/Container";
import _enhancedNavigation from "../../navigation/_enhancedNavigation";
import {
  FONT_SIZE,
  WP05,
  WP1,
  WP10,
  WP2,
  WP3,
  WP4,
  WP5,
  WP6,
  WP7,
  WP70,
  WP8,
} from "../../constants/Sizes";
import { imageBase64ToUrl, sendGroupEvent } from "../../utils/helper";
import Image from "../../components/Image";
import {
  postAddMember,
  postRemoveMember,
  postEditGroupChat,
  getDetailMessage,
  putEditMember,
  putEditGroupchat,
} from "../../actions/api";
import { resetAction } from "../../utils/backhandler";
import ImageUploadModal from "../../components/upload_image/ImageUploadModal";
import FollowItem from "../../components/follow/FollowItem";
import env from "../../utils/env";
import HeaderNormal from "../../components/HeaderNormal";
import headerStyle from "../profile/v2/ProfileScreen/style";
import MenuOptions from "../../components/MenuOptions";
import ConfirmationPopup from "../../components/popUp/ConfirmationPopUp";
import { CHAT_EVENT, Configs, MESSAGE_POP_UP } from "./Constants";
import { Form, InputTextLight } from "../../components";
import Touchable from "../../components/Touchable";

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapFromNavigationParam = (getParam) => ({
  editing: getParam("editing", false),
  room: getParam("room", {}),
  onUpdate: getParam("onUpdate", noop),
  id_groupmessage: getParam("id_groupmessage", {}),
  groupname: getParam("groupname", {}),
  groupimage: getParam("groupimage", {}),
  userChoose: getParam("selectedUser", {}),
  onEditGroup: getParam("onEditGroup", () => {}),
});

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: "50%",
    borderColor: REDDISH,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 6,
  },
});

class GroupDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roomDetail: {},
      name: this.props.groupname,
      avatar: {
        uri:
          typeof this.props.groupimage == "object"
            ? `${env.apiUrl}assets/img/default-chatgroup-avatar.png`
            : this.props.groupimage,
      },
      members: [],
      isLoad: true,
      isAdmin: false,
      isChanged: false,
      myProfile: {},
      showChangeAdminModal: false,
      showRemoveMemberModal: false,
      removedUser: {},
      showAllMembers: false,
      onEditName: false,
    };
  }

  componentDidMount = () => {
    this._getDetailMember();
    // this._getInitialData();
  };

  _getInitialData = () => {
    this.setState({
      members: [],
      isLoad: true,
    });
    this._getDetailMember();
    this.setState({
      isLoad: false,
    });
  };

  _navigateBackCallback = async () => {
    this.props.popToTop();
    this.props.navigateTo("ChatScreen");
  };

  _backHandler = async () => {
    const { navigateBack, onEditGroup } = this.props;
    const { name, avatar } = this.state;
    // const onRefresh = async () => {};
    // const navigateBackCallback = async () => {
    //   navigateBack();
    // };
    console.log("fullname", this.state.name);

    navigateBack({
      user_fullname: this.state.name,
      user_profile_picture: this.state.avatar,
    });
    onEditGroup({ receiverFullName: name, receiverProfilePicture: avatar });
  };

  _isValid = () => {
    const { name, avatar, members, isChanged } = this.state;
    if (this.props.editing) return isChanged;
    return !isEmpty(name) && !isEmpty(avatar) && !isEmpty(members);
  };

  _onUpdate = async () => {
    const { avatar, name } = this.state;
    const { dispatch, id_groupmessage, userData } = this.props;

    this.setState({ isChanged: false, isLoad: true });

    const options = {
      avatarURL: avatar.uri,
      base64: false,
    };

    const body =
      avatar.base64 != undefined
        ? {
            id_user: Number(userData.id_user),
            title: name,
            id_groupmessage: id_groupmessage,
            image: options.avatarURL,
          }
        : {
            id_user: Number(userData.id_user),
            title: name,
            id_groupmessage: id_groupmessage,
          };

    if (avatar.base64 != undefined) {
      const fileData = avatar.base64;
      options.avatarURL = fileData;
      body.image = fileData;
    }

    try {
      const response = await dispatch(
        putEditGroupchat,
        body,
        noop,
        true,
        false
      );

      console.log("resp", response);
      // await this._sendGroupEvent(
      //   "Foto group telah diganti",
      //   CHAT_EVENT.SF_STATIC_GROUP_EVENT
      // );
      this.setState({ isLoad: false });
      this.props.onUpdate();
    } catch (e) {
      this.setState({ isChanged: true, isLoad: false });
    }
  };

  _onChange = (key) => async (value) => {
    await this.setState({ [key]: value, isChanged: true });
    console.log("key", key, value);
    if (key === "avatar" || key === "name") {
      this._onUpdate();
    }
  };

  _getDetailMember = async (isLoading = true) => {
    const {
      userData: { id_user },
      dispatch,
      id_groupmessage,
    } = this.props;
    const { members } = this.state;
    const params = {
      id_user: id_user,
      id_groupmessage: id_groupmessage,
      type: "1",
    };

    const dataResponse = await dispatch(
      getDetailMessage,
      params,
      noop,
      true,
      isLoading
    );
    if (dataResponse.code == 200) {
      let newMember = members;
      if (!isEmpty(dataResponse.result.member)) {
        newMember = concat(newMember, dataResponse.result.member);
      }
      const user = dataResponse.result.member.find(
        (user) => user.id_user == id_user
      );
      this.setState({
        name: dataResponse.result.header.title,
        avatar: { uri: dataResponse.result.header.image },
        members: newMember,
        isLoad: false,
        isReady: true,
        createdBy: dataResponse.result.header.full_name,
        isAdmin: user.role === "admin",
        myProfile: user,
      });
    }
  };

  _constructParticipants = (members) => {
    const participants = [];
    members.map((user) => participants.push(user.id_user));
    return participants;
  };

  _onChangeParticipant = async (participants) => {
    const { dispatch, room, userData, id_groupmessage } = this.props;
    const { members } = this.state;

    this._onChange("members")(participants);

    const memberIds = this._constructParticipants(members);
    const newMembers = participants.filter(
      (user) => !memberIds.includes(user.id_user)
    );
    const newMemberIds = this._constructParticipants(newMembers);

    console.log("newmember", newMemberIds);
    const response = await dispatch(postAddMember, {
      id_user: Number(userData.id_user),
      id_user_insert: newMemberIds,
      id_groupmessage: id_groupmessage,
    });

    for (let index = 0; index < newMembers.length; index++) {
      // eslint-disable-next-line
      await this._sendGroupEvent(
        `mengundang ${newMembers[index].full_name}`,
        CHAT_EVENT.SF_GROUP_EVENT
      );
    }
    this.props.onUpdate();
  };

  _sendGroupEvent = async (message, type) => {
    const { room, userData } = this.props;
    await sendGroupEvent(userData, room.id, message, type);
  };

  _onLeaveGroup = async () => {
    const { userData } = this.props;
    const { myProfile, isAdmin, members } = this.state;
    if (isAdmin) {
      const admins = members.filter((user) => user.role === "admin");
      if (admins.length === 1) {
        if (members.length === 1) {
          await this._onModifyAdmin(myProfile, "member");
          this._doRemoveUser(myProfile);
          this._navigateBackCallback();
        } else {
          this.setState({ showChangeAdminModal: true });
        }
      } else {
        await this._onModifyAdmin(myProfile, "member");
        this._doRemoveUser(myProfile);
        this._navigateBackCallback();
      }
    } else {
      this._doRemoveUser(myProfile);
      this._navigateBackCallback();
    }
  };

  _doRemoveUser = async (user) => {
    const { dispatch, room, userData, navigation, id_groupmessage } =
      this.props;
    const { members } = this.state;

    this.setState({ isLoad: true });
    if (user.id_user === Number(userData.id_user)) {
      await dispatch(postRemoveMember, {
        id_user: userData.id_user,
        id_user_insert: user.id_user,
        id_groupmessage: id_groupmessage,
      });
      resetAction(
        navigation,
        1,
        [NavigationActions.navigate({ routeName: "ChatScreen" })],
        "ExploreScreen"
      );
      // await this._sendGroupEvent(
      //   "telah meninggalkan group",
      //   CHAT_EVENT.SF_GROUP_EVENT
      // );
    } else {
      const respsonse = await dispatch(postRemoveMember, {
        id_user: userData.id_user,
        id_user_insert: user.id_user,
        id_groupmessage: id_groupmessage,
      });
      const newMembers = members.filter(
        (member) => member.id_user !== user.id_user
      );
      this._onChange("members")(newMembers);
      await this._sendGroupEvent(
        `${user.full_name} telah dikeluarkan dari group`,
        CHAT_EVENT.SF_STATIC_GROUP_EVENT
      );
      this.props.onUpdate();
    }
    this.setState({ isLoad: false });
  };

  _onModifyAdmin = async (user, role) => {
    const { dispatch, room, onUpdate, userData, id_groupmessage } = this.props;
    const body = {
      id_user: userData.id_user,
      id_groupmessage: id_groupmessage,
      role,
      target_id: user.id_user,
    };
    const members = [...this.state.members];
    this.setState({ isLoad: true });
    await dispatch(putEditMember, body, noop, false, false);
    members.map((member) => {
      if (member.id_user === user.id_user) member.role = role;
      return true;
    });
    this.setState({ members, isLoad: false });
    if (role === "admin") {
      this._sendGroupEvent(
        `${user.full_name} telah menjadi admin`,
        CHAT_EVENT.SF_STATIC_GROUP_EVENT
      );
      onUpdate();
    }
  };

  _renderReplaceAdminModal = () => {
    const { userData, room, id_groupmessage } = this.props;
    const { members, showChangeAdminModal, myProfile } = this.state;
    return (
      <ConfirmationPopup
        isVisible={showChangeAdminModal}
        onCancel={() => {
          this.setState({ showChangeAdminModal: false });
        }}
        onConfirm={() => {
          this.setState({ showChangeAdminModal: false });
          const admins = members.filter(
            (user) => user.role === "admin" && user.id_user !== userData.id_user
          );
          const groupMembers = members.filter(
            (user) => user.id_user !== userData.id_user
          );
          this.props.navigateTo("ChooseAdminGroup", {
            members: groupMembers,
            onSubmitCallback: (selectedUser) => {
              this.setState({ isLoad: true });
              const user = selectedUser.find((user) => user.id_user);
              this.state.members.map((member) => {
                if (member.id_user === user.id_user) member.role = "admin";
                return true;
              });
              // this._onModifyAdmin(user.id_user, "admin");
              this.setState({ isLoad: false });
            },
            selectedUser: admins,
            room,
            id_groupmessage: id_groupmessage,
          });
        }}
        template={MESSAGE_POP_UP.CHANGE_ADMIN}
      />
    );
  };

  _renderRemoveMemberModal = () => {
    const template = MESSAGE_POP_UP.REMOVE_USER(this.state.removedUser);
    return (
      <ConfirmationPopup
        isVisible={this.state.showRemoveMemberModal}
        onCancel={() => {
          this.setState({ showRemoveMemberModal: false });
        }}
        onConfirm={() => {
          this.setState({ showRemoveMemberModal: false });
          this._doRemoveUser(this.state.removedUser);
        }}
        template={template}
      />
    );
  };

  render() {
    const { navigateTo, editing, userData } = this.props;

    const {
      avatar,
      name,
      members,
      isLoad,
      isAdmin,
      roomDetail,
      showAllMembers,
      createdBy,
      onEditName,
    } = this.state;

    const displayedMembers = showAllMembers
      ? members
      : members.slice(0, Configs.DEFAULT_DISPLAYED_MEMBER);
    const totalUserHidden = members.length - Configs.DEFAULT_DISPLAYED_MEMBER;

    return (
      <Container
        isLoading={isLoad}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={this._backHandler}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="Details Group"
              centered
            />
            <LinearGradient
              colors={SHADOW_GRADIENT}
              style={headerStyle.headerShadow}
            />
          </View>
        )}
      >
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ backgroundColor: WHITE }}
          style={{ flexGrow: 1 }}
        >
          {this._renderReplaceAdminModal()}
          {this._renderRemoveMemberModal()}

          <View
            style={{
              paddingHorizontal: WP6,
              paddingVertical: WP5,
              alignItems: "center",
              borderBottomWidth: 0.5,
              borderBottomColor: PALE_BLUE,
            }}
          >
            <ImageUploadModal
              disabled={!isAdmin}
              onChange={this._onChange("avatar")}
              removable={false}
            >
              <View style={{ width: wp("27%") }}>
                <Image
                  source={{ uri: avatar.uri }}
                  imageStyle={{
                    width: wp("27%"),
                    height: wp("27%"),
                    borderRadius: wp("27%") / 2,
                  }}
                />
                {isAdmin && (
                  <Image
                    source={require("../../assets/icons/ic_camera_clear_blue.png")}
                    style={{ position: "absolute", right: 0, bottom: 0 }}
                    size="mini"
                  />
                )}
              </View>
            </ImageUploadModal>
            <View
              style={{
                flexDirection: "row",
                // marginVertical: WP2,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {onEditName ? (
                <Form>
                  {({ onChange, onSubmit, formData }) => (
                    <View>
                      <TextInput
                        value={name}
                        onChangeText={(groupname) =>
                          this.setState({ name: groupname })
                        }
                        autoCompleteType={"password"}
                        placeholder={"Tuliskan nama group"}
                        color={REDDISH}
                        placeholderTextColor={SHIP_GREY_CALM}
                        style={style.inputEditGroup}
                      />
                      <Touchable style={style.passwordToggler}>
                        <Icon
                          name="check"
                          color={REDDISH}
                          size="large"
                          center
                          onPress={() => {
                            this._onChange("name")(this.state.name);
                            this.setState({ onEditName: false });
                          }}
                        />
                      </Touchable>
                    </View>
                  )}
                </Form>
              ) : (
                <View
                  style={{
                    flexDirection: "row",
                    marginVertical: WP2,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Text
                    type="Circular"
                    weight={600}
                    color={GUN_METAL}
                    style={{ marginRight: WP2 }}
                    size="large"
                    centered
                  >
                    {name}
                  </Text>
                  {isAdmin && (
                    <Image
                      source={require("../../assets/icons/v3/icEditGroup.png")}
                      centered
                      size="xtiny"
                      onPress={() => {
                        this.setState({ onEditName: true });
                      }}
                    />
                  )}
                </View>
              )}
            </View>
          </View>

          <View
            style={{
              paddingHorizontal: WP6,
              paddingVertical: WP3,
              backgroundColor: PALE_WHITE,
            }}
          >
            <Text
              size="mini"
              color={SHIP_GREY_CALM}
              style={{ marginBottom: WP3 }}
            >
              {`${members.length} Members`}
            </Text>

            {isAdmin && (
              <TouchableOpacity
                disabled={!isAdmin}
                onPress={() =>
                  navigateTo("AddMemberGroupChatScreen", {
                    onChange: this._onChangeParticipant,
                    isGroupChat: true,
                    selectedUser: members,
                    editing,
                  })
                }
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingVertical: WP4,
                  borderBottomColor: PALE_GREY,
                  borderBottomWidth: 1,
                }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: SILVER_TWO,
                    borderRadius: WP5,
                    width: WP10,
                    height: WP10,
                    marginBottom: WP1,
                  }}
                >
                  <Text
                    weight={400}
                    type={"Circular"}
                    color={WHITE}
                    size="massive"
                    lineHeight={WP7}
                  >
                    +
                  </Text>
                </View>
                <Text
                  style={{ marginLeft: WP3 }}
                  color={GUN_METAL}
                  size="mini"
                  weight={500}
                >
                  Add Member
                </Text>
              </TouchableOpacity>
            )}
            {displayedMembers.map((user, index) => {
              const isUserAdmin = user.role === "admin";
              const isMine = user.id_user === userData.id_user;
              return (
                <View key={index} style={{ flexDirection: "row", flex: 1 }}>
                  <FollowItem
                    onPressItem={() =>
                      this.props.navigateTo("ProfileScreenNoTab", {
                        idUser: user.id_user,
                      })
                    }
                    key={index}
                    imageSize="xsmall"
                    isMine={isMine}
                    user={user}
                    style={{ marginHorizontal: 0 }}
                    label={
                      isUserAdmin ? (
                        <View style={{ flexDirection: "row", marginLeft: WP2 }}>
                          <RNImage
                            style={{
                              width: WP4,
                              height: WP4,
                              bottom: 0,
                              right: 0,
                              aspectRatio: 4 / 5,
                            }}
                            source={require("../../assets/icons/badgeEventeer.png")}
                          />
                          <Text type="Circular" color={REDDISH} size="xmini">
                            {"  ∙  Admin"}
                          </Text>
                        </View>
                      ) : null
                    }
                  />
                  {isAdmin && user.id_user != userData.id_user && (
                    <MenuOptions
                      options={[
                        {
                          onPress: () => {
                            const newRole = isUserAdmin ? "member" : "admin";
                            this._onModifyAdmin(user, newRole);
                          },
                          title: "Jadikan sebagai admin",
                          title: isUserAdmin
                            ? "Batalkan sebagai admin"
                            : "Jadikan sebagai admin",
                          validation: true,
                          image: require("../../assets/icons/ic_group_admin.png"),
                          imageStyle: { width: WP8 },
                        },
                        {
                          onPress: () => {
                            setTimeout(
                              () =>
                                this.setState({
                                  showRemoveMemberModal: true,
                                  removedUser: user,
                                }),
                              300
                            );
                          },
                          title: "Keluarkan member",
                          validation: true,
                          image: require("../../assets/icons/ic_group_remove.png"),
                          imageStyle: { width: WP8 },
                        },
                      ]}
                      triggerComponent={(toggleModal) => (
                        <TouchableOpacity
                          onPress={toggleModal}
                          style={{
                            padding: 5,
                            justifyContent: "center",
                          }}
                        >
                          <Icon
                            centered
                            name={"dots-three-horizontal"}
                            type="Entypo"
                            color={SHIP_GREY_CALM}
                          />
                        </TouchableOpacity>
                      )}
                    />
                  )}
                </View>
              );
            })}
            {totalUserHidden > 0 && !showAllMembers && (
              <TouchableOpacity
                onPress={() => this.setState({ showAllMembers: true })}
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  paddingBottom: WP3,
                  paddingTop: WP5,
                }}
              >
                <Text
                  type="Circular"
                  color={REDDISH}
                  weight={400}
                >{`${totalUserHidden} lainnya   `}</Text>
                <Icon
                  type="FontAwesome"
                  size="slight"
                  name="chevron-down"
                  color={REDDISH}
                  style={{ marginTop: WP05 }}
                />
              </TouchableOpacity>
            )}
          </View>

          <View
            style={{
              backgroundColor: WHITE,
              borderBottomWidth: 0.5,
              borderBottomColor: PALE_BLUE,
              borderTopWidth: 0.5,
              borderTopColor: PALE_BLUE,
            }}
          >
            <TouchableOpacity
              onPress={this._onLeaveGroup}
              style={{ paddingHorizontal: WP5, paddingVertical: WP5 }}
            >
              <Text color={SHIP_GREY} weight={400} size="slight">
                Keluar dari group
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{ paddingHorizontal: WP5, paddingVertical: WP5 }}>
            <Text
              type="Circular"
              color={SHIP_GREY_CALM}
              size="xmini"
            >{`Dibuat oleh ${this.state.createdBy || ""}`}</Text>
            <Text type="Circular" color={SHIP_GREY_CALM} size="xmini">
              {moment(roomDetail.created_at).format("DD MMMM YYYY")}
            </Text>
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

GroupDetailScreen.propTypes = {
  editing: PropTypes.bool,
  room: PropTypes.objectOf(PropTypes.any),
  onUpdate: PropTypes.func,
};

GroupDetailScreen.defaultProps = {
  editing: false,
  room: {},
  onUpdate: noop,
};

export default _enhancedNavigation(
  connect(mapStateToProps, {})(GroupDetailScreen),
  mapFromNavigationParam
);
