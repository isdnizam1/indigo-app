import React, { Component } from "react";
import { ScrollView, TextInput, TouchableOpacity, View } from "react-native";
import { isEmpty, noop } from "lodash-es";
import Qiscus from "qiscus-sdk-core";
import { connect } from "react-redux";
// import { LinearGradient } from 'expo-linear-gradient'
import { WP2, WP5 } from "sf-constants/Sizes";
import {
  ORANGE_BRIGHT,
  PALE_BLUE_TWO,
  PALE_SALMON,
  PALE_WHITE,
  /*SHADOW_GRADIENT, */ SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from "sf-constants/Colors";
import { GET_MESSAGE_UNREAD } from "sf-services/message/actionTypes";
import env from "sf-utils/env";
import Container from "sf-components/Container";
import Icon from "sf-components/Icon";
import Text from "sf-components/Text";
import _enhancedNavigation from "sf-navigation/_enhancedNavigation";
import HeaderNormal from "sf-components/HeaderNormal";
import { HEADER, TOUCH_OPACITY } from "sf-constants/Styles";
import UserSelectionItem from "sf-components/chat/UserSelectionItem";
// import headerStyle from '../profile/v2/ProfileScreen/style'
import { Configs, styles } from "./Constants";
import { getProfileFollowers, getProfileFollowing } from "../../actions/api";

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam("onRefresh", () => {}),
  selectedUser: getParam("selectedUser", []),
  onChange: getParam("onChange", undefined),
  editing: getParam("editing", false),
});

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapDispatchToProps = {
  setUnreadMessage: (unreadMessageCount) => ({
    type: GET_MESSAGE_UNREAD,
    response: unreadMessageCount,
  }),
};

class AddMemberGroupChatScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      qiscus: {},
      friends: [],
      selectedUser: this.props.selectedUser,
    };
  }

  componentDidMount = async () => {
    // await this._settingUpQiscus()
    await this._getFriends();
  };

  _settingUpQiscus = async () => {
    const { userData } = this.props;

    let qiscus = new Qiscus();
    qiscus.init({
      AppId: env.qiscusAppId,
    });

    await qiscus.setUser(
      userData.id_user,
      userData.id_user,
      userData.full_name
    );

    await this.setState({
      qiscus,
    });
  };

  _getFriends = async () => {
    const { dispatch, userData } = this.props;
    const { name } = this.state;
    const params = {
      followed_by: userData.id_user,
      id_viewer: userData.id_user,
      name,
      start: 0,
      limit: 15,
    };
    const dataResponse = await dispatch(
      getProfileFollowing,
      params,
      noop,
      true,
      false
    );
    if (dataResponse.code == 200) {
      this.setState({ friends: dataResponse.result });
    }
  };

  static navigationOptions = () => ({
    gesturesEnabled: true,
  });

  _onChangeName = (name) => {
    this.setState({ name }, this._getFriends);
  };

  _onPressSuggestion = (user) => async () => {
    const { selectedUser } = this.state;

    const isUserSelected = selectedUser.find(
      (item) => item.id_user === user.id_user
    );
    let selected = [...selectedUser];
    if (isUserSelected) {
      selected = selectedUser.filter((item) => item.id_user !== user.id_user);
    } else {
      selected.push(user);
    }
    this.setState({ selectedUser: selected });
  };

  _onSubmit = () => {
    const { onChange, navigateBack } = this.props;
    const { selectedUser } = this.state;
    if (onChange) {
      onChange(selectedUser);
      navigateBack();
    } else {
      this.props.navigateTo("NewGroupChatScreen", { members: selectedUser });
    }
  };

  render() {
    const { navigateBack, editing, selectedUser: selected } = this.props;

    const { name, friends, selectedUser } = this.state;

    return (
      <Container
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={() => navigateBack()}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="Group Chat"
              centered
              backgroundColor={WHITE}
              wrapperStyle={{
                borderBottomColor: PALE_BLUE_TWO,
                borderBottomWidth: 0.5,
              }}
              rightComponent={
                <TouchableOpacity
                  onPress={this._onSubmit}
                  activeOpacity={TOUCH_OPACITY}
                  disabled={isEmpty(selectedUser)}
                  style={HEADER.rightIcon}
                >
                  <Text
                    size="slight"
                    weight={400}
                    color={!isEmpty(selectedUser) ? ORANGE_BRIGHT : PALE_SALMON}
                  >
                    Next
                  </Text>
                </TouchableOpacity>
              }
            />
          </View>
        )}
        colors={[PALE_WHITE, PALE_WHITE]}
      >
        <View
          style={{
            padding: WP5,
            borderBottomColor: PALE_BLUE_TWO,
            borderBottomWidth: 0,
            backgroundColor: WHITE,
          }}
        >
          <View style={styles.headerSearch}>
            <Icon
              centered
              background="dark-circle"
              size="large"
              color={SHIP_GREY_CALM}
              name="magnify"
              type="MaterialCommunityIcons"
              style={styles.headerSearchIcon}
            />
            <TextInput
              value={name}
              onChangeText={this._onChangeName}
              autoFocus={!__DEV__}
              placeholder={"Cari teman"}
              style={styles.headerSearchInput}
            />
          </View>
        </View>

        <View style={[styles.wrapper, styles.label, { marginTop: WP2 }]}>
          <Text weight={400} color={SHIP_GREY_CALM} size="slight">
            Daftar Teman
          </Text>
        </View>
        <ScrollView>
          {friends.map((user, index) => {
            if (
              editing &&
              selected.find((item) => item.id_user === user.id_user)
            )
              return;
            return (
              <UserSelectionItem
                key={index}
                user={user}
                onPress={this._onPressSuggestion(user)}
                selected={selectedUser.find(
                  (item) => item.id_user === user.id_user
                )}
              />
            );
          })}
        </ScrollView>
      </Container>
    );
  }
}

AddMemberGroupChatScreen.propTypes = {};

AddMemberGroupChatScreen.defaultProps = {};

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(AddMemberGroupChatScreen),
  mapFromNavigationParam
);
