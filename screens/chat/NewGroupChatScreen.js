import React, { Component } from "react";
import { TouchableOpacity, View } from "react-native";
import Qiscus from "qiscus-sdk-core";
import { connect } from "react-redux";
import { isEmpty } from "lodash-es";
import noop from "lodash-es/noop";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import PropTypes from "prop-types";
import { LinearGradient } from "expo-linear-gradient";
import env from "../../utils/env";
import _enhancedNavigation from "../../navigation/_enhancedNavigation";
import {
  GUN_METAL,
  NAVY_DARK,
  ORANGE_BRIGHT,
  PALE_BLUE,
  PALE_GREY_THREE,
  PALE_SALMON,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SILVER_TWO,
  WHITE,
} from "../../constants/Colors";
import Icon from "../../components/Icon";
import Container from "../../components/Container";
import Text from "../../components/Text";
import {
  WP1,
  WP15,
  WP3,
  WP308,
  WP4,
  WP5,
  WP6,
  WP9,
} from "../../constants/Sizes";
import {
  convertBlobToBase64,
  fetchAsBlob,
  imageBase64ToUrl,
  initiateGroupRoom,
  sendGroupEvent,
} from "../../utils/helper";
import InputTextLight from "../../components/InputTextLight";
import Image from "../../components/Image";
import {
  postAddMemberGroup,
  postCreateGroupChat,
  postCreateChatGroup,
} from "../../actions/api";
import {
  HEADER,
  TEXT_INPUT_STYLE,
  TOUCH_OPACITY,
} from "../../constants/Styles";
import HeaderNormal from "../../components/HeaderNormal";
import headerStyle from "../profile/v2/ProfileScreen/style";
import ExitWarningPopUp from "../../components/popUp/ExitWarningPopUp";
import ImageUploadModal from "../../components/upload_image/ImageUploadModal";
import { CHAT_EVENT, MESSAGE_POP_UP } from "./Constants";

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapFromNavigationParam = (getParam) => ({
  editing: getParam("editing", false),
  room: getParam("room", {}),
  onUpdate: getParam("onUpdate", noop),
  members: getParam("members", []),
});

class NewGroupChatScreen extends Component {
  static navigationOptions = () => ({
    gesturesEnabled: false,
  });

  constructor(props) {
    super(props);
    this.state = {
      name: "",
      avatar: {
        uri: `${env.apiUrl}assets/img/default-chatgroup-avatar.png`,
      },
      qiscus: {},
      members: this.props.members,
      isLoading: false,
      admin: undefined,
      isChanged: false,
      showExitWarning: false,
    };
  }

  componentDidMount = async () => {
    const { userData } = this.props;
    const { avatar } = this.state;
    var listMember = [];
    const memberIds = this._constructParticipants(this.props.members);
    listMember = [userData.id_user, ...memberIds];
    await this._settingUpQiscus();
  };

  _settingUpQiscus = async () => {
    const { userData } = this.props;

    let qiscus = new Qiscus();
    qiscus.init({
      AppId: env.qiscusAppId,
    });

    await qiscus.setUser(
      userData.id_user,
      userData.id_user,
      userData.full_name
    );

    await this.setState({
      qiscus,
    });
  };

  _backHandler = async () => {
    this.setState({ showExitWarning: true });
  };

  _navigateBackCallback = async () => {
    this.props.popToTop();
    this.props.navigateTo("ChatScreen");
  };

  _onCreateRoom = async () => {
    const { name, avatar, members, isLoading } = this.state;
    const { onRefresh, userData, navigateTo, dispatch } = this.props;
    const memberIds = this._constructParticipants(members);
    const options = {
      avatarURL: avatar.uri,
      base64: false,
    };
    const bodyCreate = {
      id_user: Number(userData.id_user),
      title: name,
      list_user: [userData.id_user, ...memberIds],
      image: options.avatarURL,
    };

    if (avatar.base64 != undefined) {
      const fileData = avatar.base64;
      options.avatarURL = fileData;
      bodyCreate.image = fileData;
    } else {
      let result = await fetchAsBlob(avatar.uri);
      let fileData = await convertBlobToBase64(result);
      options.avatarURL = fileData;
      bodyCreate.image = fileData;
    }

    this.setState({ isLoading: true });
    try {
      const response = await postCreateChatGroup(bodyCreate);
      const navigateBackCallback = this._navigateBackCallback;
      // await sendGroupEvent(
      //   userData,
      //   chatRoomDetail.room.id,
      //   `membuat group "${name}"`,
      //   CHAT_EVENT.SF_GROUP_EVENT
      // );

      this.setState({ isLoading: false });
      await navigateTo("ChatRoomScreen", {
        user_fullname: response.data.result.title,
        user_profile_picture: response.data.result.image,
        id_groupmessage: response.data.result.id_groupmessage,
        type: response.data.result.type,
        onRefresh,
        navigateBackCallback,
        fromScreen: "NewGroupChatScreen",
      });
    } catch (e) {
      this.setState({ isLoading: false });
    }
  };

  _isValid = () => {
    const { name, members } = this.state;
    return !isEmpty(name) && members.length > 0;
  };

  _onChange = (key) => (value) => {
    const { avatar } = this.state;
    this.setState({ [key]: value, isChanged: true });
  };

  _constructParticipants = (members) => {
    const participants = [];
    members.map((user) => participants.push(user.id_user));
    return participants;
  };

  _onRemoveUser = async (user) => {
    const { members } = this.state;
    const newMembers = members.filter(
      (member) => member.id_user !== user.id_user
    );
    this._onChange("members")(newMembers);
  };

  isAdmin = (user) => Number(this.state.admin) === Number(user.id_user);

  isAbleToEdit = () => {
    const { userData, editing } = this.props;
    return this.isAdmin(userData) || !editing;
  };

  render() {
    const { navigateTo, userData } = this.props;

    const { avatar, name, members, isLoading, showExitWarning } = this.state;

    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={this._backHandler}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="Group Chat"
              centered
              rightComponent={
                <TouchableOpacity
                  onPress={this._onCreateRoom}
                  activeOpacity={TOUCH_OPACITY}
                  disabled={!this._isValid()}
                  style={{ ...HEADER.rightIcon }}
                >
                  <Text
                    size="slight"
                    weight={400}
                    color={this._isValid() ? ORANGE_BRIGHT : PALE_SALMON}
                  >
                    Create
                  </Text>
                </TouchableOpacity>
              }
            />
            <LinearGradient
              colors={SHADOW_GRADIENT}
              style={headerStyle.headerShadow}
            />
          </View>
        )}
      >
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flex: 1 }}
        >
          <ExitWarningPopUp
            isVisible={showExitWarning}
            onCancel={() => {
              this.setState({ showExitWarning: false });
              this._navigateBackCallback();
            }}
            onContinue={() => this.setState({ showExitWarning: false })}
            template={MESSAGE_POP_UP.EXIT_WARNING}
          />

          <View
            style={{
              paddingHorizontal: WP6,
              paddingVertical: WP4,
              borderBottomWidth: 1,
              borderBottomColor: PALE_BLUE,
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <ImageUploadModal
              onChange={this._onChange("avatar")}
              removable={false}
            >
              <View style={{ width: wp("27%") }}>
                <Image
                  source={{ uri: avatar.uri }}
                  imageStyle={{
                    width: wp("27%"),
                    height: wp("27%"),
                    borderRadius: wp("27%") / 2,
                  }}
                />
                <Image
                  source={require("../../assets/icons/ic_camera_clear_blue.png")}
                  style={{ position: "absolute", right: 0, bottom: 0 }}
                  size="mini"
                />
              </View>
            </ImageUploadModal>

            <View
              style={{
                flexGrow: 1,
                flex: 1,
                marginLeft: WP3,
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <InputTextLight
                withLabel={false}
                placeholder="Group Name"
                value={name}
                onChangeText={this._onChange("name")}
                placeholderTextColor={SILVER_TWO}
                color={SHIP_GREY}
                bordered
                size="mini"
                type="Circular"
                returnKeyType={"next"}
                wording=" "
                maxLengthFocus
                maxLength={25}
                lineHeight={1}
                style={{ marginTop: 0, marginBottom: 0 }}
                textInputStyle={TEXT_INPUT_STYLE["inputV2"]}
                labelv2="Group Chat"
                editable={this.isAbleToEdit()}
              />
            </View>
          </View>

          <View
            style={{
              paddingHorizontal: WP5 - 1,
              paddingVertical: WP5,
              backgroundColor: PALE_GREY_THREE,
              flexGrow: 1,
              flex: 1,
            }}
          >
          <Text
              size="mini"
              weight={400}
              color={SHIP_GREY_CALM}
              style={{ marginBottom: WP5 }}
            >{`${members.length + 1} Members`}</Text>

            <View style={{ flexDirection: "row", flexWrap: "wrap", flex: 1 }}>
              <View style={{ marginRight: WP3, marginBottom: WP5 }}>
                <TouchableOpacity
                  disabled={!this.isAbleToEdit()}
                  onPress={() =>
                    navigateTo(
                      "AddMemberGroupChatScreen",
                      {
                        onChange: this._onChange("members"),
                        isGroupChat: true,
                        selectedUser: members,
                      },
                      "push"
                    )
                  }
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: NAVY_DARK,
                    borderRadius: WP15 / 2,
                    width: WP15,
                    height: WP15,
                    marginBottom: WP1,
                  }}
                >
                  <Text
                    weight={400}
                    type={"Circular"}
                    color={WHITE}
                    size="extraMassive"
                    lineHeight={WP9}
                  >
                    +
                  </Text>
                </TouchableOpacity>
                <Text
                  type="Circular"
                  size="mini"
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  color={GUN_METAL}
                  centered
                >
                  Add
                </Text>
              </View>

              <View
                style={{
                  marginRight: WP3,
                  marginBottom: WP5,
                  width: WP15,
                  height: WP15,
                }}
              >
                <Image
                  source={{ uri: userData.profile_picture }}
                  style={{ width: WP15, height: WP15, marginBottom: WP1 }}
                  imageStyle={{
                    width: WP15,
                    height: WP15,
                    borderRadius: WP15 / 2,
                  }}
                />
                <Text
                  type="Circular"
                  size="mini"
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  color={GUN_METAL}
                  centered
                >
                  you
                </Text>
              </View>
              {members.map((user, index) => (
                <View
                  key={index}
                  style={{
                    marginRight: WP3,
                    marginBottom: WP5,
                    width: WP15,
                    height: WP15,
                  }}
                >
                  <Image
                    source={{ uri: user.profile_picture }}
                    style={{ width: WP15, height: WP15, marginBottom: WP1 }}
                    imageStyle={{
                      width: WP15,
                      height: WP15,
                      borderRadius: WP15 / 2,
                    }}
                  />
                  <Text
                    type="Circular"
                    size="mini"
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    color={GUN_METAL}
                    centered
                  >
                    {user.full_name}
                  </Text>
                  <Icon
                    name="close"
                    color={WHITE}
                    backgroundColor={NAVY_DARK}
                    size="tiny"
                    background="dark-circle"
                    style={{
                      borderRadius: WP308,
                      position: "absolute",
                      right: 0,
                      top: 0,
                    }}
                    onPress={() => this._onRemoveUser(user)}
                  />
                </View>
              ))}
            </View>
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

NewGroupChatScreen.propTypes = {
  editing: PropTypes.bool,
  room: PropTypes.objectOf(PropTypes.any),
  onUpdate: PropTypes.func,
  members: PropTypes.arrayOf(PropTypes.any),
};

NewGroupChatScreen.defaultProps = {
  editing: false,
  room: {},
  onUpdate: noop,
  members: [],
};

export default _enhancedNavigation(
  connect(mapStateToProps, {})(NewGroupChatScreen),
  mapFromNavigationParam
);
