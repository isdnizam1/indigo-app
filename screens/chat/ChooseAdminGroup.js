import React, { Component } from "react";
import { isEmpty, noop } from "lodash-es";
import { ScrollView, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import Container from "../../components/Container";
import HeaderNormal from "../../components/HeaderNormal";
import {
  ORANGE_BRIGHT,
  PALE_BLUE_TWO,
  PALE_SALMON,
  PALE_WHITE,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from "../../constants/Colors";
import { HEADER, TOUCH_OPACITY } from "../../constants/Styles";
import Text from "../../components/Text";
import { WP2 } from "../../constants/Sizes";
import UserSelectionItem from "../../components/chat/UserSelectionItem";
import _enhancedNavigation from "../../navigation/_enhancedNavigation";
import { GET_MESSAGE_UNREAD } from "../../services/message/actionTypes";
import { postSetRoleGroup, putEditMember } from "../../actions/api";
import { Configs, styles } from "./Constants";

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam("onRefresh", () => {}),
  members: getParam("members", []),
  selectedUser: getParam("selectedUser", []),
  onSubmitCallback: getParam("onSubmitCallback", noop),
  room: getParam("room", {}),
  id_groupmessage: getParam("id_groupmessage", {}),
});

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapDispatchToProps = {
  setUnreadMessage: (unreadMessageCount) => ({
    type: GET_MESSAGE_UNREAD,
    response: unreadMessageCount,
  }),
};

class ChooseAdminGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      members: this.props.members || [],
      selectedUser: this.props.selectedUser || [],
      isLoading: false,
    };
  }

  componentDidMount = async () => {};

  static navigationOptions = () => ({
    gesturesEnabled: true,
  });

  _onPressSuggestion = (user) => async () => {
    const { selectedUser } = this.state;

    const isUserSelected = selectedUser.find(
      (item) => item.id_user === user.id_user
    );
    let selected = [...selectedUser];
    if (isUserSelected) {
      selected = selectedUser.filter((item) => item.id_user !== user.id_user);
    } else {
      if (selectedUser.length === Configs.MAX_GROUP_MEMBER - 1) {
        return;
      } else selected.push(user);
    }
    this.setState({ selectedUser: selected });
  };

  _onSubmit = async () => {
    const {
      room,
      onSubmitCallback,
      dispatch,
      userData,
      id_groupmessage,
      navigateBack,
    } = this.props;
    const { selectedUser } = this.state;
    this.setState({ isLoading: true });
    selectedUser.forEach(async (user) => {
      const body = {
        id_user: userData.id_user,
        id_groupmessage: id_groupmessage,
        role: "admin",
        target_id: user.id_user,
      };
      await dispatch(putEditMember, body, noop, false, false);
      // await dispatch(postSetRoleGroup, body, noop, false, false)
    });
    navigateBack();
    const response = await onSubmitCallback(selectedUser);

    this.setState({ isLoading: false });
  };

  render() {
    const { navigateBack } = this.props;

    const { members, selectedUser, isLoading } = this.state;

    return (
      <Container
        isLoading={isLoading}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={() => navigateBack()}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="Pilih Admin"
              centered
              backgroundColor={WHITE}
              wrapperStyle={{
                borderBottomColor: PALE_BLUE_TWO,
                borderBottomWidth: 0.5,
              }}
              rightComponent={
                <TouchableOpacity
                  onPress={this._onSubmit}
                  activeOpacity={TOUCH_OPACITY}
                  disabled={isEmpty(selectedUser)}
                  style={HEADER.rightIcon}
                >
                  <Text
                    size="slight"
                    weight={400}
                    color={!isEmpty(selectedUser) ? ORANGE_BRIGHT : PALE_SALMON}
                  >
                    Selesai
                  </Text>
                </TouchableOpacity>
              }
            />
          </View>
        )}
        colors={[PALE_WHITE, PALE_WHITE]}
      >
        <View style={[styles.wrapper, styles.label, { marginTop: WP2 }]}>
          <Text weight={400} color={SHIP_GREY_CALM} size="slight">
            Daftar Member
          </Text>
        </View>
        <ScrollView>
          {members.map((user, index) => {
            return (
              <UserSelectionItem
                key={index}
                user={user}
                selected={selectedUser.find(
                  (item) => item.id_user === user.id_user
                )}
                onPress={this._onPressSuggestion(user)}
              />
            );
          })}
        </ScrollView>
      </Container>
    );
  }
}

ChooseAdminGroup.propTypes = {};

ChooseAdminGroup.defaultProps = {};

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ChooseAdminGroup),
  mapFromNavigationParam
);
