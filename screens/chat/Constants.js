import { StyleSheet } from "react-native";
import { SHIP_GREY } from "sf-constants/Colors";
import {
  GREY_LABEL,
  PALE_BLUE,
  PALE_GREY_THREE,
  REDDISH,
} from "../../constants/Colors";
import { WP05, WP1, WP2, WP3, WP4, WP6 } from "../../constants/Sizes";

export const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: WP6,
  },
  inputWrapper: {},
  input: {
    color: GREY_LABEL,
    paddingVertical: WP3,
  },
  label: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: WP4,
  },
  // wrapper: {
  //   borderBottomWidth: 1,
  //   borderBottomColor: PALE_BLUE,
  // },
  container: {
    paddingVertical: WP2,
    flexDirection: "row",
    alignItems: "center",
  },
  headerSearch: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: PALE_GREY_THREE,
    paddingLeft: WP2,
    borderRadius: WP2,
    paddingVertical: WP1,
  },
  headerSearchInput: {
    padding: WP1,
    flex: 1,
    color: SHIP_GREY,
  },
  backButton: {
    paddingVertical: WP1,
    paddingHorizontal: WP3,
  },
  tabs: {
    flexDirection: "row",
    paddingHorizontal: WP2,
    borderTopWidth: 1,
    borderTopColor: PALE_BLUE,
  },
  tab: {
    width: "100%",
    alignItems: "center",
    paddingVertical: WP3 + WP05,
  },
  indicator: {
    height: WP1 - WP05,
    backgroundColor: REDDISH,
    position: "absolute",
    bottom: -1,
  },
  cancel: {
    paddingVertical: WP2 + WP05 + 0.5,
    paddingHorizontal: WP3,
  },
  clearText: {
    padding: WP2,
  },
  loader: {
    paddingHorizontal: WP2,
  },
});

export const Configs = {
  MAX_GROUP_MEMBER: 20,
  DEFAULT_DISPLAYED_MEMBER: 5,
};

export const MESSAGE_POP_UP = {
  EXIT_WARNING: {
    title: "Buat Group Chat",
    content:
      "Pembuatan group chat baru kamu belum selesai. Lanjutkan buat group chat?",
  },
  CHANGE_ADMIN: {
    title: "Ganti Admin Group",
    content:
      "Kamu harus memilih admin lainnya untuk dapat meninggalkan grup ini",
    confirmLabel: "Pilih Admin",
    cancelLabel: "Batalkan",
  },
  DELETE_CHAT: {
    title: "Hapus Chat",
    content: "Kamu memilih chat ini untuk dihapus. Lanjutkan menghapus chat?",
    confirmLabel: "Hapus Chat",
    cancelLabel: "Batalkan",
  },
  REMOVE_USER: (payload) => ({
    title: "Keluarkan Member",
    content: `Kamu memilih ${payload.full_name} untuk dikeluarkan dari group ini?`,
    confirmLabel: "Keluarkan Member",
    cancelLabel: "Batalkan",
  }),
};

export const CHAT_EVENT = {
  SF_STATIC_GROUP_EVENT: "sf_static_group_event",
  SF_GROUP_EVENT: "sf_group_event",
};

export const getUniqueId = (identifier) =>
  `${identifier}${Math.floor(Date.now() / 1000)}`;
