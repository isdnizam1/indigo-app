import React, { Component } from "react";
import { TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import SkeletonContent from "react-native-skeleton-content";
import { LinearGradient } from "expo-linear-gradient";
import { noop } from "lodash-es";
import { NavigationActions } from "@react-navigation/compat";
import ChatSkeletonConfig from "sf-components/chat/ChatSkeletonConfig";
import {
  Container,
  EmptyV3,
  FollowItem,
  HeaderNormal,
  Icon,
  Image,
  Text,
  _enhancedNavigation,
} from "../../components";
import { GET_MESSAGE_UNREAD } from "../../services/message/actionTypes";
import headerStyle from "../profile/v2/ProfileScreen/style";
import {
  GUN_METAL,
  PALE_BLUE,
  PALE_GREY_THREE,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
} from "../../constants/Colors";
import {
  WP05,
  WP10,
  WP100,
  WP105,
  WP14,
  WP205,
  WP3,
  WP4,
  WP5,
  WP6,
} from "../../constants/Sizes";
import {
  getSuggestedContact,
  getProfileFollowers,
  getProfileFollowing,
} from "../../actions/api";
import { resetAction } from "../../utils/backhandler";
import { initQiscus } from "../../utils/chat";

const contactConfig = {
  suggestion: {
    title: "Suggested",
    paddingTop: WP4,
    buttonRight: false,
    dummy: [1],
    key: "suggestions",
    withEmpty: false,
  },
  friend: {
    title: "Daftar teman",
    paddingTop: WP205,
    buttonRight: true,
    dummy: [1, 2, 3, 4, 5],
    key: "friends",
    withEmpty: true,
  },
};

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam("onRefresh", () => {}),
  fromScreen: getParam("fromScreen", "ExploreScreen"),
});

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapDispatchToProps = {
  setUnreadMessage: (unreadMessageCount) => ({
    type: GET_MESSAGE_UNREAD,
    response: unreadMessageCount,
  }),
};

class ContactScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qiscus: {},
      suggestions: [],
      friends: [],
      suggestionsLoading: true,
      friendsLoading: true,
      id_userContact: "",
    };
    this._addGroupSection = this._addGroupSection.bind(this);
  }

  componentDidMount() {
    this._settingUpQiscus();
    this._getAdminAccount();
    this._getFriends();
  }

  _onPullDownToRefresh = async () => {
    this.setState(
      {
        suggestions: [],
        friends: [],
        suggestionsLoading: true,
        friendsLoading: true,
      },
      async () => {
        await this._getAdminAccount();
        await this._getFriends();
      }
    );
  };

  _settingUpQiscus = async () => {
    const { userData } = this.props;

    let qiscus = await initQiscus(userData);
    await this.setState({
      qiscus,
    });
  };

  _getAdminAccount = async () => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props;
    const { id_userContact } = this.state;
    // const params = { id_user: "960" };
    try {
      const dataResponse = await dispatch(
        getSuggestedContact,
        { id_user },
        noop,
        true,
        false
      );
      if (dataResponse.code == 200) {
        this.setState({
          suggestions: dataResponse.result,
          suggestionsLoading: false,
        });
      } else {
        this.setState({ suggestionsLoading: false });
      }
    } catch (e) {
      this.setState({ suggestionsLoading: false });
    }
  };

  _getFriends = async () => {
    const {
      userData: { id_user },
      dispatch,
    } = this.props;
    const params = {
      followed_by: id_user,
      type: "0",
      ...params,
    };
    try {
      const dataResponse = await dispatch(
        getProfileFollowing,
        params,
        noop,
        true,
        false
      );
      if (dataResponse.code == 200) {
        this.setState({ friends: dataResponse.result, friendsLoading: false });
      } else {
        this.setState({ friendsLoading: false });
      }
    } catch (e) {
      this.setState({ friendsLoading: false });
    }
  };

  _onPressItem = async (
    with_id_user,
    user_fullname,
    user_profile_picture,
    id_groupmessage
  ) => {
    const { userData, navigateTo, onRefresh, navigation, fromScreen } =
      this.props;
    const { id_userContact } = this.state;

    // const chatRoomDetail = await initiateRoom(userData, user);
    const navigateBackCallback = async () => {
      resetAction(
        navigation,
        1,
        [
          NavigationActions.navigate({
            routeName: "ChatScreen",
            params: { fromScreen },
          }),
        ],
        fromScreen
      );
    };

    await navigateTo("ChatRoomScreen", {
      // ...chatRoomDetail,
      with_id_user,
      user_fullname,
      user_profile_picture,
      id_groupmessage,
      onRefresh,
      navigateBackCallback,
    });
  };

  _renderSkeletonItem = () => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-start",
          marginHorizontal: WP4,
          borderBottomWidth: 1,
          borderBottomColor: PALE_GREY_THREE,
        }}
      >
        <SkeletonContent
          containerStyle={{ flexGrow: 0 }}
          layout={[ChatSkeletonConfig.layouts.avatar]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
        <SkeletonContent
          containerStyle={{ flexGrow: 1, paddingVertical: WP4 }}
          layout={[
            ChatSkeletonConfig.layouts.roomName,
            ChatSkeletonConfig.layouts.lastComment,
          ]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
      </View>
    );
  };

  _renderSkeleton = (dummy) => {
    return <View>{dummy.map(() => this._renderSkeletonItem())}</View>;
  };

  _addGroupSection = () => {
    const { navigateTo } = this.props;
    return (
      <TouchableOpacity
        activeOpacity={0.75}
        style={{
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          paddingHorizontal: WP4,
          paddingVertical: WP6,
          borderBottomWidth: 1,
          borderBottomColor: PALE_BLUE,
        }}
        onPress={() => {
          navigateTo("AddMemberGroupChatScreen", {
            onRefresh: this.props.onRefresh,
          });
        }}
      >
        <View
          style={{
            width: WP10,
            height: WP10,
            borderRadius: WP10 / 2,
            backgroundColor: REDDISH,
            justifyContent: "center",
            alignItems: "center",
            marginRight: WP3,
          }}
        >
          <Icon
            centered
            name="account-multiple-plus"
            color={WHITE}
            type="MaterialCommunityIcons"
            size="slight"
          />
        </View>
        <View style={{ flex: 1 }}>
          <Text
            type={"Circular"}
            size={"mini"}
            color={GUN_METAL}
            weight={500}
            style={{ marginBottom: WP05 }}
          >
            Add Group Chat
          </Text>
          <Text
            type={"Circular"}
            size={"xmini"}
            color={SHIP_GREY_CALM}
            weight={300}
            style={{ lineHeight: WP5 }}
          >
            Ajak temanmu untuk diskusi bersama
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  _suggestionSection = (type) => {
    const { fromScreen, navigateTo } = this.props;
    const config = contactConfig[type];
    const loading = this.state[`${config.key}Loading`];
    const dummy = config.dummy;
    const data = loading ? dummy : this.state[`${config.key}`];
    const addFriend = () =>
      navigateTo("FindFriendScreen", { isMessage: true, fromScreen });
    return (
      <View style={{ paddingTop: config.paddingTop }}>
        {data.length > 0 && (
          <View
            style={{
              paddingHorizontal: WP4,
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <Text
              type={"Circular"}
              size={"xmini"}
              color={SHIP_GREY_CALM}
              weight={400}
            >
              {config.title}
            </Text>
            {config.buttonRight && (
              <TouchableOpacity onPress={addFriend} activeOpacity={0.75}>
                <Text
                  type={"Circular"}
                  size={"xmini"}
                  color={REDDISH}
                  weight={400}
                >
                  Cari Teman
                </Text>
              </TouchableOpacity>
            )}
          </View>
        )}
        {loading ? (
          this._renderSkeleton(dummy)
        ) : data.length > 0 ? (
          data.map((item, index) => (
            <FollowItem
              imageSize="xsmall"
              isMine={false}
              key={
                config.key === "suggestions"
                  ? `${index}${item.id}`
                  : `${index}${item.id_user}`
              }
              user={item}
              onPressItem={() => {
                this._onPressItem(
                  item.id_user,
                  item.full_name,
                  item.profile_picture,
                  item.id_groupmessage
                );
              }}
            />
          ))
        ) : config.withEmpty ? (
          <View style={{ width: WP100, height: WP100 }}>
            <EmptyV3
              backgroundColor={WHITE}
              title="Tambahkan teman"
              message={"Temukan  pengguna lainnya di\nEventeer"}
              icon={
                <Image
                  imageStyle={{
                    width: WP14,
                    height: WP14,
                    marginBottom: WP4,
                  }}
                  source={require("sf-assets/icons/v3/icBgGroupAdd.png")}
                />
              }
              actions={
                <TouchableOpacity
                  onPress={addFriend}
                  style={{
                    marginTop: WP4,
                    paddingHorizontal: WP4,
                    paddingVertical: WP105,
                    borderRadius: 6,
                    backgroundColor: REDDISH,
                  }}
                >
                  <Text type="Circular" size="xmini" weight={400} color={WHITE}>
                    Cari Teman
                  </Text>
                </TouchableOpacity>
              }
            />
          </View>
        ) : null}
      </View>
    );
  };

  render() {
    const { navigateBack } = this.props;
    return (
      <Container
        scrollable
        scrollBackgroundColor={WHITE}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={navigateBack}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="Contact"
              centered
            />
            <LinearGradient
              colors={SHADOW_GRADIENT}
              style={headerStyle.headerShadow}
            />
          </View>
        )}
        onPullDownToRefresh={this._onPullDownToRefresh}
        outsideScrollContentTop={this._addGroupSection}
      >
        <View style={{ flex: 1 }}>
          {this._suggestionSection("suggestion")}
          {this._suggestionSection("friend")}
        </View>
      </Container>
    );
  }
}

ContactScreen.propTypes = {};

ContactScreen.defaultProps = {};

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ContactScreen),
  mapFromNavigationParam
);
