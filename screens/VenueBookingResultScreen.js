import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, TouchableOpacity, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { GREY, ORANGE, ORANGE_BRIGHT, PINK_RED, WHITE } from '../constants/Colors'
import Header from '../components/Header'
import Icon from '../components/Icon'
import Text from '../components/Text'
import Container from '../components/Container'
import _enhancedNavigation from '../navigation/_enhancedNavigation'
import { HP2, HP3, HP5, WP100, WP2, WP4, WP40, WP5, WP6 } from '../constants/Sizes'
import Image from '../components/Image'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  status: getParam('status', 'FAILED')
})

const STYLE = StyleSheet.create({
  image: {
    paddingVertical: HP3,
  },
  title: {
    marginBottom: HP2,
  },
  message: {
    marginBottom: HP3,
  },
  buttonAction: {
    borderRadius: WP100,
    borderWidth: 2,
    borderColor: ORANGE,
    paddingHorizontal: WP5,
    paddingVertical: WP2,
    marginVertical: WP4,
  }
})

class VenueBookingResultScreen extends Component {

  _bookingConstant = () => {
    const {
      navigateBack,
      resetNavigation
    } = this.props

    return {
      SUCCESS: {
        IMAGE: require('../assets/images/bookingSuccess.png'),
        TITLE: 'Booking Success!',
        MESSAGE: 'We have received your booking. Please check your email for details. We will e contacting you soon to follow up your order',
        BUTTON_LABEL: 'Back to Home',
        BUTTON_ACTION: () => {
          resetNavigation('ExploreScreen')
          this.props.navigation.navigate('TimelineScreen')
        },
      },
      FAILED: {
        IMAGE: require('../assets/images/bookingFailed.png'),
        TITLE: 'Booking Failed!',
        MESSAGE: 'We\'re sorry the date you choose s already booked by someone else',
        BUTTON_LABEL: 'Choose another date',
        BUTTON_ACTION: () => { navigateBack() },
      }
    } }

  render() {
    const {
      status,
      isLoading,
      navigateBack,
    } = this.props

    const bookingConstantResult = this._bookingConstant()

    return (
      <Container
        isLoading={isLoading}
        colors={[WHITE, WHITE]}
        type='horizontal'
        renderHeader={() => (
          <Header
            colors={[PINK_RED, ORANGE_BRIGHT]}
            style={{ justifyContent: 'flex-start' }}
          >
            <Icon onPress={() => { navigateBack() }} style={{ alignSelf: 'center', fontWeight: 600 }} size='large' color={WHITE} name='left'/>
            <Text weight={500} size='medium' color={WHITE}>Venue Booking</Text>
          </Header>
        )}
      >
        <View style={{
          flex: 1,
          alignItems: 'center',
          paddingTop: HP5,
          paddingHorizontal: WP6,
        }}
        >
          <View style={[STYLE.image]}>
            <Image source={bookingConstantResult[status].IMAGE} imageStyle={{ height: WP40, aspectRatio: 1 }}/>
          </View>

          <Text centered color={ORANGE} size='huge' weight={500} style={[STYLE.title]}>{bookingConstantResult[status].TITLE}</Text>
          <Text centered color={GREY} style={[STYLE.message]}>{bookingConstantResult[status].MESSAGE}</Text>
          <TouchableOpacity onPress={bookingConstantResult[status].BUTTON_ACTION} style={[STYLE.buttonAction]}>
            <Text centered color={ORANGE} weight={400} size='slight'>{bookingConstantResult[status].BUTTON_LABEL}</Text>
          </TouchableOpacity>
        </View>
      </Container>
    )
  }
}

VenueBookingResultScreen.propTypes = {
  userData: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  navigateBack: PropTypes.func,
  navigateTo: PropTypes.func,
}

VenueBookingResultScreen.defaultProps = {
  userData: PropTypes.objectOf(PropTypes.any),
  isLoading: PropTypes.bool,
  navigateBack: PropTypes.func,
  navigateTo: PropTypes.func,
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(VenueBookingResultScreen),
  mapFromNavigationParam
)
