import React, { Component } from 'react'
import { View, BackHandler, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Container from 'sf-components/Container'
import Loader from 'sf-components/Loader'
import Image from 'sf-components/Image'
import HeaderNormal from 'sf-components/HeaderNormal'
import style from 'sf-styles/components/ProfileShareComponent'
import FeedItemGeneralMultiple from 'sf-components/feed/FeedItemGeneralMultiple'
import Avatar from 'sf-components/Avatar'
import ButtonV2 from 'sf-components/ButtonV2'
import Badge from 'sf-components/Badge'
import {
  setProgressComponent,
  setProgressComplete,
} from 'sf-services/helper/actionDispatcher'
import {
  GREY_CALM,
  WHITE,
  REDDISH,
  REDDISH_DISABLED,
  GUN_METAL,
  SHADOW_GRADIENT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  PALE_GREY,
} from 'sf-constants/Colors'
import Text from 'sf-components/Text'
import _enhancedNavigation from 'sf-navigation/_enhancedNavigation'
import {
  WP1,
  WP2,
  WP3,
  WP4,
  WP5,
  WP8,
  HP2,
  HP5,
  HP25,
  WP15,
  WP20,
  WP100,
} from 'sf-constants/Sizes'
import { NEED_RELOAD_FEEDS } from 'sf-services/timeline/actionTypes'
import { ProfileComponent, ExploreComponent } from 'sf-components/share'
import { postShareTimeline, getFeedDetail } from 'sf-actions/api'
import { get } from 'lodash-es'
import AutogrowInput from 'react-native-autogrow-input'
import produce from 'immer'
import { LinearGradient } from 'expo-linear-gradient'
import { HEADER } from 'sf-constants/Styles'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'

const mapStateToProps = ({ auth, song: { playback, soundObject } }) => ({
  userData: auth.user,
  playback,
  soundObject,
})

const styles = StyleSheet.create({
  container: {
    padding: WP5,
  },
  owner: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  postInput: {
    fontFamily: 'CircularBook',
    textAlignVertical: 'top',
    marginTop: WP5,
    marginBottom: WP2,
    lineHeight: WP5,
    color: GUN_METAL,
  },
  actionBarLeft: {
    width: WP15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 0,
    paddingVertical: WP3,
    marginRight: WP5,
  },
  actionBarRight: {
    width: WP20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 0,
    paddingVertical: WP3,
  },
  progressAnim: {
    height: '100%',
    paddingHorizontal: WP5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  badge: {
    marginRight: WP3,
  },
  modalBack: {
    width: WP100 - WP8,
  },
  modalBackSubtitle: {
    lineHeight: WP5,
  },
  modalBackSecondaryButton: {
    backgroundColor: PALE_GREY,
    marginTop: WP1,
    borderRadius: 8,
    paddingVertical: WP3,
  },
})

const mapFromNavigationParam = (getParam) => ({
  id: getParam('id', null),
  id_artist: getParam('id_artist', null),
  id_episode: getParam('id_episode', null),
  title: getParam('title', 'Buat Post'),
  type: getParam('type', null),
  is1000Startup: getParam('is1000Startup', false),
})

class ShareScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      description: '',
      feed: null,
      postEnabled: true,
      backModal: false,
    }
    this._onShare = this._onShare.bind(this)
    this._attachBackAction = this._attachBackAction.bind(this)
    this._releaseBackAction = this._releaseBackAction.bind(this)
    this._getData = this._getData.bind(this)
    this._backHandler = this._backHandler.bind(this)
    this._toggleBackModal = this._toggleBackModal.bind(this)
  }

  _attachBackAction() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this._backHandler,
    )
  }

  _releaseBackAction() {
    try {
      const { soundObject, playback } = this.props
      playback.isLoaded && soundObject.pauseAsync()
      this.backHandler.remove()
    } catch (err) {
      /*silent is gold*/
    }
  }

  componentDidMount() {
    const { userData, navigateTo, navigateBack } = this.props
    if (!userData.id_user) {
      navigateBack()
      navigateTo('AuthNavigator')
    } else {
      this.focusListener = this.props.navigation.addListener(
        'focus',
        this._attachBackAction,
      )
      this.blurListener = this.props.navigation.addListener(
        'blur',
        this._releaseBackAction,
      )
      this.props.type === 'timeline' && this._getData()
    }
  }

  async _getData() {
    const {
      id,
      dispatch,
      userData: { id_user },
    } = this.props
    let feed = await dispatch(getFeedDetail, {
      id_timeline: id,
      id_user,
    })
    const topic_name = feed.topic_name
    if (feed.data_journey && feed.data_journey.type === 'post')
      feed = feed.data_journey
    feed.topics = topic_name.split(',')
    this.setState(
      produce((draft) => {
        draft.feed = feed
      }),
    )
  }

  async componentWillUnmount() {
    try {
      this.backHandler.remove()
    } catch (err) {
      /*silent is gold*/
    }
  }

  _onShare() {
    const {
      id,
      id_episode,
      type,
      setNeedReloadStatus,
      userData: { id_user },
      navigateTo,
      popToTop,
      setProgressComplete,
      setProgressComponent,
      title,
      is1000Startup,
      navigateBack,
    } = this.props
    const { description, feed } = this.state

    let shareData = {
      id: type === 'timeline' ? feed.id_journey : id,
      related_to: {
        profile: 'id_user',
        timeline: 'id_journey',
        song: 'id_journey',
        explore: 'id_ads',
      }[type],
      id_user,
      description,
      title,
    }
    if (id_episode) shareData.id_episode = id_episode
    try {
      setProgressComplete(false)
      setProgressComponent(
        <View style={styles.progressAnim}>
          <Text weight={400} color={SHIP_GREY_CALM} size={'mini'} type={'Circular'}>
            Membagikan...
          </Text>
        </View>,
      )
      postShareTimeline(shareData).then(({ data }) => {
        setProgressComplete(true)
        setProgressComponent(
          <View style={styles.progressAnim}>
            <Text
              weight={400}
              color={SHIP_GREY_CALM}
              size={'mini'}
              type={'Circular'}
            >
              {'Berhasil membagikan'}
            </Text>
            <Image
              size={'badge'}
              source={require('sf-assets/icons/v3/tickReddishSemiTransparent.png')}
            />
          </View>,
        )
        setNeedReloadStatus(true)
      })
      if (is1000Startup) {
        navigateBack()
      } else {
        popToTop()
        navigateTo('HomeStack')
      }
    } finally {
      // Keep silent
    }
  }

  _rightComponent = () => {
    const { postEnabled } = this.state
    return (
      <ButtonV2
        onPress={this._onShare}
        disabled={!postEnabled}
        style={styles.actionBarRight}
        text={'Share'}
        borderColor={WHITE}
        textColor={postEnabled ? REDDISH : REDDISH_DISABLED}
      />
    )
  };

  _onTyping = (value) => {
    this.setState(
      produce((draft) => {
        draft.description = value
      }),
    )
  };

  _renderHeader = () => {
    const { postEnabled } = this.state
    return (
      <View key={postEnabled ? 'header-enabled' : 'header-disabled'}>
        <HeaderNormal
          noRightPadding
          iconStyle={styles.actionBarLeft}
          iconLeftOnPress={this._backHandler}
          style={style.headerNormal}
          centered
          rightComponent={this._rightComponent}
          textType={'Circular'}
          textSize={'slight'}
          text={this.props.title}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
      </View>
    )
  };

  _toggleBackModal = () => {
    this.setState(
      produce((draft) => {
        draft.backModal = !draft.backModal
      }),
    )
  };

  _backHandler = () => {
    this._toggleBackModal()
    return true
  };

  _renderTopic = (topic) => (
    <View style={styles.badge}>
      <Badge color={PALE_GREY} textColor={SHIP_GREY}>
        {topic}
      </Badge>
    </View>
  );

  render() {
    const {
      navigation,
      id,
      id_artist,
      type,
      title,
      userData,
      id_episode,
    } = this.props
    const { loading, backModal, feed } = this.state
    return (
      <Container
        isLoading={loading}
        isReady={true}
        renderHeader={this._renderHeader}
        scrollable
      >
        <View style={styles.container}>
          <View style={styles.owner}>
            <View style={{ paddingTop: 6, paddingRight: WP4 }}>
              <Avatar size={'small'} image={userData.profile_picture} />
            </View>
            <Text size={'slight'} weight={500} color={GUN_METAL} type={'Circular'}>
              {get(userData, 'full_name')}
            </Text>
          </View>
          <AutogrowInput
            value={this.state.description}
            autoFocus={!__DEV__}
            onChangeText={this._onTyping}
            spellCheck={false}
            autoCorrect={false}
            style={styles.postInput}
            placeholderTextColor={SHIP_GREY_CALM}
            placeholder={'Berikan komentar...'}
            defaultHeight={WP5}
          />
          <View style={{ paddingTop: HP2 }}>
            {(type === 'profile' || type === 'song') && (
              <ProfileComponent
                showTopics
                loaderHeight={
                  title.toLowerCase().indexOf('picture') >= 0 ? HP25 : null
                }
                navigation={navigation}
                id={id}
                id_artist={id_artist}
              />
            )}
            {type === 'explore' && (
              <ExploreComponent
                showTopics
                navigation={navigation}
                id={id}
                id_episode={id_episode}
              />
            )}
            {type === 'timeline' && (
              <View
                style={
                  !feed
                    ? {
                      borderWidth: 1,
                      borderColor: GREY_CALM,
                      borderRadius: WP2,
                      paddingVertical: HP5,
                    }
                    : null
                }
              >
                <Loader size={'mini'} isLoading={!feed}>
                  {feed && (
                    <View>
                      <FeedItemGeneralMultiple
                        isSharePreview
                        idTimeline={this.props.id}
                        navigateTo={this.props.navigateTo}
                        navigation={this.props.navigation}
                        parentTimeline={feed}
                        feed={feed}
                      />
                    </View>
                  )}
                </Loader>
              </View>
            )}
            {type === 'timeline' && (
              <View style={style.topicWrapper}>
                {(feed ? feed.topics : ['···']).map(this._renderTopic)}
              </View>
            )}
          </View>
        </View>
        <ModalMessageView
          style={styles.modalBack}
          subtitleStyle={styles.modalBackSubtitle}
          buttonSecondaryStyle={styles.modalBackSecondaryButton}
          toggleModal={this._toggleBackModal}
          isVisible={backModal}
          title={title}
          titleType='Circular'
          titleSize={'small'}
          titleColor={GUN_METAL}
          subtitle={`Post yang akan kamu bagikan belum selesai. Batalkan untuk ${title.toLowerCase()}?`}
          subtitleType='Circular'
          subtitleSize={'xmini'}
          subtitleWeight={400}
          subtitleColor={SHIP_GREY_CALM}
          image={null}
          buttonPrimaryText={'Lanjutkan'}
          buttonPrimaryTextType='Circular'
          buttonPrimaryTextWeight={400}
          buttonPrimaryBgColor={REDDISH}
          buttonSecondaryText={'Batalkan'}
          buttonSecondaryTextType='Circular'
          buttonSecondaryTextWeight={400}
          buttonSecondaryTextColor={SHIP_GREY_CALM}
          buttonSecondaryAction={this.props.navigateBack}
        />
      </Container>
    )
  }
}

ShareScreen.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  id_episode: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  type: PropTypes.oneOf(['explore', 'profile', 'timeline', 'song']).isRequired,
  title: PropTypes.string.isRequired,
}

ShareScreen.defaultProps = {}

const mapDispatchToProps = {
  setNeedReloadStatus: (response) => ({
    type: NEED_RELOAD_FEEDS,
    response,
  }),
  setProgressComponent,
  setProgressComplete,
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ShareScreen),
  mapFromNavigationParam,
)
