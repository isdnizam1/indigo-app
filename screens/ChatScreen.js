import React, { Component, Fragment } from "react";
import { FlatList, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { isEmpty, noop, orderBy } from "lodash-es";
import SkeletonContent from "react-native-skeleton-content";
import { LinearGradient } from "expo-linear-gradient";
import Touchable from "sf-components/Touchable";
import Icon from "sf-components/Icon";
import Container from "sf-components/Container";
import EmptyV3 from "sf-components/EmptyV3";
import Text from "sf-components/Text";
import Modal from "sf-components/Modal";
import HeaderNormal from "sf-components/HeaderNormal";
import ChatSkeletonConfig from "sf-components/chat/ChatSkeletonConfig";
import _enhancedNavigation from "../navigation/_enhancedNavigation";
import PusherClient from "../utils/clientPusher";
import { GET_MESSAGE_UNREAD } from "../services/message/actionTypes";
import {
  getCommentLastId,
  getCommentList,
  setChatRoomList,
  setLocalStorage,
} from "../utils/storage";
import {
  HP12,
  HP5,
  WP105,
  WP12,
  WP14,
  WP2,
  WP4,
  WP5,
} from "../constants/Sizes";
import { deleteChat, getAccountTypeById, getListMessage } from "../actions/api";
import { fetchMessage, getRoomAvatar } from "../utils/chat";
import { messageDispatcher } from "../services/message";
import { imageBase64ToUrl, stringBase64ToContent } from "../utils/helper";
import bugsnagClient from "../utils/bugsnag";
import {
  ORANGE_BRIGHT,
  PALE_GREY_THREE,
  PALE_LIGHT_BLUE_TWO,
  REDDISH,
  SHADOW_GRADIENT,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
} from "../constants/Colors";
import SoundfrenExploreOptions from "../components/explore/SoundfrenExploreOptions";
import { FONTS } from "../constants/Fonts";
import { HEADER } from "../constants/Styles";
import headerStyle from "./profile/v2/ProfileScreen/style";
import ChatRoomItem from "../components/chat/ChatRoomItem";
import Spacer from "../components/Spacer";
import { Empty } from "../components";
import ConfirmationPopup from "../components/popUp/ConfirmationPopUp";
import { MESSAGE_POP_UP } from "./chat/Constants";

const optionsMenu = (props) => ({
  withPromoteUser: true,
  menus: [
    {
      type: "menu",
      image: require("sf-assets/icons/badgeSoundfrenPremium.png"),
      imageStyle: {},
      name: "Premium",
      textStyle: { color: ORANGE_BRIGHT, fontFamily: FONTS.Circular["500"] },
      onPress: () => {},
      joinButton: false,
      isPremiumMenu: true,
    },
    {
      type: "menu",
      image: require("sf-assets/icons/mdi_chat.png"),
      imageStyle: {},
      name: "Feedback & Report",
      textStyle: {},
      onPress: () => {
        props.onClose();
        props.navigateTo("FeedbackScreen");
      },
    },
    {
      type: "separator",
    },
    {
      type: "menu",
      image: require("sf-assets/icons/close.png"),
      imageStyle: {},
      name: "Tutup",
      textStyle: {},
      onPress: props.onClose,
    },
  ],
});

const mapStateToProps = ({ auth, message }) => ({
  userData: auth.user,
  rooms: message.message,
});

const mapDispatchToProps = {
  setUnreadMessage: (unreadMessageCount) => ({
    type: GET_MESSAGE_UNREAD,
    response: unreadMessageCount,
  }),
  setMessage: messageDispatcher.setMessage,
};

const mapFromNavigationParam = (getParam) => ({
  fromScreen: getParam("fromScreen", "ExploreScreen"),
});

class ChatScreen extends Component {
  static navigationOptions = () => ({
    gesturesEnabled: true,
  });

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isReady: false,
      accountTypes: [],
      page: 1,
      limit: 50,
      username: null,
      idMessage: null,
      message: null,
      chatRoomList: null,
      emptyRoom: true,
      modalVisible: false,
      selectedChat: null,
    };
  }

  componentDidMount = async () => {
    const { userData, setMessage, rooms, navigateBack, navigateTo } =
      this.props;
    this._getListMessage();

    this.focusListener = this.props.navigation.addListener("focus", () => {
      this._getListMessage();
    });

    if (!userData.id_user) {
      navigateBack();
      navigateTo("AuthNavigator");
    } else {
      this.props.setUnreadMessage(0);

      try {
        await setLocalStorage("unreadMessageCount", "0");
      } catch (e) {
        //
      }

      // Promise.all(
      //   chatRoomList.map(async (item) => {
      //     if (
      //       item.avatar.includes("base64") ||
      //       (item.options && item.options.includes("base64"))
      //     ) {
      //       const fileData = await stringBase64ToContent(item.avatar);
      //       const url = await imageBase64ToUrl(userData.id_user, fileData);
      //       item.avatar_url = url;
      //       item.avatar = url;
      //       if (item.options && item.options.includes("base64")) {
      //         item.options = {};
      //       }

      //     }
      //   })
      // );

      // PusherClient(userData.id_user).bind("new_message", this._getRoomList);
    }
  };

  componentWillUnmount = async () => {
    const { userData } = this.props;
    // PusherClient(userData.id_user).unbind("new_message", this._getRoomList);
  };

  filteredRoomList = (roomList) => {
    function compare(a, b) {
      if (
        a.last_comment_message_created_at < b.last_comment_message_created_at
      ) {
        return 1;
      }
      if (
        a.last_comment_message_created_at > b.last_comment_message_created_at
      ) {
        return -1;
      }
      return 0;
    }

    const rooms = roomList.filter(
      (room) => room.last_comment_id !== 0 || room.room_type === "group"
    );
    return rooms.sort(compare);
  };

  // _getRoomList = async () => {
  //   const { userData, setMessage } = this.props;

  //   const { page, limit } = this.state;

  //   try {
  //     const userIds = [];
  //     let accountTypes = [];
  //     roomList.map((room) => {
  //       if (room.room_type === "single") {
  //         room.participants.map((user) => {
  //           if (user.email !== userData.id_user) {
  //             userIds.push(user.email);
  //           }
  //         });
  //       }
  //     });

  //     if (!isEmpty(userIds)) {
  //       accountTypes = await this.props.dispatch(getAccountTypeById, {
  //         id_user: userIds,
  //       });
  //     }

  //     const filteredRooms = this.filteredRoomList(roomList);
  //     setMessage(filteredRooms);

  //     await Promise.all(
  //       filteredRooms.map(async (item) => {
  //         const lastCommentId = await getCommentLastId(item.id);
  //         if (lastCommentId && lastCommentId != item.last_comment_id) {
  //           fetchMessage(qiscus, item.id);
  //         }
  //       })
  //     );

  //     setChatRoomList(filteredRooms);

  //     this.setState({
  //       accountTypes,
  //       isReady: true,
  //     });
  //   } catch (e) {
  //     this.setState({
  //       isReady: true,
  //     });
  //   }
  // };

  _onRefresh = async () => {
    // this._getRoomList();
  };

  _onChange = (key) => async (value) => {
    this.setState({ [key]: value, isChanged: true });
    if (this.state.chatRoomList.length === 0) {
      this.setState({ emptyRoom: true });
    }
  };

  _onDelete = async (isLoading = true) => {
    const { rooms, setMessage, dispatch } = this.props;
    const { selectedChat, chatRoomList } = this.state;
    try {
      this.setState({ isLoading: true });
      const dataResponse = await dispatch(
        deleteChat,
        { id_groupmessage: selectedChat },
        noop,
        true,
        isLoading
      );
      if (dataResponse.code === 200) {
        const roomList = chatRoomList.filter(
          (room) => room.id_groupmessage !== selectedChat
        );
        setChatRoomList(roomList);
        this._onChange("chatRoomList")(roomList);
        if (chatRoomList.length === 0) {
          this.setState({ emptyRoom: true });
        }
      }
      this.setState({ isLoading: false });
    } catch (e) {
      bugsnagClient.notify(e);
    }
  };

  _renderDeleteChat = () => {
    const { userData } = this.props;
    const { modalVisible } = this.state;
    return (
      <ConfirmationPopup
        isVisible={modalVisible}
        onCancel={() => {
          this.setState({ modalVisible: false });
        }}
        onConfirm={() => {
          this._onDelete();
          this.setState({ modalVisible: false });
        }}
        template={MESSAGE_POP_UP.DELETE_CHAT}
      />
    );
  };

  _getListMessage = async (isLoading = true) => {
    const {
      userData: { id_user },
      dispatch,
    } = this.props;
    const params = { id_user: id_user };
    const { chatRoomList } = this.state;
    // console.log('chatRoomList',chatRoomList);
    const dataResponse = await dispatch(
      getListMessage,
      params,
      noop,
      true,
      isLoading
    );
    if (Object.keys(dataResponse.result).length === 0) {
      this.setState({
        isReady: true,
        emptyRoom: true,
        chatRoomList: dataResponse.result,
      });
    } else {
      this.setState({
        isReady: true,
        emptyRoom: false,
        chatRoomList: dataResponse.result,
      });
    }
  };

  _onUpdateRoom = (room) => {
    if (!room) return;
    const { rooms, setMessage } = this.props;
    const result = rooms.map((item) => {
      if (item.id === room.id) {
        return room;
      }
      return item;
    });
    const filteredRooms = this.filteredRoomList(result);
    setMessage(filteredRooms);

    setChatRoomList(filteredRooms);
  };
  _onClickItem = async (room, receiver, accountType) => {
    const { navigateTo } = this.props;
    const item = await getCommentList(room.id);
    navigateTo("ChatRoomScreen", {
      comments: item,
      room,

      receiver,
      onUpdateRoom: this._onUpdateRoom,
      onRefresh: this._onRefresh,
      accountType,
    });
  };

  _renderSkeletonItem = () => {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-start",
          marginHorizontal: WP4,
          borderBottomWidth: 1,
          borderBottomColor: PALE_GREY_THREE,
        }}
      >
        <SkeletonContent
          containerStyle={{ flexGrow: 0 }}
          layout={[ChatSkeletonConfig.layouts.avatar]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
        <SkeletonContent
          containerStyle={{ flexGrow: 1, paddingVertical: WP4 }}
          layout={[
            ChatSkeletonConfig.layouts.roomName,
            ChatSkeletonConfig.layouts.lastComment,
          ]}
          isLoading={true}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
      </View>
    );
  };

  _renderSkeleton = () => {
    const dummy = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    return <View>{dummy.map(() => this._renderSkeletonItem())}</View>;
  };

  _renderContent = () => {
    const { rooms, navigateTo, fromScreen, userData } = this.props;
    const { emptyRoom, chatRoomList, isLoading, isReady } = this.state;
    const chatList = orderBy(chatRoomList, ["message_created"], ["desc"]);
    return emptyRoom ? (
      <EmptyV3
        title="Belum ada percakapan"
        message={"Ayo mulai percakapan dengan\nteman-temanmu sekarang"}
        icon={
          <Icon
            centered
            style={{
              width: WP14,
              height: WP14,
              borderRadius: WP14 / 2,
              marginBottom: WP2,
            }}
            padding={WP4}
            type="MaterialCommunityIcons"
            name="message-text"
            size={"huge"}
            color={WHITE}
            background="pale-light-circle"
            backgroundColor={PALE_LIGHT_BLUE_TWO}
          />
        }
        actions={
          <TouchableOpacity
            onPress={() =>
              navigateTo("ContactScreen", {
                fromScreen,
                onRefresh: this._onRefresh,
              })
            }
            style={{
              marginTop: WP4,
              paddingHorizontal: WP4,
              paddingVertical: WP105,
              borderRadius: 6,
              backgroundColor: REDDISH,
            }}
          >
            <Text type="Circular" size="xmini" weight={400} color={WHITE}>
              Mulai percakapan
            </Text>
          </TouchableOpacity>
        }
      />
    ) : (
      <View>
        <FlatList
          bounces={false}
          bouncesZoom={false}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: HP5 }}
          style={{ flexGrow: 0 }}
          data={chatList}
          keyExtractor={(item, index) => item.id_groupmessage}
          renderItem={({ item, index }) => (
            <ChatRoomItem
              onPressItem={() => {
                navigateTo("ChatRoomScreen", {
                  user_fullname: item.full_name,
                  user_profile_picture: item.avatar,
                  id_groupmessage: item.id_groupmessage,
                  with_id_user: item.id_user,
                  navigateBackOnDone: true,
                  type: item.type,
                });
              }}
              onLongPress={() => {
                this.setState({
                  selectedChat: item.id_groupmessage,
                  modalVisible: true,
                });
              }}
              key={`${index}${item.id_groupmessage}`}
              user={item}
              userData={userData}
              timeStamp={item.updated_at}
            />
          )}
          ListEmptyComponent={
            <View>
              <Spacer size={HP12} />
              <Empty />
            </View>
          }
        />
        {this._renderDeleteChat()}
      </View>
    );
  };

  _renderModalToggler = (toggleModal) => (
    <Touchable onPress={toggleModal} style={HEADER.rightIcon}>
      <Icon
        centered
        size="small"
        color={SHIP_GREY_CALM}
        name="dots-three-horizontal"
        type="Entypo"
      />
    </Touchable>
  );

  render() {
    const { navigateBack, navigateTo, fromScreen } = this.props;

    const { isLoading, isReady } = this.state;

    return (
      <Container
        isLoading={isLoading}
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={() => this._getListMessage()}
        renderHeader={() => (
          <View>
            <HeaderNormal
              iconLeftOnPress={navigateBack}
              withExtraPadding
              textWeight={400}
              textSize="slight"
              text="Message"
              centered
            />
            <LinearGradient
              colors={SHADOW_GRADIENT}
              style={headerStyle.headerShadow}
            />
          </View>
        )}
        outsideScrollContent={() => (
          <View style={{ position: "absolute", right: WP5, bottom: WP5 }}>
            <TouchableOpacity
              activeOpacity={0.75}
              style={{
                width: WP12,
                height: WP12,
                borderRadius: WP12 / 2,
                backgroundColor: REDDISH,
                justifyContent: "center",
                alignItems: "center",
              }}
              onPress={() =>
                navigateTo("ContactScreen", {
                  fromScreen,
                  onRefresh: this._onRefresh,
                })
              }
            >
              <Icon
                centered
                name="message-text"
                color={WHITE}
                type="MaterialCommunityIcons"
                size="huge"
              />
            </TouchableOpacity>
          </View>
        )}
      >
        {/* {this._renderContent()} */}

        {isReady ? this._renderContent() : this._renderSkeleton()}
      </Container>
    );
  }
}

ChatScreen.propTypes = {};

ChatScreen.defaultProps = {};

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ChatScreen),
  mapFromNavigationParam
);
