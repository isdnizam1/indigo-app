import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEmpty, isNumber } from 'lodash-es'
import { View, TouchableWithoutFeedback, TextInput, FlatList, StyleSheet } from 'react-native'
import { getArtistSpotlight, postLogArtistSpotlight, getLogActivity, postDeleteLogActivity } from '../../actions/api'
import Text from '../../components/Text'
// import ButtonV2 from '../../components/ButtonV2'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import Container from '../../components/Container'
import Icon from '../../components/Icon'
import ButtonV2 from '../../components/ButtonV2'
import Loader from '../../components/Loader'
import { WHITE, SHIP_GREY_CALM, REDDISH, NAVY_DARK, PALE_BLUE } from '../../constants/Colors'
import { WP1, WP2, WP3, WP4 } from '../../constants/Sizes'
import ArtistItem from '../../components/artist_spotlight/ArtistItem'
import InteractionManager from '../../utils/InteractionManager'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = () => ({
})

const style = StyleSheet.create({
  header: { overflow: 'hidden', paddingBottom: 4.5 },
  headerContent: {
    backgroundColor: WHITE,
    elevation: 4.5,
  },
  headerContentInner: {
    paddingHorizontal: WP4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: WP2,
  },
  headerSearch: {
    flex: 1,
    backgroundColor: 'rgba(145, 158, 171, 0.1)',
    borderRadius: 6,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: WP3,
    paddingVertical: WP1,
    marginHorizontal: WP2,
  },
  headerSearchIcon: { marginRight: WP1 },
  headerSearchInput: { flex: 1, fontFamily: 'CircularBook', color: SHIP_GREY_CALM },
  headerSearchClear: {
    paddingVertical: WP1,
    paddingHorizontal: WP2,
  },
  headerSearchCancel: {
    paddingVertical: WP2,
    paddingHorizontal: WP1 + 2,
  },
  artistRow: {
    paddingHorizontal: WP4,
    paddingVertical: WP3
  },
  artistRowRecent: {
    paddingLeft: WP4,
    paddingVertical: WP3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  artistRowDelete: {
    padding: WP4
  },
  resultWrapper: {
    paddingTop: WP1
  },
  result: {
    paddingBottom: WP3,
    marginBottom: WP1,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE
  },
  suggestion: {
    paddingBottom: WP4
  },
  recentHeading: {
    paddingHorizontal: WP4,
    marginTop: WP4,
    marginBottom: WP1
  }
})

const searchLimit = 5

class ArtistSpotlightSearchScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      q: '',
      result: [],
      recents: [],
      suggestions: [],
      showMore: false,
      isFetchMore: false,
      isSearching: false
    }
    this.timeout = null
    this._header = this._header.bind(this)
    this._handleSearch = this._handleSearch.bind(this)
    this._searchResult = this._searchResult.bind(this)
    this._clearSearch = this._clearSearch.bind(this)
    this._visitProfile = this._visitProfile.bind(this)
    this._showMore = this._showMore.bind(this)
    this._getRecents = this._getRecents.bind(this)
    this._getSuggestions = this._getSuggestions.bind(this)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._getRecents)
  }

  _getRecents = () => {
    const { userData: { id_user: id_viewer } } = this.props
    getLogActivity({
      type: 'search_artist',
      id_viewer,
      start: 0,
      limit: 5
    }).then(({ data: { result: recents } }) => this.setState({ recents }))
  }

  _handleSearch = (timeoutMs = 750, loadMore: false) => {
    const { q } = this.state
    if(!loadMore && q.length >= 2) this.setState({
      result: [],
      suggestions: [],
      isSearching: true
    })
    if(isNumber(this.timeout)) clearTimeout(this.timeout)
    q.length >= 2 && setTimeout(() => {
      const { dispatch,
        userData: {
          id_user: id_viewer
        }
      } = this.props
      const { q: search } = this.state
      dispatch(getArtistSpotlight, {
        show: 'search_artist',
        search,
        id_viewer,
        start: this.state.result.length,
        limit: searchLimit
      }).then(this._searchResult)
    }, timeoutMs)
  }

  _clearSearch = () => {
    this.setState({
      q: '',
      result: [],
      suggestions: []
    })
  }

  _getSuggestions = () => {
    const { result } = this.state
    const { dispatch,
      userData: {
        id_user: id_viewer
      }
    } = this.props
    if(isEmpty(result)) {
      if(isNumber(this.timeout)) clearTimeout(this.timeout)
      this.timeout = setTimeout(() => {
        this.setState({
          isSearching: true
        }, () => {
          dispatch(getArtistSpotlight, {
            show: 'search_suggestion',
            id_viewer,
            start: 0,
            limit: searchLimit
          }).then(({ suggestion: suggestions }) => {
            this.setState({
              suggestions,
              isSearching: false
            })
          })
        })
      }, 450)
    }
  }

  _searchResult = ({ artist: result }) => {
    this.setState({
      result: [
        ...this.state.result,
        ...result
      ],
      showMore: result.length >= searchLimit,
      isFetchMore: false,
      isSearching: false
    }, this._getSuggestions)
  }

  _header = () => {
    const { q } = this.state
    const { navigateBack } = this.props
    return (<View style={style.header}>
      <View
        style={style.headerContent}
      >
        <View
          style={style.headerContentInner}
        >
          <Icon
            centered
            onPress={navigateBack}
            background='dark-circle'
            size='large'
            color={SHIP_GREY_CALM}
            name='chevron-left'
            type='Entypo'
          />
          <View
            style={style.headerSearch}
          >
            <Icon
              centered
              background='dark-circle'
              size='large'
              color={SHIP_GREY_CALM}
              name='magnify'
              type='MaterialCommunityIcons'
              style={style.headerSearchIcon}
            />
            <TextInput
              value={q}
              onChangeText={(q) => this.setState({ q }, this._handleSearch)}
              placeholder={'Coba cari Artist'}
              autoFocus
              style={style.headerSearchInput}
            />
            <TouchableWithoutFeedback
              onPress={this._clearSearch}
            >
              <View style={style.headerSearchClear}>
                {!isEmpty(q) && (
                  <Icon
                    centered
                    background='dark-circle'
                    size='mini'
                    color={SHIP_GREY_CALM}
                    name='close'
                    type='MaterialCommunityIcons'
                  />
                )}
              </View>
            </TouchableWithoutFeedback>
          </View>
          {!isEmpty(q) && (
            <Text
              onPress={navigateBack}
              style={style.headerSearchCancel}
              type='Circular'
              size='xmini'
              weight={400}
              color={REDDISH}
            >
                Cancel
            </Text>
          )}
        </View>
      </View>
    </View>)
  };

  _visitProfile = ({ artist, log }) => () => {
    const { navigateTo, dispatch } = this.props
    const { userData: {
      id_user
    } } = this.props
    navigateTo('ProfileScreenNoTab', { idUser: artist.id_user, navigateBackOnDone: true })
    log && dispatch(postLogArtistSpotlight, {
      id_viewer: id_user,
      id_user: artist.id_user
    }).then(this._getRecents)
  }

  _removeRecent = ({ artist: { id_log_activity }, index }) => () => {
    const { recents } = this.state
    const { dispatch } = this.props
    recents.splice(index, 1)
    this.setState({ recents }, () => {
      dispatch(postDeleteLogActivity, {
        id_log_activity
      }).then(this._getRecents)
    })
  }

  _renderSearchResult = ({ item: artist, index }) => {
    const { userData: {
      id_user: myId
    } } = this.props
    return (<TouchableWithoutFeedback onPress={this._visitProfile({ artist, log: true })}>
      <View style={style.artistRow}>
        <ArtistItem myId={myId} ads={artist} loading={false} simpleListItem />
      </View>
    </TouchableWithoutFeedback>)
  }

  _renderRecentSearch = ({ item: artist, index }) => {
    const { userData: {
      id_user: myId
    } } = this.props
    return (<View style={style.artistRowRecent}>
      <TouchableWithoutFeedback onPress={this._visitProfile({ artist, log: false })}>
        <ArtistItem myId={myId} ads={artist} loading={false} simpleListItem />
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={this._removeRecent({
        artist,
        index
      })}
      >
        <View style={style.artistRowDelete}>
          <Icon
            color={SHIP_GREY_CALM}
            size={'xmini'}
            name={'close'}
          />
        </View>
      </TouchableWithoutFeedback>
    </View>)
  }

  _showMore = () => {
    this.setState({
      isFetchMore: true
    }, () => this._handleSearch(100, true))
  }

  render() {
    const { result, recents, suggestions, showMore, isFetchMore, isSearching } = this.state
    return (<Container
      scrollable
      isAvoidingView
      renderHeader={() => this._header()}
            >
      <View style={style.resultWrapper}>
        <Loader size={'mini'} isLoading={isSearching}>
          {isEmpty(result) ? <View /> : <View style={style.result}>
            <FlatList
              data={result}
              renderItem={this._renderSearchResult}
              keyExtractor={(item) => item.id_user}
            />
          </View>}
        </Loader>
      </View>
      {showMore && <View>
        <Loader size={'mini'} isLoading={isFetchMore}>
          <ButtonV2
            onPress={this._showMore} // Modal located at ProfileHeader.js
            color={WHITE}
            textColor={REDDISH}
            textSize={'mini'}
            textWeight={300}
            text={'Tampilkan lainnya'}
          />
        </Loader>
      </View>}
      {!isEmpty(suggestions) && <View style={style.suggestion}>
        <View style={style.recentHeading}>
          <Text
            color={NAVY_DARK}
            type={'Circular'}
            weight={500}
            size={'medium'}
          >
            Saran Pencarian
          </Text>
        </View>
        <FlatList
          data={suggestions}
          renderItem={this._renderSearchResult}
          keyExtractor={(item) => `suggestion-${item.id_user}`}
        />
      </View>}
      {!isEmpty(recents) && <View style={style.recent}>
        <View style={style.recentHeading}>
          <Text
            color={NAVY_DARK}
            type={'Circular'}
            weight={500}
            size={'medium'}
          >
            Pencarian Terakhir
          </Text>
        </View>
        <FlatList
          data={recents}
          renderItem={this._renderRecentSearch}
          keyExtractor={(item) => `recent-${item.id_user}`}
        />
      </View>}
    </Container>)
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(ArtistSpotlightSearchScreen),
  mapFromNavigationParam
)
