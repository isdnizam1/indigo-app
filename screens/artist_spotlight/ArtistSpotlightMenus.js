import { ORANGE_BRIGHT } from '../../constants/Colors'
import { FONTS } from '../../constants/Fonts'

export const ArtistSpotlightMenus = (props) => ({
  withPromoteUser: true,
  menus: [
    {
      type: 'menu',
      image: require('../../assets/icons/badgeSoundfrenPremium.png'),
      imageStyle: {},
      name: 'Premium',
      textStyle: { color: ORANGE_BRIGHT, fontFamily: FONTS.Circular['500'] },
      onPress: () => {},
      joinButton: false,
      isPremiumMenu: true
    },
    {
      type: 'menu',
      image: require('../../assets/icons/mdi_chat.png'),
      imageStyle: {},
      name: 'Feedback & Report',
      textStyle: {},
      onPress: () => {
        props.onClose()
        props.navigateTo('FeedbackScreen')
      }
    },
    {
      type: 'separator'
    },
    {
      type: 'menu',
      image: require('../../assets/icons/close.png'),
      imageStyle: {},
      name: 'Tutup',
      textStyle: {},
      onPress: props.onClose
    },
  ]
})

export default ArtistSpotlightMenus
