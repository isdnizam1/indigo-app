import React, { Component, Fragment } from 'react'
import {
  View,
  StyleSheet,
  FlatList,
  Animated,
  StatusBar as StatusBarRN,
  Modal as ModalRN,
  RefreshControl,
  ActivityIndicator,
} from 'react-native'
import { connect } from 'react-redux'
import { NavigationEvents } from '@react-navigation/compat'
import { isEmpty, noop, omit } from 'lodash-es'
import * as Animatable from 'react-native-animatable'
import { GET_USER_DETAIL } from 'sf-services/auth/actionTypes'
import {
  _enhancedNavigation,
  Text,
  Icon,
  Container,
  Image,
  Modal,
} from 'sf-components'
import {
  getArtistSpotlight,
  postClaimFreeTrial,
  getProfileDetail,
} from 'sf-actions/api'
import {
  WHITE,
  SHIP_GREY_CALM,
  REDDISH,
  SHIP_GREY,
  PALE_BLUE,
  NAVY_DARK,
  NO_COLOR,
  GUN_METAL,
} from 'sf-constants/Colors'
import {
  WP4,
  WP2,
  WP1,
  WP100,
  WP50,
  WP6,
  WP40,
  WP5,
  WP10,
  FONT_SIZE,
  WP85,
  WP3,
  WP20,
  WP15,
} from 'sf-constants/Sizes'
import { TOUCH_OPACITY, SHADOW_STYLE } from 'sf-constants/Styles'
import SoundfrenExploreOptions from 'sf-components/explore/SoundfrenExploreOptions'
import { getStatusBarHeight } from 'sf-utils/iphoneXHelper'
import { FONTS } from 'sf-constants/Fonts'
import ArtistItem from 'sf-components/artist_spotlight/ArtistItem'
import Song from 'sf-components/Song'
import TopArtistItem from 'sf-components/artist_spotlight/TopArtistItem'
import Touchable from 'sf-components/Touchable'
import { isEqualObject } from 'sf-utils/helper'
import { setData } from 'sf-services/screens/lv1/artist-spotlight/actionDispatcher'
import InteractionManager from 'sf-utils/InteractionManager'
import { restrictedAction } from '../../utils/helper'
import ArtistSpotlightMenus from './ArtistSpotlightMenus'

const StatusBar = Animatable.createAnimatableComponent(StatusBarRN)
const artistConfig = {
  popular_artist: {
    title: 'Soundfren Popular Artist',
    subtitle: 'Artist & musisi terpopuler',
    route: 'PopularArtistListScreen',
    key_data: 'popular_artist',
    line: false,
    horizontal: true,
    numColumns: undefined,
    empty_message:
      'Maaf, untuk saat ini belum ada\nArtist & musisi terpopuler yang tersedia',
  },
  new_music: {
    title: 'New Uploaded Music',
    subtitle: 'Lagu terbaru dari Artist Soundfren',
    route: 'NewMusicListScreen',
    key_data: 'new_music',
    line: true,
    horizontal: false,
    numColumns: undefined,
    empty_message: 'Maaf, untuk saat ini belum ada\nLagu terbaru yang tersedia',
  },
  new_playlist: {
    title: 'New Playlist',
    subtitle: 'Kompilasi lagu terbaru',
    route: 'NewPlaylistScreen',
    key_data: 'new_playlist',
    line: true,
    horizontal: true,
    numColumns: undefined,
    empty_message: 'Maaf, untuk saat ini belum ada\nPlaylist yang tersedia',
  },
  top_artist: {
    title: 'Top 5 Soundfren’s Artist',
    subtitle: 'Artist & musisi papan atas di Soundfren',
    route: '',
    key_data: 'top_artist',
    line: true,
    horizontal: false,
    numColumns: undefined,
    empty_message:
      'Maaf, untuk saat ini belum ada\nArtist & musisi papan atas yang tersedia',
  },
  new_artist: {
    title: 'New Sound. New Artist',
    subtitle: 'Artist & musisi pendatang baru',
    route: 'NewArtistListScreen',
    key_data: 'new_artist',
    line: true,
    horizontal: true,
    numColumns: undefined,
    empty_message:
      'Maaf, untuk saat ini belum ada\nArtist & musisi pendatang baru yang tersedia',
  },
}
const borderRadius = WP2
const modal = StyleSheet.create({
  modal: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,.6)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContent: {
    backgroundColor: WHITE,
    width: WP85,
    minHeight: WP40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius,
  },
  modalContentIllustration: {
    width: WP40,
    height: WP40,
    marginTop: WP10,
    marginBottom: WP5,
    paddingHorizontal: WP5,
  },
  modalContentTitle: {
    marginBottom: WP3,
  },
  modalContentDescription: {
    marginBottom: WP10,
    paddingHorizontal: WP5,
    lineHeight: WP5,
  },
  modalContentCta: {
    backgroundColor: REDDISH,
    width: '100%',
    paddingVertical: WP4 + 2,
    borderBottomLeftRadius: borderRadius,
    borderBottomRightRadius: borderRadius,
  },
  modalClose: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: WP3,
  },
  modalCloseIcon: {
    width: WP6,
    height: WP6,
  },
})

const mapStateToProps = ({
  auth,
  screen: {
    lv1: { artistspotlight },
  },
  song: { soundObject, playback },
}) => ({
  userData: auth.user,
  artistspotlight,
  soundObject,
  playback,
})

const mapDispatchToProps = {
  setUserDetail: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail,
  }),
  setData,
}

const mapFromNavigationParam = (getParam) => ({})

class ArtistSpotlightScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isRefreshing: false,
      scrollY: new Animated.Value(0),
      claimFreeTrial: false,
      shouldShowTrialModal: false,
    }
  }

  componentDidMount = () => {
    InteractionManager.runAfterInteractions(() => {
      this._getContent()
      const { navigation } = this.props
      this._willBlurSubscription = navigation.addListener('blur', () => {
        const { playback, soundObject } = this.props
        playback.isLoaded && soundObject.pauseAsync()
      })
    })
  };

  shouldComponentUpdate(nextProps, nextState) {
    const shouldUpdate =
      !isEqualObject(
        omit(this.props, ['soundObject', 'playback']),
        omit(nextProps, ['soundObject', 'playback']),
      ) ||
      !isEqualObject(omit(nextState, ['scrollY']), omit(this.state, ['scrollY']))
    return shouldUpdate
  }

  componentWillUnmount() {
    this._willBlurSubscription()
  }

  _getContent = async () => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props
    let params = { id_viewer: id_user, show: 'all' }
    const { result } = await dispatch(getArtistSpotlight, params, noop, true, false)
    if (result) {
      this.props.setData({ result, isLoaded: true })
    }
  };

  _headerSearch = () => {
    const { navigateBack, navigateTo, userData, navigation } = this.props
    const bgColor = this.state.scrollY.interpolate({
      inputRange: [-10, 0, WP5, WP15],
      outputRange: [WHITE, NO_COLOR, NO_COLOR, WHITE],
      extrapolate: 'clamp',
    })
    const searchColor = this.state.scrollY.interpolate({
      inputRange: [-10, 0, WP5, WP20],
      outputRange: [
        'rgba(145, 158, 171, 0.1)',
        'rgba(255,255,255, 0.16)',
        'rgba(255,255,255, 0.16)',
        'rgba(145, 158, 171, 0.1)',
      ],
      extrapolate: 'clamp',
    })
    const iconColor = this.state.scrollY.interpolate({
      inputRange: [-10, 0, WP5, WP20],
      outputRange: [SHIP_GREY_CALM, WHITE, WHITE, SHIP_GREY_CALM],
      extrapolate: 'clamp',
    })
    return (
      <View style={{ width: WP100, position: 'absolute', zIndex: 999, top: 0 }}>
        <StatusBar
          animated
          translucent={true}
          backgroundColor={bgColor}
          barStyle={'dark-content'}
        />
        <Animated.View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingLeft: WP4,
            paddingVertical: WP2,
            backgroundColor: bgColor,
            paddingTop: WP2 + getStatusBarHeight(true),
          }}
        >
          <Icon
            centered
            onPress={() => navigateBack()}
            background='dark-circle'
            backgroundColor='rgba(255,255,255, 0.16)'
            size='large'
            color={iconColor}
            name='chevron-left'
            type='Entypo'
          />
          <Touchable
            onPress={restrictedAction({
              action: () => navigateTo('ArtistSpotlightSearchScreen'),
              userData,
              navigation,
            })}
            activeOpacity={TOUCH_OPACITY}
            style={{ flex: 1 }}
          >
            <Animated.View
              style={{
                flex: 1,
                backgroundColor: searchColor,
                borderRadius: 6,
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: WP2,
                paddingVertical: WP1,
                marginHorizontal: WP2,
              }}
            >
              <Icon
                centered
                background='dark-circle'
                size='large'
                color={iconColor}
                name='magnify'
                type='MaterialCommunityIcons'
                style={{ marginRight: WP1 }}
              />
              <Animated.Text
                style={{
                  color: iconColor,
                  fontFamily: FONTS['Circular'][300],
                  fontSize: FONT_SIZE['mini'],
                }}
                centered
              >
                Coba cari Artist
              </Animated.Text>
            </Animated.View>
          </Touchable>
          <Modal
            position='bottom'
            swipeDirection={null}
            renderModalContent={({ toggleModal }) => {
              return (
                <SoundfrenExploreOptions
                  menuOptions={ArtistSpotlightMenus}
                  userData={this.props.userData}
                  navigateTo={navigateTo}
                  onClose={toggleModal}
                />
              )
            }}
          >
            {({ toggleModal }, M) => (
              <Fragment>
                <Touchable
                  onPress={restrictedAction({
                    action: toggleModal,
                    userData,
                    navigation,
                  })}
                  activeOpacity={TOUCH_OPACITY}
                  style={{
                    height: '100%',
                    justifyContent: 'center',
                    paddingRight: WP4,
                  }}
                >
                  <Icon
                    centered
                    background='dark-circle'
                    size='large'
                    color={iconColor}
                    name='dots-three-horizontal'
                    type='Entypo'
                  />
                </Touchable>
                {M}
              </Fragment>
            )}
          </Modal>
        </Animated.View>
      </View>
    )
  };

  _headerSection = (about) => {
    const { navigateTo, userData, navigation } = this.props
    const notPremium = userData.account_type == 'user' && userData.free_trial
    const subtitle = notPremium
      ? about?.subtitle
      : 'Upload musik terbarumu dan gapai\npendengar baru'
    const buttonText = notPremium ? 'Coba Sekarang' : 'Upload Sekarang'
    const actionButton = notPremium
      ? () => this._showTrialModal()
      : () => navigateTo('SongFileForm')
    return (
      <View>
        <Image
          tint={'black'}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 255}
          source={require('sf-assets/images/bgArtistSpotlight.jpg')}
        />
        <View
          style={{
            position: 'absolute',
            left: 0,
            top: getStatusBarHeight(true) + WP10,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <View style={{ alignItems: 'center' }}>
            <Text
              type='Circular'
              size='large'
              weight={600}
              color={WHITE}
              centered
              style={{ marginBottom: WP2 }}
            >
              {about?.title || 'Artist Spotlight'}
            </Text>
            <Text
              type='Circular'
              size='mini'
              weight={300}
              color={WHITE}
              centered
              style={{ lineHeight: WP6, marginHorizontal: WP10 }}
            >
              {subtitle}
            </Text>
            <Touchable
              activeOpacity={TOUCH_OPACITY}
              onPress={restrictedAction({
                action: actionButton,
                userData,
                navigation,
              })}
              style={{
                width: WP50,
                backgroundColor: REDDISH,
                borderRadius: 6,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: WP6,
                paddingVertical: WP2,
                ...SHADOW_STYLE['shadowThin'],
              }}
            >
              <Text type='Circular' size='mini' weight={500} color={WHITE} centered>
                {buttonText}
              </Text>
            </Touchable>
          </View>
        </View>
      </View>
    )
  };

  _showTrialModal = () => {
    this.setState({
      shouldShowTrialModal: true,
    })
  };

  _hideTrialModal = () => {
    this.setState({
      shouldShowTrialModal: false,
    })
  };

  _onTrial = () => {
    this.setState(
      { claimFreeTrial: true, shouldShowTrial: false },
      this._claimTrial,
    )
  };

  _claimTrial = () => {
    const {
      dispatch,
      userData: { id_user },
      popToTop,
      navigateTo,
      setUserDetail,
    } = this.props
    dispatch(postClaimFreeTrial, {
      id_user,
    }).then(() => {
      this.setState({ shouldShowTrialModal: false }, () => {
        dispatch(getProfileDetail, { id_user, show: 'all' }).then((dataResponse) => {
          setUserDetail(dataResponse)
          popToTop()
          navigateTo('ProfileScreenNoTab', { idUser: id_user })
        })
      })
    })
  };

  _onPressItem = (data, section) => () => {
    const { navigateTo } = this.props
    if (section === 'popular_artist' || section === 'new_artist') {
      navigateTo('ProfileScreenNoTab', {
        idUser: data.id_user,
        navigateBackOnDone: true,
      })
    }
  };

  _adItem = (ads, i, section) => {
    const {
      navigateTo,
      userData,
      navigation,
      artistspotlight: { isLoaded },
    } = this.props
    const loading = !isLoaded
    return (
      <Touchable
        disabled={loading || section == 'top_artist'}
        style={{ paddingVertical: section == 'new_music' ? 0 : WP2 }}
        key={`${i}${Math.random()}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={restrictedAction({
          action: this._onPressItem(ads, section),
          userData: __DEV__ ? {} : userData, // testing purpose, safe when forgot to update to production
          navigation,
        })}
      >
        {section === 'popular_artist' && <ArtistItem ads={ads} loading={loading} />}
        {section === 'new_music' && (
          <Song
            refreshData={this._getContent}
            idViewer={userData.id_user}
            isProfileScreen={false}
            isMine={ads.id_user == userData.id_user}
            firstSong={i == 0}
            song={ads}
            style={{ paddingRight: WP4 }}
            styleFirst={{ paddingRight: WP4, paddingTop: WP2 }}
            loading={loading}
            showArtist
            navigateTo={navigateTo}
          />
        )}
        {section === 'top_artist' && (
          <TopArtistItem
            idViewer={userData.id_user}
            refreshData={this._getContent}
            profile={userData}
            ads={ads}
            loading={loading}
            navigateTo={navigateTo}
            line={i != 0}
          />
        )}
        {section === 'new_artist' && <ArtistItem ads={ads} loading={loading} />}
      </Touchable>
    )
  };

  _section = (section) => {
    const {
      navigateTo,
      artistspotlight: { result, isLoaded },
    } = this.props
    const config = artistConfig[section]
    const dummyData = [1, 2, 3, 4, 5]
    const dataList = !isLoaded
      ? dummyData
      : !isEmpty(result)
      ? result[config.key_data]
      : []
    return (
      <View
        style={{
          borderTopColor: PALE_BLUE,
          borderTopWidth: config.line ? 1 : 0,
        }}
      >
        <View style={{ paddingVertical: WP6 }}>
          {/* HEADER */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: WP4,
            }}
          >
            <Text type='Circular' size='small' color={NAVY_DARK} weight={600}>
              {config.title}
            </Text>
            {!isEmpty(config.route) && (
              <Touchable
                activeOpacity={TOUCH_OPACITY}
                onPress={() => {
                  navigateTo(config.route)
                }}
              >
                <Text type='Circular' size='xmini' color={REDDISH} weight={400}>
                  Lihat Semua
                </Text>
              </Touchable>
            )}
          </View>
          <Text
            type='Circular'
            size='mini'
            color={SHIP_GREY}
            weight={300}
            style={{ paddingLeft: WP4 }}
          >
            {config.subtitle}
          </Text>

          {/* ITEM */}
          <FlatList
            bounces={false}
            bouncesZoom={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            horizontal={config.horizontal}
            numColumns={config.numColumns}
            style={{ flexGrow: 0 }}
            contentContainerStyle={{
              paddingLeft: section == 'top_artist' ? 0 : WP4,
              paddingTop:
                section == 'popular_artist' || section == 'new_artist' ? WP6 : WP4,
            }}
            data={dataList}
            extraData={dataList}
            renderItem={({ item, index }) => this._adItem(item, index, section)}
            ListEmptyComponent={
              <View>
                <Text
                  type='Circular'
                  size='small'
                  weight={400}
                  color={SHIP_GREY_CALM}
                >
                  {config.empty_message}
                </Text>
              </View>
            }
          />
        </View>
      </View>
    )
  };

  render() {
    const { claimFreeTrial, shouldShowTrialModal, isRefreshing } = this.state
    const {
      artistspotlight: { result },
    } = this.props
    return (
      <Container isLoading={false} noStatusBarPadding statusBarBackground={NO_COLOR}>
        <View style={{ flex: 1, backgroundColor: WHITE }}>
          <NavigationEvents onDidFocus={this._getContent} />
          {this._headerSearch()}
          <Animated.ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={isRefreshing}
                onRefresh={() => {
                  this.setState({ isRefreshing: true }, async () => {
                    await this._getContent()
                    this.setState({
                      isRefreshing: false,
                    })
                  })
                }}
              />
            }
            scrollEventThrottle={16}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: { contentOffset: { y: this.state.scrollY } },
                },
              ],
              { useNativeDriver: false },
            )}
          >
            {this._headerSection(result?.about)}
            {this._section('popular_artist')}
            {this._section('new_music')}
            {this._section('top_artist')}
            {this._section('new_artist')}
          </Animated.ScrollView>
          <ModalRN
            transparent
            animationType={'fade'}
            onRequestClose={this._hideTrialModal}
            visible={shouldShowTrialModal}
          >
            <View style={modal.modal}>
              {claimFreeTrial && (
                <View style={modal.modalContent}>
                  <ActivityIndicator color={REDDISH} size={'large'} />
                </View>
              )}
              {!claimFreeTrial && (
                <View style={modal.modalContent}>
                  <Image
                    source={require('sf-assets/icons/v3/freeTrialIllustration.png')}
                    imageStyle={modal.modalContentIllustration}
                  />
                  <Text
                    style={modal.modalContentTitle}
                    centered
                    type={'Circular'}
                    weight={500}
                    color={GUN_METAL}
                  >
                    14 Hari Free Trial Artist Profile
                  </Text>
                  <Text
                    style={modal.modalContentDescription}
                    size={'mini'}
                    type={'Circular'}
                    weight={300}
                    centered
                    color={SHIP_GREY_CALM}
                  >
                    Tunggu apa lagi? Tunjukan karya dan gapai pendengar barumu
                    sekarang
                  </Text>
                  <View style={modal.modalContentCta}>
                    <Touchable onPress={this._onTrial}>
                      <Text
                        size={'slight'}
                        type={'Circular'}
                        weight={500}
                        centered
                        color={WHITE}
                      >
                        Coba Sekarang
                      </Text>
                    </Touchable>
                  </View>
                  <View style={modal.modalClose}>
                    <Image
                      onPress={() => this._hideTrialModal()}
                      source={require('sf-assets/icons/v3/closeGrey.png')}
                      imageStyle={modal.modalCloseIcon}
                    />
                  </View>
                </View>
              )}
            </View>
          </ModalRN>
        </View>
      </Container>
    )
  }
}

ArtistSpotlightScreen.propTypes = {}

ArtistSpotlightScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ArtistSpotlightScreen),
  mapFromNavigationParam,
)
