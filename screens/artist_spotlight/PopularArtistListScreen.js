import React, { Component, Fragment } from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { concat, isEmpty, noop, uniqWith } from 'lodash-es'
import { _enhancedNavigation, Text, Icon, Container } from '../../components'
import { getArtistSpotlight } from '../../actions/api'
import { WHITE, SHIP_GREY_CALM, SHIP_GREY, NAVY_DARK, PALE_GREY } from '../../constants/Colors'
import { WP4, WP2, WP100, WP3, HP5, HP2, WP90 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import SoundfrenExploreOptions from '../../components/explore/SoundfrenExploreOptions'
import Modal from '../../components/Modal'
import ArtistItem from '../../components/artist_spotlight/ArtistItem'
import ArtistSpotlightMenus from './ArtistSpotlightMenus'

const artistConfig = {
  empty_message: 'Maaf, untuk saat ini belum ada\nartist yang tersedia'
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = () => ({
})

class PopularArtistListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: true,
      isLoading: true,
      result: {},
    }
  }

  componentDidMount = () => {
    this._getContent({ start: 0, limit: 15 }, false, false)
  }

  _getContent = async (params, loadMore = false, isLoading = false) => {
    const {
      dispatch,
      userData: {
        id_user
      }
    } = this.props

    if(!loadMore) {
      this.setState({
        isLoading: true
      })
    }

    try {
      if (!params) params = { start: 0, limit: 15 }

      const { result } = await dispatch(getArtistSpotlight, {
        id_viewer: id_user,
        show: 'popular_artist',
        ...params
      }, noop, true, isLoading)

      const dataResult = result ? result.popular_artist || [] : []

      if (!loadMore) {
        this.setState({
          result: dataResult,
          isLoading: false
        })
      } else {
        this.setState({
          result: uniqWith(concat(this.state.result, dataResult || []), (x, y) => x.id_user === y.id_user),
          isLoading: false
        })
      }
    } catch (e) {
      this.setState({ isLoading: false })
    }
  }

  _adItem = (ads, i, loading) => {
    const {
      navigateTo,
      userData: {
        id_user
      }
    } = this.props
    return (
      <TouchableOpacity
        disabled={loading}
        style={{ paddingLeft: WP3, paddingVertical: WP2, width: WP100 }}
        activeOpacity={TOUCH_OPACITY}
        onPress={() => navigateTo('ProfileScreenNoTab', { idUser: ads.id_user, navigateBackOnDone: true })}
      >
        <ArtistItem myId={id_user} ads={ads} loading={loading} simpleListItem={true} />
      </TouchableOpacity>
    )
  }

  render() {
    const {
      isReady,
      isLoading,
      result
    } = this.state
    const {
      navigateBack,
      navigateTo
    } = this.props

    const dummyData = [1, 2, 3]
    const dataList = isLoading ? dummyData : !isEmpty(result) ? result : []

    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={() => this._getContent({ start: 0, limit: 15 }, false, false)}
        onPullUpToLoad={async () => {
          await this._getContent({ start: result.length, limit: 10 }, true, false)
        }}
        isReady={isReady}
        isLoading={false}
        renderHeader={() => (
          <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: WP4,
            paddingVertical: WP2,
            backgroundColor: WHITE,
          }}
          >
            <Icon
              centered
              onPress={() => navigateBack()}
              background='dark-circle'
              size='large'
              color={SHIP_GREY_CALM}
              name='chevron-left'
              type='Entypo'
            />
            <Text type='Circular' size='mini' weight={400} color={SHIP_GREY} centered>Popular Artist</Text>
            <Modal
              position='bottom'
              swipeDirection='none'
              renderModalContent={({ toggleModal }) => {
                return (
                  <SoundfrenExploreOptions
                    menuOptions={ArtistSpotlightMenus}
                    userData={this.props.userData}
                    navigateTo={navigateTo}
                    onClose={toggleModal}
                  />
                )
              }}
            >
              {
                ({ toggleModal }, M) => (
                  <Fragment>
                    <Icon
                      centered
                      onPress={toggleModal}
                      background='dark-circle'
                      size='large'
                      color={SHIP_GREY_CALM}
                      name='dots-three-horizontal'
                      type='Entypo'
                    />
                    {M}
                  </Fragment>
                )
              }
            </Modal>
          </View>
        )}
      >
        <View>
          <Text type='Circular' style={{ margin: HP2 }} color={NAVY_DARK} size='small' weight={500}>Semua</Text>
          <FlatList
            bounces={false}
            bouncesZoom={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: HP5 }}
            style={{ flexGrow: 0 }}
            data={dataList}
            extraData={dataList}
            keyExtractor={(item) => item.id_user}
            renderItem={({ item, index }) => (
              this._adItem(item, index, isLoading)
            )}
            scrollEnabled={false}
            ItemSeparatorComponent={() => (
              <View style={{ backgroundColor: PALE_GREY, height: 1, width: WP90, alignSelf: 'center' }}/>
            )}
            ListEmptyComponent={(
              <View style={{ alignItems: 'center' }}>
                <Text type='Circular' size='small' weight={400} color={SHIP_GREY_CALM}>{artistConfig.empty_message}</Text>
              </View>
            )}
          />
        </View>
      </Container>
    )
  }
}

PopularArtistListScreen.propTypes = {}

PopularArtistListScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(PopularArtistListScreen),
  mapFromNavigationParam
)
