import React, { Component, Fragment } from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { concat, isEmpty, noop, uniqWith } from 'lodash-es'
import { NavigationEvents } from '@react-navigation/compat'
import { _enhancedNavigation, Text, Icon, Container } from '../../components'
import { getArtistSpotlight } from '../../actions/api'
import { WHITE, SHIP_GREY_CALM, SHIP_GREY, NAVY_DARK } from '../../constants/Colors'
import { WP4, WP2, WP3, HP5, HP2 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import SoundfrenExploreOptions from '../../components/explore/SoundfrenExploreOptions'
import Modal from '../../components/Modal'
import Song from '../../components/Song'
import { restrictedAction } from '../../utils/helper'
import ArtistSpotlightMenus from './ArtistSpotlightMenus'

const artistConfig = {
  empty_message: 'Maaf, untuk saat ini belum ada\nartist yang tersedia',
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = () => ({})

class NewMusicListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: true,
      isLoading: true,
      result: {},
      resultAll: {},
    }
  }

  componentDidMount = () => {
    this._getContent({ start: 0, limit: 15 }, false, false)
  };

  _getContent = async (params, loadMore = false, isLoading = false) => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props

    if (!loadMore) {
      this.setState({
        isLoading: true,
      })
    }

    try {
      if (!params) params = { start: 0, limit: 15 }

      const { result } = await dispatch(
        getArtistSpotlight,
        {
          id_viewer: id_user,
          show: 'new_music',
          ...params,
        },
        noop,
        true,
        isLoading,
      )

      const dataResult = result ? result.new_music || [] : []
      const dataResultAll = result ? result.all_music || [] : []

      if (!loadMore) {
        this.setState({
          result: dataResult,
          resultAll: dataResultAll,
          isLoading: false,
        })
      } else {
        this.setState({
          resultAll: uniqWith(
            concat(this.state.resultAll, dataResultAll || []),
            (x, y) => x.id_journey === y.id_journey,
          ),
          isLoading: false,
        })
      }
    } catch (e) {
      this.setState({ isLoading: false })
    }
  };

  _adItem = (ads, i, loading) => {
    const {
      navigateTo,
      userData: { id_user },
    } = this.props
    return (
      <TouchableOpacity
        disabled={loading}
        style={{ paddingLeft: WP3, paddingVertical: 0 }}
        activeOpacity={TOUCH_OPACITY}
      >
        <Song
          refreshData={() => this._getContent({ start: 0, limit: 15 }, false, false)}
          isMine={ads.id_user == id_user}
          idViewer={id_user}
          song={ads}
          style={{ paddingRight: WP4 }}
          loading={loading}
          showArtist
          navigateTo={navigateTo}
        />
      </TouchableOpacity>
    )
  };

  render() {
    const { isReady, isLoading, result, resultAll } = this.state
    const { navigateBack, navigateTo, userData } = this.props

    const dummyData = [1, 2, 3]
    const dataListNewMusic = isLoading ? dummyData : !isEmpty(result) ? result : []
    const dataListAllMusic = isLoading
      ? dummyData
      : !isEmpty(resultAll)
      ? resultAll
      : []

    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={() =>
          this._getContent({ start: 0, limit: 15 }, false, false)
        }
        onPullUpToLoad={async () => {
          await this._getContent(
            { start: dataListAllMusic.length, limit: 5 },
            true,
            false,
          )
        }}
        isReady={isReady}
        isLoading={false}
        renderHeader={() => (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: WP4,
              paddingVertical: WP2,
              backgroundColor: WHITE,
            }}
          >
            <NavigationEvents
              onWillFocus={() =>
                this._getContent({ start: 0, limit: 15 }, false, false)
              }
            />
            <Icon
              centered
              onPress={() => navigateBack()}
              background='dark-circle'
              size='large'
              color={SHIP_GREY_CALM}
              name='chevron-left'
              type='Entypo'
            />
            <Text
              type='Circular'
              size='mini'
              weight={400}
              color={SHIP_GREY}
              centered
            >
              New Uploaded Music
            </Text>
            <Modal
              position='bottom'
              swipeDirection='none'
              renderModalContent={({ toggleModal }) => {
                return (
                  <SoundfrenExploreOptions
                    menuOptions={ArtistSpotlightMenus}
                    userData={this.props.userData}
                    navigateTo={navigateTo}
                    onClose={toggleModal}
                  />
                )
              }}
            >
              {({ toggleModal }, M) => (
                <Fragment>
                  <Icon
                    centered
                    onPress={restrictedAction({
                      action: toggleModal,
                      userData,
                      navigateTo,
                    })}
                    background='dark-circle'
                    size='large'
                    color={SHIP_GREY_CALM}
                    name='dots-three-horizontal'
                    type='Entypo'
                  />
                  {M}
                </Fragment>
              )}
            </Modal>
          </View>
        )}
      >
        <View>
          <Text
            type='Circular'
            style={{ margin: HP2 }}
            color={NAVY_DARK}
            size='small'
            weight={500}
          >
            Music Terbaru
          </Text>
          <FlatList
            bounces={false}
            bouncesZoom={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: HP5 }}
            style={{ flexGrow: 0 }}
            data={dataListNewMusic}
            extraData={dataListNewMusic}
            keyExtractor={(item) => item.id_journey}
            renderItem={({ item, index }) => this._adItem(item, index, isLoading)}
            scrollEnabled={false}
            ListEmptyComponent={
              <View style={{ alignItems: 'center' }}>
                <Text
                  type='Circular'
                  size='small'
                  weight={400}
                  color={SHIP_GREY_CALM}
                >
                  {artistConfig.empty_message}
                </Text>
              </View>
            }
          />
          <Text
            type='Circular'
            style={{ margin: HP2 }}
            color={NAVY_DARK}
            size='small'
            weight={500}
          >
            Semua Musik
          </Text>
          <FlatList
            bounces={false}
            bouncesZoom={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: HP5 }}
            style={{ flexGrow: 0 }}
            data={dataListAllMusic}
            extraData={dataListAllMusic}
            keyExtractor={(item) => item.id_journey}
            renderItem={({ item, index }) => this._adItem(item, index, isLoading)}
            scrollEnabled={false}
            ListEmptyComponent={
              <View style={{ alignItems: 'center' }}>
                <Text
                  type='Circular'
                  size='small'
                  weight={400}
                  color={SHIP_GREY_CALM}
                >
                  {artistConfig.empty_message}
                </Text>
              </View>
            }
          />
        </View>
      </Container>
    )
  }
}

NewMusicListScreen.propTypes = {}

NewMusicListScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(NewMusicListScreen),
  mapFromNavigationParam,
)
