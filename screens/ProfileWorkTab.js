import React from 'react'
import { ImageBackground, ScrollView, Slider, TouchableOpacity, View } from 'react-native'
import { withNavigation } from '@react-navigation/compat'
import { groupBy } from 'lodash-es'
import { connect } from 'react-redux'
import { _enhancedNavigation, Button, Image, ProfileCardWork, Text } from '../components'
import { GREY20, GREY50, GREY_CALM_SEMI, ORANGE_BRIGHT, PINK_RED, SILVER_CALMER, TOMATO, WHITE, WHITE_MILK } from '../constants/Colors'
import { HP05, HP1, HP2, WP1, WP15, WP2, WP3, WP35, WP4, WP40, WP5, WP50, WP6, WP7, WP90 } from '../constants/Sizes'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../constants/Styles'
import { NavigateToInternalBrowser } from '../utils/helper'
import Player from '../components/Player'
import EmptyV2 from '../components/EmptyV2'
import { ProfileWorkTabSkeleton } from '../components/profile/ProfileWorkTabSkeletonConfig'
import { CARD_SONG_ID } from '../services/player/actionTypes'

const mapStateToProps = ({ auth, player }) => ({
  userData: auth.user,
  playerIsPlaying: player.isPlaying,
  cardSongId: player.cardSongId
})

const mapDispatchToProps = {
  setCardSongId: (id) => ({ type: CARD_SONG_ID, response: id })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  idUser: getParam('idUser', 0)
})

const songLimit = {
  free: 3,
  basic: 6,
  pro: 12
}

class ProfileWorkTab extends React.Component {
  state = {
    user: {},
    songs: [],
    videos: [],
    arts: [],
    isMine: false,
    isReady: false,
    isRefreshing: false
  }

  _renderLoadingButtonPlay = () => (
    <Image
      source={require('../assets/loading_music.gif')}
      size='mini'
      style={{ marginVertical: 0, flexGrow: 0, marginRight: WP3, padding: WP1 }}
    />
  )

  _renderButtonPlay = ({
    isLoading,
    toggle,
    isPlaying,
    isSameSong,
    changeAndPlay,
    url,
    id
  }) => {
    if (isLoading && isSameSong) return this._renderLoadingButtonPlay()
    return (
      <Button
        style={{ marginVertical: 0, flexGrow: 0, marginRight: WP3 }}
        toggle
        toggleActiveColor={SILVER_CALMER}
        toggleInactiveTextColor={TOMATO}
        backgroundColor={TOMATO}
        contentStyle={{ padding: WP1 }}
        iconName={(isPlaying && isSameSong) ? 'pause' : 'play'}
        iconType='MaterialCommunityIcons'
        iconColor={TOMATO}
        onPress={() => {
          this.props.setCardSongId(id)
          if (isSameSong) toggle()
          else {
            changeAndPlay(url)
          }
        }}
        rounded
        shadow='none'
      />
    )
  }

  _renderSongItem = (playerOpts) => ({ data, renderAction, activeId }) => {
    const additionalData = JSON.parse(data.additional_data)
    const id = data.id_journey
    const url = additionalData.url_song
    const icon = additionalData.icon
    const { toggle, isPlaying, playbackInstancePosition, playbackInstanceDuration,
      onChangeMillis, toggleSliding, changeAndPlay, songUrl, isLoading } = playerOpts
    const isSameSong = url === songUrl
    return (
      <TouchableOpacity
        onPress={async () => {
          if (additionalData.in_app_player === 0) {
            NavigateToInternalBrowser({
              url
            })
          }
        }} activeOpacity={TOUCH_OPACITY}
        style={{ paddingHorizontal: WP6, marginVertical: WP1 }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          {
            additionalData.in_app_player === 1 ?
              this._renderButtonPlay({
                isLoading,
                toggle,
                isPlaying,
                isSameSong,
                changeAndPlay,
                url,
                id
              })
              :
              <Image size='xsmall' source={{ uri: icon }} style={{ marginVertical: 0, marginRight: WP2 }} />
          }
          <View style={{ paddingHorizontal: WP2, marginRight: WP15 }}>
            <Text weight={400} size='mini' numberOfLines={1}>{data.title}</Text>
          </View>
          {
            renderAction()
          }
        </View>
        {
          activeId === data.id_journey && (
            <View style={{ paddingTop: HP1 }}>
              {
                additionalData.in_app_player === 1 && (
                  <Slider
                    style={{ flexGrow: 1, height: 40 }}
                    onSlidingComplete={(value) => {
                      onChangeMillis(value)
                      toggleSliding(false)
                    }}
                    onValueChange={() => toggleSliding(true)}
                    minimumValue={0}
                    maximumValue={playbackInstanceDuration}
                    value={id == this.props.cardSongId ? playbackInstancePosition : 0}
                    minimumTrackTintColor={TOMATO}
                    maximumTrackTintColor={GREY_CALM_SEMI}
                  />
                )
              }
              {
                data.description
                  ? <Text color={GREY50} size='tiny'>{data.description}</Text>
                  : <Text color={GREY20} size='tiny'>No description</Text>
              }
            </View>
          )
        }
      </TouchableOpacity>
    )
  }

  render() {
    const {
      songs,
      videos,
      arts,
      isMine,
      isEmpty,
      navigateTo,
      refreshSongs,
      refreshProfile,
      deleteSong,
      openModalAdd,
      refreshVideos,
      deleteVideo,
      refreshArts,
      deleteArt,
      isTabReady,
      isActiveTab,
      userType,
      user
    } = this.props
    const groupedArts = groupBy(arts, 'id_journey')
    const songsFileCount = songs.filter((x) => JSON.parse(x.additional_data ? x.additional_data : '{}').in_app_player === 1).length
    const isLimit = songsFileCount >= songLimit[userType]

    return (
      <View>
        {
          !isActiveTab ? null :
            isTabReady ? !isEmpty ? (
              <View style={{ backgroundColor: WHITE, paddingBottom: HP1 }}>
                <Player>
                  {
                    (playerOpts) => {
                      return (
                        <ProfileCardWork
                          actionAdd={() => navigateTo(
                            'Song',
                            { refreshProfile: refreshSongs, navigateBackOnDone: true, limit: isLimit, userType },
                            'push'
                          )}
                          actionEdit={(data) => {
                            const additionalData = JSON.parse(data.additional_data)
                            const songUrl = additionalData.url_song
                            if (additionalData.in_app_player) navigateTo('SongFileForm', { refreshProfile: refreshSongs, id: data.id_journey })
                            else navigateTo('SongLinkForm', { refreshProfile: refreshSongs, initialValues: { ...data, songUrl } })
                          }}
                          actionDelete={deleteSong}
                          header='Song'
                          category='Song'
                          isMine={isMine}
                          user={user}
                          dataSource={songs}
                          navigateTo={navigateTo}
                          refreshProfile={refreshProfile}
                          limit
                          limitNum={5}
                          idPerItem='id_journey'
                          renderContent={this._renderSongItem(playerOpts)}
                        />
                      )
                    }
                  }
                </Player>
                <ProfileCardWork
                  actionAdd={() => {
                    navigateTo('ProfileAddVideoScreen', { refreshProfile: refreshVideos }, 'push')
                  }}
                  actionEdit={(data) => {
                    const additionalData = JSON.parse(data.additional_data)
                    const videoUrl = additionalData.url_video
                    navigateTo('ProfileAddVideoScreen', { refreshProfile: refreshVideos, initialValues: { ...data, videoUrl } })
                  }}
                  actionDelete={deleteVideo}
                  actionIcon='bottom'
                  header='Video'
                  category='Video'
                  isMine={isMine}
                  user={user}
                  refreshProfile={refreshProfile}
                  navigateTo={navigateTo}
                  dataSource={videos}
                  idPerItem='id_journey'
                  contentDirection='row'
                  contentContainerProps={{}}
                  noDescription={true}
                  limit
                  limitNum={3}
                  renderContent={
                    ({ data, renderAction }) => {
                      const additionalData = JSON.parse(data.additional_data)
                      const imageUrl = additionalData.url_video
                      const temp = imageUrl.split('/')
                      let ytImageUrl = 'broken'
                      if (temp.length > 1) {
                        ytImageUrl = temp[temp.length - 1]
                        const temps = ytImageUrl.split('=')
                        if (temps.length > 1) {
                          ytImageUrl = temps[1]
                        }
                      }
                      ytImageUrl = `https://img.youtube.com/vi/${ytImageUrl}/default.jpg`
                      return (
                        <TouchableOpacity
                          activeOpacity={TOUCH_OPACITY} onPress={() => {
                            NavigateToInternalBrowser({
                              url: imageUrl
                            })
                          }}
                          style={{ paddingVertical: HP1, paddingHorizontal: WP5 }}
                        >
                          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <ImageBackground
                              imageStyle={{ borderRadius: 12 }}
                              style={{
                                justifyContent: 'center', backgroundColor: WHITE_MILK,
                                width: WP40, height: undefined, aspectRatio: 2,
                                alignItems: 'center',
                                marginRight: WP2
                              }}
                              source={{ uri: ytImageUrl }}
                            >
                              <Button
                                onPress={() => {
                                  NavigateToInternalBrowser({
                                    url: imageUrl
                                  })
                                }}
                                iconName='controller-play'
                                iconType='Entypo'
                                style={{ marginVertical: 0 }}
                                contentStyle={{ padding: WP1 }}
                                shadow='none'
                                colors={[PINK_RED, ORANGE_BRIGHT]}
                                rounded
                                iconColor={WHITE}
                              />
                            </ImageBackground>
                            <View
                              style={{
                                width: WP40,
                                justifyContent: 'space-around'
                              }}
                            >
                              {
                                !!data.title && (
                                  <Text style={{ marginBottom: HP1 }} size='mini' weight={400} numberOfLines={1}>{data.title}</Text>
                                )
                              }
                              {
                                !!data.description && (
                                  <Text numberOfLines={3} size='tiny'>{data.description}</Text>
                                )
                              }
                            </View>
                          </View>
                          {
                            renderAction()
                          }
                        </TouchableOpacity>
                      )
                    }
                  }
                />
                <ProfileCardWork
                  actionAdd={() => {
                    navigateTo('ProfileAddArtScreen', { refreshProfile: refreshArts }, 'push')
                  }}
                  actionEdit={(data) => {
                    navigateTo('ProfileAddArtScreen', { refreshProfile: refreshArts, initialValues: data })
                  }}
                  actionDelete={deleteArt}
                  actionIcon='bottom'
                  header='Picture'
                  category='Picture'
                  isMine={isMine}
                  user={user}
                  refreshProfile={refreshProfile}
                  navigateTo={navigateTo}
                  dataSource={groupedArts}
                  idPerItem='[0].id_journey'
                  contentDirection='row'
                  noDescription={true}
                  ContentContainer={ScrollView}
                  contentContainerProps={{
                    horizontal: true,
                    showsHorizontalScrollIndicator: false
                  }}
                  noCard
                  renderContent={
                    ({ data, renderAction }) => {
                      const groupedArt = data[0]
                      return (
                        <TouchableOpacity
                          activeOpacity={TOUCH_OPACITY}
                          onPress={
                            () => navigateTo(
                              'ProfileShowArtScreen',
                              { idJourney: groupedArt.id_journey },
                              'push'
                            )
                          }
                          style={{
                            width: WP35,
                            borderRadius: 12,
                            backgroundColor: WHITE,
                            marginVertical: HP05,
                            marginRight: WP7,
                            paddingBottom: HP1,
                            ...SHADOW_STYLE.shadow
                          }}
                        >
                          <Image
                            source={{ uri: groupedArt.attachment_file }}
                            style={{ borderTopLeftRadius: 12, borderTopRightRadius: 12 }}
                            imageStyle={{ width: WP35, aspectRatio: 1, borderTopLeftRadius: 12, borderTopRightRadius: 12 }}
                          />
                          {
                            data.length > 1 && (
                              <Image source={require('../assets/icons/gallery.png')} style={{ position: 'absolute', right: WP2, top: WP2 }} size='mini' aspectRatio={21/18}/>
                            )
                          }
                          <View style={{ width: WP35 }}>
                            {
                              !!groupedArt.title && (
                                <Text centered style={{ marginVertical: HP1 }} size='mini' weight={400} numberOfLines={1}>{groupedArt.title}</Text>
                              )
                            }
                            {
                              !!groupedArt.description && (
                                <Text centered numberOfLines={3} size='mini'>{groupedArt.description}</Text>
                              )
                            }
                          </View>
                          {
                            renderAction()
                          }
                        </TouchableOpacity>
                      )
                    }
                  }
                />
              </View>
            ) : (
              <EmptyV2
                full
                aspectRatio={320/267}
                image={require('../assets/images/noWorksBg.png')}
                style={{ alignItems: 'flex-start', paddingHorizontal: WP4 }}
                textStyle={{ textAlign: 'left' }}
                textContainerStyle={{ alignItems: 'flex-start', width: WP50 }}
                message={isMine ? 'The time has come,' : 'Hey, it’s empty here,'}
                title={isMine ? 'Let’s show special works and experience of you..' : 'come back soon'}
                actions={(
                  <View style={{ position: 'absolute', bottom: HP2, flex: 1, alignSelf: 'center', alignItems: 'center' }}>
                    {
                      isMine && <Button
                        onPress={() => openModalAdd()}
                        style={{ width: WP90 }}
                        backgroundColor={TOMATO}
                        centered
                        bottomButton
                        radius={WP4}
                        shadow='none'
                        textType='NeoSans'
                        textSize='small'
                        textColor={WHITE}
                        textWeight={500}
                        text='Add Works'
                                />
                    }
                  </View>
                )}
              />
            ) : (
              <ProfileWorkTabSkeleton isLoading={!isTabReady} />
            )
        }
      </View>
    )
  }
}

export default withNavigation(_enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProfileWorkTab),
  mapFromNavigationParam
))
