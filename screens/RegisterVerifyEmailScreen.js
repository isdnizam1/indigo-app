import React from 'react'
import { View, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import { split } from 'lodash-es'
import { Container, Header, Text, Button, Icon, _enhancedNavigation } from '../components/index'
import { PINK_RED_DARK, ORANGE_BRIGHT_DARK, WHITE, WHITE20 } from '../constants/Colors'
import { WP5, HP2 } from '../constants/Sizes'
import { getProfileDetail } from '../actions/api'
import { NavigateToInternalBrowser } from '../utils/helper'

let backgroundIntervalEmailChecker = null

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
  authIsLoading: auth.isLoading
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', true)
})

class RegisterVerifyEmailScreen extends React.Component {

  componentDidMount = async () => {
    const {
      userData,
      navigateTo
    } = this.props

    backgroundIntervalEmailChecker = setInterval(() => {
      getProfileDetail({ id_user: userData.id_user })
        .then(async ({ data: { result: dataResponse } }) => {
          if(dataResponse.email_status === 'verified') {
            await clearInterval(backgroundIntervalEmailChecker)
            navigateTo(
              'MessageScreen',
              {
                initialLoaded: true,
                title: 'Success',
                toScreen: 'RegisterGenreInterestScreen',
                toScreenParams: { initialLoaded: true },
                message: 'You\'re now verified and your account ready to be used! You can go back to soundfren app and continue the registration'
              }
            )
          }
        })
        .catch((err) => err)
    }, 5000) // Change this to pusher
  }

  componentWillUnmount = async () => {
    await clearInterval(backgroundIntervalEmailChecker)
  }

  render() {
    const {
      isLoading,
      navigateTo,
      initialLoaded,
      userData,
      authIsLoading
    } = this.props
    const openEmail = split(userData.email, '@')[1] || 'gmail.com'
    return (
      <Container isLoading={isLoading || authIsLoading} colors={[ORANGE_BRIGHT_DARK, PINK_RED_DARK]} theme='light'>
        <Header>
          {
            !initialLoaded && (
              <Icon onPress={() => navigateTo()} size='massive' color={WHITE} name='left'/>
            )
          }
        </Header>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ marginHorizontal: WP5, flex: 1, justifyContent: 'center' }}
          keyboardShouldPersistTaps='handled'
        >
          <Text style={{ marginTop: HP2 }} centered color={WHITE} type='SansPro' weight={500} size='extraMassive'>
            Verify your Email
          </Text>
          <View>
            <View style={{ marginVertical: HP2 }}>
              <Text centered color={WHITE}>
                {userData.full_name}, to start using your Soundfren, we need to verify your email ID
                <Text centered color={WHITE} weight={500}> {userData.email}</Text>
              </Text>
            </View>
            <Button
              onPress={() => {
                NavigateToInternalBrowser({
                  url: `http://${openEmail}`
                })
              }}
              rounded
              centered
              shadow='none'
              backgroundColor={WHITE20}
              textColor={WHITE}
              text={'Verify Email'}
            />
          </View>
        </ScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, null)(RegisterVerifyEmailScreen),
  mapFromNavigationParam
)

