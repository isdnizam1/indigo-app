import React from 'react'
import { ScrollView, Image, View } from 'react-native'
import { connect } from 'react-redux'
import { noop } from 'lodash-es'
import Swiper from 'react-native-swiper'
import {
  _enhancedNavigation,
  Container,
  Header,
  Icon,
  Text
} from '../components'
import { NO_COLOR, ORANGE_BRIGHT, WHITE } from '../constants/Colors'
import { WP100 } from '../constants/Sizes'
import { getProfileDetailArt } from '../actions/api'
import { DEFAULT_PAGING } from '../constants/Routes'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  idJourney: getParam('idJourney', 0)
})

class ProfileShowArtScreen extends React.Component {
  state = {
    art: {}
  }

  componentDidMount() {
    this._getInitialScreenData()
  }

  _getInitialScreenData = async (...args) => {
    await this._getArt(...args)
    this.setState({
      isReady: true,
      isRefreshing: false
    })
  }

  _getArt = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const {
      dispatch,
      idJourney
    } = this.props
    const dataResponse = await dispatch(getProfileDetailArt, { id_journey: idJourney }, noop, true, isLoading)
    this.setState({ art: dataResponse.result || {} })
  }

  render() {
    const {
      isLoading,
      navigateBack
    } = this.props

    const {
      art
    } = this.state
    return (
      <Container isLoading={isLoading} colors={[WHITE, WHITE]}>
        <Header>
          <Icon onPress={() => navigateBack()} size='massive' color={ORANGE_BRIGHT} name='close'/>
          <Text size='massive' type='SansPro' weight={500}>Picture</Text>
          <Icon size='massive' color={NO_COLOR} name='close'/>
        </Header>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ flex: 1, marginTop: 10 }}
        >
          {
            art.attachment ?
              <View style={{ flex: 1 }}>
                <Swiper containerStyle={{ width: WP100 }}>
                  {
                    art.attachment.map((value, key) => {

                      return (
                        <Image
                          key={key} source={{ uri: value.attachment_file }}
                          style={{ width: '100%', height: undefined, aspectRatio: 1 }}
                        />

                      )
                    })
                  }
                </Swiper>

                <View style={{ padding: 10 }}>
                  <Text style={{ color: '#474747', fontWeight: 'bold' }}>
                    {art.title}
                  </Text>
                  <Text style={{ color: '#474747', marginTop: 10 }}>
                    {art.description}
                  </Text>
                </View>
              </View>
              : null
          }
        </ScrollView>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(ProfileShowArtScreen),
  mapFromNavigationParam
)
