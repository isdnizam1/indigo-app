import React from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import { _enhancedNavigation, Container, Header, Icon, Text, Image, Button } from '../../../components'
import { authDispatcher } from '../../../services/auth'
import { GET_USER_DETAIL } from '../../../services/auth/actionTypes'
import { WHITE, GREY, BLACK } from '../../../constants/Colors'
import { WP90, WP4, WP2, WP6 } from '../../../constants/Sizes'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher,
  setUserDetail: (userDetail) => ({
    type: `${GET_USER_DETAIL}_SUCCESS`,
    response: userDetail
  })
}

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false)
})

class PromoteCollab extends React.Component {
  state = {
    isReady: true,
    isRefreshing: false
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  render() {
    const {
      navigateTo
    } = this.props
    return (
      <Container
        renderHeader={() => (
          <Header style={{ justifyContent: 'flex-start' }}>
            <Icon
              size='huge' name='chevron-left' type='Entypo' centered
              onPress={this._backHandler} color={BLACK}
            />
            <Text size='large' type='SansPro' weight={500}>Create Collaboration Project</Text>
          </Header>
        )}
        scrollable
      >
        <Image
          source={require('../../../assets/images/collabsIllustration.png')}
          style={{ marginBottom: WP4 }}
          imageStyle={{ width: WP90 }}
          aspectRatio={308 / 188}
        />

        <View style={{ paddingHorizontal: WP6 }}>
          <Text color={GREY} weight={500} size='extraMassive' style={{ marginBottom: 10 }}>
            Hi There!
          </Text>
          <Text color={GREY} size='slight' style={{ marginBottom: 15 }}>
            Welcome to our Collaboration Project! We’re excited to help you to find fellow artists to collaborate and
            create music together. </Text>
          <Text color={GREY} size='slight' style={{ marginBottom: 15 }}>
            So what are you waiting for? Click button below to start your journey with us!
          </Text>
          <View style={{ flexDirection: 'row', marginTop: 10 }}>

            <Button
              onPress={() => {
                navigateTo('PromoteCollab1')
              }}
              contentStyle={{ paddingHorizontal: WP6, paddingVertical: WP2, flexGrow: 0 }}
              soundfren
              rounded
              centered
              text='CREATE COLLABORATION PROJECT'
              textColor={WHITE}
              textSize='small'
              textWeight={400}
              shadow='none'
            />
          </View>
        </View>

      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(PromoteCollab),
  mapFromNavigationParam
)
