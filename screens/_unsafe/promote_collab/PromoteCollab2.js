import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import Constants from 'expo-constants'
import { connect } from 'react-redux'
import { noop } from 'lodash-es'
import { LinearGradient } from 'expo-linear-gradient'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import HTMLElement from 'react-native-render-html'
import Header from '../../../components/Header'
import Icon from '../../../components/Icon'
import { BLACK, GREY, WHITE, GREY50, GREY20, ORANGE, PURPLE, PINK_PURPLE } from '../../../constants/Colors'
import Text from '../../../components/Text'
import Container from '../../../components/Container'
import _enhancedNavigation from '../../../navigation/_enhancedNavigation'
import { WP10, WP1, WP100, WP2, WP4, WP5, WP6, HP2, WP80, HP100, HP1, WP35, WP25, WP105 } from '../../../constants/Sizes'
import { postCollabPost } from '../../../actions/api'
import { validButtonStyle } from '../../../components/promote/PromoteConstant'
import { Image, Button, Loader } from '../../../components'
import Modal from '../../../components/Modal'
import { adConstant, ADSTYLE, collaborationStyle } from '../../../components/ads/AdsConstant'
import { toNormalDate } from '../../../utils/date'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  collabData: getParam('collabData', {})
})

class PromoteCollab2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false
    }
  }

  _onSubmit = (toggleModal) => async () => {
    const {
      dispatch, userData,
      collabData
    } = this.props

    collabData.description = collabData.description.replace(/\n/g, '<br>')

    await this.setState({ loading: true })

    await dispatch(postCollabPost, { ...collabData, id_user: userData.id_user }, noop, true, true)

    this.setState({ loading: false })

    toggleModal()
  }

  _renderPopUpMessage = () => {
    return (
      <Text size='mini' color={GREY} centered style={{ marginBottom: WP2 }}>Your Collaboration project will be reviewed by
        our team. You’re one step closer to find the right person!</Text>
    )
  }

  _renderThumbnail = () => {
    const { collabData } = this.props

    const imageUri = `data:image/gif;base64,${collabData.banner_picture}`

    return (

      <TouchableOpacity
        style={{
          width: wp('100%'),
          marginVertical: wp('3%'),
          flexDirection: 'row'
        }}
      >
        <Image
          source={{ uri: imageUri }}
          style={{ marginHorizontal: WP1, borderWidth: 1, borderColor: GREY20, borderRadius: 5, flexGrow: 0 }}
          imageStyle={{ width: WP35, aspectRatio: 140 / 90 }}
        />
        <View style={{
          flexDirectionRow: 'column',
          flexGrow: 1,
          justifyContent: 'flex-start',
          flexWrap: 'wrap',
          flex: 1,
          paddingHorizontal: wp('1%'),
          marginLeft: wp('1%')
        }}
        >
          <Text size='mini' weight={400} style={{ marginBottom: wp('1.5%') }}>{collabData.title}</Text>
          <Text size='mini'>{collabData.location_name}</Text>
          <Text size='tiny' style={{ marginBottom: wp('1%') }}>{`${toNormalDate(collabData.startDate)}`}</Text>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {
              collabData.profession.map((item, index) => (
                <View
                  key={index}
                  style={{
                    borderRadius: WP100,
                    borderColor: ORANGE,
                    borderWidth: 1,
                    paddingVertical: WP1,
                    paddingHorizontal: WP2,
                    flexGrow: 0,
                    marginVertical: WP1,
                    marginRight: WP2
                  }}
                >
                  <Text color={ORANGE} size='tiny' weight={400} style={{ flexGrow: 0 }}>{item}</Text>
                </View>
              ))
            }
          </View>
        </View>
      </TouchableOpacity>)
  }

  _renderDetail = () => {
    const {
      collabData,
      userData
    } = this.props

    const imageUri = `data:image/gif;base64,${collabData.banner_picture}`

    return (
      <View style={{ marginTop: HP2 }}>
        <Image size='full' aspectRatio={2} source={{ uri: imageUri }}/>
        <Header style={{ position: 'absolute', marginTop: Constants.statusBarHeight }}>
          <Icon
            background='dark-circle' size='large' color={WHITE}
            name='chevron-left' type='Entypo'
          />
        </Header>
        <View style={collaborationStyle.header}>
          <Image
            source={{ uri: userData.profile_picture }}
            size='massive'
            style={collaborationStyle.photoProfileWrapper}
            imageStyle={collaborationStyle.photoProfile}
          />

          <View style={{ width: WP25 }}/>
          <View style={collaborationStyle.headerDetail}>
            <Text weight={500} style={collaborationStyle.title} size='slight'>
              {collabData.title}
            </Text>
            <Text size='tiny' style={collaborationStyle.headerText}>
              {collabData.startDate}
            </Text>
            <Text size='tiny' style={collaborationStyle.headerText}>
              {collabData.location_name}
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <Text size='tiny'>
                {'Posted by '}
              </Text>
              <TouchableOpacity>
                <Text color={ORANGE} size='tiny'>
                  {userData.full_name}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={ADSTYLE.divider}/>

        <View style={{ paddingHorizontal: WP6, paddingVertical: HP2 }}>
          <View style={[ADSTYLE.sectionWrapper]}>
            <Text weight={500} size={adConstant.HEADER_FONT_SIZE} style={collaborationStyle.bodyHeading}>
              {'Role Needed'}
            </Text>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
              {
                collabData.profession.map((item, index) => (
                  <View
                    key={index}
                    style={{
                      borderRadius: WP100,
                      borderColor: ORANGE,
                      borderWidth: 1,
                      paddingVertical: WP1,
                      paddingHorizontal: WP2,
                      flexGrow: 0,
                      marginVertical: WP1,
                      marginRight: WP2
                    }}
                  >
                    <Text color={ORANGE} size='tiny' weight={400} style={{ flexGrow: 0 }}>{item}</Text>
                  </View>
                ))
              }
            </View>
          </View>

          <View style={[ADSTYLE.sectionWrapper]}>
            <Text weight={500} size={adConstant.HEADER_FONT_SIZE} style={collaborationStyle.bodyHeading}>
              {`About ${userData.full_name}`}
            </Text>
            <HTMLElement
              containerStyle={{ marginTop: HP1 }}
              html={collabData.description.replace(/\n/g, '<br>')}
              baseFontStyle={{ fontFamily: 'OpenSansRegular', fontSize: adConstant.HTML_FONT_SIZE }}
            />
          </View>

          <View style={[ADSTYLE.sectionWrapper]}>
            <Text weight={500} size={adConstant.HEADER_FONT_SIZE} style={collaborationStyle.bodyHeading}>
              {'Description'}
            </Text>
            <HTMLElement
              containerStyle={{ marginTop: HP1 }}
              html={collabData.spesification.replace(/\n/g, '<br>')}
              baseFontStyle={{ fontFamily: 'OpenSansRegular', fontSize: adConstant.HTML_FONT_SIZE }}
            />
          </View>
        </View>
      </View>
    )
  }

  render() {
    const {
      popToTop
    } = this.props

    const {
      loading
    } = this.state

    return (
      <Container
        renderHeader={() => (
          <Header style={{ justifyContent: 'flex-start' }}>
            <Icon size='huge' name='chevron-left' type='Entypo' centered onPress={this._backHandler} color={BLACK}/>
            <Text size='large' type='SansPro' weight={500}>Create Collaboration Project</Text>
          </Header>
        )}
      >
        {
          loading &&
          <View style={{ position: 'absolute', width: WP100, height: HP100, backgroundColor: GREY50, zIndex: 999 }}>
            <Loader isLoading/>
          </View>
        }

        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ backgroundColor: WHITE }}
          style={{ flexGrow: 1 }}
        >

          <View style={{ alignSelf: 'center', alignItems: 'center' }}>
            <Button
              text='Step 2 of 2'
              textColor={WHITE}
              colors={[PURPLE, PINK_PURPLE]}
              rounded
              textCentered
              centered
              style={{ marginVertical: WP2, padding: 0 }}
              textSize='tiny'
              contentStyle={{ paddingVertical: WP105, paddingHorizontal: WP2 }}
            />
          </View>

          <View style={{ paddingHorizontal: WP5, paddingBottom: WP10 }}>

            <Text size='large' weight={500}>Thumbnail View</Text>
            {
              this._renderThumbnail()
            }
            <Text size='large' weight={500}>Detail View</Text>
            {
              this._renderDetail()
            }
          </View>
        </KeyboardAwareScrollView>

        <Modal
          isVisible={false}
          position='bottom'
          closeOnBackdrop={true}
          swipeDirection='none'
          renderModalContent={({ toggleModal, payload }) => (
            <View style={{ paddingHorizontal: WP6, paddingVertical: WP4 }}>
              <Image
                source={require('../../../assets/images/promoteArtistPreview.png')}
                imageStyle={{ width: WP80 }}
                aspectRatio={2.083}
              />
              <Text size='massive' weight={600} color={GREY} centered style={{ marginBottom: WP2 }}>Hurraaay !</Text>
              {
                this._renderPopUpMessage()
              }

              <Button
                soundfren
                rounded
                style={{ flexGrow: 0, paddingHorizontal: WP6, width: undefined }}
                onPress={() => {
                  toggleModal()
                  popToTop()
                }}
                textCentered
                centered
                text='THANK YOU!'
                textSize='mini'
                textWeight={400}
              />
            </View>
          )}
        >
          {({ toggleModal }, M) => (
            <View>
              <LinearGradient
                colors={validButtonStyle.buttonColor}
                start={[0, 0]} end={[1, 0]}
                style={{ width: WP100, padding: WP4, flexGrow: 0 }}
              >
                <TouchableOpacity
                  onPress={this._onSubmit(toggleModal)}
                >
                  <Text color={WHITE} centered weight={500}>
                    SUBMIT
                  </Text>
                </TouchableOpacity>
              </LinearGradient>
              {M}
            </View>
          )}
        </Modal>
      </Container>
    )
  }
}

PromoteCollab2.propTypes = {}

PromoteCollab2.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(PromoteCollab2),
  mapFromNavigationParam
)
