import React, { Component } from 'react'
import { BackHandler, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { cloneDeep } from 'lodash-es'
import { LinearGradient } from 'expo-linear-gradient'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Header from '../../../components/Header'
import Icon from '../../../components/Icon'
import { BLACK, GREY, WHITE, GREY_PLACEHOLDER, PURPLE, PINK_PURPLE } from '../../../constants/Colors'
import Text from '../../../components/Text'
import Container from '../../../components/Container'
import _enhancedNavigation from '../../../navigation/_enhancedNavigation'
import { WP10, WP100, WP4, WP5, HP2, WP105, WP2 } from '../../../constants/Sizes'
import InputTextLight from '../../../components/InputTextLight'
import ImagePromoteForm from '../../../components/promote/ImagePromoteForm'
import { getCity } from '../../../actions/api'
import InputModal from '../../../components/InputModal'
import { invalidButtonStyle, validButtonStyle } from '../../../components/promote/PromoteConstant'
import { alertBackUnsaved } from '../../../utils/alert'
import ProfessionPromoteForm from '../../../components/promote/ProfessionPromoteForm'
import { InputDate, Button } from '../../../components'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  collabData: getParam('collabData', {})
})

class PromoteCollab1 extends Component {
  _didFocusSubscription
  _willBlurSubscription

  constructor(props) {
    super(props)
    this.state = {
      collabData: {},
      invalidField: {}
    }
    this._didFocusSubscription = props.navigation.addListener('focus', (payload) =>
      BackHandler.addEventListener('hardwareBackPress', this._backHandler)
    )
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('blur', (payload) =>
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
    )
  }

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription()
    this._willBlurSubscription && this._willBlurSubscription()
  }

  _isEmpty() {
    const { collabData } = this.state

    return !(collabData.spesification ||
      collabData.banner_picture ||
      collabData.title ||
      collabData.profession ||
      collabData.location_name ||
      collabData.phone ||
      collabData.description)

  }

  _backHandler = async () => {
    if (this._isEmpty()) {
      this.props.navigateBack()
    } else {
      alertBackUnsaved(this.props.navigateBack)
    }
  }

  _isValid = () => {
    const { collabData } = this.state

    if (!collabData.banner_picture) return invalidButtonStyle
    if (!collabData.title) return invalidButtonStyle
    if (!collabData.profession) return invalidButtonStyle
    if (!collabData.location_name) return invalidButtonStyle
    if (!collabData.startDate) return invalidButtonStyle
    if (!collabData.phone) return invalidButtonStyle
    if (!collabData.description) return invalidButtonStyle
    if (!collabData.spesification) return invalidButtonStyle

    return validButtonStyle
  }

  _onChange = (key) => (value) => {
    const updatedArtistData = cloneDeep(this.state.collabData)
    updatedArtistData[key] = value
    this.setState({ collabData: updatedArtistData })
  }

  render() {
    const {
      navigateTo
    } = this.props

    const {
      collabData
    } = this.state

    const isValid = this._isValid()

    return (
      <Container
        renderHeader={() => (
          <Header style={{ justifyContent: 'flex-start' }}>
            <Icon size='huge' name='chevron-left' type='Entypo' centered onPress={this._backHandler} color={BLACK}/>
            <Text size='large' type='SansPro' weight={500}>Create Collaboration Project</Text>
          </Header>
        )}
      >
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ backgroundColor: WHITE }}
          style={{ flexGrow: 1 }}
        >
          <View style={{ alignSelf: 'center', alignItems: 'center' }}>
            <Button
              text='Step 1 of 2'
              textColor={WHITE}
              colors={[PURPLE, PINK_PURPLE]}
              rounded
              textCentered
              centered
              style={{ marginVertical: WP2, padding: 0 }}
              textSize='tiny'
              contentStyle={{ paddingVertical: WP105, paddingHorizontal: WP2 }}
            />
          </View>

          <View style={{ paddingHorizontal: WP5, paddingBottom: WP10 }}>
            <View style={{ alignItems: 'center' }}>
              <Text color={GREY} centered weight={500}>
                Collaboration Banner
              </Text>

              <View style={{ width: wp('54%') }}>
                <ImagePromoteForm
                  image={collabData.banner_picture}
                  onChange={this._onChange('banner_picture')}
                  aspectRatio={[2, 1]}
                  imageAspectRatio={2}
                />
              </View>
            </View>

            <InputTextLight
              style={{ marginTop: HP2 }}
              value={collabData.title}
              label='Collaboration Project Title'
              labelWeight={400}
              labelColor={GREY}
              placeholder='Collab with me'
              placeholderTextColor={GREY_PLACEHOLDER}
              onChangeText={this._onChange('title')}
            />

            <ProfessionPromoteForm
              profesion={collabData.profession}
              onChange={this._onChange('profession')}
            />

            <InputModal
              triggerComponent={(
                <InputTextLight
                  value={collabData.location_name}
                  label='Where is the location of the collaboration?'
                  labelWeight={400}
                  labelColor={GREY}
                  placeholderTextColor={GREY_PLACEHOLDER}
                  placeholder='Jakarta'
                  editable={false}
                />
              )}
              header='Select Location'
              suggestion={getCity}
              suggestionKey='city_name'
              suggestionPathResult='city_name'
              suggestionPathValue='id_city'
              onChange={({ city_name }) => this._onChange('location_name')(city_name)}
              suggestionCreateNewOnEmpty={false}
              asObject={true}
              createNew={false}
              placeholder='Select city here...'
            />

            <InputDate
              value={collabData.startDate}
              labelWeight={400}
              labelColor={GREY}
              placeholder='31 Oktober 2019'
              label='When will the collaboration starts?'
              onChangeText={this._onChange('startDate')}
            />

            <InputTextLight
              value={collabData.phone}
              label='What is your phone number?'
              labelWeight={400}
              labelColor={GREY}
              keyboardType='phone-pad'
              placeholder='08-xxx-xxx-xxx'
              placeholderTextColor={GREY_PLACEHOLDER}
              onChangeText={this._onChange('phone')}
            />

            <InputTextLight
              bordered
              value={collabData.spesification}
              label='Is there any requirements/specifications?'
              multiline={true}
              size='slight'
              labelWeight={400}
              labelColor={GREY}
              placeholderTextColor={GREY_PLACEHOLDER}
              placeholder='I want a drummer who is interested in Jazz, or i want a vocalist with an ability to sing dangdut, etc. You can also tell us in Bahasa!'
              onChangeText={this._onChange('spesification')}
            />

            <InputTextLight
              bordered
              value={collabData.description}
              label='About You'
              multiline={true}
              size='slight'
              labelWeight={400}
              labelColor={GREY}
              placeholderTextColor={GREY_PLACEHOLDER}
              placeholder='Please tell us about you or your band. It’s ok if you want to type in Bahasa'
              onChangeText={this._onChange('description')}
            />

          </View>
        </KeyboardAwareScrollView>

        <LinearGradient
          colors={isValid.buttonColor}
          start={[0, 0]} end={[1, 0]}
          style={{ width: WP100, padding: WP4, flexGrow: 0 }}
        >
          <TouchableOpacity
            disabled={isValid.disabled}
            onPress={async () => {
              navigateTo('PromoteCollab2', { collabData })
            }}
          >
            <Text color={WHITE} centered weight={500}>
              NEXT
            </Text>
          </TouchableOpacity>
        </LinearGradient>

      </Container>
    )
  }
}

PromoteCollab1.propTypes = {}

PromoteCollab1.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(PromoteCollab1),
  mapFromNavigationParam
)
