import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import * as WebBrowser from 'expo-web-browser'
import { _enhancedNavigation, Container, Image, Text } from '../../components'
import { SILVER_CALMER, WHITE } from '../../constants/Colors'
import { HP1, WP10, WP100, WP20, WP4, WP5, WP75, WP90 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { authDispatcher } from '../../services/auth'
import env from '../../utils/env'
import HeaderNormal from '../../components/HeaderNormal'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', () => {
  }),
  navigateBackOnDone: getParam('navigateBackOnDone', false)
})

class ProfileAddSongScreen extends React.Component {
  _handlePressButtonAsync = async () => {
    const {
      userData,
      navigateBack
    } = this.props
    const lagukuRegistration = `${env.webUrl}/register/laguku`
    const queries = `?id_user=${userData.id_user}`
    await WebBrowser.openBrowserAsync(lagukuRegistration + queries)
    navigateBack()
  }
  render() {
    const {
      isLoading,
      navigateBack,
      navigateTo,
      refreshProfile,
      navigateBackOnDone
    } = this.props

    return (
      <Container
        isLoading={isLoading} colors={[WHITE, WHITE]}
        borderedHeader
        scrollable
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text='Add Song'
            centered
          />
        )}
      >
        <View style={{ alignItems: 'center', paddingVertical: HP1, paddingHorizontal: WP4 }}>
          <Image size={WP75} source={require('../../assets/images/addMusic.png')} aspectRatio={222 / 205} />

          <TouchableOpacity
            style={{ alignItems: 'center', marginTop: WP10 }}
            activeOpacity={TOUCH_OPACITY} onPress={() => {
              navigateTo('SongLinkForm', { refreshProfile }, navigateBackOnDone ? 'replace' : undefined)
            }}
          >
            <View
              style={
                {
                  marginHorizontal: WP5, padding: WP4, flexDirection: 'row',
                  alignItems: 'center', backgroundColor: SILVER_CALMER,
                  width: WP90,
                  borderRadius: WP100 }
              }
            >
              <Image size={WP20} source={require('../../assets/images/uploadLink.png')} />
              <View style={{ paddingHorizontal: WP4 }}>
                <Text type='SansPro' weight={500}>Add songs from link</Text>
                <Text size='tiny'>Spotify, Joox, Soundcloud, etc</Text>
              </View>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={{ alignItems: 'center', marginTop: WP5 }}
            activeOpacity={TOUCH_OPACITY} onPress={() => {
              navigateTo('SongFileForm', { refreshProfile }, navigateBackOnDone ? 'replace' : undefined)
            }}
          >
            <View
              style={
                {
                  marginHorizontal: WP5, padding: WP4, flexDirection: 'row',
                  alignItems: 'center', backgroundColor: SILVER_CALMER,
                  width: WP90,
                  borderRadius: WP100
                }
              }
            >
              <Image size={WP20} source={require('../../assets/images/uploadFile.png')} />
              <View style={{ paddingHorizontal: WP4 }}>
                <Text type='SansPro' weight={500}>Add Songs from Storage</Text>
                <Text size='tiny'>Internal phone storage</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProfileAddSongScreen),
  mapFromNavigationParam
)
