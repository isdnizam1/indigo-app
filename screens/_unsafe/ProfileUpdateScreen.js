import React from 'react'
import { Keyboard, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { isEmpty, noop, reduce } from 'lodash-es'
import { CheckBox } from 'react-native-elements'
import { LinearGradient } from 'expo-linear-gradient'
import { _enhancedNavigation, Container, Form, Header, Icon, InputTextLight, Text } from '../../components'
import { GREY, GREY_BACKGROUND, GREY_LABEL, GREY_PLACEHOLDER, ORANGE_BRIGHT, PINK_RED, WHITE } from '../../constants/Colors'
import { WP1, WP100, WP105, WP2, WP3, WP30, WP4, WP5, WP55 } from '../../constants/Sizes'
import { authDispatcher } from '../../services/auth'
import { getCity, getCompany, getJob, postProfileUpdate } from '../../actions/api'
import InputModal from '../../components/InputModal'
import ImagePromoteForm from '../../components/promote/ImagePromoteForm'
import SocialMediaCard from '../../components/profile/SocialMediaCard'
import { BORDER_COLOR, BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'

export const updateProfileStyle = StyleSheet.create({
  section: {
    backgroundColor: WHITE,
    paddingHorizontal: WP5,
    paddingVertical: WP3,
    marginTop: WP105
  }
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  getUserDetailDispatcher: authDispatcher.authGetUserDetailDispatcher
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', () => {
  })
})

class ProfileUpdateScreen extends React.Component {
  state = {
    photoProfile: null,
    coverImage: null,
    cityName: '',
    countyName: ''
  }
  _onUpdateProfile = async ({ formData, isDirty }) => {
    const {
      userData,
      navigateBack,
      refreshProfile,
      dispatch,
      getUserDetailDispatcher
    } = this.props
    const {
      photoProfile,
      coverImage
    } = this.state

    const updatedUserData = {
      'id_user': userData.id_user,
      'full_name': formData.fullName,
      'job_title': formData.job_title,
      'another_job_title': formData.another_job_title,
      'company_name': formData.company_name,
      'another_company_name': formData.another_company_name,
      'id_city': isEmpty(formData.city) ? userData.location ? userData.location.id_city : null : formData.city.id_city,
      'id_country': isEmpty(formData.country) ? userData.location ? userData.location.id_country : null : formData.country.id_country,
      'profile_picture': photoProfile || undefined,
      'cover_image': coverImage || undefined,
      'profile_name': photoProfile ? 'profile.jpg' : undefined,
      social_media: {
        instagram: formData.instagram,
        twitter: formData.twitter,
        youtube: formData.youtube,
        facebook: formData.facebook,
        website: formData.website
      },
      private_gender: formData.private_gender ? 0 : 1,
      about_me: formData.about_me,
      gender: formData.gender
    }

    await dispatch(postProfileUpdate, updatedUserData, noop, true)
    getUserDetailDispatcher({ id_user: userData.id_user })
    refreshProfile()
    navigateBack()
  }

  _onChangeImage = (key) => (base64) => {
    this.setState({ [key]: base64 })
  }

  render() {
    const {
      userData: user,
      isLoading,
      navigateBack
    } = this.props
    const {
      photoProfile,
      coverImage
    } = this.state

    return (
      <Container isLoading={isLoading} colors={[WHITE, WHITE]}>
        <Form
          initialValue={{
            fullName: user.full_name,
            job_title: user.job_title,
            another_job_title: reduce(user.job, (result, value, key) => {
              result.push(value.job_title)
              return result
            }, []),
            company_name: user.company,
            another_company_name: reduce(user.job, (result, value, key) => {
              result.push(value.company_name)
              return result
            }, []),
            city: user.location,
            country: user.location,
            gender: user.gender,
            instagram: user.social_media.instagram,
            twitter: user.social_media.twitter,
            youtube: user.social_media.youtube,
            facebook: user.social_media.facebook,
            website: user.social_media.website,
            about_me: user.about_me,
            private_gender: user.private_gender == 0
          }} onSubmit={this._onUpdateProfile}
        >
          {({ onChange, onSubmit, formData }) => (
            <View style={{ flex: 1 }}>
              <Header>
                <Icon onPress={() => navigateBack()} size='massive' color={ORANGE_BRIGHT} name='left'/>
                <Text style={{ alignSelf: 'flex-start' }} size='massive' type='SansPro' weight={500}>Edit Profile</Text>
                <TouchableOpacity
                  onPress={() => {
                    Keyboard.dismiss()
                    onSubmit()
                  }} activeOpacity={TOUCH_OPACITY}
                >
                  <Text size='large' color={ORANGE_BRIGHT}>Save</Text>
                </TouchableOpacity>
              </Header>
              <ScrollView
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps='handled'
                style={{ backgroundColor: GREY_BACKGROUND }}
              >

                <View style={[updateProfileStyle.section, { flexDirection: 'row', justifyContent: 'space-between' }]}>
                  <View style={{ width: WP30 }}>
                    <ImagePromoteForm
                      label='Profile Picture'
                      image={photoProfile || user.profile_picture}
                      onChange={this._onChangeImage('photoProfile')}
                      aspectRatio={[1, 1]}
                      imageAspectRatio={1}
                      rounded
                      base64={photoProfile != null}
                    />
                  </View>

                  <View style={{ width: WP55 }}>
                    <ImagePromoteForm
                      label='Page Banner'
                      image={coverImage || user.cover_image}
                      onChange={this._onChangeImage('coverImage')}
                      aspectRatio={[3, 1]}
                      imageAspectRatio={2}
                      base64={coverImage != null}
                    />
                  </View>
                </View>

                <View style={[updateProfileStyle.section]}>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ width: WP30, flexDirection: 'row', alignItems: 'center' }}>
                      <Text weight={500} color={GREY} size='mini'>Name</Text>
                    </View>
                    <View style={{ flexGrow: 1 }}>
                      <InputTextLight
                        value={formData.fullName}
                        withLabel={false}
                        onChangeText={onChange('fullName')}
                      />
                    </View>
                  </View>

                  <InputModal
                    triggerComponent={(
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={{ width: WP30, flexDirection: 'row', alignItems: 'center' }}>
                          <Text weight={500} color={GREY} size='mini'>City</Text>
                        </View>
                        <View style={{ flexGrow: 1 }}>
                          <InputTextLight
                            value={isEmpty(formData.city) ? '' : formData.city.city_name}
                            withLabel={false}
                            editable={false}
                            onChangeText={onChange('fullName')}
                          />
                        </View>
                      </View>
                    )}
                    header='Select City'
                    suggestion={getCity}
                    suggestionKey='city_name'
                    suggestionPathResult='city_name'
                    suggestionPathValue='id_city'
                    onChange={onChange('city')}
                    suggestionCreateNewOnEmpty={false}
                    selected={isEmpty(formData.city) ? '' : formData.city.city_name}
                    asObject={true}
                    createNew={false}
                  />

                  <Text weight={500} color={GREY} size='mini'>About Me</Text>
                  <InputTextLight
                    value={formData.about_me}
                    withLabel={false}
                    multiline
                    bordered
                    maxLength={160}
                    onChangeText={onChange('about_me')}
                  />
                </View>

                <View style={[updateProfileStyle.section]}>
                  <Text weight={500} color={GREY_PLACEHOLDER} style={{ marginVertical: WP1 }}>Headline Job</Text>
                  <View style={{
                    borderWidth: BORDER_WIDTH,
                    borderColor: BORDER_COLOR,
                    borderRadius: WP2,
                    paddingHorizontal: WP4,
                    paddingVertical: WP2
                  }}
                  >
                    <InputModal
                      triggerComponent={(
                        <InputTextLight
                          bold
                          value={formData.job_title}
                          label='Job title'
                          placeholder='Ex: Vocalist'
                          editable={false}
                        />
                      )}
                      header='Select profession'
                      suggestion={getJob}
                      suggestionKey='job_title'
                      suggestionPathResult='job_title'
                      onChange={onChange('job_title')}
                      selected={formData.job_title}
                      placeholder='Search job here...'
                    />

                    <InputModal
                      triggerComponent={(
                        <InputTextLight
                          bold
                          value={formData.company_name}
                          label='Company'
                          placeholder='Ex: Self Employed'
                          editable={false}
                        />
                      )}
                      header='Select Band or Company'
                      suggestion={getCompany}
                      suggestionKey='company_name'
                      suggestionPathResult='company_name'
                      onChange={onChange('company_name')}
                      selected={formData.company_name}
                      placeholder='Search company here...'
                    />
                  </View>

                  <Text weight={500} color={GREY_PLACEHOLDER} style={{ marginVertical: WP1, marginTop: WP3 }}>Another Job</Text>
                  <View style={{
                    borderWidth: BORDER_WIDTH,
                    borderColor: BORDER_COLOR,
                    borderRadius: WP2,
                    paddingHorizontal: WP4,
                    paddingVertical: WP2
                  }}
                  >
                    {
                      formData.another_job_title.map((job, index) => (
                        <View
                          key={index}
                          style={{
                            borderWidth: BORDER_WIDTH,
                            borderColor: BORDER_COLOR,
                            borderRadius: WP2,
                            paddingHorizontal: WP4,
                            paddingVertical: WP2,
                            marginTop: WP1,
                            borderStyle: 'dashed'
                          }}
                        >
                          <TouchableOpacity
                            onPress={() => {
                              const {
                                another_job_title,
                                another_company_name
                              } = formData

                              another_job_title.splice(index, 1)
                              another_company_name.splice(index, 1)
                              onChange('another_job_title', true)(another_job_title, true)
                              onChange('another_company_name', true)(another_company_name, true)
                            }}
                            style={{
                              backgroundColor: GREY_LABEL,
                              padding: 2,
                              flexGrow: 0,
                              position: 'absolute',
                              right: 4,
                              top: 5,
                              borderRadius: WP100
                            }}
                          >
                            <Icon centered name='close' color={WHITE} size='mini'/>
                          </TouchableOpacity>
                          <InputModal
                            triggerComponent={(
                              <InputTextLight
                                bold
                                value={formData.another_job_title[index]}
                                label='Another Job title'
                                placeholder='Ex: Vocalist'
                                editable={false}
                              />
                            )}
                            header='Select profession'
                            suggestion={getJob}
                            suggestionKey='job_title'
                            suggestionPathResult='job_title'
                            onChange={onChange('another_job_title', true, index)}
                            selected={formData.another_job_title[index]}
                            placeholder='Search job here...'
                          />

                          <InputModal
                            triggerComponent={(
                              <InputTextLight
                                bold
                                value={formData.another_company_name[index]}
                                label='Another Company'
                                placeholder='Ex: Self Employed'
                                editable={false}
                              />
                            )}
                            header='Select Band or Company'
                            suggestion={getCompany}
                            suggestionKey='company_name'
                            suggestionPathResult='company_name'
                            onChange={onChange('another_company_name', true, index)}
                            selected={formData.another_company_name[index]}
                            placeholder='Search company here...'
                          />
                        </View>
                      ))
                    }

                    <TouchableOpacity
                      onPress={() => {
                        onChange('another_job_title', true)(formData.job_title)
                        onChange('another_company_name', true)(formData.company_name)
                      }}
                      activeOpacity={TOUCH_OPACITY}
                      style={{ flexDirection: 'row', justifyContent: 'center', marginTop: !isEmpty(formData.another_job_title) ? WP2 : 0 }}
                    >
                      <Icon centered name='pluscircleo' color={ORANGE_BRIGHT} size='slight'/>
                      <Text style={{ flexGrow: 0, marginHorizontal: WP2 }} size='mini' weight={500} color={ORANGE_BRIGHT}>
                        Add another job
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={[updateProfileStyle.section]}>
                  <Text size='slight' weight={500} color={GREY_LABEL}>Social Media</Text>
                  <SocialMediaCard
                    icon={require('../../assets/icons/social_media/instagram.png')}
                    value={formData.instagram}
                    placeholder='https://instagram.com/'
                    onChange={onChange('instagram')}
                  />
                  <SocialMediaCard
                    icon={require('../../assets/icons/social_media/twitter.png')}
                    value={formData.twitter}
                    placeholder='https://twitter.com/'
                    onChange={onChange('twitter')}
                  />
                  <SocialMediaCard
                    icon={require('../../assets/icons/social_media/youtube.png')}
                    value={formData.youtube}
                    placeholder='https://youtube.com/'
                    onChange={onChange('youtube')}
                  />
                  <SocialMediaCard
                    icon={require('../../assets/icons/social_media/facebook.png')}
                    value={formData.facebook}
                    placeholder='https://facebook.com/'
                    onChange={onChange('facebook')}
                  />
                  <SocialMediaCard
                    icon={require('../../assets/icons/social_media/website.png')}
                    value={formData.website}
                    placeholder='https://website.com/'
                    onChange={onChange('website')}
                  />
                </View>

                <View style={[updateProfileStyle.section]}>
                  <Text size='slight' weight={500} color={GREY_LABEL}>Private Information</Text>
                  <View style={{ alignItems: 'flex-end' }}>
                    <Text
                      size='mini' weight={500} color={GREY_LABEL}
                      style={{ position: 'absolute', top: WP2 }}
                    >Show on profile</Text>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flexGrow: 0, flexDirection: 'row', alignItems: 'flex-end' }}>
                      <CheckBox
                        checked={formData.private_gender}
                        onPress={() => {
                          onChange('private_gender')(!formData.private_gender)
                        }}
                      />
                    </View>
                  </View>

                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flexGrow: 1 }}>
                      <InputTextLight
                        value={user.email}
                        label='Email'
                        bold
                        disabled
                        labelColor={GREY_LABEL}
                        size='mini'
                        editable={false}
                      />
                    </View>
                  </View>

                </View>
              </ScrollView>
              <LinearGradient
                colors={[PINK_RED, ORANGE_BRIGHT]}
                start={[0, 0]} end={[1, 0]}
                style={{ width: WP100, padding: WP4, flexGrow: 0 }}
              >
                <TouchableOpacity
                  onPress={() => {
                    Keyboard.dismiss()
                    onSubmit()
                  }}
                >
                  <Text color={WHITE} centered weight={500}>
                    SAVE
                  </Text>
                </TouchableOpacity>
              </LinearGradient>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ProfileUpdateScreen),
  mapFromNavigationParam
)
