import React, { Component } from "react";
import { connect } from "react-redux";
import { find, isEmpty, noop, toLower } from "lodash-es";
import { NavigationEvents } from "@react-navigation/compat";
import {
  BackHandler,
  ToastAndroid,
  TouchableOpacity,
  View,
} from "react-native";
import { GREY, SHIP_GREY_CALM, WHITE } from "sf-constants/Colors";
import { Container, Icon, Image, Text } from "sf-components";
import {
  WP1,
  WP10,
  WP100,
  WP105,
  WP2,
  WP3,
  WP4,
  WP5,
  WP8,
  WP80,
} from "sf-constants/Sizes";

import _enhancedNavigation from "sf-navigation/_enhancedNavigation";
import {
  getAds,
  getAllAds,
  getSettingDetail,
  postLogPremium,
} from "sf-actions/api";
import { minimizeApp } from "sf-utils/backhandler";
import {
  getNotificationFlagCache,
  setExploreCache,
  setNotificationFlagCache,
} from "sf-utils/storage";
import { setExploreData } from "sf-services/screens/lv0/explore/actionDispatcher";
import {
  setActivityNotifications,
  setNotificationFlag,
  setUpdateNotifications,
} from "sf-services/notification/actionDispatcher";
import InteractionManager from "sf-utils/InteractionManager";
import * as Notifications from "expo-notifications";
import { NavigateToInternalBrowserNoTab } from "sf-utils/helper";
import Modal from "sf-components/Modal";
import { TOUCH_OPACITY } from "sf-constants/Styles";
import ImageAuto from "sf-components/ImageAuto";
import AnnouncementSectionV2 from "sf-components/home/AnnouncementSectionV2";
import CategorySection from "sf-components/home/CategorySection";
import HomeSection from "sf-components/home/HomeSection";
import { getNotifMappingAction } from "sf-components/notification/NotifAction";
import { restrictedAction } from "../utils/helper";
import { getNotificationMapping } from "../actions/api";
import { setNotifMappingList } from "../utils/storage";
import notificationPushRegister from "../utils/notificationPushRegister";

const mapStateToProps = ({
  auth,
  message,
  banner,
  screen: {
    lv0: { explore },
  },
}) => ({
  userData: auth.user,
  newMessageCount: message.newMessageCount,
  bannerPopup: banner.banner,
  explore,
});

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam("initialLoaded", false),
});

const mapDispatchToProps = {
  setExploreData,
  setNotificationFlag,
  setUpdateNotifications,
  setActivityNotifications,
};

class ExploreScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: "",
      isRefreshing: !props.explore.isLoaded,
      activeLearn: "",
      exitable: false,
    };
    this.attachBackAction = this.attachBackAction.bind(this);
    this.releaseBackAction = this.releaseBackAction.bind(this);
    this._refreshData = this._refreshData.bind(this);
    this._getContent = this._getContent.bind(this);
    this._getAdList = this._getAdList.bind(this);
    this._getPackages = this._getPackages.bind(this);
    this._handleReceiveNotification =
      this._handleReceiveNotification.bind(this);
  }
  //
  // shouldComponentUpdate(nextProps, nextState) {
  //   const shouldUpdate = !isEqualObject(this.props, nextProps) || this.state.isRefreshing != nextState.isRefreshing
  //   return shouldUpdate
  // }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener(
      "focus",
      this.attachBackAction
    );
    this.blurListener = this.props.navigation.addListener(
      "blur",
      this.releaseBackAction
    );
    InteractionManager.runAfterInteractions(() => {
      this._getAdList("announcementList", "announcement");
      this._getContent();
      this._getNotificationMapping();
      // __DEV__ && this.props.navigateTo('PodcastCategoryScreen', {})
    });
    getNotificationFlagCache().then((value) => {
      this.props.setNotificationFlag(!!value);
    });
    Notifications.addNotificationReceivedListener(
      this._handleReceiveNotification
    );
  }

  componentWillUnmount() {
    this.releaseBackAction();
  }

  attachBackAction() {
    const backConfirmation = () => {
      const { exitable } = this.state;
      if (exitable) minimizeApp();
      else
        this.setState({ exitable: true }, () => {
          ToastAndroid.show("Press again to exit.", ToastAndroid.SHORT);
          this.timeout = setTimeout(
            () => this.setState({ exitable: false }),
            2000
          );
        });
      return true;
    };
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backConfirmation.bind(this)
    );
  }

  releaseBackAction() {
    typeof this.backHandler === "object" &&
      typeof this.backHandler.remove === "function" &&
      this.backHandler.remove();
  }

  _handleReceiveNotification = (notification) => {
    this.props.setNotificationFlag(true);
    setNotificationFlagCache(true);
  };

  _getNotificationMapping = async () => {
    const { dispatch, userData } = this.props;
    const mapping = await dispatch(getNotificationMapping, {});
    await setNotifMappingList(mapping);
    await notificationPushRegister(userData.id_user);
  };

  _getPackages = async () => {
    const { dispatch } = this.props;
    const dataResponse = await dispatch(
      getSettingDetail,
      {
        setting_name: "premium_member",
        key_name: "package",
      },
      noop,
      true,
      false
    );
    const packages = JSON.parse(dataResponse.result.value);
    this.setState({
      packages,
    });
  };

  _getContent = async () => {
    const { dispatch, userData } = this.props;
    const { activeLearn } = this.state;

    try {
      const { result } = await dispatch(
        getAllAds,
        {
          start: 0,
          limit: 5,
          id_user: userData.id_user,
        },
        noop,
        true,
        false
      );
      await this._getPackages();

      this.props.setExploreData({
        categoryList: result.category,
        artistList: result.artist,
        bandList: result.band,
        gamificationData: result.gamification,
        soundconnectList: result.sc_session,
        videoList: result.video,
        soundplayList: result.sp_album,
        eventAuditionList: result.event_audition,
        collaborationList: result.collaboration,
        newsUpdateList: result.news_update,
        isLoaded: true,
      });
      this.setState({
        activeLearn: !isEmpty(activeLearn)
          ? activeLearn === "podcast" && result.sp_album.length > 0
            ? "podcast"
            : activeLearn === "video" && result.video.length > 0
            ? "video"
            : ""
          : result.sp_album.length > 0
          ? "podcast"
          : "video",
      });
    } finally {
      this.setState(
        {
          isRefreshing: false,
        },
        () => {
          setExploreCache(this.props.explore);
        }
      );
    }
  };
  _getAdList = async (listName, category) => {
    const {
      dispatch,
      userData: { id_user },
    } = this.props;
    try {
      const newCategory =
        category === "soundconnect"
          ? "sc_session"
          : category === "soundplay"
          ? "sp_album"
          : category;
      const data = await dispatch(
        getAds,
        {
          status: "active",
          category: newCategory,
          start: 0,
          limit: 5,
          id_user,
        },
        noop,
        true,
        false
      );
      if (data.code === 200) {
        this.props.setExploreData({
          [listName]: data.result,
        });
      }
    } catch (e) {
      // keep silent
    }
  };
  _onPullDownToRefresh = () => {
    this.setState(
      {
        isRefreshing: true,
      },
      this._refreshData
    );
  };

  _refreshData = () => {
    this._getAdList("announcementList", "announcement");
    this._getContent();
  };

  _onClickBanner = async () => {
    const { packages } = this.state;
    const { bannerPopup, navigateTo, dispatch, userData, paymentSourceSet } =
      this.props;

    if (bannerPopup.bannerId === "1") {
      if (packages) {
        const currentPackage = find(
          packages,
          (packageDetail) => toLower(packageDetail.package_name) === "maestro"
        );
        dispatch(
          postLogPremium,
          {
            id_user: userData.id_user,
            previous_screen: "Popup Maestro",
          },
          noop,
          true,
          true
        );
        paymentSourceSet("Popup Maestro");
        navigateTo("PromoteUser3", {
          selectedPackage: currentPackage,
        });
      }
    } else {
      if (!bannerPopup.internal_link) {
        NavigateToInternalBrowserNoTab({
          url: bannerPopup.link,
        });
      } else {
        const notifAction = await getNotifMappingAction({
          url_mobile: "internal_link",
          url: bannerPopup.link,
        });
        if (notifAction) {
          navigateTo(notifAction.to, notifAction.payload);
        }
      }
    }
  };

  _renderHeader = () => {
    const { newMessageCount, navigateTo } = this.props;
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingHorizontal: WP4,
          paddingVertical: WP2,
        }}
      >
        <Image
          imageStyle={{
            width: undefined,
            height: WP8,
            aspectRatio: 1,
            borderRadius: 6,
          }}
          source={require("../assets/icons/logo/indigo_icon_bg.png")}
          onPress={() => navigateTo("EventSelectionScreen")}
        />

        <TouchableOpacity
          onPress={restrictedAction({
            action: () => navigateTo("SearchScreen"),
            userData: this.props.userData,
            navigation: this.props.navigation,
          })}
          activeOpacity={0.75}
          style={{
            flex: 1,
            backgroundColor: "rgba(145, 158, 171, 0.1)",
            borderRadius: 6,
            flexDirection: "row",
            alignItems: "center",
            paddingHorizontal: WP2,
            paddingVertical: WP1,
            marginHorizontal: WP2,
          }}
        >
          <Icon
            centered
            background="dark-circle"
            size="large"
            color={SHIP_GREY_CALM}
            name="magnify"
            type="MaterialCommunityIcons"
            style={{ marginRight: WP1 }}
          />
          <Text
            type="Circular"
            size="mini"
            weight={300}
            color={SHIP_GREY_CALM}
            centered
          >
            Coba cari webinar
          </Text>
        </TouchableOpacity>

        <View
          style={{
            paddingLeft: WP105,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Image
            imageStyle={{
              width: undefined,
              height: WP5,
              aspectRatio: 1,
            }}
            source={
              // newMessageCount > 0
              //   ? require("sf-assets/icons/icPaperPlaneWithCircle.png")
              require("sf-assets/icons/icPaperPlaneGrey.png")
            }
            style={{ margin: -WP3, padding: WP3 }}
            onPress={() => {
              navigateTo("ChatScreen", {
                fromScreen: "ExploreScreen",
              });
            }}
          />
        </View>
      </View>
    );
  };

  render() {
    const {
      navigateTo,
      userData: { id_user },
      explore: {
        announcementList,
        categoryList,
        artistList,
        gamificationData,
        soundconnectList,
        videoList,
        soundplayList,
        eventAuditionList,
        collaborationList,
        newsUpdateList,
        isLoaded,
      },
      bannerPopup,
    } = this.props;
    const listLearn = {
      podcast: soundplayList,
      video: videoList,
    };

    const { isRefreshing, packages, activeLearn } = this.state;
    return (
      <Container
        theme="dark"
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={this._onPullDownToRefresh}
        isReady
        type="horizontal"
        hasBottomNavbar
        borderedHeader
        renderHeader={this._renderHeader}
        outsideContent={() => (
          <>
            {!isEmpty(bannerPopup) && !isEmpty(packages) ? (
              <Modal
                transparent
                isVisible={true}
                position="center"
                animationIn="fadeIn"
                animationOut="fadeOut"
                backgroundColor={"transparent"}
                modalStyle={{ alignSelf: "center", width: WP80 }}
                renderModalContent={({ toggleModal }) => (
                  <View
                    style={{
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity
                      activeOpacity={TOUCH_OPACITY}
                      onPress={() => {
                        this._onClickBanner();
                        toggleModal();
                      }}
                    >
                      <ImageAuto
                        source={{ uri: bannerPopup.image }}
                        width={WP80}
                      />
                    </TouchableOpacity>
                    <View
                      style={{
                        position: "absolute",
                        top: -WP4,
                        right: -WP4,
                      }}
                    >
                      <Icon
                        backgroundColor={WHITE}
                        style={{ borderRadius: WP100 }}
                        color={GREY}
                        size={WP10}
                        name="closecircle"
                        type="AntDesign"
                        onPress={() => toggleModal()}
                      />
                    </View>
                  </View>
                )}
              >
                {({ toggleModal }, M) => <View>{M}</View>}
              </Modal>
            ) : null}
          </>
        )}
      >
        <NavigationEvents onDidFocus={this._refreshData} />
        <View>
          <AnnouncementSectionV2
            navigateBack={this.props.navigateBack}
            navigateTo={navigateTo}
            navigation={this.props.navigation}
            idUser={this.props.userData.id_user}
            list={announcementList}
            loading={!isLoaded || isRefreshing}
          />

          <CategorySection
            navigateTo={navigateTo}
            navigation={this.props.navigation}
            list={categoryList}
            loading={!isLoaded || isRefreshing}
            idUser={id_user}
            activeLearn={activeLearn}
          />

          <HomeSection
            navigateBack={this.props.navigateBack}
            idUser={this.props.userData.id_user}
            navigateTo={navigateTo}
            navigation={this.props.navigation}
            category="soundconnect"
            adList={soundconnectList}
            loading={!isLoaded || isRefreshing}
          />
          <HomeSection
            navigateBack={this.props.navigateBack}
            idUser={this.props.userData.id_user}
            navigateTo={navigateTo}
            navigation={this.props.navigation}
            category="learn"
            adList={listLearn}
            loading={!isLoaded || isRefreshing}
            activeLearn={activeLearn}
            onSelectCategory={(value) => this.setState({ activeLearn: value })}
          />
          <HomeSection
            navigateBack={this.props.navigateBack}
            idUser={this.props.userData.id_user}
            navigateTo={navigateTo}
            navigation={this.props.navigation}
            category="event_audition"
            adList={eventAuditionList}
            loading={!isLoaded || isRefreshing}
          />
          <HomeSection
            navigateBack={this.props.navigateBack}
            idUser={this.props.userData.id_user}
            navigateTo={navigateTo}
            navigation={this.props.navigation}
            category="collaboration"
            adList={collaborationList}
            loading={!isLoaded || isRefreshing}
          />
        </View>
      </Container>
    );
  }
}

ExploreScreen.propTypes = {};

ExploreScreen.defaultProps = {};

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ExploreScreen),
  mapFromNavigationParam
);
