import React from 'react'
import { View } from 'react-native'
import { withNavigation } from '@react-navigation/compat'
import { connect } from 'react-redux'
import { isEmpty, map } from 'lodash-es'
import { _enhancedNavigation, Button, DeletePost, Modal } from '../components'
import { TOMATO, WHITE } from '../constants/Colors'
import { HP2, WP4, WP50, WP90 } from '../constants/Sizes'
import FeedItemGeneral from '../components/feed/FeedItemGeneral'
import EmptyV2 from '../components/EmptyV2'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
})

const defaultFeeds = [{ type: 'post' }, { type: 'post' }, { type: 'post' }]

class ProfileMyPostTab extends React.Component {
  _onPressTopic = (topic) => {
    this.props.navigateTo('TimelineScreen', { topicId: topic.id_topic }, 'push')
  };

  render() {
    const {
      feeds,
      deletededFeeds,
      userData,
      navigateTo,
      isTabReady,
      isActive,
      onDeletePost,
      isMine,
      getInitialScreenData,
    } = this.props

    const feedList = isTabReady ? feeds : defaultFeeds

    return (
      <View style={{ flex: 1 }}>
        <Modal
          unstyled
          renderModalContent={({ toggleModal, payload }) => (
            <DeletePost
              onCancel={toggleModal}
              onConfirm={() => {
                toggleModal()
                onDeletePost(Number(payload.id_journey))
              }}
            />
          )}
        >
          {({ toggleModal }, M) => (
            <View style={{ flexGrow: 1 }}>
              {!isEmpty(feedList) ? (
                map(
                  feedList,
                  (feed, index) =>
                    deletededFeeds.indexOf(parseInt(feed.id_journey)) == -1 && (
                      <FeedItemGeneral
                        {...this.props}
                        ownFeed={userData.id_user == feed.id_user}
                        navigateTo={navigateTo}
                        onDeleteFeed={onDeletePost}
                        onRefresh={getInitialScreenData}
                        key={index}
                        onPressOption={feed.id_user === userData.id_user ? toggleModal : null}
                        feed={feed}
                        onPressTopic={this._onPressTopic}
                        isLoading={!isTabReady}
                      />
                    ),
                )
              ) : isActive ? (
                <EmptyV2
                  full
                  aspectRatio={320 / 267}
                  image={require('../assets/images/noPostBg.png')}
                  style={{ alignItems: 'flex-end', paddingHorizontal: WP4 }}
                  textStyle={{ textAlign: 'right' }}
                  textContainerStyle={{ alignItems: 'flex-end', width: WP50 }}
                  message={isMine ? 'It’s so quite here,' : 'Hey, it’s empty here,'}
                  title={isMine ? 'Let’s make some noise with a new post..' : 'come back soon'}
                  actions={
                    <View
                      style={{
                        position: 'absolute',
                        bottom: HP2,
                        flex: 1,
                        alignSelf: 'center',
                        alignItems: 'center',
                      }}
                    >
                      {isMine && (
                        <Button
                          onPress={() =>
                            navigateTo('FeedPostScreen', {
                              refreshFeeds: () => {},
                              topicIds: undefined,
                            })
                          }
                          style={{ width: WP90 }}
                          backgroundColor={TOMATO}
                          centered
                          bottomButton
                          radius={WP4}
                          shadow='none'
                          textType='NeoSans'
                          textSize='small'
                          textColor={WHITE}
                          textWeight={500}
                          text='Create Post'
                        />
                      )}
                    </View>
                  }
                />
              ) : null}
              {M}
            </View>
          )}
        </Modal>
      </View>
    )
  }
}

export default withNavigation(
  _enhancedNavigation(connect(mapStateToProps, {})(ProfileMyPostTab), mapFromNavigationParam),
)
