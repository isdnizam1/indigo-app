import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { WP4, WP6, WP7 } from '../../../constants/Sizes'
import Image from '../../../components/Image'
import Text from '../../../components/Text'
import { GREY_CALM } from '../../../constants/Colors'

const MenuItem = (props) => {
  const {
    onPress,
    label,
    image,
    type
  } = props

  if (type === 'separator') {
    return (
      <View style={{ borderBottomColor: GREY_CALM, borderBottomWidth: 1 }} />
    )
  }

  return (
    <TouchableOpacity
      key={`${Math.random()}`}
      onPress={onPress}
      style={{
        flexDirection: 'row', paddingVertical: WP4, alignItems: 'center', paddingHorizontal: WP6
      }}
    >
      {
        image &&
          <Image
            source={image}
            imageStyle={{ marginRight: WP6, height: WP7 }}
            centered
          />
      }

      <Text type='Circular' size='medium' style={{}}>{label}</Text>
    </TouchableOpacity>
  )
}

MenuItem.propTypes = {}

MenuItem.defaultProps = {}

export default MenuItem
