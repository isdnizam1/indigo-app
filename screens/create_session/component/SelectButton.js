import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import noop from 'lodash-es/noop'
import Text from '../../../components/Text'
import { PALE_BLUE_TWO, REDDISH, REDDISH_LIGHT, SHIP_GREY, SHIP_GREY_CALM } from '../../../constants/Colors'
import { WP3 } from '../../../constants/Sizes'

const styles = {
  container: {
    padding: 0,
    flexDirection: 'row',
    marginBottom: WP3
  },
  button: {
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRightWidth: 1,
    flex: 1,
    alignItems: 'center',
    paddingVertical: WP3,
  },
  firstButton: {
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
    marginLeft: 0
  },
  lastButton: {
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
  },
  label: {

  },
  activeButton: {
    backgroundColor: REDDISH_LIGHT,
    borderColor: REDDISH
  },
  activeLabel: {

  }
}

const SelectButton = (props) => {
  const {
    options,
    value,
    onChange,
    style
  } = props

  return (
    <View style={[
      styles.container,
      style
    ]}
    >
      {
        options.map((item, index) => {
          const isFirstButton = index === 0
          const isLastButton = index === options.length - 1
          const isSelected = value === item.value
          return (
            <TouchableOpacity
              key={index}
              style={[
                styles.button,
                isFirstButton ? styles.firstButton : {},
                isLastButton ? styles.lastButton : {},
                isSelected ? styles.activeButton : {}
              ]}
              onPress={() => onChange(item.value)}
            >
              <Text weight={400} type='Circular' size='xmini' color={isSelected ? REDDISH : SHIP_GREY}>{item.label}</Text>
            </TouchableOpacity>
          )
        })
      }
    </View>
  )
}

SelectButton.propTypes = {
  options: PropTypes.arrayOf(PropTypes.any),
  value: PropTypes.string,
  onChange: PropTypes.func
}

SelectButton.defaultProps = {
  options: [],
  value: '',
  onChange: noop
}

export default SelectButton
