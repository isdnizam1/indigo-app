import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View } from 'react-native'
import noop from 'lodash-es/noop'
import DropDownPicker from 'react-native-dropdown-picker'
import { PALE_BLUE_TWO, SHIP_GREY, SHIP_GREY_CALM, WHITE, TRANSPARENT } from '../../../constants/Colors'
import { WP1, WP2, WP5 } from '../../../constants/Sizes'

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: '100%',
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 6,
  },
  container: {
    marginBottom: WP5,
    marginTop: WP1,
    zIndex: 99999
  }
})

const InputDropdown = (props) => {
  const {
    options,
    placeholder,
    value,
    onChange,
    style
  } = props

  return (
    <View style={[
      styles.container,
      style
    ]}
    >
      <DropDownPicker
        placeholder={placeholder}
        labelStyle={{ color: SHIP_GREY }}
        itemStyle={{ justifyContent: 'flex-start' }}
        items={options}
        style={{ backgroundColor: TRANSPARENT }}
        dropDownStyle={{ backgroundColor: WHITE }}
        onChangeItem={(item) => onChange(item.value)}
        onChangeText={(item) => onChange(item.value)}
        value={value}
      />
    </View>
  )
}

InputDropdown.propTypes = {
  options: PropTypes.arrayOf(PropTypes.any),
  value: PropTypes.string,
  onChange: PropTypes.func
}

InputDropdown.defaultProps = {
  options: [],
  value: '',
  onChange: noop
}

export default InputDropdown
