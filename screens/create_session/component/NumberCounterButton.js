import React, { Component } from 'react'
import PropTypes from 'prop-types'
import noop from 'lodash-es/noop'
import { TouchableOpacity, View } from 'react-native'
import Text from '../../../components/Text'
import { SHIP_GREY_CALM } from '../../../constants/Colors'
import { TEXT_INPUT_STYLE } from '../../../constants/Styles'
import { WP2, WP4 } from '../../../constants/Sizes'

const styles = {
  container: {
    ...TEXT_INPUT_STYLE.inputV2,
    padding: 0,
    width: undefined,
    flexDirection: 'row',
    alignItems: 'center'
  },
  child: {
    padding: WP2
  }
}

class NumberCounterButton extends Component {
  _onAdd = () => {
    const { value, onChange, multipleValue } = this.props
    onChange(value + multipleValue)
  }

  _onSubtract = () => {
    const { value, onChange, multipleValue } = this.props
    onChange(value - multipleValue)
  }

  _actionButton = (label, action, shouldDisabled) => {
    const {
      disabled
    } = this.props

    return (
      <TouchableOpacity
        onPress={action}
        disabled={disabled || shouldDisabled}
        style={{ ...styles.child, paddingHorizontal: WP4 }}
      >
        <Text type='Circular' color={SHIP_GREY_CALM}>{label}</Text>
      </TouchableOpacity>
    )
  }

  render() {
    const {
      value, multipleValue, minValue, maxValue
    } = this.props

    return (
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.container}>
          {this._actionButton('-', this._onSubtract, (minValue && value - multipleValue < minValue) )}
          <Text
            type='Circular'
            color={SHIP_GREY_CALM}
            style={{ ...styles.child, marginHorizontal: WP4 }}
          >
            {value}
          </Text>
          {this._actionButton('+', this._onAdd, (maxValue && value + multipleValue > maxValue) )}
        </View>
      </View>
    )
  }
}

NumberCounterButton.propTypes = {
  value: PropTypes.number,
  onChange: PropTypes.func,
  multipleValue: PropTypes.func,
  minValue: PropTypes.number,
  maxValue: PropTypes.number
}

NumberCounterButton.defaultProps = {
  value: 0,
  multipleValue: 5,
  onChange: noop
}

export default NumberCounterButton
