import React from 'react'
import { BackHandler, Keyboard, Platform, ScrollView, StatusBar, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { noop } from 'lodash-es'
import * as yup from 'yup'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import * as ImageManipulator from 'expo-image-manipulator'
import { CheckBox } from 'react-native-elements'
import { LinearGradient } from 'expo-linear-gradient'
import { WP4 } from 'sf-constants/Sizes'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { _enhancedNavigation, Container, Form, Text } from '../../components'
import { GUN_METAL, ORANGE_BRIGHT, PALE_LIGHT_BLUE_TWO, PALE_SALMON, PALE_WHITE, REDDISH, SHADOW_GRADIENT, SHIP_GREY, SILVER_TWO, WHITE } from '../../constants/Colors'
import { HP2, WP2, WP25, WP3, WP305, WP308, WP5, WP6, WP8 } from '../../constants/Sizes'
import { HEADER, TEXT_INPUT_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import HeaderNormal from '../../components/HeaderNormal'
import Image from '../../components/Image'
import InputTextLight from '../../components/InputTextLight'
import { FONTS } from '../../constants/Fonts'
import Icon from '../../components/Icon'
import Modal from '../../components/Modal'
import { postCreateSession } from '../../actions/api'
import PopUpInfo from '../../components/popUp/PopUpInfo'
import { sessionDispatcher } from '../../services/session'
import styles from './styles'
import MenuItem from './component/MenuItem'
import constants from './constants'

export const FORM_VALIDATION = yup.object().shape({
  speaker_name: yup.string().required(),
  speaker_photo_base64: yup.string().required(),
  speaker_profession: yup.string().required(),
  about_speaker: yup.string().required()
})

const mapStateToProps = ({ auth, session }) => ({
  userData: auth.user,
  currentSession: session.data
})

const mapDispatchToProps = {
  setDataSession: sessionDispatcher.sessionDataSet,
  clearDataSession: sessionDispatcher.sessionDataClear
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', noop),
  initialValues: getParam('initialValues', {}),
  navigateBackOnDone: getParam('navigateBackOnDone', false),
  sessionDetail: getParam('sessionDetail', {})
})

class CreateSessionContentForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      images: [],
      isUploading: false,
      isSuccess: false,
      portfolios: this.props.currentSession.portfolios || []
    }
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler)
  }

  _backHandler = async () => {
    this.props.navigateBack()
  }

  _selectPhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCameraRoll === 'granted') {
      const { uri } = await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
      return await ImageManipulator.manipulateAsync(uri, [{
        resize: {
          width: 620,
          height: 620
        }
      }], {
        base64: true,
        compress: 0.9,
        format: ImageManipulator.SaveFormat.JPG
      })
    }
  }

  _takePhoto = async () => {
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
      const { uri } = await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: true,
        aspect: [1, 1]
      })
      return await ImageManipulator.manipulateAsync(uri, [{
        resize: {
          width: 620,
          height: 620
        }
      }], {
        base64: true,
        compress: 0.9,
        format: ImageManipulator.SaveFormat.JPG
      })
    }
  }

  _handlePhoto = async (result, onChange) => {
    if (Platform.OS === 'ios') StatusBar.setHidden(false)
    if (!result) {
      return
    }

    onChange('speaker_photo_base64')(result.base64)
    onChange('speaker_photo_uri')(result.uri)
  }

  _onSubmit = async ({ formData }) => {
    const { dispatch, sessionDetail, userData, clearDataSession } = this.props
    const { portfolios } = this.state
    const { speaker_name, speaker_photo_base64, speaker_profession, about_speaker } = formData
    this.setState({ isUploading: true })
    const body = {
      speaker_name, speaker_profession, about_speaker,
      speaker_photo: speaker_photo_base64,
      ...sessionDetail,
      id_user: userData.id_user,
      portofolio: portfolios
    }
    try {
      await dispatch(postCreateSession, body)
      this.setState({ isUploading: false, isSuccess: true })
      clearDataSession()
    } catch (e) {
      this.setState({ isUploading: false })
    }
  }

  _onSelfSpeakerChange = (onChange) => {
    const { userData, currentSession, setDataSession } = this.props
    const { selfSpeaker } = currentSession
    const isSelfSpeaker = !selfSpeaker
    setDataSession({ selfSpeaker: isSelfSpeaker })
    if (isSelfSpeaker) {
      onChange('speaker_name')(userData.full_name)
      onChange('speaker_photo_base64')(userData.profile_picture)
      onChange('speaker_photo_uri')(userData.profile_picture)
      onChange('speaker_profession')(userData.job_title)

    } else {
      onChange('speaker_name')()
      onChange('speaker_photo_base64')()
      onChange('speaker_photo_uri')()
      onChange('speaker_profession')()
    }
  }

  _onAddPortfolio = async () => {
    const { setDataSession } = this.props
    const newPortfolios = [...this.state.portfolios]
    newPortfolios.push('')
    this.setState({ portfolios: newPortfolios })
    setDataSession({ portfolios: newPortfolios })
  }

  _onEditPortfolios = (index) => (value) => {
    const { setDataSession } = this.props
    const {
      portfolios
    } = this.state
    portfolios[index] = value
    this.setState({
      portfolios
    })
    setDataSession({ portfolios })
  }

  _onDeletePortfolio = (index) => () => {
    const { setDataSession } = this.props
    const {
      portfolios
    } = this.state
    portfolios.splice(index, 1)
    this.setState({
      portfolios
    })
    setDataSession({ portfolios })
  }

  _renderAvatar = (image, onChange) => {
    return (
      <Modal
        renderModalContent={({ toggleModal }) => (
          <View
            style={{
              paddingVertical: WP3
            }}
          >
            <MenuItem
              onPress={() => {
                this._selectPhoto().then((result) => {
                  this._handlePhoto(result, onChange)
                  toggleModal()
                })
              }}
              label='Choose from Library'
            />
            <MenuItem
              label='Take Photo'
              onPress={() => {
                this._takePhoto().then((result) => {
                  this._handlePhoto(result, onChange)
                  toggleModal()
                })
              }}
            />

            <MenuItem
              label='Remove Current Photo'
              onPress={() => {
                onChange('speaker_photo_base64')(undefined)
                onChange('speaker_photo_uri')(undefined)
                toggleModal()
              }}
            />
            <MenuItem
              type='separator'
            />
            <MenuItem
              image={require('../../assets/icons/close.png')}
              label='Tutup'
              onPress={toggleModal}
            />
          </View>
        )}
      >
        {
          ({ toggleModal }, M) => (
            <>
              <TouchableOpacity onPress={toggleModal} activeOpacity={0.8}>
                <View style={styles.avatarWrapper}>
                  {image ? (
                    <Image style={styles.avatar} imageStyle={styles.avatar} source={{ uri: image }} />
                  ) : (
                    <Image style={styles.avatar} imageStyle={styles.avatar} source={require('../../assets/images/default_avatar.png')} />
                  )}
                  <Image style={styles.cameraIcon} imageStyle={styles.cameraIconImage} source={require('../../assets/icons/icCamera.png')} />
                </View>
              </TouchableOpacity>
              {M}
            </>
          )
        }
      </Modal>
    )
  }

  render() {
    let {
      navigateBack, setDataSession, currentSession
    } = this.props

    const {
      isUploading, isSuccess, portfolios
    } = this.state

    return (
      <Container colors={[WHITE, WHITE]} isAvoidingView={true} >
        <Form
          validation={FORM_VALIDATION}
          onSubmit={this._onSubmit}
          initialValue={{
            ...currentSession
          }}
          onChangeInterceptor={({ key, text }) => setDataSession({ [key]: text })}
        >
          {({ onChange, formData, onSubmit, isValid }) => (
            <View style={{ overflow: 'hidden' }}>
              <View>
                <HeaderNormal
                  iconLeftOnPress={() => navigateBack()}
                  iconLeftWrapperStyle={{ marginRight: WP6 }}
                  textType='Circular'
                  textColor={SHIP_GREY}
                  textWeight={400}
                  text='Create Session'
                  centered
                  rightComponent={(
                    <View style={{ right: 0, paddingHorizontal: WP4 }}>
                      {!isUploading &&
                      <TouchableOpacity
                        onPress={() => {
                          Keyboard.dismiss()
                          onSubmit()
                        }}
                        activeOpacity={TOUCH_OPACITY}
                        disabled={!isValid}
                        style={HEADER.rightIcon}
                      >
                        <Text type='Circular' size='small' weight={400} color={isValid ? ORANGE_BRIGHT : PALE_SALMON}>Finish</Text>
                      </TouchableOpacity>
                      }
                      {isUploading &&
                      <Image
                        spinning
                        source={require('sf-assets/icons/ic_loading.png')}
                        imageStyle={{ width: WP8, aspectRatio: 1 }}
                        style={HEADER.rightIcon}
                      />
                      }
                    </View>
                  )}
                />
                <LinearGradient
                  colors={SHADOW_GRADIENT}
                  style={HEADER.shadow}
                />
              </View>

              {
                isSuccess && (
                  <PopUpInfo
                    closeOnBackdrop={true}
                    isVisible={isSuccess}
                    template={constants.INFO.success}
                    onPress={() => {
                      this.props.navigation.popToTop()
                      this.props.navigateTo('NotificationScreen', { defaultTab: 'activities' }, 'push')
                    }}
                  />
                )
              }

              <KeyboardAwareScrollView
                keyboardDismissMode='interactive'
                keyboardShouldPersistTaps='always'
                extraScrollHeight={HEADER.height}
                innerRef={(r) => this._scrollRef = r}
              >
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{ paddingBottom: WP25, backgroundColor: PALE_WHITE }}
                  onContentSizeChange={() => this._scrollRef.scrollToEnd()}
                >
                  <View style={{ ...styles.section, alignItems: 'center', paddingVertical: WP6 }}>
                    {this._renderAvatar(formData.speaker_photo_uri, onChange)}
                  </View>

                  <View style={styles.section}>
                    <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>INFORMASI PEMBICARA</Text>

                    <CheckBox
                      disabled={isUploading}
                      checked={currentSession.selfSpeaker}
                      containerStyle={{
                        backgroundColor: WHITE, borderWidth: 0, margin: 0,
                        paddingHorizontal: -WP2, marginBottom: WP2
                      }}
                      onPress={() => {
                        this._onSelfSpeakerChange(onChange)
                      }}
                      title='Saya sendiri sebagai pembicara'
                      textStyle={{ fontSize: WP308, color: currentSession.selfSpeaker ? REDDISH : SHIP_GREY }}
                      fontFamily={FONTS.Circular['200']}
                      wrapperStyle={{ marginHorizontal: -WP2, marginTop: -WP3 }}
                      checkedIcon={(
                        <Image
                          imageStyle={{ width: WP5, aspectRatio: 1 }}
                          source={require('sf-assets/icons/icCheckboxActive.png')}
                        />
                      )}
                      uncheckedIcon={(
                        <Image
                          imageStyle={{ width: WP5, aspectRatio: 1 }}
                          source={require('sf-assets/icons/icCheckboxInactive.png')}
                        />
                      )}
                    />

                    <InputTextLight
                      withLabel={false}
                      placeholder='Tulis nama pembicara'
                      value={formData.speaker_name}
                      onChangeText={onChange('speaker_name')}
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      bordered
                      size='mini'
                      type='Circular'
                      returnKeyType={'next'}
                      wording=' '
                      maxLengthFocus
                      lineHeight={1}
                      style={{ marginTop: 0, marginBottom: 0 }}
                      textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                      labelv2='Nama lengkap'
                      editable={!isUploading}
                    />

                    <InputTextLight
                      withLabel={false}
                      placeholder='Tulis profesi pembicara'
                      value={formData.speaker_profession}
                      onChangeText={onChange('speaker_profession')}
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      bordered
                      size='mini'
                      type='Circular'
                      returnKeyType={'next'}
                      wording=' '
                      maxLengthFocus
                      lineHeight={1}
                      style={{ marginTop: 0, marginBottom: 0 }}
                      textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                      labelv2='Profesi'
                      editable={!isUploading}
                    />

                    <InputTextLight
                      withLabel={false}
                      placeholder='Tulis deskripsi pembicara'
                      value={formData.about_speaker}
                      onChangeText={onChange('about_speaker')}
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      bordered
                      size='mini'
                      type='Circular'
                      returnKeyType={'next'}
                      wording=' '
                      maxLengthFocus
                      lineHeight={1}
                      style={{ marginTop: 0, marginBottom: 0 }}
                      textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                      labelv2='Tentang pembicara'
                      editable={!isUploading}
                      multiline
                      maxLine={8}
                    />
                  </View>

                  <View style={{ ...styles.section, borderBottomWidth: 0 }}>
                    <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>PORTFOLIO/WORKS</Text>

                    {
                      portfolios.map((item, index) => (
                        <InputTextLight
                          key={index}
                          withLabel={false}
                          placeholder='Masukkan link/url portfolio'
                          value={item}
                          onChangeText={this._onEditPortfolios(index)}
                          placeholderTextColor={SILVER_TWO}
                          color={SHIP_GREY}
                          bordered
                          size='mini'
                          type='Circular'
                          returnKeyType={'next'}
                          wording=' '
                          maxLengthFocus
                          lineHeight={1}
                          style={{ marginTop: 0, marginBottom: 0 }}
                          textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                          editable={!isUploading}
                          labelv2={`Link portfolio #${index + 1}`}
                          onRemoveAble={this._onDeletePortfolio(index)}
                          removeIcon={(
                            <Icon
                              style={{ marginLeft: WP305 }}
                              centered
                              size='small'
                              color={PALE_LIGHT_BLUE_TWO}
                              onPress={this._onDeletePortfolio(index)}
                              name='close' type='MaterialCommunityIcons'
                            />
                          )}
                        />
                      ))
                    }
                    <TouchableOpacity onPress={this._onAddPortfolio}>
                      <Text color={REDDISH} type='Circular' weight={400} size='mini'>
                      + Tambah link
                      </Text>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
              </KeyboardAwareScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateSessionContentForm),
  mapFromNavigationParam
)
