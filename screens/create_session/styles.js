import { WP10, WP20, WP3, WP30, WP4 } from '../../constants/Sizes'
import { GREY_CALM_SEMI, GREY_CHAT, PALE_BLUE } from '../../constants/Colors'
import { isTabletOrIpad } from '../../utils/helper'
import { SHADOW_STYLE } from '../../constants/Styles'

const styles = {
  section: {
    paddingHorizontal: WP4,
    paddingVertical: WP3,
    marginBottom: WP4,
    borderBottomColor: PALE_BLUE,
    borderBottomWidth: 1
  },
  avatarWrapper: {
    width: isTabletOrIpad() ? WP20 : WP30,
    height: isTabletOrIpad() ? WP20 : WP30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: GREY_CHAT,
    borderWidth: 1.5,
    borderColor: GREY_CALM_SEMI,
    borderRadius: WP30/2
  },
  avatar: {
    width: WP30 - 3,
    height: WP30 - 3,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: (WP30 - 3)/2
  },
  cameraIcon: {
    ...SHADOW_STYLE.shadowThin,
    position: 'absolute',
    width: WP10,
    height: WP10,
    bottom: 0,
    right: 0,
    opacity: 1,
    shadowOpacity: 0.075
  },
  cameraIconImage: {
    width: WP10,
    height: WP10,
  }
}

export default styles
