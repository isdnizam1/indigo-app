const MODERATOR_OPTIONS = [
  {
    value: 'Pilih Sendiri',
    label: 'Pilih Sendiri'
  },
  {
    value: 'Tim Soundfren',
    label: 'Tim Soundfren'
  },
]

const PRICE = [
  { value: 'Rp 35.000', label: 'Rp 35.000' },
  { value: 'Rp 50.000', label: 'Rp 50.000' },
  { value: 'Rp 75.000', label: 'Rp 75.000' },
  { value: 'Rp 100.000', label: 'Rp 100.000' },
]

const INFO = {
  materi: {
    title: 'Materi Presentasi',
    content: 'Materi presentasi ini dapat diberikan kepada kami tim Soundfren maksimal H-1 dari jadwal sesi yang dipilih.',
    image: require('../../assets/icons/icTopicSession.png')
  },
  tiket: {
    title: 'Tiket Sesi',
    content: 'Hasil pendapatan dari penjualan tiket akan dibagi 50 : 50, untuk pembicara sesi dan tim soundfren.',
    image: require('../../assets/icons/icTicketSession.png')
  },
  success: {
    title: 'Draft sesi diterima',
    content: 'Soundfren akan mereview sesi ini dan menghubungi kamu segera',
    image: require('../../assets/icons/icSuccesLarge.png')
  }
}

const EXIT_WARNING = {
  title: 'Buat Sesi Soundfren Connect',
  content: 'Pembuatan sesi sedang berlangsung. Lanjutkan buat sesi?'
}

export default {
  MODERATOR_OPTIONS,
  PRICE,
  INFO,
  EXIT_WARNING
}
