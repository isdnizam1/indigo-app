import React from 'react'
import { BackHandler, Keyboard, ScrollView, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { noop } from 'lodash-es'
import * as yup from 'yup'
import { CheckBox } from 'react-native-elements'
import { LinearGradient } from 'expo-linear-gradient'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { _enhancedNavigation, Container, Form, InputTextLight, Text } from '../../components'
import { GUN_METAL, ORANGE_BRIGHT, PALE_SALMON, PALE_WHITE, REDDISH, SHADOW_GRADIENT, SHIP_GREY, SILVER_TWO, WHITE } from '../../constants/Colors'
import { HP2, WP1, WP2, WP25, WP3, WP308, WP4, WP5, WP8 } from '../../constants/Sizes'
import { HEADER, TEXT_INPUT_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import HeaderNormal from '../../components/HeaderNormal'
import Image from '../../components/Image'
import { FONTS } from '../../constants/Fonts'
import SelectDate from '../../components/SelectDate'
import PopUpInfo from '../../components/popUp/PopUpInfo'
import ExitWarningPopUp from '../../components/popUp/ExitWarningPopUp'
import { sessionDispatcher } from '../../services/session'
import styles from './styles'
import SelectButton from './component/SelectButton'
import constants from './constants'
import InputDropdown from './component/InputDropdown'
import NumberCounterButton from './component/NumberCounterButton'

export const FORM_VALIDATION = yup.object().shape({
  title: yup.string().required(),
  description: yup.string().required(),
  price: yup.string().required(),
  total_seats: yup.number().required(),
  date_schedule: yup.string().required(),
  session_moderator: yup.string().required()
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  clearDataSession: sessionDispatcher.sessionDataClear
}

const mapFromNavigationParam = (getParam) => ({
  refreshProfile: getParam('refreshProfile', noop),
  initialValues: getParam('initialValues', {}),
  navigateBackOnDone: getParam('navigateBackOnDone', false)
})

class CreateSessionContentForm extends React.Component {
  static navigationOptions = () => ({
    gesturesEnabled: false
  })

  constructor(props) {
    super(props)
    this.state = {
      isUploading: false,
      showExitWarning: false
    }
  }

  async componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => this._backHandler())
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', () => this._backHandler())
  }

  _backHandler = async () => {
    this.setState({ showExitWarning: true })
  }

  _onNext = async ({ formData }) => {
    const {
      navigateTo
    } = this.props

    const sessionDetail = {
      available_presentation: formData.available_presentation,
      date_schedule: formData.date_schedule,
      description: formData.description,
      price: formData.price.replace('Rp '),
      session_moderator: formData.session_moderator,
      title: formData.title,
      total_seats: formData.total_seats
    }

    navigateTo('CreateSessionSpeakerForm', { sessionDetail })
  }

  render() {
    const {
      isUploading, showExitWarning
    } = this.state

    return (
      <Container colors={[WHITE, WHITE]}>

        <ExitWarningPopUp
          isVisible={showExitWarning}
          onContinue={() => this.setState({ showExitWarning: false })}
          onCancel={() => {
            this.setState({ showExitWarning: false })
            this.props.navigateBack()
            this.props.clearDataSession()
          }}
          template={constants.EXIT_WARNING}
        />
        <Form
          validation={FORM_VALIDATION}
          onSubmit={this._onNext}
          initialValue={{
            total_seats: 10,
            available_presentation: 0
          }}
        >
          {({ onChange, formData, onSubmit, isValid }) => (
            <View style={{ overflow: 'hidden' }}>
              <View>
                <HeaderNormal
                  iconLeftWrapperStyle={{ marginRight: WP3 }}
                  iconLeftOnPress={() => {
                    this._backHandler()
                  }}
                  textType='Circular'
                  textColor={SHIP_GREY}
                  textWeight={400}
                  text='Create Session'
                  centered
                  noRightPadding
                  rightComponent={(
                    <View style={{ right: 0 }}>
                      {!isUploading &&
                      <TouchableOpacity
                        onPress={() => {
                          Keyboard.dismiss()
                          onSubmit()
                        }}
                        style={HEADER.rightIcon}
                        activeOpacity={TOUCH_OPACITY}
                        disabled={!isValid}
                      >
                        <Text type='Circular' size='small' weight={400} color={isValid ? ORANGE_BRIGHT : PALE_SALMON}>Next</Text>
                      </TouchableOpacity>
                      }
                      {isUploading &&
                      <Image
                        spinning
                        source={require('sf-assets/icons/ic_loading.png')}
                        imageStyle={{ width: WP8, aspectRatio: 1 }}
                        style={HEADER.rightIcon}
                      />
                      }
                    </View>
                  )}
                />
                <LinearGradient
                  colors={SHADOW_GRADIENT}
                  style={HEADER.shadow}
                />
              </View>

              <KeyboardAwareScrollView
                keyboardDismissMode='interactive'
                keyboardShouldPersistTaps='always'
                extraScrollHeight={HEADER.height}
              >
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  contentContainerStyle={{ paddingBottom: WP25, paddingTop: WP3, backgroundColor: PALE_WHITE }}
                >
                  <View style={styles.section}>
                    <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>MATERI</Text>

                    <InputTextLight
                      withLabel={false}
                      placeholder='Tulis topik sesimu'
                      value={formData.title}
                      onChangeText={onChange('title')}
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      bordered
                      size='mini'
                      type='Circular'
                      returnKeyType={'next'}
                      wording=' '
                      maxLengthFocus
                      lineHeight={1}
                      style={{ marginTop: 0, marginBottom: 0 }}
                      textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                      labelv2='Judul'
                      editable={!isUploading}
                    />

                    <InputTextLight
                      withLabel={false}
                      placeholder='Tulis poin materi yang akan dibawakan'
                      value={formData.description}
                      onChangeText={onChange('description')}
                      placeholderTextColor={SILVER_TWO}
                      color={SHIP_GREY}
                      bordered
                      size='mini'
                      type='Circular'
                      returnKeyType={'next'}
                      wording=' '
                      maxLengthFocus
                      lineHeight={1}
                      style={{ marginTop: 0, marginBottom: 0 }}
                      textInputStyle={TEXT_INPUT_STYLE['inputV2']}
                      labelv2='Poin materi yang dibawakan'
                      editable={!isUploading}
                      multiline
                      maxLine={7}
                      subLabel={'(3 poin)'}
                    />

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                      <CheckBox
                        disabled={isUploading}
                        checked={formData.available_presentation == 1}
                        containerStyle={{
                          backgroundColor: WHITE, borderWidth: 0, margin: 0,
                          paddingHorizontal: -WP2, marginBottom: WP2
                        }}
                        onPress={() => {
                          onChange('available_presentation')(formData.available_presentation == 1 ? 0 : 1)
                        }}
                        title='Materi presentasi sudah tersedia'
                        textStyle={{ fontSize: WP308, color: formData.available_presentation == 1 ? REDDISH : SHIP_GREY }}
                        fontFamily={FONTS.Circular['200']}
                        wrapperStyle={{ marginHorizontal: -WP2, marginTop: -WP3 }}
                        checkedIcon={(
                          <Image
                            imageStyle={{ width: WP5, aspectRatio: 1 }}
                            source={require('sf-assets/icons/icCheckboxActive.png')}
                          />
                        )}
                        uncheckedIcon={(
                          <Image
                            imageStyle={{ width: WP5, aspectRatio: 1 }}
                            source={require('sf-assets/icons/icCheckboxInactive.png')}
                          />
                        )}
                      />
                      <PopUpInfo
                        template={constants.INFO.materi}
                      >
                        <Image
                          source={require('../../assets/icons/icAbout_light.png')}
                          imageStyle={{ height: WP5 }}
                          centered
                          style={{ padding: WP3, margin: -WP3 }}
                        />
                      </PopUpInfo>
                    </View>

                  </View>

                  <View style={styles.section}>
                    <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ marginBottom: HP2 }}>INFO SESI</Text>

                    <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
                    Jadwal Sesi
                    </Text>

                    <SelectDate
                      value={formData.date_schedule}
                      dateFormat='MM/YYYY'
                      minimumDate={new Date()}
                      mode='date'
                      onChangeDate={onChange('date_schedule')}
                      style={{ marginBottom: WP4 }}
                      withDate={false}
                      disabled={isUploading}
                    />

                    <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
                    Moderator Sesi
                    </Text>

                    <SelectButton
                      value={formData.session_moderator}
                      options={constants.MODERATOR_OPTIONS}
                      onChange={onChange('session_moderator')}
                    />

                  </View>

                  <View style={{ ...styles.section, borderBottomWidth: 0 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: HP2 }}>
                      <Text type='Circular' size='xmini' weight={500} color={GUN_METAL} style={{ }}>TIKET</Text>
                      <PopUpInfo
                        template={constants.INFO.tiket}
                      >
                        <Image
                          source={require('../../assets/icons/icAbout_light.png')}
                          imageStyle={{ height: WP5 }}
                          centered
                          style={{ padding: WP3, margin: -WP3 }}
                        />
                      </PopUpInfo>
                    </View>

                    <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
                      {'Harga Tiket '}
                      <Text weight={200} type='Circular' size={'xmini'} color={SILVER_TWO}>
                      (Minimal Rp 35.000)
                      </Text>
                    </Text>
                    <InputDropdown
                      style={{ zIndex: 99999 }}
                      value={formData.price}
                      options={constants.PRICE}
                      onChange={onChange('price')}
                    />

                    <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
                      {'Kuota Tiket '}
                      <Text weight={200} type='Circular' size={'xmini'} color={SILVER_TWO}>
                      (Minimal 10 tiket)
                      </Text>
                    </Text>
                    <NumberCounterButton
                      value={formData.total_seats}
                      onChange={onChange('total_seats')}
                      minValue={10}
                    />

                  </View>
                </ScrollView>
              </KeyboardAwareScrollView>
            </View>
          )}
        </Form>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(CreateSessionContentForm),
  mapFromNavigationParam
)
