import React from 'react'
import { connect } from 'react-redux'
import { BackHandler, Image, View, StyleSheet, TouchableOpacity } from 'react-native'
import { KeyboardAwareScrollView as ScrollView } from 'react-native-keyboard-aware-scroll-view'
import {
  startCase,
  isObject,
  isNumber,
  uniqWith,
  isEqual,
  isEmpty,
  isNil,
  get,
  pick,
  upperFirst,
  isArray,
  noop,
} from 'lodash-es'
import { CancelToken } from 'axios'
import { _enhancedNavigation, Container } from 'sf-components'
import Touchable from 'sf-components/Touchable'
import { Peoples, Activities, SearchBox, Recents } from 'sf-components/search'
import { getAdvancedSearch } from 'sf-actions/api'
import { getRecent, pushRecent } from 'sf-utils/storage'
import {
  setCurrentTab,
  setRecentSearch,
  setSearchingState,
  setQuery,
} from 'sf-services/search/actionDispatcher'
import { validateQuery, filterAdListing, isEqualObject } from 'sf-utils/helper'
import { KEY_RECENT_SEARCH } from 'sf-constants/Storage'
import bugsnagClient from 'sf-utils/bugsnag'
import VisibleView from 'sf-components/VisibleView'
import InteractionManager from 'sf-utils/InteractionManager'
import produce from 'immer'
import { FlatList } from 'react-native'
import { WP30, WP40, WP60, HP10, HP5, HP205, WP5, WP20, HP2, WP2, WP50, WP55, WP6, WP3, WP1, WP25 } from '../../constants/Sizes'
import { Text, Icon, Card } from '../../components'
import { getVideoSearch, getPodcastSearch, getLearnSearch, getLogActivity, postDeleteLogActivity } from '../../actions/api'
import { REDDISH, GUN_METAL, SHIP_GREY_CALM, SHIP_GREY, CLEAR_BLUE } from '../../constants/Colors'
import { SHADOW_STYLE } from '../../constants/Styles'
import StartupSearchBox from './component/StartupSearchBox'
import styles from './styles'
import { ImageBackground } from 'react-native'

const mapStateToProps = ({
  auth,
  search: { currentTab, query, peopleFilters },
}) => ({
  userData: auth.user,
  currentTab,
  query,
  peopleFilters,
})

const mapDispatchToProps = {
  setCurrentTab,
  setRecentSearch,
  setSearchingState,
  setQuery,
}

const mapFromNavigationParam = (getParam) => ({
  searchType: getParam('searchType', '')
})

class StartupSearchScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSearching: false,
      loadMore: false,
      query: '',
      video: { lastQuery: null, data: [] },
      podcast: { lastQuery: null, data: [] },
      data: null,
      avoidScreenUpdate: false,
      networkError: false,
      recents: [],
      searchType: '',
      error: null,
      isLoadingRemove: false,
    }
    this.cancelToken = null
    this._releaseLoader = this._releaseLoader.bind(this)
    this._attachBackAction = this._attachBackAction.bind(this)
    this._releaseBackAction = this._releaseBackAction.bind(this)
    this._onBack = this._onBack.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true
  }

  static getDerivedStateFromProps(props, state) {
    const { query } = props
    return { query }
  }

  _onBack = () => {
    this.props.setQuery('')
    this.props.navigation.goBack()
    return true
  };

  _attachBackAction = (e) => {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this._onBack
    )
  };

  _releaseBackAction() {
    typeof this.backHandler !== 'undefined' && this.backHandler.remove()
  }

  async _releaseLoader() {
    this.setState(
      produce((draft) => {
        draft.isSearching = false
        draft.loadMore = false
        draft.avoidScreenUpdate = false
      })
    )
  }

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener(
      'focus',
      this._attachBackAction
    )
    this.blurListener = this.props.navigation.addListener(
      'blur',
      this._releaseBackAction
    )
    this._getSearchHistory()
  }

  _getSearchHistory = async () => {
    const { userData: { id_user: id_viewer }, searchType } = this.props
    getLogActivity({
      type:
        searchType === 'video' ? 'history_search_video' :
        searchType === 'learn' ? 'search_learning_list' :
        'search_podcast',
      id_viewer,
      start: 0,
      limit: 5
    }).then(({ data: { result: recents } }) => this.setState({ recents }))
  }

  componentDidUpdate(prevProps, prevState) {
    const { query, currentTab, setSearchingState } = this.props
    const { isSearching } = this.state
    const newQuery = query || this.state.query
    const validQuery = validateQuery(newQuery)
    if (prevProps.query !== '' & newQuery === '') {
      this.setState({
        data: null,
        error: null
      })
    }

    !!prevProps.query && !query && validQuery && this.search(query, false, true)
    prevProps.isSearching != isSearching && setSearchingState(isSearching)
    // prevProps.query != query &&
    //   this._onQueryChange(
    //     query,
    //     prevProps.currentTab != currentTab ? 100 : 850
    //   )

    !isEqualObject(prevProps.peopleFilters, this.props.peopleFilters) && validQuery &&
      this.search(query, false, true)
    // prevProps.currentTab != currentTab &&
    //   this._onQueryChange(query, 100)
  }

  search(query, loadMore, forceSearch, show = null) {
    const { all, activity, isSearching } = this.state
    const { currentTab, peopleFilters } = this.props
    const {
      userData: { id_user },
    } = this.props
    if (
      (!isSearching || !loadMore) &&
      (forceSearch ||
        (!currentTab && query != all.lastQuery) ||
        (currentTab && query != this.state[currentTab].lastQuery) ||
        loadMore)
    ) {
      this.setState(
        produce((draft) => {
          draft.loadMore = loadMore
        }),
        async () => {
          const start =
              !loadMore ? 0 : this.state[currentTab].data.length,
            limit = 10,
            type = currentTab,
            search = query || this.state.query
          try {
            this.cancelToken = CancelToken.source()
            this.requestTimeout = setTimeout(
              () =>
                this.cancelToken.cancel(
                  `Advanced search time exceed 10s, user has been offered to retry the request. Keyword : "${query}"`
                ),
              10000
            )
            const params = {
              id_user,
              search: (search || '').toLowerCase(),
              platform: '1000startup',
              start,
              limit,
            }
            let result = {}
            const getSearch =
              this.props.searchType === 'video' ? getVideoSearch :
              this.props.searchType === 'learn' ? getLearnSearch :
              getPodcastSearch
            const { data } = await getSearch(
              params,
              this.cancelToken.token
            )

            if (data.code == 200) {
              if (this.props.searchType === 'video') {
                this.setState({
                  data: data.result,
                  error: null
                })
              } else if (this.props.searchType === 'learn') {
                this.setState({
                  data: data.result,
                  error: null
                })
              } else {
                this.setState({
                  data: data.result.search,
                  error: null
                })
              }
            } else {
              this.setState({
                error: data,
                data: null
              })
            }

            let saveToHistory = true
            saveToHistory && !!query && pushRecent(KEY_RECENT_SEARCH, query)
            isNumber(this.requestTimeout) && clearTimeout(this.requestTimeout)
          } catch (e) {
            !isNil(e.message) && bugsnagClient.notify(new Error(e.message))
            this.setState(
              produce((draft) => {
                draft.networkError = !isNil(e.message)
                draft.isSearching = false
              }),
              this._releaseLoader
            )
            isNumber(this.requestTimeout) && clearTimeout(this.requestTimeout)
          }
        }
      )
    }
    this._getSearchHistory()
  }

  _renderHeader = () => {
    return (<StartupSearchBox
    onBack={this._onBack}
    query={this.state.query}
    onPressX={() => this.setState({ data: null })}
    onPressCancel={
      this.props.searchType === 'video' ? () => this.props.navigateTo('VideoCategoryScreen') :
      this.props.searchType === 'learn' ? () => this.props.navigateTo('StartupLearnScreen') :
      () => this.props.navigateTo('PodcastCategoryScreen')}
    onSubmit={() => { this.search(this.state.query, false, true) }}
    placeholder={
      this.props.searchType === 'video' ? 'Cari Video' :
      this.props.searchType === 'learn' ? 'Cari Pembelajaran' :
      'Cari Podcast'}
            />)
  };

  _updateRecents = () => {
    getRecent(KEY_RECENT_SEARCH).then((recents) => {
      this.props.setRecentSearch(recents.filter((recent) => !!recent))
    })
  };

  _renderRecent = ({ item, index }) => {
    const { isLoadingRemove } = this.state
    return (
      <View style={style.recent}>
        <Touchable
          onPress={() => {
            this.props.setQuery(item.value)
            this.search(item.value, false, true)
          }}
          style={style.recentQuery}
        >
          <Text size={'slight'} color={SHIP_GREY}>
            {upperFirst(item.value)}
          </Text>
        </Touchable>
        <Touchable onPress={ async () => isLoadingRemove? noop : await this._removeRecent(index)} style={style.recentDelete}>
          <Icon size={'xmini'} color={SHIP_GREY_CALM} name={'close'} />
        </Touchable>
      </View>
    )
  };

  _removeRecent = async (index) => {
    const { recents } = this.state
    const { dispatch, searchType } = this.props

    this.setState({ isLoadingRemove: true })
    await dispatch(postDeleteLogActivity, {
      id_log_activity: recents[index].id_log_activity
    }).then(() => {
    })
    if (searchType !== 'video' && !isEmpty(recents)) {
      recents.splice(index, 1)
    }
    // this._getSearchHistory()
    this.setState({ recents, isLoadingRemove: false })
  }

  _renderEmpty = () => {
    const { error, isSearching, query, recents } = this.state
    const newQuery = query || this.props.query
    if (isEmpty(newQuery)) {
      if (recents.length > 0) {
        return (
          <FlatList
            contentContainerStyle={{ padding: 15 }}
            data={recents}
            ListHeaderComponent={<View>
              <Text color={GUN_METAL} type={'Circular'} weight={600}>
                {`Pencarian${
                  this.props.searchType === 'video' ? ' Video' :
                  this.props.searchType === 'learn' ? '' :
                  ' Podcast'
                  } Terakhir`}
              </Text>
            </View>}
            renderItem={this._renderRecent}
          />
        )
      }
    } else {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ marginBottom: HP205 }}>
            {error !== null
              ? <Image source={require('sf-assets/images/reloadMagnifier.png')} resizeMode={'contain'} style={{ width: WP40, height: WP40 }} />
              : <Image source={require('sf-assets/images/searchMagnifier.png')} resizeMode={'contain'} style={{ width: WP40, height: WP40 }} />
            }
          </View>
          <View>
            <Text
              style={{ width: WP60, textAlign: 'center' }}
              size='small'
              type='Circular'
            >
              {error !== null
                ? error.code != 500
                  ? error.message
                  : 'Maaf, kami tidak menemukan hasil untuk pencarian terkait'
                :
                  this.props.searchType === 'video' ? 'Silahkan masukkan kata kunci untuk mencari video' :
                  this.props.searchType === 'learn' ? 'Silahkan masukkan kata kunci untuk mencari pembelajaran' :
                  'Silahkan masukkan kata kunci untuk mencari podcast'
              }
            </Text>
          </View>
        </View>
      )
    }
  }

  _renderVideoItem = (data) => {
    const { item, index } = data
    const additionalData = JSON.parse(item.additional_data)
    return (
      <Card
        onPress={() => this.props.navigateTo('VideoMMBListScreen', {
        category: additionalData.category[0],
        subCategory: additionalData.sub_category
      })} style={[{ borderRadius: 5, marginVertical: HP2 }, SHADOW_STYLE['shadowThin']]}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ marginRight: 15 }}>
              <Image source={{ uri: additionalData.thumbnail }} style={{ height: WP20, width: WP20, borderRadius: 6 }} />
            </View>
            <View>
              <View style={{ flexDirection: 'row', marginBottom: 10, maxWidth: WP55 }}>
                <Text
                  style={{ maxWidth: WP25 }}
                  color={REDDISH}
                  size='mini'
                  type='Circular'
                  weight={'700'}
                  numberOfLines={1}
                >
                  {additionalData.category[0]}
                </Text>
                <Icon
                  size='mini'
                  name='keyboard-arrow-right'
                  type='MaterialIcons'
                  centered
                  color={REDDISH}
                />
                <Text
                  style={{ maxWidth: WP25 }}
                  color={REDDISH}
                  size='mini'
                  type='Circular'
                  weight={'700'}
                  numberOfLines={1}
                >
                  {additionalData.sub_category}
                </Text>
              </View>
              <View style={{ maxWidth: WP55, marginBottom: 10 }}>
                <Text
                  color={GUN_METAL}
                  size='small'
                  type='Circular'
                  weight={'700'}
                  style={{ lineHeight: 21 }}
                  numberOfLines={2}
                  multiline
                >
                  {item.title}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                {item.video_status !== 'not-played' && <Image source={item.video_status === 'playing' ? require('sf-assets/icons/ic_progress.png') : require('sf-assets/icons/ic_done.png')} style={{ marginRight: WP5 }} />}
                <Text
                  color={SHIP_GREY_CALM}
                  size='mini'
                  lineHeight={18}
                  type='Circular'
                >
                  {`${additionalData.video_duration} · ${item.total_view} Views`}
                </Text>
              </View>
            </View>
          </View>
          <View>
            <Icon
              size='massive'
              name='keyboard-arrow-right'
              type='MaterialIcons'
              centered
              color={REDDISH}
            />
          </View>
        </View>
      </Card>
    )
  }

  _renderPodcastItem = (data) => {
    const { item, index } = data
    return (
      <Card
        onPress={() =>
        this.props.navigateTo('PodcastByModuleScreen', {
          module: item.module,
          category_id: null,
        })
      } style={[{ borderRadius: 5, marginVertical: HP2 }, SHADOW_STYLE['shadowThin']]}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ marginRight: 15 }}>
              <Image source={{ uri: item.image }} style={{ height: WP20, width: WP20, borderRadius: 6 }} />
            </View>
            <View style={{ maxWidth: WP50 }}>
            <View style={{ flexDirection: 'row', marginBottom: 10, maxWidth: WP55 }}>
                <Text
                  color={REDDISH}
                  style={{ maxWidth: WP25 }}
                  numberOfLines={1}
                  size='mini'
                  type='Circular'
                  weight={'700'}
                >
                  {item.album_category}
                </Text>
                <Icon
                  size='mini'
                  name='keyboard-arrow-right'
                  type='MaterialIcons'
                  centered
                  color={REDDISH}
                />
                <Text
                  color={REDDISH}
                  style={{ maxWidth: WP25 }}
                  numberOfLines={1}
                  size='mini'
                  type='Circular'
                  weight={'700'}
                >
                  {item.module}
                </Text>
              </View>
              <View style={{ maxWidth: WP60, marginBottom: 10 }}>
                <Text
                  color={GUN_METAL}
                  size='small'
                  type='Circular'
                  weight={'700'}
                  style={{ lineHeight: 21 }}
                  numberOfLines={2}
                  multiline
                >
                  {item.title}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center' }}>
                {item.played_status !== 'not-played' && <Image source={item.played_status === 'playing' ? require('sf-assets/icons/ic_progress.png') : require('sf-assets/icons/ic_done.png')} style={{ marginRight: WP5 }} />}
                <Text
                  color={SHIP_GREY_CALM}
                  size='mini'
                  lineHeight={18}
                  type='Circular'
                >
                  {item.speaker[0].title}
                </Text>
              </View>
            </View>
          </View>
          <View>
            <Icon
              size='massive'
              name='keyboard-arrow-right'
              type='MaterialIcons'
              centered
              color={REDDISH}
            />
          </View>
        </View>
      </Card>
    )
  }

  _renderLearnItem = (data) => {
    const { item } = data
    // let dataLearn = item.content ? item.content : item
    const title = item.title ? item.title : null

    return (
			<View>
        <TouchableOpacity
          style={styles.itemCategoryContainer}
          onPress={() => {
            if (!item.is_locked) {
              if (item.type === 'video') {
                this.props.navigateTo('PremiumVideoTeaser', {
                  id_ads: item.id_ads,
                  isStartup: true
                })
              } else if (item.type === 'sp_album') {
                this.props.navigateTo('SoundplayDetail', {
                  id_ads: item.id_ads,
                  isStartup: true
                })
              } else if (item.type === 'news_update') {
                this.props.navigateTo('NewsDetailScreen', {
                  id_ads: item.id_ads,
                  is1000Startup: true
                })
              } else if (item.type === 'sc_session') {
                this.props.navigateTo('SessionDetail', {
                  id_ads: item.id_ads,
                  isStartup: true
                })
              }
            }
          }}
        >
          <View style={styles.itemCategoryContentContainer}>
            <View style={{ flexDirection: 'row' }}>
              {item.icon ? (
                <Image
                  style={{ width: 48, height: undefined }}
                  aspectRatio={1}
                  source={
                    item.icon ? { uri: item.icon } :
                      require('../../assets/icons/ic_list_learn.png')
                  }
                />
              )
                :
                (
                  <ImageBackground style={{ width: 48, height: 48, justifyContent: 'center', alignItems: 'center' }} source={require('../../assets/icons/icon_category.png')}>
                    <Image
                      style={{ width: 38, height: undefined }}
                      aspectRatio={1}
                      source={
                        item.icon ? { uri: item.icon } :
                          require('../../assets/icons/ic_list_learn.png')
                      }
                    />
                  </ImageBackground>
                )
              }
              <View style={styles.itemCategoryTextContainer}>
                <Text
                  type='Circular'
                  size={'tiny'}
                  lineHeight={10}
                  weight={700}
                  color={'#FF651F'}
                >
                  {item.group || title}
                </Text>
                <Text
                  type='Circular'
                  size={'small'}
                  lineHeight={21}
                  weight={700}
                  color={item.is_locked ? '#C4C4C4' : '#454F5B'}
                >
                  {item.title}
                </Text>
                <View style={styles.itemContentTypeContainer}>
                  <Image
                    style={{ width: 15, height: undefined, marginRight: 5, opacity: item.is_locked ? 0.5 : 1 }}
                    aspectRatio={1}
                    source={
                      item.type === 'video' ? require('../../assets/icons/ic_list_video.png') :
                        item.type === 'sp_album' ? require('../../assets/icons/ic_list_podcast.png') :
                          item.type === 'news_update' ? require('../../assets/icons/ic_list_article.png') :
                            item.type === 'sc_session' ? require('../../assets/icons/ic_list_webinar.png') :
                              require('../../assets/icons/ic_list_video.png')
                    }
                  />
                  <Text
                    type='Circular'
                    size={'xmini'}
                    lineHeight={14}
                    weight={400}
                    color={item.is_locked ? '#C4C4C4' : '#454F5B'}
                  >
                    {
                      item.type === 'video' ? 'Video' :
                        item.type === 'sp_album' ? 'Podcast' :
                          item.type === 'news_update' ? 'Article' :
                            item.type === 'sc_session' ? 'Webinar' :
                              item.type
                    }
                  </Text>
                </View>
              </View>
            </View>
            <View style={{
              ...styles.itemContentStatusContainer,
              backgroundColor:
                item.is_locked ? '#C4C4C4' :
                  item.status === 'not-played' ? '#FF651F' :
                    item.status === 'completed' ? '#42B70B' :
                      item.status === 'continue' ? CLEAR_BLUE : '#C4C4C4'
            }}
            >
              <Text
                type='Circular'
                size={'xmini'}
                lineHeight={14}
                weight={700}
                maxFontSizeMultiplier={1}
                color={'#FFFFFF'}
              >{
                  item.is_locked ? 'Locked' :
                    item.status === 'not-played' && item.type === 'video' ? 'Watch' :
                      item.status === 'not-played' && item.type === 'sc_session' ? 'Join' :
                        item.status === 'not-played' && item.type === 'sp_album' ? 'Listen' :
                          item.status === 'not-played' && item.type === 'news_update' ? 'Read' :
                            item.status === 'not-played' ? 'Play' :
                              item.status === 'completed' ? 'Completed' :
                                item.status === 'continue' ? 'Continue' :
                                  item.status === 'locked' ? 'Locked' :
                                    ''
                }</Text>
            </View>
          </View>
        </TouchableOpacity>
			</View>
		)
  }

  render() {
    const {
      data,
      recents
    } = this.state
    const {
      query,
    } = this.props
    console.log(`data ${JSON.stringify(data)}`)
    console.log(`recent ${JSON.stringify(recents)}`)
    return (
      <Container
        scrollable={false}
        renderHeader={this._renderHeader}
        isLoading={false}
        isReady={true}
      >
        {data !== null
        ? <ScrollView keyboardShouldPersistTaps='handled'>
            <FlatList
              data={data}
              contentContainerStyle={{ padding: 15 }}
              renderItem={
                this.props.searchType === 'video' ? this._renderVideoItem :
                this.props.searchType === 'learn' ? this._renderLearnItem :
                this._renderPodcastItem
              }
            />
        </ScrollView>
        : this._renderEmpty()}
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(StartupSearchScreen),
  mapFromNavigationParam
)

const style = StyleSheet.create({
  recents: {
    paddingVertical: WP6,
    paddingHorizontal: WP5,
  },
  recent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgb(244, 246, 248)',
  },
  recentQuery: {
    paddingVertical: WP3,
    flex: 1,
  },
  recentDelete: {
    paddingVertical: WP3,
    paddingHorizontal: WP1,
  },
})