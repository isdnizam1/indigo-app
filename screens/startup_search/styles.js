import { WP1, WP3, WP4, WP40, WP6 } from '../../constants/Sizes'
import { NAVY_DARK, PALE_BLUE, PALE_BLUE_TWO, REDDISH, SHIP_GREY_CALM } from '../../constants/Colors'

export default {
  section: {
    paddingHorizontal: WP4,
    paddingVertical: WP3,
    marginBottom: WP4,
    borderBottomColor: PALE_BLUE,
    borderBottomWidth: 1
  },
  imageWrapper: {
    width: WP40,
    height: WP40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10
  },
  imagePlaceholder: {
    marginBottom: 4,
    width: 80
  },
  cameraIcon: {
    width: WP6,
    aspectRatio: 1,
    marginRight: WP1
  },
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 20
  },
  contentContainer: {
    marginBottom: 20,
  },
  itemFaqContainer: {
    marginHorizontal: 15,
    marginTop: 15,
  },
  itemFaqAnswerContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  titleText: {
    color: NAVY_DARK,
    fontSize: 16,
    lineHeight: 24,
    marginHorizontal: 10,
    marginVertical: 10
  },
  questionText: {

  },
  answerText: {
    width: '95%',
  },
  dotAnswer: {
    width: 4,
    height: 4,
    borderRadius: 8,
    backgroundColor: SHIP_GREY_CALM,
    marginTop: 7,
    marginRight: 5
  },
  activeDotContainer: {
    width: 9,
    height: 9,
    borderRadius: 18,
    backgroundColor: '#ff651f20',
    marginTop: 7,
    marginLeft: -2,
    marginRight: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activeDot: {
    width: 4,
    height: 4,
    borderRadius: 8,
    backgroundColor: REDDISH,
  },
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: '#E4E6E7',
    marginVertical: 5
  },
  timelineContainer: {
    marginVertical: 10,
    marginHorizontal: 20
  },
  timelineItemContainer: {
    flexDirection: 'row'
  },
  timelineDataContainer: {
    marginLeft: 5
  },
  timelinePhaseText: {
    paddingTop: 3
  },
  timelineTitleText: {
    marginVertical: 2
  },
  timelineDateText: {
    //
  },
  lineProgress: {
    height: 55,
    width: 1,
    backgroundColor: PALE_BLUE_TWO,
    marginLeft: 2,
    marginVertical: 6
  },
  itemCategoryContainer: {
    borderRadius: 10,
    marginBottom: 16,
    padding: 16,
    backgroundColor: '#FFFFFF',
    elevation: 2,
  },
  itemCategoryContentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemCategoryTextContainer: {
    width: 140,
    marginLeft: 20,
    marginRight: -10,
    alignSelf: 'center'
  },
  iconRightContainer: {
    alignSelf: 'center'
  },
  contentHeaderContainer: {
    backgroundColor: '#F9FAFB',
    marginBottom: 20
  },
  itemSeparator: {
    height: 0.75,
    marginHorizontal: 100,
    backgroundColor: '#DFE3E8'
  },
  itemContentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
    padding: 10
  },
  itemContentTextContainer: {
    marginLeft: 10
  },
  itemContentTypeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 2
  },
  itemContentStatusContainer: {
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 6,
    borderRadius: 5,
    alignSelf: 'flex-end'
  },
}
