import React from 'react'
import {
  View,
  TextInput,
  StyleSheet,
  Animated,
} from 'react-native'
import { isEmpty } from 'lodash-es'
import { connect } from 'react-redux'
import Touchable from 'sf-components/Touchable'
import Icon from 'sf-components/Icon'
import Spacer from 'sf-components/Spacer'
import Text from 'sf-components/Text'
import Loader from 'sf-components/Loader'
import VisibleView from 'sf-components/VisibleView'
import {
  REDDISH,
  SHIP_GREY_CALM,
  SHADOW_GRADIENT,
  PALE_BLUE,
  SHIP_GREY
} from 'sf-constants/Colors'
import { WP05, WP1, WP2, WP3, WP4 } from 'sf-constants/Sizes'
import produce from 'immer'
import { LinearGradient } from 'expo-linear-gradient'
import { HEADER } from 'sf-constants/Styles'
import {
  setCurrentTab,
  setQuery,
  setSearchingState,
} from 'sf-services/search/actionDispatcher'

const mapStateToProps = ({ search: { query, currentTab, isSearching } }) => ({
  query,
  currentTab,
  isSearching,
})

const mapDispatchToProps = {
  setCurrentTab,
  setQuery,
  setSearchingState,
}

class StartupSearchBox extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tabLayout: {
        people: {},
        activity: {},
        video: {},
        podcast: {},
        news: {},
      },
      indicatorWidth: new Animated.Value(0),
      indicatorLeft: new Animated.Value(0),
    }
    this._onCancel = this._onCancel.bind(this)
    this._clearText = this._clearText.bind(this)
  }

  _onTyping = (value) => {
    this.props.setQuery(value)
  };

  _onLayout = ({ _dispatchInstances: { key }, nativeEvent: { layout } }) => {
    this.setState(
      produce((draft) => {
        draft.tabLayout[key] = layout
      })
    )
  };

  _onCancel = () => {
    const { setCurrentTab, setQuery, onPressCancel } = this.props
    onPressCancel()
    setCurrentTab(null)
    setQuery('')
  };

  _clearText = () => {
    this.props.setQuery('')
    this.props.onPressX()
  };

  render() {
    const { query, currentTab, isSearching, onBack, placeholder, onSubmit } = this.props
    const { indicatorLeft, indicatorWidth } = this.state
    return (
      <View style={style.wrapper}>
        <View style={style.container}>
          <Touchable style={style.backButton}>
            <Icon
              centered
              onPress={onBack}
              background='dark-circle'
              size='large'
              color={SHIP_GREY_CALM}
              name='chevron-left'
              type='Entypo'
            />
          </Touchable>
          <View style={style.headerSearch}>
            <Icon
              centered
              background='dark-circle'
              size='large'
              color={SHIP_GREY_CALM}
              name='magnify'
              type='MaterialCommunityIcons'
              style={style.headerSearchIcon}
            />
            <TextInput
              value={query}
              onChangeText={this._onTyping}
              autoFocus={!__DEV__}
              placeholder={placeholder}
              onSubmitEditing={() => onSubmit() }
              style={style.headerSearchInput}
            />
            <VisibleView visible={!isEmpty(query) && isSearching}>
              <View style={style.loader}>
                <Loader size={'xmini'} isLoading />
              </View>
            </VisibleView>
            <VisibleView visible={!isEmpty(query) && !isSearching}>
              <Touchable onPress={this._clearText} style={style.clearText}>
                <Icon
                  centered
                  background='dark-circle'
                  size='mini'
                  color={SHIP_GREY_CALM}
                  name='close'
                  type='MaterialCommunityIcons'
                  style={style.headerSearchIcon}
                />
              </Touchable>
            </VisibleView>
          </View>
          <VisibleView visible={isEmpty(query) && isEmpty(currentTab)}>
            <Spacer horizontal size={WP4} />
          </VisibleView>
          <VisibleView visible={!isEmpty(query) || !isEmpty(currentTab)}>
            <Touchable onPress={this._onCancel} style={style.cancel}>
              <Text
                style={style.headerSearchCancel}
                type='Circular'
                size='xmini'
                weight={400}
                color={REDDISH}
              >
                Cancel
              </Text>
            </Touchable>
          </VisibleView>
        </View>
        <Animated.View
          style={[
            style.indicator,
            {
              width: indicatorWidth,
              left: indicatorLeft,
            },
          ]}
        />
        <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
      </View>
    )
  }
}

const style = StyleSheet.create({
  wrapper: {
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  container: {
    paddingVertical: WP2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerSearch: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'rgb(244, 246, 248)',
    paddingLeft: WP2,
    borderRadius: WP2,
  },
  headerSearchInput: {
    padding: WP1,
    flex: 1,
    color: SHIP_GREY
  },
  backButton: {
    paddingVertical: WP1,
    paddingHorizontal: WP3,
  },
  tabs: {
    flexDirection: 'row',
    paddingHorizontal: WP2,
    borderTopWidth: 1,
    borderTopColor: PALE_BLUE,
  },
  tab: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: WP3 + WP05,
  },
  indicator: {
    height: WP1 - WP05,
    backgroundColor: REDDISH,
    position: 'absolute',
    bottom: -1,
  },
  cancel: {
    paddingVertical: WP2 + WP05 + 0.5,
    paddingHorizontal: WP3,
  },
  clearText: {
    padding: WP2,
  },
  loader: {
    paddingHorizontal: WP2,
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(StartupSearchBox))
