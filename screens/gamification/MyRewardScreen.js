import { isEmpty, noop } from 'lodash-es'
import React, { Component } from 'react'
import { View } from 'react-native'
import { NavigationEvents } from '@react-navigation/compat'
import { connect } from 'react-redux'
import { getReward } from '../../actions/api'
import { _enhancedNavigation, Container, HeaderNormal, Image } from '../../components'
import AboutRewardModal from '../../components/gamification/AboutRewardModal'
import GamificationSectionActivity from '../../components/gamification/GamificationSectionActivity'
import GamificationSectionsRewards from '../../components/gamification/GamificationSectionsRewards'
import GamificationSectionUser from '../../components/gamification/GamificationSectionUser'
import { WHITE } from '../../constants/Colors'
import { WP1, WP6 } from '../../constants/Sizes'
import { getBottomSpace } from '../../utils/iphoneXHelper'
import { HEADER } from '../../constants/Styles'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
}

const mapFromNavigationParam = (getParam) => ({
})

class MyRewardScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: false,
      isLoading: true,
      result: {}
    }
  }

  async componentDidMount() {
    await this._getDataReward()
  }

  _getDataReward = async () => {
    const {
      dispatch,
      userData
    } = this.props
    try {
      const data = await dispatch(getReward, { id_user: userData.id_user }, noop, true, false)
      this.setState({ isReady: true })
      if (data.code === 200) {
        this.setState({ result: data.result })
      }
    } catch (e) {
      this.setState({ isReady: true })
    }
  }

  _onPullDownToRefresh = () => {
    this._getDataReward()
  }

  render() {
    const { navigateTo, navigateBack } = this.props
    const { isReady, isLoading, result } = this.state
    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        onPullDownToRefresh={this._onPullDownToRefresh}
        isReady={isReady}
        style={{ paddingBottom: getBottomSpace() }}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text='My Rewards'
            centered
            noRightPadding
            rightComponent={
              <AboutRewardModal
                description={~isEmpty(result.about_reward) ? result.about_reward : ''}
                triggerComponent={
                  <Image
                    style={HEADER.rightIcon}
                    source={require('../../assets/icons/icAbout.png')}
                    size='xtiny'
                    imageStyle={{ marginLeft: WP1 }}
                  />
                }
              />
            }
          />
        )}
      >
        <View style={{ paddingTop: WP6 }}>
          <NavigationEvents
            onWillFocus={this._onPullDownToRefresh}
          />
          <GamificationSectionUser isLoading={isLoading} user={result.user} />
          <GamificationSectionActivity navigateTo={navigateTo} isLoading={isLoading} acitvity={result.available_activity} />
          <GamificationSectionsRewards navigateTo={navigateTo} isLoading={isLoading} user={result.user} available_reward={result.available_reward} />
        </View>
      </Container>
    )
  }
}

MyRewardScreen.propTypes = {}

MyRewardScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(MyRewardScreen),
  mapFromNavigationParam
)
