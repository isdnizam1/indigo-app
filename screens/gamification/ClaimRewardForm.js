import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, StyleSheet, Modal } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { noop } from 'lodash-es'
import { isValidPhoneNumber } from '../../utils/helper'
import { Button, Container, InputTextLight, Text, HeaderNormal, Image, ModalMessageTrigger, _enhancedNavigation, ModalMessageView } from '../../components'
import { WP2, WP3, WP4, WP6, WP205, WP25, WP308, WP5, WP1, HP25 } from '../../constants/Sizes'
import { GREY, TOMATO, WHITE, GREY_SEMI, GREY_CHAT, TOMATO_CALM, TOMATO_SEMI, GREY_LABEL, GREY_WARM } from '../../constants/Colors'
import { SHADOW_STYLE } from '../../constants/Styles'
import { postClaim } from '../../actions/api'
import { getBottomSpace, ifIphoneX } from '../../utils/iphoneXHelper'

const styles = StyleSheet.create({
  input: {
    paddingHorizontal: WP205,
    paddingVertical: WP205,
    height: WP5 + WP308 + WP2,
    borderColor: GREY_SEMI,
    borderRadius: 6,
  }
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {}

const mapFromNavigationParam = (getParam) => ({
  dataReward: getParam('selectedReward', null),
  userReward: getParam('userReward', null),
})

class ClaimRewardForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      isReady: true,
      modalVisible: false,
      full_name: '',
      whatsapp: '',
      whatsapp_error: '',
      address: '',
      messageDescription: '',
      messageVisible: false,
    }
  }

  inputWhatsApp = React.createRef()
  inputAddress = React.createRef()

  _onChangeText = (key) => (value) => {
    this.setState({ [key]: value })
  }

  onWhatsappChange = (whatsapp) => {
    whatsapp && this.setState({ whatsapp: whatsapp.trim().replace(/\s/g, '') })
    if (whatsapp && isValidPhoneNumber(whatsapp)) this.setState({ whatsapp_error: '' })
    else if (!whatsapp) this.setState({ whatsapp_error: '', whatsapp: '' })
    else this.setState({ whatsapp_error: 'Enter valid whatsapp number' })
  }

  async onSubmitForm() {
    let { userReward: { id_user }, dataReward: { type, feature }, dispatch } = this.props
    let { whatsapp, full_name, address } = this.state
    this.setState({ isLoading: true })
    try {
      let body = { type, id_user, full_name, whatsapp, address, feature }
      const { code, message } = await dispatch(postClaim, body, noop, true, true)
      this.setState({ isLoading: false })
      if (code == 200) {
        this.setState({ modalVisible: true })
      } else {
        this.setState({
          messageDescription: message,
        }, () => {
          setTimeout(() => {
            this.setState({ messageVisible: true })
          }, 500)
        })
      }
    } catch (err) {
      this.setState({ isLoading: false })
    }
  }

  _customLabel = (title, subtitle, required = true) => {
    return (
      <Text weight={'500'} type='NeoSans' size={'mini'}>
        {title}
        {subtitle && (<Text weight={'300'} type='NeoSans' size={'mini'} color={GREY_LABEL}> {subtitle}</Text>)}
        {required && (<Text weight={'500'} type='NeoSans' size={'mini'} color={TOMATO}> *</Text>)}
      </Text>
    )
  }

  _backToMyRewards = () => {
    const { popToTop, navigateTo } = this.props
    Promise.all([
      popToTop(),
      navigateTo('MyRewardScreen')
    ])
  }

  render() {
    const {
      navigateBack,
      dataReward,
      navigation: { popToTop },
    } = this.props
    const {
      isLoading,
      isReady,
      modalVisible,
      full_name,
      whatsapp,
      whatsapp_error,
      address,
      messageDescription,
      messageVisible
    } = this.state
    let onSubmit = this.onSubmitForm.bind(this)
    return (
      <Container
        isLoading={isLoading}
        isReady={isReady}
        style={{ paddingBottom: getBottomSpace() }}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text='Reward Form'
            centered
          />
        )}
      >
        <View style={{ flex: 1, flexGrow: 1 }}>
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
            <View style={{ paddingTop: WP6, paddingHorizontal: WP6 }}>
              <View style={{ marginBottom: WP2 }}>
                <Text size='mini'>Please complete this form and our team will contact you for processing this reward.</Text>
                <View style={{ marginTop: WP4 }}>
                  <Text size='mini' type='NeoSans' weight={500}>My Reward</Text>
                  <View style={{ paddingVertical: WP2 }}>
                    <View
                      style={{
                        backgroundColor: WHITE,
                        alignItems: 'center',
                        flexDirection: 'row',
                        borderRadius: 12,
                        ...SHADOW_STYLE['shadowThin']
                      }}
                    >
                      <Image
                        centered
                        source={{ uri: dataReward?.background_img }}
                        style={{ borderRadius: 12, backgroundColor: GREY_CHAT }}
                        imageStyle={{ height: undefined, width: WP25, aspectRatio: 1, borderRadius: 12 }}
                      />
                      <View style={{ padding: WP3, height: WP25, flex: 1 }}>
                        <Text style={{ marginBottom: WP1 }} numberOfLines={2} ellipsizeMode='tail' size='tiny' weight={400} color={GREY}>{dataReward?.label}</Text>
                        <Text numberOfLines={1} size='tiny' color={GREY_WARM}>Reward for {dataReward?.type}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>

              <View>
                <View style={{ marginBottom: WP4 }}>
                  {this._customLabel('Full Name')}
                  <InputTextLight
                    bordered
                    size='mini'
                    backgroundColor={GREY_CHAT}
                    returnKeyType={'next'}
                    placeholder='Enter Full Name for Delivery'
                    placeholderTextColor={GREY_SEMI}
                    value={full_name}
                    onChangeText={this._onChangeText('full_name')}
                    containerStyle={{
                      height: WP308 + WP2
                    }}
                    textInputStyle={styles.input}
                    onSubmit={() => this.inputWhatsApp.current.focus()}
                  />
                </View>

                <View style={{ marginBottom: WP4 }}>
                  {this._customLabel('Whatsapp Number')}
                  <InputTextLight
                    innerRef={this.inputWhatsApp}
                    bordered
                    size='mini'
                    backgroundColor={GREY_CHAT}
                    returnKeyType={'done'}
                    keyboardType={'numeric'}
                    placeholder='Ex: 081234567890'
                    placeholderTextColor={GREY_SEMI}
                    value={whatsapp}
                    onChangeText={this.onWhatsappChange.bind(this)}
                    containerStyle={{
                      height: WP308 + WP2
                    }}
                    textInputStyle={styles.input}
                    error={whatsapp_error}
                    onSubmit={() => this.inputAddress.current.focus()}
                  />
                </View>

                <View style={{ marginBottom: WP4 }}>
                  {this._customLabel('Address', '(for Delivery)')}
                  <InputTextLight
                    innerRef={this.inputAddress}
                    bordered
                    size='mini'
                    backgroundColor={GREY_CHAT}
                    returnKeyType={'done'}
                    placeholder='Enter Address for Delivery'
                    placeholderTextColor={GREY_SEMI}
                    value={address}
                    onChangeText={this._onChangeText('address')}
                    containerStyle={{
                      height: WP308 + WP2
                    }}
                    textInputStyle={styles.input}
                  />
                </View>
              </View>
            </View>
          </KeyboardAwareScrollView>

          <View style={{ paddingHorizontal: WP6 }}>
            <View style={{ paddingBottom: ifIphoneX(WP3, WP6), paddingTop: WP2 }}>
              <ModalMessageTrigger
                image={null}
                subtitle={'Your form is ready,\nsend to Soundfren team?'}
                subtitleSize={'small'}
                buttonPrimaryText={'Yes'}
                buttonPrimaryBgColor={TOMATO_CALM}
                buttonPrimaryAction={onSubmit}
                buttonSecondaryText={'Check again'}
                triggerComponent={(togle) => (
                  <Button
                    colors={[TOMATO_CALM, TOMATO_CALM]}
                    compact='center'
                    radius={15}
                    textStyle={{ marginVertical: 10, fontWeight: '500', textTransform: 'none' }}
                    marginless
                    disable={!whatsapp || whatsapp_error || !full_name || !address}
                    disableColor={TOMATO_SEMI}
                    toggle
                    autoFocus
                    toggleActive={false}
                    toggleInactiveTextColor={WHITE}
                    toggleInactiveColor={whatsapp && !whatsapp_error && full_name && address ? TOMATO_CALM : TOMATO_SEMI}
                    text={'Continue'}
                    textSize='medium'
                    textType='NeoSans'
                    centered
                    onPress={() => togle()}
                    shadow='none'
                  />
                )}
              />
            </View>
          </View>
        </View>
        <Modal
          transparent
          onRequestClose={() => { }}
          animationType='fade'
          visible={modalVisible}
        >
          <View
            style={{
              flex: 1,
              backgroundColor: WHITE,
              justifyContent: 'space-between',
              paddingHorizontal: WP6,
              paddingBottom: ifIphoneX(getBottomSpace() + WP3, WP6),
            }}
          >
            <View style={{ flex: 1, paddingTop: HP25, alignItems: 'center' }}>
              <Text size='semiJumbo' weight={500}>Thank you</Text>
              <Text size='small'>We’ll be in touch soon!</Text>
            </View>
            <Button
              colors={[TOMATO_CALM, TOMATO_CALM]}
              compact='center'
              radius={15}
              textStyle={{ marginVertical: WP3 }}
              marginless
              text={'Okay, Got it'}
              textSize='medium'
              textType='NeoSans'
              textColor={WHITE}
              textWeight={500}
              centered
              shadow='none'
              onPress={() => {
                this.setState({ modalVisible: false }, () => popToTop())
              }}
            />
          </View>
        </Modal>
        <ModalMessageView
          toggleModal={() => this.setState({ messageVisible: !messageVisible })}
          isVisible={messageVisible}
          title={'Sorry'}
          subtitle={messageDescription}
          buttonPrimaryText={'Back to My Rewards'}
          buttonPrimaryAction={this._backToMyRewards}
        />
      </Container>
    )
  }

}

ClaimRewardForm.propTypes = {}
ClaimRewardForm.defaultProps = {}
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ClaimRewardForm),
  mapFromNavigationParam
)

