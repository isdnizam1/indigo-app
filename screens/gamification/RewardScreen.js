import React, { Component } from 'react'
import { View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { connect } from 'react-redux'
import { isEmpty, upperCase } from 'lodash-es'
import { Button, Container, HeaderNormal, Text, Image, _enhancedNavigation, ModalMessageTrigger } from '../../components'
import { GREY_CALM_SEMI, TOMATO_CALM, WHITE, WHITE_MILK } from '../../constants/Colors'
import { WP100, WP2, WP3, WP6, WP7 } from '../../constants/Sizes'
import { getBottomSpace, ifIphoneX } from '../../utils/iphoneXHelper'
import { postCheckClaim } from '../../actions/api'
import { REWARDS_STYLE } from '../../components/gamification/GamificationSectionsRewards'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
}

const mapFromNavigationParam = (getParam) => ({
  category: getParam('category', null),
  reward: getParam('reward', {}),
  userReward: getParam('userReward', {})
})

class RewardScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      message: null
    }
  }

  render() {
    const { navigateTo, navigateBack, reward, userReward } = this.props
    const widthItem = WP100
    const heightImage = widthItem / 2
    const styles = REWARDS_STYLE[upperCase(reward?.type)]
    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        isReady={true}
        style={{ paddingBottom: getBottomSpace() }}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text='Reward details'
            centered
          />
        )}
        outsideScrollContent={() => (
          <ModalMessageTrigger
            title={'Sorry'}
            subtitle={this.state.message ?? 'You’re not there yet..\nEarn more stars to claim this reward'}
            buttonPrimaryText={'Get My Stars'}
            buttonPrimaryAction={() => navigateBack()}
            triggerComponent={(togle) => (
              <View style={{ width: '100%', backgroundColor: WHITE, paddingHorizontal: WP6, paddingBottom: ifIphoneX(WP3, WP6), paddingTop: WP2 }}>
                <Button
                  colors={[TOMATO_CALM, TOMATO_CALM]}
                  compact='center'
                  radius={15}
                  textStyle={{ marginVertical: WP3 }}
                  marginless
                  text={'Click to claim'}
                  textSize='medium'
                  textType='NeoSans'
                  textColor={WHITE}
                  textWeight={500}
                  centered
                  shadow='none'
                  onPress={() => {
                    if (Number(userReward?.current_stage.stars) >= Number(reward?.stars)) {
                      const type = upperCase(reward?.type)
                      const routes = reward?.feature.includes('sf_play') ? 'ClaimAlbumList' : reward?.feature.includes('sf_connect') ? 'ClaimSessionList' : type == 'LEGEND' ? 'ClaimRewardForm' : ''
                      postCheckClaim({ id_user: this.props.userData.id_user, feature: reward?.feature }).then((response) => {
                        if (!isEmpty(routes) && response.data.code == 200) navigateTo(routes, { userReward, selectedReward: reward })
                        else {
                          this.setState({ message: response.data.message })
                          togle()
                        }
                      })
                    } else {
                      togle()
                    }
                  }}
                />
              </View>
            )}
          />
        )}
      >
        <View>
          <View style={{ backgroundColor: WHITE_MILK, height: heightImage, width: '100%' }}>
            <Image
              source={{ uri: reward?.background_img }}
              imageStyle={{ width: WP100, height: heightImage, aspectRatio: 2 }}
            />
            <LinearGradient
              colors={styles.colors}
              start={styles.start}
              end={styles.end}
              style={{
                width: '100%',
                height: heightImage,
                position: 'absolute',
                zIndex: 2,
                justifyContent: 'flex-end',
                padding: WP3,
              }}
            />
          </View>
          <View style={{ paddingHorizontal: WP6, paddingTop: WP7 }}>
            <Text size='xmini' weight={500} style={{ marginBottom: WP3 }}>{`Reward for ${reward?.type}`}</Text>
            <Text size='mini' style={{ marginBottom: WP3 }}>{reward?.label}</Text>
            <View style={{ width: '100%', height: 1, backgroundColor: GREY_CALM_SEMI, marginBottom: WP3 }} />
            <View>
              {
                reward?.description.length > 0 && (
                  reward?.description.map((item, index) => (
                    <Text key={index}>
                      <Text size='tiny' weight={500}>{`${index + 1}. `}</Text>
                      <Text size='tiny' weight={300}>{item}</Text>
                    </Text>
                  ))
                )
              }
            </View>
          </View>
        </View>
      </Container>
    )
  }
}

RewardScreen.propTypes = {}

RewardScreen.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(RewardScreen),
  mapFromNavigationParam
)
