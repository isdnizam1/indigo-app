import { first, includes, noop, remove } from 'lodash-es'
import React, { Component } from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { getAds, postClaim } from '../../actions/api'
import { Button, Container, HeaderNormal, Text, _enhancedNavigation, ModalMessageTrigger, ModalMessageView } from '../../components'
import { GREY_WARM, TOMATO_CALM, TOMATO_SEMI, WHITE } from '../../constants/Colors'
import { WP1, WP2, WP3, WP35, WP6 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { voucherDispatcher } from '../../services/voucher'
import { getBottomSpace } from '../../utils/iphoneXHelper'
import { speakerDisplayName } from '../../utils/transformation'
import SessionItem from '../soundconnect/SessionItem'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
  voucherSet: voucherDispatcher.voucherSet
}

const mapFromNavigationParam = (getParam) => ({
  dataReward: getParam('selectedReward', {}),
  userReward: getParam('userReward', {})
})

class ClaimSessionList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: false,
      selected: [],
      sessionList: [],
      sessionLoading: true,
      messageDescription: '',
      messageVisible: false,
    }
  }

  async componentDidMount() {
    await this._getAdsList()
  }

  _getAdsList = async () => {
    const {
      dispatch,
      dataReward: { feature },
    } = this.props

    this.setState({ sessionLoading: true })
    try {
      const data = await dispatch(getAds, {
        status: 'active',
        category: 'sc_session',
        session_type: feature.includes('special_edition') ? 'special' : 'standard'
      }, noop, true, true)
      this.setState({ sessionLoading: false, isReady: true })
      if (data.code === 200) {
        this.setState({ sessionList: data.result })
      }
    } catch (e) {
      this.setState({ sessionLoading: false, isReady: true })
    }
  }

  _onPullDownToRefresh = () => {
    this.setState({
      sessionList: [],
    }, this.componentDidMount)
  }

  _selectItem = async (item) => {
    const {
      selected
    } = this.state
    if (includes(selected, item)) {
      remove(selected, (data) => data === item)
    } else {
      remove(selected)
      selected.push(item)
    }
    this.setState({ selected })
  }

  _renderItem = ({ item, index }) => {
    const {
      selected
    } = this.state
    const isSelected = includes(selected, item)

    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY}
        key={index}
        style={{
          marginBottom: WP2
        }}
        onPress={() => {
          this._selectItem(item)
        }}
      >
        <SessionItem
          isSelected={isSelected}
          ads={item}
          loading={this.state.sessionLoading}
          showButton={false}
        />
      </TouchableOpacity>
    )
  }

  _onClaim = async () => {
    const { dispatch, popToTop, navigateTo, dataReward: { type, feature }, userReward: { id_user }, voucherSet } = this.props
    const { selected } = this.state
    try {
      const id_ads = first(selected).id_ads
      let body = { type, id_user, id_ads, feature }

      const { code, result, message } = await dispatch(postClaim, body, noop, true, true)
      if (code == 200) {
        Promise.all([
          voucherSet(result),
          popToTop(),
          navigateTo('SessionRegisterForm', { id_ads })
        ])
      } else {
        this.setState({
          messageDescription: message,
        }, () => {
          setTimeout(() => {
            this.setState({ messageVisible: true })
          }, 500)
        })
      }
    } catch (e) {
      //
    }
  }

  _backToMyRewards = () => {
    const { popToTop, navigateTo } = this.props
    Promise.all([
      popToTop(),
      navigateTo('MyRewardScreen')
    ])
  }

  render() {
    const { navigateBack, isLoading } = this.props
    const {
      selected,
      isReady,
      sessionList,
      sessionLoading,
      messageDescription,
      messageVisible,
    } = this.state
    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        style={{ paddingBottom: getBottomSpace() }}
        onPullDownToRefresh={this._onPullDownToRefresh}
        isReady={isReady}
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            centered
            text='Session list'
            rightComponent={(
              <ModalMessageTrigger
                title={first(selected)?.title}
                titleSize={'tiny'}
                subtitle={speakerDisplayName(first(selected)?.speaker)}
                subtitleSize={'tiny'}
                subtitleColor={GREY_WARM}
                subtitleStyle={{ marginBottom: WP3 }}
                fullImage
                image={{ uri: first(selected)?.image }}
                aspectRatio={2.5}
                imageStyle={{ height: WP35 }}
                buttonPrimaryText={'Get Free 1 Ticket'}
                buttonPrimaryBgColor={TOMATO_CALM}
                buttonPrimaryAction={() => this._onClaim()}
                buttonSecondaryText={'Choose another'}
                triggerComponent={(togle) => (
                  <Button
                    onPress={() => togle()}
                    colors={[TOMATO_CALM, TOMATO_CALM]}
                    compact='center'
                    radius={6}
                    contentStyle={{ paddingHorizontal: WP3, paddingVertical: WP1 }}
                    marginless
                    toggle
                    toggleActive={false}
                    disable={!(selected.length > 0)}
                    disableColor={TOMATO_SEMI}
                    toggleInactiveTextColor={WHITE}
                    toggleInactiveColor={selected.length > 0 ? TOMATO_CALM : TOMATO_SEMI}
                    shadow='none'
                    text='Next'
                    textType='NeoSans'
                    textSize='tiny'
                    textWeight={500}
                  />
                )}
              />
            )}
          />
        )}
      >
        <View style={{ paddingHorizontal: WP6, paddingTop: WP6 }}>
          <Text centered size='mini'>
            Choose
            <Text centered size='mini' weight={500}> 1 session </Text>
            to get free 1 ticket
          </Text>

          <FlatList
            contentContainerStyle={{
              paddingTop: WP6
            }}
            data={sessionList}
            keyExtractor={(item, index) => `key${index}`}
            renderItem={this._renderItem}
            ListEmptyComponent={
              !sessionLoading && (
                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                  <Text centered size='mini' type='NeoSans'>{'It’s empty here,\n coming soon on next session.'}</Text>
                </View>
              )
            }
          />
        </View>
        <ModalMessageView
          toggleModal={() => this.setState({ messageVisible: !messageVisible })}
          isVisible={messageVisible}
          title={'Sorry'}
          subtitle={messageDescription}
          buttonPrimaryText={'Back to My Rewards'}
          buttonPrimaryAction={this._backToMyRewards}
        />
      </Container>
    )
  }
}

ClaimSessionList.propTypes = {}

ClaimSessionList.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ClaimSessionList),
  mapFromNavigationParam
)
