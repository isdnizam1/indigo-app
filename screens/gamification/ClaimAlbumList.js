import { first, includes, noop, remove } from 'lodash-es'
import React, { Component } from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { getAds, postClaim } from '../../actions/api'
import { Button, Container, HeaderNormal, Text, _enhancedNavigation, Image, ModalMessageTrigger, ModalMessageView } from '../../components'
import { GREY_WARM, TOMATO_CALM, TOMATO_SEMI, WHITE } from '../../constants/Colors'
import { WP1, WP2, WP20, WP3, WP305, WP6 } from '../../constants/Sizes'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { getBottomSpace } from '../../utils/iphoneXHelper'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapDispatchToProps = {
}

const mapFromNavigationParam = (getParam) => ({
  dataReward: getParam('selectedReward', {}),
  userReward: getParam('userReward', {})
})

class ClaimAlbumList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isReady: false,
      selected: [],
      albumList: [],
      albumLoading: true,
      messageDescription: '',
      messageVisible: '',
    }
  }

  async componentDidMount() {
    await this._getAdsList()
  }

  _getAdsList = async () => {
    const {
      dispatch
    } = this.props

    this.setState({ albumLoading: true })
    try {
      const data = await dispatch(getAds, {
        status: 'active',
        category: 'sp_album',
      }, noop, true, true)
      this.setState({ albumLoading: false, isReady: true })
      if (data.code === 200) {
        this.setState({ albumList: data.result })
      }
    } catch (e) {
      this.setState({ albumLoading: false, isReady: true })
    }
  }

  _onPullDownToRefresh = () => {
    this.setState({
      albumList: [],
    }, this.componentDidMount)
  }

  _selectItem = async (item) => {
    const {
      selected
    } = this.state
    if (includes(selected, item)) {
      remove(selected, (data) => data === item)
    } else {
      remove(selected)
      selected.push(item)
    }
    this.setState({ selected })
  }

  _renderItem = ({ item, index }) => {
    const {
      selected
    } = this.state
    const isSelected = includes(selected, item)

    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY}
        key={index}
        style={{
          marginBottom: WP2
        }}
        onPress={() => {
          this._selectItem(item)
        }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ marginRight: WP3 }}>
            <Image
              imageStyle={{ width: WP305, aspectRatio: 1 }}
              source={isSelected ? require('../../assets/icons/icCheckboxActive.png') : require('../../assets/icons/icCheckboxInactive.png')}
            />
          </View>
          <View style={{
            backgroundColor: WHITE,
            borderRadius: 12,
            width: WP20, height: WP20,
            marginRight: WP3,
          }}
          >

            <View style={{ overflow: 'hidden', borderRadius: 12 }}>
              <Image
                source={{ uri: item.image }}
                aspectRatio={1}
                imageStyle={{ width: WP20, height: WP20 }}
              />
            </View>
          </View>
          <View style={{ height: '100%' }}>
            <Text numberOfLines={3} size='tiny' type='NeoSans' weight={400}>{item.title}</Text>
            <Text numberOfLines={1} size='tiny' color={GREY_WARM}>{item.speaker?.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _onClaim = async () => {
    const { dispatch, popToTop, navigateTo, dataReward: { type, feature }, userReward: { id_user } } = this.props
    const { selected } = this.state
    try {
      const id_ads = first(selected).id_ads
      let body = { type, id_user, id_ads, feature }

      const { code, message } = await dispatch(postClaim, body, noop, true, true)
      if (code == 200) {
        Promise.all([
          popToTop(),
          navigateTo('SoundplayDetail', { id_ads })
        ])
      } else {
        this.setState({
          messageDescription: message,
        }, () => {
          setTimeout(() => {
            this.setState({ messageVisible: true })
          }, 500)
        })
      }
    } catch (e) {
      //
    }
  }

  _backToMyRewards = () => {
    const { popToTop, navigateTo } = this.props
    Promise.all([
      popToTop(),
      navigateTo('MyRewardScreen')
    ])
  }

  render() {
    const { navigateBack, isLoading } = this.props
    const {
      selected,
      isReady,
      albumList,
      albumLoading,
      messageDescription,
      messageVisible,
    } = this.state
    return (
      <Container
        theme='dark'
        scrollable
        scrollBackgroundColor={WHITE}
        style={{ paddingBottom: getBottomSpace() }}
        onPullDownToRefresh={this._onPullDownToRefresh}
        isReady={isReady}
        isLoading={isLoading}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            centered
            text='Album list'
            rightComponent={(
              <ModalMessageTrigger
                title={first(selected)?.title}
                titleSize={'tiny'}
                subtitle={first(selected)?.speaker?.title}
                subtitleSize={'tiny'}
                subtitleColor={GREY_WARM}
                subtitleStyle={{ marginBottom: WP3 }}
                image={null}
                customImage={(
                  <View style={{
                    marginTop: WP6,
                    backgroundColor: WHITE,
                    borderRadius: 12,
                    width: WP20,
                    height: WP20,
                    ...SHADOW_STYLE['shadow']
                  }}
                  >
                    <View style={{ overflow: 'hidden', borderRadius: 12 }}>
                      <Image
                        source={{ uri: first(selected)?.image }}
                        aspectRatio={1}
                        imageStyle={{ width: WP20, height: WP20 }}
                      />
                    </View>
                  </View>
                )}
                buttonPrimaryText={'Get Free Access'}
                buttonPrimaryBgColor={TOMATO_CALM}
                buttonPrimaryAction={() => this._onClaim()}
                buttonSecondaryText={'Choose another'}
                triggerComponent={(togle) => (
                  <Button
                    onPress={() => togle()}
                    colors={[TOMATO_CALM, TOMATO_CALM]}
                    compact='center'
                    radius={6}
                    contentStyle={{ paddingHorizontal: WP3, paddingVertical: WP1 }}
                    marginless
                    toggle
                    toggleActive={false}
                    disable={!(selected.length > 0)}
                    disableColor={TOMATO_SEMI}
                    toggleInactiveTextColor={WHITE}
                    toggleInactiveColor={selected.length > 0 ? TOMATO_CALM : TOMATO_SEMI}
                    shadow='none'
                    text='Next'
                    textType='NeoSans'
                    textSize='tiny'
                    textWeight={500}
                  />
                )}
              />
            )}
          />
        )}
      >
        <View style={{ paddingHorizontal: WP6, paddingTop: WP6 }}>
          <Text centered size='mini'>
            Choose
            <Text centered size='mini' weight={500}> 1 album </Text>
            to get free access
          </Text>

          <FlatList
            contentContainerStyle={{
              paddingTop: WP6
            }}
            data={albumList}
            keyExtractor={(item, index) => `key${index}`}
            renderItem={this._renderItem}
            ListEmptyComponent={
              !albumLoading && (
                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                  <Text centered size='mini' type='NeoSans'>{'It’s empty here,\n coming soon audio content.'}</Text>
                </View>
              )
            }
          />
        </View>
        <ModalMessageView
          toggleModal={() => this.setState({ messageVisible: !messageVisible })}
          isVisible={messageVisible}
          title={'Sorry'}
          subtitle={messageDescription}
          buttonPrimaryText={'Back to My Rewards'}
          buttonPrimaryAction={this._backToMyRewards}
        />
      </Container >
    )
  }
}

ClaimAlbumList.propTypes = {}

ClaimAlbumList.defaultProps = {}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ClaimAlbumList),
  mapFromNavigationParam
)
