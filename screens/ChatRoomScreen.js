import React, { Component, useEffect } from "react";
import PropTypes, { object } from "prop-types";
import { connect } from "react-redux";
import {
  BackHandler,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  TouchableOpacity,
  View,
} from "react-native";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import * as DocumentPicker from "expo-document-picker";
import { concat, isEmpty, isFunction, noop, orderBy } from "lodash-es";
import moment from "moment";
import { FlatList } from "react-native-gesture-handler";
import _enhancedNavigation from "../navigation/_enhancedNavigation";
import Header from "../components/Header";
import Icon from "../components/Icon";
import {
  GREY_CHAT,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from "../constants/Colors";
import Container from "../components/Container";
import ChatCommentForm from "../components/chat/ChatCommentForm";
import {
  getAccountTypeById,
  postLogMessaging,
  postMessageNotification,
  postSendNotifGroup,
  postTriggerPusher,
  getDetailMessage,
  postSendMessage,
} from "../actions/api";
import ChatBubble from "../components/chat/ChatBubble";
import Text from "../components/Text";
import PusherClient from "../utils/clientPusher";
import { WP1, WP3, WP4 } from "../constants/Sizes";
import ChatDate from "../components/chat/ChatDate";
import Avatar from "../components/Avatar";
import Image from "../components/Image";
import {
  constructNewComment,
  getTextMessage,
  _constructListComments,
} from "../utils/chat";
import ChatColorPalette from "../constants/ChatColorsPalette";
import { messageDispatcher } from "../services/message";
import { getAuthJWT, setCommentList } from "../utils/storage";
import Paho from "paho-mqtt";
import { NavigationEvents } from "@react-navigation/compat";
import Loader from "../components/Loader";

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

const mapFromNavigationParam = (getParam) => ({
  comments: getParam("comments", []),
  onRefresh: getParam("onRefresh", () => {}),
  // onUpdateRoom: getParam("onUpdateRoom", () => {}),
  // template: getParam("template", ""),
  navigateBackCallback: getParam("navigateBackCallback", undefined),
  accountType: getParam("accountType", {}),
  idReceiver: getParam("with_id_user", {}),
  receiver_FullName: getParam("user_fullname", {}),
  receiver_ProfilePicture: getParam("user_profile_picture", {}),
  id_groupmessage: getParam("id_groupmessage", {}),
  type: getParam("type", {}),
  fromScreen: getParam("fromScreen", {}),
});

const mapDispatchToProps = {
  setMessage: messageDispatcher.setMessage,
};

class ChatRoomScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      atchImage: null,
      isSendingFile: false,
      isPremium: false,
      isReady: false,
      isReachEnd: false,
      userColors: {},
      comments: this.props.comments || [],
      accountType: this.props.accountType,
      messageList: [],
      hasScroll: false,
      lastMessage: false,
      sendingMessage: false,
      typedMessage: null,
      idGroup:
        typeof this.props.id_groupmessage !== "object"
          ? this.props.id_groupmessage
          : null,
      receiver: null,
      receiverFullName: "",
      receiverProfilePicture: null,
      isGroup: false,
      token: null,
    };
    this._navigateBackCallback = this._navigateBackCallback.bind(this);
  }

  static navigationOptions = () => ({
    gesturesEnabled: false,
  });

  // _isGroup = () => this.setState({ isGroup: true });

  _mapUserColor = (participants) => {
    const userColors = {};
    participants.map((user, index) => {
      userColors[user.id_user] = ChatColorPalette[index];
    });
    return userColors;
  };

  componentDidMount = async () => {
    const { userData, accountType, dispatch, receiver, onRefresh, navigation } =
      this.props;
    this._getDetailMessage();

    this.state.token = await getAuthJWT();

    // BackHandler.addEventListener("hardwareBackPress", this._backHandler);
    this._mqtt.addEventListener("mqtt", this._mqtt());
    if (typeof accountType === "object") {
      const accountTypes = await dispatch(getAccountTypeById, {
        id_user: this.props.idReceiver,
      });
      this.setState({
        accountType: accountTypes.verified_status,
      });
    }
  };

  componentWillUnmount = async () => {
    const { userData, navigation } = this.props;

    this.props.onRefresh();
    BackHandler.removeEventListener("hardwareBackPress", this._backHandler);
  };

  _mqtt(chat) {
    const { userData, type, idReceiver, id_groupmessage } = this.props;
    const { token, idGroup, messageList } = this.state;
    const subscription = __DEV__ ? "dev-message/" : "message/";

    var client = new Paho.Client(
      "livechat.eventeer.id/mqtt",
      Number(8081),
      "mqtt"
    );

    var date = moment().format("YYYY-MM-DD HH:mm:ss");

    const personalData =
      id_groupmessage !== undefined
        ? {
            created_at: date,
            token: token,
            full_name: userData.full_name,
            id_user: userData.id_user,
            id_groupmessage: idGroup,
            with_id_user: idReceiver,
            message: chat,
            type: "personal",
          }
        : {
            created_at: date,
            token: token,
            full_name: userData.full_name,
            id_user: userData.id_user,
            with_id_user: idReceiver,
            message: chat,
            type: "personal",
          };

    const data =
      type === "group"
        ? {
            created_at: date,
            token: token,
            full_name: userData.full_name,
            id_user: userData.id_user,
            id_groupmessage: idGroup,
            message: chat,
            type: "group",
          }
        : personalData;

    client.connect({
      onSuccess: function () {
        console.log("connected" + idGroup);
        client.subscribe("dev-message/" + `${idGroup}`);
        if (chat != undefined) {
          var message = new Paho.Message(JSON.stringify(data));
          message.destinationName = 'dev-message/' + `${idGroup}`;
          client.send(message);
        }
      },
      userName: "username",
      password: "password",
      useSSL: true,
    });

    client.onMessageArrived = (message) => {
      const newMessage = JSON.parse(message.payloadString);
      const messageList = this.state.messageList;
      messageList.push(newMessage);
      this.setState({ sendingMessage: false, messageList: messageList });
      console.log("payload", message.payloadString);
      console.log("messageList", messageList);
    };
  }

  _onSubmitComment = async ({ formData }) => {
    const { userData, dispatch, idReceiver, id_groupmessage } = this.props;
    const { comments, idGroup } = this.state;
    const body =
      idGroup !== null
        ? {
            id_user: userData.id_user,
            message: formData.comment,
            with_id_user: idReceiver,
            id_groupmessage: idGroup,
          }
        : {
            id_user: userData.id_user,
            message: formData.comment,
            with_id_user: idReceiver,
          };
    const unique_id = `${userData.id_user}${Math.floor(Date.now() / 1000)}`;

    this._mqtt(formData.comment);
    this.setState({
      ownComment: true,
      typedMessage: formData.comment,
      sendingMessage: true,
    });

    this._setMessage([
      { unique_id, ownComment: true, message: formData.comment },
      ...comments,
    ]);

    this._logMessage();
  };

  _setMessage(newComments) {
    this.setState({ comments: newComments, isReady: true, loading: false });
  }
  _logMessage() {
    const { dispatch, userData } = this.props;

    const { receiver } = this.state;

    dispatch(
      postLogMessaging,
      { id_user: userData.id_user, id_target: receiver.email },
      null,
      false,
      false
    );
  }

  _triggerNotification(sendComment) {
    const { dispatch, userData, onRefresh } = this.props;
    const { receiver, room, comments } = this.state;

    const newComment = constructNewComment(sendComment);

    const newComments = comments.slice();
    const objIndex = comments.findIndex(
      (obj) => obj.unique_id === newComment.unique_id
    );
    newComments[objIndex] = newComment;

    this._setMessage(newComments);

    const text = getTextMessage(sendComment);

    const pusherTriggerBody = {
      id_recipient: receiver.email,
      id_sender: userData.id_user,
      event: "new_message",
      url_mobile: "new_message",
      // content: `New message from ${this.receiverFullName}`,
    };

    dispatch(postTriggerPusher, pusherTriggerBody, noop, true);

    // if (this.isGroup()) {
    const notifGroupPayload = {
      id_sender: userData.id_user,
      message: text,
      // title: `${userData.full_name} @ ${sendComment.room_name}`,
    };
    dispatch(postSendNotifGroup, notifGroupPayload, noop, true);
    // } else {
    const expoTriggerBody = {
      id_recipient: receiver.email,
      id_sender: userData.id_user,
      text,
    };
    dispatch(postMessageNotification, expoTriggerBody, noop, true);
    // }

    onRefresh();
  }

  _getDetailMessage = async (isLoading = true) => {
    const {
      userData: { id_user },
      dispatch,
      idReceiver,
      receiver_FullName,
      receiver_ProfilePicture,
      type,
    } = this.props;
    const { messageList, idGroup } = this.state;
    if (idGroup != null) {
      const params = {
        id_user: id_user,
        id_groupmessage: idGroup,
        type: type === "group" ? "1" : "0",
      };
      const dataResponse = await dispatch(
        getDetailMessage,
        params,
        noop,
        true,
        isLoading
      );

      if (dataResponse.code == 200) {
        let newMessage = messageList;

        if (!isEmpty(dataResponse.result.message))
          newMessage = concat(newMessage, dataResponse.result.message);
        this.setState({
          messageList: dataResponse.result.message,
          lastMessage:
            dataResponse.result.message.length === messageList.length,
          sendingMessage: false,
          isGroup: type === "group" ? true : false,
          isReady: true,
          receiver: idReceiver !== null ? idReceiver : null,
          receiverFullName:
            dataResponse.result.header.title ||
            dataResponse.result.header.full_name,
          receiverProfilePicture:
            dataResponse.result.header.profile_picture ||
            dataResponse.result.header.image,
          userColors:
            type === "group"
              ? this._mapUserColor(dataResponse.result.member)
              : "fd7e14",
        });
      }
    } else {
      this.setState({
        receiver: idReceiver,
        receiverFullName: receiver_FullName,
        receiverProfilePicture: receiver_ProfilePicture,
        userColors:
          type === "group"
            ? this._mapUserColor(dataResponse.result.member)
            : "fd7e14",
        isReady: true,
      });
    }
  };

  _backHandler = async () => {
    const { navigateBackCallback, navigateBack, navigateTo } = this.props;

    if (isFunction(navigateBackCallback)) {
      navigateBackCallback();
    } else {
      navigateBack();
    }
  };

  _onSelectPhoto = async () => {
    if (Platform.OS === "ios") {
      StatusBar.setHidden(true);
    }
    const { status: statusCameraRoll } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    if (statusCameraRoll === "granted") {
      let result = await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: Platform.OS !== "ios",
      });
      return result;
    }
  };

  _onChooseFile = async () => {
    if (Platform.OS === "ios") {
      StatusBar.setHidden(true);
    }

    let result = await DocumentPicker.getDocumentAsync({});
    console.log("result", result);
    this._onUploadFile(result);
  };

  _onUploadFile = async (file) => {
    if (Platform.OS === "ios") {
      StatusBar.setHidden(false);
    }
    const { userData, idReceiver } = this.props;

    const { comments, room, idGroup } = this.state;

    if (!file) {
      return;
    }

    if (!file.cancelled) {
      await this.setState({ isSendingFile: true });
      const fileData = file;
      console.log("picture", file);

      const body =
        idGroup !== null
          ? {
              id_user: userData.id_user,
              with_id_user: idReceiver,
              id_groupmessage: idGroup,
              attachment: fileData,
            }
          : {
              id_user: userData.id_user,
              with_id_user: idReceiver,
              id_groupmessage: idGroup,
              attachment: fileData,
            };

      const unique_id = `${userData.id_user}${Math.floor(Date.now() / 1000)}`;
      this._setMessage([{ unique_id, ownComment: true }, ...comments]);

      try {
        const response = await postSendMessage(body);
        console.log("response", response);
        this.setState({ isSendingFile: false });
        this._triggerNotification(sendComment);
      } catch (e) {
        this.setState({ isSendingFile: false });
      }
    }
  };

  _takePhoto = async () => {
    if (Platform.OS === "ios") {
      StatusBar.setHidden(true);
    }
    const { status: statusCamera } = await Permissions.askAsync(
      Permissions.CAMERA
    );
    const { status: statusCameraRoll } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    if (statusCamera === "granted" && statusCameraRoll === "granted") {
      let result = await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: Platform.OS !== "ios",
      });
      return result;
    }
  };

  _onSendPhoto = async (pictureResult) => {
    if (Platform.OS === "ios") {
      StatusBar.setHidden(false);
    }
    const { userData, idReceiver } = this.props;

    const { comments, room, idGroup } = this.state;

    if (!pictureResult) {
      return;
    }

    if (!pictureResult.cancelled) {
      await this.setState({ isSendingFile: true });
      const fileData = pictureResult.base64;
      console.log("picture", pictureResult.base64);

      const body =
        idGroup !== null
          ? {
              id_user: userData.id_user,
              with_id_user: idReceiver,
              id_groupmessage: idGroup,
              attachment: fileData,
            }
          : {
              id_user: userData.id_user,
              with_id_user: idReceiver,
              id_groupmessage: idGroup,
              attachment: fileData,
            };

      const unique_id = `${userData.id_user}${Math.floor(Date.now() / 1000)}`;
      this._setMessage([{ unique_id, ownComment: true }, ...comments]);

      try {
        const response = await postSendMessage(body);
        console.log("response", response);
        this.setState({ isSendingFile: false });
        this._triggerNotification(sendComment);
      } catch (e) {
        this.setState({ isSendingFile: false });
      }
    }
  };

  _onDeleteComment = (message) => async () => {
    const { qiscus } = this.props;

    const { room, comments } = this.state;

    try {
      const newComments = comments.filter(
        (comment) => comment.unique_id !== message.unique_id
      );
      this._setMessage(newComments);
    } catch (e) {}
  };

  _onUpdateRoom = () => {
    // this._getRoom();
  };

  _onPressRoom = () => {
    const { navigateTo, idReceiver } = this.props;
    const { isGroup } = this.state;

    if (isGroup) {
      this._navigateToGroupEdit();
    } else {
      navigateTo("ProfileScreen", { idUser: idReceiver }, "push");
    }
  };

  _navigateToGroupEdit = () => {
    const { navigateTo, id_groupmessage } = this.props;

    const { room, receiverFullName, receiverProfilePicture } = this.state;

    navigateTo("GroupDetailScreen", {
      editing: true,
      id_groupmessage: id_groupmessage,
      groupname: receiverFullName,
      groupimage: receiverProfilePicture,
      onEditGroup: this._onEditGroup,
    });
  };

  _onEditGroup = (data) => {
    this.setState(data);
  };

  _loadMoreChat = async () => {
    const {
      userData: { id_user },
    } = this.props;
    const { isReachEnd } = this.state;
    if (!isReachEnd) {
      this.setState({ isReachEnd: false });
      await this._getDetailMessage({ id_user: id_user }, noop, true, true);
      this.setState({ isReachEnd: true });
    } else {
      //
    }
  };

  _sendingMessage = (message) => {
    const {
      userData: { id_user },
    } = this.props;
    const { sendingMessage } = this.state;
    if (sendingMessage) {
      let newMessage = [message];
      newMessage = concat(newMessage, messageList);
    }
  };

  _navigateBackCallback = async () => {
    this.props.popToTop();
    this.props.navigateTo("ChatScreen");
  };

  render() {
    const {
      navigateTo,
      template,
      userData,
      navigateBack,
      navigateBackCallback,
      navigation,
      fromScreen,
    } = this.props;

    const {
      isReady,
      loading,
      comments,
      receiver,
      receiverFullName,
      receiverProfilePicture,
      isPremium,
      userColors,
      isReachEnd,
      isSendingFile,
      accountType,
      messageList,
      lastMessage,
      hasScroll,
      sendingMessage,
      typedMessage,
    } = this.state;

    // const isGroup = this.isGroup();
    const chatList = orderBy(messageList, ["created_at"], ["desc"]);

    return (
      <Container
        isLoading={false}
        isReady={isReady}
        borderedHeader
        colors={[WHITE, GREY_CHAT]}
        renderHeader={() => (
          <Header
            backgroundColor={WHITE}
            style={{
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Icon
              onPress={
                fromScreen != "NewGroupChatScreen"
                  ? navigateBack
                  : this._navigateBackCallback
                // : resetAction(
                //     navigation,
                //     1,
                //     [NavigationActions.navigate({ routeName: "ChatScreen" })],
                //     "ExploreScreen"
                //   )
              }
              size="large"
              color={SHIP_GREY_CALM}
              style={{ marginRight: WP4 }}
              name="chevron-left"
              type="Entypo"
              centered
            />
            <TouchableOpacity
              onPress={this._onPressRoom}
              style={{
                flexDirection: "row",
                flexGrow: 1,
                justifyContent: "flex-start",
                alignItems: "center",
              }}
            >
              <Avatar
                image={receiverProfilePicture}
                size="mini"
                style={{ marginRight: WP4 }}
                verifiedStatus={
                  typeof accountType === "object"
                    ? accountType.verified_status
                    : ""
                }
                verifiedSize={WP3}
              />
              <Text size="mini" color={SHIP_GREY} weight={400} centered>
                {`${receiverFullName}`}
              </Text>
              {isPremium && (
                <Image
                  size="xtiny"
                  style={{ marginLeft: WP1 }}
                  source={require("../assets/icons/badge.png")}
                />
              )}
            </TouchableOpacity>
          </Header>
        )}
      >
        <FlatList
          inverted
          keyboardShouldPersistTaps="handled"
          showsVerticalScrollIndicator={false}
          initialNumToRender={15}
          style={{
            paddingHorizontal: WP4,
            backgroundColor: WHITE,
          }}
          ListFooterComponent={
            !isReachEnd &&
            chatList.length >= 20 && <Loader size="mini" isLoading />
          }
          ListHeaderComponent={
            !isSendingFile ? (
              sendingMessage && (
                <ChatBubble
                  isSendingFile={sendingMessage}
                  message={typedMessage}
                />
              )
            ) : (
              <ChatBubble isSendingFile={isSendingFile} />
            )
          }
          onMomentumScrollBegin={() => this.setState({ hasScroll: true })}
          onMomentumScrollEnd={() => this.setState({ hasScroll: false })}
          onEndReachedThreshold={0.01}
          onEndReached={this._loadMoreChat}
          data={chatList}
          keyExtractor={(item) => `item${item.id_message}`}
          renderItem={({ item, index }) => {
            const chat = item;
            const previousDate = chatList[index + 1]
              ? chatList[index + 1].created_at
              : undefined;

            return (
              <View key={chat.id_message}>
                {!moment(chat.created_at).isSame(
                  moment(previousDate),
                  "day"
                ) && <ChatDate date={chat.created_at} />}
                {chat.type !== "notification" && (
                  <ChatBubble
                    key={index}
                    message={chat.message}
                    username={chat.full_name}
                    avatar={chat.avatar}
                    timeStamp={chat.created_at}
                    ownComment={chat.id_user === userData.id_user}
                    navigateTo={navigateTo}
                    participant={receiver}
                    payload={chat}
                    onDelete={this._onDeleteComment(chat)}
                    isGroup={this.state.isGroup}
                    color={userColors[chat.id_user]}
                    isSendingFile={isSendingFile}
                  />
                )}
                {chat.type === "notification" && (
                  <ChatBubble
                    key={index}
                    userData={userData}
                    payload={chat}
                    isGroup={this.state.isGroup}
                  />
                )}
              </View>
            );
          }}
        />
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <ChatCommentForm
            template={template}
            onSubmit={this._onSubmitComment}
            onSelectPhoto={this._onSelectPhoto}
            onTakePhoto={this._takePhoto}
            onUploadPhoto={this._onSendPhoto}
            onChooseFile={this._onChooseFile}
            onUploadFile={this._onUploadFile}
          />
        </KeyboardAvoidingView>
      </Container>
    );
  }
}

ChatRoomScreen.propTypes = {
  onRefresh: PropTypes.func,
  qiscus: PropTypes.objectOf(PropTypes.any),
  // room: PropTypes.objectOf(PropTypes.any),
};

ChatRoomScreen.defaultProps = {
  onRefresh: () => {},
  qiscus: {},
  // room: {},
};

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(ChatRoomScreen),
  mapFromNavigationParam
);
