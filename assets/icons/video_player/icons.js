import { ActivityIndicator, StyleSheet, Image } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import React from 'react'
const ICON_COLOR = '#FFF'
const BOTTOM_BAR_ICON_SIZE = 30
const style = StyleSheet.create({
  iconStyle: {
    textAlign: 'center',
  },
  iconImage: {
    width: 26,
    height: 26,
    alignSelf: 'center'
  }
})
export const PlayIcon = () => (<Image style={style.iconImage} source={require('./play.png')} />)
export const PauseIcon = () => (<Image style={{ ...style.iconImage, width: 32, height: 32 }} source={require('./pause.png')} />)
export const ReplayIcon = () => (<Image style={{ ...style.iconImage, width: 32, height: 32 }} source={require('./replay.png')} />)
export const RewindIcon = () => (<Image style={style.iconImage} source={require('./rewind.png')} />)
export const ForwardIcon = () => (<Image style={style.iconImage} source={require('./forward.png')} />)
// export const PauseIcon = () => (<Ionicons name='md-pause' size={BOTTOM_BAR_ICON_SIZE} color={ICON_COLOR} style={style.iconStyle}/>)
export const Spinner = () => <ActivityIndicator color={ICON_COLOR} size='large'/>
// export const FullscreenEnterIcon = () => (<Ionicons name='ios-rewind' size={BOTTOM_BAR_ICON_SIZE} color={ICON_COLOR} style={style.iconStyle}/>)
export const FullscreenEnterIcon = () => (<MaterialIcons name='fullscreen' size={BOTTOM_BAR_ICON_SIZE} color={ICON_COLOR} style={style.iconStyle}/>)
export const FullscreenExitIcon = () => (<MaterialIcons name='fullscreen-exit' size={BOTTOM_BAR_ICON_SIZE} color={ICON_COLOR} style={style.iconStyle}/>)
// export const ReplayIcon = () => (<MaterialIcons name='replay' size={CENTER_ICON_SIZE} color={ICON_COLOR} style={style.iconStyle}/>)