import React from 'react'
import { View, Image, LogBox } from 'react-native'
import * as SplashScreen from 'expo-splash-screen'
import * as Icon from '@expo/vector-icons'
import * as Font from 'expo-font'
import { Asset } from 'expo-asset'
import { Provider } from 'react-redux'
import { enableScreens } from 'react-native-screens'
import { AppearanceProvider } from 'react-native-appearance'
import { enableMapSet } from 'immer'
import * as Notifications from 'expo-notifications'
import { getNotifMappingAction } from 'sf-components/notification/NotifAction'
import RootNavigator from './navigation/RootNavigator'
import styles from './constants/Styles'
import stores from './services/stores'
import { loadLocales } from './utils/lang'
import { networkChecker } from './utils/network'
import bugsnagClient from './utils/bugsnag'
import { Empty } from './components'
import { HP100, WP162 } from './constants/Sizes'
import { initDb } from './utils/dbUtils'
import { notificationMapper } from './utils/notification'
import { postLogPushNotification } from './actions/api'

enableScreens()
loadLocales()
networkChecker()
enableMapSet()

if (!__DEV__) {
  LogBox.ignoreAllLogs(['Animated: `useNativeDriver`'])
  // eslint-disable-next-line no-console
  console.log = () => {}
}

const ErrorBoundary = bugsnagClient.getPlugin('react')
export default class AppContainer extends React.Component {
  render() {
    return (
      <ErrorBoundary FallbackComponent={Empty}>
        <App {...this.props} />
      </ErrorBoundary>
    )
  }
}

class App extends React.Component {
  constructor(props) {
    super(props)
    SplashScreen.preventAutoHideAsync()
  }

  state = {
    areResourcesReady: false,
    isLoadingComplete: false,
  };

  componentDidMount() {
    this._cacheSplashResourcesAsync() // ask for resources
      .then(() => this.setState({ areResourcesReady: true })) // mark resources as loaded
      .catch(this._handleLoadingError)
    Notifications.addNotificationResponseReceivedListener(async (notification) => {
      const data = notificationMapper(
        notification.notification.request.content.data,
      )
      const notifAction = await getNotifMappingAction({
        url_mobile: data.action,
        url: data.url || data.action,
      })
      postLogPushNotification({
        id_user: data.id_user,
        id: data.id,
      })
      if (notifAction) {
       
        global.coldStartNotification = {
          to: notifAction.to,
          payload: notifAction.payload,
        }
      }
    })
  }

  _loadResourcesAsync = async () => {
    await SplashScreen.hideAsync()
    return Promise.all([
      initDb,
      Asset.loadAsync([require('./assets/images/icon.png')]),
      Asset.loadAsync([require('./assets/images/bottomNavbarCurved.png')]),
      Asset.loadAsync([
        require('./assets/icons/bottomNavbar/plusCircleGradient.png'),
      ]),
      Asset.loadAsync([require('./assets/icons/bottomNavbar/homeActive.png')]),
      Asset.loadAsync([require('./assets/icons/bottomNavbar/homeInactive.png')]),
      Asset.loadAsync([require('./assets/icons/bottomNavbar/timelineActive.png')]),
      Asset.loadAsync([require('./assets/icons/bottomNavbar/timelineInactive.png')]),
      Asset.loadAsync([
        require('./assets/icons/bottomNavbar/notificationActive.png'),
      ]),
      Asset.loadAsync([
        require('./assets/icons/bottomNavbar/notificationInactive.png'),
      ]),
      Asset.loadAsync([require('./assets/icons/bottomNavbar/profileActive.png')]),
      Asset.loadAsync([require('./assets/icons/bottomNavbar/profileInactive.png')]),
      Font.loadAsync({
        ...Icon.Ionicons.font,
        ...Icon.FontAwesome.font,
        OpenSansLight: require('./assets/fonts/OpenSans-Light.ttf'),
        OpenSansRegular: require('./assets/fonts/OpenSans-Regular.ttf'),
        OpenSansSemiBold: require('./assets/fonts/OpenSans-SemiBold.ttf'),
        OpenSansBold: require('./assets/fonts/OpenSans-Bold.ttf'),
        OpenSansExtraBold: require('./assets/fonts/OpenSans-ExtraBold.ttf'),
        SansProExtraLight: require('./assets/fonts/SourceSansPro-ExtraLight.ttf'),
        SansProLight: require('./assets/fonts/SourceSansPro-Light.ttf'),
        SansProRegular: require('./assets/fonts/SourceSansPro-Regular.ttf'),
        SansProSemiBold: require('./assets/fonts/SourceSansPro-SemiBold.ttf'),
        SansProBold: require('./assets/fonts/SourceSansPro-Bold.ttf'),
        SansProExtraBold: require('./assets/fonts/SourceSansPro-Black.ttf'),
        NeoSansExtraLight: require('./assets/fonts/NeoSans-ExtraLight.ttf'),
        NeoSansLight: require('./assets/fonts/NeoSans-Light.ttf'),
        NeoSansRegular: require('./assets/fonts/NeoSans-Regular.ttf'),
        NeoSansSemiBold: require('./assets/fonts/NeoSans-SemiBold.ttf'),
        NeoSansBold: require('./assets/fonts/NeoSans-Bold.ttf'),
        NeoSansExtraBold: require('./assets/fonts/NeoSans-ExtraBold.ttf'),
        NeoSansExtraBoldItalic: require('./assets/fonts/NeoSans-ExtraBoldItalic.ttf'),
        KotoriRoseRegular: require('./assets/fonts/KotoriRose-Regular.otf'),
        KotoriRoseBold: require('./assets/fonts/KotoriRose-Bold.otf'),
        CircularBook: require('./assets/fonts/CircularStd-Book.otf'),
        CircularMedium: require('./assets/fonts/CircularStd-Medium.otf'),
        CircularBold: require('./assets/fonts/CircularStd-Bold.otf'),
        CircularBlack: require('./assets/fonts/CircularStd-Black.otf'),
      }),
    ])
  };

  _handleLoadingError = (error) => {
    bugsnagClient.notify(error)
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true })
  };

  _cacheSplashResourcesAsync = async () => {
    const gif = require('./assets/loading.gif')
    return Asset.fromModule(gif).downloadAsync()
  };

  _cacheResourcesAsync = async () => {
    this._loadResourcesAsync()
      .then(this._handleFinishLoading)
      .catch(this._handleLoadingError)
  };

  render() {
    if (!this.state.areResourcesReady) {
      return null
    }
 
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      this._cacheResourcesAsync();
      return null
    } else {
      return (
        <View style={styles.container}>
          <Provider store={stores}>
            <AppearanceProvider>
              <RootNavigator />
            </AppearanceProvider>
          </Provider>
        </View>
      )
    }
}
}
