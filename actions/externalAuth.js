import * as Google from 'expo-google-app-auth'
import * as AppAuth from 'expo-app-auth'
import * as Facebook from 'expo-facebook'
import * as AppleAuthentication from 'expo-apple-authentication'
import env from '../utils/env'
import bugsnagClient from '../utils/bugsnag'

export const FacebookAuth = async () => {
  await Facebook.initializeAsync({
    appId: env.facebookAuthAppId,
    appName: 'Soundfren'
  })
  const { type, token } = await Facebook.logInWithReadPermissionsAsync({
    permissions: env.facebookAuthPermissions,
    behavior: 'web'
  })
  if (type === 'success') {
    const graphFacebookResponse = await fetch(`https://graph.facebook.com/me?fields=id,name,picture,email,birthday&access_token&access_token=${token}`)
    return await graphFacebookResponse.json()
  }
  return null
}

export const GoogleAuth = async () => {
  const response = await Google.logInAsync({
    clientId: env.googleAuthClientId,
    iosStandaloneAppClientId: env.googleAuthIosStandaloneAppClientId,
    androidStandaloneAppClientId: env.googleAuthAndroidStandaloneAppClientId,
    redirectUrl: `${AppAuth.OAuthRedirect}:/oauth2redirect/google`,
    scopes: env.googleAuthScopes
  })
  const { type, user } = response
  if (type === 'success') {
    return user
  }
  return null
}

export const AppleAuth = async () => {
  try {
    const credential = await AppleAuthentication.signInAsync({
      requestedScopes: [
        AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
        AppleAuthentication.AppleAuthenticationScope.EMAIL,
      ],
    })
    // signed in
    return credential
  } catch (e) {
    if (e.code === 'ERR_CANCELED') {
      return null
    } else {
      bugsnagClient.notify(e)
    }
  }
}
