import { clientHttp } from "../utils/clientHttp";

export const postExpoToken = (body) =>
  clientHttp.post("/v1/notification/expo", body);
export const getJob = (params) => clientHttp.get("/v1/job", { params });
export const getJobGroup = (params) =>
  clientHttp.get("/v1/job/group", {
    params: { created_by: 1, ...params },
  });
export const getRole = (params) =>
  clientHttp.get("/v1/settings/detail", { params });

export const getRoleStartUp = (params) =>
  clientHttp.get("/v1/settings/detail", {
    params: {
      // ...params,
      show: "array",
      key_name: "role",
      setting_name: "1000startup",
    },
  });
export const getJobStartUp = (params) =>
  clientHttp.get("/v1/job/startup", { params });

export const getCompany = (params) => clientHttp.get("/v1/company", { params });
export const getCountry = (params) =>
  clientHttp.get("/v1/location/country", { params });
export const getCity = (params) =>
  clientHttp.get("/v1/location/city", { params });

export const getCityV3 = (params) =>
  clientHttp.get("/v3/location/city", { params });
export const getProvince = (params) =>
  clientHttp.get("/v3/location/province", { params });

export const getGenreInterest = (params) =>
  clientHttp.get("/v1/interest", { params });
export const getUsers = (params) =>
  clientHttp.get("/v1/search/user", { params });
export const getProfileDetail = (params) =>
  clientHttp.get("/v1/users", { params });
export const getProfileProgress = (params) =>
  clientHttp.get("/v1/users/profile_progress", { params });
export const getProfileFollowers = (params) =>
  clientHttp.get("/v1/connection/followers", { params });
export const getProfileFollowing = (params) =>
  clientHttp.get("/v1/connection/following", { params });
export const postProfileUnfollow = (body) =>
  clientHttp.post("/v1/connection/unfollow_action", body);
export const postProfileFollow = (body) =>
  clientHttp.post("/v1/connection/follow_action", body);
export const getProfileFollowStatus = (params) =>
  clientHttp.get("/v1/connection/status_followed", { params });
export const postProfileUpdate = (body) =>
  clientHttp.post("/v1/users/change_headline", body);
export const postProfileUpdateAbout = (body) =>
  clientHttp.post("/v1/users/update_user", body, {
    params: { update_type: "about_me" },
  });
export const postProfileUpdatePassword = (body) =>
  clientHttp.post("/v1/users/change_password", body);
export const postProfileUpdateBasic = (body) =>
  clientHttp.post("/v1/users/update_user", body, {
    params: { update_type: "basic_info" },
  });
export const getJourneyDetail = (params) =>
  clientHttp.get("/v1/journey", { params });
export const getProfileArts = (params) =>
  clientHttp.get("/v1/journey", {
    params: { ...params, type: "visual_art" },
  });
export const getProfileSongVideo = (params) =>
  clientHttp.get("/v1/journey/song_video", { params });
export const postProfileAddArt = (body) =>
  clientHttp.post("/v1/journey/create_journey/visual_art", body);
export const putProfileEditArt = (body) =>
  clientHttp.post("/v1/journey/edit_journey/visual_art", body);
export const getProfileDelArt = (idArt) =>
  clientHttp.get(`/v1/journey/remove_photo/${idArt}`);
export const getProfileDetailArt = (params) =>
  clientHttp.get("/v1/journey", { params });
export const getProfileVideos = (params) =>
  clientHttp.get("/v1/journey", {
    params: { ...params, type: "video" },
  });
export const postProfileAddVideo = (body) =>
  clientHttp.post("/v1/journey/create_journey/video", body);
export const putProfileEditVideo = (body) =>
  clientHttp.post("/v1/journey/edit_journey/video", body);
export const postProfileDelVideo = (body) =>
  clientHttp.post("/v1/journey/remove_journey/", body);

export const postProfileAddPortofolio = (body) =>
  clientHttp.post("/v1/users/add_portofolio", body);
export const putProfileEditPortofolio = (body) =>
  clientHttp.post("/v1/users/edit_portofolio", body);
export const postProfileDelPortofolio = (body) =>
  clientHttp.post("/v1/users/remove_portofolio/", body);

export const postProfileDelJourney = (body) =>
  clientHttp.post("/v1/journey/remove_journey/", body);
export const getProfileSongs = (params) =>
  clientHttp.get("/v1/journey", {
    params: { ...params, type: "song" },
  });
export const postSongLink = (body) =>
  clientHttp.post("/v1/journey/create_journey/song", body);
export const postSongFile = (body) =>
  clientHttp.post("/v1/journey/create_journey/song", body, {
    params: { type: "upload" },
  });
export const putSongLink = (body) =>
  clientHttp.post("v1/journey/edit_journey/song", body);
export const putSongFile = (body) =>
  clientHttp.post("v1/journey/edit_journey/song", body, {
    params: { type: "upload" },
  });
export const getProfileDelSong = (body) =>
  clientHttp.post("/v1/journey/remove_journey/", body);
export const getFriendSuggestion = (params) =>
  clientHttp.get("/v1/search/suggestion", { params });
export const getFeeds = (params) => clientHttp.get("/v1/timeline", { params });
export const getFeedsByTopic = (params) =>
  clientHttp.get("/v1/timeline/topic", { params });
export const getFeedDetail = (params) =>
  clientHttp.get("/v1/timeline/detail", { params });
export const getFeedLikes = (params) =>
  clientHttp.get("/v1/timeline/get_liked", { params });
export const getFeedComments = (params) =>
  clientHttp.get("/v1/timeline/detail_comment", { params });
export const getListTopics = (params) =>
  clientHttp.get("/v1/timeline/list_topic", { params });
export const postFeedComment = (body) =>
  clientHttp.post("/v1/timeline/create_comment", body);
export const postFeedCommentLike = (body) =>
  clientHttp.post("/v1/timeline/send_like", body);
export const deleteFeedComment = (body) =>
  clientHttp.post("/v1/timeline/remove_comment", body);
export const postFeedLike = (body) =>
  clientHttp.post("/v1/timeline/send_like", body);
export const getTimelineAds = (params) =>
  clientHttp.get("/v1/ads/artist_showcase", { params });
export const getAds = (params) => clientHttp.get("/v1/ads", { params });
export const submitSubmission = (body) =>
  clientHttp.post("/v1/ads/submit_submission", body);
export const postStartupSubmission = (body) =>
  clientHttp.post("/v3/profile/submission_1000startup", body);

export const getStartupSubmission = (params) =>
  clientHttp.get("/v3/profile/submission_1000startup", { params });

export const getStartupCategory = (params) =>
  clientHttp.get(
    "/v1/settings/detail?setting_name=1000startup&key_name=innovation_category&show=array",
    {}
  );

export const getAdsDetail = (params) =>
  clientHttp.get("/v1/ads/detail", { params });
export const postJourney = (body, config = {}) =>
  clientHttp.post("/v1/journey/create_journey/post", body, config);
export const postMusicJourney = (body) =>
  clientHttp.post("v1/journey/create_journey/music_journey", body);
export const postPerformanceJourney = (body) =>
  clientHttp.post("v1/journey/create_journey/performance_journey", body);
export const postUpdatePerformanceJourney = (body) =>
  clientHttp.post("v1/journey/edit_journey/performance_journey", body);
export const deleteJourney = (idJourney) =>
  clientHttp.post("/v1/journey/remove_journey", {
    id_journey: idJourney,
  });
export const postLogin = (body) =>
  clientHttp.post("/v1/authentication/login", body);
export const postLogin_V2 = (body) =>
  clientHttp.post("/v2/authentication/login", body);
export const postLoginFacebook = (body) =>
  clientHttp.post("/v2/registration/social_media/facebook", body);
export const postLoginGoogle = (body) =>
  clientHttp.post("/v2/registration/social_media/google", body);
export const postForgotPassword = (body) =>
  clientHttp.post("/v1/authentication/forgot_password", body);
export const postInputForgotPassword = (body) =>
  clientHttp.post("/v1/authentication/check_forgot_password_code", body);
export const postForgotNewPassword = (body) =>
  clientHttp.post("/v1/authentication/change_password", body);
export const postRegister = (body) =>
  clientHttp.post("/v1/authentication/register", body);
export const postRegisterMusicIndustry = (body) =>
  clientHttp.post("/v1/authentication/create_headline_experience", body);
export const postRegisterLocation = (body) =>
  clientHttp.post("/v1/authentication/update_location", body);
export const postRegisterVerifyEmail = (body) =>
  clientHttp.post("/v1/authentication/verify_email", body);
export const postRegisterPhoto = (body) =>
  clientHttp.post("/v1/authentication/upload_photo", body);
export const postInterest = (body) =>
  clientHttp.post("/v1/interest/create_interest", body);
export const getNotification = (params) =>
  clientHttp.get("/v1/notification", { params });
export const postReadNotification = (body) =>
  clientHttp.post("/v1/notification/update_read_notif", body);
export const postTriggerPusher = (body) =>
  clientHttp.post("/v1/pusher/trigger", body);
export const postMessageNotification = (body) =>
  clientHttp.post("/v1/message/send_notif", body);
export const postLogPushNotification = (body) =>
  clientHttp.post("/v1/notification/pushClicked", body);
export const postBooking = (body) => clientHttp.post("/v1/booking/send", body);
export const getMentionSuggestions = (params) =>
  clientHttp.get("/v1/users/suggestion", { params });
export const getUserFeed = (params) =>
  clientHttp.get("/v1/users/timeline", { params });
export const postPromoteBand = (body) =>
  clientHttp.post("/v1/ads/promote_band", body);
export const postCollabPost = (body) =>
  clientHttp.post("/v1/ads/apply_collaboration", body);
export const putCollabPost = (body) =>
  clientHttp.post("/v1/ads/edit_collaboration", body);
export const postLogCollab = (body) =>
  clientHttp.post("/v1/Log/collaboration", body);
export const postLogPremium = (body) =>
  clientHttp.post("/v1/log/premium_screen", body);
export const getSubscriptionMonth = (params) =>
  clientHttp.get("/v1/settings/detail", { params });
export const getSubscriptionVoucher = (params) =>
  clientHttp.get("/v1/voucher/detail", { params });
export const getVoucherDetail = (params) =>
  clientHttp.get("/v1/voucher", { params });
export const getSubscriptionDetail = (params) =>
  clientHttp.get("/v1/users/premium_user_info", { params });
export const getSettingDetail = (params) =>
  clientHttp.get("/v1/settings/detail", { params });
export const getTopGenre = (params) =>
  clientHttp.get("/v2/genre/top_genre", { params });
export const getFindFriends = (params) =>
  clientHttp.get("/v2/artist/find_friend", { params });
export const postRegister_v2 = (body) =>
  clientHttp.post("/v2/registration/submit", body);
export const postRegisterStep1_v2 = (body) =>
  clientHttp.post("/v2/registration/submit?step=1", body);
export const postRegisterStep2_v2 = (body) =>
  clientHttp.post("/v2/registration/submit?step=2", body);
export const postRegisterStep2Skip_v2 = (body) =>
  clientHttp.post("/v2/registration/submit?step=2&action=skip", body);
export const postRegisterStep3_v2 = (body) =>
  clientHttp.post("/v2/registration/submit?step=3", body);
export const postVerificationLink = (body) =>
  clientHttp.post("/v2/registration/send_verification_email", body);
export const postFeedback = (body) =>
  clientHttp.post("/v1/settings/send_feedback", body);
export const postFeedbackReason = (body) =>
  clientHttp.post("/v1/settings/send_reason_feedback", body);
export const getExperiences = (params) =>
  clientHttp.get("/v1/journey/experience", { params });
export const postRemoveExperiences = (body) =>
  clientHttp.post("v1/journey/remove_journey", body);
export const postEditMusicExperiences = (body) =>
  clientHttp.post("v1/journey/edit_journey/music_journey", body);
export const postEditPerformanceExperiences = (body) =>
  clientHttp.post("v1/journey/edit_journey/performance_journey", body);
export const postAdsSubmission = (body) =>
  clientHttp.post("v1/ads/create_ads", body, {
    params: { category: "submission" },
  });
export const getMessageSuggestions = (params) =>
  clientHttp.get("v1/message/suggestion", { params });
export const postLogShareProfile = (body) =>
  clientHttp.post("/v1/Log/activity", body, {
    params: { type: "share_profile" },
  });
export const postLogMessaging = (body) =>
  clientHttp.post("v1/log/activity", body, {
    params: { type: "messaging" },
  });
export const postLogSoundfrenPlaySeeAllButton = (body) =>
  clientHttp.post("v1/log/activity", body, {
    params: { type: "soundfren_play_page" },
  });
export const postMusicFile = (body, config) =>
  clientHttp.post("v1/music/upload", body, config);
export const getTrending = (params) =>
  clientHttp.get("v1/timeline/trending", { params });
export const getAccountTypeById = (params) =>
  clientHttp.get("v1/users/account_type", { params });
export const getDetailReferralCode = (params) =>
  clientHttp.get("v1/referral/detail", { params });
export const getCreatedProject = (params) =>
  clientHttp.get("v1/project/created", { params });
export const getJoinedProject = (params) =>
  clientHttp.get("v1/project/joined", { params });
export const getCollaborationDetailProject = (params) =>
  clientHttp.get("v1/project/collaboration", { params });
export const getMyCollaboration = (params) =>
  clientHttp.get("/v3/ads/my_project", { params });
export const getListCollaborator = (params) =>
  clientHttp.get("/v3/ads/list_collaborator", { params });
export const getListCollaboration = (params) =>
  clientHttp.get("/v3/ads/list_collaboration", { params });
export const postJoinCollaboration = (body) =>
  clientHttp.post("/v3/ads/join_collaboration", body);
export const postEditMemberStatus = (body) =>
  clientHttp.post("/v3/ads/member_status", body);
export const postJoinSession = (body) =>
  clientHttp.post("v2/ads/join_session", body);
export const getCurrentSDKVersion = (params) =>
  clientHttp.get("/v1/settings/detail", {
    params: { setting_name: "sdk", key_name: "min" },
  });
export const getProfileDataShare = (params) =>
  clientHttp.get("/v1/share/dataShare", {
    params: { type: "profile", ...params },
  });
export const getExploreDataShare = (params) =>
  clientHttp.get("/v1/share/dataShare", {
    params: { type: "explore", ...params },
  });
export const postShareTimeline = (body) =>
  clientHttp.post("v1/share/timeline", body);
export const postLogSoundfenPlayEpisode = (body) =>
  clientHttp.post("v1/log/soundfren_play", body, {
    params: { type: "messaging" },
  });
export const getAdvancedSearch = (params, cancelToken) =>
  clientHttp.get("/v2/search", { params, cancelToken });

export const getVideoSearch = (params, cancelToken) =>
  clientHttp.get("/v2/ads/video", { params, cancelToken });
export const getPodcastSearch = (params, cancelToken) =>
  clientHttp.get("/v2/ads/podcast", { params, cancelToken });
export const getLearnSearch = (params, cancelToken) =>
  clientHttp.get("/v2/ads/learning_list", { params, cancelToken });

export const getPeople = (params, cancelToken) =>
  clientHttp.get("/v2/search", {
    ...params,
    type: "people",
    show: "people",
    cancelToken,
  });
export const getStartupUser = (params) =>
  clientHttp.get("v3/registration/startup", { params });
export const postRegisterV3 = (body) =>
  clientHttp.post("v3/registration/submit", body);
export const postRegisterV3SendVerificationEmail = (body) =>
  clientHttp.post("v3/registration/send_verification_email", body);
export const postRegisterV3SetupProfile = (body) =>
  clientHttp.post("v3/registration/setup_profile", body);
export const postRegisterV3SelectJobGroup = (body) =>
  clientHttp.post("v3/registration/select_job_group", body);
export const postRegisterV3CreateCompanyJob = (body) =>
  clientHttp.post("v3/registration/create_company_job", body);
export const postRegisterV3SelectGenre = (body) =>
  clientHttp.post("v3/registration/select_genre", body);
export const postRegisterV3Google = (body) =>
  clientHttp.post("v3/registration/social_media/google", body);
export const postRegisterV3Facebook = (body) =>
  clientHttp.post("v3/registration/social_media/facebook", body);
export const postRegisterV3Apple = (body) =>
  clientHttp.post("v3/registration/social_media/apple", body);
export const postRegisterV3Skip = (body) =>
  clientHttp.post("v3/registration/skip_registration", body);
export const postCheckEmail = (body) =>
  clientHttp.post("v1/authentication/check_email", body);
export const postCheckRecoveryCode = (body) =>
  clientHttp.post("v1/authentication/check_forgot_password_code", body);
export const postFeedbackAlbum = (body) =>
  clientHttp.post("v1/feedback/send_feedback", body);

// GENERAL TAB
export const getGeneralTabData = (params) =>
  clientHttp.get(
    "/v1/settings/detail?setting_name=1000startup&key_name=general&show=array",
    { params }
  );
export const getFaqData = (params) =>
  clientHttp.get(
    "/v1/settings/detail?setting_name=1000startup&key_name=faq&show=array",
    { params }
  );
export const getTimelineData = (params) =>
  clientHttp.get(
    "/v1/settings/detail?setting_name=1000startup&key_name=timeline&show=array",
    { params }
  );
export const getTermsData = (params) =>
  clientHttp.get(
    "/v1/settings/detail?setting_name=1000startup&key_name=term_condition&show=array",
    { params }
  );
export const getRewardData = (params) =>
  clientHttp.get(
    "/v1/settings/detail?setting_name=1000startup&key_name=rewards&show=array",
    { params }
  );

// NOTIFICATION
export const getVisitedProfile = (params) =>
  clientHttp.get("v1/notification/visited_profile", { params });
export const getNotificationMapping = () =>
  clientHttp.get("v1/settings/detail", {
    params: {
      setting_name: "notification",
      key_name: "mapping_screen",
      show: "value",
    },
  });

// CHAT
export const postCreateGroupChat = (body) =>
  clientHttp.post("v1/message/create_group", body);
export const postCreateChatGroup = (body) =>
  clientHttp.post("/v1/chat/create_group_message", body);
export const postEditGroupChat = (body) =>
  clientHttp.post("v1/message/edit_group", body);
export const postAddMemberGroup = (body) =>
  clientHttp.post("v1/message/add_member_group", body);
export const postRemoveMemberGroup = (body) =>
  clientHttp.post("v1/message/remove_member_group", body);
export const postLeaveGroup = (body) =>
  clientHttp.post("v1/message/leave_group", body);
export const postSendNotifGroup = (body) =>
  clientHttp.post("v1/message/send_group_notif", body);
export const getGroupDetail = (params) =>
  clientHttp.get("v1/message/group_info", { params });
export const getBannerPopup = (params) =>
  clientHttp.get("v1/settings/detail?setting_name=banner&key_name=pop_up", {
    params,
  });
export const postUploadImage = (body) =>
  clientHttp.post("v1/message/upload_image", body);
export const postSetRoleGroup = (body) =>
  clientHttp.post("v1/message/setStatusUser", body);
export const getSuggestedContact = (params) =>
  clientHttp.get("/v1/chat/suggested_contact", { params });
export const getListMessage = (params) =>
  clientHttp.get("/v1/chat/list_message", { params });
export const getContact = (params) =>
  clientHttp.get("/v1/chat/list_contact", { params });
export const getDetailMessage = (params) =>
  clientHttp.get("/v1/chat/detail_message", { params });
export const postSendMessage = (body) =>
  clientHttp.post("/v1/chat/send_message", body);
export const postAddMember = (body) =>
  clientHttp.post("/v1/chat/add_member", body);
export const postRemoveMember = (body) =>
  clientHttp.post("/v1/chat/remove_member", body);
export const putEditMember = (body) =>
  clientHttp.put("/v1/chat/edit_member", body);
export const deleteChat = (body) =>
  clientHttp.post("/v1/chat/delete_chat", body);
export const putEditGroupchat = (body) =>
  clientHttp.put("/v1/chat/edit_groupchat", body);

// VOUCHER
export const getVoucherUser = (params) =>
  clientHttp.get("v1/voucher/user", { params });
export const getSingleVoucherDetail = (params) =>
  clientHttp.get("v1/voucher/detail", { params });

// ADS
export const getAllAds = (params) => clientHttp.get("v2/ads", { params });
export const postAdsFeedback = (body) =>
  clientHttp.post("/v1/Log/activity", body, {
    params: { type: "news_feedback" },
  });
export const getAdsSFLearn = (params) =>
  clientHttp.get("v2/ads/soundfren_learn", { params });
export const postLogMenuVideo = (body) =>
  clientHttp.post("/v1/Log/activity", body, {
    params: { type: "view_menu" },
  });
export const postAdsComment = (body) =>
  clientHttp.post("/v2/ads/create_comment", body);
export const postDeleteAdsComment = (body) =>
  clientHttp.post("/v2/ads/remove_comment", body);
export const postLikeAdsComment = (body) =>
  clientHttp.post("/v2/ads/send_like", body);
export const getAdsCollab = (params) =>
  clientHttp.get("v2/ads/collaboration", { params });
export const getDetailCollab = (params) =>
  clientHttp.get("/v3/ads/detail", { params });
export const getDetailProject = (params) =>
  clientHttp.get("/v3/ads/detail_project", { params });
export const getListCollaborationLv1 = (params) =>
  clientHttp.get("/v3/ads/list_collaboration_lv1", { params });
export const getSearchSuggestion = (params) =>
  clientHttp.get("v1/settings/detail", { params });
export const getAdsSubmission = (params) =>
  clientHttp.get("v2/ads/event_audition", { params });
export const getAdsSubmissionLocation = () =>
  clientHttp.get("v2/ads/submission_location");
export const getNews = (params) => clientHttp.get("v2/ads/news", { params });
export const postDeleteAds = (body) =>
  clientHttp.post("/v2/ads/remove_ads", body);

// GAMIFICATION (Reward)
export const getReward = (params) =>
  clientHttp.get("/v1/gamification/reward", { params });
export const postClaim = (body) =>
  clientHttp.post("/v1/gamification/claim", body);
export const postCheckClaim = (body) =>
  clientHttp.post("/v1/gamification/check_claim", body);
export const postLogMenuReward = (body) =>
  clientHttp.post("/v1/Log/activity", body, {
    params: { type: "reward_page" },
  });

export const getProfileDetailV2 = (params) =>
  clientHttp.get("/v2/profile", { params });
export const getJourneyV2 = (params) =>
  clientHttp.get("/v2/profile/journey", { params });
export const postClaimFreeTrial = (body) =>
  clientHttp.post("/v2/profile/set_trial", body);
export const postUploadCoverImage = (body) =>
  clientHttp.post("/v2/profile/change_cover_image", body);
export const getGenreSuggestion = (params) =>
  clientHttp.get("v2/genre/suggestion", { params });
export const postEditProfile = (body) =>
  clientHttp.post("v2/profile/edit", body);
export const getArtistSpotlight = (params) =>
  clientHttp.get("v2/artist", { params });
export const postLogArtistSpotlight = (body) =>
  clientHttp.post("v1/log/activity", body, {
    params: { type: "search_artist" },
  });
export const postLogViewJourney = (body) =>
  clientHttp.post("v1/log/activity", body, {
    params: { type: "view_journey" },
  });
export const postLogRequestSession = (body) =>
  clientHttp.post("v1/log/activity", body, {
    params: { type: "session_request" },
  });
export const getLogActivity = (params) =>
  clientHttp.get("v1/log/log_activity", { params });
export const postDeleteLogActivity = (body) =>
  clientHttp.post("v1/log/remove_activity", body);
export const postLogCreateCollab = (body) =>
  clientHttp.post("v1/log/activity", body, {
    params: { type: "view_create_collab" },
  });
export const postLogActivity = (body) =>
  clientHttp.post("v1/log/activity", body, {
    params: { type: "view_menu" },
  });

//SCHEDULE
export const postSchedule = (body) =>
  clientHttp.post("/v1/schedule/create_schedule", body);
export const putSchedule = (body) =>
  clientHttp.post("/v1/schedule/edit_schedule", body);
export const deleteSchedule = (body) =>
  clientHttp.post("/v1/schedule/remove_schedule", body);
export const getSchedule = (params) =>
  clientHttp.get("v1/schedule", { params });

//SESSION
export const postCreateSession = (body) =>
  clientHttp.post("/v1/ads/create_session", body);

// LUCKY FREN
export const getLuckyFren = (params) =>
  clientHttp.get("/v2/gamification", { params });
export const postCheckInToday = (body) =>
  clientHttp.post("/v2/gamification/checkin", body);
export const getLuckyFrenLeaderBoard = (params) =>
  clientHttp.get("/v2/gamification/leaderboard", { params });
export const getLuckyFrenCategory = (params) =>
  clientHttp.get("/v2/gamification/mission_category", { params });
export const getLuckyFrenMission = (params) =>
  clientHttp.get("/v2/gamification/mission", { params });
export const getGiftBox = (params) =>
  clientHttp.get("/v2/gamification/gift_box", { params });
export const postOpenLuckyBox = (body) =>
  clientHttp.post("/v2/gamification/open_luckyBox", body);
export const getRewardDetail = (params) =>
  clientHttp.get("/v2/gamification/reward", { params });
export const postClaimReward = (body) =>
  clientHttp.post("/v2/gamification/claim_reward", body);

// MEMBERSHIP PLAN
export const getMembershipScreen = (params) =>
  clientHttp.get("/v1/users/premium_membership", { params });
export const getMembershipDetail = (params) =>
  clientHttp.get("/v1/users/premium_membership_detail", { params });
export const postMembershipPlan = (body) =>
  clientHttp.post("/v1/payment/premium_user", body);
//PAYMENT
export const getPaymentList = (params) =>
  clientHttp.get("v1/payment/list", { params });
export const getStartupVideo = (params) =>
  clientHttp.get("v2/ads/video", { params });

export const getStartupTabMission = (params) =>
  clientHttp.get("v2/gamification/startup", { params });
// export const getStartupWebinar = (params) => clientHttp.get('v2/ads/sc_session', { params })

// export const getStartupWebinar = (params) => clientHttp.get('v2/ads/sc_session', { params })

//1000 STARTUP
export const postEditProfile1000Startup = (body) =>
  clientHttp.post("v3/profile/user_profile_1000startup", body);
export const getProfileDetailV3 = (params) =>
  clientHttp.get("/v3/profile/user_profile_1000startup", { params });
export const getProfileProgressV2 = (params) =>
  clientHttp.get("/v2/profile/progress", { params });
export const getFaq1000Startup = () =>
  clientHttp.get(
    "/v1/settings/detail?setting_name=1000startup&key_name=general&show=array"
  );
export const getNews1000Startup = (params) =>
  clientHttp.get("v2/ads/news?platform=1000startup", { params });
export const getStartupTabLearn = () =>
  clientHttp.get(
    "/v1/settings/detail?setting_name=1000startup&key_name=learn&show=array"
  );
export const postCheckInTodayStartup = (body) =>
  clientHttp.post("/v2/gamification/checkin_startup", body);
export const getStartupMissionHome = (params) =>
  clientHttp.get("/v2/gamification", { params });
export const postSubmitReferralStartup = (body) =>
  clientHttp.post("/v1/referral/submit_referral", body);
export const getStartupPodcastCategory = (params) =>
  clientHttp.get("/v2/ads/podcast_category", {
    params: { platform: "1000startup" },
  });
  export const getPodcastCategory = (params) =>
  clientHttp.get("/v2/ads/podcast_category", { params });
export const getStartupPodcastByModule = (params) =>
  clientHttp.get("/v2/ads/podcast", { params });
export const getListEpisodePodcast = (params) =>
  clientHttp.get("/v2/ads/podcast", { params });
export const postVideoStatus = (body) =>
  clientHttp.post("/v2/ads/status_video", body);
export const getVideoCategory = () =>
  clientHttp.get(
    "/v1/settings/detail?setting_name=1000startup&key_name=video_category"
  );
export const getLearningList = (params) =>
  clientHttp.get(`/v2/ads/learning_list?id_user=${params.id_user}`);
// export const getVideoList = (body) => clientHttp.get('/v2/ads/video', body)
export const getVideoList = (body) =>
  clientHttp.get(
    `/v2/ads/video?platform=1000startup&content_category=${body.content_category}&content_sub_category=${body.content_sub_category}&id_user=${body.id_user}`
  );
export const postEpisodePodcastStatus = (body) =>
  clientHttp.post("/v1/log/soundfren_play", body);
export const postScreenTimeAds = (body) =>
  clientHttp.post("/v2/ads/view_duration", body);
export const getAnnouncement = (params) =>
  clientHttp.get("/v2/ads/announcement_info", { params });

// Feature Like
export const postLike = (body) => clientHttp.post("/v2/ads/send_like", body);

export const getStartupGeneral = (params) =>
  clientHttp.get("/v2/startup/general", { params });

//feature team up

export const getTeamUpInfo = (params) =>
  clientHttp.get("/v2/startup/teamup", { params });
export const getTeamUpSearch = (params) =>
  clientHttp.get("v2/startup/search", { params });
export const postTeamUpInvite = (body) =>
  clientHttp.post("/v2/startup/invite_user", body);
export const getTeamUpInvitation = (params) =>
  clientHttp.get("/v2/startup/teamup_invitation_list", { params });
export const postTeamUpStatus = (body) =>
  clientHttp.post("/v2/startup/teamup_invitation_status", body);
export const postLockRole = (body) =>
  clientHttp.post("/v2/startup/locked_role", body);
export const getDiscussion = (params) =>
  clientHttp.get(
    `/v2/startup/teamup_chat?id_startup_team=${params.id_startup_team}`
  );
export const postDiscussion = (body) =>
  clientHttp.post("/v2/startup/teamup_chat", body);
export const postStartUpComment = (body) =>
  clientHttp.post("/v2/ads/create_comment", body);
export const deleteStartUpComment = (body) =>
  clientHttp.post("/v2/ads/remove_comment", body);

// feature event selection lv.0
export const getEventList = (params) => clientHttp.get("/v1/event", { params });
export const getEventParticipantList = (params) =>
  clientHttp.get("/v1/event/participant", { params });
export const postEventParticipant = (body) =>
  clientHttp.post("/v1/event/join", body);
