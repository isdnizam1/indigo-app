import {
  SET_REG_CTA_ENABLED,
  SET_REG_STEP,
  SET_REG_DATA,
  RESET_REG_DATA,
  SET_REG_PASS_VISIBILITY,
  SET_REG_FOCUS,
  SET_REG_PERFORM_HTTP,
  SET_REGISTRATION
} from './actionTypes'

export const setCtaEnabled = (response) => ({
  type: SET_REG_CTA_ENABLED,
  response
})

export const setRegistration = (response) => ({
  type: SET_REGISTRATION,
  response
})

export const setFocus = (response) => ({
  type: SET_REG_FOCUS,
  response
})

export const setStep = (response) => ({
  type: SET_REG_STEP,
  response
})

export const setPerformHttp = (response) => ({
  type: SET_REG_PERFORM_HTTP,
  response
})

export const setPasswordVisibility = (response) => ({
  type: SET_REG_PASS_VISIBILITY,
  response
})

export const setData = (response) => ({
  type: SET_REG_DATA,
  response
})

export const resetData = () => ({
  type: RESET_REG_DATA
})

export default {
  setCtaEnabled,
  setStep,
  setData,
  resetData,
  setPasswordVisibility,
  setFocus,
  setPerformHttp
}
