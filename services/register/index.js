import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const registerReducer = reducer
export const registerDispatcher = actionDispatcher
export const registerTypes = actionTypes
