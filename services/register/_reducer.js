import { Animated } from 'react-native'
import {
  SET_REG_STEP,
  SET_REG_DATA,
  SET_REG_FOCUS,
  RESET_REG_DATA,
  SET_REG_CTA_ENABLED,
  SET_REG_PASS_VISIBILITY,
  SET_REG_PERFORM_HTTP,
  SET_REGISTRATION
} from './actionTypes'

const initialState = {
  behaviour: {
    focused: false,
    step: 0,
    totalStep: 7,
    showPassword: false,
    performHttp: false,
    ctaEnabled: false,
    progress: new Animated.Value(0),
    httpProgress: new Animated.Value(0),
    httpVisibility: new Animated.Value(0)
  },
  data: {
    id_user: null,
    email: null,
    password: null,
    name: null,
    id_city: null,
    profile_type: null, // enum('personal', group)
    profile_picture: null,
    referral_code: null,
    job_group_name: null,
    job_title: null,
    company_name: null,
    interest_name: [],
    registered_via: '',
    gender: null
  }
}

export default registerReducer = (currentState = initialState, { type, response }) => {
  switch (type) {
  case SET_REG_CTA_ENABLED:
  case SET_REG_STEP:
  case SET_REG_PASS_VISIBILITY:
  case SET_REG_PERFORM_HTTP:
  case SET_REG_FOCUS:
    // eslint-disable-next-line no-case-declarations
    const field = {
      [SET_REG_CTA_ENABLED]: 'ctaEnabled',
      [SET_REG_STEP]: 'step',
      [SET_REG_PERFORM_HTTP]: 'performHttp',
      [SET_REG_PASS_VISIBILITY]: 'showPassword',
      [SET_REG_FOCUS]: 'focused',
    }[type]
    return {
      ...currentState,
      behaviour: {
        ...currentState.behaviour,
        [field]: response
      }
    }
  case SET_REG_DATA:
    return {
      ...currentState,
      data: {
        ...currentState.data,
        ...response
      }
    }
  case SET_REGISTRATION:
    return {
      ...response,
      behaviour: {
        ...response.behaviour,
        progress: new Animated.Value(0),
        httpProgress: new Animated.Value(0),
        httpVisibility: new Animated.Value(0)
      }
    }
  case RESET_REG_DATA:
    return { ...initialState }
  default:
    return currentState
  }
}
