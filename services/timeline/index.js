import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const timelineReducer = reducer
export const timelineDispatcher = actionDispatcher
export const timelineTypes = actionTypes
