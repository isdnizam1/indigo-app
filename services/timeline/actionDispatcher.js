import { NEED_RELOAD_FEEDS, SET_TOPIC_ID } from './actionTypes'

export const shouldReloadFeed = (response) => {
  return ({
    type: NEED_RELOAD_FEEDS,
    response
  })
}

export const setTopicId = (response) => {
  return ({
    type: SET_TOPIC_ID,
    response
  })
}

export default {
  shouldReloadFeed,
  setTopicId
}
