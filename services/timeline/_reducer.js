import { NEED_RELOAD_FEEDS } from './actionTypes'
import { SET_TIMELINE_SCROLL_DIRECTION, SET_TOPIC_ID } from './actionTypes'

const initialState = {
  isNeedReload: false,
  scrollDirection: null,
  topicId: 10
}

export default timelineReducer = (currentState = initialState, { type, response }) => {
  switch (type) {
  case NEED_RELOAD_FEEDS:
    return {
      ...currentState,
      isNeedReload: response
    }
  case SET_TOPIC_ID:
    return {
      ...currentState,
      topicId: response
    }
  case SET_TIMELINE_SCROLL_DIRECTION:
    return {
      ...currentState,
      scrollDirection: response
    }
  default:
    return {
      ...currentState
    }
  }
}
