import { PLAY, STOP, CARD_SONG_ID, SET_SP_PLAYER_ACTION } from './actionTypes'

const initialState = {
  isPlaying: false,
  cardSongId: null,
  spPlayerAction: null
}

export default playerReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case PLAY:
    return {
      ...currentState,
      isPlaying: true
    }
  case STOP:
    return {
      ...currentState,
      isPlaying: false
    }
  case CARD_SONG_ID:
    return {
      ...currentState,
      cardSongId: response
    }
  case SET_SP_PLAYER_ACTION:
    return {
      ...currentState,
      spPlayerAction: response
    }
  default:
    return {
      ...currentState
    }
  }
}
