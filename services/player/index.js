import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const playerReducer = reducer
export const playerDispatcher = actionDispatcher
export const playerTypes = actionTypes
