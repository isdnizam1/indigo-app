import { PLAY, STOP } from './actionTypes'

export const playDispatcher = (queries) => ({
  type: PLAY,
  queries
})

export const stopDispatcher = (queries) => ({
  type: STOP,
  queries
})

export default {
  playDispatcher,
  stopDispatcher
}
