export const PLAY = 'PLAY'
export const STOP = 'STOP'
export const CARD_SONG_ID = 'CARD_SONG_ID'
export const SET_SP_PLAYER_ACTION = 'SET_SP_PLAYER_ACTION'

export default {
  PLAY,
  STOP,
  SET_SP_PLAYER_ACTION
}
