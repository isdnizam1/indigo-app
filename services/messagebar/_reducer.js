import produce from 'immer'
import { SET_CLEAR_BLUE_MESSAGE } from './actionTypes'

const initialState = {
  messageClearBlue: null
}

const messageBarReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case SET_CLEAR_BLUE_MESSAGE:
    return produce(currentState, (draft) => {
      draft.messageClearBlue = response
    })
  default:
    return currentState
  }
}

export default messageBarReducer
