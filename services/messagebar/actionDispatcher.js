import {
  SET_CLEAR_BLUE_MESSAGE
} from './actionTypes'

export const setClearBlueMessage = (response) => ({
  type: SET_CLEAR_BLUE_MESSAGE,
  response,
})

export const emptyClearBlueMessage = () => ({
  type: SET_CLEAR_BLUE_MESSAGE,
  response: null,
})
