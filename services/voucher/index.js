import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const voucherReducer = reducer
export const voucherDispatcher = actionDispatcher
export const voucherTypes = actionTypes
