import { VOUCHER_SETTER, VOUCHER_CLEAR } from './actionTypes'

export const voucherSet = (response) => ({
  type: VOUCHER_SETTER,
  response
})

export const voucherClear = (response) => ({
  type: VOUCHER_CLEAR,
  response
})

export default {
  voucherSet,
  voucherClear
}
