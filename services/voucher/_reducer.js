import { VOUCHER_SETTER, VOUCHER_CLEAR } from './actionTypes'

const initialState = {
  isLoading: false,
  voucher: undefined,
}

export default jobReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case VOUCHER_SETTER:
    return {
      ...currentState,
      voucher: response
    }
  case VOUCHER_CLEAR:
    return {
      ...currentState,
      voucher: undefined
    }
  default:
    return {
      ...currentState
    }
  }
}
