import produce from 'immer'
import { SET_SEARCH_QUERY, SET_CURRENT_TAB, SET_RECENT_SEARCH, SET_ACTIVITY_FILTER, SET_SEARCHING_STATE, SET_PEOPLE_FILTER, SET_FILTER_POSITION } from './actionTypes'

const initialState = {
  query: '',
  currentTab: null,
  recents: [],
  activityFilter: null,
  isSearching: false,
  peopleFilters: { city_name: '', job_title: '', interest_name: '' },
  filterPosition: {
    people: { x: 0, y: 0 },
    activity: { x: 0, y: 0 },
  }
}

const searchReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case SET_SEARCH_QUERY:
    return produce(currentState, (draft) => {
      draft.query = response
    })
  case SET_CURRENT_TAB:
    return produce(currentState, (draft) => {
      draft.currentTab = response
    })
  case SET_RECENT_SEARCH:
    return produce(currentState, (draft) => {
      draft.recents = response
    })
  case SET_ACTIVITY_FILTER:
    return produce(currentState, (draft) => {
      draft.activityFilter = response
    })
  case SET_SEARCHING_STATE:
    return produce(currentState, (draft) => {
      draft.isSearching = response
    })
  case SET_PEOPLE_FILTER:
    return produce(currentState, (draft) => {
      draft.peopleFilters[Object.keys(response)[0]] = Object.values(response)[0]
    })
  case SET_FILTER_POSITION:
    return produce(currentState, (draft) => {
      draft.filterPosition[response.key] = response.value
    })
  default:
    return currentState
  }
}

export default searchReducer
