import {
  SET_CURRENT_TAB,
  SET_SEARCH_QUERY,
  SET_RECENT_SEARCH,
  SET_ACTIVITY_FILTER,
  SET_SEARCHING_STATE,
  SET_PEOPLE_FILTER,
  SET_FILTER_POSITION,
} from './actionTypes'

export const setQuery = (response) => ({
  type: SET_SEARCH_QUERY,
  response,
})

export const setCurrentTab = (response) => ({
  type: SET_CURRENT_TAB,
  response,
})

export const setRecentSearch = (response) => ({
  type: SET_RECENT_SEARCH,
  response,
})

export const setActivityFilter = (response) => ({
  type: SET_ACTIVITY_FILTER,
  response,
})

export const setSearchingState = (response) => ({
  type: SET_SEARCHING_STATE,
  response,
})

export const setPeopleFilter = (response) => ({
  type: SET_PEOPLE_FILTER,
  response,
})

export const setFilterPosition = (response) => ({
  type: SET_FILTER_POSITION,
  response,
})
