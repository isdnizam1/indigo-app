import {
  KEYBOARD_VISIBLE,
  SET_CURRENT_SCREEN,
  SET_PROGRESS_COMPONENT,
  SET_PROGRESS_COMPLETE,
  SET_CAME_FROM_BOTTOM_NAVIGATION,
  PUSH_DELETED_COLLAB_IDS
} from './actionTypes'

export const keyboardVisible = (response) => ({
  type: KEYBOARD_VISIBLE,
  response
})

export const setCurrentScreen = (response) => ({
  type: SET_CURRENT_SCREEN,
  response
})

export const setProgressComponent = (response) => ({
  type: SET_PROGRESS_COMPONENT,
  response
})

export const setProgressComplete = (response) => ({
  type: SET_PROGRESS_COMPLETE,
  response
})

export const setCameFromBottomNavigation = (response) => ({
  type: SET_CAME_FROM_BOTTOM_NAVIGATION,
  response
})

export const pushDeletedCollabIds = (response) => ({
  type: PUSH_DELETED_COLLAB_IDS,
  response
})

export default {
  keyboardVisible,
  setCurrentScreen,
  setProgressComponent,
  setProgressComplete,
  setCameFromBottomNavigation,
  pushDeletedCollabIds
}
