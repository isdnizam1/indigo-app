import {
  createRef
} from 'react'
import {
  Animated
} from 'react-native'
import produce from 'immer'
import {
  KEYBOARD_VISIBLE,
  SET_SEARCHBOX_REF,
  SET_CURRENT_SCREEN,
  SET_PROGRESS_COMPONENT,
  SET_PROGRESS_COMPLETE,
  SET_CAME_FROM_BOTTOM_NAVIGATION,
  PUSH_DELETED_COLLAB_IDS
} from './actionTypes'

const initialState = {
  keyboard: {
    visible: false,
    ctaV3MarginBottom: new Animated.Value(65),
  },
  search: {
    ref: createRef(),
  },
  currentScreen: null,
  progressComponent: null,
  progressComplete: false,
  cameFromBottomNavigation: false,
  deletedCollabIds: []
}

export default jobReducer = (
  currentState = initialState, {
    type,
    response,
    error
  }
) => {
  switch (type) {
  case KEYBOARD_VISIBLE:
    return {
      ...currentState,
      keyboard: {
        ...currentState.keyboard,
        visible: response,
      },
    }
  case SET_SEARCHBOX_REF:
    return {
      ...currentState,
      search: {
        ...currentState.keyboard,
        ref: response,
      },
    }
  case SET_CURRENT_SCREEN:
    return produce(currentState, (draft) => {
      draft.currentScreen = response
    })
  case SET_PROGRESS_COMPONENT:
    return produce(currentState, (draft) => {
      draft.progressComponent = response
    })
  case SET_PROGRESS_COMPLETE:
    return produce(currentState, (draft) => {
      draft.progressComplete = response
    })
  case SET_CAME_FROM_BOTTOM_NAVIGATION:
    return produce(currentState, (draft) => {
      draft.cameFromBottomNavigation = response
    })
  case PUSH_DELETED_COLLAB_IDS:
    return produce(currentState, (draft) => {
      draft.deletedCollabIds.push(response)
    })
  default:
    return currentState
  }
}
