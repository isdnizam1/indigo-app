import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const exploreReducer = reducer
export const exploreDispatcher = actionDispatcher
export const exploreTypes = actionTypes
