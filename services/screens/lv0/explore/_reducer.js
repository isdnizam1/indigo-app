import { SET_EXPLORE_SCREEN_DATA } from './actionTypes'

const initialState = {
  announcementList: [],
  categoryList: [],
  artistList: [],
  bandList: [],
  gamificationData: {},
  soundconnectList: [],
  videoList: [],
  soundplayList: [],
  eventAuditionList: [],
  collaborationList: [],
  newsUpdateList: [],
  isLoaded: false
}

export default exploreReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case SET_EXPLORE_SCREEN_DATA:
    return {
      ...currentState,
      ...response,
    }
  default:
    return currentState
  }
}
