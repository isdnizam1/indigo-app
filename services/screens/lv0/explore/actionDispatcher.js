import { SET_EXPLORE_SCREEN_DATA } from './actionTypes'

export const setExploreData = (response) => ({
  type: SET_EXPLORE_SCREEN_DATA,
  response
})

export default { setExploreData }
