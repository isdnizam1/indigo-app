import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const luckyFrenReducer = reducer
export const luckyFrenDispatcher = actionDispatcher
export const luckyFrenTypes = actionTypes
