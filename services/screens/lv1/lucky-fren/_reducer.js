import { SET_LUCKY_FREN_LV1 } from './actionTypes'

const initialState = {
  result: {},
  isLoaded: false
}

export default exploreReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case SET_LUCKY_FREN_LV1:
    return {
      ...currentState,
      ...response,
    }
  default:
    return currentState
  }
}
