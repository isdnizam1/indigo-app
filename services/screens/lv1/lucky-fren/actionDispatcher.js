import { SET_LUCKY_FREN_LV1 } from './actionTypes'

export const setData = (response) => ({
  type: SET_LUCKY_FREN_LV1,
  response
})

export default { setData }
