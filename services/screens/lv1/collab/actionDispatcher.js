import {
  SET_COLLAB_LV0_SCREEN_DATA
} from './actionTypes'

export const setCollabData = (response) => ({
  type: SET_COLLAB_LV0_SCREEN_DATA,
  response
})

export default {
  setCollabData
}
