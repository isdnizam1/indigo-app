import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const collabReducer = reducer
export const collabDispatcher = actionDispatcher
export const collabTypes = actionTypes
