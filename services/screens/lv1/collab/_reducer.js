import {
  SET_COLLAB_LV0_SCREEN_DATA
} from './actionTypes'

const initialState = {
  about: {},
  collaborator: [],
  collaboration: [],
}

export default exploreReducer = (currentState = initialState, {
  type,
  response,
  error
}) => {
  switch (type) {
  case SET_COLLAB_LV0_SCREEN_DATA:
    return {
      ...currentState,
      ...response,
    }
  default:
    return currentState
  }
}
