import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const submissionReducer = reducer
export const submissionDispatcher = actionDispatcher
export const submissionTypes = actionTypes
