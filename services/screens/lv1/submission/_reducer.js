import { SET_DATA_SUBMISSION_LV1 } from './actionTypes'

const initialState = {
  about: {},
  event_audition: [],
  isLoaded: false
}

export default exploreReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case SET_DATA_SUBMISSION_LV1:
    return {
      ...currentState,
      ...response,
    }
  default:
    return currentState
  }
}
