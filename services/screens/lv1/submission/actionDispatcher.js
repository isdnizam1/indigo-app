import { SET_DATA_SUBMISSION_LV1 } from './actionTypes'

export const setDataSubmission = (response) => ({
  type: SET_DATA_SUBMISSION_LV1,
  response
})

export default { setDataSubmission }
