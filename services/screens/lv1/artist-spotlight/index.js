import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const artistSpotlightReducer = reducer
export const artistSpotlightDispatcher = actionDispatcher
export const artistSpotlightTypes = actionTypes
