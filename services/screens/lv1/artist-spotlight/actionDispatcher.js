import { SET_ARTIST_SPOTLIGHT_LV1 } from './actionTypes'

export const setData = (response) => ({
  type: SET_ARTIST_SPOTLIGHT_LV1,
  response
})

export default { setData }
