import { combineReducers } from 'redux'
import { KEY_NETWORK_REDUCER, NETWORK_ONLINE } from '../constants/Network'
import { KEY_COACHMARK_LEVEL } from '../constants/Storage'
import { authReducer } from './auth'
import { notificationReducer } from './notification'
import { messageReducer } from './message'
import { jobReducer } from './job'
import { registerReducer } from './register'
import { playerReducer } from './player'
import { timelineReducer } from './timeline'
import { voucherReducer } from './voucher'
import { paymentReducer } from './payment'
import { helperReducer } from './helper'
import { songReducer } from './song'
import messageBarReducer from './messagebar/_reducer'
import { exploreReducer as explorelv0 } from './screens/lv0/explore'
import { collabReducer as collablv1 } from './screens/lv1/collab'
import { artistSpotlightReducer as artistspotlightlv1 } from './screens/lv1/artist-spotlight'
import { submissionReducer as submissionlv1 } from './screens/lv1/submission'
import { luckyFrenReducer as luckyFrenlv1 } from './screens/lv1/lucky-fren'
import { submissionReducer } from './submission'
import { sessionReducer } from './session'
import searchReducer from './search/_reducer'
import { bannerReducer } from './banner'
import { reviewReducer } from './review'
import { appReducer } from './app'

const settingReducer = (currentState = { coachmarkLevel: 0, network: NETWORK_ONLINE }, { type, response, error }) => {
  if (type === `${KEY_COACHMARK_LEVEL}_SETTER`) {
    return {
      ...currentState,
      coachmarkLevel: response.coachmarkLevel
    }
  } else if(type === KEY_NETWORK_REDUCER) {
    return {
      ...currentState,
      network: response.network
    }
  } else {
    return {
      ...currentState
    }
  }
}

const screen = combineReducers({
  lv0: combineReducers({
    explore: explorelv0,
  }),
  lv1: combineReducers({
    artistspotlight: artistspotlightlv1,
    collab: collablv1,
    submission: submissionlv1,
    luckyfren: luckyFrenlv1,
  }),
})

export default combineReducers({
  app: appReducer,
  timeline: timelineReducer,
  auth: authReducer,
  notification: notificationReducer,
  message: messageReducer,
  job: jobReducer,
  setting: settingReducer,
  player: playerReducer,
  voucher: voucherReducer,
  banner: bannerReducer,
  review: reviewReducer,
  payment: paymentReducer,
  register: registerReducer,
  helper: helperReducer,
  song: songReducer,
  screen,
  submission: submissionReducer,
  session: sessionReducer,
  search: searchReducer,
  messagebar: messageBarReducer
})
