import { takeLatest, all, put } from 'redux-saga/effects'
import { authSaga, authTypes } from './auth'
import { notificationSaga, notificationTypes } from './notification'

const actionDispatcher = function *generalDispatcher({ action, actionData, type, mapper, reduxType }) {
  try {
    const { data } = yield call(action, actionData)
    yield put({ type: `${reduxType}_SUCCESS`, response: mapper(data.result) })
  } catch (e) {
    yield put({ type: `${reduxType}_FAILED`, e })
  }
}

export default function *sagas() {
  yield all([
    takeLatest('GENERAL_ACTION_DISPATCH', actionDispatcher), //general dispatch
    takeLatest(authTypes.GET_USER_DETAIL, authSaga.getUserDetailWorker),
    takeLatest(notificationTypes.GET_NOTIFICATION, notificationSaga.getNotificationWorker),
  ])
}
