import { ISLOGIN_SETTER, LOADING_SETTER } from './actionTypes'

const initialState = {
  isLoading: true,
  isLogin: false,
}

export default appReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case LOADING_SETTER:
    return {
      ...currentState,
      isLoading: response
    }
  case ISLOGIN_SETTER:
    return {
      ...currentState,
      isLogin: response
    }
  default:
    return {
      ...currentState
    }
  }
}
