import { ISLOGIN_SETTER, LOADING_SETTER } from './actionTypes'

export const setLoading = (response) => ({
  type: LOADING_SETTER,
  response
})
export const setIsLogin = (response) => ({
  type: ISLOGIN_SETTER,
  response
})

export default {
  setLoading,
  setIsLogin
}
