import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const appReducer = reducer
export const appDispatcher = actionDispatcher
export const appTypes = actionTypes
