import { GET_NOTIFICATION, SET_UPDATE_NOTIFICATIONS, SET_ACTIVITY_NOTIFICATIONS, SET_SHOW_NOTIFICATION_FLAG } from './actionTypes'

export const newNotificationDispatcher = (queries) => ({
  type: GET_NOTIFICATION,
  queries
})

export const setUpdateNotifications = (response) => ({
  type: SET_UPDATE_NOTIFICATIONS,
  response
})

export const setActivityNotifications = (response) => ({
  type: SET_ACTIVITY_NOTIFICATIONS,
  response
})

export const setNotificationFlag = (response) => ({
  type: SET_SHOW_NOTIFICATION_FLAG,
  response
})

export default {
  newNotificationDispatcher,
  setUpdateNotifications,
  setActivityNotifications,
  setNotificationFlag
}
