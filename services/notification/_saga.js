import { call, put } from 'redux-saga/effects'
import { getNotification } from '../../actions/api'
import { GET_NOTIFICATION } from './actionTypes'

export const getNotificationWorker = function *getNotificationWorker(dispatcher) {
  try {
    const { data } = yield call(getNotification, dispatcher.queries)
    yield put({ type: `${GET_NOTIFICATION}_SUCCESS`, response: data.result })
  } catch (e) {
    yield put({ type: 'ERROR_MESSAGE_FAILED', e })
    yield put({ type: `${GET_NOTIFICATION}_FAILED`, e })
  }
}

export default {
  getNotificationWorker
}
