import produce from 'immer'
import { GET_NOTIFICATION, SET_UPDATE_NOTIFICATIONS, SET_ACTIVITY_NOTIFICATIONS, SET_SHOW_NOTIFICATION_FLAG } from './actionTypes'

const initialState = {
  isLoading: false,
  notifications: [],
  pusherNotification: {},
  pusherNotificationFlag: false,
  updates_notifications: [],
  activities_notifications: [],
  showFlag: false
}

export default notificationReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case GET_NOTIFICATION:
    return {
      ...currentState,
      isLoading: true
    }
  case SET_UPDATE_NOTIFICATIONS:
    return produce(currentState, (draft) => {
      draft.updates_notifications = response
    })
  case SET_ACTIVITY_NOTIFICATIONS:
    return produce(currentState, (draft) => {
      draft.activities_notifications = response
    })
  case SET_SHOW_NOTIFICATION_FLAG:
    return produce(currentState, (draft) => {
      draft.showFlag = response
    })
  case `${GET_NOTIFICATION}_SUCCESS`:
    return {
      ...currentState,
      isLoading: false,
      notifications: response
    }
  case `${GET_NOTIFICATION}_FAILED`:
    return {
      ...currentState,
      isLoading: false,
      notifications: [],
      error,
    }
  case `${GET_NOTIFICATION}_PUSHER`:
    return {
      ...currentState,
      pusherNotification: response,
      pusherNotificationFlag: true,
    }
  case `${GET_NOTIFICATION}_CLEAR_PUSHER_FLAG`:
    return {
      ...currentState,
      pusherNotificationFlag: false,
    }
  case `${GET_NOTIFICATION}_CLEAR_PUSHER`:
    return {
      ...currentState,
      pusherNotification: {}
    }
  default:
    return {
      ...currentState
    }
  }
}
