import { GET_USER_DETAIL } from './actionTypes'

const initialState = {
  isLoading: true,
  user: {},
}

export default authReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case GET_USER_DETAIL:
    return {
      ...currentState,
      isLoading: true,
    }
  case `${GET_USER_DETAIL}_SUCCESS`:
    return {
      ...currentState,
      isLoading: false,
      user: response
    }
  case `${GET_USER_DETAIL}_FAILED`:
    return {
      ...currentState,
      isLoading: false,
      user: {},
      error,
    }
  default:
    return {
      ...currentState
    }
  }
}
