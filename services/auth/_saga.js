import { call, put } from 'redux-saga/effects'
import { getProfileDetail } from '../../actions/api'
import { GET_USER_DETAIL } from './actionTypes'

export const getUserDetailWorker = function *getUserDetailWorker(dispatcher) {
  try {
    const { data } = yield call(getProfileDetail, dispatcher.queries)
    yield put({ type: `${GET_USER_DETAIL}_SUCCESS`, response: data.result })
  } catch (e) {
    yield put({ type: 'ERROR_MESSAGE_FAILED', e })
    yield put({ type: `${GET_USER_DETAIL}_FAILED`, e })
  }
}

export default {
  getUserDetailWorker
}
