import { GET_USER_DETAIL } from './actionTypes'

export const authGetUserDetailDispatcher = (queries) => ({
  type: GET_USER_DETAIL,
  queries: {
    ...queries,
    show: 'all',
  }
})

export default {
  authGetUserDetailDispatcher
}
