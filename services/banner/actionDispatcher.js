import { BANNER_SETTER, BANNER_CLEAR } from './actionTypes'

export const setBanner = (response) => ({
  type: BANNER_SETTER,
  response
})

export const clearBanner = (response) => ({
  type: BANNER_CLEAR,
  response
})

export default {
  setBanner,
  clearBanner
}
