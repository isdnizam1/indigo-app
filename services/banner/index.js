import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const bannerReducer = reducer
export const bannerDispatcher = actionDispatcher
export const bannerTypes = actionTypes
