import { BANNER_SETTER, BANNER_CLEAR } from './actionTypes'

const initialState = {
  isLoading: false,
  banner: undefined,
}

export default jobReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case BANNER_SETTER:
    return {
      ...currentState,
      banner: response
    }
  case BANNER_CLEAR:
    return {
      ...currentState,
      banner: undefined
    }
  default:
    return {
      ...currentState
    }
  }
}
