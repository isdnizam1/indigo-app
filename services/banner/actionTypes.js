export const BANNER_SETTER = 'BANNER_SETTER'
export const BANNER_CLEAR = 'BANNER_CLEAR'

export default {
  BANNER_SETTER,
  BANNER_CLEAR
}
