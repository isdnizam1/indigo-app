import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const jobReducer = reducer
export const jobDispatcher = actionDispatcher
export const jobTypes = actionTypes
