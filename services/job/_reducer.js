import { JOB_REGISTRATION_SETTER, JOB_REGISTRATION_CLEAR } from './actionTypes'

const initialState = {
  isLoading: false,
  jobRegistrationForm: {
  },
}

export default jobReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case JOB_REGISTRATION_SETTER:
    return {
      ...currentState,
      jobRegistrationForm: response
    }
  case JOB_REGISTRATION_CLEAR:
    return {
      ...currentState,
      jobRegistrationForm: {
      }
    }
  default:
    return {
      ...currentState
    }
  }
}
