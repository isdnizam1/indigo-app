import { JOB_REGISTRATION_CLEAR, JOB_REGISTRATION_SETTER } from './actionTypes'

export const jobRegistrationSave = (response) => ({
  type: JOB_REGISTRATION_SETTER,
  response
})

export const jobRegistrationClear = (response) => ({
  type: JOB_REGISTRATION_CLEAR,
  response
})

export default {
  jobRegistrationSave,
  jobRegistrationClear
}
