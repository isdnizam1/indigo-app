import { SUBMISSION_DATA_SETTER, SUBMISSION_DATA_CLEAR } from './actionTypes'

export const submissionDataSet = (response) => ({
  type: SUBMISSION_DATA_SETTER,
  response
})

export const submissionDataClear = (response) => ({
  type: SUBMISSION_DATA_CLEAR,
  response
})

export default {
  submissionDataSet,
  submissionDataClear
}
