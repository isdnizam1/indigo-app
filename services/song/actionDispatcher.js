import { SET_ID_JOURNEY, SET_PLAYBACK } from './actionTypes'

export const setIdJourney = (response) => ({
  type: SET_ID_JOURNEY,
  response
})

export const setPlayback = (response) => ({
  type: SET_PLAYBACK,
  response
})

export default {
  setIdJourney,
  setPlayback
}
