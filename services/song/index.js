import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const songReducer = reducer
export const songDispatcher = actionDispatcher
export const songTypes = actionTypes
