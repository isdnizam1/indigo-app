import Audio from '../../utils/audio'
import { SET_ID_JOURNEY, SET_PLAYBACK } from './actionTypes'

const initialState = {
  id_journey: null,
  soundObject: new Audio.Sound(),
  playback: {
    isPlaying: false,
    isBuffering: false,
    didJustFinish: false,
    positionMillis: 0,
    durationMillis: 0,
    isLoaded: false
  }
}

export default songReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case SET_ID_JOURNEY:
    currentState.id_journey = response
    return currentState
  case SET_PLAYBACK:
    // eslint-disable-next-line no-case-declarations
    const {
      isPlaying,
      isBuffering,
      didJustFinish,
      positionMillis,
      durationMillis,
      isLoaded
    } = response
    currentState.playback = {
      isPlaying,
      isBuffering,
      didJustFinish,
      positionMillis,
      durationMillis,
      isLoaded
    }
    return currentState
  default:
    return currentState
  }
}
