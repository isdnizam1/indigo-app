import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const messageReducer = reducer
export const messageDispatcher = actionDispatcher
export const messageTypes = actionTypes
