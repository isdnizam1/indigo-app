import { GET_MESSAGE, GET_MESSAGE_UNREAD, SET_MESSAGE } from './actionTypes'

const initialState = {
  message: [],
  isLoading: false,
  newMessageCount: 0,
}

export default authReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case SET_MESSAGE:
    return {
      ...currentState,
      message: response
    }
  case GET_MESSAGE:
    return {
      ...currentState,
    }
  case GET_MESSAGE_UNREAD:
    return {
      ...currentState,
      newMessageCount: response
    }
  default:
    return {
      ...currentState
    }
  }
}
