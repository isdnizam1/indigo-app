import { GET_MESSAGE, SET_MESSAGE } from './actionTypes'

export const newNMessageDispatcher = (queries) => ({
  type: GET_MESSAGE,
  queries
})
export const setMessage = (response) => ({
  type: SET_MESSAGE,
  response
})

export default {
  newNMessageDispatcher,
  setMessage
}
