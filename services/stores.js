import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import registerMiddleware from '../middlewares/register'
import helperMiddleware from '../middlewares/helper'
import songMiddleware from '../middlewares/song'
import reducers from './reducers'
import sagas from './sagas'

const sagaMiddleware = createSagaMiddleware()
const stores = createStore(
  reducers,
  applyMiddleware(
    sagaMiddleware,
    registerMiddleware,
    helperMiddleware,
    songMiddleware
  )
)
sagaMiddleware.run(sagas)
export default stores
