import { PAYMENT_SOURCE_SETTER, PAYMENT_SOURCE_CLEAR, PAYMENT_SOURCE_ID_ADS_SETTER } from './actionTypes'

const initialState = {
  paymentSource: '',
  idAds: null
}

export default paymentReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case PAYMENT_SOURCE_SETTER:
    return {
      ...currentState,
      paymentSource: response
    }
  case PAYMENT_SOURCE_ID_ADS_SETTER:
    return {
      ...currentState,
      idAds: response
    }
  case PAYMENT_SOURCE_CLEAR:
    return {
      ...currentState,
      paymentSource: '',
      idAds: null
    }
  default:
    return {
      ...currentState
    }
  }
}
