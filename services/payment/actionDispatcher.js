import { PAYMENT_SOURCE_SETTER, PAYMENT_SOURCE_CLEAR, PAYMENT_SOURCE_ID_ADS_SETTER } from './actionTypes'

export const paymentSourceSet = (response) => ({
  type: PAYMENT_SOURCE_SETTER,
  response
})

export const paymentSourceIdAdsSet = (response) => ({
  type: PAYMENT_SOURCE_ID_ADS_SETTER,
  response
})

export const paymentSourceClear = (response) => ({
  type: PAYMENT_SOURCE_CLEAR,
  response
})

export default {
  paymentSourceSet,
  paymentSourceClear,
  paymentSourceIdAdsSet
}
