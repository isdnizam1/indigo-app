import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const paymentReducer = reducer
export const paymentDispatcher = actionDispatcher
export const paymentTypes = actionTypes
