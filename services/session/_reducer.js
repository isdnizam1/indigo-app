import { SESSION_DATA_CLEAR, SESSION_DATA_SETTER } from './actionTypes'

const initialState = {
  data: {}
}

export default jobReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case SESSION_DATA_SETTER:
    return {
      ...currentState,
      data: {
        ...currentState.data,
        ...response
      }
    }
  case SESSION_DATA_CLEAR:
    return {
      ...currentState,
      data: {}
    }
  default:
    return {
      ...currentState
    }
  }
}
