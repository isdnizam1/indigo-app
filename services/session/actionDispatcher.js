import { SESSION_DATA_SETTER, SESSION_DATA_CLEAR } from './actionTypes'

export const sessionDataSet = (response) => ({
  type: SESSION_DATA_SETTER,
  response
})

export const sessionDataClear = (response) => ({
  type: SESSION_DATA_CLEAR,
  response
})

export default {
  sessionDataSet,
  sessionDataClear
}
