import { REVIEW_SETTER, REVIEW_CLEAR } from './actionTypes'

export const setReview = (response) => ({
  type: REVIEW_SETTER,
  response
})

export const clearReview = (response) => ({
  type: REVIEW_CLEAR,
  response
})

export default {
  setReview,
  clearReview
}
