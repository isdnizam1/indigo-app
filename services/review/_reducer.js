import { REVIEW_SETTER, REVIEW_CLEAR, ADD_PROFILE_SETTER, ADD_PROFILE_CLEAR } from './actionTypes'

const initialState = {
  review: false,
  addProfile: false
}

export default jobReducer = (currentState = initialState, { type, response, error }) => {
  switch (type) {
  case REVIEW_SETTER:
    return {
      ...currentState,
      review: response
    }
  case REVIEW_CLEAR:
    return {
      ...currentState,
      review: false
    }
  case ADD_PROFILE_SETTER:
    return {
      ...currentState,
      addProfile: response
    }
  case ADD_PROFILE_CLEAR:
    return {
      ...currentState,
      addProfile: false
    }
  default:
    return {
      ...currentState
    }
  }
}
