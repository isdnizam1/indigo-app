export const REVIEW_SETTER = 'REVIEW_SETTER'
export const REVIEW_CLEAR = 'REVIEW_CLEAR'
export const ADD_PROFILE_SETTER = 'ADD_PROFILE_SETTER'
export const ADD_PROFILE_CLEAR = 'ADD_PROFILE_CLEAR'

export default {
  REVIEW_SETTER,
  REVIEW_CLEAR,
  ADD_PROFILE_SETTER,
  ADD_PROFILE_CLEAR,
}
