import reducer from './_reducer'
import actionDispatcher from './actionDispatcher'
import actionTypes from './actionTypes'

export const reviewReducer = reducer
export const reviewDispatcher = actionDispatcher
export const reviewTypes = actionTypes
