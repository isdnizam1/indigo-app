import { StyleSheet, Dimensions } from 'react-native'
import { WP18, WP15, WP5, WP208, WP25, WP1050 } from '../constants/Sizes'
import { SHIP_GREY_CALM, TOMATO } from '../constants/Colors'
import { getBottomSpace } from '../utils/iphoneXHelper'

const { width } = Dimensions.get('window')

export default StyleSheet.create({
  wrapper: {
    width,
    height: WP18,
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: getBottomSpace()/2
  },
  innerWrapper: {
    width,
    height: WP18,
    flexDirection: 'row',
    paddingHorizontal: 3
  },
  backgroundImage: {
    width,
    height: WP18,
    position: 'absolute',
    left: 0,
    bottom: 0
  },
  wrapButton: {
    zIndex: 999,
    position: 'absolute',
    left: '50%',
    marginLeft: -WP15/2 + 0.2475,
    bottom: WP5
  },
  wrapButtonKanal: {
    zIndex: 999,
    left: '90%',
    marginLeft: -WP15,
    bottom: WP1050
  },
  buttonKanal: {
    width: WP25,
    height: WP25,
  },
  addButton: {
    width: WP15,
    height: WP15,
  },
  tabItem: {
    flex: 1,
    height: WP18 + 15.25,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 5
  },
  tabItemWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabIcon: {
    width: 24.75,
    height: 24.75,
  },
  tabLabel: {
    fontFamily: 'CircularBook',
    color: SHIP_GREY_CALM,
    fontSize: WP208
  },
  tabLabelActive: {
    fontFamily: 'CircularBook',
    color: TOMATO
  }
})