import { StyleSheet } from 'react-native'
import { WP5 } from 'sf-constants/Sizes'

export default StyleSheet.create({
  recents: {
    paddingVertical: WP5,
    paddingHorizontal: WP5
  }
})
