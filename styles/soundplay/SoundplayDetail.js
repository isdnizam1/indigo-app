import { StyleSheet, Dimensions } from 'react-native'
import {
  WP2,
  WP3,
  WP10,
  WP40,
  WP95,
  HP1,
  HP2,
  HP3,
  HP5,
} from 'sf-constants/Sizes'
import { WHITE, GREY_CALM, REDDISH, PALE_GREY } from 'sf-constants/Colors'

const style = StyleSheet.create({
  wrapper: {
    alignItems: 'center',
    paddingHorizontal: WP10,
    backgroundColor: WHITE,
  },
  poster: {
    width: WP40,
    height: WP40,
    borderRadius: WP2,
    marginTop: HP1,
    marginBottom: HP3
  },
  spSpeaker: {
    marginVertical: HP1
  },
  buttonWrapper: {
    marginTop: HP3,
    marginBottom: HP2
  },
  playButton: {
    position: 'absolute',
    bottom: 0,
    borderRadius: 24,
    backgroundColor: REDDISH,
    width: 128,
    height: 48,
    left: Dimensions.get('window').width / 2 - 64,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 100
  },
  playIcon: {
    width: 15,
    height: 15,
    marginRight: WP2
  },
  episodeWrapper: {
    width: WP95,
    paddingHorizontal: WP3,
    alignItems: 'flex-start',
    marginBottom: HP5,
    marginTop: HP3,
    backgroundColor: WHITE
  },
  episodeList: {
    width: '100%',
    paddingVertical: HP2
  },
  episodeTitle: {
    marginBottom: 3,
    paddingRight: 72,
  },
  episodeBorder: {
    borderTopWidth: 1,
    borderTopColor: GREY_CALM
  },
  premiumBadge: {
    position: 'absolute',
    width: 68,
    height: 24,
    borderRadius: 5,
    right: 0,
    top: HP1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: PALE_GREY,
    textAlign: 'center',
    textAlignVertical: 'center',
    lineHeight: 16
  }
})

export default style
