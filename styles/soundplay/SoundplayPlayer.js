import { StyleSheet, Dimensions } from 'react-native'
import Constants from 'expo-constants'
import {
  WP3,
  WP10,
  WP80,
  WP90,
  WP100,
  HP1,
  HP2,
  HP3,
  HP4,
  HP100,
  GREY_DARKER
} from '../../constants/Sizes'
import { WHITE, GREY_CALMER } from '../../constants/Colors'
import { isIOS } from '../../utils/helper'

const { width, height } = Dimensions.get('window')

const style = StyleSheet.create({
  wrapper: {
    alignItems: 'center',
    paddingHorizontal: WP10,
    height: '100%',
    justifyContent: 'center',
  },
  actionBar: {
    width,
    position: 'absolute',
    top: isIOS() ? Constants.statusBarHeight : 0,
    left: 0,
    paddingHorizontal: WP3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  posterWrapper: {
    width: WP80,
    height: WP80,
    borderRadius: WP3,
    marginBottom: HP3,
    elevation: 2,
    backgroundColor: '#ffffff'
  },
  poster: {
    width: WP80,
    height: WP80,
    borderRadius: WP3
  },
  spSpeaker: {
    marginTop: HP1,
    marginBottom: HP3
  },
  durationWrapper: {
    flexDirection: 'row'
  },
  slider: {
    flex: 1,
    marginTop: -0.5
  },
  controlsWrapper: {
    flexDirection: 'row',
    marginTop: HP4,
    alignItems: 'center',
    marginBottom: HP2
  },
  controlButton: {
    paddingHorizontal: WP3,
    flexGrow: 1
  },
  controlIcon: {
    height: 44,
    width: 44
  },
  premiumPopup: {
    position: 'absolute',
    height,
    width: WP100,
    elevation: 5,
    zIndex: 20,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  premiumModal: {
    position: 'absolute',
    height,
    width: WP100,
    zIndex: 20,
    elevation: 5,
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.7)',
    justifyContent: 'flex-end'
  },
  premiumWrapper: {
    elevation: 5,
    backgroundColor: WHITE,
    width,
    zIndex: 21,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: HP4,
    paddingBottom: 27.5,
  },
  ratingPopup: {
    position: 'absolute',
    height,
    width: WP100,
    elevation: 5,
    zIndex: 20,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  ratingModal: {
    position: 'absolute',
    height,
    width: WP100,
    zIndex: 20,
    elevation: 5,
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.7)',
    justifyContent: 'flex-end'
  },
  ratingWrapper: {
    elevation: 5,
    backgroundColor: WHITE,
    width,
    zIndex: 21,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopLeftRadius: 17,
    borderTopRightRadius: 17,
    paddingTop: HP4,
    paddingBottom: 27.5,
  },
  ratingForm: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1.1,
    borderColor: GREY_CALMER,
    paddingVertical: 6,
    paddingHorizontal: 9,
    borderRadius: 6.5,
    width: WP90
  },
  ratingInput: {
    marginTop: 0,
    marginBottom: 0,
    paddingLeft: 15,
    fontFamily: 'CircularBold',
    flex: 1,
    color: GREY_DARKER,
  },
  powered: {
    position: 'absolute',
    width: WP100,
    top: Dimensions.get('window').height - 60,
    // backgroundColor: '#ff0000'
  }
})

export default style