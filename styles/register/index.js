import { Dimensions, StyleSheet } from "react-native";
import {
  GREY_CALM_SEMI,
  GREY_CHAT,
  GREY_DARK90,
  LIPSTICK_TWO,
  PALE_BLUE,
  PALE_WHITE,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  TOMATO_SEMI,
  WHITE,
} from "../../constants/Colors";
import {
  FONT_SIZE,
  WP1,
  WP15,
  WP2,
  WP25,
  WP3,
  WP305,
  WP4,
  WP40,
  WP60,
  WP8,
} from "../../constants/Sizes";
import { isTabletOrIpad } from "../../utils/helper";

const { width } = Dimensions.get("window");

const style = StyleSheet.create({
  rootView: {
    flex: 1,
    width,
  },
  wrapper: {
    flex: 1,
    width,
    backgroundColor: PALE_WHITE,
  },
  headerNormal: {
    paddingVertical: WP1,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  contentScrollView: {
    paddingHorizontal: WP8,
    flex: 1,
  },
  progressWrapper: {
    height: 3,
    marginTop: -3,
    backgroundColor: WHITE,
  },
  progressInner: {
    height: 3,
    backgroundColor: REDDISH,
  },
  httpProgressWrapper: {
    height: 2,
  },
  httpProgress: {
    height: 2,
    backgroundColor: REDDISH,
  },
  headingDefault: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
  },
  headingNoBack: {
    marginTop: WP8,
  },
  headingProfession: {
    justifyContent: "center",
    alignItems: "center",
  },
  headingName: {
    marginTop: WP4,
    marginBottom: WP1,
  },
  ctaWrapper: {
    paddingHorizontal: WP8,
    paddingVertical: WP4,
    backgroundColor: WHITE,
  },
  cta: {
    backgroundColor: TOMATO_SEMI,
    borderRadius: 15,
    height: isTabletOrIpad() ? 68 : 58,
    justifyContent: "center",
  },
  ctaEnabled: {
    backgroundColor: REDDISH,
    elevation: 1,
  },
  ctaText: {
    letterSpacing: 0.25,
  },
  wrapperInner: {
    flex: 1,
    paddingTop: WP8,
  },
  arrowBack: {
    paddingVertical: WP3,
    width: 80,
    alignItems: "center",
    backgroundColor: WHITE,
  },
  formGroup: {
    marginBottom: WP4,
  },
  label: {
    marginBottom: WP1,
    color: SHIP_GREY,
  },
  passwordToggler: {
    position: "absolute",
    right: 0,
    paddingVertical: isTabletOrIpad() ? 18 : 12,
    paddingHorizontal: WP4,
    top: 6,
  },
  passwordToggler2: {
    position: "absolute",
    right: 0,
    paddingVertical: isTabletOrIpad() ? 18 : 12,
    paddingHorizontal: WP4,
    top: 0,
    bottom: 0,
  },
  input: {
    borderWidth: 1,
    height: isTabletOrIpad() ? 50 : 38,
    borderRadius: 5,
    borderColor: GREY_CALM_SEMI,
    backgroundColor: WHITE,
    paddingHorizontal: WP4,
    fontSize: FONT_SIZE[isTabletOrIpad() ? "xtiny" : "mini"],
    color: SHIP_GREY,
    marginVertical: WP2 * 0.7,
  },
  inputEditGroup: {
    borderWidth: 1,
    minWidth: WP60,
    height: isTabletOrIpad() ? 50 : 38,
    borderRadius: 5,
    borderColor: REDDISH,
    backgroundColor: WHITE,
    paddingHorizontal: WP4,
    fontSize: FONT_SIZE[isTabletOrIpad() ? "xtiny" : "mini"],
    color: SHIP_GREY_CALM,
    marginVertical: WP2 * 0.7,
  },
  inputError: {
    color: LIPSTICK_TWO,
    borderColor: LIPSTICK_TWO,
  },
  avatarWrapper: {
    width: isTabletOrIpad() ? WP15 : WP25,
    height: isTabletOrIpad() ? WP15 : WP25,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: GREY_CHAT,
    borderWidth: 1.5,
    borderColor: GREY_CALM_SEMI,
    borderRadius: WP25 / 2,
  },
  avatar: {
    width: WP25 - 3,
    height: WP25 - 3,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: (WP25 - 3) / 2,
  },
  bigAvatarWrapper: {
    width: isTabletOrIpad() ? WP25 : WP40,
    height: isTabletOrIpad() ? WP25 : WP40,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: GREY_CHAT,
    borderWidth: 1.5,
    borderColor: GREY_CALM_SEMI,
    borderRadius: WP40 / 2,
  },
  bigAvatar: {
    width: WP40 - 3,
    height: WP40 - 3,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: (WP40 - 3) / 2,
  },
  cameraIcon: {
    position: "absolute",
    width: 28,
    height: 28,
    bottom: 3,
    right: 3,
  },
  horizontalPadding: {
    paddingHorizontal: WP8,
  },
  noHorizontalPadding: {
    paddingHorizontal: 0,
  },
  tabsWrapper: {
    flexDirection: "row",
    backgroundColor: GREY_CHAT,
    borderRadius: isTabletOrIpad() ? 32 : 21,
  },
  tab: {
    flex: 1,
    height: isTabletOrIpad() ? 64 : 42,
    paddingVertical: isTabletOrIpad() ? 15 : 11,
    alignItems: "center",
    justifyContent: "center",
  },
  tabIndicator: {
    height: isTabletOrIpad() ? 64 : 42,
    width: (width - WP8 * 2) / 2,
    backgroundColor: GREY_DARK90,
    borderRadius: isTabletOrIpad() ? 32 : 21,
    position: "absolute",
  },
  tabContent: {
    padding: WP8,
    height: 300,
    width,
  },
  jobGroupWrapper: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
    marginBottom: WP3,
  },
  jobGroupName: {
    marginBottom: WP4,
    borderRadius: 10,
    width: WP40,
    height: WP25,
    overflow: "hidden",
    justifyContent: "flex-end",
  },
  jobGroupNameWrapper: {
    width: "100%",
    height: "100%",
    justifyContent: "flex-end",
    padding: WP305,
  },
  jobGroupEven: {
    marginRight: WP2,
  },
  jobGroupOdd: {
    marginLeft: WP2,
  },
  jobGroupSelected: {
    backgroundColor: REDDISH,
  },
  jobGroupTick: {
    position: "absolute",
    top: 8,
    right: 8,
    width: 16,
    height: 16,
  },
  genreDivider: {
    borderTopWidth: 1,
    borderTopColor: GREY_CALM_SEMI,
    marginTop: WP4,
    marginBottom: -WP3,
    marginHorizontal: WP8,
  },
  genreScrollArea: {
    paddingHorizontal: WP8,
    paddingVertical: WP4,
  },
  genreButton: {
    backgroundColor: GREY_CHAT,
    height: isTabletOrIpad() ? 52 : 38,
    borderRadius: isTabletOrIpad() ? 26 : 19,
    paddingHorizontal: WP3 + 2,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    flexDirection: "column",
  },
  genreButtonReversed: {
    backgroundColor: GREY_DARK90,
    marginRight: WP15,
  },
  genreButtonSelected: {
    backgroundColor: REDDISH,
  },
  genreClose: {
    width: 17,
    height: 17,
    marginLeft: WP2,
  },
  referralPopup: {
    width,
    height: width * (420 / 812),
    position: "absolute",
    left: 0,
    bottom: 0,
  },
  referralBackground: {
    width,
    backgroundColor: WHITE,
    height: width * (420 / 812),
    justifyContent: "flex-start",
  },
});

export default style;
