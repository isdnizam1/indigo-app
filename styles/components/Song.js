import { StyleSheet } from 'react-native'
import { WP1, WP2, WP4, WP5, WP10, WP12 } from '../../constants/Sizes'
import { PALE_GREY, REDDISH, PALE_BLUE, SKELETON_COLOR } from '../../constants/Colors'

export default StyleSheet.create({
  firstSong: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: WP4,
  },
  song: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: WP4,
    borderTopWidth: 1,
    borderTopColor: PALE_GREY,
  },
  songCoverWrapper: {
    width: WP12,
    height: WP12,
    borderRadius: WP2,
    backgroundColor: SKELETON_COLOR
  },
  songCover: {
    width: WP12,
    height: WP12,
    borderRadius: WP2,
    backgroundColor: SKELETON_COLOR
  },
  songPause: {
    width: WP12,
    height: WP12,
    borderRadius: WP2,
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  songPauseIcon: {
    width: WP5,
    height: WP5,
  },
  songContent: {
    marginLeft: WP5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: WP2,
    flex: 1,
  },
  songContentMeta: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: WP1,
  },
  playingIllustration: {
    width: WP4,
    height: WP4,
    marginRight: WP1,
  },
  progressBar: {
    width: WP10,
    height: WP1 / 2,
    backgroundColor: PALE_BLUE,
  },
  progressBarActive: {
    height: WP1 / 2,
    backgroundColor: REDDISH,
  },
  invisibleTimerHelper: {
    position: 'absolute',
    opacity: 0,
    left: 100,
    bottom: 5,
  },
  dotsWrapper: {
    padding: WP2,
    marginRight: -WP2
  }
})