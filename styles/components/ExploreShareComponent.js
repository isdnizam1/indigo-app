import { StyleSheet } from 'react-native'
import { WP1, WP3, WP100, HP1 } from '../../constants/Sizes'
import { SILVER_CALMER } from '../../constants/Colors'

const borderRadius = 15

const style = StyleSheet.create({
  cardWrapper: {
    backgroundColor: SILVER_CALMER,
    borderRadius
  },
  topicWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: HP1,
    paddingTop: HP1,
  },
  topic: {
    marginTop: HP1,
    marginRight: HP1,
    padding: WP1,
    paddingHorizontal: WP3,
    backgroundColor: SILVER_CALMER,
    borderRadius: WP100,
    flexDirection: 'row',
    justifyContent: 'center'
  }
})

export default style