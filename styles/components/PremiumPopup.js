import { StyleSheet, Dimensions, StatusBar } from 'react-native'
import { isIOS } from '../../utils/helper'
import { HP3, WP15 } from '../../constants/Sizes'

const { width, height } = Dimensions.get('window')

let statusBarHeight = isIOS() ? 0 : StatusBar.currentHeight

const style = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    width,
    height: height + statusBarHeight + 20,
    top: 0,
    left: 0,
    zIndex: 9999,
    backgroundColor: 'transparent',
    elevation: 50
  },
  modal: {
    flex: 1,
    width,
    height: height + statusBarHeight + 20,
    backgroundColor: 'rgba(0,0,0,0.85)'
  },
  content: {
    position: 'absolute',
    width,
    paddingTop: HP3,
    paddingBottom: WP15,
    backgroundColor: '#ffffff',
    elevation: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  closeIcon: {
    width: 20,
    height: 20
  }
})

export default style