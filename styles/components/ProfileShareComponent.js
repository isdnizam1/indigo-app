import { StyleSheet } from 'react-native'
import { WP1, WP3, WP4, WP100, HP1, HP25, WP7 } from '../../constants/Sizes'
import { SILVER_CALMER } from '../../constants/Colors'

const borderRadius = 15
const borderTopLeftRadius = borderRadius
const borderBottomLeftRadius = borderRadius
const height = 104
const backgroundColor = SILVER_CALMER
const flexDirection = 'row'

const style = StyleSheet.create({
  cardWrapper: {
    backgroundColor: SILVER_CALMER,
    borderRadius
  },
  topicWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: HP1,
    paddingTop: HP1,
  },
  topic: {
    marginTop: HP1,
    marginRight: HP1,
    padding: WP1,
    paddingHorizontal: WP3,
    backgroundColor: SILVER_CALMER,
    borderRadius: WP100,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  visualArtWrapper: {
    height: HP25,
    borderRadius
  },
  visualArtImage: {
    height,
    borderRadius
  },
  videoWrapper: {
    flexDirection,
    height,
    borderRadius,
    backgroundColor
  },
  videoImage: {
    width: height,
    height,
    borderTopLeftRadius,
    borderBottomLeftRadius
  },
  experienceWrapper: {
    flexDirection,
    height,
    borderRadius,
    backgroundColor
  },
  experienceImage: {
    width: height,
    height,
    borderTopLeftRadius,
    borderBottomLeftRadius
  },
  songWrapper: {
    flexDirection,
    height,
    borderRadius,
    backgroundColor
  },
  songImage: {
    width: height,
    height,
    borderTopLeftRadius,
    borderBottomLeftRadius
  },
  contentWrapper: {
    flex: 1,
    paddingHorizontal: WP4,
    paddingVertical: HP1
  },
  contentTitle: {
    marginVertical: 2
  },
  iconFloat: {
    width: 40,
    height: 40,
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginLeft: -20,
    marginTop: -20
  },
  slider: {
    marginHorizontal: -WP4,
    height: WP7,
  },
  contentFooter: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  playerDurations: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
})

export default style