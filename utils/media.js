import * as MediaLibrary from 'expo-media-library'
import * as Permissions from 'expo-permissions'
import bugsnagClient from './bugsnag'

export const saveToCameraRoll = async (uri) => {
  const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
  if (statusCameraRoll === 'granted') {
    try {
      const asset = await MediaLibrary.createAssetAsync(uri)
      const album = await MediaLibrary.getAlbumAsync('Soundfren')
      if (album) {
        await MediaLibrary.addAssetsToAlbumAsync(asset, album, false)
      } else {
        await MediaLibrary.createAlbumAsync('Soundfren', asset, false)
      }
    } catch (e) {
      bugsnagClient.notify(e)
    }
  }
}