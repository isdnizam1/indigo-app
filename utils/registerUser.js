import * as Notifications from 'expo-notifications'
import { isFunction } from 'lodash-es'
import NavigationService from '../navigation/_serviceNavigation'
import { getNotifMappingAction } from '../components/notification/NotifAction'
import { postLogPushNotification } from '../actions/api'
import { initializeAmplitudeClient } from './clientAmplitude'
import { pusherClientForChannel, pusherNotificationListener } from './clientPusher'
import { getLocalStorage, getUserId, setLocalStorage } from './storage'
import notificationPushRegister from './notificationPushRegister'
import { notificationMapper } from './notification'
import bugsnagClient from './bugsnag'
import Branch from './expoBranch'

const registerBranch = async () => {
  Branch.subscribe(async ({ params }) => {
    if (params && params['+clicked_branch_link']) {
      const notifAction = await getNotifMappingAction({
        url_mobile: 'internal_link',
        url: params['$deeplink_path'],
      })
      if (notifAction) {
        NavigationService.navigate(notifAction.to, notifAction.payload)
      }
    }
  })
}

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
})

export const registerUser = async (userId, props) => {
  try {
    //REGISTER AMPLITUDE
    initializeAmplitudeClient(userId)

    //CLEAR ALL SCHEDULED LOCAL NOTIFICATION
    await Notifications.dismissAllNotificationsAsync()

    //REGISTER BRANCH
    await registerBranch()

    //REGISTER PUSH EXPO
    await notificationPushRegister(userId)
    Notifications.addNotificationResponseReceivedListener(async (notification) => {
      // await Notifications.scheduleNotificationAsync({
      //   content: notification.request.content,
      //   trigger: { seconds: 2 },
      // })
      const data = notificationMapper(
        notification.notification.request.content.data,
      )
      const notifAction = await getNotifMappingAction({
        url_mobile: data.action,
        url: data.url || data.action,
      })
      if (notifAction) {
        postLogPushNotification({
          id_user: data.id_user,
          id: data.id,
        })
        if (isFunction(notifAction.direct)) {
          notifAction.direct()
        } else {
          props.navigation.navigate(notifAction.to, notifAction.payload)
        }
      }
    })

    //REGISTER PUSHER
    await pusherClientForChannel(userId, 'new_message', async (data) => {
      const unreadMessageCount =
        Number(await getLocalStorage('unreadMessageCount')) || 0
      const newMessageCount = unreadMessageCount + 1
      await setLocalStorage('unreadMessageCount', newMessageCount.toString())
      props.setUnreadMessage(newMessageCount)
    })
    await pusherClientForChannel(userId, 'refresh_profile', async (data) => {
      const userId = await getUserId()
      props.getUserDetailDispatcher(userId)
    })
    await pusherNotificationListener(userId, (notification) => {
      props.setNewNotification(notificationMapper(notification))
    })
  } catch (e) {
    bugsnagClient.notify(e)
    // logAmplitudeEvent('errorInitializeUserData', e)
  }
}
