import moment from 'moment'
import 'moment/min/locales'

export const daysRemaining = (dueToDate, setTime = true) => {
  const todayDate = moment()
  const eventDate = setTime ? moment(dueToDate).set({ hour: 23, minute: 59, second: 59, millisecond: 999 }) : moment(dueToDate)
  return eventDate.diff(todayDate, 'days')
}

export const toNormalDate = (date, fullMonth = false, withYear = true) => {
  return moment(date).locale('id').format(`DD ${fullMonth ? 'MMMM' : 'MMM'}${withYear ? ' YYYY' : ''}`)
}

export const toMonthDate = (date) => {
  if (date === 'Now') return date
  return moment(date, 'MM/YYYY').format('MMMM YYYY')
}

export const fullDateToMonthDate = (date) => {
  if (date === 'Now') return date
  return moment(date, 'DD/MM/YYYY').format('MMMM YYYY')
}

export const toDateHistory = (date, todayTime = false) => {
  if (moment(date).isSame(moment(), 'day')) return todayTime ? moment(date).format('HH:mm') : 'Hari ini'
  else if (moment(date).isSame(moment().subtract(1, 'days'), 'day')) return 'Kemarin'
  return moment(date).format(todayTime ? 'DD/MM/YYYY' : 'MMM DD')
}

export const relativeTime = () => {
  return {
    future: 'dalam %s',
    past: '%s lalu',
    s: 'baru saja',
    ss: '%d lalu',
    m: '%d menit lalu',
    mm: '%d menit lalu',
    h: '%d jam lalu',
    hh: '%d jam lalu',
    d: '%d hari lalu',
    dd: '%d hari lalu',
    w: '%d minggu lalu',
    ww: '%d minggu lalu',
    M: '%d bulan lalu',
    MM: '%d bulan lalu',
    y: '%d tahun lalu',
    yy: '%d tahun lalu'
  }
}

export const monthToMonthYear = (date) => {
  if (date === 'Now') return date
  return moment(date, 'MM/YYYY').format('MMM YYYY')
}