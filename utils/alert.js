import { Alert } from 'react-native'

export const alertMessage = (title, message, action = () => {
}) => Alert.alert(
  title || 'Oops!',
  message || 'Don\'t worry, everything\'s gonna be alright. Please try again',
  [{
    text: 'OK', onPress: () => {
      action()
    }
  }],
  { cancelable: false }
)

export const alertWithTwoButton = (title, message, negative, positive) => Alert.alert(
  title || 'Ooopsss server busy',
  message || 'Don\'t worry, everything\'s gonna be alright. Please try again',
  [{
    text: negative && negative.text ? negative.text : 'Cancel', onPress: () => {
      if (negative) negative.action()
    }
  },
  {
    text: positive && positive.text ? positive.text : 'OK', onPress: () => {
      if (positive) positive.action()
    }
  }],
  { cancelable: false }
)

export const alertDelete = (deleteAction) => {
  alertWithTwoButton('Delete',
    'Are you sure to delete this content?',
    {
      text: 'Cancel', action: () => {}
    },
    {
      text: 'Delete', action: deleteAction
    }
  )
}

export const alertBackUnsaved = (navigateBack) => {
  alertWithTwoButton('Unsaved changes',
    'Do you want to discard your change before navigating away?',
    {
      text: 'Cancel', action: () => {
      }
    },
    {
      text: 'Discard', action: navigateBack
    }
  )
}
