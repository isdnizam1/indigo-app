import NetInfo from '@react-native-community/netinfo'
import stores from '../services/stores'
import { KEY_NETWORK_REDUCER, NETWORK_OFFLINE, NETWORK_ONLINE } from '../constants/Network'

export const networkChecker = () => {
  NetInfo.addEventListener(
    (state) => {
      stores.dispatch({ type: KEY_NETWORK_REDUCER, response: { network: state.isConnected ? NETWORK_ONLINE : NETWORK_OFFLINE } })
    }
  )
}
