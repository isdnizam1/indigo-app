import { AsyncStorage } from "react-native";
import { isString, isEqual, uniqWith } from "lodash-es";
import * as FileSystem from "expo-file-system";
import {
  KEY_USER_ID,
  KEY_USER_CACHE,
  KEY_CHAT_ROOM_LIST,
  KEY_REGISTRATION_DATA,
  KEY_CHAT_COMMENT_LAST_ID,
  KEY_EXPLORE,
  KEY_COLLAB_LV1,
  KEY_NOTIFICATION_FLAG,
  KEY_NOTIF_MAPPING,
  KEY_SHOW_MODAL_ONBOARDING_1000STARTUP,
  KEY_USER_EMAIL,
  KEY_AUTHENTICATION_JWT,
} from "../constants/Storage";
import { clearDb, getRoomById, setRoomByID } from "./dbUtils";

export const getUserId = async () => await AsyncStorage.getItem(KEY_USER_ID);
export const setUserId = async (value) =>
  await AsyncStorage.setItem(KEY_USER_ID, value);

export const setUserCache = async (value) =>
  await AsyncStorage.setItem(KEY_USER_CACHE, value);
export const getUserCache = async () =>
  await AsyncStorage.getItem(KEY_USER_CACHE);
export const setNotificationFlagCache = async (value) =>
  await AsyncStorage.setItem(KEY_NOTIFICATION_FLAG, value ? "1" : "");
export const getNotificationFlagCache = async (value) =>
  await AsyncStorage.getItem(KEY_NOTIFICATION_FLAG);
export const setExploreCache = async (value) =>
  await AsyncStorage.setItem(KEY_EXPLORE, JSON.stringify(value));
export const getExploreCache = async () =>
  await AsyncStorage.getItem(KEY_EXPLORE);
export const setCollabLv1Cache = async (value) =>
  await AsyncStorage.setItem(KEY_COLLAB_LV1, JSON.stringify(value));
export const getCollabLv1Cache = async () =>
  await AsyncStorage.getItem(KEY_COLLAB_LV1);
export const getShowModalOnboarding1000Startup = async () =>
  await AsyncStorage.getItem(KEY_SHOW_MODAL_ONBOARDING_1000STARTUP);
export const setShowModalOnboarding1000Startup = async (value) =>
  await AsyncStorage.setItem(KEY_SHOW_MODAL_ONBOARDING_1000STARTUP, value);
export const getUserEmail = async () =>
  await AsyncStorage.getItem(KEY_USER_EMAIL);
export const setUserEmail = async (value) =>
  await AsyncStorage.setItem(KEY_USER_EMAIL, value);

export const getAuthJWT = async () =>
  await AsyncStorage.getItem(KEY_AUTHENTICATION_JWT);
export const setAuthJWT = async (value) =>
  await AsyncStorage.setItem(KEY_AUTHENTICATION_JWT, value);

export const setNotifMappingList = async (value) =>
  await AsyncStorage.setItem(KEY_NOTIF_MAPPING, JSON.stringify(value));
export const getNotifMappingList = async () => {
  const mapping = await AsyncStorage.getItem(KEY_NOTIF_MAPPING);
  try {
    return JSON.parse(mapping);
  } catch (e) {
    return {};
  }
};

export const getLocalStorage = async (key) => await AsyncStorage.getItem(key);
export const setLocalStorage = async (key, value) =>
  await AsyncStorage.setItem(key, value);
export const setRegistrationData = (value) =>
  AsyncStorage.setItem(KEY_REGISTRATION_DATA, JSON.stringify(value));
export const removeLocalStorage = async (keys) =>
  await AsyncStorage.multiRemove(keys);
export const clearLocalStorage = async () => {
  const asyncStorageKeys = await AsyncStorage.getAllKeys();
  if (asyncStorageKeys.length > 0) {
    AsyncStorage.clear();
  }
  clearDb();
};

export const getChatRoomList = async () => {
  const userId = await getUserId();
  const cacheUri = `${FileSystem.cacheDirectory}${KEY_CHAT_ROOM_LIST}${userId}`;
  try {
    const list = await FileSystem.readAsStringAsync(cacheUri);
    return JSON.parse(list) || [];
  } catch (error) {
    return [];
  }
};

export const setChatRoomList = async (value) => {
  const userId = await getUserId();
  const list = JSON.stringify(value);
  const cacheUri = `${FileSystem.cacheDirectory}${KEY_CHAT_ROOM_LIST}${userId}`;
  await FileSystem.writeAsStringAsync(cacheUri, list);
};

export const getCommentList = async (roomId) => {
  try {
    const list = await getRoomById(roomId);
    return JSON.parse(list.data) || [];
  } catch (error) {
    return [];
  }
  // const userId = await getUserId()
  // const cacheUri = `${FileSystem.cacheDirectory}${KEY_CHAT_COMMENT_LIST}${roomId}${userId}`
  // const result = await FileSystem.getInfoAsync(cacheUri)
  // if(result.exists) {
  //   try {
  //     const list = await FileSystem.readAsStringAsync(cacheUri)
  //     return JSON.parse(list) || []
  //   } catch (error) {
  //     return []
  //   }
  // } else {
  //   return []
  // }
};

export const setCommentList = async (roomId, comments) => {
  setRoomByID(roomId, JSON.stringify(comments));
  // const userId = await getUserId()
  // const list = JSON.stringify(comments.slice(0, 20))
  // const cacheUri = `${FileSystem.cacheDirectory}${KEY_CHAT_COMMENT_LIST}${roomId}${userId}`
  // await FileSystem.writeAsStringAsync(cacheUri, list)
  if (comments && comments.length > 0) {
    await setCommentLastId(roomId, comments[0].id);
  }
};

export const getCommentLastId = async (roomId) => {
  const userId = await getUserId();
  const key = `${KEY_CHAT_COMMENT_LAST_ID}${roomId}${userId}`;
  return AsyncStorage.getItem(key);
};

export const setCommentLastId = async (roomId, id) => {
  const userId = await getUserId();
  const key = `${KEY_CHAT_COMMENT_LAST_ID}${roomId}${userId}`;
  await AsyncStorage.setItem(key, id.toString());
};

export const getRecent = async (key) => {
  const recentSearch = await AsyncStorage.getItem(key);
  return recentSearch ? JSON.parse(recentSearch) : [];
};

export const getRegistrationData = async () => {
  const data = await AsyncStorage.getItem(KEY_REGISTRATION_DATA);
  return data ? JSON.parse(data) : null;
};

export const pushRecent = async (key, value) => {
  const q = isString(value) ? value.toLowerCase() : value;
  let recentSearch = await getRecent(key);
  recentSearch = recentSearch.filter((item) => {
    return isString(item) ? item != q : !isEqual(q, item);
  });
  recentSearch.unshift(q);
  AsyncStorage.setItem(
    key,
    JSON.stringify(uniqWith(recentSearch, isEqual).slice(0, 5))
  );
};

export const setRecent = async (key, values = []) => {
  AsyncStorage.setItem(key, JSON.stringify(values));
};
