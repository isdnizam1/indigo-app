import Database, { createTable, dropTable, executeSql } from 'expo-sqlite-query-helper'

export const initDb = async () => {
  Database('soundfrendb.db')
  await createTable('rooms', {
    id_room: 'INTEGER PRIMARY KEY NOT NULL',
    data: 'TEXT NULL'
  })
}

export const clearDb = async () => {
  await dropTable('rooms')
}

export const getRoomById = async (id) => {
  await initDb()
  const result = await executeSql('SELECT * FROM rooms WHERE id_room = ?;', [id])
  if(result.rows._array.length > 0) {
    return result.rows?._array[0]
  } else {
    return []
  }
}
export const setRoomByID = async (id, data) => {
  await initDb()
  const result = await executeSql('REPLACE INTO rooms (id_room, data) VALUES(?, ?);', [id, data])
  return result
}