import Pusher from 'pusher-js/react-native'
import { pusherConstant } from '../constants/Pusher'

Pusher.logToConsole = true
const pusher = new Pusher(pusherConstant.APP_KEY, {
  cluster: pusherConstant.APP_CLUSTER,
  forceTLS: true,
})

const clientPusher = (userId) => {
  return pusher.subscribe(userId)
}

export const pusherNotificationListener = (userId, notificationHandler) => {
  try {
    const channel = clientPusher(userId)
    channel.bind('followed_by_user', function (data) {
      notificationHandler(data)
    })
    channel.bind('comment_on_status', function (data) {
      notificationHandler(data)
    })

    channel.bind('reply_on_comment', function (data) {
      notificationHandler(data)
    })

    channel.bind('like_on_status', function (data) {
      notificationHandler(data)
    })

    channel.bind('like_on_comment', function (data) {
      notificationHandler(data)
    })

    channel.bind('mentioned_on_post', function (data) {
      notificationHandler(data)
    })
  } catch (e) {}
}

export const pusherClientForChannel = (userId, topic, notificationHandler) => {
  const channel = clientPusher(userId)

  channel.bind(topic, function (data) {
    notificationHandler(data)
  })
}

export default clientPusher
