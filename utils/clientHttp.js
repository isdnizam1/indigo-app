import axios from "axios";
import { isFunction, noop } from "lodash-es";
import stores from "../services/stores";
import { KEY_NETWORK_REDUCER, NETWORK_BUSY } from "../constants/Network";
import { KEY_USER_ID, KEY_USER_LAST_SKIPPED_STEP } from "../constants/Storage";
import NavigationService from "../navigation/_serviceNavigation";
import { removeLocalStorage } from "./storage";
import { alertMessage } from "./alert";
import env from "./env";
import bugsnagClient from "./bugsnag";
import { getAuthJWT } from "sf-utils/storage";

const defaultHeaders = {
  // Authorization: `Bearer ${token}`,
  "X-API-KEY": env.apiKey,
  maxContentLength: Infinity,
  maxBodyLength: Infinity,
};

const clientHttp = axios.create({
  baseURL: env.apiUrl,
  timeout: 15000, // increase timeout by 5000 ms, aang
  // headers: defaultHeaders,
});

clientHttp.interceptors.request.use(async (config) => {
  const token = await getAuthJWT();

  return {
    ...config,
    headers: {
      ...defaultHeaders,
      Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6OTA4ODMsImRldmljZSI6bnVsbCwidGltZXN0YW1wIjoxNjU0Njk4NjU0fQ.8KC31lMX6MnaAhNxjLFNmqOH3sSpTno8SjRsjhAvilk`,
      // Authorization: `Bearer ${token}`,
    },
  };
});

const dispatcher = (action, formData = {}, setState, allowError = false) =>
  new Promise((resolve, reject) => {
    action(formData)
      .then(({ data }) => {
        const errorCode = Number(data.code);
        if (allowError) resolve(data);
        else if (errorCode < 400 && errorCode >= 200) {
          resolve(data.result);
        } else {
          bugsnagClient.notify(new Error(JSON.stringify(data)));
          if (isFunction(setState))
            setState({ isLoading: false }, () => {
              let action = () => {};
              if (data.message === "User not found") {
                action = () => {
                  removeLocalStorage([
                    KEY_USER_ID,
                    KEY_USER_LAST_SKIPPED_STEP,
                  ]).then(() => {
                    NavigationService.navigate("AuthNavigator");
                  });
                };
              }
              // alertMessage(
              //   `Error ${data.status || 'Failed'}`,
              //   data.message || `Something went wrong, we're going to fix it - ${errorCode}`,
              //   action,
              // )
            });
          else {
            // alertMessage()
          }
        }
      })
      .catch((err) => {
        // console.log(err.response);
        if (err.response.status == 401) {
          removeLocalStorage([KEY_USER_ID, KEY_USER_LAST_SKIPPED_STEP]).then(
            () => {
              NavigationService.navigate("AuthNavigator");
            }
          );
        }
        if (err.response) {
          // client received an error response (5xx, 4xx)
        } else if (err.request) {
          // client never received a response, or request never left
        } else {
          // anything else
        }

        reject(err);
        bugsnagClient.notify(err);
        // let errorCallback = () => alertMessage()
        let errorCallback = noop;

        if (allowError) errorCallback = () => {};

        if (isFunction(setState)) {
          setState({ isLoading: false }, errorCallback);
        } else if (!allowError) {
          stores.dispatch({
            type: KEY_NETWORK_REDUCER,
            response: { network: NETWORK_BUSY },
          });
        }
      });
  });

export { defaultHeaders, dispatcher, clientHttp };
