import { BackHandler } from 'react-native'
import { NavigationActions, StackActions } from '@react-navigation/compat'

// eslint-disable-next-line no-console
console.disableYellowBox = true

export const minimizeApp = () => {
  try {
    BackHandler.exitApp()
  } catch(err) { /*silent is gold*/ }
}

export const resetAction = (navigation, index = 0, actions = [], initialStackRoute = 'TimelineScreen') => {
  try {
    const doReset = StackActions.reset({
      index,
      actions: [
        NavigationActions.navigate({ routeName: initialStackRoute }),
        ...actions
      ]
    })
    navigation.dispatch(doReset)
  } catch(err) { /*silent is gold*/ }
}
