import Constants from "expo-constants";

const ENV = {
  dev: {
    apiUrl: "https://apidev.indigoconnect.id/",
    apiAuth:
      "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIiLCJkZXZpY2UiOiJtb2JpbGUiLCJ0aW1lc3RhbXAiOjE2NDg1MTU5NDF9.B48n6GQadzaO_5mY5EuzQC56v0ZLyFINpHHbFZzz1-4",
    apiKey: "bebf128669be148fb1a536656f4de8faeb8bffa0",
    webUrl: "https://webdev.soundfren.id",
    amplitudeKey: "15f2dd9943a5e4fb29fe7d35feca1b9c",
    facebookAuthAppId: "979872618865409",
    facebookAuthPermissions: ["public_profile", "email"],
    googleAuthClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosStandaloneAppClientId:
      "14830201723-ogh5gkevk70d0c4059iept7tfunj8kbv.apps.googleusercontent.com",
    googleAuthAndroidClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthAndroidStandaloneAppClientId:
      "14830201723-7rr49bgu1i3945j69591rag3p4fj4dgd.apps.googleusercontent.com",
    googleAuthScopes: ["profile", "email", "openid"],
    qiscusAppId: "soundfren-d0hbxnsjise",
    qiscusSecretKey: "088dfdbc380189b353de70cacd814f2c",
  },
  stagingdev: {
    apiUrl: "https://apidev.indigoconnect.id/",
    apiAuth: "Basic QFNPVU5ERlJFTjpzb3VuZGZyZW5AbjU0NCE3Mjg=",
    apiKey: "bebf128669be148fb1a536656f4de8faeb8bffa0",
    webUrl: "https://webdev.soundfren.id",
    amplitudeKey: "15f2dd9943a5e4fb29fe7d35feca1b9c",
    facebookAuthAppId: "979872618865409",
    facebookAuthPermissions: ["public_profile", "email"],
    googleAuthClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosStandaloneAppClientId:
      "14830201723-ogh5gkevk70d0c4059iept7tfunj8kbv.apps.googleusercontent.com",
    googleAuthAndroidClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthAndroidStandaloneAppClientId:
      "14830201723-7rr49bgu1i3945j69591rag3p4fj4dgd.apps.googleusercontent.com",
    googleAuthScopes: ["profile", "email", "openid"],
    qiscusAppId: "soundfren-d0hbxnsjise",
    qiscusSecretKey: "088dfdbc380189b353de70cacd814f2c",
  },
  stagingfeature: {
    apiUrl: "https://apidev.indigoconnect.id/",
    apiAuth:
      "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIiLCJkZXZpY2UiOiJtb2JpbGUiLCJ0aW1lc3RhbXAiOjE2NDg1MTU5NDF9.B48n6GQadzaO_5mY5EuzQC56v0ZLyFINpHHbFZzz1-4",
    apiKey: "bebf128669be148fb1a536656f4de8faeb8bffa0",
    webUrl: "https://webdev.soundfren.id",
    amplitudeKey: "15f2dd9943a5e4fb29fe7d35feca1b9c",
    facebookAuthAppId: "979872618865409",
    facebookAuthPermissions: ["public_profile", "email"],
    googleAuthClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosStandaloneAppClientId:
      "14830201723-ogh5gkevk70d0c4059iept7tfunj8kbv.apps.googleusercontent.com",
    googleAuthAndroidClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthAndroidStandaloneAppClientId:
      "14830201723-7rr49bgu1i3945j69591rag3p4fj4dgd.apps.googleusercontent.com",
    googleAuthScopes: ["profile", "email", "openid"],
    qiscusAppId: "soundfren-d0hbxnsjise",
    qiscusSecretKey: "088dfdbc380189b353de70cacd814f2c",
  },
  stagingprod: {
    apiUrl: "https://apistagingprod.soundfren.id/",
    apiAuth:
      "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIiLCJkZXZpY2UiOiJtb2JpbGUiLCJ0aW1lc3RhbXAiOjE2NDg1MTU5NDF9.B48n6GQadzaO_5mY5EuzQC56v0ZLyFINpHHbFZzz1-4",
    apiKey: "bebf128669be148fb1a536656f4de8faeb8bffa0",
    webUrl: "https://soundfren.id",
    amplitudeKey: "15f2dd9943a5e4fb29fe7d35feca1b9c",
    facebookAuthAppId: "979872618865409",
    facebookAuthPermissions: ["public_profile", "email"],
    googleAuthClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosStandaloneAppClientId:
      "14830201723-ogh5gkevk70d0c4059iept7tfunj8kbv.apps.googleusercontent.com",
    googleAuthAndroidClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthAndroidStandaloneAppClientId:
      "14830201723-7rr49bgu1i3945j69591rag3p4fj4dgd.apps.googleusercontent.com",
    googleAuthScopes: ["profile", "email", "openid"],
    qiscusAppId: "soundfren-d0hbxnsjise",
    qiscusSecretKey: "088dfdbc380189b353de70cacd814f2c",
  },
  prod: {
    apiUrl: "https://api.indigoconnect.id/",
    apiAuth:
      "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIiLCJkZXZpY2UiOiJtb2JpbGUiLCJ0aW1lc3RhbXAiOjE2NDg1MTU5NDF9.B48n6GQadzaO_5mY5EuzQC56v0ZLyFINpHHbFZzz1-4",
    apiKey: "bebf128669be148fb1a536656f4de8faeb8bffa0",
    webUrl: "https://eventeer.id",
    amplitudeKey: "447a3b8fa077d6b3eb35a3b558d51566",
    facebookAuthAppId: "979872618865409",
    facebookAuthPermissions: ["public_profile", "email"],
    googleAuthClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthIosStandaloneAppClientId:
      "14830201723-ogh5gkevk70d0c4059iept7tfunj8kbv.apps.googleusercontent.com",
    googleAuthAndroidClientId:
      "14830201723-ap56arsvu6t53f836ic0ol0u703ujq95.apps.googleusercontent.com",
    googleAuthAndroidStandaloneAppClientId:
      "14830201723-7rr49bgu1i3945j69591rag3p4fj4dgd.apps.googleusercontent.com",
    googleAuthScopes: ["profile", "email", "openid"],
    qiscusAppId: "soundfren-d0hbxnsjise",
    qiscusSecretKey: "088dfdbc380189b353de70cacd814f2c",
  },
};

export const isDevelopment =
  Constants.manifest.releaseChannel !== "prod" &&
  Constants.manifest.releaseChannel !== "stagingprod";

export default ENV[
  __DEV__ ? "dev" : Constants.manifest.releaseChannel || "dev"
];
