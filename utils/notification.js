import * as Notifications from 'expo-notifications'

export const notificationMapper = (originalNotification) => ({
  profilePicture: originalNotification.profile_picture,
  username: originalNotification.full_name,
  content: originalNotification.content_mobile || originalNotification.content,
  timestamp: new Date(),
  action: originalNotification.url_mobile,
  idRelatedTo: originalNotification.id_related_to,
  relatedTo: originalNotification.related_to,
  status: originalNotification.status,
  from: originalNotification.notif_from,
  url: originalNotification.url,
  id: originalNotification.id || originalNotification.id_notif,
  id_user: originalNotification.id_user,
})

export async function checkNotificationsAsync() {
  const settings = await Notifications.getPermissionsAsync()
  return (
    settings.granted || settings.ios?.status === Notifications.IosAuthorizationStatus.PROVISIONAL
  )
}

export async function requestNotificationPermissionsAsync() {
  return await Notifications.requestPermissionsAsync({
    ios: {
      allowAlert: true,
      allowBadge: true,
      allowSound: true,
      allowAnnouncements: true,
    },
  })
}