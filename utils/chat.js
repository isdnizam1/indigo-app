import Qiscus from 'qiscus-sdk-core'
import { isEmpty, uniqBy, orderBy } from 'lodash-es'
import env, { isDevelopment } from './env'
import { setCommentList } from './storage'

export const getQiscusPrefix = () => {
  return isDevelopment ? '' : '' // CHANGE THIS LINE FOR DEV_XX
}
export const initQiscus = async (user) => {
  let qiscus = new Qiscus()
  qiscus.init({
    AppId: env.qiscusAppId
  })
  await qiscus.setUser(getQiscusPrefix() + user.id_user, getQiscusPrefix() + user.id_user, user.full_name)
  return qiscus
}

export const getRoomAvatar = (room) => {
  // const options = JSON.parse(room.options)
  // return options.base64 ? `data:image/gif;base64,${room.avatar}` : room.avatar
  return room.avatar
}

export const constructNewComment = (sendComment) => ({
  id: sendComment.id,
  message: sendComment.message,
  username: sendComment.username_as,
  avatar: sendComment.user_avatar_url,
  timestamp: sendComment.timestamp,
  ownComment: true,
  isSent: true,
  isRead: false,
  payload: sendComment.payload,
  unique_id: sendComment.unique_id,
  username_real: sendComment.username_real,
  user_id_str: sendComment.user_id_str
})

export const getTextMessage = (sendComment) => {
  let text = ''
  if (sendComment.payload.type === 'image' ) {
    if(sendComment.room_type === 'group' ) {
      text = `${sendComment.username_as} @ ${sendComment.room_name} send a photo`
    } else {
      text = `${sendComment.room_name } send you a photo`
    }
  }

  if (!isEmpty(sendComment.message)) {
    text = sendComment.message.substring(0, 20)
    if (sendComment.message.length > 20) text += '...'
  }

  return text
}

export const fetchMessage = async (qiscus, roomId) => {
  const comments = await qiscus.loadComments(roomId)
  await setCommentList(roomId, comments)
  return comments
}

export const _constructListComments = (comments) => {
  const uniq = uniqBy(comments, 'id')
  const sort = orderBy(uniq, ['unix_nano_timestamp'], ['desc'])
  return sort
}