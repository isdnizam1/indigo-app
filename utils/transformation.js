import { get } from 'lodash-es'

export const speakerDisplayName = (speakers = []) => {
  if (speakers.length === 0) return ''
  if (speakers.length >= 3) return 'multiple speakers'
  let speakerDisplayName = get(speakers, '0.title')
  if (speakers[1]) speakerDisplayName += ` and ${get(speakers, '1.title')}`
  return speakerDisplayName
}
