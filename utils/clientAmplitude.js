import * as Amplitude from 'expo-analytics-amplitude'
import ENV from './env'

export const initializeAmplitudeClient = (userId) => {
  Amplitude.initializeAsync(ENV.amplitudeKey).then(() => {
    Amplitude.setUserIdAsync(userId)
  })
}

export const logAmplitudeEvent = (eventName, eventProperties = undefined) => {
  Amplitude.initializeAsync(ENV.amplitudeKey).then(() => {
    if (eventProperties) Amplitude.logEventWithPropertiesAsync(eventName, eventProperties)
    else Amplitude.logEventAsync(eventName)
  })
}
