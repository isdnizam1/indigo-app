import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import * as ImageManipulator from 'expo-image-manipulator'
import { omitBy, noop, isEmpty, isNil, get } from 'lodash-es'
import { isIOS } from './helper'
import { getFileSize } from './file'

const compress = 0.9

export const selectPhoto = (
  onSelected = noop,
  name = '',
  aspectRatio = null,
  options = {},
  resizeRatio = []
) => async () => {
  if(!!options && !!aspectRatio) options.aspect = aspectRatio
  if(!options && !!aspectRatio) options = { aspect: aspectRatio }
  const { status: statusCameraRoll } = await Permissions.askAsync(
    Permissions.CAMERA_ROLL
  )
  let result = {}
  if (statusCameraRoll === 'granted') {
    result = await ImagePicker.launchImageLibraryAsync({
      base64: true,
      allowsEditing: !!get(options, 'aspect'),
      ...options,
    })
    if (!result.cancelled) {
      let uri,
        base64
      if (isEmpty(resizeRatio)) {
        uri = result.uri
        base64 = result.base64
      } else {
        const {
          base64: base64resized,
          uri: uriResized,
        } = await ImageManipulator.manipulateAsync(
          result.uri,
          [
            {
              resize: omitBy({
                width: resizeRatio[0],
                height: isIOS() ? resizeRatio[0] : resizeRatio[1],
              }, isNil),
            },
          ],
          {
            base64: true,
            compress,
            format: ImageManipulator.SaveFormat.JPG,
          }
        )
        base64 = base64resized
        uri = uriResized
      }
      onSelected(`${name}Uri`)(uri)
      onSelected(`${name}b64`)(base64)

      let fileName = uri.split('/')
      fileName = fileName[fileName.length - 1]
      onSelected(`${name}Name`)(fileName)

      const fileSize = await getFileSize(uri)
      onSelected(`${name}Size`)(fileSize)

      return {
        uri,
        base64,
        fileName,
        fileSize,
      }
    }
  }
}

export const takePhoto = (
  onSelected = noop,
  name = '',
  aspectRatio = null,
  options = {},
  resizeRatio = []
) => async () => {
  if(!!options && !!aspectRatio) options.aspect = aspectRatio
  if(!options && !!aspectRatio) options = { aspect: aspectRatio }
  const { status: statusCamera } = await Permissions.askAsync(
    Permissions.CAMERA
  )
  const { status: statusCameraRoll } = await Permissions.askAsync(
    Permissions.CAMERA_ROLL
  )
  let result = {}
  if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
    result = await ImagePicker.launchCameraAsync({
      base64: true,
      allowsEditing: !!get(options, 'aspect'),
      ...options,
    })
    if (!result.cancelled) {
      let uri,
        base64
      if (isEmpty(resizeRatio)) {
        uri = result.uri
        base64 = result.base64
      } else {
        const {
          base64: base64resized,
          uri: uriResized,
        } = await ImageManipulator.manipulateAsync(
          result.uri,
          [
            {
              resize: omitBy({
                width: resizeRatio[0],
                height: isIOS() ? resizeRatio[0] : resizeRatio[1],
              }, isNil),
            },
          ],
          {
            base64: true,
            compress,
            format: ImageManipulator.SaveFormat.JPG,
          }
        )
        base64 = base64resized
        uri = uriResized
      }
      onSelected(`${name}Uri`)(uri)
      onSelected(`${name}b64`)(base64)

      let fileName = uri.split('/')
      fileName = fileName[fileName.length - 1]
      const fileSize = await getFileSize(uri)

      return {
        uri,
        base64,
        fileName,
        fileSize,
      }
    }
  }
}

export const formatBytes = (bytes, showUnit = false, decimals = 2) => {
  if (bytes === 0) return '0 Bytes'
  const k = 1024
  const dm = decimals < 0 ? 0 : decimals
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  const i = Math.floor(Math.log(bytes) / Math.log(k))
  const value = parseFloat((bytes / Math.pow(k, i)).toFixed(dm))
  if (!showUnit) return value
  else return `${value} ${sizes[i]}`
}
