import i18n from 'i18n-js'
import * as Localization from 'expo-localization'
import id from '../locales/id.json'

export const loadLocales = () => {
  i18n.fallbacks = true
  i18n.translations = { id }
  i18n.locale = Localization.locale
}
