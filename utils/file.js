import * as FileSystem from 'expo-file-system'

export const getFileSize = async (fileUri) => {
  let fileInfo = await FileSystem.getInfoAsync(fileUri)
  return fileInfo.size
}