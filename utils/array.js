import { reduce } from 'lodash-es'
export const getStringArrayFromObjectArray = (array, objectKey) => reduce(array, (result, value, key) => {
  result.push(value[objectKey])
  return result
}, [])
