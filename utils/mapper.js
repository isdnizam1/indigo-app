import { reduce, get, isArray, isFunction, isNull, join, isString } from 'lodash-es'

export const objectCreator = (source, mapper) => (
  reduce(mapper, (result, value, mappedKey) => {
    let val
    if (isArray(value)) {
      if(!isString(value[1])) {
        const tempSource = get(source, value[0])
        if(tempSource) val = objectMapper(tempSource, value[1])
        else val = []
      } else {
        const length = value.length
        const lastKey = value[length - 1]
        const separator = get(source, lastKey) ? ' ' : lastKey
        const concatedValue = []
        for (let i = 0; i < length; i++) {
          const currentValue = get(source, value[i])
          if(currentValue) concatedValue.push(currentValue)
        }
        val = join(concatedValue, separator)
      }
    }
    else if (isFunction(value)) val = value(source)
    else if (!isArray(value) && !isString(value)) val = objectMapper(source, value)
    else val = get(source, value)
    result[mappedKey] = val
    return result
  }, {})
)

export const objectMapper = (source, mapper) => {
  if (isArray(source)) {
    const result = []
    for (let value of source) {
      if (!isNull(mapper)) value = objectCreator(value, mapper)
      result.push(value)
    }
    return result
  } else {
    return objectCreator(source, mapper)
  }
}

export const mapAdditionalData = (source) => ({
  ...source,
  additionalData: JSON.parse(source.additional_data)
})

export const mapListWithAdditionalData = (list) => {
  return list.map((item) => (
    mapAdditionalData(item)
  ))
}
