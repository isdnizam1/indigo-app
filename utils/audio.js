import { Audio } from 'expo-av'

Audio.setAudioModeAsync({
  staysActiveInBackground: true,
  interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
  playsInSilentModeIOS: true,
  allowsRecordingIOS: false,
  playThroughEarpieceAndroid: false,
  interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX
})

export default Audio