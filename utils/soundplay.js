import * as Notifications from 'expo-notifications'
import { isIOS } from './helper'

const actions = [
  {
    identifier: 'spPause1',
    buttonTitle: 'Pause',
    options: {
      opensAppToForeground: false,
    },
  },
  {
    identifier: 'spNext1',
    buttonTitle: 'Next',
    options: {
      opensAppToForeground: false,
    },
  },
  {
    identifier: 'spPrev2',
    buttonTitle: 'Prev',
    options: {
      opensAppToForeground: false,
    },
  },
  {
    identifier: 'spPause2',
    buttonTitle: 'Pause',
    options: {
      opensAppToForeground: false,
    },
  },
  {
    identifier: 'spNext2',
    buttonTitle: 'Next',
    options: {
      opensAppToForeground: false,
    },
  },
  {
    identifier: 'spPlay3',
    buttonTitle: 'Play',
    options: {
      opensAppToForeground: false,
    },
  },
]

const notificationCategories = [
  {
    id: 'spCategory1',
    actions: actions.slice(0, 2),
    description: 'First episode playing',
  },
  {
    id: 'spCategory2',
    actions: actions.slice(2, 5),
    description: 'Non first episode playing',
  },
  {
    id: 'spCategory3',
    actions: actions.slice(-1),
    description: 'Paused',
  },
]

const localNotification = async (sp, index, categoryId) => {
  await Notifications.dismissAllNotificationsAsync()
  const notif = {
    title: 'Soundfren Play',
    body: sp.additional_data.episodeList[index].title,
    attachments: { url: sp.image },
    data: sp,
    sticky: true,
    categoryIdentifier: categoryId,
    sound: false,
    vibrate: false,
    android: {
      chanelId: 'soundfrenpodcast',
      sound: false,
      vibrate: false,
    },
  }
  if (!isIOS()) {
    Notifications.setNotificationHandler({
      handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: false,
        shouldSetBadge: false,
      }),
    })
    Notifications.scheduleNotificationAsync(notif)
  }
}

export { notificationCategories, actions, localNotification }
