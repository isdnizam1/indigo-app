import { isNil } from 'lodash-es'

const feedbackFeed = { type: 'feedback' }
const trendingFeed = { type: 'trending' }

export const putFeedback = (feeds, isFeedback) => {
  if (isFeedback !== 0) {
    return feeds.filter((item) => item.type !== 'feedback')
  }
  if (feeds.findIndex((item) => item.type === 'feedback') >= 0) {
    return feeds
  }
  if (isFeedback === 0 && feeds.length < 3) {
    return [...feeds, feedbackFeed]
  }
  if (isFeedback === 0) {
    const newFeeds = feeds
    feeds.splice(4, 0, feedbackFeed)
    return newFeeds
  }
  return feeds
}

export const constructTimeline = (feeds, isTrending, isFeedback) => {
  let timelineFeeds = [...feeds]
  if (!isNil(isFeedback)) {
    timelineFeeds = putFeedback(feeds, isFeedback)
  }
  if (isTrending) return [trendingFeed, ...timelineFeeds]
  return timelineFeeds
}
