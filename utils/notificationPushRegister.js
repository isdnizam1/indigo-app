import * as Notifications from 'expo-notifications'
import { postExpoToken } from '../actions/api'
import { checkNotificationsAsync, requestNotificationPermissionsAsync } from './notification'

export default async function notificationPushRegister(userID) {

  let finalStatus = await checkNotificationsAsync()

  if (!finalStatus) {
    finalStatus = await requestNotificationPermissionsAsync()
  }

  if (!finalStatus) {
    return 0
  } else {
    await notificationExpoRegister(userID)
  }
}

export const notificationExpoRegister = async (userId) => {
  try {
    let token = await Notifications.getExpoPushTokenAsync()
    const body = {
      id_user: userId,
      token: token.data
    }
    await postExpoToken(body)
  } catch (e) {
    //
  }
}
