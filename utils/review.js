import { AsyncStorage } from 'react-native'
import * as StoreReview from 'expo-store-review'
import { isEmpty } from 'lodash-es'
import moment from 'moment'
import { KEY_LAST_TIME_REVIEW, KEY_REVIEW } from '../constants/Storage'
import stores from '../services/stores'
import { ADD_PROFILE_CLEAR, ADD_PROFILE_SETTER, REVIEW_SETTER } from '../services/review/actionTypes'
import { getLocalStorage } from './storage'

export const showReviewIfNotYet = async () => {
  const lastBannerTime = await getLocalStorage(KEY_LAST_TIME_REVIEW) || 0
  const interval = moment().unix() - lastBannerTime
  if(interval < 86400) {
    return
  }
  await AsyncStorage.setItem(KEY_LAST_TIME_REVIEW, String(moment().unix()))
  const item = await AsyncStorage.getItem(KEY_REVIEW)
  if(isEmpty(item)) {
    stores.dispatch({ type: REVIEW_SETTER, response: true })
  }
}

export const storeReviewAction = async () => {
  await AsyncStorage.setItem(KEY_REVIEW, (new Date()).toString())
  const isAvailable = await StoreReview.isAvailableAsync() && await StoreReview.hasAction()
  if (isAvailable) {
    await StoreReview.requestReview()
  }
}

export const setAddProfile = async () => {
  stores.dispatch({ type: ADD_PROFILE_SETTER, response: true })
}

export const clearAddProfile = async () => {
  stores.dispatch({ type: ADD_PROFILE_CLEAR })
}