import bugsnag from '@bugsnag/expo'
import Constants from 'expo-constants'

const bugsnagClient = bugsnag({
  releaseStage: __DEV__ ? 'dev' : Constants.manifest.releaseChannel || 'prod',
})

export default bugsnagClient
