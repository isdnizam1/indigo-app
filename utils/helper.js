import Numeral from 'numeral'
import * as Linking from 'expo-linking'
import {
  toLower,
  includes,
  omit,
  isNil,
  isEmpty,
  isString,
  isObject,
  get,
  isFunction,
  noop,
} from 'lodash-es'
import { Platform, Alert, Dimensions } from 'react-native'
import moment from 'moment/min/moment-with-locales'
import { GREY_DISABLE, ORANGE_BRIGHT, PINK_RED } from '../constants/Colors'
import NavigationService from '../navigation/_serviceNavigation'
import { postScreenTimeAds, postUploadImage } from '../actions/api'
import { getUniqueId } from '../screens/chat/Constants'
import { dispatcher } from './clientHttp'
import { getQiscusPrefix, initQiscus } from './chat'

export const getUserIdFromLink = (link) => {
  const splitedLink = link.split('/')
  return splitedLink[splitedLink.length - 1]
}

export const currencyFormatter = (number) => {
  const currency = Numeral(number).format('0,0')
  // eslint-disable-next-line no-useless-escape
  return currency.replace(/\,/g, '.')
}

export const isValidUrl = (string) => {
  // const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
  // eslint-disable-next-line no-useless-escape
  const regexp = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
  return regexp.test(string)
}

export const getImageFromFeed = (feed) => {
  const images = []
  feed.attachment.map((item) => {
    if (item.attachment_type === 'photo') {
      const image = {
        url: item.attachment_file,
        label: feed.full_name,
        timeStamp: feed.created_at,
      }
      images.push(image)
    }
  })
  return images
}

export const getGenre = (genres) => {
  const dataToDisplay = genres.reduce((accum, item) => `${accum}${item.name}, `, '')

  return dataToDisplay.substring(0, dataToDisplay.length - 2)
}

export const formatGenreList = (genres) => {
  const dataToDisplay = genres.reduce((accum, item) => `${accum}${item}, `, '')

  return dataToDisplay.substring(0, dataToDisplay.length - 2)
}

export const isPremiumUser = (type) => type === 'premium'

export const invalidButtonStyle = {
  buttonColor: [GREY_DISABLE, GREY_DISABLE],
  disabled: true,
  opacity: 0.5,
}

export const validButtonStyle = {
  buttonColor: [PINK_RED, ORANGE_BRIGHT],
  disabled: false,
  opacity: 1,
}

export const isEmailValid = (email) => {
  // eslint-disable-next-line no-useless-escape
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

export const getEmailDomain = (email) => {
  const lastIndex = email.lastIndexOf('@')
  return email.substring(lastIndex + 1)
}

export const getImageSong = (work, additionalData) => {
  if (additionalData.cover_image) return additionalData.cover_image
  if (additionalData.icon) return additionalData.icon
  if (additionalData.url_video)
    return `https://img.youtube.com/vi/${getYoutbeId(
      additionalData.url_video,
    )}/hqdefault.jpg`
  return IMAGE_TYPES[work.type]
}

export const initiateRoom = async (userFrom, userTo) => {
  try {
    const qiscus = await initQiscus(userFrom)
    const chatRoom = await qiscus.chatTarget(getQiscusPrefix() + userTo.id_user)
    const receiver = await chatRoom.participants.find(
      async (element) => await (element.email !== userFrom.id_user),
    )
    return { room: chatRoom, qiscus, receiver, template: '' }
  } catch (e) {}
}

export const initiateGroupRoom = async (
  userFrom,
  participants,
  groupName,
  options,
) => {
  const qiscus = await initQiscus(userFrom)
  const chatRoom = await qiscus.createGroupRoom(
    groupName,
    participants.map((el) => getQiscusPrefix() + el),
    options,
  )
  const receiver = await chatRoom.participants.filter(
    async (element) => await (element.email !== userFrom.id_user),
  )
  return {
    room: { ...chatRoom, room_type: 'group' },
    qiscus,
    receiver,
    template: '',
    isGroupChat: true,
  }
}

export const sendGroupEvent = async (userData, roomId, message, type) => {
  try {
    const qiscus = await initQiscus(userData)
    const payload = {
      type,
      content: {
        message,
      },
    }

    return await qiscus.sendComment(
      roomId,
      message,
      getUniqueId(userData.id_user),
      'custom',
      JSON.stringify(payload),
    )
  } catch (e) {}
}

export const NavigateToInternalBrowser = (payload) => {
  if (isExternalUrl(payload.url)) {
    Linking.openURL(payload.url)
  } else {
    NavigationService.push('BrowserScreenNoTab', payload)
  }
}

export const isExternalUrl = (url) => {
  let whitelist = ['spotify', 'gmail', 'yahoo']
  url &&
    whitelist.map((str) => {
      if (toLower(url).includes(str)) return true
    })
  return false
}

export const isIOS = () => Platform.OS === 'ios'

export const fetchAsBlob = (url) => fetch(url).then((response) => response.blob())

export const convertBlobToBase64 = (blob) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onerror = reject
    reader.onload = () => {
      resolve(reader.result)
    }
    reader.readAsDataURL(blob)
  })

export const NavigateToInternalBrowserNoTab = (payload) => {
  if (isExternalUrl(payload.url)) {
    Linking.openURL(payload.url)
  } else {
    NavigationService.push('BrowserScreenNoTab', payload)
  }
}

export const isValidImageUri = (data) => {
  if (data) return data.includes('http') || data.includes('base64')
  return false
}

export const getTime = (milli) => {
  let time = new Date(milli)
  let hours = `0${time.getUTCHours()}`.slice(-2)
  let minutes = `0${time.getUTCMinutes()}`.slice(-2)
  let seconds = `0${time.getUTCSeconds()}`.slice(-2)
  return parseInt(hours || '0') > 0
    ? `${hours}:${minutes}:${seconds}`
    : `${minutes}:${seconds}`
}

export const getYoutbeId = (url) => {
  let regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/
  let match = url.match(regExp)
  return match && match[7].length == 11 ? match[7] : false
}

export const getYoutubeId = getYoutbeId

export const isValidPhoneNumber = (phoneNumber) => {
  let pattern = new RegExp(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s./0-9]*$/g)
  return pattern.test(phoneNumber) && phoneNumber.length >= 10
}

export const validateQuery = (query) => {
  return (query || '').trim().length >= 3
}

export const forceUpdate = () => {
  Alert.alert(
    'Update Available',
    'Please Update to the latest version',
    [
      {
        text: 'Update Now',
        onPress: () => {
          Linking.openURL('https://bit.ly/SoundfrenApps')
          setTimeout(() => forceUpdate(), 500)
        },
      },
    ],
    { cancelable: false },
  )
}

export const shouldUpdate = (prevProps, nextProps) => {
  return JSON.stringify(prevProps) != JSON.stringify(nextProps)
}

export const goToMailbox = (emailAddress) => {
  let domain = emailAddress.split('@')[1]
  if (includes(domain, 'yahoo')) domain = 'mail.yahoo.com'
  Linking.openURL(`http://${domain}`)
}

export const imageBase64ToUrl = async (id_user, base64) => {
  const result = await dispatcher(postUploadImage, {
    id_user,
    image: base64,
  })
  return result.image
}

export const stringBase64ToContent = async (string) => {
  // eslint-disable-next-line no-useless-escape
  const base64 = string.split(/^(data\:image.+)base64\,/)
  if (base64.length > 1) {
    return base64[base64.length - 1]
  } else {
    return base64[0]
  }
}

export const getRandomInt = (min, max) => {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min + 1)) + min
}

export const thousandSeparator = (text, separator = '.') => {
  return text?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator)
}

export const filterAdListing = (ads) => {
  return (ads || []).map((item) => {
    const slimObject = omit(item, ['description', 'percentase', 'created_by'])
    return slimObject
  })
}

export const getSpeaker = (speaker) => {
  if (Array.isArray(speaker)) {
    return makeCommaSeparatedString(
      speaker.map(function (item) {
        return item['title']
      }),
    )
  } else {
    return speaker
  }
}

export const makeCommaSeparatedString = (arr, useOxfordComma) => {
  if (!Array.isArray(arr)) return arr
  const listStart = arr.slice(0, -1).join(', ')
  const listEnd = arr.slice(-1)
  const conjunction =
    arr.length <= 1 ? '' : useOxfordComma && arr.length > 2 ? ', and ' : ' and '
  return [listStart, listEnd].join(conjunction)
}

export const aspectRatio = () => {
  const { height, width } = Dimensions.get('window')
  return height / width
}

export const isTabletOrIpad = () => {
  return aspectRatio() < 1.6
}

export const hexToRGB = (hexCode, opacity) => {
  if (
    !isNil(hexCode) &&
    !isEmpty(hexCode) &&
    isString(hexCode) &&
    hexCode.substring(0, 1) == '#'
  ) {
    let hex = hexCode.replace('#', '')
    if (hex.length === 3) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
    }
    let r = parseInt(hex.substring(0, 2), 16),
      g = parseInt(hex.substring(2, 4), 16),
      b = parseInt(hex.substring(4, 6), 16)
    return `rgba(${r},${g},${b},${opacity})`
  } else return hexCode
}

export const _journeyMapper = (arrJourney) => {
  return arrJourney.map((journey) => {
    if (journey.additional_data) {
      journey.additional_data = JSON.parse(journey.additional_data)
      if (journey.additional_data && journey.additional_data.url_video) {
        if (
          journey.additional_data.url_video.indexOf('/youtu') >= 0 ||
          journey.additional_data.url_video.indexOf('youtube.com') >= 0
        ) {
          journey.additional_data.cover_image = `https://img.youtube.com/vi/${getYoutbeId(
            journey.additional_data.url_video,
          )}/mqdefault.jpg`
        }
      }
      if (journey.additional_data && journey.additional_data.event_date) {
        if (journey.additional_data.event_date === 'Invalid date' && __DEV__) {
          journey.additional_data.event_date = '09/2020'
        }
        const splitDate = journey.additional_data.event_date.split('/')
        journey.event_date = moment(
          new Date(`${splitDate[1]}-${splitDate[0]}-01`),
        ).format('yyyy-MM-DD')
        journey.event_date_id = moment(
          new Date(`${splitDate[1]}-${splitDate[0]}-01`),
        ).format('MMM yyyy')
      }
    }
    if (journey.created_at) {
      journey.created_id = moment(journey.created_at).locale('id').format('d MMM Y')
      journey.created_month = journey.created_id.replace(/\d+\s([A-Z])/g, '$1')
    }
    if (isString(journey.total_view))
      journey.total_view = parseInt(journey.total_view)
    return journey
  })
}

export const bytesToSize = (bytes) => {
  const sizes = ['Bytes', 'kb', 'mb', 'gb', 'tb']
  if (bytes === 0) return '0 Byte'
  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)))
  const convertedSize = bytes / Math.pow(1024, i)
  return `${convertedSize.toFixed(2)} ${sizes[i]}`
}

export const isEqualObject = (object1, object2) => {
  const keys1 = Object.keys(object1)
  const keys2 = Object.keys(object2)

  if (keys1.length !== keys2.length) {
    return false
  }

  for (const key of keys1) {
    const val1 = object1[key]
    const val2 = object2[key]
    const areObjects = isObject(val1) && isObject(val2)
    if (
      (areObjects && !isEqualObject(val1, val2)) ||
      (!areObjects && val1 !== val2)
    ) {
      return false
    }
  }

  return true
}

export const getWordAt = (s, pos) => {
  while (s[pos] == ' ') pos--
  pos = s.lastIndexOf(' ', pos) + 1
  let end = s.indexOf(' ', pos)
  if (end == -1) end = s.length // set to length if it was the last word
  return s.substring(pos, end)
}

export const linkify = (inputText) => {
  let replacedText,
replacePattern1,
replacePattern2,
replacePattern3

  //URLs starting with http://, https://, or ftp://
  replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim
  replacedText = inputText.replace(
    replacePattern1,
    '<a href="$1" target="_blank">$1</a>',
  )

  //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
  replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim
  replacedText = replacedText.replace(
    replacePattern2,
    '$1<a href="http://$2" target="_blank">$2</a>',
  )

  //Change email addresses to mailto:: links.
  replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim
  replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>')

  replacedText = replacedText.replace(
    /(\s[a-z]+\.com)/gim,
    ' <a href="http://$1">$1</a>',
  )
  replacedText = replacedText.replace(
    /(\s[a-z]+\.id)/gim,
    ' <a href="http://$1">$1</a>',
  )
  replacedText = replacedText.replace(
    /(\s[a-z]+\.net)/gim,
    ' <a href="http://$1">$1</a>',
  )
  replacedText = replacedText.replace(
    /(\s[a-z]+\.org)/gim,
    ' <a href="http://$1">$1</a>',
  )
  replacedText = replacedText.replace(
    /(\s[a-z]+\.co\.id)/gim,
    ' <a href="http://$1">$1</a>',
  )

  return replacedText
}

export const osText = (androidText, iosText) => {
  return isIOS() ? iosText : androidText
}

export const restrictedAction = ({
  action,
  userData,
  navigation,
  navigateTo = null,
  isFreeAccess = false,
}) => {
  return isFreeAccess ?
    action :
    get(userData, 'id_user')
      ? action
      : isFunction(navigateTo)
        ? () => navigateTo('AuthNavigator')
        : () => navigation.navigate('AuthNavigator')
}

export const toPriceFormat = (price) => {
  return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}

export const postScreenTimeAdsHelper = async ({ viewFrom, id_ads, id_user, dispatch, isLoading = true, isStartup = false }) => {
  if (!isStartup || isEmpty(id_ads) || isEmpty(viewFrom) || isEmpty(id_user)) {
    // console.log('called')
    return
  }
  // console.log('called')
  const dateFormat = 'YYYY-MM-DD HH:mm:ss'
  const viewUntil = moment().format(dateFormat)
  const duration = moment.duration(moment(viewUntil, dateFormat).diff(moment(viewFrom, dateFormat))).asSeconds()
  const body = { id_user, id_ads, view_from: viewFrom, view_until: viewUntil, duration }
  try {
    if (dispatch) {
      // console.log('run with dispatch ')
      const ads = await dispatch(
        postScreenTimeAds,
        body,
        noop,
        false,
        isLoading,
      )
      return
    }
    const ads = await postScreenTimeAds(body)

  } catch (e) {
    console.log(e)
  }
}
