import Constants from 'expo-constants'
import noop from 'lodash-es/noop'
import { get } from 'lodash-es'

const enableBranch = () => {
  const { manifest } = Constants
  return get(manifest, 'ios.config.branch.apiKey', undefined) || get(manifest, 'android.config.branch.apiKey', undefined)
}

const mockBranch = {
  subscribe: noop,
  createBranchUniversalObject: async () => ({
    showShareSheet: noop,
  })
}

// if (Constants.appOwnership !== 'standalone' || !enableBranch()) {
//   module.exports = mockBranch
// }
// else{
//   module.exports = require('expo-branch')
// }

let branch = mockBranch
try {
  branch = require('expo-branch')
  module.exports = branch
} catch (e) {
  module.exports = branch
}
