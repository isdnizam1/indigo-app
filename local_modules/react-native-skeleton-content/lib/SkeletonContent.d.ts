import * as React from "react";
import { Animated } from "react-native";
import { ISkeletonContentProps, IState, IDirection } from "./Constants";
export default class SkeletonContent extends React.Component<ISkeletonContentProps, IState> {
    static defaultProps: ISkeletonContentProps;
    state: IState;
    animationPulse: Animated.Value;
    animationShiver: Animated.Value;
    interpolatedBackgroundColor: Animated.AnimatedInterpolation;
    getGradientStartDirection: () => IDirection;
    getGradientEndDirection: () => IDirection;
    gradientStart: IDirection;
    gradientEnd: IDirection;
    static getDerivedStateFromProps(nextProps: ISkeletonContentProps, prevState: IState): {
        isLoading: boolean;
        layout: any[] | undefined;
    } | null;
    componentDidMount(): void;
    playAnimation: () => void;
    getBoneStyles: (boneLayout: any) => any;
    getGradientTransform: (boneLayout: any) => object;
    getPositionRange: (boneLayout: any) => number[];
    getStaticBone: (layoutStyle: any, key: number) => JSX.Element;
    getShiverBone: (layoutStyle: any, key: number) => JSX.Element;
    getBones: (layout: any[], children: any) => JSX.Element[];
    renderLayout: (isLoading: boolean, bones: JSX.Element[], children: any) => JSX.Element[];
    render(): JSX.Element;
}
