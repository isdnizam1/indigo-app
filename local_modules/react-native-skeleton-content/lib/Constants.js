'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
let react_native_1 = require('react-native')
exports.DEFAULT_BORDER_RADIUS = 4
exports.DEFAULT_DURATION = 1200
exports.DEFAULT_ANIMATION_TYPE = 'shiver'
exports.DEFAULT_ANIMATION_DIRECTION = 'horizontalRight'
exports.DEFAULT_BONE_COLOR = '#E1E9EE'
exports.DEFAULT_HIGHLIGHT_COLOR = '#F2F8FC'
exports.DEFAULT_EASING = react_native_1.Easing.bezier(0.5, 0, 0.25, 1)
exports.DEFAULT_INTENSITY = 0.6
exports.DEFAULT_LOADING = true
