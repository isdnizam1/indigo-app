#!/bin/bash
declare -a simulators=("1A7A018C-4F00-4F2C-9D38-EC5EB5033601" "7F76D5E6-AC4B-48CE-A638-8A9519AA9CE7")

for i in "${simulators[@]}"
do
    xcrun instruments -w $i
    xcrun simctl install $i ~/.expo/ios-simulator-app-cache/Exponent-2.13.0.app
    xcrun simctl openurl $i exp://127.0.0.1:19000
done
