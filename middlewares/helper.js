import { Animated, Easing } from 'react-native'

const helperMiddleware = ({ dispatch, getState }) => (next) => (action) => {
  const prevState = getState()
  const nextAction = next(action)
  const nextState = getState()
  const {
    helper: {
      keyboard: {
        visible,
        ctaV3MarginBottom
      },
    },
  } = nextState
  if(prevState.helper.keyboard.visible != visible) {
    Animated.timing(ctaV3MarginBottom, {
      toValue: visible ? 12 : 65,
      duration: 150,
      easing: Easing.linear,
      useNativeDriver: false
    }).start()
  }
  return nextAction
}

export default helperMiddleware
