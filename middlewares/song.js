const songMiddleware = ({ dispatch, getState }) => (next) => (action) => {
  const nextAction = next(action)
  const nextState = getState()
  const {
    song: {
      id_journey: nowPlaying,
      soundObject,
      playback: {
        isLoaded,
        isPlaying
      }
    },
  } = nextState
  if(nowPlaying == null && isLoaded && isPlaying) {
    soundObject.pauseAsync()
  }
  return nextAction
}

export default songMiddleware
