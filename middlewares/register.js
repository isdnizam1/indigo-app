import { Animated, Dimensions, Easing } from 'react-native'
import { setRegistrationData } from '../utils/storage'

const { width } = Dimensions.get('window')

const registerMiddleware = ({ dispatch, getState }) => (next) => (action) => {
  const prevState = getState()
  const nextAction = next(action)
  const nextState = getState()
  const {
    register: {
      behaviour: {
        performHttp,
        totalStep,
        step,
        progress,
        httpProgress,
        httpVisibility,
        focused,
      },
    },
  } = nextState
  if (focused) {
    setRegistrationData(nextState.register)
    if (prevState.register.behaviour.step != step) {
      Animated.timing(progress, {
        toValue: (width / (totalStep - 1)) * (step - 1),
        duration: 350,
        delay: 500,
        easing: Easing.ease,
        useNativeDriver: false
      }).start()
    }
    if (prevState.register.behaviour.performHttp != performHttp) {
      if (performHttp) {
        httpProgress.setValue(0)
        httpVisibility.setValue(1)
        Animated.timing(httpProgress, {
          toValue: width - 50,
          duration: 2500,
          easing: Easing.ease,
          useNativeDriver: false
        }).start()
      } else {
        Animated.timing(httpProgress, {
          toValue: width,
          duration: 400,
          easing: Easing.ease,
          useNativeDriver: false
        }).start(() => {
          Animated.timing(httpVisibility, {
            toValue: 0,
            duration: 2000,
            delay: 500,
            useNativeDriver: false
          }).start()
        })
      }
    }
  }
  return nextAction
}

export default registerMiddleware
