# Soundfren Mobile Apps

## `Branch Convention`

|Name|Description|Git|
|---|---|---|
|`feature/soun-xxx`|Create, modify, fixing bugs feature|from `development`|
|`hotfix/soun-xxx`|Urgent bug fixing on production|from `release`|
|**`development`**|Under development code base|-|
|**`release`**|Staging code base|-|
|**`master`**|Released code base|-|
|**`v0`**|Legacy code base|-|

## `Release`
We do the release with expo release flow. All the shortcuts is already written on NPM command (see package.json).

### Release OTA
Only use ota if only if there is no changes in `app.json` (see https://docs.expo.io/versions/latest/guides/configuring-ota-updates/)

#### **Release STAGINGDEV (OTA)**
- push to the remote branch `development`
- No CI? `npm run otadev` from your machine

#### **Release STAGINGFEATURE (OTA)**
- manual trigger from GitLab UI
- No CI? `npm run otastagingfeature` from your machine

#### **Release STAGINGPROD (OTA)**
- manual trigger from GitLab UI
- No CI? `npm run otastagingprod` from your machine

#### **Release PROD (OTA)**
- if it's a HOTFIX, just merge the hotfix branch to the `release`
- if it is not a HOTFIX, isolate the `development` branch that already merged with the feature branches (QA passed) then merge `development` to `release`
- please double check the version and build number (use the semantic versioning)
- push to the remote branch `release`
- the CI will trigger the job (ota prod)
- No CI? `npm run otaprod` from your machine
- if it's a HOTFIX, merge `release` to `development` (you have to leveling the development branch)
- if it is not a HOTFIX, immediately changes the version and build number for the next release on the `development` branch

### Release Binary
- build all binaries for all release-channel (stagingfeature, stagingdev, stagingprod, prod)
- post an information binaries on slack channel

#### **STAGINGDEV and STAGINGFEATURE and STAGINGPROD and PROD Binary**
- checkout to `development` branch
- changes the binary number on app.json (iOS -> ios.buildNumber and Android -> android.versionCode)
- Android `npm run publishdev:a` | `publishstagingfeature:a` | `npm run publishstagingprod:a` | `npm run publish:a`
- iOS `npm run publishdev:i` | `publishstagingfeature:i` | `npm run publishstagingprod:i` | `npm run publish:i`
- input username and password for expo (Soundfren)
- the binary automatically will stored to the Play Store and App Connect

## `Code Convention`
|Code|Convention|
|---|---|
|Class|`CamelCase`|
|Local Variable|`pascalCase`|
|Method|`_underScorePascalCase`|
|Constant Variable|`UPPER_KEBAB_CASE`|

## `Libraries`
- Expo
- React Native
- React Navigation
- React Redux
- React Saga
- Axios
- Pusher
- Qisqus
