const TABS = [
  {
    type: 'people',
    label: 'PEOPLE',
    style: {
      flex: 0.85
    }
  },
  {
    type: 'activity',
    label: 'ACTIVITY',
    style: {
      flex: 0.98
    }
  },
  {
    type: 'video',
    label: 'VIDEO',
    style: {
      flex: 0.72
    }
  },
  {
    type: 'podcast',
    label: 'PODCAST',
    style: {
      flex: 1
    }
  }
]

const ACTIVITIES = [
  {
    type: 'collaboration',
    label: 'Collaboration'
  },
  {
    type: 'event',
    label: 'Event'
  },
  {
    type: 'sc_session',
    label: 'Soundfren Connect'
  },
  {
    type: 'submission',
    label: 'Submission'
  },
]

const LEVEL1 = {
  sc_session: {
    screen: 'SoundconnectScreen'
  },
  sp_album: {
    screen: 'SoundfrenLearnScreen'
  },
  podcast: {
    screen: 'SoundfrenLearnScreen'
  },
  video: {
    screen: 'SoundfrenLearnScreen',
    params: {
      tabToShow: 1
    }
  },
  band: {
    screen: 'AdsScreenNoTab',
    params: {
      category: 'band'
    }
  },
  collaboration: {
    screen: 'CollaborationScreen',
    params: {
      category: 'collaboration'
    }
  },
  event: {
    screen: null,
    params: {
      category: 'event'
    }
  },
  submission: {
    screen: 'Submission',
    params: {
      category: 'submission'
    }
  },
  news_update: {
    screen: 'TimelineScreen',
    params: {
      topicId: 13,
      allowBack: true
    }
  }
}

const CITY_SUGGESTIONS = [
  // { city_name: 'Semua Kota' },
  { city_name: 'Jakarta Selatan' },
  { city_name: 'Bandung' },
  { city_name: 'Yogyakarta' },
  { city_name: 'Bekasi' }
]

const JOB_SUGGESTIONS = [
  // { job_title: 'Semua Profesi' },
  { job_title: 'Front End' },
  { job_title: 'Back End' },
  { job_title: 'React Native' },
  { job_title: 'UIUX Designer' },
  { job_title: 'Cyber Security' },
  { job_title: 'Product Management' },
]

const GENRE_SUGGESTIONS = [
  // { interest_name: 'Semua Genre' },
  { interest_name: 'Machine Learning' },
  { interest_name: 'React Js' },
  { interest_name: 'Flutter' },
  { interest_name: 'React Native' },
]

export { TABS, ACTIVITIES, LEVEL1, CITY_SUGGESTIONS, JOB_SUGGESTIONS, GENRE_SUGGESTIONS }
