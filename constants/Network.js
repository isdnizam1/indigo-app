export const KEY_NETWORK_REDUCER = 'networkReducer'
export const NETWORK_ONLINE = 'networkOnline'
export const NETWORK_OFFLINE = 'networkOffline'
export const NETWORK_BUSY = 'networkBusy'
