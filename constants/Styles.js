import { StyleSheet } from 'react-native'
import { colors, GREY, PALE_BLUE_TWO, SHIP_GREY_CALM, SILVER } from './Colors'
import { HP1, HP10, WP100, WP12, WP3, WP4 } from './Sizes'

export const LINEAR_TYPE = {
  horizontal: {
    start: [0, 0],
    end: [1, 0]
  },
  vertical: {
    start: [0, 0],
    end: [0, 1]
  },
  diagonal: {
    start: [0, 0],
    end: [1, 1]
  }
}

export const TOUCH_OPACITY = 0.8
export const REFRESH_HEIGHT = HP10
export const BORDER_WIDTH = 1
export const BORDER_COLOR = SILVER
export const BORDER_COLOR_ACTIVE = GREY

export const
  CONTAINER_STYLE = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.white
    },
    footerContainer: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      alignItems: 'center',
      backgroundColor: colors.white,
      paddingVertical: 20
    },
  }),
  SHADOW_STYLE = StyleSheet.create({
    none: {},
    shadowThin: {
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 3,
    },
    shadow: {
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },
    shadowBold: {
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 6,
      },
      shadowOpacity: 0.39,
      shadowRadius: 8.30,
      elevation: 13,
    },
    shadowSoft: {
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.075,
      shadowRadius: 5,
      elevation: 4,
    },
  }),
  MODAL_STYLE = StyleSheet.create({
    center: {},
    bottom: {
      margin: 0,
      justifyContent: 'flex-end'
    }
  }),
  BORDER_STYLE = StyleSheet.create({
    top: {
      borderColor: BORDER_COLOR,
      borderTopWidth: BORDER_WIDTH
    },
    bottom: {
      borderColor: BORDER_COLOR,
      borderBottomWidth: BORDER_WIDTH
    },
    rounded: {
      borderColor: BORDER_COLOR,
      borderWidth: BORDER_WIDTH
    }
  }),
  TEXT_INPUT_STYLE = StyleSheet.create({
    inputV2: {
      padding: HP1,
      width: '100%',
      borderColor: PALE_BLUE_TWO,
      borderWidth: 1,
      color: SHIP_GREY_CALM,
      borderRadius: 6,
    }
  })

export const HEADER = StyleSheet.create({
  shadow: {
    width: WP100,
    height: WP3,
    position: 'absolute',
    left: 0,
    bottom: -WP3,
    right: 0,
    zIndex: 999,
  },
  rightIcon: {
    paddingVertical: WP3, paddingRight: WP4, paddingLeft: WP3
  },
  height: WP12
})

export default {
  ...CONTAINER_STYLE,
  ...SHADOW_STYLE,
  ...MODAL_STYLE,
  ...BORDER_STYLE,
  ...TEXT_INPUT_STYLE,
  HEADER
}
