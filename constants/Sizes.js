import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen'

export const HP05 = hp('0.5%'),
  HP08 = hp('0.8%'),
  HP1 = hp('1%'),
  HP105 = hp('1.5%'),
  HP2 = hp('2%'),
  HP205 = hp('2.5%'),
  HP3 = hp('3%'),
  HP305 = hp('3.5%'),
  HP4 = hp('4%'),
  HP5 = hp('5%'),
  HP6 = hp('6%'),
  HP7 = hp('7%'),
  HP8 = hp('8%'),
  HP9 = hp('9%'),
  HP10 = hp('10%'),
  HP1050 = hp('10.5%'),
  HP11 = hp('11%'),
  HP12 = hp('12%'),
  HP135 = hp('13.5%'),
  HP15 = hp('15%'),
  HP16 = hp('16%'),
  HP20 = hp('20%'),
  HP25 = hp('25%'),
  HP30 = hp('30%'),
  HP35 = hp('35%'),
  HP40 = hp('40%'),
  HP45 = hp('45%'),
  HP50 = hp('50%'),
  HP80 = hp('80%'),
  HP90 = hp('90%'),
  HP100 = hp('100%'),
  WP05 = wp('.5%'),
  WP1 = wp('1%'),
  WP105 = wp('1.5%'),
  WP2 = wp('2%'),
  WP22 = wp('22%'),
  WP27 = wp('27%'),
  WP205 = wp('2.5%'),
  WP208 = wp('2.8%'),
  WP3 = wp('3%'),
  WP301 = wp('3.1%'),
  WP305 = wp('3.5%'),
  WP308 = wp('3.8%'),
  WP4 = wp('4%'),
  WP401 = wp('4.1%'),
  WP405 = wp('4.5%'),
  WP408 = wp('4.8%'),
  WP5 = wp('5%'),
  WP502 = wp('5.2%'),
  WP505 = wp('5.5%'),
  WP508 = wp('5.8%'),
  WP509 = wp('5.9%'),
  WP6 = wp('6%'),
  WP606 = wp('6.6%'),
  WP7 = wp('7%'),
  WP8 = wp('8%'),
  WP9 = wp('9%'),
  WP10 = wp('10%'),
  WP11 = wp('11%'),
  WP12 = wp('12%'),
  WP135 = wp('13.5%'),
  WP14 = wp('14%'),
  WP15 = wp('15%'),
  WP16 = wp('16%'),
  WP162 = wp('16.2%'),
  WP17 = wp('17%'),
  WP18 = wp('18%'),
  WP20 = wp('20%'),
  WP24 = wp('24%'),
  WP25 = wp('25%'),
  WP28 = wp('28%'),
  WP29 = wp('29%'),
  WP30 = wp('30%'),
  WP35 = wp('35%'),
  WP375 = wp('37.5%'),
  WP40 = wp('40%'),
  WP43 = wp('43%'),
  WP44 = wp('44%'),
  WP45 = wp('45%'),
  WP46 = wp('46%'),
  WP48 = wp('48%'),
  WP49 = wp('49%'),
  WP50 = wp('50%'),
  WP55 = wp('55%'),
  WP60 = wp('60%'),
  WP63 = wp('63%'),
  WP65 = wp('65%'),
  WP67 = wp('67%'),
  WP70 = wp('70%'),
  WP75 = wp('75%'),
  WP80 = wp('80%'),
  WP85 = wp('85%'),
  WP88 = wp('88%'),
  WP90 = wp('90%'),
  WP95 = wp('95%'),
  WP100 = wp('100%'),
  WP1050 = wp('105%')

export const FONT_SIZE = {
  xxtiny: WP105, //6
  petite: WP2, //6
  xtiny: WP205, //8
  tiny: WP301, //HP2 //10
  xmini: WP305, //11
  mini: WP308, //HP205 //12
  slight: WP401, //13
  small: WP405, //HP3 //14
  medium: WP408, //15
  large: WP502, //HP4 //16
  huge: WP509, //HP5 //18
  massive: WP606, //HP6 //20
  extraMassive: WP8, //24,
  semiJumbo: 32,
  jumbo: 38,
}

export const ICON_SIZE = {
  xxtiny: WP105, //8
  xtiny: WP205, //8
  tiny: WP301,
  xmini: WP305, //11
  mini: WP308,
  slight: WP401, //13
  small: WP405,
  large: WP502,
  huge: WP6,
  massive: WP8,
  extraMassive: WP25,
}

export const IMAGE_SIZE = {
  badge: WP3,
  xtiny: WP405,
  tiny: WP6,
  xmini: WP7,
  mini: WP8,
  xsmall: WP10,
  small: WP12,
  medium: WP14,
  regular: WP15,
  large: WP16,
  huge: WP20,
  massive: WP25,
  extraMassive: WP30,
  extraMassive2: WP35,
  ultraMassive: WP40,
  fullSemi: WP90,
  full: WP100,
}

export const LOADER_SIZE = {
  tiny: WP5,
  xmini: WP7,
  mini: WP10,
  slight: WP15,
  small: WP25,
  large: WP50,
}
