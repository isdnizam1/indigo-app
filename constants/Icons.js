export const SONG_SOURCES = {
  'others': require('sf-assets/icons/v3/songOthers.png'),
  'soundcloud': require('sf-assets/icons/v3/songSoundcloud.png'),
  'joox': require('sf-assets/icons/v3/songJoox.png'),
  'spotify': require('sf-assets/icons/v3/songSpotify.png'),
}