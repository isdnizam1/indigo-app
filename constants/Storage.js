export const KEY_USER_ID = "@soundfren:user:id";
export const KEY_USER_CACHE = "@soundfren:user:cache";
export const KEY_USER_LAST_SKIPPED_STEP = "@soundfren:user:lastSkippedStep";
export const KEY_COACHMARK_LEVEL = "@soundfren:coachmarkLevel";
export const KEY_COACHMARK_APPEAR = "@soundfren:coachmark:appear";
export const KEY_CHAT_ROOM_LIST = "@soundfren:chatRoomList";
export const KEY_CHAT_COMMENT_LIST = "@soundfren:chatCommentList";
export const KEY_CHAT_COMMENT_LAST_ID = "@soundfren:chatCommentLastId";
export const KEY_USER_HIDE_GREETINGS_SC =
  "@soundfren:user:hideGreetingsSoundconnect";
export const KEY_BANNER_POPUP = "@soundfren:popupbanner";
export const KEY_LAST_TIME_BANNER_POPUP = "@soundfren:lasttimepopupbanner";
export const KEY_RECENT_SEARCH = "@soundfren:recent:search";
export const KEY_RECENT_CITY = "@soundfren:recent:city";
export const KEY_RECENT_PROFESSION = "@soundfren:recent:profession";
export const KEY_RECENT_GENRE = "@soundfren:recent:genre";
export const KEY_REGISTRATION_DATA = "@soundfren:user:registration:v3";
export const KEY_FORGOT_PASSWROD_EMAIL = "@soundfren:forgot:passwrod:email";
export const KEY_EXPLORE = "@soundfren:cache:explore";
export const KEY_COLLAB_LV1 = "@soundfren:cache:collablv1";
export const KEY_NOTIFICATION_FLAG = "@soundfren:cache:notifcationFlag";
export const KEY_REVIEW = "@soundfren:review";
export const KEY_LAST_TIME_REVIEW = "@soundfren:reviewlasttime";
export const KEY_NOTIF_MAPPING = "@soundfren:notificationMapping";
export const KEY_SHOW_MODAL_ONBOARDING_1000STARTUP =
  "@soundfren:showModalOnboarding1000Startup";
export const KEY_USER_EMAIL = "@soundfren:email";
export const KEY_AUTHENTICATION_JWT = "@soundfren:user:authenticationJWT";
