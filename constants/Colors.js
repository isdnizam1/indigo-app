export const ORANGE = "#fd451a", //rgb(253, 69, 26)
  ORANGE20 = "rgb(255, 129, 71)",
  ORANGE_BRIGHT = "#30A3DD", //rgb(255, 86, 8)
  ORANGE_BRIGHT_DARK = "#ACDAF1", //rgb(212, 76, 43)
  ORANGE_TOMATO = "rgb(229, 66, 43)",
  PINK_WHITE = "rgb(239, 233, 241)",
  PINK_RED = "#f70061", //rgb(247, 0, 97)
  PINK_RED_DARK = "#ca157a", //rgb(202, 21, 122)
  PINK_PURPLE = "#db19a9", //rgb(219, 25, 169)
  GREEN30 = "#3BA064",
  TEAL_GREEN = "#0DC76C",
  PURPLE = "#6a37f7", //rgb(106, 55, 247)
  PURPLE_DARK = "rgb(82, 22, 114)",
  PURPLE_DARKER = "rgb(30, 19, 63)",
  PURPLE_DARKER_GRADIENT = "rgb(82, 23, 98)",
  GREY = "#434343", //rgb(67, 67, 67)
  GREY_DARK = "rgb(21, 25, 50)",
  GREY_DARKEN = "rgb(105, 105, 105)",
  GREY_DARKER = "#4f4f4f", // rgb(79, 79, 79)
  GREY_DARK80 = "rgba(35, 42, 49, .8)",
  GREY_LABEL = "rgb(88, 88, 88)",
  GREY_BACKGROUND = "rgb(220, 220, 220)",
  GREY_PLACEHOLDER = "#B2B2B2",
  GREY_BORDER = "rgb(223, 227, 232)",
  GREY02 = "rgba(67, 67, 67, .02)",
  GREY05 = "rgba(67, 67, 67, .05)",
  GREY10 = "rgba(67, 67, 67, .1)",
  GREY20 = "rgba(67, 67, 67, .2)",
  GREY50 = "rgba(67, 67, 67, .5)",
  GREY60 = "rgba(67, 67, 67, .6)",
  GREY65 = "rgba(67, 67, 67, .65)",
  GREY80 = "rgba(67, 67, 67, .8)",
  GREY_WARM = "#8f8f8f", //rgb(143, 143, 143)
  GREY_WHITE = "#f1f1f1",
  GREY_CALM = "rgb(233, 236, 236)",
  GREY_CALMER = "rgb(224, 224, 224)",
  GREY_CALM_SEMI = "rgb(216, 216, 216)",
  GREY_SEMI = "rgba(216, 216, 216, .5)",
  GREY_BLUE = "rgb(62, 71, 87)",
  GREY_DISABLE = "rgb(109, 113, 116)",
  GREY_ABOUT = "rgb(33, 33, 33)",
  GREY_STEEL = "rgb(136, 138, 140)",
  WHITE = "#ffffff", //rgb(255, 255, 255)
  WHITE_MILK = "#f6f6f6",
  WHITE20 = "rgba(255, 255, 255, .2)",
  WHITE30 = "rgba(255, 255, 255, .3)",
  WHITE50 = "rgba(255, 255, 255, .5)",
  WHITE80 = "rgba(255, 255, 255, .8)",
  SILVER = "#dddfe0", //rgb(221, 223, 224)
  SILVER_CALM = "#e6eaed",
  SILVER_WHITE = "rgb(245, 249, 252)",
  SILVER_CALMER = "rgb(246, 247, 248)",
  SILVER_GREY = "rgb(146, 153, 162)",
  BLUE_LINK = "#0645AD",
  BLUE_LIGHT = "rgb(38, 128, 235)",
  BLUE_FACEBOOK = "#3b5998", //rgb(59, 89, 152)
  MARINE = "rgb(8, 65, 92)",
  MARINE_LIGHT = "rgb(19, 118, 149)",
  RED_GOOGLE = "#d1422b", //rgb(209, 66, 43)
  YELLOW = "#FBC02D", //rgb(255, 208, 65)
  NO_COLOR = "transparent",
  BLACK = "#000000",
  BLACK20 = "rgba(0, 0, 0, .2)",
  BLACK70 = "rgba(0, 0, 0, .7)",
  BLACK90 = "rgba(0, 0, 0, .9)",
  BLUE_DARK = "rgb(31, 17, 68)",
  BLUE_DARK_LIGHT = "rgb(82, 22, 114)",
  BLUE_DARK_PURPLE = "rgb(51, 48, 180)",
  PURPLE_BLUE_DARKER = "rgb(79, 22, 97)",
  PURE_RED = "#ff0000",
  TOMATO = "#E5422B",
  BLUE_CHAT = "#3287ec",
  BLUE_MAIN = "#3267E3",
  BLUE_FOCUS = "#D6E0F8",
  BLUE_CHAT20 = "rgba(50, 135, 236, .2)",
  GREY_CHAT = "rgb(246, 246, 246)",
  GREY_CHAT_15 = "#453C60",
  GREY_DARK90 = "rgb(21, 26, 48)",
  WARM_PURPLE = "rgb(113, 51, 154)",
  WARM_PURPLE_LIGHT = "rgb(233, 213, 245)",
  BLACK10 = "rgba(0, 0, 0, .1)",
  DEEP_ROSE = "rgb(193, 70, 128)", // #c14680
  ROYAL_PURPLE = "rgb(91, 0, 123)", // #5b007b
  ROYAL_BLUE = "rgb(30, 80, 188)", // #1e50bc
  DARK_INDIGO = "rgb(9, 10, 43)", // #090a2b
  BLUE_AZURE = "rgb(32, 171, 255)", // #20abff
  TEAL = "rgb(0, 168, 150)", // #20abff
  LIGHT_TEAL = "rgb(212, 255, 250)", // #d4fffa
  LIPSTICK = "rgb(201, 37, 45)", // #c9252d
  LIGHT_PINK = "rgb(255, 242, 243)", // #fff2f3
  ICE_BLUE = "rgb(242, 248, 255)", // #f2f8ff
  SAPPHIRE = "rgb(53, 36, 172)", // #3524AC
  LIGHT_PURPLE = "rgb(199, 206, 239)", // #c7ceef
  GREEN = "#19a695",
  GREEN_TOSCA = "#00a895",
  GREEN_FLOWKIT = "#29CC6A",
  GREEN_SOFT = "#D6F8E8",
  DARK_SLATE_BLUE = "rgb(41, 43, 112)", // #292b70
  MANILA = "rgb(255, 254, 142)", // #fffe8e
  MANGO = "rgb(255, 162, 33)", // #ffa221
  GREY_ICON = "rgb(180, 180, 180)",
  MAIZE = "rgb(247, 213, 67)", // #f7d543
  TRANSPARENT = "transparent",
  AZUL = "rgb(38, 128, 235)",
  TOMATO_CALM = "rgb(251, 110, 82)",
  TOMATO_CALM50 = "rgba(251, 110, 82, 0.5)",
  TOMATO_SEMI = "rgb(242, 222, 218)",
  SKELETON_COLOR = "#f4f4f4",
  SKELETON_HIGHLIGHT = "#ffffff",
  PUMPKIN = "#f9c74f",
  PUMPKIN_LIGHT = "#fff0cc",
  PUMPKIN_DARK = "#f28c00",
  LIPSTICK_DARK = "#4d0004",
  LIPSTICK_LIGHT = "#ffe6e7",
  LIPSTICK_TWO = "#e12c53",
  NAVY_BLUE = "#00234e",
  LIGHT_BLUE = "#30a3dd",
  SOFT_BLUE = "#4eb7f7",
  NAVY_DARK = "#212b36", //rgb(33, 43, 54)
  SHIP_GREY = "#637381", //rgb(99, 115, 129)
  SHIP_GREY_CALM = "#919eab", //rgb(145 158 171)
  REDDISH = "#CC0000", //rgb(255, 101, 31)
  REDDISH_DISABLED = "#F7D5D5", //rgb(255, 101, 31)
  GUN_METAL = "#454f5b", //rgb(69 79 91)
  PALE_WHITE = "#fcfcfc", //rgb(252 252 252)
  PALE_GREY = "#f4f6f8", //rgb(244 246 248)
  PALE_BLUE = "#e4e6e7", //rgb(228 230 231)
  CLEAR_BLUE = "rgb(80, 184, 60)", //rgb 50 135 236
  GREY_WARM_1 = "#828282", // rgb(130, 130, 130)
  SILVER_TWO = "#d0d4d9", // rgb(208, 212, 217)
  PALE_GREY_TWO = "#f9fafb", // rgb(249, 250, 251)
  PALE_GREY_THREE = "rgb(244, 246, 248)", // rgb(249, 250, 251)
  PALE_BLUE_TWO = "#dfe3e8", // rgb(223, 227, 232)
  PALE_LIGHT_BLUE_TWO = "#c4cdd5", // rgb(196, 205, 213)
  REDDISH_LIGHT = "#30A3DD", // rgb(255, 240, 233)
  PALE_SALMON = "#FCEEEE", // rgb(255, 185, 153)
  PALE_SALMON20 = "rgba(255, 185, 153, 0.2)", // rgb(255, 185, 153)
  BABY_BLUE = "#a2c8f6", // rgb(162, 200, 246)
  GREY_LIGHT = "#cccccc", // rgb(204, 204, 204)
  MELON = "#ff8855", // rgb(255, 136, 85)
  MELON_TWO = "#f86b64", // rgb(248, 107, 100)
  ORANGE_YELLOW = "#ffab1d", // rgb(255, 171, 29)
  WARM_PINK = "#fa5293", // rgb(250, 82, 147)
  IRIS = "#5151c6", // rgb(81, 81, 198)
  LAVENDER_BLUE = "#888bf4", // rgb(136, 139, 244)
  SOFT_BLUE_TWO = "#548af0", // rgb(84, 138, 240)
  BRIGHT_LIGHT_BLUE = "#2dc8ed", // rgb(45, 200, 237)
  GREEN_30 = "rgb(80, 184, 60)",
  GREEN_20 = "rgba(80, 184, 60, .2)",
  RED_20 = "#eb6e5c",
  VERY_LIGHT_PINK_TWO = "#fcecea",
  BLUE_GOOGLE = "#4285F4",
  GREEN_MMB = "#40C700",
  CYPRUS = "#EC3232",
  BLUE10 = "#ACDAF1",

  // indigo space design color
  NEUTRAL_950 = "#1A1A1A",
  NEUTRAL_900 = "#242424",
  NEUTRAL_850 = "#2E2E2E",
  NEUTRAL_800 = "#383838",
  NEUTRAL_750 = "#424242",
  NEUTRAL_700 = "#4D4D4D",
  NEUTRAL_650 = "#575757",
  NEUTRAL_600 = "#616161",
  NEUTRAL_550 = "#6B6B6B",
  NEUTRAL_500 = "#757575",
  NEUTRAL_450 = "#A3A3A3",
  NEUTRAL_400 = "#ADADAD",
  NEUTRAL_350 = "#B8B8B8",
  NEUTRAL_300 = "#C2C2C2",
  NEUTRAL_250 = "#CCCCCC",
  NEUTRAL_200 = "#D6D6D6",
  NEUTRAL_150 = "#E0E0E0",
  NEUTRAL_100 = "#EBEBEB",
  NEUTRAL_50 = "#F5F5F5",

  NEUTRAL_OLD_90 = "#0B0B0B",
  NEUTRAL_OLD_80 = "#1C1C1C",
  NEUTRAL_OLD_70 = "#2E2E2E",
  NEUTRAL_OLD_60 = "#404040",
  NEUTRAL_OLD_50 = "#525252",
  NEUTRAL_OLD_40 = "#6B6B6B",
  NEUTRAL_OLD_30 = "#CECECE",
  NEUTRAL_OLD_20 = "#F1F1F1",
  NEUTRAL_OLD_10 = "#FFFFFF",

  BASE_900 = "#000000",
  BASE_200 = "#F4F6F8",
  BASE_100 = "#F9FAFB",
  BASE_50 = "#ffffff",

  PRIMARY_150 = "#290000",
  PRIMARY_140 = "#440000",
  PRIMARY_130 = "#660000",
  PRIMARY_120 = "#880000",
  PRIMARY_110 = "#AA0000",
  PRIMARY_100 = "#CC0000",
  PRIMARY_90 = "#D52B2B",
  PRIMARY_80 = "#DD5555",
  PRIMARY_70 = "#E68080",
  PRIMARY_60 = "#EEAAAA",
  PRIMARY_50 = "#F1B8B8",
  PRIMARY_40 = "#F4C6C6",
  PRIMARY_30 = "#F7D5D5",
  PRIMARY_20 = "#F9E3E3",
  PRIMARY_10 = "#FCEEEE",

  PRIMARY_OLD_90 = "#2D0407",
  PRIMARY_OLD_80 = "#43060A",
  PRIMARY_OLD_70 = "#6D0B11",
  PRIMARY_OLD_60 = "#980F19",
  PRIMARY_OLD_50 = "#ED1827",
  PRIMARY_OLD_40 = "#F04450",
  PRIMARY_OLD_30 = "#F46F79",
  PRIMARY_OLD_20 = "#F9B1B6",
  PRIMARY_OLD_10 = "#FCDCDF",

  SEMANTIC_MAIN = "#CC0000",
  // SEMANTIC_FOCUS = rgba(204, 0, 0, 0.2),
  SEMANTIC_SURFACE = "#F5CCCC",
  SEMANTIC_BORDER = "#EEAAAA",
  SEMANTIC_HOVER = "#AA0000",
  SEMANTIC_PRESSED = "#660000",

  WARNING_MAIN = "#FBC02D",
  // WARNING_FOCUS = "rgba(251, 192, 45, 0.2)",
  WARNING_SURFACE = "#FEF2D5",
  WARNING_BORDER = "#FEEAB9",
  WARNING_HOVER = "#D1A025",
  WARNING_PRESSED = "#7D6016",

  SUCCESS_MAIN = "#2BA640",
  // SUCCESS_FOCUS = "rgba(43, 166, 64, 0.2)",
  SUCCESS_SURFACE = "#D5EDD9",
  SUCCESS_BORDER = "#B8E1BF",
  SUCCESS_HOVER = "#248A35",
  SUCCESS_PRESSED = "#155320",

  DANGER_MAIN = "#FF4E45",
  // DANGER_FOCUS = "rgba(255, 78, 69, 0.2)",
  DANGER_SURFACE = "#FFDCDA",
  DANGER_BORDER = "#FFC4C1",
  DANGER_HOVER = "#D44139",
  DANGER_PRESSED = "#802722",

  INFO_MAIN = "#5E84F1",
  // INFO_FOCUS = "rgba(94, 132, 241, 0.2)",
  INFO_SURFACE = "#DFE6FC",
  INFO_BORDER = "#C9D6FA",
  INFO_HOVER = "#4E6EC9",
  INFO_PRESSED = "#2F4278";

export const colors = {
  orange: ORANGE,
  orangeBright: ORANGE_BRIGHT,
  orangeBrightDark: ORANGE_BRIGHT_DARK,
  pinkRed: PINK_RED,
  pinkRedDark: PINK_RED_DARK,
  pinkPurple: PINK_PURPLE,
  purple: PURPLE,
  grey: GREY,
  grey20: GREY20,
  grey50: GREY50,
  grey80: GREY80,
  greyWarm: GREY_WARM,
  white: WHITE,
  white20: WHITE20,
  white50: WHITE50,
  white80: WHITE80,
  silver: SILVER,
  silverCalm: SILVER_CALM,
  blueFacebook: BLUE_FACEBOOK,
  redGoogle: RED_GOOGLE,
  yellow: YELLOW,
  noColor: NO_COLOR,
};

export const SHADOW_GRADIENT = [
  "rgba(0,0,0,.042)",
  "rgba(0,0,0,.028)",
  "rgba(0,0,0,.012)",
  "rgba(0,0,0,0)",
];

export const SHADOW_GRADIENT_REVERSE = [
  "rgba(0,0,0,0)",
  "rgba(0,0,0,.012)",
  "rgba(0,0,0,.028)",
  "rgba(0,0,0,.042)",
];
