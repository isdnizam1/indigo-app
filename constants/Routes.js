import React from 'react'
import Text from '../components/Text'
import {
  HP10,
  WP15,
  WP2,
  WP4,
  WP5,
  WP90,
  WP20,
  WP6,
  WP9,
  WP12,
  WP10,
  WP24,
  WP1,
  WP35,
  WP17,
  WP105,
  WP05,
  WP8,
  WP3,
  WP502,
  WP205,
  HP8
} from './Sizes'
import { WHITE, BLUE_LIGHT, ICE_BLUE } from './Colors'

export const REGISTRATION_STEPS = {
  0: 'RegisterPersonalScreen',
  1: 'RegisterLocationJobScreen',
  2: 'RegisterPhotoScreen',
  3: 'RegisterGenreScreen',
  4: 'RegisterVerificationScreen'
}

export const COACHMARK_STEP = [
  {
    title: 'Hello, Fren !',
    description: 'Welcome to Soundren, This is one small step for your giant leap toward a whole lot opportunities in Music Industry',
    actionText: 'I’m in !',
    actionPosition: 'center',
    image: require('../assets/images/coachmarkHelloFren.png'),
    containerStyle: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    position: {
      paddingHorizontal: WP4,
      width: WP90
    }
  },
  {
    title: 'Home',
    description: <Text centered size='mini'>Posts from the people you follow will appear here and you also can <Text
      size='mini' weight={500}
                                                                                                               >discover</Text> popular posts from a bunch of cool people !</Text>,
    actionText: 'Next',
    image: require('../assets/images/coachmarkTimeline.png'),
    containerStyle: {
      justifyContent: 'flex-end',
      alignItems: 'flex-start',
      paddingBottom: HP10
    },
    position: {
      paddingHorizontal: WP4,
      width: WP90
    },
    arrow: {
      width: 0,
      height: 0,
      marginLeft: WP15,
      borderLeftWidth: WP5,
      borderRightWidth: WP5,
      borderBottomWidth: WP5,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: WHITE,
      borderRightColor: 'transparent',
      borderBottomColor: 'transparent'
    }
  },
  {
    title: 'Advanced Search',
    // description: 'Need collaboration partner ? you can search it based on name, location, profession and interest',
    description: 'Find everything you need here; People, podcast, activities and etc.',
    actionText: 'Next',
    image: require('../assets/images/coachmarkSearch.png'),
    containerStyle: {
      justifyContent: 'flex-end',
      alignItems: 'flex-start',
      paddingBottom: HP10
    },
    position: {
      paddingHorizontal: WP4,
      width: WP90
    },
    arrow: {
      width: 0,
      height: 0,
      marginLeft: WP15,
      borderLeftWidth: WP5,
      borderRightWidth: WP5,
      borderBottomWidth: WP5,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: WHITE,
      borderBottomColor: 'transparent'
    }
  },
  {
    title: 'Explore',
    description: (<Text centered size='mini'>Explore more opportunities in Soundfren such as <Text size='mini' weight={500}>Collaboration, Artist Spotlight, News, Event, Band/Solo Audition</Text> !</Text>),
    actionText: 'Next',
    image: require('../assets/images/coachmarkExplore.png'),
    containerStyle: {
      justifyContent: 'flex-end',
      alignItems: 'center',
      paddingBottom: HP10
    },
    position: {
      paddingHorizontal: WP4,
      width: WP90
    },
    arrow: {
      width: 0,
      height: 0,
      marginLeft: -WP5,
      marginTop: -2,
      borderLeftWidth: WP2,
      borderRightWidth: WP2,
      borderBottomWidth: WP5,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: 'transparent',
      borderBottomColor: WHITE,
      transform: [
        {
          rotate: '180deg'
        }
      ]
    }
  },
  {
    title: 'Profile',
    description: 'You can update your experience, interest, song, video, and picture here. Complete your profile to get people to know you more',
    actionText: 'Ok, got it !',
    image: require('../assets/images/coachmarkProfile.png'),
    containerStyle: {
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      paddingBottom: HP10
    },
    position: {
      paddingHorizontal: WP4,
      width: WP90
    },
    arrow: {
      width: 0,
      height: 0,
      justifyContent: 'flex-end',
      marginRight: WP15,
      borderLeftWidth: WP5,
      borderRightWidth: WP5,
      borderBottomWidth: WP5,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: WHITE,
      borderBottomColor: 'transparent'
    }
  }
]

export const COACHMARK_STEP_V2 = [
  {
    title: 'Welcome to Soundfren!',
    description: 'Here’s a few steps you might find useful',
    actionText: 'Start now',
    image: require('../assets/icons/exploreActive.png'),
    imageSize: WP20,
    type: 'white',
    positionText: 'center',
    containerStyle: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    position: {
      paddingHorizontal: WP9,
      width: '100%'
    },
    isFirst: true
  },
  {
    title: 'Timeline',
    description: 'Share what’s on your mind and\nsee what others are talking about.',
    actionText: 'Next',
    image: require('../assets/images/coachmarkTimelineV2.png'),
    imageRatio: 113/107,
    imageStyle: {
      position: 'absolute',
      top: -WP2,
      right: -(WP17+WP105)
    },
    type: 'blue',
    positionText: 'flex-start',
    containerStyle: {
      justifyContent: 'flex-end',
      alignItems: 'flex-start',
      paddingBottom: HP10
    },
    position: {
      paddingHorizontal: WP6,
      width: '100%'
    },
    arrowPosition: 'bottom-left',
    arrow: {
      width: 0,
      height: 0,
      marginLeft: WP6,
      borderLeftWidth: WP10,
      borderRightWidth: WP6,
      borderBottomWidth: WP6,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: BLUE_LIGHT,
      borderRightColor: 'transparent',
      borderBottomColor: 'transparent'
    }
  },
  {
    title: 'Create Post',
    description: 'Post something to interact with others',
    actionText: 'Next',
    image: require('../assets/images/coachmarkCreatePostV2.png'),
    imageRatio: 110/110,
    imageSize: WP35,
    imageStyle: {
      position: 'absolute',
      top: -WP205,
      right: -(WP12+WP1)
    },
    type: 'white',
    positionText: 'flex-start',
    position: {
      paddingHorizontal: WP2,
      width: '100%'
    },
    arrowPosition: 'bottom-right',
    arrow: {
      width: 0,
      height: 0,
      marginRight: WP2,
      marginBottom: WP6,
      borderRightWidth: WP10,
      borderLeftWidth: WP6,
      borderBottomWidth: WP6,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: ICE_BLUE,
      borderBottomColor: 'transparent'
    }
  },
  {
    title: 'Message',
    description: 'Reach out to others by sending\nmessage here',
    actionText: 'Next',
    image: require('../assets/images/coachmarkMessageV2.png'),
    imageRatio: 122/122,
    imageStyle: {
      position: 'absolute',
      top: -WP3,
      right: -WP8
    },
    type: 'blue',
    positionText: 'flex-start',
    position: {
      paddingHorizontal: WP2,
      width: '100%'
    },
    arrowPosition: 'top-right',
    arrow: {
      width: 0,
      height: 0,
      marginRight: WP2,
      marginTop: HP8,
      borderRightWidth: WP10,
      borderRighttWidth: WP6,
      borderTopWidth: WP6,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: BLUE_LIGHT,
      borderTopColor: 'transparent'
    }
  },
  {
    title: 'Search',
    description: 'Find people, activities, podcasts, events,\nnews and all here..',
    actionText: 'Next',
    image: require('../assets/images/coachmarkSearchV2.png'),
    imageRatio: 106/106,
    imageStyle: {
      position: 'absolute',
      top: -WP1,
      right: -(WP9)
    },
    type: 'blue',
    positionText: 'flex-start',
    containerStyle: {
      justifyContent: 'flex-end',
      alignItems: 'flex-start',
      paddingBottom: HP10
    },
    position: {
      paddingHorizontal: WP6,
      width: '100%'
    },
    arrow: {
      width: 0,
      height: 0,
      marginLeft: WP24,
      borderLeftWidth: WP6,
      borderRightWidth: WP6,
      borderTopWidth: WP6,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: 'transparent',
      borderTopColor: BLUE_LIGHT
    }
  },
  {
    title: 'Explore',
    description: 'Everything you need to Connect, Collaborate, and Interact with others',
    actionText: 'Next',
    image: require('../assets/images/coachmarkExploreV2.png'),
    imageSize: WP35,
    imageRatio: 143/107,
    imageStyle: {
      position: 'absolute',
      top: -WP105,
      right: -(WP12-WP05)
    },
    type: 'blue',
    positionText: 'flex-start',
    containerStyle: {
      justifyContent: 'flex-end',
      alignItems: 'center',
      paddingBottom: HP10
    },
    position: {
      paddingHorizontal: WP6,
      width: '100%'
    },
    arrow: {
      width: 0,
      height: 0,
      marginLeft: 0,
      borderLeftWidth: WP6,
      borderRightWidth: WP6,
      borderTopWidth: WP6,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: 'transparent',
      borderTopColor: BLUE_LIGHT
    }
  },
  {
    title: 'Dashboard',
    description: 'Stay notified and\ntrack your activities here',
    actionText: 'Next',
    image: require('../assets/images/coachmarkNotificationV2.png'),
    imageRatio: 109/103,
    imageSize: WP35,
    imageStyle: {
      position: 'absolute',
      top: -WP05,
      right: -(WP502)
    },
    type: 'blue',
    positionText: 'flex-start',
    containerStyle: {
      justifyContent: 'flex-end',
      alignItems: 'center',
      paddingBottom: HP10
    },
    position: {
      paddingHorizontal: WP6,
      width: '100%'
    },
    arrow: {
      width: 0,
      height: 0,
      marginLeft: WP35+WP6,
      borderLeftWidth: WP6,
      borderRightWidth: WP6,
      borderTopWidth: WP6,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: 'transparent',
      borderTopColor: BLUE_LIGHT
    }
  },
  {
    title: 'Profile',
    description: 'Complete your profile now and\nstart exploring Soundfren. Enjoy!',
    actionText: 'Done',
    image: require('../assets/images/coachmarkProfileV2.png'),
    imageRatio: 103/103,
    imageSize: WP35,
    imageStyle: {
      position: 'absolute',
      top: -WP05,
      right: -(WP12+WP1)
    },
    type: 'blue',
    positionText: 'flex-start',
    containerStyle: {
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      paddingBottom: HP10
    },
    position: {
      paddingHorizontal: WP6,
      width: '100%'
    },
    arrowPosition: 'bottom-right',
    arrow: {
      width: 0,
      height: 0,
      marginRight: WP6,
      borderRightWidth: WP10,
      borderLeftWidth: WP6,
      borderBottomWidth: WP6,
      borderStyle: 'solid',
      backgroundColor: 'transparent',
      borderLeftColor: 'transparent',
      borderRightColor: BLUE_LIGHT,
      borderBottomColor: 'transparent'
    }
  }
]

export const DEFAULT_PAGING = {
  start: 0,
  limit: 100
}

export const SUGGESTION_PAGING = {
  start: 0,
  limit: 15
}

export const FOLLOW_PAGING = {
  start: 0,
  limit: 15
}

