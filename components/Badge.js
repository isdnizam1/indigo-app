import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import Text from 'sf-components/Text'
import { WP1, WP2 } from 'sf-constants/Sizes'
import { PALE_BLUE } from 'sf-constants/Colors'

class Badge extends PureComponent {
  static propTypes = {
    style: PropTypes.any,
    color: PropTypes.any,
    textColor: PropTypes.any,
    textSize: PropTypes.any,
    textWeight: PropTypes.any,
    radius: PropTypes.number,
    padding: PropTypes.any,
  };

  static defaultProps = {
    style: {},
    color: PALE_BLUE,
    radius: WP2,
    textColor: null,
    textSize: 'mini',
    textWeight: 400,
    padding: [WP1, WP2], // [Vertical, Horizontal]
  };

  render() {
    const {
      style,
      radius: borderRadius,
      color: backgroundColor,
      textColor: color,
      textSize: size,
      textWeight: weight,
      padding,
      children,
    } = this.props
    const [paddingVertical, paddingHorizontal] = padding
    return children ? (
      <View
        style={[
          {
            borderRadius,
            backgroundColor,
            paddingVertical,
            paddingHorizontal,
          },
          style,
        ]}
      >
        <Text
          centered
          weight={weight}
          size={size}
          color={color}
          type={'Circular'}
        >
          {children}
        </Text>
      </View>
    ) : (<View />)
  }
}

export default Badge
