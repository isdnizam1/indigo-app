import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import { isFunction } from 'lodash-es'
import * as Animatable from 'react-native-animatable'
import { Ionicons, AntDesign, Feather, FontAwesome, MaterialIcons, Entypo, EvilIcons, MaterialCommunityIcons, FontAwesome5 } from '@expo/vector-icons'
import { GREY, GREY80, PALE_LIGHT_BLUE_TWO } from '../constants/Colors'
import { ICON_SIZE, WP1, WP100, WP4, WP5 } from '../constants/Sizes'
import { TOUCH_OPACITY } from '../constants/Styles'

const icons = {
  Ionicons,
  AntDesign,
  Feather,
  FontAwesome,
  MaterialIcons,
  Entypo,
  EvilIcons,
  MaterialCommunityIcons,
  FontAwesome5
}
const propsType = {
  key: PropTypes.any,
  type: PropTypes.oneOf(['AntDesign', 'Feather', 'FontAwesome', 'MaterialIcons', 'Entypo', 'EvilIcons', 'MaterialCommunityIcons', 'Ionicons', 'FontAwesome5']),
  name: PropTypes.string.isRequired,
  size: PropTypes.any,
  padding: PropTypes.any,
  centered: PropTypes.bool,
  marginRight: PropTypes.bool,
  color: PropTypes.string,
  onPress: PropTypes.func,
  onLongPress: PropTypes.func,
  style: PropTypes.object,
  background: PropTypes.oneOf(['dark-circle', 'none', 'pale-light-circle']),
  backgroundColor: PropTypes.string
}

const propsDefault = {
  key: undefined,
  type: 'AntDesign',
  size: 'small',
  color: GREY,
  centered: false,
  background: 'none',
  marginRight: false,
}

const BACKGROUND = {
  'dark-circle': {
    paddingRight: WP1,
    paddingLeft: WP1,
    paddingVertical: WP1,
    backgroundColor: GREY80,
    borderRadius: 100,
  },
  'pale-light-circle': {
    backgroundColor: PALE_LIGHT_BLUE_TWO,
    borderRadius: WP100,
  },
  'none': {}
}

const PADDED = {
  'dark-circle': WP1 * 2,
  'pale-light-circle': WP5,
  'none': 0
}

const Icon = (props) => {
  const {
    type,
    name,
    color,
    size,
    centered,
    onPress,
    onLongPress,
    style,
    marginRight,
    background,
    backgroundColor
  } = props
  const CurrentIcon = Animatable.createAnimatableComponent(icons[type])
  const currentSize = ICON_SIZE[size] || size
  return (
    <TouchableOpacity
      activeOpacity={TOUCH_OPACITY}
      onPress={onPress}
      onLongPress={onLongPress}
      disabled={!isFunction(onPress) && !isFunction(onLongPress)}
      style={[{
        width: currentSize + PADDED[background], height: currentSize + PADDED[background],
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: centered ? 'center' : 'flex-start',
        marginRight: marginRight ? WP4 : 0,
      }, BACKGROUND[background], { backgroundColor }, style]}
    >
      <CurrentIcon name={name} size={currentSize} style={{ color }} />
    </TouchableOpacity>
  )
}

Icon.propTypes = propsType
Icon.defaultProps = propsDefault
export default Icon
