import React from 'react'
import { View, StyleSheet, Animated, Easing } from 'react-native'
import { WP100, WP05, WP4 } from 'sf-constants/Sizes'
import { REDDISH, WHITE } from 'sf-constants/Colors'
import { connect } from 'react-redux'
import {
  setProgressComponent,
  setProgressComplete,
} from 'sf-services/helper/actionDispatcher'

const style = StyleSheet.create({
  anim: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: WP100,
  },
  timelineAnim: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    width: WP100,
    marginLeft: -WP4,
    height: '100%',
    backgroundColor: WHITE,
    justifyContent: 'flex-end',
  },
})

class ProgressAnim extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      progress: new Animated.Value(0),
      fade: new Animated.Value(0),
    }
  }

  componentDidUpdate(prevProps, prevState) {
    !!this.props.progressComponent &&
      Animated.timing(this.state.fade, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      }).start(() => {
        Animated.timing(this.state.progress, {
          toValue: WP100,
          duration: 1000,
          easing: Easing.ease,
          useNativeDriver: false
        }).start()
      })
    !prevProps.progressComplete &&
      this.props.progressComplete &&
      setTimeout(() => {
        Animated.timing(this.state.fade, {
          toValue: 0,
          duration: 500,
          delay: 1750,
          useNativeDriver: true
        }).start(() => {
          this.props.setProgressComponent(null)
          this.props.setProgressComplete(false)
        })
      }, 1500)
  }

  render() {
    const { isTimeline, height, progressComponent } = this.props
    return progressComponent ? (
      <Animated.View
        style={[
          isTimeline ? style.timelineAnim : style.anim,
          {
            height,
            opacity: this.state.fade,
          },
        ]}
      >
        {progressComponent}
        <Animated.View
          style={{
            height: WP05,
            backgroundColor: REDDISH,
            width: this.state.progress,
          }}
        />
      </Animated.View>
    ) : (
      <View />
    )
  }
}

const mapStateToProps = ({
  helper: { progressComponent, progressComplete },
}) => ({ progressComponent, progressComplete })

export default connect(mapStateToProps, {
  setProgressComponent,
  setProgressComplete,
})(ProgressAnim)
