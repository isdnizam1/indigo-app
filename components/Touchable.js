import React from 'react'
import { TouchableNativeFeedback, TouchableOpacity, Platform, View } from 'react-native'
import { omit, pick } from 'lodash-es'
import { TOUCH_OPACITY } from 'sf-constants/Styles'

/* eslint-disable */

export default Platform.select({
  ios: (props) => {
    return (
      <TouchableOpacity
        {...omit(props, ['activeOpacity', 'children'])}
        activeOpacity={TOUCH_OPACITY}
      >
        {props.children}
      </TouchableOpacity>
    );
  },
  android: (props) => {
    const { disabled } = props;
    const TouchElement = disabled ? View : TouchableNativeFeedback;
    return (
      <TouchElement {...pick(props, disabled ? ['style'] : ['onPress', 'useForeground'])}>
        <View
          {...omit(props, [
            'children',
            'onPress',
            'useForeground',
            'activeOpacity',
            disabled ? 'style' : null,
          ])}
        >
          {props.children}
        </View>
      </TouchElement>
    );
  },
});
