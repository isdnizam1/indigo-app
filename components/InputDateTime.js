import React, { Component } from 'react'
import PropTypes from 'prop-types'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { View, TouchableOpacity } from 'react-native'

class InputDateTime extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isDateTimePickerVisible: false,
      selectedDate: this.props.date
    }
  }

  _showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true })
  };

  _hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false })
  };

  _handleDatePicked = (date) => {
    this.props.onChange(date)
    this._hideDateTimePicker()
  };

  render() {
    const {
      isDateTimePickerVisible,
      selectedDate
    } = this.state

    const {
      children,
      mode,
      minimumDate,
    } = this.props

    return (
      <View>
        <DateTimePicker
          isVisible={isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          mode={mode}
          date={selectedDate}
          minimumDate={minimumDate}
        />
        <TouchableOpacity onPress={this._showDateTimePicker}>
          { children }
        </TouchableOpacity>
      </View>
    )
  }
}

InputDateTime.propTypes = {
  children: PropTypes.any,
  onChange: PropTypes.func,
  mode: PropTypes.string, //[date, time, datetime]
  date: PropTypes.objectOf(PropTypes.any),
  minimumDate: PropTypes.objectOf(PropTypes.any)
}

InputDateTime.defaultProps = {
  children: () => {},
  onChange: () => {},
  mode: 'date',
  date: new Date(),
  minimumDate: undefined
}

export default InputDateTime
