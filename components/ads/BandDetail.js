import React, { Component } from 'react'
import { View, TouchableOpacity, Slider } from 'react-native'
import HTMLElement from 'react-native-render-html'
import { isEmpty } from 'lodash-es'
import { HP1, WP1, WP15, WP2, WP3, WP6 } from '../../constants/Sizes'
import Text from '../Text'
import Card from '../Card'
import Image from '../Image'
import Player from '../Player'
import { GREY20, GREY50, ORANGE, WHITE, SILVER_CALMER, TOMATO, GREY_CALM_SEMI } from '../../constants/Colors'
import { getUserIdFromLink, NavigateToInternalBrowserNoTab } from '../../utils/helper'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { Button, ProfileCardWork } from '../index'
import { adConstant } from './AdsConstant'
import AdsBandVideoItem from './AdsBandVideoItem'

const SOCIAL_MEDIA_IMAGES = {
  'facebook': require('../../assets/icons/social_media/facebook.png'),
  'instagram': require('../../assets/icons/social_media/instagram.png'),
  'twitter': require('../../assets/icons/social_media/twitter.png'),
  'youtube': require('../../assets/icons/social_media/youtube.png')
}

class BandDetail extends Component {
  _getGenres = (genres) => {
    let dataToDisplay
    if (genres) {
      dataToDisplay = genres.reduce((accum, item) => (
        `${accum}${item.name}, `
      ), '')
    } else {
      dataToDisplay = ''
    }

    return (
      <Text color={adConstant.FONT_COLOR} size={adConstant.TEXT_FONT_SIZE}>
        {dataToDisplay.substring(0, dataToDisplay.length - 2)}
      </Text>
    )
  }

  _getMembers = (members, navigateTo) => {
    return (
      <View style={{
        flexDirection: 'row',
        flex: 1,
        flexWrap: 'wrap'
      }}
      >
        {
          members.map((item, index) => {
            let onPress = () => { }
            if (item.link) onPress = () => navigateTo('ProfileScreen', { idUser: getUserIdFromLink(item.link) })

            return (
              <Text key={`member-${index}`} onPress={onPress} color={ORANGE} size={adConstant.TEXT_FONT_SIZE}>
                {item.name}
                {index !== members.length - 1 && ', '}
              </Text>
            )
          })
        }
      </View>
    )
  }

  _getLocation = (location) => {
    return (
      <View style={{
        flexDirection: 'row'
      }}
      >
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY} onPress={() => {
            NavigateToInternalBrowserNoTab({
              url: location.link
            })
          }}
        >
          <Text size={adConstant.TEXT_FONT_SIZE}>{location.name}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _detailSection = (label, content) => (
    <View style={{
      flexDirection: 'row',
      flex: 1
    }}
    >
      <Text style={{ flex: 0.3 }} size={adConstant.TEXT_FONT_SIZE} color={adConstant.FONT_COLOR} weight={400}>
        {`${label}`}
      </Text>
      <Text size={adConstant.TEXT_FONT_SIZE} color={adConstant.FONT_COLOR} weight={400}>: </Text>
      <View style={{ flex: 0.7 }}>
        {content}
      </View>
    </View>
  )

  _socMedSection = (socMeds) => {
    return (
      <View style={{ paddingTop: WP3, flexDirection: 'row' }}>
        {
          socMeds.map((item, index) => {
            return (
              <Image
                key={`socmed-${index}`}
                size='mini'
                style={[
                  { marginHorizontal: WP3 },
                  index === 0 && { marginLeft: 0 }
                ]}
                source={SOCIAL_MEDIA_IMAGES[item.name]}
                onPress={() => {
                  NavigateToInternalBrowserNoTab({
                    url: item.link
                  })
                }}
              />
            )
          })
        }
      </View>
    )
  }

  _renderLoadingButtonPlay = () => (
    <Image
      source={require('../../assets/loading_music.gif')}
      size='mini'
      style={{ marginVertical: 0, flexGrow: 0, marginRight: WP3, padding: WP1 }}
    />
  )

  _renderButtonPlay = ({
    isLoading,
    toggle,
    isPlaying,
    isSameSong,
    changeAndPlay,
    url
  }) => {
    if (isLoading && isSameSong) return this._renderLoadingButtonPlay()
    return (
      <Button
        style={{ marginVertical: 0, flexGrow: 0, marginRight: WP3 }}
        toggle
        toggleActiveColor={SILVER_CALMER}
        toggleInactiveTextColor={TOMATO}
        backgroundColor={TOMATO}
        contentStyle={{ padding: WP1 }}
        iconName={(isPlaying && isSameSong) ? 'pause' : 'play'}
        iconType='MaterialCommunityIcons'
        iconColor={TOMATO}
        onPress={() => {
          if (isSameSong) toggle()
          else {
            changeAndPlay(url)
          }
        }}
        rounded
        shadow='none'
      />
    )
  }

  _renderSongItem = (playerOpts) => ({ data, renderAction, activeId }) => {
    const url = data.link
    const icon = data.image
    const { toggle, isPlaying, playbackInstancePosition, playbackInstanceDuration,
      onChangeMillis, toggleSliding, changeAndPlay, songUrl, isLoading } = playerOpts
    const isSameSong = url === songUrl
    return (
      <TouchableOpacity
        disabled={data.in_app_player === 1}
        onPress={async () => {
          NavigateToInternalBrowserNoTab({
            url
          })
        }} activeOpacity={TOUCH_OPACITY}
        style={{ padding: WP6 }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          {
            data.in_app_player === 1 ?
              this._renderButtonPlay({
                isLoading,
                toggle,
                isPlaying,
                isSameSong,
                changeAndPlay,
                url
              })
              :
              <Image size='xsmall' source={{ uri: icon }} style={{ marginVertical: 0, marginRight: WP2 }} />
          }
          <View style={{ paddingHorizontal: WP2, marginRight: WP15 }}>
            <Text weight={400} size='mini' numberOfLines={1}>{data.name}</Text>
          </View>
          {
            renderAction()
          }
        </View>
        {
          activeId === url && (
            <View style={{ paddingTop: HP1 }}>
              {
                data.in_app_player === 1 && (
                  <Slider
                    style={{ flexGrow: 1, height: 40 }}
                    onSlidingComplete={(value) => {
                      onChangeMillis(value)
                      toggleSliding(false)
                    }}
                    onValueChange={() => toggleSliding(true)}
                    minimumValue={0}
                    maximumValue={playbackInstanceDuration}
                    value={playbackInstancePosition}
                    minimumTrackTintColor={TOMATO}
                    maximumTrackTintColor={GREY_CALM_SEMI}
                  />
                )
              }
              {
                data.description
                  ? <Text color={GREY50} size='tiny'>{data.description}</Text>
                  : <Text color={GREY20} size='tiny'>No description</Text>
              }
            </View>
          )
        }
      </TouchableOpacity>
    )
  }

  render() {
    const {
      ads,
      onLinkPress,
      navigateTo
    } = this.props
    const additionalData = JSON.parse(ads.additional_data)
    return (
      <View>
        <Card first>
          <Text weight={500} size='massive'>{ads.title}</Text>
          <View style={{ marginTop: WP3 }}>
            {this._detailSection('Genres', this._getGenres(additionalData.genre))}
            {this._detailSection('Location', this._getLocation(additionalData.location))}
            {this._detailSection('Created By', (
              <Text
                size='mini' color={ORANGE}
                style={{ textDecorationLine: 'underline' }}
                onPress={() => navigateTo('ProfileScreen', { idUser: ads.id_user })}
              >
                {ads.created_by}
              </Text>)
            )}
          </View>
        </Card>
        <Card>
          {this._detailSection('Members', this._getMembers(additionalData.members, navigateTo))}
        </Card>
        <View style={{ backgroundColor: WHITE, paddingBottom: HP1 }}>
          <Player>
            {
              (playerOpts) => {
                return (
                  <ProfileCardWork
                    header='Songs'
                    category='Song'
                    isMine={false}
                    dataSource={additionalData.song}
                    idPerItem='link'
                    navigateTo={navigateTo}
                    renderContent={this._renderSongItem(playerOpts)}
                  />
                )
              }
            }
          </Player>
        </View>
        {
          !isEmpty(additionalData.video) && additionalData.video?.length > 0 && (
            <Card>
              <Text weight={400}>Video</Text>
              <View style={{ paddingTop: WP2 }}>
                {
                  additionalData.video.map((item, index) => {
                    return (
                      <TouchableOpacity
                        activeOpacity={TOUCH_OPACITY}
                        key={index}
                        onPress={() => NavigateToInternalBrowserNoTab({ url: item.link })}
                      >
                        <AdsBandVideoItem item={item} />
                      </TouchableOpacity>
                    )
                  })
                }
              </View>
            </Card>
          )
        }
        <Card>
          <Text weight={400}>{`About ${ads.title}`}</Text>
          <HTMLElement
            containerStyle={{ marginTop: WP3 }}
            html={ads.description.replace(/(\r\n|\n|\r|)/gm, '')}
            baseFontStyle={{
              fontFamily: 'OpenSansRegular',
              fontSize: adConstant.HTML_FONT_SIZE,
              color: adConstant.FONT_COLOR
            }}
            onLinkPress={onLinkPress}
          />
        </Card>
        <Card>
          <Text weight={400}>Find Us on</Text>
          {this._socMedSection(additionalData.social_media)}
        </Card>
      </View>
    )
  }
}

BandDetail.propTypes = {}

BandDetail.defaultProps = {}

export default BandDetail
