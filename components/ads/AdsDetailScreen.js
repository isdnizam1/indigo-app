import React from 'react'
import { View, ImageBackground, TouchableOpacity,Linking } from 'react-native'
import { connect } from 'react-redux'
import { isEmpty, noop, get } from 'lodash-es'
import moment from 'moment'
import ButtonV2 from 'sf-components/ButtonV2'
import { REDDISH } from 'sf-constants/Colors'
import { WP305 } from 'sf-constants/Sizes'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import Button from '../Button'
import Container from '../Container'
import Image from '../Image'
import Text from '../Text'
import { GREY, ORANGE, WHITE, GREY_CALM_SEMI } from '../../constants/Colors'
import {
  HP2,
  WP100,
  WP4,
  WP6,
  WP50,
  IMAGE_SIZE,
  HP50,
  HP15,
  HP1,
  WP5,
} from '../../constants/Sizes'
import { getAdsDetail, postLogCollab } from '../../actions/api'
import {
  currencyFormatter,
  initiateRoom,
  isIOS,
  isPremiumUser,
  NavigateToInternalBrowser,
  restrictedAction,
} from '../../utils/helper'
import { BORDER_STYLE, HEADER, TOUCH_OPACITY } from '../../constants/Styles'
import HeaderNormal from '../HeaderNormal'
import { BottomSheet } from '..'
import ImageComponent from '../Image'
import { paymentDispatcher } from '../../services/payment'
import NewsDetailScreen from '../../screens/news/NewsDetailScreen'
import BandDetail from './BandDetail'
import DefaultDetail from './DefaultDetail'
import VenueDetail from './VenueDetail'
import JobCollaborationDetail from './JobCollaborationDetail'
import EventDetail from './EventDetail'
import CollaborationDetail from './CollaborationDetail'
import { title } from './AdsConstant'
import AdsFeedback from './AdsFeedback'
import handleRedirectAnnouncement, {
  announcementAction,
} from './HandleRedirectAnnouncement'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const mapDispatchToProps = {
  paymentSourceSet: paymentDispatcher.paymentSourceSet,
}

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam('onRefresh', () => {}),
  idAds: getParam('id_ads', 0),
})

const mapAdDetailScreen = {
  band: BandDetail,
  venue: VenueDetail,
  job_collaboration: JobCollaborationDetail,
  event: EventDetail,
  submission: View,
  collaboration: CollaborationDetail,
}

// const mapShareTitle = {
//   band: 'Bagikan Profile',
//   event: 'Gabikan Event',
//   submission: 'Bagikan Submission',
//   collaboration: 'Bagikan Collaboration',
//   news_update: 'News Update'
// }

class AdsDetailScreen extends React.Component {
  state = {
    ads: {},
    isReady: false,
    showFeedback: true,
  };

  async componentDidMount() {
    await this._getInitialScreenData()
  }

  limitAlert = React.createRef();

  _getInitialScreenData = async (...args) => {
    await this._getAdsDetail(...args)
    await this.setState({
      isReady: true,
    })
  };

  _getAdsDetail = async (isLoading = true) => {
    const {
      dispatch,
      userData: { id_user },
      idAds: id_ads,
      navigateBack,
      navigateTo,
      popToTop,
    } = this.props
    const dataResponse = await dispatch(
      getAdsDetail,
      { id_user, id_ads },
      noop,
      false,
      isLoading,
    )
    this.setState({ ads: dataResponse }, () => {
      dataResponse.category == 'video' &&
        (() => {
          navigateBack()
          navigateTo('PremiumVideoTeaser', { id_ads: dataResponse.id_ads })
        })()
    })
    if (
      get(dataResponse, 'category') == 'submission' ||
      get(dataResponse, 'category') == 'event'
    ) {
      popToTop()
      this.props.navigation.push('SubmissionPreview', { id_ads })
    }
  };

  _onLinkPress = (evt, href, htmlAttribs) => {
    const { navigateTo } = this.props

    const splitedHref = href.split('/')

    const uriPrefix = splitedHref[0]
    const relatedId = splitedHref[splitedHref.length - 1]
    if (uriPrefix === 'user') {
      navigateTo('ProfileScreen', { idUser: relatedId })
    } else {
      NavigateToInternalBrowser({
        url: href,
      })
    }
  };

  _onSubmitFeedbackCallback = () => {
    this.setState({ showFeedback: false })
  };

  _adHeaderImage = () => {
    const { ads } = this.state

    if (ads.category === 'band') {
      const additionalData = JSON.parse(ads.additional_data)
      return isEmpty(additionalData.photos)
        ? ads.image
        : additionalData.photos[0].link
    }

    return ads.image
  };

  _onJoinCollab = async () => {
    const { dispatch, userData, idAds: id_ads } = this.props

    await this.setState({ isLoading: true })
    await dispatch(
      postLogCollab,
      { id_user: userData.id_user, id_ads },
      noop,
      true,
      true,
    )
    this.setState({ isLoading: false })
  };

  _collaborationAction(ads) {
    const { navigateTo, userData } = this.props
    const { id_user } = ads
    return (
      <View
        style={{
          width: WP100,
          paddingVertical: HP2,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          ...BORDER_STYLE['top'],
          paddingHorizontal: WP4,
        }}
      >
        <Button
          marginless
          onPress={async () => {
            await this._onJoinCollab()
            const chatRoomDetail = await initiateRoom(userData, {
              id_user,
            })
            chatRoomDetail.template = 'Hai, saya ingin collab dengan anda '
            await navigateTo('ChatRoomScreen', chatRoomDetail)
          }}
          width='100%'
          soundfren
          centered
          shadow='none'
          textColor={WHITE}
          style={{ margin: 0, marginVertical: 0 }}
          disable={userData.id_user === id_user}
          text={userData.id_user === id_user ? 'It\'s you' : 'Join to Collaboration'}
        />
      </View>
    )
  }

  _submissionAction(ads) {
    const { navigateTo, userData, paymentSourceSet } = this.props
    const additionalData = JSON.parse(ads.additional_data)
    const { internal_link, link, name } = additionalData.redirect || {}
    return (
      <>
        <BottomSheet
          backdropClose
          innerRef={this.limitAlert}
          snapPoints={[HP50, 0]}
          renderContent={({ closeBottomSheet }) => (
            <View
              style={{
                padding: WP6,
                justifyContent: 'center',
                alignItems: 'center',
                flex: 1,
              }}
            >
              <Image
                source={require('../../assets/images/limitModalImage.png')}
                imageStyle={{ height: HP15 }}
                aspectRatio={182 / 108}
              />
              <Text
                centered
                type='NeoSans'
                style={{ marginVertical: HP1 }}
                weight={600}
                size='massive'
              >
                Oops...!
              </Text>
              <Text
                centered
                type='NeoSans'
                style={{
                  paddingHorizontal: WP5,
                  marginVertical: HP1,
                }}
                size='mini'
              >
                Unfortunately, this feature is only for premium member and you can’t
                upgrade it directly on our apps. Please contact administrator for
                further information.
              </Text>
            </View>
          )}
        />
        <View
          style={{
            width: WP100,
            paddingVertical: HP2,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            ...BORDER_STYLE['top'],
            paddingHorizontal: WP4,
          }}
        >
          <Button
            marginless
            width='100%'
            onPress={async () => {
              if (Number(ads.is_premium) && !isPremiumUser(userData.account_type)) {
                if (isIOS()) {
                  this.limitAlert.current?.snapTo(0)
                } else {
                  paymentSourceSet('submission')
                  navigateTo('MembershipScreen')
                }
              } else {
                if (internal_link)
                  navigateTo(
                    'SubmissionRegisterForm',
                    { idAds: ads.id_ads },
                    'push',
                  )
                else NavigateToInternalBrowser({ url: link })
              }
            }}
            backgroundColor={ORANGE}
            centered
            bottomButton
            radius={WP4}
            shadow='none'
            textType='NeoSans'
            textSize='small'
            textColor={WHITE}
            textWeight={500}
            text={internal_link ? 'Join Submission' : name}
          />
        </View>
      </>
    )
  }

  _venueAction(ads) {
    const { navigateTo } = this.props

    const additionalData = ads.additional_data
      ? JSON.parse(ads.additional_data)
      : {}

    return (
      <View
        style={{
          width: WP100,
          paddingVertical: HP2,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          ...BORDER_STYLE['top'],
          paddingHorizontal: WP4,
        }}
      >
        <Text color={ORANGE} style={{ marginRight: WP6 }} weight={400}>
          {`${additionalData.price.currency} ${currencyFormatter(
            additionalData.price.value,
          )}`}
          <Text color={GREY} weight={300}>
            {` /${additionalData.price.time}`}
          </Text>
        </Text>
        <Button
          marginless
          onPress={() => {
            navigateTo('VenueBookingScreen', { venue: ads })
          }}
          width='100%'
          soundfren
          centered
          shadow='none'
          textColor={WHITE}
          text={additionalData.redirect.name}
        />
      </View>
    )
  }

  _eventAction(ads) {
    const additionalData = ads.additional_data
      ? JSON.parse(ads.additional_data)
      : {}

    return (
      <View
        style={{
          width: WP100,
          paddingVertical: HP2,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          ...BORDER_STYLE['top'],
          paddingHorizontal: WP4,
        }}
      >
        <Button
          marginless
          onPress={() => {
            NavigateToInternalBrowser({
              url: additionalData.redirect.link,
            })
          }}
          width='100%'
          soundfren
          centered
          shadow='none'
          textColor={WHITE}
          text={additionalData.redirect.name}
        />
      </View>
    )
  }

  _announcementAction = (ads) => {
    const action = announcementAction(ads)
    const additionalData = ads.additional_data
      ? JSON.parse(ads.additional_data)
      : {}
    const isFreeAccess = additionalData.free_access ?? false
    if (action) {
      return (
        <View
          style={{
            width: WP100,
            paddingVertical: HP2,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: WP4,
          }}
        >
          <ButtonV2
            marginless
            onPress={restrictedAction({
              action: () => handleRedirectAnnouncement(ads, this.props.navigation),
              userData: this.props.userData,
              navigation: this.props.navigation,
              isFreeAccess
            })}
            style={{ width: '100%', paddingVertical: WP305 }}
            textColor={WHITE}
            textSize={'slight'}
            color={REDDISH}
            text='Selengkapnya'
          />
        </View>
      )
    }
  };

  render() {
    const { isLoading, navigateBack, navigateTo, idAds, dispatch } = this.props
    const { ads, isReady, showFeedback } = this.state
    const DetailScreen = mapAdDetailScreen[ads.category] || DefaultDetail

    if (ads.category === 'news_update') {
      return <NewsDetailScreen id_ads={idAds} {...this.props} />
    }

    return (
      <Container
        theme='dark'
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            text={title[ads.category] || 'Title'}
            textSize={'slight'}
            textType={'Circular'}
            rightComponent={
              <ImageComponent
                source={require('../../assets/icons/icShare.png')}
                style={[HEADER.rightIcon, { opacity: 0 }]}
                size='xtiny'
                onPress={noop}
              />
            }
            centered
          />
        )}
        outsideScrollContent={() => {
          switch (ads.category) {
            case 'submission':
              return this._submissionAction(ads)
            case 'collaboration':
              return this._collaborationAction(ads)
            case 'venue':
              return this._venueAction(ads)
            case 'event':
              return this._eventAction(ads)
            case 'announcement':
              return this._announcementAction(ads)
            case 'sc_announcement':
              return this._announcementAction(ads)
            default:
              return null
          }
        }}
        scrollable
        scrollBackgroundColor={ads.category === 'band' ? GREY_CALM_SEMI : WHITE}
        scrollContentContainerStyle={{
          justifyContent: 'space-between',
        }}
      >
        <View>
          <View style={{ backgroundColor: WHITE }}>
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onPress={() =>
                navigateTo('ImagePreviewScreen', {
                  images: [{ url: this._adHeaderImage() }],
                })
              }
            >
              <ImageBackground
                style={{ height: WP50 }}
                aspectRatio={2}
                source={{ uri: this._adHeaderImage() }}
              >
                {moment().diff(moment(ads.updated_at), 'days') <= 3 && (
                  <Image
                    style={{ position: 'absolute', left: 0, top: 0 }}
                    size='large'
                    aspectRatio={58 / 21}
                    source={require('../../assets/images/labelNew2.png')}
                  />
                )}
              </ImageBackground>
            </TouchableOpacity>
            {!!ads.image && ads.category === 'band' && (
              <Image
                centered={false}
                onPress={() =>
                  navigateTo('ImagePreviewScreen', {
                    images: [{ url: ads.image }],
                  })
                }
                key={moment().unix()}
                size='extraMassive'
                source={{ uri: ads.image }}
                style={{
                  marginTop: -IMAGE_SIZE['extraMassive'] / 2,
                  paddingHorizontal: WP6,
                }}
                imageStyle={{ borderRadius: 5 }}
              />
            )}
          </View>
          <DetailScreen
            ads={ads}
            onLinkPress={this._onLinkPress}
            navigateTo={navigateTo}
          />
          {ads.category === 'news_update' && showFeedback && (
            <AdsFeedback
              ads={ads}
              dispatch={dispatch}
              onSubmitCallback={this._onSubmitFeedbackCallback}
            />
          )}
        </View>
      </Container>
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(AdsDetailScreen),
  mapFromNavigationParam,
)
