import { isEmpty } from 'lodash-es'
import { NavigateToInternalBrowserNoTab } from '../../utils/helper'
import { getNotifMappingAction } from '../notification/NotifAction'
import * as Linking from 'expo-linking'

export const announcementAction = (ads) => {
  const additionalData = JSON.parse(ads.additional_data)
  const { redirect } = additionalData
  if (redirect.internal_link === 'false' || !isEmpty(redirect.link)) {
    return true
  }
  return false
}

export const handleRedirectAnnouncement = async (ads, navigation) => {
  const additionalData = JSON.parse(ads.additional_data)
  const { redirect } = additionalData
  if (redirect.internal_link === 'false') {
    // NavigateToInternalBrowserNoTab({
    //   url: redirect.link,
    // })
     Linking.openURL(redirect.link);
  } else if (!isEmpty(redirect.link)) {
    const notifAction = await getNotifMappingAction({
      url_mobile: 'internal_link',
      url: redirect.link,
    })
    // console.log({ notifAction })
    if (notifAction) {
      navigation.push(notifAction.to, {
        ...notifAction.payload,
        key: `Announcement${Math.random()}`,
      })
      // navigateTo(notifAction.to, notifAction.payload)
    }
  } else {
    // keep silent
  }
}

export default handleRedirectAnnouncement
