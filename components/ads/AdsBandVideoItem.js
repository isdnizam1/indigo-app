import { isEmpty } from 'lodash-es'
import React, { Component } from 'react'
import { View } from 'react-native'
import { GREY_CALM_SEMI, GREY_CHAT, TOMATO, WHITE50 } from '../../constants/Colors'
import { WP11, WP2, WP30, WP4, WP40 } from '../../constants/Sizes'
import Image from '../Image'
import LinkPreviewCard from '../LinkPreviewCard'
import Text from '../Text'

class AdsBandVideoItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showFullDescription: false,
    }
  }

  render() {
    const { item } = this.props
    const { showFullDescription } = this.state
    return (
      <View key={`${Math.random()}`} style={{ flexDirection: 'row', alignItems: 'flex-start', borderTopColor: GREY_CALM_SEMI, borderTopWidth: 1, paddingTop: WP2, paddingBottom: WP4 }}>
        <View style={{ width: WP40, height: WP30, backgroundColor: GREY_CHAT, }}>
          <LinkPreviewCard
            text={item.link}
            imageOnly
            imageStyle={{ width: WP40, height: WP30, aspectRatio: 40 / 30 }}
            imageProps={{
              centered: true,
              resizeMode: 'cover'
            }}
          />
          <View style={{ flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center', backgroundColor: WHITE50, }} >
            <Image centered imageStyle={{ width: WP11, }} aspectRatio={1} source={{ uri: item.image }} />
          </View>
        </View>

        <View style={{ flex: 1, padding: WP2 }}>
          <Text weight={500} size='xmini'>{item.name}</Text>
          {
            !isEmpty(item.description) && (
              <View>
                <Text size={'xtiny'}>
                  { showFullDescription ? item.description : item.description.substring(0, 170)}
                  { item.description.length > 170 && (<Text onPress={() => this.setState({ showFullDescription: !showFullDescription })} size='tiny' color={TOMATO}>{showFullDescription ? ' read less...' : ' read more...'}</Text>)}
                </Text>
              </View>
            )
          }
        </View>
      </View>
    )
  }
}

export default AdsBandVideoItem
