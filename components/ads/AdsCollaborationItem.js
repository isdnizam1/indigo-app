import React, { Component } from 'react'
import { ScrollView, TouchableOpacity, View } from 'react-native'
import moment from 'moment'
import { isEmpty } from 'lodash-es'
import Text from '../Text'
import { WP05, WP1, WP100, WP105, WP2, WP25, WP3, WP35 } from '../../constants/Sizes'
import { GREY_WARM, ORANGE, ORANGE_TOMATO } from '../../constants/Colors'
import Image from '../Image'
import { BORDER_STYLE, BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'

class AdsCollaborationItem extends Component {
  _onPressItem = () => {
    const {
      ads,
      navigateTo
    } = this.props

    navigateTo('CollaborationPreview', { id: ads.id_ads })
  }

  render() {
    const {
      ads,
    } = this.props
    const additionalData = JSON.parse(ads.additional_data)

    return (
      <View
        style={{
          width: WP100,
          paddingHorizontal: WP2,
          marginVertical: WP3,
          flexDirection: 'row'
        }}
      >
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY} onPress={this._onPressItem}
        >
          <Image
            source={{ uri: ads.image }}
            style={{ marginHorizontal: WP1, ...BORDER_STYLE['rounded'], borderRadius: 5, flexGrow: 0 }}
            imageStyle={{ height: WP25, width: WP35, aspectRatio: 1 }}
          />

          {
            moment().diff(moment(ads.updated_at), 'days') <= 3 &&
            <Image style={{ height: WP2, position: 'absolute', left: 0, top: 0, marginHorizontal: WP1, marginVertical: WP1 }} aspectRatio={44 / 16} source={require('../../assets/images/labelNew.png')} />
          }

        </TouchableOpacity>
        <View style={{
          flexDirectionRow: 'column',
          flexGrow: 1,
          justifyContent: 'flex-start',
          flexWrap: 'wrap',
          flex: 1,
          paddingHorizontal: WP1,
          marginLeft: WP1
        }}
        >
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY} onPress={this._onPressItem}
          >
            <Text
              size='small' numberOfLines={2} ellipsizeMode='tail' weight={500}
              style={{ marginBottom: WP105 }}
            >{ads.title}</Text>
            <Text color={ORANGE_TOMATO} size='tiny' style={{ marginBottom: WP05 }}>{`${ads.day_left} days left`}</Text>
            <Text size='mini' style={{ marginBottom: WP1 }}>{additionalData?.location?.name}</Text>
          </TouchableOpacity>
          <ScrollView
            showsHorizontalScrollIndicator={false} horizontal
          >
            {
              !isEmpty(additionalData.profession) && additionalData.profession.slice(0, 4).map((item, index) => (
                <View
                  key={index}
                  style={{
                    borderRadius: WP100,
                    borderColor: index === 3 && additionalData.profession.length > 4 ? GREY_WARM : ORANGE,
                    borderWidth: BORDER_WIDTH,
                    paddingVertical: WP1,
                    paddingHorizontal: WP2,
                    flexGrow: 0,
                    marginVertical: WP1,
                    marginRight: WP2
                  }}
                >
                  <Text
                    color={index === 3 && additionalData.profession.length > 4 ? GREY_WARM : ORANGE} size='tiny' weight={400}
                    style={{ flexGrow: 0 }}
                  >{index === 3 && additionalData.profession.length > 4 ? 'More...' : item.name}</Text>
                </View>
              ))
            }
          </ScrollView>
        </View>
      </View>
    )
  }
}

AdsCollaborationItem.propTypes = {}

AdsCollaborationItem.defaultProps = {}

export default AdsCollaborationItem
