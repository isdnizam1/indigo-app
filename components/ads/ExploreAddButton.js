import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import { HP105, HP2, HP6, WP1, WP2, WP3, WP4, WP6 } from '../../constants/Sizes'
import { NO_COLOR, WHITE } from '../../constants/Colors'
import Modal from '../Modal'
import Text from '../Text'
import Image from '../Image'

const SUBMISSION_ICON = require('../../assets/icons/elementIconSubmission.png')
const COLLAB_ICON = require('../../assets/icons/elementIconCollaboration.png')
const ARTIST_ICON = require('../../assets/icons/elementArtistShowcase.png')

const styles = {
  modal: {
    margin: 0,
    position: 'absolute',
    right: WP2,
    bottom: HP6
  },
  actionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginVertical: HP105
  },
  buttonAdd: {
    position: 'absolute',
    right: WP4,
    bottom: HP2
  }
}

class ExploreAddButton extends Component {
  _toggleButton = (toggleModal) => {
    toggleModal()
  }

  _renderActionButton = (label, link, image, toggleModal, isPremium) => {
    const { navigateTo } = this.props
    return (
      <TouchableOpacity
        style={styles.actionButton}
        onPress={() => {
          this._toggleButton(toggleModal)
          navigateTo(link)
        }}
      >
        {isPremium && (
          <Image
            size={WP4}
            aspectRatio={1}
            source={require('../../assets/icons/badge.png')}
            style={{ marginRight: WP2 }}
          />
        )}
        <Text weight={500} color={WHITE}>{label}</Text>
        <Image
          source={image}
          aspectRatio={1}
          imageStyle={{
            width: 58,
            height: 58,
            marginLeft: WP3,
            marginRight: WP1
          }}
        />
      </TouchableOpacity>
    )
  }

  _renderModalContent = ({ toggleModal }) => (
    <View style={{
      alignItems: 'flex-end',
      paddingRight: WP2,
      paddingBottom: WP6
    }}
    >
      {this._renderActionButton('Submission', 'CreateSubmissionDetailForm', SUBMISSION_ICON, toggleModal)}
      {this._renderActionButton('Collaboration', 'CollaborationForm', COLLAB_ICON, toggleModal)}
      {this._renderActionButton('Artist Spotlight', 'PromoteBandScreen', ARTIST_ICON, toggleModal, true)}
      <TouchableOpacity
        style={{ marginTop: WP3 }}
        onPress={() => {
          this._toggleButton(toggleModal)
        }}
      >
        <Image
          source={require('../../assets/icons/newCloseTomato.png')}
          aspectRatio={1}
          onPress={() => {
            this._toggleButton(toggleModal)
          }}
          imageStyle={{
            width: 70,
            height: 70,
          }}
        />
      </TouchableOpacity>
    </View>
  )

  render() {
    return (
      <View>
        <Modal
          position='center'
          closeOnBackdrop={false}
          renderModalContent={this._renderModalContent}
          modalStyle={styles.modal}
          backgroundColor={NO_COLOR}
          swipeDirection={null}
        >
          {({ toggleModal, isVisible }, M) => (
            <View>
              {
                !isVisible && (
                  <TouchableOpacity
                    style={{
                      position: 'absolute',
                      right: WP4,
                      bottom: WP6
                    }}
                    onPress={() => {
                      this._toggleButton(toggleModal)
                    }}
                  >
                    <Image
                      source={require('../../assets/icons/newAddTomato.png')}
                      aspectRatio={1}
                      onPress={() => {
                        this._toggleButton(toggleModal)
                      }}
                      imageStyle={{
                        width: 70,
                        height: 70,
                      }}
                    />
                  </TouchableOpacity>
                )
              }
              {M}
            </View>
          )}
        </Modal>
      </View>
    )
  }
}

ExploreAddButton.propTypes = {}

ExploreAddButton.defaultProps = {}

export default ExploreAddButton
