import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import HTMLElement from 'react-native-render-html'
import moment from 'moment'
import { isEmpty } from 'lodash-es'
import { ORANGE } from 'sf-constants/Colors'
import Image from 'sf-components/Image'
import { getUserIdFromLink } from 'sf-utils/helper'
import Text from 'sf-components/Text'
import { HP1, WP1, WP100, WP2, WP25 } from 'sf-constants/Sizes'
import { BORDER_WIDTH, TOUCH_OPACITY } from 'sf-constants/Styles'
import { adConstant, ADSTYLE, collaborationStyle } from './AdsConstant'

class CollaborationDetail extends Component {
  render() {
    const {
      ads,
      onLinkPress,
      navigateTo
    } = this.props

    const additionalData = JSON.parse(ads.additional_data)
    const startDate = additionalData.date ? moment(additionalData.date.start).format('DD MMMM YYYY') : ''

    return (
      <>
        <View style={collaborationStyle.header}>
          <Image
            source={{ uri: ads.profile_picture }}
            size='massive'
            style={collaborationStyle.photoProfileWrapper}
            imageStyle={collaborationStyle.photoProfile}
          />

          <View style={{ width: WP25 }}/>
          <View style={collaborationStyle.headerDetail}>
            <Text weight={500} style={collaborationStyle.title} size='slight'>
              {ads.title}
            </Text>
            <Text size='tiny' style={collaborationStyle.headerText}>
              {startDate}
            </Text>
            <Text size='tiny' style={collaborationStyle.headerText}>
              {additionalData.location.name}
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <Text size='tiny'>
                {'Posted by '}
              </Text>
              <TouchableOpacity activeOpacity={TOUCH_OPACITY} onPress={() => navigateTo('ProfileScreen', { idUser: getUserIdFromLink(ads.id_user) })}>
                <Text color={ORANGE} size='tiny'>
                  {ads.created_by}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={ADSTYLE.divider}/>

        <View>
          <View style={[ADSTYLE.sectionWrapper]}>
            <Text weight={500} size={adConstant.HEADER_FONT_SIZE} style={collaborationStyle.bodyHeading}>
              {'Role Needed'}
            </Text>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
              {
                !isEmpty(additionalData.profession) && additionalData.profession.map((item, index) => (
                  <View
                    key={index}
                    style={{
                      borderRadius: WP100,
                      borderColor: ORANGE,
                      borderWidth: BORDER_WIDTH,
                      paddingVertical: WP1,
                      paddingHorizontal: WP2,
                      flexGrow: 0,
                      marginVertical: WP1,
                      marginRight: WP2
                    }}
                  >
                    <Text color={ORANGE} size='tiny' weight={400} style={{ flexGrow: 0 }}>{item.name}</Text>
                  </View>
                ))
              }
            </View>
          </View>

          <View style={[ADSTYLE.sectionWrapper]}>
            <Text weight={500} size={adConstant.HEADER_FONT_SIZE} style={collaborationStyle.bodyHeading}>
              {`About ${ads.created_by}`}
            </Text>
            <HTMLElement
              containerStyle={{ marginTop: HP1 }}
              html={ads.description.replace(/(\r\n|\n|\r|)/gm, '')}
              baseFontStyle={{ fontFamily: 'OpenSansRegular', fontSize: adConstant.HTML_FONT_SIZE }}
              onLinkPress={onLinkPress}
            />
          </View>

          <View style={[ADSTYLE.sectionWrapper]}>
            <Text weight={500} size={adConstant.HEADER_FONT_SIZE} style={collaborationStyle.bodyHeading}>
              {'Description'}
            </Text>
            <HTMLElement
              containerStyle={{ marginTop: HP1 }}
              html={additionalData.specification.replace(/(\r\n|\n|\r|)/gm, '')}
              baseFontStyle={{ fontFamily: 'OpenSansRegular', fontSize: adConstant.HTML_FONT_SIZE }}
              onLinkPress={onLinkPress}
            />
          </View>
        </View>
      </>
    )
  }
}

CollaborationDetail.propTypes = {}

CollaborationDetail.defaultProps = {}

export default CollaborationDetail
