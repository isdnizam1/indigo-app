import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import moment from 'moment'
import Text from '../Text'
import { WP1, WP100, WP105, WP2, WP3, WP35 } from '../../constants/Sizes'
import { GREY_WARM } from '../../constants/Colors'
import Image from '../Image'
import { toNormalDate } from '../../utils/date'
import { BORDER_COLOR, BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'
import Icon from '../Icon'

class AdsEventItem extends Component {
  render() {
    const {
      ads,
      navigateTo,
    } = this.props

    const additionalData = JSON.parse(ads.additional_data)

    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY} onPress={() => navigateTo('AdsDetailScreenNoTab', { id_ads: ads.id_ads })}
        style={{
          width: WP100,
          paddingHorizontal: WP2,
          marginVertical: WP3,
          flexDirection: 'row'
        }}
      >

        <Image
          source={{ uri: ads.image }}
          style={{ marginHorizontal: WP1, borderWidth: BORDER_WIDTH, borderColor: BORDER_COLOR, borderRadius: 5, flexGrow: 0 }}
          imageStyle={{ height: WP35, aspectRatio: 1 }}
        />

        {
          moment().diff(moment(ads.updated_at), 'days') <= 3 &&
          <Image style={{ height: WP2, position: 'absolute', left: 0, top: 0, marginHorizontal: WP3, marginVertical: WP1 }} aspectRatio={44/16} source={require('../../assets/images/labelNew.png')} />
        }

        <View style={{
          flexDirectionRow: 'column',
          flexGrow: 1,
          justifyContent: 'flex-start',
          flexWrap: 'wrap',
          flex: 1,
          paddingHorizontal: WP1,
          marginLeft: WP1
        }}
        >
          <Text size='mini' weight={400} style={{ marginBottom: WP105 }}>{ads.title}</Text>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Text size='mini' style={{ marginBottom: WP1 }}>{`${toNormalDate(additionalData.date.start)}`}</Text>
            <Icon centered color={GREY_WARM} type='Entypo' name='dot-single'/>
            <Text size='mini'>{additionalData.location.name}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

AdsEventItem.propTypes = {}

AdsEventItem.defaultProps = {}

export default AdsEventItem
