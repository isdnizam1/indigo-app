import React, { Component } from 'react'
import { View } from 'react-native'
import HTMLElement from 'react-native-render-html'
import { WP4, WP305, WP3 } from 'sf-constants/Sizes'
import { SHIP_GREY } from 'sf-constants/Colors'
import Spacer from 'sf-components/Spacer'
import { WP8 } from '../../constants/Sizes'
import Text from '../Text'
import { ADSTYLE } from './AdsConstant'

class DefaultDetail extends Component {
  render() {
    const {
      ads,
      onLinkPress
    } = this.props

    return (
      <View>
        <Spacer size={WP3} />

        <View style={[ADSTYLE.titleWrapper]}>
          <Text lineHeight={WP8} type={'Circular'} weight={600} size={'huge'}>
            {ads.title}
          </Text>
        </View>

        <View style={[ADSTYLE.sectionWrapper]}>
          <HTMLElement
          textSelectable
          defaultTextProps={{selectable:true}}
            html={ads.description.replace(/(\r\n|\n|\r|)/gm, '').replace(/(\s{2,})/gm, ' ').trim().replace('> ', '>')}
            baseFontStyle={{ fontFamily: 'CircularBook', fontSize: WP305, lineHeight: WP4 * 1.2, color: SHIP_GREY }}
            onLinkPress={onLinkPress}
          />
        </View>

      </View>
    )
  }
}

DefaultDetail.propTypes = {}

DefaultDetail.defaultProps = {}

export default DefaultDetail
