import React, { Component } from 'react'
import { Image as RNImage, TouchableOpacity, View } from 'react-native'
import HTMLElement from 'react-native-render-html'
import Avatar from '../Avatar'
import Text from '../Text'
import { HP05, HP1, WP3 } from '../../constants/Sizes'
import { ORANGE } from '../../constants/Colors'
import { getUserIdFromLink, NavigateToInternalBrowser } from '../../utils/helper'
import { toNormalDate } from '../../utils/date'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { adConstant, ADSTYLE } from './AdsConstant'

class EventDetail extends Component {
  _detailSection = (additionalData) => {
    return (
      <View style={{ marginBottom: 15 }}>
        <View style={[ADSTYLE.detailWrapper]}>
          <View style={[ADSTYLE.detailIconWrapper]}>
            <RNImage source={require('../../assets/icons/iconDate.png')} style={[ADSTYLE.detailIcon]}/>
          </View>
          <Text
            size={adConstant.TEXT_FONT_SIZE}
            color={adConstant.FONT_COLOR}
          >{`${toNormalDate(additionalData.date.start)} - ${toNormalDate(additionalData.date.end)}`}</Text>
        </View>

        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY} onPress={() => {
            NavigateToInternalBrowser({
              url: additionalData.location.link
            })
          }} style={[ADSTYLE.detailWrapper]}
        >
          <View style={[ADSTYLE.detailIconWrapper]}>
            <RNImage source={require('../../assets/icons/location.png')} style={[ADSTYLE.detailIcon]}/>
          </View>
          <Text size={adConstant.TEXT_FONT_SIZE} color={adConstant.FONT_COLOR}>{additionalData.location.name}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    const {
      ads,
      onLinkPress,
      navigateTo
    } = this.props

    const additionalData = JSON.parse(ads.additional_data)

    return (
      <View>

        <View style={[ADSTYLE.sectionWrapper, { marginBottom: 0, paddingBottom: HP05 }]}>
          <Text weight={500} size={adConstant.TITLE_FONT_SIZE} style={ADSTYLE.title}>
            {ads.title}
          </Text>
          {this._detailSection(additionalData)}
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <Avatar
              image={additionalData.requested_by.image} size='tiny'
              onPress={() => {
                navigateTo('ProfileScreen', { idUser: getUserIdFromLink(additionalData.requested_by.link) }, 'push')
              }}
              imageStyle={{ marginRight: WP3 }}
            />
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <Text size={adConstant.TEXT_FONT_SIZE} color={adConstant.FONT_COLOR}>{'Requested by '}</Text>
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() => navigateTo('ProfileScreen', { idUser: getUserIdFromLink(additionalData.requested_by.link) })}
              >
                <Text color={ORANGE} size={adConstant.TEXT_FONT_SIZE}>{additionalData.requested_by.name}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={[ADSTYLE.sectionWrapper]}>
          <HTMLElement
            containerStyle={{ marginTop: HP1 }}
            html={ads.description.replace(/(\r\n|\n|\r|)/gm, '')}
            baseFontStyle={{ fontFamily: 'OpenSansRegular', fontSize: adConstant.HTML_FONT_SIZE }}
            onLinkPress={onLinkPress}
          />
        </View>
      </View>
    )
  }
}

EventDetail.propTypes = {}

EventDetail.defaultProps = {}

export default EventDetail
