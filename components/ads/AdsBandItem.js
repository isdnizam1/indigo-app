import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import moment from 'moment'
import Text from '../Text'
import { WP1, WP2, WP44, WP3, WP6 } from '../../constants/Sizes'
import Image from '../Image'
import { getGenre } from '../../utils/helper'
import { TOUCH_OPACITY, SHADOW_STYLE } from '../../constants/Styles'
import { GREY, GREY_CALM_SEMI } from '../../constants/Colors'

class AdsBandItem extends Component {

  shouldComponentUpdate() {
    return false
  }

  render() {
    const {
      ads,
      navigateTo
    } = this.props

    const additionalData = JSON.parse(ads.additional_data)
    const sizeItem = WP44-WP3

    return (
      <TouchableOpacity
        onPress={() => navigateTo('AdsDetailScreenNoTab', { id_ads: ads.id_ads })}
        activeOpacity={TOUCH_OPACITY}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          alignSelf: 'flex-start',
          width: sizeItem,
          marginLeft: WP6,
          marginTop: WP6,
        }}
      >
        <View style={{
          backgroundColor: GREY_CALM_SEMI,
          borderRadius: 12,
          marginBottom: WP2,
          ...SHADOW_STYLE['shadowThin']
        }}
        >
          <View style={{
            borderRadius: 12,
            overflow: 'hidden',
          }}
          >
            <Image
              source={{ uri: ads.image }}
              imageStyle={{ height: sizeItem, width: sizeItem, aspectRatio: 1 }}
            />
          </View>
          {
            moment().diff(moment(ads.updated_at), 'days') <= 3 &&
            <Image style={{ height: WP2, position: 'absolute', left: 0, top: 0, marginVertical: WP1 }} aspectRatio={44 / 16} source={require('../../assets/images/labelNew.png')} />
          }
        </View>

        <View style={{
          paddingHorizontal: WP2,
          marginBottom: WP2,
        }}
        >
          <Text centered size='tiny' color={GREY} weight={500} type='NeoSans' numberOfLines={2}>{ads.title}</Text>
          <Text centered size='xtiny' numberOfLines={2}>{getGenre(additionalData.genre)}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

AdsBandItem.propTypes = {}

AdsBandItem.defaultProps = {}

export default AdsBandItem
