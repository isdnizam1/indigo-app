import { StyleSheet } from 'react-native'
import { FONT_SIZE, HP1, HP2, WP27, WP3, WP4, WP405, WP5, WP6 } from '../../constants/Sizes'
import { GREY, MARINE, MARINE_LIGHT, PINK_PURPLE, PURPLE, SILVER_CALM, TOMATO, WHITE, BLUE_DARK_PURPLE, PURPLE_BLUE_DARKER } from '../../constants/Colors'
import { NavigateToInternalBrowser } from '../../utils/helper'
import { BORDER_STYLE } from '../../constants/Styles'
import { adBanner } from './AdsTab'

export const ADSTYLE = StyleSheet.create({
  sectionWrapper: {
    paddingHorizontal: WP6,
    paddingVertical: HP2,
    backgroundColor: WHITE,
    marginBottom: 5
  },
  detailWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 1
  },
  detailItem: {
    flexGrow: 0,
    marginRight: WP4
  },
  detailIconWrapper: {
    width: WP5
  },
  detailIcon: {},
  titleWrapper: {
    paddingHorizontal: WP6,
    paddingTop: HP2,
    backgroundColor: WHITE
  },
  title: {
    marginBottom: HP2
  },
  header: {
    marginBottom: HP1
  },
  songWrapper: {
    flex: 1, flexDirection: 'row',
    marginBottom: 4,
    alignItems: 'center',
    ...BORDER_STYLE['top'],
    paddingTop: 6,
    paddingBottom: 3
  },
  divider: {
    height: 6,
    backgroundColor: SILVER_CALM
  }
})

export const adConstant = {
  HTML_FONT_SIZE: FONT_SIZE['tiny'],
  TITLE_FONT_SIZE: 'large',
  HEADER_FONT_SIZE: 'small',
  TEXT_FONT_SIZE: 'mini',
  FONT_COLOR: GREY
}

export const banner = {
  announcement: {
    isVisible: false,
    content: '',
    label: '',
    image: null,
    colors: [PINK_PURPLE, PURPLE],
    start: [0, 0],
    end: [1, 0],
    aspectRatio: 1,
    bannerComponent: () => {
    }
  },
  band: {
    isVisible: true,
    content: 'Get the world to know about you and your work too!',
    label: 'Promote Me!',
    link: 'https://soundfrenads.typeform.com/to/SghDW7',
    image: require('../../assets/images/explore-page-more-band-exploration.png'),
    onPress: (navigateTo) => {
      navigateTo('PromoteBandScreen')
    },
    colors: [PINK_PURPLE, PURPLE],
    start: [0, 0],
    end: [1, 0],
    aspectRatio: 1,
    buttonStyle: { backgroundColor: TOMATO },
    bannerComponent: (route, navigateTo) => adBanner(route, navigateTo),
    bannerImage: require('../../assets/images/bannerArtist.png'),
    bannerImageRatio: 320/130
  },
  collaboration: {
    isVisible: true,
    content: 'Need people to collaborate or maybe talents to perform in your event?',
    label: 'Create Collaboration',
    link: 'https://soundfrenads.typeform.com/to/pKWnZX',
    image: require('../../assets/images/IllustrationCollab.png'),
    onPress: (navigateTo) => {
      navigateTo('Collaboration')
    },
    colors: [PURPLE_BLUE_DARKER, BLUE_DARK_PURPLE],
    start: [0, 1],
    end: [0, 0],
    aspectRatio: 450/275,
    buttonStyle: { backgroundColor: TOMATO },
    bannerComponent: (route, navigateTo, getAds, filters) => adBanner(route, navigateTo, getAds, filters),
    bannerImage: require('../../assets/images/bannerCollaboration.png'),
    bannerImageRatio: 320/130
  },
  venue: {
    isVisible: true,
    content: 'Do you want Soundfren to help you rent your cool venue too?',
    label: 'Rent My Venue!',
    link: 'https://soundfrenads.typeform.com/to/Onz2rx',
    image: require('../../assets/images/explore-page-more-venue.png'),
    onPress: () => {
      NavigateToInternalBrowser({
        url: 'https://soundfrenads.typeform.com/to/Onz2rx'
      })
    },
    colors: [PINK_PURPLE, PURPLE],
    start: [0, 0],
    end: [1, 0],
    aspectRatio: 1,
    bannerComponent: (route, navigateTo) => adBanner(route, navigateTo)
  },
  submission: {
    isVisible: true,
    content: 'Get the best talent for your events or business now!',
    label: 'Create Submission',
    link: '',
    image: require('../../assets/images/elementImagesIllustration.png'),
    onPress: (navigateTo) => {
      navigateTo('Submission')
    },
    colors: [MARINE, MARINE_LIGHT],
    start: [0, 1],
    end: [0, 0],
    aspectRatio: 242 / 124,
    buttonStyle: { backgroundColor: TOMATO },
    bannerComponent: (route, navigateTo) => adBanner(route, navigateTo),
    bannerImage: require('../../assets/images/bannerSubmission.png'),
    bannerImageRatio: 320/130
  }
}

export const title = {
  announcement: 'Announcement',
  band: 'Artist Spotlight',
  collaboration: 'Collaboration',
  venue: 'Venue',
  submission: 'Submission',
  event: 'Event'
}

export const collaborationStyle = StyleSheet.create({
  header: {
    paddingHorizontal: WP6,
    paddingVertical: WP3,
    flexDirection: 'row'
  },
  body: {
    paddingHorizontal: WP6,
    paddingVertical: WP4
  },
  photoProfileWrapper: {
    position: 'absolute',
    top: -(WP6),
    left: WP6
  },
  photoProfile: {
    width: WP27,
    height: WP27
  },
  title: {
    marginBottom: 3
  },
  headerDetail: {
    paddingLeft: WP405
  },
  headerText: {
    marginBottom: 3
  },
  bodyHeading: {}
})
