import React, { Component } from 'react'
import { TouchableOpacity, View, Image as RNImage } from 'react-native'
import moment from 'moment'
import Text from '../Text'
import { WP1, WP100, WP105, WP2, WP3, WP25, WP301, WP405 } from '../../constants/Sizes'
import Image from '../Image'
import { daysRemaining } from '../../utils/date'
import { BORDER_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import Button from '../Button'
import { ORANGE, BLUE_LIGHT, GREY_WARM } from '../../constants/Colors'

class AdsAuditionItem extends Component {
  render() {
    const {
      ads,
      navigateTo
    } = this.props

    const additionalData = JSON.parse(ads.additional_data)

    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY} onPress={() => navigateTo('AdsDetailScreenNoTab', { id_ads: ads.id_ads })}
        style={{
          width: WP100,
          paddingHorizontal: WP2,
          marginVertical: WP3,
          flexDirection: 'row'
        }}
      >
        <Image
          source={{ uri: ads.image }}
          style={{ marginHorizontal: WP1, ...BORDER_STYLE['rounded'], borderRadius: 4, flexGrow: 0 }}
          imageStyle={{ height: WP25, aspectRatio: 1 }}
        />

        {
          moment().diff(moment(ads.updated_at), 'days') <= 3 &&
          <Image style={{ height: WP2, position: 'absolute', left: 0, top: 0, marginHorizontal: WP3, marginVertical: WP1 }} aspectRatio={44 / 16} source={require('../../assets/images/labelNew.png')} />
        }

        <View style={{
          flexDirectionRow: 'column',
          flexGrow: 1,
          justifyContent: 'flex-start',
          flexWrap: 'wrap',
          flex: 1,
          paddingHorizontal: WP1,
        }}
        >

          <Text style={{ marginLeft: WP1, marginBottom: WP105, alignItems: 'center', }} size='small' weight={500}>
            {ads.title}
            {ads.is_premium == 1 && <Text>{' '}</Text>}
            {
              ads.is_premium == 1 && (
                <RNImage
                  source={require('../../assets/icons/badgePremium.png')}
                  style={{ height: WP405, width: WP405 }}
                />
              )
            }
          </Text>
          {
            additionalData.benefit && (
              <View style={{ marginLeft: WP1, marginBottom: WP105, alignItems: 'center', flexDirection: 'row' }}>
                <RNImage
                  source={require('../../assets/icons/icBenefits.png')}
                  style={{ height: WP301, aspectRatio: 1, marginRight: WP1 }}
                />
                <Text color={BLUE_LIGHT} size='xtiny' weight={400}>{additionalData.benefit[0]}</Text>
              </View>
            )
          }
          <Text style={{ marginLeft: WP1, marginBottom: WP1 }} color={GREY_WARM} size='tiny'>{additionalData.location.name}</Text>
          <Button
            radius={4}
            colors={[ORANGE, ORANGE]}
            compact='center'
            style={{ alignItems: 'flex-start' }}
            marginless
            toggle
            toggleActive={false}
            toggleInactiveTextColor={ORANGE}
            text={`${daysRemaining(additionalData.date.end)} Days Left`}
            textSize='xtiny'
            shadow='none'
            textWeight={300}
          />
        </View>
      </TouchableOpacity>
    )
  }
}

AdsAuditionItem.propTypes = {}

AdsAuditionItem.defaultProps = {}

export default AdsAuditionItem
