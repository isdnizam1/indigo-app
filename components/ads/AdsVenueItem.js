import React, { Component } from 'react'
import { View } from 'react-native'
import Text from '../Text'
import { WP1, WP2, WP3, WP40, WP50 } from '../../constants/Sizes'
import { GREY, ORANGE } from '../../constants/Colors'
import Image from '../Image'
import { currencyFormatter } from '../../utils/helper'
import { BORDER_STYLE } from '../../constants/Styles'

class AdsVenueItem extends Component {
  render() {
    const {
      ads,
      navigateTo,
    } = this.props

    const additionalData = JSON.parse(ads.additional_data)

    return (
      <View style={{
        padding: WP2,
        width: WP50,
        flexWrap: 'wrap',
      }}
      >
        <Image
          onPress={() => navigateTo('AdsDetailScreen', { id_ads: ads.id_ads })}
          source={{ uri: ads.image }}
          style={{ marginHorizontal: WP1, ...BORDER_STYLE['rounded'], borderRadius: 5, marginVertical: 5 }}
          imageStyle={{ height: WP40, aspectRatio: 1 }}
        />
        <View style={{
          paddingHorizontal: WP3
        }}
        >
          <Text size='mini' color={GREY} weight={400}>{ads.title}</Text>
          <Text size='tiny' color={GREY}>{additionalData.location.name}</Text>
          <Text size='mini' color={ORANGE}>
            {`${additionalData.price.currency} ${currencyFormatter(additionalData.price.value)}`}
            <Text size='mini' color={GREY}>{`/${additionalData.price.time}`}</Text>
          </Text>
        </View>
      </View>
    )
  }
}

AdsVenueItem.propTypes = {}

AdsVenueItem.defaultProps = {}

export default AdsVenueItem
