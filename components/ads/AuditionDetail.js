import React, { Component } from 'react'
import { Image as RNImage, TouchableOpacity, View } from 'react-native'
import HTMLElement from 'react-native-render-html'
import moment from 'moment'
import Text from '../Text'
import { HP1, WP502, HP05, WP100, WP1, WP2 } from '../../constants/Sizes'
import { GREY, GREY_CALM, GREY_WARM, ORANGE } from '../../constants/Colors'
import { isIOS, NavigateToInternalBrowser } from '../../utils/helper'
import { toNormalDate } from '../../utils/date'
import { TOUCH_OPACITY, BORDER_WIDTH } from '../../constants/Styles'
import Icon from '../Icon'
import Button from '../Button'
import { adConstant, ADSTYLE } from './AdsConstant'

class AuditionDetail extends Component {
  _detailSection = (additionalData) => {
    return (
      <View style={{ flexDirection: 'row', marginBottom: HP1, }}>
        <View style={[ADSTYLE.detailWrapper, ADSTYLE.detailItem]}>
          <View style={[ADSTYLE.detailIconWrapper]}>
            <RNImage source={require('../../assets/icons/iconDate.png')} style={[ADSTYLE.detailIcon]}/>
          </View>
          <Text
            size='tiny'
            color={adConstant.FONT_COLOR}
          >{`${toNormalDate(additionalData.date.start)} - ${toNormalDate(additionalData.date.end)}`}</Text>
        </View>

        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY} onPress={() => {
            NavigateToInternalBrowser({
              url: additionalData.location.link
            })
          }} style={[ADSTYLE.detailWrapper, ADSTYLE.detailItem]}
        >
          <View style={[ADSTYLE.detailIconWrapper]}>
            <RNImage source={require('../../assets/icons/location.png')} style={[ADSTYLE.detailIcon]}/>
          </View>
          <Text size='tiny' color={adConstant.FONT_COLOR}>{additionalData.location.name}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _renderListItem = (value) => (
    <View style={{ flexDirection: 'row' }}>
      <Icon centered color={GREY} size='large' type='Entypo' name='dot-single'/>
      <Text size='mini'>{value}</Text>
    </View>
  )

  _renderListGenre = (value) => (
    <View
      key={value}
      style={{
        borderRadius: WP100,
        borderColor: ORANGE,
        borderWidth: BORDER_WIDTH,
        paddingVertical: WP1,
        paddingHorizontal: WP502,
        marginVertical: WP1,
        marginRight: WP2,
        flexGrow: 0,
      }}
    >
      <Text centered color={ORANGE} size='tiny' weight={400} style={{ flexGrow: 0 }}>{value}</Text>
    </View>
  )

  render() {
    const {
      ads,
      onLinkPress,
    } = this.props

    const additionalData = JSON.parse(ads.additional_data)

    return (
      <View style={{ backgroundColor: GREY_CALM, flexGrow: 1 }}>

        <View style={[ADSTYLE.sectionWrapper]}>
          <View style={{ marginBottom: HP1, alignItems: 'flex-end' }}>
            <Text size='mini' color={GREY_WARM}>
              {moment(ads.created_at).fromNow(true)} ago
            </Text>
          </View>
          <Text weight={500} size={adConstant.TITLE_FONT_SIZE} style={[ADSTYLE.title, { alignItems: 'center', }]}>
            {ads.title}
            {ads.is_premium == 1 && <Text>{' '}</Text>}
            {
              ads.is_premium == 1 && (
                <RNImage
                  source={require('../../assets/icons/badgePremium.png')}
                  style={{ height: WP502, width: WP502 }}
                />
              )
            }
          </Text>

          {this._detailSection(additionalData)}

          <Text
            size='tiny'
            color={GREY_WARM}
            style={{ marginBottom: HP05, }}
          >
            Promoted by {ads.created_by}
          </Text>
          {
            ads.is_premium === 1 && !isIOS() && (
              <Button
                rounded
                colors={[ORANGE, ORANGE]}
                compact='center'
                style={{ alignItems: 'flex-start' }}
                marginless
                toggle
                toggleActive={false}
                toggleInactiveTextColor={ORANGE}
                ext='Submission for user premium'
                textSize='xtiny'
                shadow='none'
                textWeight={300}
              />
            )
          }
        </View>

        <View style={[ADSTYLE.sectionWrapper]}>
          <Text weight={500} size='mini' style={ADSTYLE.header}>
            {'MUSIC PREFERENCE'}
          </Text>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {
              Array.isArray(additionalData.genre) ?
                additionalData.genre.map((genre) => (
                  this._renderListGenre(genre)
                ))
                : this._renderListGenre(additionalData.genre)
            }
          </View>
        </View>

        <View style={[ADSTYLE.sectionWrapper]}>
          <Text weight={500} size='mini' style={ADSTYLE.header}>
            {'REQUIREMENTS'}
          </Text>
          {
            additionalData.requirement.map((requirement) => (
              this._renderListItem(requirement)
            ))
          }
        </View>

        <View style={[ADSTYLE.sectionWrapper]}>
          <Text weight={500} size='mini' style={ADSTYLE.header}>
            {'BENEFITS'}
          </Text>
          {
            additionalData.benefit.map((benefit) => (
              this._renderListItem(benefit)
            ))
          }
        </View>

        <View style={[ADSTYLE.sectionWrapper]}>
          <Text weight={500} size='mini' style={ADSTYLE.header}>
            {'DESCRIPTION'}
          </Text>
          <HTMLElement
            containerStyle={{}}
            html={ads.description.replace(/(\r\n|\n|\r|)/gm, '')}
            baseFontStyle={{ fontFamily: 'OpenSansRegular', fontSize: adConstant.HTML_FONT_SIZE, color: GREY }}
            onLinkPress={onLinkPress}
          />
        </View>
      </View>
    )
  }
}

AuditionDetail.propTypes = {}

AuditionDetail.defaultProps = {}

export default AuditionDetail
