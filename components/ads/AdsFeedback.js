import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import { get, noop } from 'lodash-es'
import { connect } from 'react-redux'
import Text from '../Text'
import { WP3, WP4, WP6 } from '../../constants/Sizes'
import { BLUE_LIGHT, GREY_CALM } from '../../constants/Colors'
import { postAdsFeedback } from '../../actions/api'

const styles = {
  container: {
    paddingHorizontal: WP6,
  },
  wrapper: {
    flexDirection: 'row',
    paddingHorizontal: WP4,
    paddingVertical: WP3,
    backgroundColor: GREY_CALM,
    borderRadius: 5,
    marginBottom: WP6
  },
  buttonText: {
    color: BLUE_LIGHT
  }
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

class AdsFeedback extends React.Component {

  _onSubmitFeedback = (feedback) => async () => {
    const {
      dispatch, onSubmitCallback, userData, ads
    } = this.props
    await dispatch(postAdsFeedback, {
      id_user: userData.id_user,
      value: {
        id_ads: ads.id_ads, feedback
      }
    })
    onSubmitCallback()
  }

  render() {
    const { ads } = this.props
    const isFeedback = get(ads.additional_data, 'news_feedback.is_feedback', false)
    if (isFeedback) return null
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Text size='xmini'>Was this article helpful? </Text>
          <TouchableOpacity onPress={this._onSubmitFeedback('helpful')}>
            <Text size='xmini' style={styles.buttonText}>Yes</Text>
          </TouchableOpacity>
          <Text size='xmini'> / </Text>
          <TouchableOpacity onPress={this._onSubmitFeedback('not helpful')}>
            <Text size='xmini' style={styles.buttonText}>No</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

AdsFeedback.propTypes = {
  ads: PropTypes.string,
  dispatch: PropTypes.func,
  onSubmitCallback: PropTypes.func
}

AdsFeedback.defaultProps = {
  ads: {},
  dispatch: noop,
  onSubmitCallback: noop,
}

export default connect(mapStateToProps, {})(AdsFeedback)
