import React, { Component } from 'react'
import { View, TouchableOpacity, Image as RNImage } from 'react-native'
import HTMLElement from 'react-native-render-html'
import { isEmpty } from 'lodash-es'
import { TabBar, TabView } from 'react-native-tab-view'
import Text from '../Text'
import { HP1, WP1, WP105, WP40, WP44, WP6 } from '../../constants/Sizes'
import { GREY, GREY_WARM, ORANGE_BRIGHT, WHITE } from '../../constants/Colors'
import ImageWithPreview from '../ImageWithPreview'
import { NavigateToInternalBrowser } from '../../utils/helper'
import { BORDER_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { ADSTYLE, adConstant } from './AdsConstant'

class VenueDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 0,
      tabHeight: undefined,
      routes: [
        { key: 'description', title: 'Deskripsi' },
        { key: 'gallery', title: 'Galeri' }
      ]
    }
  }

  _detailSection = (additionalData) => {
    return (
      <View>
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY} onPress={() => {
            NavigateToInternalBrowser({
              url: additionalData.location.link
            })
          }} style={[ADSTYLE.detailWrapper]}
        >
          <View style={[ADSTYLE.detailIconWrapper]}>
            <RNImage source={require('../../assets/icons/location.png')} style={[ADSTYLE.detailIcon]}/>
          </View>
          <Text size={adConstant.TEXT_FONT_SIZE} color={adConstant.FONT_COLOR}>{additionalData.location.name}</Text>
        </TouchableOpacity>

        <View style={[ADSTYLE.detailWrapper]}>
          <View style={[ADSTYLE.detailIconWrapper]}>
            <RNImage source={require('../../assets/icons/people.png')} style={[ADSTYLE.detailIcon]}/>
          </View>
          <Text
            size={adConstant.TEXT_FONT_SIZE}
            color={adConstant.FONT_COLOR}
          >{`${additionalData.capacity} orang`}</Text>
        </View>
      </View>
    )
  }

  _infoItem = (label, content, link) => {

    return (
      <View style={{ flex: 1, flexDirection: 'row', marginBottom: 3 }}>
        <Text size={adConstant.TEXT_FONT_SIZE} color={adConstant.FONT_COLOR} weight={400}>{`${label}: `}</Text>
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY} onPress={() => {
            NavigateToInternalBrowser({
              url: link
            })
          }} disabled={isEmpty(link)} style={{ paddingRight: WP6 }}
        >
          <Text
            size={adConstant.TEXT_FONT_SIZE} color={adConstant.FONT_COLOR}
            style={{ paddingRight: WP6 }}
          >{content}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _onChangeTab = (index) => {
    this.setState({
      index
    })
  }

  render() {
    const {
      ads,
      onLinkPress
    } = this.props

    const {
      tabHeight
    } = this.state

    const additionalData = JSON.parse(ads.additional_data)

    return (
      <View>
        <View style={[ADSTYLE.titleWrapper]}>
          <Text weight={500} size={adConstant.TITLE_FONT_SIZE} style={ADSTYLE.title}>
            {ads.title}
          </Text>
        </View>

        <View style={[ADSTYLE.sectionWrapper]}>
          {this._detailSection(additionalData)}
        </View>

        <View style={[ADSTYLE.sectionWrapper]}>
          <TabView
            style={{ height: tabHeight }}
            navigationState={this.state}
            renderScene={
              ({ route, jumpTo }) => {
                switch (route.key) {
                case 'description':
                  return (
                    <View>
                      <View style={[ADSTYLE.sectionWrapper]}>
                        <HTMLElement
                          containerStyle={{ marginTop: HP1 }}
                          html={ads.description}
                          baseFontStyle={{ fontFamily: 'OpenSansRegular', fontSize: adConstant.HTML_FONT_SIZE }}
                          onLinkPress={onLinkPress}
                        />
                      </View>
                      <View style={[ADSTYLE.sectionWrapper]}>
                        <Text weight={500} size={adConstant.HEADER_FONT_SIZE} style={ADSTYLE.header}>
                          {'Venue Info'}
                        </Text>
                        {this._infoItem('Lokasi', additionalData.location.name, additionalData.location.link)}
                        {this._infoItem('Alamat', additionalData.address.name, additionalData.address.link)}
                        {this._infoItem('Kapasitas', `${additionalData.capacity} orang`)}
                        {this._infoItem('Owner', additionalData.owner.name, additionalData.owner.link)}
                      </View>
                    </View>
                  )
                case 'gallery':
                  return (
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
                      {
                        additionalData.photos.map((item, index) => {
                          return (
                            <ImageWithPreview
                              source={{ uri: item.link }}
                              style={{
                                width: WP40,
                                marginBottom: WP1,
                                marginHorizontal: WP105
                              }}
                              imageStyle={{ width: WP40 }}
                              aspectRatio={1}
                              key={index}
                            />)
                        })
                      }
                      {
                        additionalData.photos.length === 0 && (
                          <Text>No Image</Text>
                        )
                      }
                    </View>
                  )
                }
              }
            }
            renderTabBar={(props) => (
              <TabBar
                {...props}
                style={[{
                  backgroundColor: WHITE, elevation: 0, ...BORDER_STYLE['top'],
                  ...BORDER_STYLE['bottom'],
                }]}
                indicatorStyle={{ backgroundColor: ORANGE_BRIGHT }}
                scrollEnabled
                tabStyle={{ width: WP44 }}
                renderLabel={({ route, focused, color }) => {
                  return (
                    <View>
                      <Text centered size='mini' color={focused ? GREY : GREY_WARM} weight={400}>
                        {route.title}
                      </Text>
                    </View>
                  )
                }}
              />
            )}
            onIndexChange={this._onChangeTab}
          />
        </View>

      </View>
    )
  }
}

VenueDetail.propTypes = {}

VenueDetail.defaultProps = {}

export default VenueDetail
