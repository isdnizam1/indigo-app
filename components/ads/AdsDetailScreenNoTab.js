import React from 'react'
import { connect } from 'react-redux'
import _enhancedNavigation from '../../navigation/_enhancedNavigation'
import AdsDetailScreen from './AdsDetailScreen'

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  onRefresh: getParam('onRefresh', () => {
  }),
  idAds: getParam('id_ads', 0)
})

class AdsDetailScreenNoTab extends React.Component {

  render() {
    return (
      <AdsDetailScreen {...this.props} />
    )
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(AdsDetailScreenNoTab),
  mapFromNavigationParam
)
