import React, { Component } from 'react'
import { TouchableOpacity, View, ScrollView } from 'react-native'
import { isEmpty } from 'lodash-es'
import Text from '../Text'
import { WP100, WP105, WP3, WP05, WP6, WP20, WP40, WP11, WP305, WP405 } from '../../constants/Sizes'
import { TOMATO, GREY_WARM, WHITE_MILK, WHITE20 } from '../../constants/Colors'
import Image from '../Image'
import { TOUCH_OPACITY, SHADOW_STYLE } from '../../constants/Styles'

class AdsVideoItem extends Component {
  _onPressItem = () => {
    const {
      ads,
      navigateTo
    } = this.props

    navigateTo('PremiumVideoTeaser', { id_ads: ads.id_ads })
  }

  render() {
    const {
      ads,
    } = this.props

    const additionalData = JSON.parse(ads.additional_data)

    return (
      <View
        style={{
          width: WP100,
          paddingHorizontal: WP6,
          marginBottom: WP3,
          flexDirection: 'row'
        }}
      >
        <TouchableOpacity
          style={{ borderRadius: 12, backgroundColor: WHITE_MILK, ...SHADOW_STYLE['shadowThin'] }}
          activeOpacity={TOUCH_OPACITY} onPress={this._onPressItem}
        >
          <View>
            <Image
              source={{ uri: ads.image }}
              style={{ borderRadius: 12, }}
              imageStyle={{ height: WP20, width: WP40, aspectRatio: 2 / 1, borderRadius: 12 }}
            />

            <View style={{ flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }} >
              <View style={{ width: WP11, height: WP11, borderRadius: WP100, backgroundColor: WHITE20, ...SHADOW_STYLE['shadow'] }}>
                <Image centered imageStyle={{ width: WP11, }} aspectRatio={1} source={require('../../assets/icons/video_player/play.png')} />
              </View>
            </View>

            {
              ads.is_premium === 1 && (
                <Image
                  style={{ position: 'absolute', left: 0, top: 0, marginLeft: WP305, marginTop: WP3, }}
                  source={require('../../assets/icons/badgePremium.png')}
                  imageStyle={{ height: WP405, aspectRatio: 1 }}
                />
              )
            }
          </View>

        </TouchableOpacity>
        <View style={{
          flexDirectionRow: 'column',
          flexGrow: 1,
          flexWrap: 'wrap',
          flex: 1,
          marginLeft: WP3
        }}
        >
          <TouchableOpacity
            style={{ flex: 1 }}
            activeOpacity={TOUCH_OPACITY} onPress={this._onPressItem}
          >
            <Text
              size='tiny' numberOfLines={2} ellipsizeMode='tail' weight={500}
              style={{ marginBottom: WP105 }}
            >{ads.title}</Text>
            <Text color={GREY_WARM} size='xtiny' style={{ marginBottom: WP05 }}>{additionalData.video_duration}</Text>
          </TouchableOpacity>
          <ScrollView
            style={{ flexGrow: 0 }}
            contentContainerStyle={{ flexGrow: 0 }}
            showsHorizontalScrollIndicator={false} horizontal
          >
            {
              !isEmpty(additionalData.category) && additionalData.category.slice(0, 4).map((item, index) => (
                <View
                  key={index}
                  style={{
                    borderRadius: WP100,
                    borderColor: index === 3 && additionalData.category.length > 4 ? GREY_WARM : TOMATO,
                    borderWidth: 1,
                    paddingVertical: WP05,
                    paddingHorizontal: WP105,
                    justifyContent: 'center',
                    flexGrow: 0,
                    marginVertical: WP05,
                    marginRight: WP105
                  }}
                >
                  <Text
                    color={index === 3 && additionalData.category.length > 4 ? GREY_WARM : TOMATO}
                    size='xtiny'
                    style={{ flexGrow: 0 }}
                  >
                    {index === 3 && additionalData.category.length > 4 ? 'More...' : item}
                  </Text>
                </View>
              ))
            }
          </ScrollView>
        </View>
      </View>
    )
  }
}

AdsVideoItem.propTypes = {}

AdsVideoItem.defaultProps = {}

export default AdsVideoItem
