import React from 'react'
import { FlatList, Image as RNImage, StyleSheet, TouchableOpacity, View, ImageBackground } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen'
import PropTypes from 'prop-types'
import { PURPLE, WHITE, TOMATO } from '../../constants/Colors'
import Loader from '../Loader'
import Text from '../Text'
import { HP25, HP3, WP100, WP105, WP3, WP30, WP6, WP60, WP10, WP2, WP8 } from '../../constants/Sizes'
import { TOUCH_OPACITY, BORDER_STYLE } from '../../constants/Styles'
import { banner } from './AdsConstant'
import AdsFilter from './AdsFilter'

export const adsTabStyle = StyleSheet.create({
  bannerWrapper: {
    width: WP100,
    paddingHorizontal: WP6,
    paddingVertical: HP3,
    minHeight: HP25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  contentWrapper: {
    width: WP60
  },
  contentText: {
    color: WHITE,
    marginBottom: WP3
  },
  button: {
    backgroundColor: PURPLE,
    borderRadius: WP100,
    paddingVertical: WP105,
    paddingHorizontal: WP6,
    alignItems: 'center',
    alignSelf: 'flex-start'
  },
  buttonLabel: {
    color: WHITE,
    fontSize: wp('3.4%')
  },

  bannerContainer: {
    flex: 1,
    paddingHorizontal: WP10,
    paddingVertical: WP8,
  },
  bannerContentWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingTop: WP3,
  },
  bannerContentText: {
    color: WHITE,
    textAlign: 'left'
  },
  bannerButton: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    backgroundColor: TOMATO,
    borderRadius: 4,
    paddingVertical: WP2,
    paddingHorizontal: WP3,
    alignItems: 'center',
    alignSelf: 'flex-start'
  },
})

export const adBanner = (route, navigateTo, getAds, filters = {}) => {
  const currentBanner = banner[route]
  return (
    <View>
      {
        currentBanner.bannerImage ? (
          <ImageBackground source={currentBanner.bannerImage} style={{ width: WP100, aspectRatio: currentBanner.bannerImageRatio }}>
            <View style={[adsTabStyle.bannerContainer]}>
              <View style={[adsTabStyle.bannerContentWrapper]}>
                <Text size='tiny' weight={500} style={[adsTabStyle.bannerContentText]}>{currentBanner.content}</Text>
                <TouchableOpacity style={[adsTabStyle.bannerButton, currentBanner.buttonStyle]} activeOpacity={TOUCH_OPACITY} onPress={() => currentBanner.onPress(navigateTo)}>
                  <Text size='tiny' weight={600} type='NeoSans' style={[adsTabStyle.buttonLabel]}>{currentBanner.label}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
        ) : (
          <LinearGradient
            colors={currentBanner.colors}
            start={currentBanner.start} end={currentBanner.end} style={[adsTabStyle.bannerWrapper]}
          >
            <View style={[adsTabStyle.contentWrapper]}>
              <Text size='tiny' weight={500} style={[adsTabStyle.contentText]}>{currentBanner.content}</Text>
              <TouchableOpacity style={[adsTabStyle.button, currentBanner.buttonStyle]} activeOpacity={TOUCH_OPACITY} onPress={() => currentBanner.onPress(navigateTo)}>
                <Text size='tiny' weight={600} type='NeoSans' style={[adsTabStyle.buttonLabel]}>{currentBanner.label}</Text>
              </TouchableOpacity>
            </View>
            <RNImage source={currentBanner.image} style={{ width: WP30, aspectRatio: currentBanner.aspectRatio }} />
          </LinearGradient>
        )
      }
      {
        route === 'collaboration' && <AdsFilter getAds={getAds} filters={filters}/>
      }
    </View>
  )
}

class AdsTab extends React.PureComponent {
  _bannerComponent = () => {
    const {
      route,
      activeRoute
    } = this.props

    if (banner[route]) {
      return (banner[route].isVisible && (activeRoute === route)) &&
        banner[route].bannerComponent(route, this.props.navigateTo, this.props.getAds, this.props.filters)
    }
  }

  render() {
    const {
      adsList,
      navigateTo,
      AdsItem,
      isTabReady,
      numColumns,
      route
    } = this.props

    if (!isTabReady) {
      return (
        <View style={{ flexGrow: 1, backgroundColor: WHITE }}>
          <Loader isLoading />
        </View>
      )
    }

    return (
      <View style={{
      }}
      >

        <FlatList
          ListHeaderComponent={this._bannerComponent()}
          contentContainerStyle={{
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'flex-start'
          }}
          ListEmptyComponent={
            <View style={{ padding: WP3, width: '100%', alignItems: 'center', ...BORDER_STYLE['bottom'] }}>
              <Text centered size='mini'>No item found</Text>
            </View>
          }
          data={adsList}
          numColumns={numColumns}
          keyExtractor={(item, index) => `key${ index}`}
          renderItem={({ item, index }) => (
            <AdsItem
              ads={item}
              key={`${index}${route}`}
              navigateTo={navigateTo}
            />
          )}
        />
      </View>
    )
  }
}

AdsTab.propTypes = {
  adsListL: PropTypes.array,
  navigateTo: PropTypes.func,
  AdsItem: PropTypes.func,
  isTabReady: PropTypes.bool,
  route: PropTypes.string,
  activeRoute: PropTypes.string,
  numColumns: PropTypes.number
}

AdsTab.defaultProps = {
  adsListL: [],
  navigateTo: () => {
  },
  AdsItem: () => {
  },
  isTabReady: true,
  route: '',
  activeRoute: '',
  numColumns: 1
}

export default AdsTab
