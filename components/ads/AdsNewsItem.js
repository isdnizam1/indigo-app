import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import moment from 'moment'
import Text from '../Text'
import { WP1, WP100, WP105, WP2, WP3, WP35, WP95, HP3 } from '../../constants/Sizes'
import Image from '../Image'
import { BORDER_STYLE, BORDER_WIDTH, TOUCH_OPACITY, BORDER_COLOR } from '../../constants/Styles'

class AdsNewsItem extends Component {
  render() {
    const {
      ads,
      index,
      navigateTo,
    } = this.props

    if (index === 0) {
      return (
        <View style={{
          marginVertical: WP105,
          padding: WP2,
          marginBottom: HP3
        }}
        >
          <Image
            centered
            onPress={() => navigateTo('NewsDetailScreen', { id_ads: ads.id_ads })}
            source={{ uri: ads.image }}
            style={{ borderWidth: BORDER_WIDTH, borderColor: BORDER_COLOR, borderRadius: 5 }}
            imageStyle={{ height: undefined, width: WP95, aspectRatio: 2.5 }}
          />

          {
            moment().diff(moment(ads.updated_at), 'days') <= 3 &&
            <Image style={{ height: WP2, position: 'absolute', left: 0, top: 0, marginHorizontal: WP2, marginVertical: WP3 }} aspectRatio={44 / 16} source={require('../../assets/images/labelNew.png')} />
          }

          <Text numberOfLines={1} ellipsizeMode='tail' size='mini' weight={400} style={{ marginVertical: WP105 }}>{ads.title}</Text>
          <Text numberOfLines={2} ellipsizeMode='tail' size='mini'>{ads.description.replace(/<(.|\n)*?>/g, '')}</Text>
        </View>
      )
    }
    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY} onPress={() => navigateTo('NewsDetailScreen', { id_ads: ads.id_ads })}
        style={{
          width: WP100,
          paddingHorizontal: WP2,
          marginVertical: WP3,
          flexDirection: 'row'
        }}
      >
        <Image
          source={{ uri: ads.image }}
          style={{ marginHorizontal: WP1, ...BORDER_STYLE['rounded'], borderRadius: 5, flexGrow: 0 }}
          imageStyle={{ height: WP35, aspectRatio: 1 }}
        />

        {
          moment().diff(moment(ads.updated_at), 'days') <= 3 &&
          <Image style={{ height: WP2, position: 'absolute', left: 0, top: 0, marginHorizontal: WP3, marginVertical: WP1 }} aspectRatio={44 / 16} source={require('../../assets/images/labelNew.png')} />
        }

        <View style={{
          flexDirectionRow: 'column',
          flexGrow: 1,
          justifyContent: 'flex-start',
          flex: 1,
          paddingHorizontal: WP1,
          marginLeft: WP1
        }}
        >
          <Text numberOfLines={2} ellipsizeMode='tail' size='mini' weight={400} style={{ marginBottom: WP105 }}>{ads.title}</Text>
          <Text numberOfLines={4} ellipsizeMode='tail' size='mini'>{ads.description.replace(/<(.|\n)*?>/g, '')}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

AdsNewsItem.propTypes = {}

AdsNewsItem.defaultProps = {}

export default AdsNewsItem
