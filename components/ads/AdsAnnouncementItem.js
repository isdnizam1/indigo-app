import React, { Component } from 'react'
import { View } from 'react-native'
import Image from '../Image'
import { WP1, WP105, WP95 } from '../../constants/Sizes'
import { BORDER_COLOR, BORDER_WIDTH } from '../../constants/Styles'

class AdsAnnouncementItem extends Component {
  render() {
    const {
      ads,
      navigateTo
    } = this.props

    return (
      <View style={{
        marginVertical: WP105
      }}
      >
        <Image
          centered
          onPress={() => {
            navigateTo('AdsDetailScreenNoTab', { id_ads: ads.id_ads })
          }}
          source={{ uri: ads.image }}
          style={{ marginHorizontal: WP1, borderWidth: BORDER_WIDTH, borderColor: BORDER_COLOR, borderRadius: 5 }}
          imageStyle={{ height: undefined, width: WP95, aspectRatio: 2.5 }}
        />
      </View>
    )
  }
}

AdsAnnouncementItem.propTypes = {}

AdsAnnouncementItem.defaultProps = {}

export default AdsAnnouncementItem
