import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { isEmpty, startCase, toLower } from 'lodash-es'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Text from '../Text'
import { WP1, WP105, WP2, WP5 } from '../../constants/Sizes'
import { GREY, GREY_CHAT, GREY_SEMI, WARM_PURPLE, WARM_PURPLE_LIGHT, WHITE } from '../../constants/Colors'
import Icon from '../Icon'
import LinearGradientCompact from '../LinearGradientCompact'
import { getCity, getJob } from '../../actions/api'
import { TOUCH_OPACITY } from '../../constants/Styles'
import InputModal from '../InputModal'

class AdsFilter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dropdownProfessionKey: this.props.filters.profession_name || '',
      dropdownCityKey: ''
    }
  }

    _onChangeDropdown = (dropdownKey) => (value) => {
      this.setState({
        [dropdownKey]: value
      }, this._search)
    }

    _search = () => {
      const {
        dropdownProfessionKey,
        dropdownCityKey
      } = this.state
      const {
        getAds
      } = this.props
      getAds({ profession_name: dropdownProfessionKey, location_name: dropdownCityKey })
    }

    _dropdownTrigger(label, isSelected) {
      const borderColor = isSelected ? WARM_PURPLE : GREY_CHAT
      const bgColor = isSelected ? WARM_PURPLE_LIGHT : GREY_CHAT
      const textColor = isSelected ? WARM_PURPLE : GREY
      const typeIcon = isSelected ? 'FontAwesome' : 'Entypo'
      const nameIcon = isSelected ? 'check' : 'chevron-down'
      return (
        <LinearGradientCompact
          radius={8}
          colors={[borderColor, borderColor]}
          start={[0, 0]} end={[1, 0]}
          style={{
            marginHorizontal: WP1,
            marginVertical: WP1,
            padding: 1
          }}
        >
          <LinearGradientCompact
            radius={8}
            rounded
            colors={[bgColor, bgColor]}
            start={[0, 0]} end={[1, 0]}
            style={{
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: WP2,
              paddingVertical: WP2
            }}
          >
            <Text size='tiny' type='SansPro' weight={isSelected ? 500 : 400} style={{ color: textColor }}>{label}</Text>
            <Icon
              centered
              color={textColor}
              type={typeIcon}
              name={nameIcon}
              size={isSelected ? 'mini' : 'small'}
            />
          </LinearGradientCompact>
        </LinearGradientCompact>
      )
    }

    render() {
      const {
        dropdownProfessionKey,
        dropdownCityKey,
        dropdownInterestKey
      } = this.state
      const isExist = !!(!isEmpty(dropdownProfessionKey) || !isEmpty(dropdownInterestKey) || !isEmpty(dropdownCityKey))
      return (
        <View style={{
          borderBottomColor: GREY_SEMI,
          borderBottomWidth: WP1
        }}
        >
          <View style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexWrap: 'wrap',
            paddingHorizontal: WP5,
            paddingVertical: WP2
          }}
          >

            <Text size='tiny' color={GREY} style={{ marginRight: WP1, }}>{isExist ? 'Search in' : 'Filter by'}</Text>
            <InputModal
              extraResult
              refreshOnSelect
              triggerComponent={(
                this._dropdownTrigger('City', !!dropdownCityKey)
              )}
              header='Select City'
              suggestion={getCity}
              suggestionKey='city_name'
              suggestionPathResult='city_name'
              onChange={this._onChangeDropdown('dropdownCityKey')}
              selected={dropdownCityKey}
              reformatFromApi={(text) => startCase(toLower(text))}
              createNew={false}
              placeholder='Search city here...'
            />
            <InputModal
              extraResult
              refreshOnSelect
              triggerComponent={(
                this._dropdownTrigger('Profession', !!dropdownProfessionKey)
              )}
              header='Select Profession'
              suggestion={getJob}
              suggestionKey='job_title'
              suggestionPathResult='job_title'
              onChange={this._onChangeDropdown('dropdownProfessionKey')}
              selected={dropdownProfessionKey}
              createNew={false}
              placeholder='Search profession here...'
            />
          </View>
          {
            isExist &&
            <View style={{
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: WP5,
              paddingVertical: WP2,
              borderTopColor: GREY_SEMI,
              borderTopWidth: 1,
              flexWrap: 'wrap'
            }}
            >
              <Text size='tiny' color={GREY} style={{ marginRight: WP1 }}>Search in</Text>
              {
                !isEmpty(dropdownCityKey) && (
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    onPress={() => this.setState({ dropdownCityKey: '' }, this._search)}
                  >
                    <LinearGradient
                      colors={[WARM_PURPLE, WARM_PURPLE]}
                      start={[0, 0]} end={[1, 0]}
                      style={{
                        paddingHorizontal: WP2, paddingVertical: WP105, margin: WP1, borderRadius: 8,
                        alignItems: 'center', flexDirection: 'row',
                      }}
                    >
                      <Text size='mini' color={WHITE}>{`${dropdownCityKey}  `}</Text>
                      <Icon
                        centered
                        color={WHITE}
                        type='Ionicons'
                        name='ios-close-circle'
                      />
                    </LinearGradient>
                  </TouchableOpacity>
                )
              }
              {
                !isEmpty(dropdownProfessionKey) && (
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    onPress={() => this.setState({ dropdownProfessionKey: '' }, this._search)}
                  >
                    <LinearGradient
                      colors={[WARM_PURPLE, WARM_PURPLE]}
                      start={[0, 0]} end={[1, 0]}
                      style={{
                        paddingHorizontal: WP2, paddingVertical: WP105, margin: WP1, borderRadius: 8,
                        alignItems: 'center', flexDirection: 'row',
                      }}
                    >
                      <Text size='mini' color={WHITE}>{`${dropdownProfessionKey}  `}</Text>
                      <Icon
                        centered
                        color={WHITE}
                        type='Ionicons'
                        name='ios-close-circle'
                      />
                    </LinearGradient>
                  </TouchableOpacity>
                )
              }
              {
                !isEmpty(dropdownInterestKey) && (
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    onPress={() => this.setState({ dropdownInterestKey: '' }, this._search)}
                  >
                    <LinearGradient
                      colors={[WARM_PURPLE, WARM_PURPLE]}
                      start={[0, 0]} end={[1, 0]}
                      style={{
                        paddingHorizontal: WP2, paddingVertical: WP105, margin: WP1, borderRadius: 8,
                        alignItems: 'center', flexDirection: 'row',
                      }}
                    >
                      <Text size='mini' color={WHITE}>{`${dropdownInterestKey}  `}</Text>
                      <Icon
                        centered
                        color={WHITE}
                        type='Ionicons'
                        name='ios-close-circle'
                      />
                    </LinearGradient>
                  </TouchableOpacity>
                )
              }
            </View>
          }
        </View>
      )
    }
}

AdsFilter.propTypes = {
  filters: PropTypes.objectOf(PropTypes.any)
}

AdsFilter.defaultProps = {
  filters: {}
}

export default AdsFilter
