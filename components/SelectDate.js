import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import { useColorScheme } from "react-native-appearance";
import { isEmpty } from "lodash-es";
import {
  SHIP_GREY,
  SHIP_GREY_CALM,
  REDDISH,
  GREY_LIGHT,
  GREY_PLACEHOLDER,
} from "../constants/Colors";
import { WP1, WP2, WP4 } from "../constants/Sizes";
import { TOUCH_OPACITY } from "../constants/Styles";
import Text from "./Text";
import Icon from "./Icon";
import RequiredMark from "./RequiredMark";

const propsType = {
  dateFormat: PropTypes.string,
  mode: PropTypes.oneOf(["date", "time", "datetime"]),
  maximumDate: PropTypes.string,
  minimumDate: PropTypes.string,
  onChangeDate: PropTypes.func,
  required: PropTypes.bool,
  bold: PropTypes.bool,
  label: PropTypes.string,
  value: PropTypes.string,
  style: PropTypes.any,
  inputStyle: PropTypes.any,
  labelWeight: PropTypes.number,
  labelColor: PropTypes.string,
  wording: PropTypes.string,
  withDate: PropTypes.bool,
  disabled: PropTypes.bool,
};

const propsDefault = {
  dateFormat: "YYYY-MM-DD",
  mode: "date",
  required: false,
  labelWeight: 400,
  labelColor: SHIP_GREY,
  withDate: true,
};

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: "100%",
    borderColor: GREY_PLACEHOLDER,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 8,
  },
});

const SelectDate = (props) => {
  const {
    onChangeDate,
    bold,
    label,
    required,
    style,
    labelWeight,
    labelColor,
    textColor,
    wording,
    dateFormat,
    mode,
    minimumDate,
    maximumDate,
    withDate,
    disabled,
    children,
  } = props;
  const [isDateTimePickerVisible, setShow] = useState(false);
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    onChangeDate(value);
  }, [value]);

  const isDarkMode = () => {
    const colorScheme = useColorScheme();
    return colorScheme == "dark";
  };

  const _showDateTimePicker = () => setShow(true);

  const _hideDateTimePicker = () => setShow(false);

  const _handleDatePicked = (date) => {
    const newDate = moment(date).local("id").format(dateFormat);
    _hideDateTimePicker();
    setValue(newDate);
    onChangeDate(newDate);
  };

  const _renderDatePicker = () => (
    <DateTimePicker
      mode={mode}
      date={!isEmpty(value) ? new Date(value) : new Date()}
      minimumDate={
        minimumDate ? moment(minimumDate, dateFormat).toDate() : undefined
      }
      maximumDate={
        maximumDate ? moment(maximumDate, dateFormat).toDate() : undefined
      }
      isVisible={isDateTimePickerVisible}
      onConfirm={(date) => _handleDatePicked(date)}
      onCancel={_hideDateTimePicker}
      isDarkModeEnabled={isDarkMode()}
    />
  );

  if (children) {
    return (
      <>
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          style={{ ...style }}
          onPress={_showDateTimePicker}
          disabled={disabled}
        >
          {children}
        </TouchableOpacity>
        {_renderDatePicker()}
      </>
    );
  }

  return (
    <View
      style={[
        {
          marginBottom: WP4,
          flexGrow: 1,
        },
        style,
      ]}
    >
      {!!label && (
        <View style={{ flexDirection: "row", marginBottom: WP1 }}>
          <Text
            type="Circular"
            size={"xmini"}
            color={labelColor || SHIP_GREY}
            weight={bold ? 500 : labelWeight}
          >
            {label}
          </Text>
          {required && <RequiredMark />}
        </View>
      )}

      <View
        style={{ width: "100%", flexDirection: "row", alignItems: "center" }}
      >
        {withDate && (
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            style={{ flex: 1, marginRight: WP2 }}
            onPress={_showDateTimePicker}
            disabled={disabled}
          >
            <View
              style={[
                styles.input,
                props.inputStyle,
                {
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                },
              ]}
            >
              <Text
                type="Circular"
                size="xmini"
                weight={300}
                color={textColor || GREY_PLACEHOLDER}
              >
                {!isEmpty(value)
                  ? moment(value, dateFormat).format("DD")
                  : "Tgl"}
              </Text>
              <Icon
                background="dark-circle"
                size="small"
                color={textColor || SHIP_GREY_CALM}
                name={"chevron-down"}
                type={"MaterialCommunityIcons"}
              />
            </View>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          style={{ flex: 2, marginRight: WP2 }}
          onPress={_showDateTimePicker}
          disabled={disabled}
        >
          <View
            style={[
              styles.input,
              props.inputStyle,
              {
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              },
            ]}
          >
            <Text
              type="Circular"
              size="xmini"
              weight={300}
              color={textColor || GREY_PLACEHOLDER}
            >
              {!isEmpty(value)
                ? moment(value, dateFormat).format("MMMM")
                : "Bulan"}
            </Text>
            <Icon
              background="dark-circle"
              size="small"
              color={textColor || SHIP_GREY_CALM}
              name={"chevron-down"}
              type={"MaterialCommunityIcons"}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          style={{ flex: 2 }}
          onPress={_showDateTimePicker}
          disabled={disabled}
        >
          <View
            style={[
              styles.input,
              props.inputStyle,
              {
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
              },
            ]}
          >
            <Text
              type="Circular"
              size="xmini"
              weight={300}
              color={textColor || GREY_PLACEHOLDER}
            >
              {!isEmpty(value)
                ? moment(value, dateFormat).format("YYYY")
                : "Tahun"}
            </Text>
            <Icon
              background="dark-circle"
              size="small"
              color={textColor || SHIP_GREY_CALM}
              name={"chevron-down"}
              type={"MaterialCommunityIcons"}
            />
          </View>
        </TouchableOpacity>
      </View>
      {_renderDatePicker()}
      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        {!!wording && (
          <View style={{ flexDirection: "row" }}>
            <Text type="Circular" size="xtiny" color={SHIP_GREY_CALM}>
              {wording}
            </Text>
          </View>
        )}
      </View>
    </View>
  );
};

SelectDate.propTypes = propsType;
SelectDate.defaultProps = propsDefault;
export default SelectDate;
