import React from "react";
import PropTypes from "prop-types";
import { Image as RNImage, View } from "react-native";
import { isEmpty, isString, keys } from "lodash-es";
import SkeletonContent from "react-native-skeleton-content";
import {
  CLEAR_BLUE,
  REDDISH,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
} from "../constants/Colors";
import { IMAGE_SIZE, WP100, WP4, WP6 } from "../constants/Sizes";
import { BORDER_COLOR, BORDER_WIDTH } from "../constants/Styles";
import Image from "./Image";
import Icon from "./Icon";
import ProfileSkeleton from "./profile/ProfileSkeletonConfig";

const propsType = {
  image: PropTypes.any,
  size: PropTypes.oneOf(keys(IMAGE_SIZE)),
  onPress: PropTypes.func,
  imageStyle: PropTypes.any,
  shadow: PropTypes.bool,
  noPlaceholder: PropTypes.bool,
  isProfile: PropTypes.bool,
  isGroup: PropTypes.bool,
  verifiedStatus: PropTypes.oneOf(["verified", "not verified"]),
  verifiedSize: PropTypes.number,
  isLoading: PropTypes.bool,
  passValidation: PropTypes.bool,
  newItem: PropTypes.bool,
};

const propsDefault = {
  image: null,
  size: "small",
  shadow: true,
  noPlaceholder: false,
  isProfile: false,
  isGroup: false,
  verifiedStatus: "not verified",
  isLoading: false,
  passValidation: false,
  newItem: false,
};

class Avatar extends React.PureComponent {
  render() {
    const {
      image,
      size,
      onPress,
      imageStyle,
      // shadow,
      noPlaceholder,
      style,
      verifiedStatus,
      isProfile,
      isGroup,
      verifiedSize,
      isLoading,
      passValidation,
      newItem,
    } = this.props;
    const isImage = !isEmpty(image) || passValidation;
    const sizeVerified = verifiedSize || (isProfile ? WP6 : WP4);
    const dotSize = sizeVerified * 0.8;
    return (
      <View
        style={[
          {
            backgroundColor: WHITE,
            borderRadius: WP100,
            borderWidth: isImage ? 0 : BORDER_WIDTH,
            borderColor: BORDER_COLOR,
            justifyContent: "center",
            width: IMAGE_SIZE[size],
            height: IMAGE_SIZE[size],
          },
          isImage ? imageStyle : {},
          style,
        ]}
      >
        <SkeletonContent
          layout={[ProfileSkeleton.layout.avatar(size)]}
          isLoading={isLoading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <Image
            onPress={onPress}
            source={
              isImage
                ? isString(image)
                  ? { uri: `${image}` }
                  : image
                : noPlaceholder
                ? null
                : require("../assets/icons/user.png")
            }
            imageStyle={[
              {
                aspectRatio: 1,
                height: IMAGE_SIZE[size] - (isImage ? 0 : IMAGE_SIZE[size] / 3),
                width: undefined,
                borderRadius:
                  (IMAGE_SIZE[size] - (isImage ? 0 : IMAGE_SIZE[size] / 3)) / 2,
              },
              isImage ? imageStyle : {},
            ]}
          />
          {newItem && (
            <RNImage
              style={{
                position: "absolute",
                width: dotSize,
                height: dotSize,
                top: 1.25,
                right: 1.25,
              }}
              source={require("../assets/icons/v3/dotReddish.png")}
            />
          )}
          {verifiedStatus === "verified" && (
            <RNImage
              style={{
                position: "absolute",
                width: sizeVerified,
                height: sizeVerified,
                bottom: 0,
                right: 0,
              }}
              source={require("../assets/icons/tickCircleBlue.png")}
            />
          )}
          {isGroup && (
            <View
              style={{
                position: "absolute",
                bottom: 0,
                right: 0,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Icon
                centered
                style={{
                  width: sizeVerified,
                  height: sizeVerified,
                  borderRadius: sizeVerified / 2,
                }}
                type="MaterialCommunityIcons"
                name="account-multiple"
                size={sizeVerified / 2}
                color={WHITE}
                background="dark-circle"
                backgroundColor={REDDISH}
              />
            </View>
          )}
        </SkeletonContent>
      </View>
    );
  }
}

Avatar.propTypes = propsType;
Avatar.defaultProps = propsDefault;
export default React.memo(Avatar);
