import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import { isEmpty } from 'lodash-es'
import * as WebBrowser from 'expo-web-browser'
import Image from '../Image'
import { WP2 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { NavigateToInternalBrowser } from '../../utils/helper'

const SocialMediaInfo = (props) => {
  const {
    link,
    image,
    email,
    toggleModal
  } = props

  let url = link
  if (email) url = `mailto:${ link}`
  else {
    if (url.substr(0, 8) !== 'https://') url = `https://${ url}`
  }

  return (
    <TouchableOpacity
      onPress={() => {
        toggleModal()
        if (email) {
          WebBrowser.openBrowserAsync(url)
        } else {
          NavigateToInternalBrowser({
            url: isEmpty(url) ? 'https://soundfren.id/' : url
          })
        }
      }} activeOpacity={TOUCH_OPACITY}
      style={{}}
    >
      <Image
        size='xsmall'
        source={image}
        style={{ marginRight: WP2 }}
      />
    </TouchableOpacity>
  )
}

SocialMediaInfo.propTypes = {
  image: PropTypes.any,
  link: PropTypes.string,
  email: PropTypes.bool,
  toggleModal: PropTypes.func
}

SocialMediaInfo.defaultProps = {
  image: require('../../assets/icons/social_media/website.png'),
  link: '',
  email: false,
  toggleModal: () => {}
}

export default SocialMediaInfo
