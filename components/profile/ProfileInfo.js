import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import { isEmpty, capitalize } from 'lodash-es'
import { WP1, WP100, WP2, WP20, WP4, WP70, HP1, WP3, WP5, WP7, HP7, WP8 } from '../../constants/Sizes'
import { ORANGE, TOMATO, GREY_WARM, GREY, GREY_ABOUT, WHITE } from '../../constants/Colors'
import Text from '../Text'
import Icon from '../Icon'
import Modal from '../Modal'
import Image from '../Image'
import { TOUCH_OPACITY, BORDER_WIDTH } from '../../constants/Styles'
import SocialMediaInfo from './SocialMediaInfo'

export const profileInfoStyle = StyleSheet.create({
  section: {
    paddingVertical: WP2
  },
  sectionHeader: {
    marginBottom: HP1
  }
})

const ProfileInfo = (props) => {
  const {
    user } = props

  return (
    <Modal
      isVisible={false}
      renderModalContent={({ toggleModal }) => (
        <View style={{ paddingBottom: HP7, paddingHorizontal: WP5 }}>
          <Image
            source={require('../../assets/images/illustrationProfile.png')}
            style={{ position: 'absolute', bottom: 0, right: 0 }}
            imageStyle={{ width: WP70, height: undefined, aspectRatio: 206/165, }}
          />
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            style={{
              position: 'absolute',
              right: 0,
              top: 0,
              paddingHorizontal: WP3,
              paddingVertical: WP1,
              borderRadius: 6,
              borderWidth: BORDER_WIDTH,
              borderColor: TOMATO,
              backgroundColor: TOMATO,
              marginRight: WP7,
              marginTop: WP8
            }}
            onPress={() => toggleModal()}
          >
            <Text centered size='tiny' type='NeoSans' weight={500} color={WHITE}>Back</Text>
          </TouchableOpacity>
          <View style={{
            width: WP20,
            borderRadius: 5,
            height: 5,
            marginTop: 10,
            alignSelf: 'center'
          }}
          />
          <View style={{
            paddingVertical: WP4,
            paddingHorizontal: WP4
          }}
          >
            <Text size='mini' type='NeoSans' weight={500} color={TOMATO}>{'More Info'}</Text>
          </View>
          <View style={{ paddingHorizontal: WP4 }}>
            {
              !isEmpty(user.birthdate) && (
                <View style={[profileInfoStyle.section]}>
                  <Text
                    size='tiny' color={GREY_WARM}
                    style={profileInfoStyle.sectionHeader}
                  >Birthday</Text>
                  <Text color={GREY_ABOUT} size='mini'>{capitalize(user.birthdate)}</Text>
                </View>
              )
            }
            {
              !isEmpty(user.gender) && (
                <View style={[profileInfoStyle.section]}>
                  <Text
                    size='tiny' color={GREY_WARM}
                    style={profileInfoStyle.sectionHeader}
                  >Gender</Text>
                  <Text weight={500} color={GREY} size='mini'>{capitalize(user.gender)}</Text>
                </View>
              )
            }
            {
              !isEmpty(user.interest) && (
                <View style={[profileInfoStyle.section]}>
                  <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text
                      size='tiny' color={GREY_WARM}
                      style={profileInfoStyle.sectionHeader}
                    >Interest</Text>
                  </View>
                  <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {
                      user.interest.map((item, index) => (
                        <View
                          key={index}
                          style={{
                            borderRadius: WP100,
                            borderColor: ORANGE,
                            borderWidth: 1,
                            paddingVertical: WP1,
                            paddingHorizontal: WP2,
                            flexGrow: 0,
                            marginVertical: WP1,
                            marginRight: WP2
                          }}
                        >
                          <Text
                            color={ORANGE} size='mini' weight={400}
                            style={{ flexGrow: 0 }}
                          >{item.interest_name}</Text>
                        </View>
                      ))
                    }
                  </View>
                </View>
              )
            }
            <View style={[profileInfoStyle.section]}>
              <Text
                size='tiny' color={GREY_WARM}
                style={profileInfoStyle.sectionHeader}
              >Social Media</Text>
              <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                {
                  !isEmpty(user.social_media) && !isEmpty(user.social_media.instagram) && (
                    <SocialMediaInfo
                      toggleModal={toggleModal}
                      image={require('../../assets/icons/social_media/instagram.png')}
                      link={user.social_media.instagram}
                    />
                  )
                }
                {
                  !isEmpty(user.social_media) && !isEmpty(user.social_media.twitter) && (
                    <SocialMediaInfo
                      toggleModal={toggleModal}
                      image={require('../../assets/icons/social_media/twitter.png')}
                      link={user.social_media.twitter}
                    />
                  )
                }
                {
                  !isEmpty(user.social_media) && !isEmpty(user.social_media.youtube) && (
                    <SocialMediaInfo
                      toggleModal={toggleModal}
                      image={require('../../assets/icons/social_media/youtube.png')}
                      link={user.social_media.youtube}
                    />
                  )
                }
                {
                  !isEmpty(user.social_media) && !isEmpty(user.social_media.facebook) && (
                    <SocialMediaInfo
                      toggleModal={toggleModal}
                      image={require('../../assets/icons/social_media/facebook.png')}
                      link={user.social_media.facebook}
                    />
                  )
                }
                {
                  !isEmpty(user.social_media) && !isEmpty(user.social_media.website) && (
                    <SocialMediaInfo
                      toggleModal={toggleModal}
                      image={require('../../assets/icons/social_media/website.png')}
                      link={user.social_media.website}
                    />
                  )
                }
                <SocialMediaInfo
                  toggleModal={toggleModal}
                  image={require('../../assets/icons/social_media/email2.png')}
                  link={user.email}
                  email
                />
              </View>
            </View>
          </View>
        </View>
      )}
    >
      {({ toggleModal }, M) => (
        <React.Fragment>
          <TouchableOpacity
            onPress={toggleModal}
            style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: WP1 }}
          >
            <Icon
              size='small' centered color={TOMATO} type='Entypo' name='dots-three-horizontal'
              style={{ marginRight: WP2 }}
            />
            <Text
              size='mini'
              color={TOMATO}
              weight={500}
            >{'See more info'}</Text>
          </TouchableOpacity>
          {M}
        </React.Fragment>
      )}
    </Modal>
  )
}

ProfileInfo.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  refreshProfile: PropTypes.func,
  navigateTo: PropTypes.func,
  isMine: PropTypes.bool
}

ProfileInfo.defaultProps = {
  user: {
    job: [],
    interest: []
  },
  refreshProfile: () => {
  },
  navigateTo: () => {
  },
  isMine: false
}

export default ProfileInfo
