import React from 'react'
import SkeletonContent from 'react-native-skeleton-content'
import { View } from 'react-native'
import { HP1, WP2, WP25, WP3, WP30, WP35, WP40, WP5, WP505, WP6, WP7, WP70 } from '../../constants/Sizes'
import { SKELETON_COLOR, SKELETON_HIGHLIGHT, WHITE } from '../../constants/Colors'

const styles = {
  container: {
    flexGrow: 1,
    backgroundColor: WHITE,
    paddingBottom: HP1,
    paddingHorizontal: WP6,
    alignItems: 'flex-start'
  }
}

const layout = {
  sectionHeader: {
    width: WP30,
    height: WP6,
    marginBottom: WP3,
    marginTop: WP6
  },
  songItem: {
    width: WP70,
    height: WP505,
    marginBottom: WP2
  },
  videoThumbnail: {
    width: WP40,
    height: WP25,
    marginRight: WP3
  },
  videoTitle: {
    width: WP30,
    height: WP6,
    marginBottom: WP2
  },
  videoSubTitle: {
    width: WP40,
    height: WP5
  },
  artItem: {
    width: WP35,
    height: WP35,
    marginRight: WP7,
    marginBottom: HP1
  }
}

export const ProfileWorkTabSkeleton = ({ isLoading }) => {
  const skeletonProps = {
    isLoading,
    boneColor: SKELETON_COLOR,
    highlightColor: SKELETON_HIGHLIGHT
  }
  return (
    <View style={styles.container}>
      <SkeletonContent
        layout={[layout.sectionHeader]}
        {...skeletonProps}
      />
      <SkeletonContent
        layout={[layout.songItem, layout.songItem, layout.songItem]}
        {...skeletonProps}
      />

      <SkeletonContent
        layout={[layout.sectionHeader]}
        {...skeletonProps}
      />
      <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: WP2 }}>
        <SkeletonContent
          containerStyle={{ flexGrow: 0 }}
          layout={[layout.videoThumbnail]}
          {...skeletonProps}
        />
        <SkeletonContent
          containerStyle={{ flexGrow: 0 }}
          layout={[layout.videoTitle, layout.videoSubTitle]}
          {...skeletonProps}
        />
      </View>

      <SkeletonContent
        layout={[layout.sectionHeader]}
        {...skeletonProps}
      />
      <SkeletonContent
        containerStyle={{ flexDirection: 'row' }}
        layout={[layout.artItem, layout.artItem, layout.artItem]}
        {...skeletonProps}
      />
    </View>
  )
}

export default {
  layout,
  styles,
  ProfileWorkTabSkeleton
}
