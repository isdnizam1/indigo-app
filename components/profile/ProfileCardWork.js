import React from 'react'
import PropTypes from 'prop-types'
import { Alert, TouchableOpacity, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { isEmpty, map, noop, get, toLower } from 'lodash-es'
import { GREY, ORANGE_BRIGHT, PINK_PURPLE, PINK_RED, PURPLE, WHITE, GREY_WARM, BLUE_LIGHT } from '../../constants/Colors'
import { FONT_SIZE, HP05, HP1, WP1, WP2, WP20, WP4, WP6, WP90 } from '../../constants/Sizes'
import Text from '../Text'
import Icon from '../Icon'
import { TOUCH_OPACITY, SHADOW_STYLE } from '../../constants/Styles'
import { Button } from '../index'
import { alertDelete } from '../../utils/alert'

const propsType = {
  actionAdd: PropTypes.func,
  actionEdit: PropTypes.func,
  actionSave: PropTypes.func,
  actionDelete: PropTypes.func,
  header: PropTypes.string,
  dataSource: PropTypes.array,
  renderContent: PropTypes.func,
  isMine: PropTypes.bool,
  ContentContainer: PropTypes.element,
  contentContainerProps: PropTypes.any,
  limit: PropTypes.bool,
  limitNum: PropTypes.number,
  itemMargin: PropTypes.number
}

const propsDefault = {
  actionAdd: noop,
  actionEdit: noop,
  actionSave: noop,
  actionIcon: 'right',
  contentDirection: 'column',
  ContentContainer: View,
  contentContainerProps: {},
  itemMargin: 0
}

class ProfileCardWork extends React.Component {
  state = {
    isEditing: false,
    activeId: null,
    isExpand: false,
  }

  _doAction = () => {
    const {
      actionAdd,
      actionSave,
      dataSource
    } = this.props
    const {
      isEditing
    } = this.state

    if (!isEmpty(dataSource)) {
      if (isEditing) {
        actionSave()
        this.setState({ isEditing: false })
      } else {
        this.setState({ isEditing: true })
      }
    } else {
      actionAdd()
    }
  }

  _toggleExpand = () => {
    const {
      isExpand
    } = this.state
    this.setState({ isExpand: !isExpand })
  }

  _goToWorklist = () => {
    const {
      navigateTo,
      refreshProfile,
      category,
      user,
      actionAdd,
      actionEdit,
      actionDelete
    } = this.props
    navigateTo('WorkListScreen', { refreshProfile, category: toLower(category), user, actionAdd, actionEdit, actionDelete }, 'push' )
  }

  render() {
    const {
      header,
      dataSource,
      renderContent,
      actionDelete,
      idPerItem,
      noDescription,
      actionIcon,
      actionEdit,
      ContentContainer,
      contentContainerProps,
      category,
      noCard,
      user,
      noCardPadding,
      limit,
      limitNum,
      itemMargin
    } = this.props
    const {
      isEditing,
      activeId,
      isExpand
    } = this.state

    const isOver = limit && !isEmpty(dataSource) && limitNum < dataSource.length

    return (
      <View style={{ backgroundColor: WHITE }}>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
            paddingHorizontal: WP6,
            paddingTop: WP6,
            paddingBottom: HP1
          }}
          disabled={!user}
          onPress={this._goToWorklist}
        >

          <View style={{
            alignItems: 'center',
            flexDirection: 'row' }}
          >
            <Text weight={500} size='mini'>
              {header}
            </Text>
            <Icon size='tiny' style={{ marginHorizontal: WP1 }} centered color={GREY_WARM} type='Entypo' name='dot-single'/>
            <Text size='mini' color={GREY_WARM}>
              {isEmpty(dataSource) ? 'empty' : `${Array.isArray(dataSource) ? dataSource.length : Object.keys(dataSource).length } ${category}`}
            </Text>
          </View>
          { !isEmpty(user) &&
          <Icon size='small' style={{ marginHorizontal: WP1 }} centered color={GREY} type='Entypo' name='chevron-right'/>
          }
        </TouchableOpacity>
        {
          !isEmpty(dataSource) && (
            <View style={{ paddingHorizontal: WP6 }}>
              <ContentContainer
                {...contentContainerProps}
                style={noCard ? {} : {
                  width: WP90,
                  borderRadius: 12,
                  backgroundColor: WHITE,
                  marginVertical: HP05,
                  paddingTop: noCardPadding ? 0 : HP1,
                  alignSelf: 'center',
                  overflow: 'hidden',
                  ...SHADOW_STYLE.shadow
                }}
              >
                {
                  map(isExpand || !limit ? dataSource : dataSource.slice(0, limitNum), (data, i) => {
                    return (
                      <View
                        key={`${i}${new Date()}`}
                        style={{ marginBottom: itemMargin }}
                      >
                        {
                          renderContent({
                            data, i,
                            renderAction: () => (
                              noDescription && !isEditing ? null :
                                <View style={[{
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                  marginVertical: 0,
                                  position: 'absolute',
                                  right: 0,
                                }, actionIcon === 'bottom' && {
                                  marginVertical: HP1,
                                  position: undefined,
                                }]}
                                >
                                  {
                                    isEditing ?
                                      <LinearGradient
                                        style={{ flexDirection: 'row', paddingLeft: actionIcon === 'bottom' ? 0 : WP20 }}
                                        colors={['#ffffff00', WHITE]}
                                        start={[0, 0]} end={[0.4, 0]}
                                      >
                                        <Button
                                          onPress={() => actionEdit(data)}
                                          iconName='pencil'
                                          iconType='MaterialCommunityIcons'
                                          iconSize='mini'
                                          style={{ marginVertical: 0 }}
                                          contentStyle={{ padding: WP2 }}
                                          shadow='none'
                                          colors={[PURPLE, PINK_PURPLE]}
                                          rounded
                                          iconColor={WHITE}
                                        />
                                        <Button
                                          onPress={() => alertDelete(() => actionDelete(get(data, idPerItem)))}
                                          iconName='close'
                                          iconType='FontAwesome'
                                          iconSize='mini'
                                          style={{ marginVertical: 0, marginHorizontal: WP4 }}
                                          contentStyle={{ padding: WP2 }}
                                          shadow='none'
                                          colors={[PINK_RED, ORANGE_BRIGHT]}
                                          rounded
                                          iconColor={WHITE}
                                        />
                                        <Button
                                          onPress={() => Alert.alert('Stay Tuned', 'We will inform this feature to you soon.')}
                                          iconName='share-alt'
                                          iconType='FontAwesome'
                                          iconSize='mini'
                                          style={{ marginVertical: 0 }}
                                          contentStyle={{ padding: WP2 }}
                                          shadow='shadow'
                                          colors={[PURPLE, PINK_PURPLE]}
                                          rounded
                                          iconColor={WHITE}
                                        />
                                      </LinearGradient>
                                      : <TouchableOpacity
                                        onPress={() => this.setState({ activeId: activeId === data[idPerItem] ? null : data[idPerItem] })}
                                        activeOpacity={TOUCH_OPACITY}
                                        style={{
                                          padding: WP1, width: FONT_SIZE['tiny'] + WP2, height: FONT_SIZE['tiny'] + WP2
                                        }}
                                        >
                                        <Icon
                                          size='tiny' color={GREY} type='Entypo'
                                          name={activeId === data[idPerItem] ? 'chevron-up' : 'chevron-down'}
                                        />
                                      </TouchableOpacity>
                                  }
                                </View>
                            ),
                            activeId
                          })
                        }
                      </View>
                    )
                  })
                }
                {
                  limit && isOver ? (
                    <TouchableOpacity
                      onPress={this._goToWorklist}
                      style={{
                        flex: 1,
                        backgroundColor: BLUE_LIGHT,
                        alignContent: 'center',
                        padding: HP1,
                        marginTop: HP05,
                        borderBottomLeftRadius: 12,
                        borderBottomRightRadius: 12,
                      }}
                    >
                      <Text centered size='mini' type='NeoSans' weight={500} color={WHITE}>
                        {isExpand ? `less ${toLower(category)}` : `more ${toLower(category)}`}
                      </Text>
                    </TouchableOpacity>
                  ) :
                    (
                      <View style={{
                        marginBottom: HP1
                      }}
                      />
                    )
                }
              </ContentContainer>
            </View>
          )
        }
      </View>
    )
  }
}

ProfileCardWork.propTypes = propsType
ProfileCardWork.defaultProps = propsDefault
export default ProfileCardWork