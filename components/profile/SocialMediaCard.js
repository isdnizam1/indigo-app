import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import Image from '../Image'
import InputTextLight from '../InputTextLight'
import { WP4 } from '../../constants/Sizes'

const SocialMediaCard = (props) => {
  const {
    icon,
    value,
    placeholder,
    onChange,
  } = props

  return (
    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
      <View style={{ flexGrow: 0 }}>
        <Image
          size='mini'
          source={icon}
          style={{ marginRight: WP4 }}
        />
      </View>
      <View style={{ flexGrow: 1 }}>
        <InputTextLight
          value={value}
          withLabel={false}
          size='mini'
          placeholder={placeholder}
          onChangeText={onChange}
        />
      </View>
    </View>
  )
}

SocialMediaCard.propTypes = {
  icon: PropTypes.objectOf(PropTypes.any),
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
}

SocialMediaCard.defaultProps = {
  icon: require('../../assets/icons/photoInactive.png'),
  value: '',
  placeholder: '',
  onChange: () => {},
}

export default SocialMediaCard
