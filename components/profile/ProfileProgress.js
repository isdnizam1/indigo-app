import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Image } from 'react-native'
import { WP1, HP1, HP2, HP3, WP3, WP5 } from '../../constants/Sizes'
import { GREY_WARM, GREY_CALM_SEMI, WHITE, GREEN_TOSCA } from '../../constants/Colors'
import Text from '../Text'
import Loader from '../Loader'
import { SHADOW_STYLE } from '../../constants/Styles'

export const style = StyleSheet.create({
  progressWrapper: {
    marginTop: HP1,
    marginBottom: HP1,
    borderRadius: 12,
    paddingHorizontal: WP5,
    paddingTop: HP1 + 2,
    backgroundColor: WHITE,
    ...SHADOW_STYLE.shadow,
  },
  tickCircle: {
    width: 26,
    height: 26,
    position: 'absolute',
    right: -15,
    top: -8.5,
    borderRadius: 14
  },
  progresIndicator: {
    flex: 1,
    backgroundColor: GREY_CALM_SEMI,
    height: 8,
    marginHorizontal: 2
  },
  firstProgress: {
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
  },
  lastProgress: {
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6
  },
  progressCompleted: {
    backgroundColor: GREEN_TOSCA,
    zIndex: 50
  },
  progressDescription: {
    paddingVertical: HP1 * 0.6,
    flexDirection: 'row',
    alignItems: 'center'
  },
  progressDone: {
    color: GREEN_TOSCA,
    marginLeft: WP3
  },
  progressUndone: {
    color: GREY_CALM_SEMI,
    marginLeft: WP3
  },
  shareProfile: {
    backgroundColor: GREEN_TOSCA,
    paddingTop: 4,
    paddingBottom: 5,
    marginTop: HP3,
    marginHorizontal: -WP5,
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12
  },
  chevronDown: { width: 15, alignSelf: 'center', marginVertical: HP1 }
})

const progressDescriptions = {
  location: 'Location, Profession, Company, and Interest',
  picture: 'Add Profile Picture and Header Picture',
  experience: 'Add Experience',
  works: 'Add Works',
  first_post: 'Create First Post'
}

const ProfileProgress = (props) => {
  let {
    progress: {
      completed,
      experience,
      first_post,
      location,
      not_completed,
      picture,
      works
    },
    expanded
  } = props
  let sortedProgress = [{ location }, { picture }, { experience }, { works }, { first_post }]/*.sort((a, b) => Object.values(a)[0] < Object.values(b)[0])*/
  let totalStep = 0
  if(typeof completed !== 'undefined') totalStep = completed + not_completed
  return (<View>
    {typeof completed === 'undefined' && (<View style={{ paddingVertical: HP3, marginBottom: HP2 }}>
      <Loader size={'mini'} isLoading={true} />
    </View>)}
    {typeof completed !== 'undefined' && (<View style={{ ...style.progressWrapper }}>
      <Text centered color={GREY_WARM} size={'xtiny'}>Your profile progress</Text>
      <View style={{ flexDirection: 'row', marginHorizontal: -WP1, marginTop: HP2 }}>
        {[...Array(totalStep).keys()].map((index) => {
          return (<View
            style={[
              style.progresIndicator,
              index == 0 && (style.firstProgress),
              index+1 == totalStep && (style.lastProgress),
              index+1 <= completed && (style.progressCompleted)
            ]}
            key={index}
                  >
            {index+1 == completed && (<Image resizeMode={'contain'} style={style.tickCircle} source={require('../../assets/icons/tickCircleTosca.png')} />)}
          </View>)
        })}
      </View>
      {completed < totalStep && expanded && (<View>
        <View style={{ marginTop: HP2 }}>
          {[true, false].map((progressState) => {
            return (<View key={Math.random()}>
              {sortedProgress.map((item) => {
                let label = Object.keys(item)[0]
                let done = Object.values(item)[0]
                return done == progressState ? (<View style={style.progressDescription} key={label}>
                  <Image
                    style={{ width: 12, height: 12 }} source={
                      done ? require('../../assets/icons/tickCircleOutlined.png') : require('../../assets/icons/tickEmptyOutlined.png')
                    }
                  />
                  <Text style={[done ? style.progressDone : style.progressUndone]} size={'xtiny'}>{progressDescriptions[label]}</Text>
                </View>) : <View key={Math.random()} />
              })}
            </View>)
          })}
        </View>
      </View>)}
      {completed < totalStep && (<Image
        resizeMode={'contain'} style={style.chevronDown} source={
          expanded ? require('../../assets/icons/chevronUp.png') : require('../../assets/icons/chevronDown.png')
        }
                                 />)}
      {completed >= totalStep && (<View style={style.shareProfile}>
        <Text centered color={WHITE} weight={600} size={'mini'}>Share my profile</Text>
      </View>)}
    </View>)}
  </View>)
}

ProfileProgress.propTypes = {
  progress: PropTypes.any
}

ProfileProgress.defaultProps = {
  progress: null
}

export default ProfileProgress