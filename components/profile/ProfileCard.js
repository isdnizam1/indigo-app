import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { WHITE } from '../../constants/Colors'
import { HP1, HP2, WP4 } from '../../constants/Sizes'

import Text from '../Text'
import Icon from '../Icon'
import { BORDER_STYLE, TOUCH_OPACITY } from '../../constants/Styles'

const propsType = {
}

const propsDefault = {

}

const ProfileCard = (props) => {
  const {
    header,
    action,
    content,
    isMine,
  } = props
  return (
    <View style={{ backgroundColor: WHITE, marginVertical: HP1 }}>
      <View style={{
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        ...BORDER_STYLE['bottom'],
        paddingHorizontal: WP4,
        paddingVertical: HP2
      }}
      >
        <Text weight={400}>
          {header}
        </Text>
        {
          isMine ? (
            <TouchableOpacity activeOpacity={TOUCH_OPACITY} onPress={action}>
              {
                content ? (
                  <Icon centered type='MaterialCommunityIcons' name='pencil'/>
                ) : (
                  <Icon centered name='plus'/>
                )
              }
            </TouchableOpacity>
          ) : null
        }

      </View>
      {
        content
      }
    </View>
  )
}

ProfileCard.propTypes = propsType
ProfileCard.defaultProps = propsDefault
export default ProfileCard
