/* @flow */

import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import moment from 'moment'
import { startCase, toLower } from 'lodash-es'
import { HP2, WP2 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import Avatar from '../Avatar'
import TextName from '../TextName'
import Icon from '../Icon'
import { GREY_WARM } from '../../constants/Colors'
import Text from '../Text'

const ProfileSnippet = ({ user, onPressOption, navigateProfile }: {
  full_name: string; profile_picture: string; account_type: string,
  created_at: any; job_title: string; city_name: string; optionType: string;
}) => {
  return (
    <TouchableOpacity
      style={{ flexDirection: 'row', marginBottom: HP2, alignItems: 'center' }}
      activeOpacity={TOUCH_OPACITY}
      onPress={navigateProfile}
    >
      <View style={{ marginRight: WP2 }}>
        <Avatar image={user.profile_picture}/>
      </View>
      <View>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <TextName user={user}/>
          <Icon centered color={GREY_WARM} type='Entypo' name='dot-single'/>
          <Text size='mini' color={GREY_WARM}>
            {moment(user.created_at).fromNow(true)}
          </Text>
        </View>
        <Text
          size='mini'
        >{user.job_title ? `${user.job_title} · ` : ''}{startCase(toLower(user.city_name))}</Text>
      </View>
      {
        onPressOption && (
          <Icon
            style={{ alignSelf: 'flex-start', position: 'absolute', right: 0 }}
            centered
            onPress={() => {
              user.optionType = 'feed'
              onPressOption(user)
            }}
            name={'dots-three-horizontal'} type='Entypo' color={GREY_WARM}
          />
        )
      }
    </TouchableOpacity>
  )
}

export default ProfileSnippet
