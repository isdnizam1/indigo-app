import { IMAGE_SIZE, WP105, WP25, WP35, WP5, WP50, WP6, WP60, WP75 } from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'

const style = {}

const layout = {
  avatar: (size) => ({
    width: IMAGE_SIZE[size],
    height: IMAGE_SIZE[size],
    borderRadius: IMAGE_SIZE[size]/2,
    ...SHADOW_STYLE['shadowThin']
  }),
  name: {
    width: WP25,
    height: WP6
  },
  job: {
    width: WP75,
    height: WP5,
    marginBottom: WP105
  },
  location: {
    width: WP50,
    height: WP5,
    marginBottom: WP105
  },
  about: {
    width: WP60,
    height: WP5,
    marginBottom: WP105
  },
  moreInfo: {
    width: WP35,
    height: WP5
  }
}

export default {
  layout,
  style
}
