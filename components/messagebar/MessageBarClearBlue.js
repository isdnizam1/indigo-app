import React, { Component } from 'react'
import {
  View,
  TouchableOpacity
} from 'react-native'
import Text from 'sf-components/Text'
import Icon from 'sf-components/Icon'
import { WHITE, CLEAR_BLUE } from 'sf-constants/Colors'
import { WP3, WP100 } from 'sf-constants/Sizes'
import { emptyClearBlueMessage } from 'sf-services/messagebar/actionDispatcher'
import { connect } from 'react-redux'

class MessageBarClearBlue extends Component {

  componentDidUpdate(prevProps, state) {
    if(!prevProps.messageClearBlue && this.props.messageClearBlue) {
      setTimeout(() => this.props.emptyClearBlueMessage(), 6000)
    }
  }

  render() {
    const { messageClearBlue, emptyClearBlueMessage } = this.props
    if(!messageClearBlue) return <View />
    return (<TouchableOpacity onPress={emptyClearBlueMessage} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: CLEAR_BLUE, padding: WP3, position: 'absolute', left: 0, width: WP100 }}>
      <Text size={'slight'} type={'Circular'} color={WHITE}>{messageClearBlue}</Text>
      <View>
        <Icon size={'huge'} color={WHITE} type={'MaterialCommunityIcons'} name={'check-circle'} />
      </View>
    </TouchableOpacity>)
  }

}

export default connect(
  ({ messagebar: { messageClearBlue } }) => ({ messageClearBlue }),
  { emptyClearBlueMessage }
)(MessageBarClearBlue)
