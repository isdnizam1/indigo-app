import React from 'react'
import PropTypes from 'prop-types'
import { View, TextInput, FlatList, ActivityIndicator, Keyboard } from 'react-native'
import { keys, map, isFunction, remove, includes } from 'lodash-es'
import { GREY, GREY50, GREY_WARM, SILVER, WHITE, WHITE20, WHITE50 } from '../constants/Colors'
import { FONT_SIZE, WP205, WP50, HP105, HP1, WP105, WP2, WP305, WP1, WP3, WP100, WP4 } from '../constants/Sizes'
import { BORDER_STYLE } from '../constants/Styles'
import Text from './Text'
import Icon from './Icon'
import Dropdown from './Dropdown'
import ListItem from './ListItem'
import Button from './Button'

const propsType = {
  key: PropTypes.any,
  rounded: PropTypes.bool,
  flex1: PropTypes.bool,
  backgroundColor: PropTypes.string,
  label: PropTypes.string,
  iconType: PropTypes.string,
  iconName: PropTypes.string,
  iconColor: PropTypes.string,
  iconSize: PropTypes.string,
  size: PropTypes.oneOf(keys(FONT_SIZE)),
  radius: PropTypes.number,
  multiline: PropTypes.bool,
  value: PropTypes.string,
  onChangeText: PropTypes.func,
  onLayout: PropTypes.func,
  placeholder: PropTypes.string,
  secureTextEntry: PropTypes.bool,
  selection: PropTypes.bool,
  selectionData: PropTypes.array,
  selectionDataBackground: PropTypes.string,
  suggestion: PropTypes.any,
  suggestionKey: PropTypes.string,
  suggestionPathResult: PropTypes.string,
  suggestionPathValue: PropTypes.string,
  suggestionCreateNewOnEmpty: PropTypes.bool,
  toggleSecureTextEntry: PropTypes.bool,
  inputStyle: PropTypes.object,
  editable: PropTypes.bool,
  noBorder: PropTypes.bool
}

const propsDefault = {
  size: 'small',
  isLight: false,
  noBorder: false,
  flex1: false,
  rounded: false,
  selection: false,
  selectionDataBackground: WHITE50,
  suggestion: '',
  suggestionCreateNewOnEmpty: true,
  backgroundColor: WHITE20,
  multiline: false,
  label: 'Your Input',
  placeholder: undefined,
  secureTextEntry: false,
  toggleSecureTextEntry: false,
  onLayout: () => {
  },
  editable: true
}

class InputText extends React.Component {
  constructor(props) {
    super(props)
  }

  state = {
    value: this.props.value,
    isLoading: false,
    isEditing: false,
    suggestionVisible: false,
    suggestionData: [],
    showValue: this.props.secureTextEntry
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.value !== this.props.value) {
      this.setState({
        value: this.props.value
      })
    }
  }

  _onChangeValue = (value, replace = false) => {
    const { onChangeText } = this.props
    const { isEditing } = this.state
    if (isEditing || replace) this.setState({ isEditing: false, selectedValue: replace ? null : value })
    onChangeText(value, replace)
  }

  _onChangeText = (text, fetching = true) => {
    const { onChangeText, suggestion } = this.props
    this.setState((state) => ({
      value: text,
      selectedText: !fetching ? text : state.selectedText
    }))
    if (!isFunction(suggestion)) onChangeText(text)
    if (fetching) this._onFetchSuggestion(text)
  }

  _onEndEditing = () => {
    const { onChangeText, suggestion } = this.props
    const { isEditing } = this.state
    if (isEditing && isFunction(suggestion)) {
      onChangeText('')
      this.setState((state) => ({
        value: state.selectedText || state.selectedValue || '',
        isEditing: false
      }))
    }
  }

  _onFocus = () => {
    const { value } = this.state
    this.setState({ isEditing: true }, () => {
      this._onFetchSuggestion(value)
    })
  }

  _onBlur = () => {
    this._onCloseSuggestion()
  }

  _onOpenSuggestion = () => {
    const { suggestion } = this.props
    if (isFunction(suggestion)) {
      this.setState({
        suggestionVisible: true
      })
    }
  }

  _onSelectSuggestion = (text, value) => () => {
    const { selection } = this.props
    if (!selection && text !== null && value !== null) {
      Keyboard.dismiss()
      this._onCloseSuggestion()
    }
    this._onChangeText(selection ? '' : text, false)
    this._onChangeValue(value || text)
  }

  _onCloseSuggestion = () => {
    const { suggestion } = this.props
    if (isFunction(suggestion)) {
      this.setState({
        isLoading: false,
        suggestionVisible: false
      })
    }
  }

  _onFetchSuggestion = (query = '') => {
    const { suggestion, suggestionKey } = this.props
    if (isFunction(suggestion)) {
      this.setState({
        isLoading: true
      }, () => {
        suggestion({
          [suggestionKey]: query,
          limit: 6,
          start: 0
        }).then(({ data }) => {
          const suggestionData = data.result || []
          this.setState({
            suggestionData,
            isLoading: false,
            suggestionVisible: true
          })
        })
      })
    }
  }

  render() {
    const {
      size,
      backgroundColor,
      rounded,
      radius,
      multiline,
      onLayout,
      label,
      placeholder,
      returnKeyType,
      toggleSecureTextEntry,
      selection,
      selectionData,
      selectionDataBackground,
      suggestionPathResult,
      suggestionPathValue,
      suggestionCreateNewOnEmpty,
      isLight,
      editable,
      inputStyle,
      flex1,
      noBorder
    } = this.props
    const {
      value,
      isLoading,
      showValue,
      suggestionVisible,
      suggestionData,
      selectedValue,
      inputTextWidth, inputTextHeight
    } = this.state

    return (
      <View style={flex1 ? { flex: 1 } : { width: '100%' }}>
        <View
          style={{ marginBottom: HP1, marginTop: HP105 }}
          ref={(ref) => {
            this.inputTextContainer = ref
          }}
          onLayout={({ nativeEvent }) => {
            if (this.inputTextContainer) {
              this.inputTextContainer.measure((x, y, width, height, pageX, pageY) => {
                this.setState({
                  inputTextHeight: height, inputTextWidth: width
                }, () => {
                  onLayout({ x, y, width, height, pageX, pageY })
                })
              })
            }
          }}
        >
          {label && (<Text
            size={isLight ? 'small' : 'large'} style={{ paddingHorizontal: isLight ? 0 : WP2 }}
            color={isLight ? GREY_WARM : WHITE} weight={isLight ? 300 : 400}
                     >{label}</Text>)}
          <View
            style={
              [
                {
                  backgroundColor,
                  padding: WP205,
                  paddingHorizontal: isLight ? 0 : WP305,
                  borderRadius: radius || rounded ? WP50 : WP105,
                  marginVertical: isLight ? 0 : HP1,
                  flexDirection: 'row',
                  alignItems: 'center'
                },
                isLight && !noBorder ? {
                  ...BORDER_STYLE['bottom']
                } : {}
              ]
            }
          >
            {
              selection && (
                <Icon
                  style={{ marginRight: WP305 }}
                  centered
                  name='search1' size='large' color={WHITE}
                />
              )
            }
            <TextInput
              returnKeyType={returnKeyType}
              value={value}
              style={[{
                flex: 1,
                color: isLight ? GREY : WHITE,
                fontSize: FONT_SIZE[size] || FONT_SIZE[propsDefault.size]
              }, inputStyle]}
              onFocus={this._onFocus}
              onBlur={this._onBlur}
              onEndEditing={this._onEndEditing}
              secureTextEntry={showValue}
              onChangeText={this._onChangeText}
              placeholder={placeholder}
              placeholderTextColor={SILVER}
              multiline={multiline}
              editable={editable}
            />
            {
              toggleSecureTextEntry && (
                <Icon
                  style={{ marginLeft: WP305 }}
                  centered
                  onPress={() => this.setState((state) => ({ showValue: !state.showValue }))}
                  name={showValue ? 'eye' : 'eye-with-line'} type='Entypo' size='large' color={WHITE}
                />
              )
            }
            {
              isLoading && (
                <ActivityIndicator size='small' color={WHITE}/>
              )
            }
          </View>
        </View>
        {
          suggestionVisible && (
            <Dropdown top={inputTextHeight} width={inputTextWidth}>
              <FlatList
                keyboardShouldPersistTaps='handled'
                keyExtractor={(item, index) => `${index}`}
                data={suggestionData}
                ListEmptyComponent={
                  !suggestionCreateNewOnEmpty ? (
                    <ListItem>
                      <Text
                        centered color={GREY50} style={{ fontStyle: 'italic' }}
                        size='mini'
                      >{`There is no suggestion for "${value}"`}</Text>
                    </ListItem>
                  ) : (
                    <View>
                      <ListItem>
                        <Text>{value}</Text>
                      </ListItem>
                      <Button
                        onPress={this._onSelectSuggestion(value)}
                        nativeEffect rounded centered
                        soundfren
                        compact='flex-end'
                        shadow='none'
                        textColor={WHITE}
                        textSize='small'
                        textWeight={300}
                        style={{ alignSelf: 'flex-end', marginRight: WP305 }}
                        text='Create New'
                      />
                    </View>
                  )
                }
                renderItem={({ item, index }) => (
                  <ListItem
                    inline
                    onPress={
                      selection && includes(selectionData, item[suggestionPathValue || suggestionPathResult])
                        ? null
                        : item[suggestionPathValue || suggestionPathResult] === selectedValue
                          ? this._onSelectSuggestion(null, null)
                          : this._onSelectSuggestion(item[suggestionPathResult], item[suggestionPathValue || suggestionPathResult])
                    }
                  >
                    <Text style={{ flex: 1 }} searchWords={[value]}>{item[suggestionPathResult]}</Text>
                    {
                      selection && includes(selectionData, item[suggestionPathValue || suggestionPathResult])
                        ? <Icon centered name='check'/>
                        : item[suggestionPathValue || suggestionPathResult] === selectedValue
                          ? <Icon centered name='check'/>
                          : null
                    }
                  </ListItem>
                )}
              />
            </Dropdown>
          )
        }
        {
          selection && (
            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
              {
                map(selectionData, (data, i) => (
                  <View
                    key={i}
                    style={{
                      margin: WP1, padding: WP1, paddingHorizontal: WP3,
                      backgroundColor: selectionDataBackground,
                      borderRadius: WP100,
                      flexDirection: 'row',
                      justifyContent: 'center'
                    }}
                  >
                    <Text style={{ marginRight: WP4 }} color={WHITE}>{data}</Text>
                    <Icon
                      centered color={WHITE} size='tiny' name='close'
                      onPress={() => {
                        remove(selectionData, (item) => item === data)
                        this._onChangeValue(selectionData, true)
                      }}
                    />
                  </View>
                ))
              }
            </View>
          )
        }
      </View>
    )
  }
}

InputText.propTypes = propsType

InputText.defaultProps = propsDefault

export default InputText
