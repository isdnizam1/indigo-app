import React from 'react'
import { View } from 'react-native'
import { isEmpty, isNil } from 'lodash-es'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { WP1, WP2, WP3, WP10 } from '../constants/Sizes'
import { SHIP_GREY } from '../constants/Colors'
import { hexToRGB } from '../utils/helper'
import Icon from './Icon'
import Text from './Text'
import Touchable from './Touchable'

const propsType = {
  borderColor: PropTypes.string,
  borderWidth: PropTypes.number,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  forceBorderColor: PropTypes.bool,
  icon: PropTypes.string,
  iconSize: PropTypes.string,
  iconType: PropTypes.string,
  leftComponent: PropTypes.any,
  onLayout: PropTypes.func,
  onPress: PropTypes.func,
  rightComponent: PropTypes.any,
  style: PropTypes.object,
  text: PropTypes.string,
  textColor: PropTypes.string,
  textSize: PropTypes.string,
  textWeight: PropTypes.number,
}

class ButtonV2 extends React.PureComponent {
  render() {
    const {
      borderColor,
      borderWidth,
      children,
      color,
      disabled,
      forceBorderColor,
      icon,
      iconColor,
      iconSize,
      iconType,
      leftComponent,
      onLayout,
      onPress,
      rightComponent,
      style,
      text,
      textColor,
      textSize,
      textWeight,
    } = this.props
    return (
      <Touchable
        useForeground
        disabled={!!disabled}
        onPress={disabled ? null : onPress}
        onLayout={onLayout}
        style={[
          {
            alignItems: 'center',
            backgroundColor: hexToRGB(color, disabled ? 0.5 : 1),
            borderColor:
              isNil(color) || forceBorderColor ? borderColor : hexToRGB(color, disabled ? 0.5 : 1),
            borderRadius: 7,
            borderWidth,
            flexDirection: 'row',
            justifyContent: 'center',
            paddingVertical: WP2 + 2,
            paddingHorizontal: WP3,
          },
          style,
        ]}
      >
        {icon && (
          <View>
            <Icon
              size={iconSize}
              style={{
                marginRight: isEmpty(text) ? 0 : WP1,
                height: WP10,
              }}
              color={iconColor}
              type={iconType}
              name={icon}
            />
          </View>
        )}
        {leftComponent && (React.isValidElement(leftComponent) ? leftComponent : leftComponent())}
        {!isEmpty(text) && (
          <Text
            size={textSize}
            color={textColor}
            type={'Circular'}
            numberOfLines={1}
            weight={textWeight}
          >
            {text}
          </Text>
        )}
        {children}
        {!!rightComponent &&
          (React.isValidElement(rightComponent) ? rightComponent : rightComponent())}
      </Touchable>
    )
  }
}

ButtonV2.propTypes = propsType
ButtonV2.defaultProps = {
  borderColor: 'rgb(223, 227, 232)',
  disabled: false,
  iconColor: 'rgb(189, 189, 189)',
  iconType: 'MaterialCommunityIcons',
  onLayout: () => {},
  onPress: () => {},
  textColor: SHIP_GREY,
  textSize: 'xmini',
  text: '',
  textWeight: 500,
  borderWidth: 1,
  forceBorderColor: false,
}
export default ButtonV2
