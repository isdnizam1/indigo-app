import { isString } from "util";
import React, { Component } from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, View, ScrollView, SafeAreaView } from "react-native";
import {
  isFunction,
  isEqual,
  isEmpty,
  reduce,
  isArray,
  includes,
  map,
  noop,
  difference,
} from "lodash-es";
import { connect } from "react-redux";
import { WP5 } from "sf-constants/Sizes";
import {
  WP1,
  WP2,
  WP4,
  HP100,
  WP3,
  WP105,
  HP50,
  WP205,
  WP6,
  WP24,
} from "../constants/Sizes";
import {
  WHITE,
  SHIP_GREY_CALM,
  REDDISH,
  NAVY_DARK,
  PALE_GREY,
  GUN_METAL,
  PALE_BLUE,
  SHIP_GREY,
  PALE_BLUE_TWO,
  GREEN_SOFT,
  GREEN30,
} from "../constants/Colors";
import { TOUCH_OPACITY } from "../constants/Styles";
import Modal from "./Modal";
import Text from "./Text";
import Icon from "./Icon";

import ButtonV2 from "./ButtonV2";

import { getJob } from "../actions/api";
import SelectModalFilterV2 from "./SelectModalFilterV2";
import { select } from "redux-saga/effects";

const propsType = {
  triggerComponent: PropTypes.objectOf(PropTypes.any),
  keyword: PropTypes.string,
  onChange: PropTypes.func,
  header: PropTypes.string,
  suggestion: PropTypes.func,
  renderItem: PropTypes.func,
  reformatFromApi: PropTypes.func,
  suggestionKey: PropTypes.string,
  suggestionPathResult: PropTypes.string,
  suggestionPathValue: PropTypes.string,
  asObject: PropTypes.bool,
  placeholder: PropTypes.string,
  refreshOnSelect: PropTypes.bool,
  extraResult: PropTypes.bool,
  iconName: PropTypes.string,
  iconType: PropTypes.string,
  iconSize: PropTypes.string,
  selection: PropTypes.any,
  selectionKey: PropTypes.string,
  selectionWording: PropTypes.string,
  showSelection: PropTypes.bool,
  onRemoveSelection: PropTypes.func,
  quickSearchTitle: PropTypes.string,
  quickSearchKey: PropTypes.string,
  actionNext: PropTypes.func,
  quickSearch: PropTypes.func,
  disabled: PropTypes.bool,
};

const propsDefault = {
  keyword: "",
  triggerComponent: {},
  onChange: () => {},
  header: "",
  suggestion: () => {},
  suggestionKey: "",
  suggestionPathResult: "",
  suggestionPathValue: "",
  asObject: false,
  createNew: true,
  placeholder: "",
  refreshOnSelect: false,
  extraResult: false,
  iconName: "",
  iconType: "AntDesign",
  iconSize: "small",
  selection: "",
  selectionWording: "",
  showSelection: false,
  onRemoveSelection: () => {},
  quickSearchTitle: "",
  actionNext: () => {},
  quickSearch: noop,
  disabled: false,
};

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

class SelectModalFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedJob: this.props.selected.length == 0 ? [] : this.props.selected,
      keyword: "",
      inputKey: "",
      suggestions: [],
      suggestionsRecent: [],
      showSuggestion: false,
      modalHeight: HP100,
    };
  }

  componentDidMount = () => {
    this._onFetchSuggestion();
  };

  _onFocus = () => {
    this.setState({ showSuggestion: true }, this._onFetchSuggestion);
  };

  _onBlur = () => {
    this.setState({ showSuggestion: false });
  };

  _onSelectSuggestion = (value, closeModalCallback) => {
    const {
      onChange,
      suggestionPathResult,
      asObject,
      refreshOnSelect,
      selection,
      selected,
      actionNext,
    } = this.props;

    const item = selected.find((value) => value.id_job === value.id_job);
    if (selected.length == 0) {
      this.setState({ keyword: value });
      onChange(value);
    } else {
      const newItem = selected.concat(value);
      this.setState({ keyword: newItem });
      onChange(newItem);
    }

    if (!isArray(selection)) {
      closeModalCallback();
      setTimeout(() => {
        actionNext();
      }, 500);
    } else {
      this._onFocus();
    }
  };

  _onCreateNewOption = (closeModalCallback) => {
    const { keyword } = this.state;
    const { onChange } = this.props;

    onChange(keyword);
    closeModalCallback();
  };

  _onFetchSuggestion = () => {
    const {
      suggestion,
      suggestionKey,
      suggestionType,
      userData: { id_user },
      suggestionPathResult,
      extraResult,
      reformatFromApi,
      suggestionPathValue,
      id_province,
    } = this.props;
    const { keyword } = this.state;
    if (isFunction(suggestion)) {
      let params = {
        [suggestionKey]: keyword,
        id_user,
        limit: 5,
        start: 0,
      };
      if (suggestionPathValue === "id_province") {
        params = {
          [suggestionKey]: keyword,
          limit: 5,
          start: 0,
        };
      } else if (id_province) {
        params = {
          ...params,
          id_province,
        };
      }
      suggestion(params)
        .then(({ data }) => {
          let suggestionData = data.result;
          if (reformatFromApi) {
            suggestionData = reduce(
              suggestionData,
              (result, item) => {
                result.push({
                  [suggestionPathResult]: reformatFromApi(
                    item[suggestionPathResult]
                  ),
                  [suggestionPathValue]: item[suggestionPathValue],
                });
                return result;
              },
              []
            );
          }
          let suggestionDataRecentlySearch = [];
          if (extraResult) {
            suggestionDataRecentlySearch = data.recentlySearch || [];
            if (!isEmpty(suggestionDataRecentlySearch)) {
              suggestionDataRecentlySearch = reduce(
                data.recentlySearch,
                (result, item) => {
                  result.push({
                    [suggestionPathResult]: reformatFromApi
                      ? reformatFromApi(item.value)
                      : item.value,
                    [suggestionPathValue]: item[suggestionPathValue],
                  });
                  return result;
                },
                []
              );
            }
          }
          this.setState({
            suggestions: suggestionData,
            suggestionsRecent: suggestionDataRecentlySearch,
            showSuggestion: true,
          });
        })
        .catch(() => {});
    }
  };

  _isSelected = (item) => {
    const { selected, suggestionPathResult } = this.props;

    if (isArray(selected))
      return includes(selected, item[suggestionPathResult]);
    else return isEqual(selected, item[suggestionPathResult]);
  };

  // RENDER LIST
  _suggestionList() {
    const {
      renderAsButtons,
      suggestionKey,
      showSelection,
      onRemoveSelection,
      selected,
    } = this.props;
    const { keyword, suggestions, selectedJob } = this.state;
    const data = selected.length == 0 ? suggestions : selectedJob;
    return (
      <ScrollView
        horizontal={!!renderAsButtons}
        showsHorizontalScrollIndicator={!renderAsButtons}
        showsVerticalScrollIndicator={!!renderAsButtons}
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={
          renderAsButtons
            ? {
                flex: 1,
                flexDirection: "row",
                flexWrap: "wrap",
              }
            : {
                paddingTop: WP4,
                paddingBottom: WP2,
              }
        }
      >
        <View
          style={{
            paddingBottom: WP3,
            flexDirection: "row",
          }}
        >
          <Text
            style={{ flex: 1 }}
            type="Circular"
            color={SHIP_GREY}
            size="mini"
            weight={400}
          >
            Profession
          </Text>

          <SelectModalFilterV2
            refreshOnSelect
            triggerComponent={
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() => {}}
                style={{
                  alignItems: "flex-end",
                }}
              >
                <Text type="Circular" size="xmini" weight={400} color={GREEN30}>
                  See All
                </Text>
              </TouchableOpacity>
            }
            header="Profession"
            suggestion={getJob}
            suggestionKey="job_title"
            suggestionPathResult="job_title"
            suggestionPathValue="id_job"
            onChange={async (value) => {
              const selection = difference(value, selectedJob);
              await this.setState({
                selectedJob: selectedJob.concat(selection),
              });
            }}
            createNew={true}
            placeholder="Find Profession"
            selection={selected}
            selectionWording="Interest Terpilih"
            showSelection
            onRemoveSelection={(index) => {}}
            quickSearchTitle={"Saran Pencarian Interest"}
            // quickSearchList={suggestionGenre}
          />
        </View>

        {showSelection && (
          <View style={{ paddingVertical: !isEmpty(keyword) ? WP4 : 0 }}>
            <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
              {map(data, (itemSelection, index) => {
                let isSelected = selectedJob.find(
                  (item) => item.id_job === itemSelection.id_job
                );

                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      // toggleModal();
                      isSelected = true;
                    }}
                    activeOpacity={TOUCH_OPACITY}
                    style={{
                      marginRight: WP2,
                      marginBottom: WP2,
                      flexDirection: "row",
                      alignItems: "center",
                      paddingHorizontal: WP3,
                      paddingVertical: WP2,
                      borderRadius: 6,
                      backgroundColor:
                        isSelected == undefined ? WHITE : GREEN30,
                      borderWidth: 1,
                      borderColor: PALE_BLUE_TWO,
                    }}
                  >
                    <Text
                      size="xmini"
                      type="Circular"
                      weight={400}
                      color={isSelected == undefined ? SHIP_GREY : WHITE}
                      style={{ marginRight: 0 }}
                    >
                      {suggestionKey
                        ? itemSelection[suggestionKey]
                        : itemSelection}
                    </Text>
                  </TouchableOpacity>
                );
              })}
            </View>
          </View>
        )}
      </ScrollView>
    );
  }
  // END RENDER LIST

  render() {
    const { triggerComponent, header, disabled } = this.props;

    const { showSuggestion, selectedJob } = this.state;

    return (
      <Modal
        renderModalContent={({ toggleModal }) => (
          <SafeAreaView style={{ height: HP50 }}>
            <View
              style={{
                backgroundColor: WHITE,
                height: HP50,
                borderTopStartRadius: 50,
                borderTopEndRadius: 50,
              }}
            >
              <View style={{ flex: 1 }}>
                {/* header */}
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    paddingHorizontal: WP4,
                    paddingVertical: WP6,
                    borderBottomWidth: 1,
                    borderBottomColor: PALE_BLUE,
                  }}
                >
                  <Text
                    type="Circular"
                    size="medium"
                    weight={600}
                    color={NAVY_DARK}
                  >
                    {header}
                  </Text>
                  <Icon
                    centered
                    onPress={toggleModal}
                    background="dark-circle"
                    size="large"
                    color={SHIP_GREY_CALM}
                    name="close"
                    type="MaterialCommunityIcons"
                  />
                </View>

                {/* body */}
                <View
                  style={{ flex: 1, paddingHorizontal: WP4, paddingTop: WP4 }}
                >
                  {/*Suggestion*/}
                  {showSuggestion &&
                    this._suggestionList(
                      //   suggestions,
                      toggleModal
                    )}
                </View>
              </View>

              {/* footer */}
              <View
                style={{
                  backgroundColor: WHITE,
                  flexDirection: "row",
                  width: "100%",
                  paddingHorizontal: WP4,
                  paddingVertical: WP5,
                  height: WP24,
                  alignItems: "center",
                }}
              >
                <ButtonV2
                  color={GREEN_SOFT}
                  textColor={GREEN30}
                  textSize={"slight"}
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: WP5,
                    marginRight: WP2,
                  }}
                  onPress={() => {}}
                  text={"Reset"}
                />
                <ButtonV2
                  color={GREEN30}
                  textColor={WHITE}
                  textSize={"slight"}
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: WP5,
                  }}
                  onPress={() => {
                    this._onSelectSuggestion(selectedJob, toggleModal);
                  }}
                  text={"Apply"}
                />
              </View>
            </View>
          </SafeAreaView>
        )}
      >
        {({ toggleModal, isVisible }, M) => (
          <View style={{ flexGrow: 1 }}>
            <TouchableOpacity
              activeOpacity={disabled ? 1 : TOUCH_OPACITY}
              onPress={() => {
                if (!this.props.disabled) {
                  if (!isVisible) this._onFetchSuggestion();
                  toggleModal();
                }
              }}
            >
              <View pointerEvents="none">{triggerComponent}</View>
            </TouchableOpacity>
            {M}
          </View>
        )}
      </Modal>
    );
  }
}

SelectModalFilter.propTypes = propsType;

SelectModalFilter.defaultProps = propsDefault;

export default connect(mapStateToProps, {})(SelectModalFilter);
