import React from 'react'
import { View, ImageBackground, TouchableOpacity } from 'react-native'
import { WP5, HP1, WP1, WP25, WP100, HP05, WP3 } from '../../constants/Sizes'
import Text from '../Text'
import { GREY, GREY_STEEL, WHITE, WHITE_MILK, TEAL } from '../../constants/Colors'
import { TOUCH_OPACITY } from '../../constants/Styles'

const WorksExperienceItem = (props) => {
  const {
    data,
    renderAction,
    navigateTo
  } = props

  const additionalData = JSON.parse(data.additional_data)

  _getImageFromMedia = (media) => {
    const images = []
    media.map((item) => {
      const image = {
        url: item
      }
      images.push(image)
    })
    return images
  }

  return (
    <TouchableOpacity
      activeOpacity={TOUCH_OPACITY}
      style={{
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-around',
        width: WP100,
        padding: WP5,
        flexDirection: 'row'
      }}
      onPress={() => navigateTo('DetailExperienceScreen', { data })}
    >

      {
        additionalData.media.slice(0, 1).map((item, index) => (
          <TouchableOpacity
            key={`${index}${new Date()}`}
            onPress={() => navigateTo('ImagePreviewScreen', { images: _getImageFromMedia(additionalData.media) })}
          >
            <ImageBackground
              style={{
                justifyContent: 'center', backgroundColor: WHITE_MILK,
                width: WP25, height: undefined, aspectRatio: 1,
                alignItems: 'center'
              }}
              imageStyle={{ borderRadius: 12 }}
              source={{ uri: item }}
            />
          </TouchableOpacity>
        ))
      }
      <View style={{ flex: 1, paddingLeft: WP5 }}>
        {
          data.type === 'music_journey' && (
            <>
              <View style={{ backgroundColor: TEAL, borderRadius: WP100, paddingHorizontal: WP3, paddingVertical: WP1, alignSelf: 'baseline' }}>
                <Text size='tiny' type='NeoSans' weight={500} color={WHITE}>Music Experience</Text>
              </View>
              <Text size='tiny' weight={500} color={GREY} style={{ marginTop: HP1 }}>{additionalData.role}</Text>
              <Text size='tiny' color={GREY} style={{ marginTop: HP05 }}>at {additionalData.company}</Text>
              <Text size='tiny' color={GREY_STEEL} style={{ marginTop: HP05 }}>{additionalData.date_join?.start} - {additionalData.date_join?.until}</Text>
            </>
          )
        }
        {
          data.type === 'performance_journey' && (
            <>
              <View style={{ backgroundColor: TEAL, borderRadius: WP100, paddingHorizontal: WP3, paddingVertical: WP1, alignSelf: 'baseline' }}>
                <Text size='tiny' type='NeoSans' weight={500} color={WHITE}>Performance Journey</Text>
              </View>
              <Text size='tiny' weight={500} color={GREY} style={{ marginTop: HP1 }}>{data.title}</Text>
              <Text size='tiny' color={GREY} style={{ marginTop: HP05 }}>at {additionalData.event_location}</Text>
              <Text size='tiny' color={GREY_STEEL} style={{ marginTop: HP05 }}>{additionalData.event_date}</Text>
            </>
          )
        }
        {/* <Text weight={500} color={GREY} size='tiny' numberOfLines={1}>{data.title}</Text>
        {
          !isEmpty(artist)
            && <Text color={GREY} size='tiny' style={{ marginTop: HP05 }}>Artist: {artist}</Text>
        }
        {
          data.description
            ? <Text color={GREY_WARM} size='tiny' style={{ marginTop: HP05 }}>{data.description}</Text>
            : <Text color={GREY_WARM} size='tiny' style={{ marginTop: HP05 }}>No description</Text>
        } */}
      </View>
      <View style={{ alignSelf: 'flex-start' }}>
        {renderAction}
      </View>
    </TouchableOpacity>
  )
}

export default WorksExperienceItem