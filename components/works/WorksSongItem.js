import React, { useState, useCallback, useEffect } from 'react'
import { View, ImageBackground, TouchableOpacity } from 'react-native'
import { isEmpty } from 'lodash-es'
import Axios from 'axios'
import { WP5, WP1, WP25, WP100, HP05 } from '../../constants/Sizes'
import Text from '../Text'
import { GREY, GREY_WARM, WHITE, WHITE_MILK, PINK_RED } from '../../constants/Colors'
import Button from '../Button'
import { NavigateToInternalBrowser } from '../../utils/helper'
import { Image } from '..'

const WorksSongItem = (props) => {
  const {
    data,
    renderAction,
    playerOpts
  } = props

  const additionalData = JSON.parse(data.additional_data)
  const isAppPlayer = additionalData.in_app_player === 1
  const url = additionalData.url_song
  const icon = additionalData.icon
  const cover = additionalData.cover_image
  const artist = additionalData.artist_name
  const { toggle, isPlaying, changeAndPlay, songUrl, isLoading } = playerOpts
  const isSameSong = url === songUrl

  const _renderLoadingButtonPlay = () => (
    <Image
      source={require('../../assets/loading_music.gif')}
      size='mini'
      style={{ marginVertical: 0, flexGrow: 0 }}
    />
  )

  const [remoteImage, setRemoteImage] = useState('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=')

  const findCover = useCallback(() => {
    Axios.get(url).then(({ data }) => {
      const regMatch = data.match(/(og|twitter)?:image.+?>/ig)
      if(regMatch && typeof regMatch === 'object' && regMatch.length > 0) {
        // eslint-disable-next-line no-useless-escape
        const urls = regMatch[0].match(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig)
        if(urls && typeof urls === 'object' && urls.length > 0) {
          setRemoteImage(urls[0])
        }
      }
    })
  }, [setRemoteImage])

  useEffect(() => {
    findCover()
  }, [data])

  return (
    <View
      style={{
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-around',
        width: WP100,
        padding: WP5,
        flexDirection: 'row'
      }}
    >
      <TouchableOpacity
        onPress={async () => {
          if (isAppPlayer) {
            NavigateToInternalBrowser({
              url
            })
          }
        }}
      >
        <ImageBackground
          imageStyle={{ borderRadius: 12 }}
          style={{
            justifyContent: 'center', backgroundColor: WHITE_MILK,
            width: WP25, height: undefined, aspectRatio: 1,
            alignItems: 'center'
          }}
          source={{ uri: cover || remoteImage }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            {
              isAppPlayer && isLoading && isSameSong ?
                _renderLoadingButtonPlay()
                : isAppPlayer ?
                  <Button
                    onPress={() => {
                      if (isSameSong) toggle()
                      else {
                        changeAndPlay(url)
                      }
                    }}
                    iconName={(isPlaying && isSameSong) ? 'pause' : 'play'}
                    iconType='MaterialCommunityIcons'
                    style={{ marginVertical: 0 }}
                    contentStyle={{ padding: WP1 }}
                    shadow='none'
                    colors={[WHITE, WHITE]}
                    rounded
                    iconColor={PINK_RED}
                  /> :
                  <Image size='xsmall' source={{ uri: icon }} style={{ marginVertical: 0 }} />
            }
          </View>
        </ImageBackground>
      </TouchableOpacity>
      <View style={{ flex: 1, paddingLeft: WP5 }}>
        <Text weight={500} color={GREY} size='tiny' numberOfLines={1}>{data.title}</Text>
        {
          !isEmpty(artist)
            && <Text color={GREY} size='tiny' style={{ marginTop: HP05 }}>Artist: {artist}</Text>
        }
        {
          data.description
            ? <Text color={GREY_WARM} size='tiny' style={{ marginTop: HP05 }}>{data.description}</Text>
            : <Text color={GREY_WARM} size='tiny' style={{ marginTop: HP05 }}>No description</Text>
        }
      </View>
      <View style={{ alignSelf: 'flex-start' }}>
        {renderAction}
      </View>
    </View>
  )
}

export default WorksSongItem