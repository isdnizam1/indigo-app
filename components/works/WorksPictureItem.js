import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { WP2, WP5, WP50, WP30 } from '../../constants/Sizes'
import Text from '../Text'
import { GREY } from '../../constants/Colors'
import { Image } from '..'

const WorksPictureItem = (props) => {
  const {
    data: groupedArt,
    renderAction,
    navigateTo
  } = props
  const data = groupedArt[0]
  return (
    <TouchableOpacity
      onPress={
        () => navigateTo(
          'ProfileShowArtScreen',
          { idJourney: data.id_journey },
          'push'
        )
      }
      style={{
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
        justifyContent: 'space-around',
        width: WP50,
        padding: WP5,
        flexDirection: 'row'
      }}
    >
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <View style={{
          borderRadius: 12,
          marginBottom: WP2,
        }}
        >
          <View style={{
            borderRadius: 12,
            overflow: 'hidden',
          }}
          >
            <Image
              source={{ uri: data.attachment_file }}
              imageStyle={{ width: WP30, aspectRatio: 1 }}
            />
          </View>
        </View>

        <View style={{
          paddingHorizontal: WP2,
          marginBottom: WP2,
        }}
        >
          <Text centered size='tiny' color={GREY} weight={500} numberOfLines={2}>{data.title}</Text>
          <Text centered size='xtiny' numberOfLines={2}>{data.description}</Text>
        </View>
      </View>
      <View style={{ alignSelf: 'flex-start' }}>
        {renderAction}
      </View>
    </TouchableOpacity>
  )
}

export default WorksPictureItem