import React from 'react'
import { View, ImageBackground } from 'react-native'
import { WP5, WP30, HP1, WP1, WP40, WP100 } from '../../constants/Sizes'
import Text from '../Text'
import { GREY_WARM, WHITE, WHITE_MILK, PINK_RED } from '../../constants/Colors'
import Button from '../Button'
import { NavigateToInternalBrowser } from '../../utils/helper'

const WorksVideoItem = (props) => {
  const {
    data,
    renderAction
  } = props

  const additionalData = JSON.parse(data.additional_data)
  const imageUrl = additionalData.url_video
  const temp = imageUrl.split('/')
  let ytImageUrl = 'broken'
  if (temp.length > 1) {
    ytImageUrl = temp[temp.length - 1]
    const temps = ytImageUrl.split('=')
    if (temps.length > 1) {
      ytImageUrl = temps[1]
    }
  }
  ytImageUrl = `https://img.youtube.com/vi/${ytImageUrl}/default.jpg`
  return (
    <View
      style={{
        alignItems: 'flex-start',
        alignSelf: 'center',
        justifyContent: 'space-around',
        width: WP100,
        padding: WP5,
        flexDirection: 'row'
      }}
    >
      <ImageBackground
        imageStyle={{ borderRadius: 12 }}
        style={{
          justifyContent: 'center', backgroundColor: WHITE_MILK,
          width: WP30, height: undefined, aspectRatio: 100/56,
          alignItems: 'center'
        }}
        source={{ uri: ytImageUrl }}
      >
        <Button
          onPress={() => {
            NavigateToInternalBrowser({
              url: imageUrl
            })
          }}
          iconName='controller-play'
          iconType='Entypo'
          style={{ marginVertical: 0 }}
          contentStyle={{ padding: WP1 }}
          shadow='none'
          colors={[WHITE, WHITE]}
          rounded
          iconColor={PINK_RED}
        />
      </ImageBackground>
      <View
        style={{
          width: WP40,
          justifyContent: 'space-around'
        }}
      >
        {
          !!data.title && (
            <Text style={{ marginBottom: HP1 }} size='tiny' weight={500} numberOfLines={1}>{data.title}</Text>
          )
        }
        {
          !!data.description && (
            <Text numberOfLines={2} size='xtiny' color={GREY_WARM}>{data.description}</Text>
          )
        }
      </View>
      <View style={{ alignSelf: 'flex-start' }}>
        {renderAction}
      </View>
    </View>
  )
}

export default WorksVideoItem