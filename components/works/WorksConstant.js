import { StyleSheet } from 'react-native'
import { HP1, HP2, WP27, WP3, WP4, WP405, WP5, WP6 } from '../../constants/Sizes'
import { SILVER_CALM, WHITE } from '../../constants/Colors'
import { BORDER_STYLE } from '../../constants/Styles'
import WorksPictureItem from './WorksPictureItem'
import WorksVideoItem from './WorksVideoItem'
import WorksSongItem from './WorksSongItem'
import WorksExperienceItem from './WorksExperienceItem'

export const WORKSTYLE = StyleSheet.create({
  sectionWrapper: {
    paddingHorizontal: WP6,
    paddingVertical: HP2,
    backgroundColor: WHITE,
    marginBottom: 5
  },
  detailWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 1
  },
  detailItem: {
    flexGrow: 0,
    marginRight: WP4
  },
  detailIconWrapper: {
    width: WP5
  },
  detailIcon: {},
  titleWrapper: {
    paddingHorizontal: WP6,
    paddingTop: HP2,
    backgroundColor: WHITE
  },
  title: {
    marginBottom: HP2
  },
  header: {
    marginBottom: HP1
  },
  songWrapper: {
    flex: 1, flexDirection: 'row',
    marginBottom: 4,
    alignItems: 'center',
    ...BORDER_STYLE['top'],
    paddingTop: 6,
    paddingBottom: 3
  },
  divider: {
    height: 6,
    backgroundColor: SILVER_CALM
  }
})

export const WORKSPAGE = {
  song: {
    title: 'Song',
    addCaption: 'Add Song',
    bannerImage: require('../../assets/images/songBanner.png'),
    itemSelector: '',
    idPerItem: 'id_journey',
    item: WorksSongItem
  },
  video: {
    title: 'Video',
    addCaption: 'Add Video',
    bannerImage: require('../../assets/images/videoBanner.jpg'),
    itemSelector: '',
    idPerItem: 'id_journey',
    item: WorksVideoItem
  },
  picture: {
    title: 'Picture',
    addCaption: 'Add Picture',
    bannerImage: require('../../assets/images/pictureBanner.jpg'),
    numColumns: 2,
    itemSelector: '',
    idPerItem: '[0].id_journey',
    item: WorksPictureItem
  },
  journey: {
    title: 'Music Experience',
    addCaption: 'Add Experience',
    itemSelector: 'music',
    bannerImage: require('../../assets/images/musicBanner.jpg'),
    idPerItem: 'id_journey',
    item: WorksExperienceItem
  },
  performance: {
    title: 'Performance',
    addCaption: 'Add Experience',
    itemSelector: 'performance',
    bannerImage: require('../../assets/images/performanceBanner.jpg'),
    idPerItem: 'id_journey',
    item: WorksExperienceItem
  },
  null: {

  }
}

export const collaborationStyle = StyleSheet.create({
  header: {
    paddingHorizontal: WP6,
    paddingVertical: WP3,
    flexDirection: 'row'
  },
  body: {
    paddingHorizontal: WP6,
    paddingVertical: WP4
  },
  photoProfileWrapper: {
    position: 'absolute',
    top: -(WP6),
    left: WP6
  },
  photoProfile: {
    width: WP27,
    height: WP27
  },
  title: {
    marginBottom: 3
  },
  headerDetail: {
    paddingLeft: WP405
  },
  headerText: {
    marginBottom: 3
  },
  bodyHeading: {}
})
