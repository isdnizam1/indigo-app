import React, { useEffect, useRef } from 'react'
import { View, Animated, Dimensions, Image as RNImage } from 'react-native'
import PropTypes from 'prop-types'
import { TOMATO, GREY80, WHITE, TRANSPARENT } from '../constants/Colors'
import { HP1, HP4 } from '../constants/Sizes'
import style from '../styles/components/ConfirmationPopup'
import Button from './Button'
import Text from './Text'

const ConfirmationPopup = (props) => {

  let { height } = Dimensions.get('window')
  let { visible, title, message, onConfirm, onCancel, confirmText, cancelText, cancelStyle } = props
  let fadeAnim = useRef(new Animated.Value(0)).current
  let slideAnim = useRef(new Animated.Value(0)).current
  let visibleAnim = useRef(new Animated.Value(height)).current

  useEffect(() => {
    setTimeout(() => {
      Animated.timing(fadeAnim, {
        toValue: visible ? 1 : 0,
        duration: visible ? 250 : 400,
        useNativeDriver: false
      }).start()
    }, visible ? 50 : 450)
    setTimeout(() => {
      Animated.timing(slideAnim, {
        toValue: visible ? 0 : -height * 2,
        duration: visible ? 500 : 350,
        useNativeDriver: false
      }).start()
    }, visible ? 300 : 50)
    setTimeout(() => {
      Animated.timing(visibleAnim, {
        toValue: visible ? 0 : -height,
        duration: 0,
        useNativeDriver: false
      }).start()
    }, visible ? 0 : 850)
  }, [visible])

  return (
    <Animated.View style={[style.wrapper, { top: visibleAnim }]}>
      <Animated.View style={[style.modal, { opacity: fadeAnim }]} />
      <Animated.View style={[style.content, { bottom: slideAnim }]}>
        {/*<View style={{ width: WP100 }}>
          <TouchableOpacity onPress={onCancel} style={{ paddingBottom: HP1, marginBottom: HP2, paddingHorizontal: WP5 }}>
            <RNImage style={style.closeIcon} source={require('../assets/icons/close_circle_gray.png')} />
          </TouchableOpacity>
        </View>*/}
        <RNImage style={{ marginBottom: HP1, marginTop: HP4 }} source={require('../assets/images/areYouSure.png')} />
        <Text style={{ marginVertical: HP1 }} weight={700} color={GREY80} size={'large'}>{title}</Text>
        <Text color={GREY80} size={'tiny'}>{message}</Text>
        <View style={style.buttonWrapper}>
          <View style={[style.buttonContainer, cancelStyle || null]}>
            <Button
              style={{ marginTop: HP4, borderWidth: 1.8, backgroundColor: WHITE, borderRadius: 17, borderColor: TOMATO }}
              radius={15}
              shadow={'none'}
              colors={[TRANSPARENT, TRANSPARENT]}
              onPress={onCancel}
              text={cancelText || 'Cancel'}
              textColor={WHITE}
              textCentered={true}
              textSize={'small'}
              textStyle={{ width: '100%', paddingVertical: HP1, color: TOMATO }}
              textWeight={500}
            />
          </View>
          <View style={style.buttonContainer}>
            <Button
              style={{ marginTop: HP4, borderWidth: 1.8, borderRadius: 17, backgroundColor: TOMATO, borderColor: TOMATO }}
              radius={15}
              shadow={'none'}
              colors={[TOMATO, TOMATO]}
              onPress={onConfirm}
              text={confirmText || 'Yes'}
              textColor={WHITE}
              textCentered={true}
              textSize={'small'}
              textStyle={{ width: '100%', paddingVertical: HP1 }}
              textWeight={500}
            />
          </View>
        </View>
      </Animated.View>
    </Animated.View>
  )

}

ConfirmationPopup.defaultProps = {
  title: null,
  message: null,
  onConfirm: null,
  onCancel: null,
  visible: false
}

ConfirmationPopup.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  onConfirm: PropTypes.func,
  onCancel: PropTypes.func,
  visible: PropTypes.bool
}

export default ConfirmationPopup