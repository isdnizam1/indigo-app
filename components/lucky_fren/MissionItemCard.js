import React, { memo } from 'react'
import { View } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { GUN_METAL, PALE_BLUE_TWO, PALE_GREY_THREE, PALE_GREY_TWO, PALE_LIGHT_BLUE_TWO, PALE_SALMON, REDDISH, SHIP_GREY, SHIP_GREY_CALM, SKELETON_COLOR, SKELETON_HIGHLIGHT, WHITE } from '../../constants/Colors'
import { HP05, HP1, WP05, WP1, WP105, WP11, WP2, WP3, WP305, WP35, WP4, WP40, WP44, WP5 } from '../../constants/Sizes'
import Image from '../Image'
import { SHADOW_STYLE } from '../../constants/Styles'
import Avatar from '../Avatar'
import Text from '../Text'
import Icon from '../Icon'
import RankingLabel from './RankingLabel'

const MissionItemCard = ({
  loading,
  item,
  isExpired,
  withPoint = true
}) => {

  const isChecked = item.quantity == item.completed_quantity

  return (
    <View
      style={{
        marginVertical: HP1,
        paddingVertical: WP2,
        borderRadius: 6,
        borderBottomEndRadius: 6,
        borderBottomStartRadius: 6,
        backgroundColor: WHITE,
        elevation: 1,
        ...SHADOW_STYLE['shadowThin']
      }}
    >
      <View
        style={{
          borderRadius: 6,
          borderBottomEndRadius: 6,
          borderBottomStartRadius: 6,
          overflow: 'hidden',
        }}
      >
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            paddingHorizontal: WP4,
            paddingVertical: WP3,
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ marginRight: WP3 }}>
              {
                isChecked ? (
                  <Image
                    style={{ width: '100%', top: 0, bottom: 0 }}
                    size={WP5} source={require('sf-assets/icons/tickDoubleGreen.png')}
                  />
                ) : (<View
                  style={{ backgroundColor: PALE_BLUE_TWO, width: WP5, height: WP5, borderRadius: WP5 / 2, padding: WP05, overflow: 'hidden' }}
                     >
                  <View
                    style={{ backgroundColor: PALE_GREY_TWO, width: '100%', height: '100%', borderRadius: (WP5 - WP05) / 2, justifyContent: 'center', alignItems: 'center' }}
                  />
                </View>)
              }
            </View>
          </View>
          <SkeletonContent
            containerStyle={{ flex: 1 }}
            layout={[
              { width: WP40, height: WP4, marginBottom: WP105 },
              { width: WP35, height: WP3 },
            ]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <View>
              <Text numberOfLines={1} type='Circular' color={GUN_METAL} size='mini' weight={500} style={{ marginBottom: WP05 }}>{item.mission_name}</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: HP05 }}>
                <Text numberOfLines={1} lineHeight={WP5} type='Circular' size='xmini' color={SHIP_GREY_CALM} weight={400}>{item.completed_quantity}/{item.quantity} {item.label_type}</Text>
                {
                  (withPoint && item.total_point) && (
                    <>
                      <View style={{ width: WP05, height: WP05, borderRadius: WP05 / 2, backgroundColor: SHIP_GREY_CALM, marginHorizontal: WP2 }} />
                      <Image size='xtiny' style={{ marginRight: WP1 }} source={require('../../assets/icons/icPoint.png')} />
                      <Text numberOfLines={1} lineHeight={WP5} type='Circular' size='xmini' color={SHIP_GREY} weight={400}>+{item.total_point} poin</Text>
                    </>
                  )
                }
              </View>
              {!isChecked && !isExpired && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text numberOfLines={1} type='Circular' color={REDDISH} size='xmini' style={{ marginBottom: WP05 }}>{item.action_label}</Text>
                <View>
                  <Icon color={REDDISH} type={'MaterialCommunityIcons'} name={'chevron-right'} />
                </View>
              </View>}
            </View>
          </SkeletonContent>
        </View>
      </View>
    </View>
  )
}

export default memo(MissionItemCard)
