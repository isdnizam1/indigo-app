import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { WP1, WP105, WP5 } from '../../constants/Sizes'
import Image from '../Image'
import Text from '../Text'
import { SHIP_GREY } from '../../constants/Colors'

const Point = ({ value, iconSize, textSize }) => (
  <View style={{ marginTop: WP105, flexDirection: 'row', alignItems: 'center', }}>
    <Image size={iconSize} style={{ marginRight: WP1 }} source={require('sf-assets/icons/icPoint.png')} />
    <Text numberOfLines={1} lineHeight={WP5} type='Circular' size={textSize} color={SHIP_GREY} weight={400}>{value}</Text>
  </View>
)

Point.propTypes = {
  value: PropTypes.string,
  iconSize: PropTypes.string,
  textSize: PropTypes.string
}

Point.defaultProps = {
  value: '',
  iconSize: 'xtiny',
  textSize: 'xmini'
}

export default Point
