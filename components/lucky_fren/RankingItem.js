import React from 'react'
import { View } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { GUN_METAL, SHIP_GREY, SKELETON_COLOR, SKELETON_HIGHLIGHT, WHITE } from '../../constants/Colors'
import { WP105, WP3, WP35, WP4, WP05, WP1, WP5, WP40, WP305 } from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'
import Avatar from '../Avatar'
import Image from '../Image'
import Text from '../Text'
import RankingLabel from './RankingLabel'

const RankingItem = ({
  loading,
  isList = true,
  image,
  name,
  point,
  position,
  latest_position = 'constant'
}) => {
  return (
    <View
      style={{
        borderRadius: 6,
        borderBottomEndRadius: isList ? 6 : 0,
        borderBottomStartRadius: isList ? 6 : 0,
        backgroundColor: WHITE,
        ...SHADOW_STYLE[isList ? 'shadowThin' : 'none'],
      }}
    >
      <View
        style={{
          borderRadius: 6,
          borderBottomEndRadius: isList ? 6 : 0,
          borderBottomStartRadius: isList ? 6 : 0,
          overflow: 'hidden',
        }}
      >
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: WP4,
            paddingVertical: isList ? WP3 : WP305,
          }}
        >
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ marginRight: WP3 }}>
              <Avatar image={image} isLoading={loading} />
            </View>
            <SkeletonContent
              containerStyle={{ flex: 1 }}
              layout={[
                { width: WP40, height: WP4, marginBottom: WP105 },
                { width: WP35, height: WP3 },
              ]}
              isLoading={loading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View>
                <Text numberOfLines={1} type='Circular' color={GUN_METAL} size='mini' weight={500} style={{ marginBottom: WP05 }}>{name}</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Image size='xtiny' style={{ marginRight: WP1 }} source={require('../../assets/icons/icPoint.png')} />
                  <Text numberOfLines={1} lineHeight={WP5} type='Circular' size='xmini' color={SHIP_GREY} weight={400}>{point} points</Text>
                  {
                    ['down', 'up'].includes(latest_position) ? (
                      <Image size='badge' style={{ marginLeft: WP1 }} source={latest_position == 'down' ? require('../../assets/icons/icRankingDown.png') : require('../../assets/icons/icRankingUp.png')} />
                    ) : null
                  }
                </View>
              </View>
            </SkeletonContent>
          </View>
          <RankingLabel position={position} />
        </View>
      </View>
    </View>
  )
}

export default RankingItem
