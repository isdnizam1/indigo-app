import React, { memo } from 'react'
import { LinearGradient } from 'expo-linear-gradient'
import Image from '../Image'
import { WP1, WP105, WP6 } from '../../constants/Sizes'
import {
  BRIGHT_LIGHT_BLUE,
  IRIS,
  LAVENDER_BLUE,
  MELON,
  MELON_TWO,
  ORANGE_YELLOW,
  PALE_GREY_THREE,
  SHIP_GREY_CALM,
  SOFT_BLUE_TWO,
  WARM_PINK,
  WHITE,
} from '../../constants/Colors'
import Text from '../Text'

const RankingLabel = ({ position }) => {
  const rank_3 = [MELON, ORANGE_YELLOW],
    range_10 = [MELON_TWO, WARM_PINK],
    range_20 = [IRIS, LAVENDER_BLUE],
    range_30 = [SOFT_BLUE_TWO, BRIGHT_LIGHT_BLUE],
    rank_other = [PALE_GREY_THREE, PALE_GREY_THREE],
    backgroundColor =
      position <= 3
        ? rank_3
        : position <= 10
        ? range_10
        : position <= 20
        ? range_20
        : position <= 30
        ? range_30
        : rank_other,
    contentColor = position <= 30 ? WHITE : SHIP_GREY_CALM,
    icon =
      position <= 30
        ? require('../../assets/icons/icTrophyWhite.png')
        : require('../../assets/icons/icTrophyGrey.png')
  return position ? (
    <LinearGradient
      colors={backgroundColor}
      start={[1, 0]}
      end={[0, 0]}
      style={{
        paddingHorizontal: WP105,
        paddingVertical: WP1,
        borderRadius: 6,
        flexDirection: 'row',
        alignItems: 'center',
      }}
    >
      <Image source={icon} size='badge' style={{ marginRight: WP1 }} />
      <Text
        type='Circular'
        size='xmini'
        weight='500'
        color={contentColor}
      >{`#${position}`}</Text>
    </LinearGradient>
  ) : (
    <LinearGradient
      colors={backgroundColor}
      start={[1, 0]}
      end={[0, 0]}
      style={{
        width: WP6 * 2,
        height: WP6,
        borderRadius: 6,
        flexDirection: 'row',
        alignItems: 'center',
      }}
    />
  )
}

export default memo(RankingLabel)
