import React, { memo } from 'react'
import { View } from 'react-native'
import { PALE_GREY_THREE } from '../../constants/Colors'
import { WP4, WP44 } from '../../constants/Sizes'
import Image from '../Image'

const MissionItem = ({ loading, image, width, style, isExpired }) => {
  const imageWidth = width || WP44,
    imageHeight = width || WP44,
    aspectRatio = 1,
    radius = 6

  return (
    <View
      style={[
        {
          marginRight: WP4,
          width: imageWidth,
        },
        style,
      ]}
    >
      <View
        style={{
          width: imageWidth,
          height: imageHeight,
          backgroundColor: PALE_GREY_THREE,
          borderRadius: radius,
        }}
      >
        {!loading && (
          <View>
            <View
              style={{
                borderRadius: radius,
                overflow: 'hidden',
              }}
            >
              <Image
                source={{ uri: image }}
                imageStyle={{ height: imageHeight, aspectRatio }}
              />
            </View>
          </View>
        )}
      </View>
    </View>
  )
}

export default memo(MissionItem)
