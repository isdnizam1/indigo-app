import React, { memo } from 'react'
import { View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { LIPSTICK_TWO, MELON, ORANGE_YELLOW, PALE_GREY_THREE, PALE_LIGHT_BLUE_TWO, PALE_SALMON, REDDISH, SHIP_GREY_CALM, SKELETON_COLOR, SKELETON_HIGHLIGHT, } from '../../constants/Colors'
import { WP05, WP11, WP2, WP205, WP305 } from '../../constants/Sizes'
import Image from '../Image'
import Text from '../Text'

const CheckInItem = ({
  loading,
  key,
  isLucky,
  isChecked,
  point,
  day
}) => {
  const
    colorInActive = [PALE_GREY_THREE, PALE_GREY_THREE],
    borderActive = [MELON, ORANGE_YELLOW],
    bgActive = [REDDISH, LIPSTICK_TWO],
    colorsBorder = isChecked && !isLucky ? borderActive : colorInActive,
    colorsBG = isChecked && !isLucky ? bgActive : colorInActive
  return (
    <View key={key} style={{ width: WP11, marginRight: WP205, }}>
      <View style={{ marginBottom: WP2 }}>
        <LinearGradient
          colors={colorsBorder}
          start={[1, 0]} end={[0, 0]}
          style={{ width: WP11, height: WP11, borderRadius: WP11 / 2, padding: !isLucky ? WP05 : 0, overflow: 'hidden' }}
        >
          <LinearGradient
            colors={colorsBG}
            start={[1, 0]} end={[0, 0]}
            style={{ width: '100%', height: '100%', borderRadius: (WP11 - WP05) / 2, justifyContent: 'center', alignItems: 'center' }}
          >
            {
              isLucky ?
                (
                  <Image size={WP11} source={isChecked ? require('sf-assets/icons/icLuckyBoxActive.png') : require('sf-assets/icons/icLuckyBox.png')} />
                ) : (
                  <Text centered type='Circular' size='mini' weight={500} color={isChecked ? PALE_SALMON : PALE_LIGHT_BLUE_TWO} >{point}</Text>
                )
            }
          </LinearGradient>
        </LinearGradient>
        {
          isChecked && (
            <Image
              style={{ position: 'absolute', right: -WP05, bottom: WP05, zIndex: 99 }}
              size='xtiny' source={require('sf-assets/icons/icCheckGreen.png')}
            />
          )
        }
      </View>
      {
        loading ?
          (
            <View style={{ width: '100%', height: WP305, backgroundColor: PALE_GREY_THREE }} />
          ) : (
            <Text centered type='Circular' size='xmini' color={SHIP_GREY_CALM} >{`Hari ${day}`}</Text>
          )
      }
    </View>
  )
}

export default memo(CheckInItem)
