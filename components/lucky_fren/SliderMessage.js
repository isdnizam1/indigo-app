import React, { useState, memo } from 'react'
import { View } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { PALE_BLUE_TWO, REDDISH, SHIP_GREY_CALM } from '../../constants/Colors'
import { WP05, WP1, WP105, WP2, WP4, WP80 } from '../../constants/Sizes'
import { toNormalDate } from '../../utils/date'
import Text from '../Text'

const SliderMessage = ({
  endDate,
  isExpired
}) => {
  let data = [
    `Event ini berlangsung s/d ${toNormalDate(endDate)}`,
    'Check in terlebih dahulu untuk memulai misi',
    'Kumpulkan poinmu dan tukarkan dengan hadiahnya'
  ]
  if(isExpired) data = [data[0]]
  const [activeSlider, setActiveSlider] = useState(0)
  return (
    <View style={{ width: '100%', paddingVertical: WP05, paddingHorizontal: WP4, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
      <Carousel
        style={{ flex: 1 }}
        onSnapToItem={(i) => setActiveSlider(i)}
        data={data}
        firstItem={activeSlider}
        inactiveSlideScale={1}
        activeSlideAlignment='start'
        sliderWidth={WP80 + WP4}
        itemWidth={WP80 + WP4}
        extraData={activeSlider}
        autoplay={true}
        loop={true}
        autoplayDelay={5000}
        contentContainerStyle={{
          paddingVertical: WP2
        }}
        renderItem={({ item, index }) => (
          <View style={{ paddingVertical: WP2 }}>
            <Text key={`${index}+${Math.random()}`} type='Circular' size='tiny' weight={300} color={SHIP_GREY_CALM}>{item}</Text>
          </View>
        )}
      />
      <Pagination
        dotsLength={data.length}
        activeDotIndex={activeSlider}
        dotColor={REDDISH}
        inactiveDotColor={PALE_BLUE_TWO}
        inactiveDotScale={1}
        animatedDuration={0}
        animatedTension={0}
        containerStyle={{
          justifyContent: 'flex-end',
          paddingHorizontal: 0,
          paddingVertical: 0,
          marginHorizontal: 0,
        }}
        dotContainerStyle={{ marginLeft: 0, marginHorizontal: WP1 }}
        dotStyle={{
          width: WP105,
          height: WP105,
          borderRadius: WP105 / 2,
        }}
      />
    </View>
  )
}

export default SliderMessage
