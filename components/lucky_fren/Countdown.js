import React, { memo, useEffect, useState } from 'react'
import { View } from 'react-native'
import moment from 'moment'
import { REDDISH } from '../../constants/Colors'
import { WP1, WP6 } from '../../constants/Sizes'
import Text from '../Text'

const Countdown = ({
  onFinish,
  isShowHours = true,
  isShowMinutes = true,
  isShowSeparator = true
}) => {
  let tomorrow = moment().utcOffset('+0700').add(1, 'd').format('YYYY-MM-DD')
  // let lusa = moment().utcOffset('+0700').add(2, 'd').format('YYYY-MM-DD') //don't delete (for testing)

  let startTime = moment().utcOffset('+0700')
  let endTime = moment(`${tomorrow} 00:00:00`, 'YYYY-MM-DD hh:mm:ss').utcOffset('+0700')
  // let lusaTime = moment(`${lusa} 00:00:00`, 'YYYY-MM-DD hh:mm:ss').utcOffset('+0700') //don't delete (for testing)

  let totalSec = endTime.diff(startTime, 'seconds')
  // let totalLusa = lusaTime.diff(endTime, 'seconds') //don't delete (for testing)

  const [countdown, setCountDown] = useState(totalSec)

  const reset = async () => {
    await onFinish()
    await setCountDown(totalSec)
  }

  useEffect(() => {
    let interval = null
    interval = setInterval(() => {
      if (countdown <= 0) {
        clearInterval(interval)
        reset()
      } else {
        setCountDown(countdown - 1)
      }
    }, 1000)
    return () => {
      clearInterval(interval)
    }
  }, [countdown])

  let h = parseInt(countdown / 3600) % 24
  let m = parseInt(countdown / 60) % 60
  let s = countdown % 60

  const hours = `${h < 10 ? `0${h}` : h}`
  const minutes = `${m < 10 ? `0${m}` : m}`
  const seconds = `${s < 10 ? `0${s}` : s}`
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
      {
        isShowHours && (
          <>
            <View style={{ minWidth: WP6, paddingHorizontal: WP1, paddingVertical: WP1, backgroundColor: 'rgba(255, 101, 31, 0.1)', borderRadius: 4, marginHorizontal: WP1 }}>
              <Text centered size='xmini' weight={300} type='Circular' color={REDDISH}>{hours}</Text>
            </View>
            {isShowSeparator && (
              <Text centered size='xmini' weight={300} type='Circular' color={REDDISH}>:</Text>
            )}
          </>
        )
      }
      {
        isShowMinutes && (
          <>
            <View style={{ minWidth: WP6, paddingHorizontal: WP1, paddingVertical: WP1, backgroundColor: 'rgba(255, 101, 31, 0.1)', borderRadius: 4, marginHorizontal: WP1 }}>
              <Text centered size='xmini' weight={300} type='Circular' color={REDDISH}>{minutes}</Text>
            </View>
            {isShowSeparator && (
              <Text centered size='xmini' weight={300} type='Circular' color={REDDISH}>:</Text>
            )}
          </>
        )
      }
      <View style={{ minWidth: WP6, paddingHorizontal: WP1, paddingVertical: WP1, backgroundColor: 'rgba(255, 101, 31, 0.1)', borderRadius: 4, marginHorizontal: WP1 }}>
        <Text centered size='xmini' weight={300} type='Circular' color={REDDISH}>{seconds}</Text>
      </View>
    </View>
  )
}

export default memo(Countdown)
