import React, { memo } from 'react'
import { TouchableOpacity, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { upperFirst } from 'lodash-es'
import Image from '../Image'
import Text from '../Text'
import { GUN_METAL, PALE_GREY_THREE, TOMATO, WHITE, } from '../../constants/Colors'
import { WP1, WP105, WP2, WP4, WP40, WP5 } from '../../constants/Sizes'
import Point from './Point'

const RewardItem = ({
  reward,
  point,
  image,
  stock,
  loading,
  navigateTo,
  id_reward,
  isExpired,
  is1000Startup = false
}) => {
  const title = loading ? 'Title' : reward,
    subtitle = point,
    imageWidth = WP40,
    imageHeight = WP40,
    aspectRatio = 1,
    radius = 6,
    label = isExpired ? 'Telah Berakhir' : (loading ? 'Stok' : stock > 0 ? `Tersisa ${stock} lagi` : 'Stok habis'),
    labelColor = [TOMATO, TOMATO]

  return (
    <TouchableOpacity
      style={{
        marginRight: WP4,
        width: imageWidth,
      }}
      onPress={() => navigateTo('RewardDetailScreen', { id_reward, isExpired, is1000Startup })}
    >
      <View style={{ width: imageWidth, height: imageHeight, backgroundColor: PALE_GREY_THREE, borderRadius: radius }}>
        {
          !loading && (
            <View>
              <View style={{
                borderRadius: radius,
                overflow: 'hidden',
              }}
              >
                <Image
                  source={{ uri: image }}
                  imageStyle={{ height: imageHeight, aspectRatio }}
                />
                {
                  label && (
                    <View
                      style={{
                        position: 'absolute',
                        alignSelf: 'center',
                        top: 0,
                        left: 0,
                      }}
                    >
                      <View
                        style={{
                          overflow: 'hidden',
                          borderBottomRightRadius: 6
                        }}
                      >
                        <LinearGradient
                          colors={labelColor}
                          start={[1, 0]} end={[0, 0]}
                          style={{
                            paddingHorizontal: WP105,
                            paddingVertical: WP1,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                        >
                          <Text type='Circular' size='tiny' color={WHITE} weight={500} centered>{label}</Text>
                        </LinearGradient>
                      </View>
                    </View>
                  )
                }
              </View>
            </View>
          )
        }
      </View>
      <View>
        <Text lineHeight={WP5} numberOfLines={2} type='Circular' size='mini' color={GUN_METAL} weight={500} style={{ marginTop: WP2 }}>{upperFirst(title)}</Text>
        <Point value={upperFirst(subtitle)}/>
      </View>
    </TouchableOpacity>
  )
}

export default memo(RewardItem)
