import React, { Component } from "react";
import { connect } from "react-redux";
import RNModal from "react-native-modal";
import { View, Image, FlatList, TextInput } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Text, _enhancedNavigation } from "../components";
import {
  BLACK,
  GUN_METAL,
  REDDISH,
  SHIP_GREY_CALM,
  TRANSPARENT,
  WHITE,
  ORANGE_TOMATO,
  LIPSTICK_TWO,
} from "../constants/Colors";
import {
  HP1,
  HP2,
  HP25,
  HP3,
  WP1,
  WP100,
  WP2,
  WP3,
  WP6,
  WP95,
} from "../constants/Sizes";
import ButtonV2 from "../components/ButtonV2";
import { postEventParticipant } from "../actions/api";
import { isEmpty, upperCase } from "lodash-es";
import { getUserId } from "../utils/storage";
import ConfirmationModalV3 from "./ConfirmationModalV3";
import style from "sf-styles/register";
import DeactivateParticipant from "./DeactivateParticipant";

class EventSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      blockedModalVisible: false,
      referralCode: "",
      referralEvent: "",
      eventId: null,
    };
    this.onSubmit = this._onSubmit.bind(this);
  }

  _dateFormatter = (eventDate) => {
    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec",
    ];
    const date = new Date(eventDate.slice(0, 10));
    const day = date.getDate();
    const month = monthNames[date.getMonth()];
    const year = date.getFullYear();
    const formattedDate = `${day} ${month} ${year}`;
    return formattedDate;
  };

  _onSubmit = async (eventId, referralEvent, isJoined, isBlocked) => {
    if (!isJoined && referralEvent) {
      this.setState({
        modalVisible: true,
        referralEvent,
        eventId,
        referralCode: null,
      });
      return;
    }
    if (isJoined && isBlocked) {
      this.setState({
        blockedModalVisible: true,
        referralEvent,
        eventId,
        referralCode: null,
      });
      return;
    }

    const { navigateTo } = this.props;
    const userId = await getUserId();
    const landingScreen = "AppNavigator";
    const body = {
      id_event: eventId,
      id_user: userId,
      referral_code: !isEmpty(this.state.referralCode)
        ? this.state.referralCode.toUpperCase()
        : null,
    };

    try {
      const response = await postEventParticipant(body);
      if (response.data.code === 200) {
        navigateTo(landingScreen, { initialLoaded: true }, "replace");

        this.setState({ modalVisible: false });
      }
    } catch (err) {
      // diam itu emas:)
    }
  };

  render() {
    return (
      <View>
        <FlatList
          data={this.props.data}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({ item, index }) => (
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                width: WP100,
                paddingVertical: HP2,
              }}
            >
              <View
                style={{
                  backgroundColor: WHITE,
                  width: WP95,
                  marginVertical: HP1,
                  borderRadius: 6,
                  elevation: 2,
                  paddingHorizontal: WP3,
                  paddingVertical: WP3 * 1.5,
                }}
              >
                <View>
                  {item.user_joined && (
                    <LinearGradient
                      colors={[LIPSTICK_TWO, "#FF651F"]}
                      start={[12, 9.5]}
                      end={[36, 9.5]}
                    >
                      <View
                        style={{
                          position: "absolute",
                          zIndex: 10,
                          top: 0,
                          backgroundColor: !item.user_blocked
                            ? REDDISH
                            : ORANGE_TOMATO,
                          borderTopLeftRadius: 6,
                          borderBottomRightRadius: 6,
                          height: HP3,
                          justifyContent: "center",
                          alignItems: "flex-end",
                          paddingRight: WP2,
                          paddingLeft: WP2,
                        }}
                      >
                        <Text size={"xmini"} weight={500} color={WHITE}>
                          {!item.user_blocked ? "Diikuti" : "Blocked"}
                        </Text>
                      </View>
                    </LinearGradient>
                  )}
                  <Image
                    style={{
                      width: "100%",
                      height: HP25 * 1.1,
                      resizeMode: "cover",
                      borderRadius: 6,
                    }}
                    source={{
                      uri: !item.image
                        ? "https://eventeer.id/assets/image/banner-default.jpg"
                        : item.image,
                    }}
                  />
                </View>
                <Text
                  color={BLACK}
                  weight={600}
                  size={"medium"}
                  style={{
                    textAlign: "left",
                    paddingTop: HP1 * 1.4,
                    paddingBottom: HP1 * 0.2,
                  }}
                >
                  {item.event_title}
                </Text>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Text size={"tiny"} weight={400} color={SHIP_GREY_CALM}>
                    {item.event_location}
                  </Text>
                  <View
                    style={{
                      width: WP1 * 0.8,
                      height: WP1 * 0.8,
                      backgroundColor: SHIP_GREY_CALM,
                      borderRadius: 50,
                      marginHorizontal: WP2 * 0.8,
                    }}
                  />
                  <Text size={"tiny"} weight={500} color={SHIP_GREY_CALM}>
                    {this._dateFormatter(item.start_date)} -{" "}
                    {this._dateFormatter(item.end_date)}
                  </Text>
                </View>

                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      paddingTop: HP1,
                      alignItems: "center",
                    }}
                  >
                    <View
                      style={{
                        width: WP6,
                        height: WP6,
                        borderColor: SHIP_GREY_CALM,
                        borderWidth: 1,
                        borderRadius: 50,
                        marginRight: WP2 * 0.8,
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <Text size={"xmini"} weight={500} color={GUN_METAL}>
                        {item.vendor_name.slice(0, 1).toUpperCase()}
                      </Text>
                    </View>

                    <Text size={"slight"} weight={500} color={GUN_METAL}>
                      {item.vendor_name}
                    </Text>
                  </View>

                  <ButtonV2
                    borderColor={TRANSPARENT}
                    style={{
                      backgroundColor: REDDISH,
                      paddingVertical: WP2 * 0.8,
                    }}
                    onPress={() =>
                      this._onSubmit(
                        item.id_event,
                        item.referral_code,
                        item.user_joined,
                        item.user_blocked
                      )
                    }
                    iconSize={"xmini"}
                    textColor={WHITE}
                    text={!item.user_blocked ? "Ikuti Event" : "Details"}
                  />
                </View>
              </View>
            </View>
          )}
        />
        <RNModal
          isVisible={this.state.blockedModalVisible}
          // onBackdropPress={() => this.setState({ blockedModalVisible: false })}
          // onBackButtonPress={() =>
          //   this.setState({ blockedModalVisible: false })
          // }
          style={{
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <DeactivateParticipant
            onConfirm={() => this.setState({ blockedModalVisible: false })}
          />
        </RNModal>

        <ConfirmationModalV3
          visible={this.state.modalVisible}
          title={"Event code"}
          titleSize={"large"}
          titleWeight={600}
          subtitle="Silahkan isi event code dari event yang ingin kamu ikuti"
          extraComponent={
            <View
              style={{
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <TextInput
                autoCapitalize={"characters"}
                defaultValue={this.state.referralCode}
                onChangeText={(value) => this.setState({ referralCode: value })}
                autoCompleteType={"name"}
                placeholder={"Masukkan event code"}
                style={[
                  style.input,
                  isEmpty(this.state.referralCode) ||
                  this.state.referralEvent ==
                    this.state.referralCode.toUpperCase()
                    ? null
                    : style.inputError,
                  { width: "100%" },
                ]}
              />
            </View>
          }
          primary={{
            text: "Masuk",
            onPress: () =>
              this.state.referralEvent ===
                this.state.referralCode.toUpperCase() &&
              this._onSubmit(this.state.eventId, this.state.referralCode, true),
          }}
          secondary={{
            text: `Batalkan`,
            onPress: () =>
              this.setState({ modalVisible: false, referralCode: null }),
            textColor: REDDISH,
            backgroundColor: WHITE,
            style: {
              borderWidth: 1,
              borderColor: REDDISH,
            },
          }}
        />
      </View>
    );
  }
}

export default EventSelection;
