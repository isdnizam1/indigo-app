import React from 'react'
import PropTypes from 'prop-types'
import RNModal from 'react-native-modal'
import { keys } from 'lodash-es'
import { View, Platform } from 'react-native'
import noop from 'lodash-es/noop'
import { MODAL_STYLE } from '../constants/Styles'
import { WHITE } from '../constants/Colors'
import { HP1 } from '../constants/Sizes'

const propsType = {
  key: PropTypes.any,
  position: PropTypes.oneOf(keys(MODAL_STYLE)),
  backgroundColor: PropTypes.string,
  isVisible: PropTypes.bool,
  useNativeDriver: PropTypes.bool,
  closeOnBackdrop: PropTypes.bool,
  unstyled: PropTypes.bool,
  renderModalContent: PropTypes.func,
  onShow: PropTypes.func,
  swipeDirection: PropTypes.string,
  style: PropTypes.object,
  modalStyle: PropTypes.object,
  animationIn: PropTypes.string,
  animationOut: PropTypes.string,
  closeBackdropCallback: PropTypes.func,
  onClose: PropTypes.func,
}

const propsDefault = {
  position: 'bottom',
  backgroundColor: WHITE,
  isVisible: false,
  useNativeDriver: true,
  swipeDirection: 'down',
  style: {},
  closeOnBackdrop: false,
  modalStyle: {},
  animationInTiming: 300,
  animationIn: 'slideInUp',
  animationOut: 'slideOutDown',
  unstyled: false,
  onShow: () => {
  },
  closeBackdropCallback: noop,
  onClose: noop
}

class Modal extends React.PureComponent {
  state = {
    isVisible: this.props.isVisible,
    payload: {}
  }

  _toggleModal = async (payload) => {
    if(this.state.isVisible) this.props.onClose()
    await this.setState((state) => ({ isVisible: !state.isVisible, payload }))
  }

  render() {
    const {
      position,
      backgroundColor,
      renderModalContent,
      children,
      swipeDirection,
      style,
      modalStyle,
      animationIn,
      animationOut,
      closeOnBackdrop,
      unstyled,
      onShow,
      closeBackdropCallback,
      onModalHide,
      onBackButtonPress,
    } = this.props
    const {
      isVisible,
      payload
    } = this.state

    return children({ toggleModal: this._toggleModal, isVisible }, (
      <RNModal
        transparent
        onModalHide={onModalHide}
        isVisible={isVisible}
        onShow={onShow}
        style={[MODAL_STYLE[position], modalStyle]}
        onBackdropPress={closeOnBackdrop ? closeBackdropCallback : this._toggleModal}
        onBackButtonPress={onBackButtonPress ? () => { this.setState({ isVisible: false }), onBackButtonPress() } : () => this.setState({ isVisible: false })}
        onSwipeComplete={this._toggleModal}
        animationIn={animationIn}
        animationOut={animationOut}
        swipeDirection={swipeDirection}
        propagateSwipe={Platform.OS === 'ios'}
        avoidKeyboard={Platform.OS === 'ios'}
      >
        <View
          style={[
            unstyled ? { width: '100%', height: '100%' } : {
              backgroundColor,
              borderTopRightRadius: HP1,
              borderTopLeftRadius: HP1
            },
            style
          ]}
        >
          {renderModalContent({ toggleModal: this._toggleModal, payload: payload ? payload : {}, isVisible })}
        </View>
      </RNModal>
    ))
  }
}

Modal.propTypes = propsType

Modal.defaultProps = propsDefault

export default Modal
