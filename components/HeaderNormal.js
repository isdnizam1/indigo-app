import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { isFunction, noop } from 'lodash-es'
import { WP2, WP3, WP4 } from 'sf-constants/Sizes'
import { NO_COLOR } from 'sf-constants/Colors'
import { HEADER } from '../constants/Styles'
import { SHADOW_GRADIENT, SHIP_GREY, SHIP_GREY_CALM } from '../constants/Colors'
import Text from './Text'
import Icon from './Icon'
import Touchable from './Touchable'

const propsType = {
  key: PropTypes.any,
  backgroundColor: PropTypes.string,
  colors: PropTypes.array,
  style: PropTypes.object,
  childStyle: PropTypes.object,
  wrapperStyle: PropTypes.object,
  iconLeftOnPress: PropTypes.func,
  content: PropTypes.func,
  iconLeft: PropTypes.string,
  iconLeftColor: PropTypes.string,
  iconLeftSize: PropTypes.string,
  iconLeftType: PropTypes.string,
  iconLeftBackground: PropTypes.string,
  iconLeftBackgroundColor: PropTypes.string,
  text: PropTypes.string,
  textSize: PropTypes.string,
  textType: PropTypes.string,
  textColor: PropTypes.string,
  textWeight: PropTypes.number,
  rightComponent: PropTypes.any,
  centered: PropTypes.bool,
  withExtraPadding: PropTypes.bool,
  noRightPadding: PropTypes.bool,
  iconStyle: PropTypes.any,
  withDummyRightIcon: PropTypes.bool,
  shadow: PropTypes.bool,
}

const propsDefault = {
  style: {},
  backgroundColor: 'transparent',
  iconLeftOnPress: noop,
  iconLeft: 'chevron-left',
  iconLeftColor: SHIP_GREY_CALM,
  iconLeftSize: 'large',
  iconLeftType: 'Entypo',
  iconLeftBackground: 'none',
  textSize: 'small',
  textColor: SHIP_GREY,
  textType: 'Circular',
  textWeight: 500,
  centered: false,
  noRightPadding: true,
  withExtraPadding: false,
}

class HeaderNormal extends React.PureComponent {
  render() {
    const {
      backgroundColor,
      colors,
      style,
      childStyle,
      wrapperStyle,
      iconLeftOnPress,
      iconLeft,
      iconLeftColor,
      iconLeftSize,
      iconLeftType,
      iconLeftBackground,
      iconLeftBackgroundColor,
      text,
      textSize,
      textColor,
      textType,
      textWeight,
      rightComponent,
      centered,
      iconStyle,
      children,
      withExtraPadding,
      noRightPadding,
      withDummyRightIcon,
      shadow,
      content,
    } = this.props

    const renderIcon = (isRightIcon = false) => (
      <Touchable
        style={[
          {
            paddingVertical: WP3,
            paddingLeft: isRightIcon ? WP3 : WP4,
            paddingRight: isRightIcon ? WP4 : WP3,
          },
          iconStyle,
        ]}
        onPress={isRightIcon ? noop : iconLeftOnPress}
      >
        <View style={this.props.iconLeftWrapperStyle}>
          <Icon
            size={iconLeftSize}
            background={iconLeftBackground}
            backgroundColor={iconLeftBackgroundColor}
            name={iconLeft}
            type={iconLeftType}
            centered
            color={isRightIcon ? NO_COLOR : iconLeftColor}
          />
        </View>
      </Touchable>
    )

    return (
      <View>
        <View style={wrapperStyle}>
          <LinearGradient
            colors={colors || [backgroundColor, backgroundColor]}
            start={[0, 0]}
            end={[1, 0]}
            style={[
              {
                backgroundColor,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              },
              style,
            ]}
          >
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              {centered && iconLeft && renderIcon()}
              <View style={{ flexDirection: 'row', flexGrow: 1, flex: 1 }}>
                {!centered && iconLeft && renderIcon()}
                {isFunction(content) ? (
                  content()
                ) : (
                  <View
                    style={{
                      flexGrow: 1,
                      paddingVertical: withExtraPadding ? WP3 : WP2,
                      paddingLeft: iconLeft ? 0 : WP4,
                      paddingRight: noRightPadding ? 0 : WP4,
                    }}
                  >
                    <Text
                      numberOfLines={1}
                      color={textColor}
                      centered={centered}
                      size={textSize}
                      type={textType}
                      weight={textWeight}
                    >
                      {text}
                    </Text>
                  </View>
                )}
              </View>
              {/*<View style={{ justifyContent: 'center' }}>*/}
              {withDummyRightIcon && renderIcon(true)}
              {/*</View>*/}
              {rightComponent
                ? isFunction(rightComponent)
                  ? rightComponent()
                  : rightComponent
                : renderIcon(true)}
            </View>
          </LinearGradient>
          {children && <View style={childStyle}>{children}</View>}
        </View>
        {shadow && <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />}
      </View>
    )
  }
}

HeaderNormal.propTypes = propsType
HeaderNormal.defaultProps = propsDefault
export default HeaderNormal
