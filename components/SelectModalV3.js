import { isString } from 'util'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  TouchableOpacity,
  View,
  TextInput,
  ScrollView,
  SafeAreaView,
} from 'react-native'
import {
  isFunction,
  isEqual,
  isEmpty,
  reduce,
  isArray,
  includes,
  map,
  noop,
} from 'lodash-es'
import { connect } from 'react-redux'
import { WP5 } from 'sf-constants/Sizes'
import {
  WP1,
  WP2,
  WP4,
  HP100,
  WP3,
  WP105,
  WP308,
  HP50,
  WP15,
  WP205,
  WP8,
} from '../constants/Sizes'
import {
  WHITE,
  SHIP_GREY_CALM,
  REDDISH,
  NAVY_DARK,
  PALE_GREY,
  GUN_METAL,
  PALE_BLUE,
  SHIP_GREY,
  CLEAR_BLUE,
  PALE_BLUE_TWO,
  BABY_BLUE,
} from '../constants/Colors'
import { TOUCH_OPACITY } from '../constants/Styles'
import Modal from './Modal'
import Text from './Text'
import Icon from './Icon'
import Button from './Button'
import ButtonV2 from './ButtonV2'
import Spacer from './Spacer'

const propsType = {
  triggerComponent: PropTypes.objectOf(PropTypes.any),
  keyword: PropTypes.string,
  onChange: PropTypes.func,
  header: PropTypes.string,
  suggestion: PropTypes.func,
  renderItem: PropTypes.func,
  reformatFromApi: PropTypes.func,
  suggestionKey: PropTypes.string,
  suggestionPathResult: PropTypes.string,
  suggestionPathValue: PropTypes.string,
  asObject: PropTypes.bool,
  placeholder: PropTypes.string,
  refreshOnSelect: PropTypes.bool,
  extraResult: PropTypes.bool,
  iconName: PropTypes.string,
  iconType: PropTypes.string,
  iconSize: PropTypes.string,
  selection: PropTypes.any,
  selectionKey: PropTypes.string,
  selectionWording: PropTypes.string,
  showSelection: PropTypes.bool,
  onRemoveSelection: PropTypes.func,
  quickSearchTitle: PropTypes.string,
  quickSearchList: PropTypes.any,
  quickSearchKey: PropTypes.string,
  actionNext: PropTypes.func,
  quickSearch: PropTypes.func,
  disabled: PropTypes.bool,
}

const propsDefault = {
  keyword: '',
  triggerComponent: {},
  onChange: () => {},
  header: '',
  suggestion: () => {},
  suggestionKey: '',
  suggestionPathResult: '',
  suggestionPathValue: '',
  asObject: false,
  createNew: true,
  placeholder: '',
  refreshOnSelect: false,
  extraResult: false,
  iconName: '',
  iconType: 'AntDesign',
  iconSize: 'small',
  selection: '',
  selectionWording: '',
  showSelection: false,
  onRemoveSelection: () => {},
  quickSearchTitle: '',
  quickSearchList: [],
  actionNext: () => {},
  quickSearch: noop,
  disabled: false,
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

const ButtonItem = ({ onPress, title, remove }) => (
  <TouchableOpacity
    onPress={onPress}
    activeOpacity={TOUCH_OPACITY}
    style={{
      marginRight: WP2,
      marginBottom: WP2,
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: WP3,
      paddingVertical: WP2,
      borderRadius: 6,
      backgroundColor: WHITE,
      borderWidth: 1,
      borderColor: PALE_BLUE_TWO,
    }}
  >
    <Text
      size='xmini'
      type='Circular'
      weight={400}
      color={SHIP_GREY}
      style={{ marginRight: remove ? WP205 : 0 }}
    >
      {title}
    </Text>
    {remove && (
      <Icon
        onPress={onPress}
        centered
        size='small'
        color={PALE_BLUE_TWO}
        name={'close'}
        type='MaterialCommunityIcons'
      />
    )}
  </TouchableOpacity>
)

class SelectModalV3 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      keyword: '',
      inputKey: '',
      suggestions: [],
      suggestionsRecent: [],
      showSuggestion: false,
      modalHeight: HP100,
      quickSearchList: this.props.quickSearchList || [],
    }
  }

  componentDidMount = () => {
    this._getQuickSearch()
    this._onFetchSuggestion()
  };

  _onFocus = () => {
    this.setState({ showSuggestion: true }, this._onFetchSuggestion)
  };

  _onBlur = () => {
    this.setState({ showSuggestion: false })
  };

  _onChangeText = (text) => {
    this.setState({ keyword: text }, this._onFetchSuggestion)
  };

  _getQuickSearch = async () => {
    const { quickSearch } = this.props
    try {
      if (isFunction(quickSearch)) {
        const { data } = await quickSearch({})
        await this.setState({
          quickSearchList: data.result,
        })
      }
    } catch (e) {
      // silent
    }
  };

  _onSelectSuggestion = (value, closeModalCallback) => {
    const {
      onChange,
      suggestionPathResult,
      asObject,
      refreshOnSelect,
      selection,
      actionNext,
    } = this.props

    if (asObject) {
      if (value[suggestionPathResult] !== this.props.selected) {
        if (!refreshOnSelect)
          this.setState({ keyword: value[suggestionPathResult] })
        onChange(value)
      } else {
        this.setState({ keyword: '' })
        onChange({})
      }
    } else {
      if (value[suggestionPathResult] !== this.props.selected) {
        if (!refreshOnSelect)
          this.setState({ keyword: value[suggestionPathResult] })
        else this.setState({ keyword: '' })
        onChange(value[suggestionPathResult])
      } else {
        this.setState({ keyword: '' })
        onChange('')
      }
    }
    if (!isArray(selection)) {
      closeModalCallback()
      setTimeout(() => {
        actionNext()
      }, 500)
    } else {
      this._onFocus()
    }
  };

  _onCreateNewOption = (closeModalCallback) => {
    const { keyword } = this.state
    const { onChange } = this.props

    onChange(keyword)
    closeModalCallback()
  };

  _onClearInput = () => {
    this._onFocus()
  };

  _onFetchSuggestion = () => {
    const {
      suggestion,
      suggestionKey,
      suggestionType,
      userData: { id_user },
      suggestionPathResult,
      extraResult,
      reformatFromApi,
      suggestionPathValue,
      id_province,
    } = this.props
    const { keyword } = this.state
    if (isFunction(suggestion)) {
      let params = {
        [suggestionKey]: keyword,
        id_user,
        limit: 6,
        start: 0,
      }
      if (suggestionPathValue === 'id_province') {
        params = {
          [suggestionKey]: keyword,
          limit: 6,
          start: 0,
        }
      } else if (id_province) {
        params = {
          ...params,
          id_province,
        }
      }
      suggestion(params)
        .then(({ data }) => {
          let suggestionData = data.result
          if (reformatFromApi) {
            suggestionData = reduce(
              suggestionData,
              (result, item) => {
                result.push({
                  [suggestionPathResult]: reformatFromApi(
                    item[suggestionPathResult],
                  ),
                  [suggestionPathValue]: item[suggestionPathValue],
                })
                return result
              },
              [],
            )
          }
          let suggestionDataRecentlySearch = []
          if (extraResult) {
            suggestionDataRecentlySearch = data.recentlySearch || []
            if (!isEmpty(suggestionDataRecentlySearch)) {
              suggestionDataRecentlySearch = reduce(
                data.recentlySearch,
                (result, item) => {
                  result.push({
                    [suggestionPathResult]: reformatFromApi
                      ? reformatFromApi(item.value)
                      : item.value,
                    [suggestionPathValue]: item[suggestionPathValue],
                  })
                  return result
                },
                [],
              )
            }
          }
          this.setState({
            suggestions: suggestionData,
            suggestionsRecent: suggestionDataRecentlySearch,
            showSuggestion: true,
          })
        })
        .catch(() => {})
    }
  };

  _isSelected = (item) => {
    const { selected, suggestionPathResult } = this.props

    if (isArray(selected)) return includes(selected, item[suggestionPathResult])
    else return isEqual(selected, item[suggestionPathResult])
  };

  // RENDER LIST
  _suggestionList(
    suggestions,
    toggleModal,
    suggestionPathResult,
    suggestionsRecent,
  ) {
    const {
      extraResult,
      renderAsButtons,
      forceShowQuickSearch,
      quickSearchTitle,
      quickSearchKey,
      selection,
      selectionKey,
      showSelection,
      onRemoveSelection,
      onChange,
    } = this.props
    const { keyword, quickSearchList } = this.state
    return (
      <ScrollView
        horizontal={!!renderAsButtons}
        showsHorizontalScrollIndicator={!renderAsButtons}
        showsVerticalScrollIndicator={!!renderAsButtons}
        keyboardShouldPersistTaps='handled'
        contentContainerStyle={
          renderAsButtons
            ? {
                flex: 1,
                flexDirection: 'row',
                flexWrap: 'wrap',
              }
            : {
                paddingTop: WP4,
                paddingBottom: WP2,
              }
        }
      >
        {!isEmpty(suggestionsRecent) && isEmpty(keyword) && extraResult && (
          <Text type='Circular' color={NAVY_DARK} size='mini' weight={500}>
            Pencarian Terakhir
          </Text>
        )}
        {!isEmpty(suggestionsRecent) && isEmpty(keyword) && extraResult && (
          <View style={{ marginTop: WP2, marginBottom: WP3 }}>
            {suggestionsRecent.map((item, index) =>
              this._renderItem(index, item, toggleModal, suggestionPathResult),
            )}
          </View>
        )}
        {isEmpty(keyword) && extraResult && suggestions.length >= 0 && (
          <View style={{ paddingBottom: WP2 }}>
            <Text type='Circular' color={NAVY_DARK} size='mini' weight={500}>
              Paling banyak dicari
            </Text>
          </View>
        )}
        {!isEmpty(keyword) &&
          (suggestions || []).map((item, index) =>
            this._renderItem(index, item, toggleModal, suggestionPathResult),
          )}
        {!isEmpty(keyword) && this._createNewOption(toggleModal)}
        {((isEmpty(keyword) && isEmpty(selection)) || forceShowQuickSearch) &&
          quickSearchList.length > 0 && (
            <View>
              <Spacer size={WP3} />
              <View style={{ paddingBottom: WP3 }}>
                <Text type='Circular' color={SHIP_GREY} size='mini' weight={300}>
                  {quickSearchTitle}
                </Text>
              </View>
              <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                {map(quickSearchList, (quick, quickIndex) => (
                  <ButtonItem
                    onPress={() =>
                      onChange(quickSearchKey ? quick[quickSearchKey] : quick)
                    }
                    title={quickSearchKey ? quick[quickSearchKey] : quick}
                    remove={false}
                  />
                ))}
              </View>
            </View>
          )}
        {showSelection && !isEmpty(selection) && selection?.length > 0 && (
          <View style={{ paddingVertical: !isEmpty(keyword) ? WP4 : 0 }}>
            {isArray(selection) ? (
              <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                {map(selection, (itemSelection, index) => (
                  <ButtonItem
                    onPress={() => {
                      toggleModal()
                      onRemoveSelection(index)
                    }}
                    title={
                      selectionKey ? itemSelection[selectionKey] : itemSelection
                    }
                    remove={true}
                  />
                ))}
              </View>
            ) : (
              <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                <ButtonItem
                  onPress={() => {
                    toggleModal()
                    onRemoveSelection()
                  }}
                  title={selection}
                  remove={true}
                />
              </View>
            )}
          </View>
        )}
      </ScrollView>
    )
  }

  _renderItem = (index, item, toggleModal, suggestionPathResult) => {
    const { iconName, iconType, iconSize, renderAsButtons } = this.props
    if (renderAsButtons)
      return (
        <ButtonV2
          style={{
            paddingHorizontal: WP3,
            paddingVertical: WP2,
            marginTop: WP2,
            marginRight: WP2,
          }}
          key={index}
          text={item[suggestionPathResult]}
          onPress={() => this._onSelectSuggestion(item, toggleModal)}
        />
      )
    else
      return (
        <TouchableOpacity
          key={index}
          style={{
            paddingVertical: WP2,
            paddingHorizontal: WP1,
            borderBottomWidth: 1,
            borderBottomColor: PALE_GREY,
          }}
          onPress={() => {
            this._onSelectSuggestion(item, toggleModal)
          }}
        >
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <View style={{ alignItems: 'center', flexDirection: 'row' }}>
              {!isEmpty(iconName) && (
                <Icon
                  style={{ marginRight: WP3 }}
                  centered
                  color={this._isSelected(item) ? PALE_GREY : GUN_METAL}
                  type={iconType}
                  name={iconName}
                  size={iconSize}
                />
              )}
              {this._renderItemText(item, suggestionPathResult)}
            </View>
          </View>
        </TouchableOpacity>
      )
  };

  _renderItemText = (item, suggestionPathResult) => {
    const { renderItem } = this.props
    if (renderItem) {
      const itemEl = renderItem(item[suggestionPathResult])
      if (React.isValidElement(itemEl)) return itemEl
      else
        return (
          <Text
            size='mini'
            type='Circular'
            color={this._isSelected(item) ? REDDISH : GUN_METAL}
            weight={this._isSelected(item) ? 400 : 300}
          >
            {itemEl}
          </Text>
        )
    } else
      return (
        <Text
          size='mini'
          type='Circular'
          color={this._isSelected(item) ? REDDISH : GUN_METAL}
          weight={this._isSelected(item) ? 400 : 300}
        >
          {item[suggestionPathResult]}
        </Text>
      )
  };

  _createNewOption(toggleModal) {
    const { createNew, suggestionKey } = this.props
    const { keyword, suggestions } = this.state

    if (!createNew && isEmpty(suggestions)) {
      return (
        <View style={{ paddingTop: WP3, paddingHorizontal: WP2 }}>
          <Text
            centered
            size='slight'
            type='Circular'
            color={SHIP_GREY_CALM}
            weight={500}
          >{`Sorry, no results found for “${keyword}”`}</Text>
        </View>
      )
    }

    if (
      createNew &&
      (isEmpty(suggestions) ||
        (suggestions || [])
          .map((item) => {
            let result = suggestionKey ? item[suggestionKey] : item
            if (isString(result)) result = result.toLowerCase()
            return result
          })
          .indexOf(keyword.toLowerCase()) == -1)
    ) {
      return (
        <View>
          <View style={{ paddingVertical: WP205, paddingHorizontal: WP1 }}>
            <Text size='mini' type='Circular' color={GUN_METAL} weight={300}>
              {keyword}
            </Text>
          </View>
          <Button
            onPress={() => {
              this._onCreateNewOption(toggleModal)
            }}
            backgroundColor={CLEAR_BLUE}
            nativeEffect
            centered
            radius={6}
            compact='flex-end'
            shadow='none'
            textColor={WHITE}
            textSize='tiny'
            textType='Circular'
            textWeight={400}
            marginless
            style={{ alignSelf: 'flex-end', marginTop: WP105 }}
            text='Create New'
          />
        </View>
      )
    }
  }
  // END RENDER LIST

  render() {
    const {
      triggerComponent,
      suggestionPathResult,
      placeholder,
      header,
      selection,
      selectionWording,
      actionNext,
      disabled,
    } = this.props

    const { keyword, suggestions, suggestionsRecent, showSuggestion } = this.state
    return (
      <Modal
        renderModalContent={({ toggleModal }) => (
          <SafeAreaView style={{ height: HP50 }}>
            <View
              style={{
                backgroundColor: WHITE,
                height: HP50,
              }}
            >
              <View style={{ flex: 1 }}>
                {/* header */}
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingHorizontal: WP4,
                    paddingVertical: WP3,
                    borderBottomWidth: 1,
                    borderBottomColor: PALE_BLUE,
                  }}
                >
                  <Text type='Circular' size='small' weight={600} color={NAVY_DARK}>
                    {header}
                  </Text>
                  <Icon
                    centered
                    onPress={toggleModal}
                    background='dark-circle'
                    size='large'
                    color={SHIP_GREY_CALM}
                    name='close'
                    type='MaterialCommunityIcons'
                  />
                </View>

                {/* body */}
                <View style={{ flex: 1, paddingHorizontal: WP4, paddingTop: WP4 }}>
                  {/* search input */}
                  <View
                    style={{
                      backgroundColor: 'rgba(145, 158, 171, 0.1)',
                      borderRadius: 6,
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingHorizontal: WP2,
                      paddingVertical: WP105,
                    }}
                  >
                    <Icon
                      centered
                      background='dark-circle'
                      size='large'
                      color={SHIP_GREY_CALM}
                      name='magnify'
                      type='MaterialCommunityIcons'
                    />
                    <TextInput
                      returnKeyType='search'
                      value={keyword}
                      numberOfLines={1}
                      style={{
                        flex: 1,
                        fontFamily: 'CircularBook',
                        color: SHIP_GREY_CALM,
                        fontSize: WP308,
                        marginRight: WP2,
                        paddingVertical: WP105,
                      }}
                      onChangeText={this._onChangeText}
                      placeholderTextColor={SHIP_GREY_CALM}
                      onFocus={this._onFocus}
                      placeholder={placeholder}
                    />
                    {!isEmpty(keyword) && (
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({ keyword: '' }, this._onClearInput)
                        }
                      >
                        <Icon
                          centered
                          color={SHIP_GREY_CALM}
                          type='MaterialCommunityIcons'
                          name='close'
                          size='slight'
                        />
                      </TouchableOpacity>
                    )}
                  </View>

                  {/*Suggestion*/}
                  {showSuggestion &&
                    this._suggestionList(
                      suggestions,
                      toggleModal,
                      suggestionPathResult,
                      suggestionsRecent,
                    )}
                </View>
              </View>

              {/* footer */}
              <View
                style={{
                  backgroundColor: WHITE,
                  width: '100%',
                  paddingHorizontal: WP4,
                  height: WP15,
                }}
              >
                <View
                  style={{
                    width: '100%',
                    height: 1,
                    backgroundColor: PALE_BLUE,
                  }}
                />
                {isArray(selection) && (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingVertical: WP3,
                    }}
                  >
                    <Text
                      type='Circular'
                      size='xmini'
                      weight={400}
                      color={SHIP_GREY}
                    >
                      {selection.length > 0
                        ? `${selection.length} ${selectionWording}`
                        : ' '}
                    </Text>
                    <View>
                      {isEmpty(keyword) ? (
                        <TouchableOpacity
                          disabled={selection.length == 0}
                          activeOpacity={TOUCH_OPACITY}
                          onPress={() => {
                            toggleModal()
                            setTimeout(() => {
                              actionNext()
                            }, 500)
                          }}
                          style={{
                            paddingHorizontal: WP3,
                            paddingVertical: WP2,
                            borderRadius: 6,
                            backgroundColor:
                              selection.length > 0 ? CLEAR_BLUE : BABY_BLUE,
                          }}
                        >
                          <Text
                            type='Circular'
                            size='xmini'
                            weight={400}
                            color={WHITE}
                          >
                            Done
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <View style={{ height: WP8 }} />
                      )}
                    </View>
                  </View>
                )}
              </View>
            </View>
          </SafeAreaView>
        )}
      >
        {({ toggleModal, isVisible }, M) => (
          <View style={{ flexGrow: 1 }}>
            <TouchableOpacity
              activeOpacity={disabled ? 1 : TOUCH_OPACITY}
              onPress={() => {
                if (!this.props.disabled) {
                  if (!isVisible) this._onFetchSuggestion()
                  toggleModal()
                }
              }}
            >
              <View pointerEvents='none'>{triggerComponent}</View>
            </TouchableOpacity>
            {M}
          </View>
        )}
      </Modal>
    )
  }
}

SelectModalV3.propTypes = propsType

SelectModalV3.defaultProps = propsDefault

export default connect(mapStateToProps, {})(SelectModalV3)
