import React from 'react'
import { View, StyleSheet } from 'react-native'
import { Text, Image } from 'sf-components'
import Spacer from 'sf-components/Spacer'
import { WP4, WP5, WP20 } from 'sf-constants/Sizes'
import { SHIP_GREY_CALM } from 'sf-constants/Colors'
import { LinearGradient } from 'expo-linear-gradient'
import { GUN_METAL, PALE_BLUE_TWO, PALE_LIGHT_BLUE_TWO } from '../../constants/Colors'
import { WP10, WP30 } from '../../constants/Sizes'

const style = StyleSheet.create({
  container: { paddingHorizontal: WP10 }
})

const Empty = ({ query, isInvitation = false }) => {
  return (
    <View style={style.container}>
      <Spacer size={WP20} />
      {!isInvitation ?
        <Image size={'extraMassive'} source={require('sf-assets/images/emptyMagnifier.png')} /> :
        <LinearGradient
          colors={[PALE_BLUE_TWO, PALE_LIGHT_BLUE_TWO]}
          style={{
            width: WP30,
            height: WP30,
            borderRadius: WP30 / 2,
            justifyContent: 'center',
            alignSelf: 'center'
          }}
        >
          <Image size={'huge'} source={require('sf-assets/images/fluentPeopleCommunity.png')} />
        </LinearGradient>}

      <Spacer size={WP4} />
      { isInvitation && <Text size='small' centered type='Circular' color={GUN_METAL} weight={500}>{'Belum ada invitation'}</Text> }
      <Text
        lineHeight={WP5}
        size={'mini'}
        color={SHIP_GREY_CALM}
        type={'Circular'}
        centered
      >
        {isInvitation? 'Ayo undang member founder kamu untuk mengikuti rangkaian program ini' : query ? `Maaf, kami tidak menemukan pencarian untuk “${query}”` : 'Maaf, kami tidak menemukan hasil untuk pencarian terkait'}
      </Text>
    </View>
  )
}

export default Empty
