import React from 'react'
import { View } from 'react-native'
import { WP4, WP12 } from 'sf-constants/Sizes'
import { isEmpty } from 'lodash-es'
import { CYPRUS, GREEN_MMB, WHITE } from 'sf-constants/Colors'
import Icon from '../Icon'
import Text from '../Text'

const Alert = ({ textError }) => {
  return (
    <View style={{
      position: 'absolute',
      width: '100%',
      height: WP12,
      flexDirection: 'row',
      backgroundColor: isEmpty(textError) ? GREEN_MMB : CYPRUS,
      alignItems: 'center',
      paddingHorizontal: WP4,
      justifyContent: 'space-between'

    }}
    >
      <Text
        type='Circular'
        size='slight'
        weight={300}
        color={WHITE}
      >
        {isEmpty(textError) ? 'Invitation berhasil dikirimkan' : textError}
      </Text>
      <Icon
        centered
        background='dark-circle'
        size='large'
        color={WHITE}
        name={isEmpty(textError) ? 'check-circle-outline' : 'dnd-forwardslash'}
        type='MaterialIcons'
      // style={style.headerSearchIcon}
      />
    </View>
  )
}

export default Alert
