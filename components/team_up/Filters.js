import React from 'react'
import { ScrollView, View, StyleSheet } from 'react-native'
import { WP05, WP105, WP2, WP3, WP5 } from 'sf-constants/Sizes'
import { SHIP_GREY, CLEAR_BLUE, WHITE, PALE_BLUE } from 'sf-constants/Colors'
import { noop, startCase, toLower, isEmpty } from 'lodash-es'
import SelectModalV3 from 'sf-components/SelectModalV3'
import { SHIP_GREY_CALM, PALE_GREY } from 'sf-constants/Colors'
import { getCityV3 } from 'sf-actions/api'
import { WP1 } from 'sf-constants/Sizes'
import Text from '../Text'
import Spacer from '../Spacer'
import ButtonV2 from '../ButtonV2'
import Icon from '../Icon'

const style = StyleSheet.create({
  scrollView: {
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  container: {
    paddingVertical: WP3,
    flexDirection: 'row',
    alignItems: 'center',
  },
  option: {
    paddingVertical: WP105,
    paddingHorizontal: WP3,
    marginRight: WP2,
    marginLeft: WP05,

  },
})

const Filters = ({ onChangeCity, selectedCity, isActive, roleType, }) => {
  const _renderModal = ({ component }) => {
    return (
      <SelectModalV3
        refreshOnSelect
        triggerComponent={component}
        header='Kota/Lokasi'
        suggestion={getCityV3}
        suggestionKey='city_name'
        suggestionPathResult='city_name'
        suggestionPathValue='id_city'
        onChange={onChangeCity}
        createNew={false}
        reformatFromApi={(text) => startCase(toLower(text))}
        placeholder='Coba cari Kota'
        selection={selectedCity || ''}
        showSelection
        onRemoveSelection={() => onChangeCity({})}
        asObject
      />
    )
  }

  const _renderOption = () => {
    return (
      <>
        <ButtonV2
          onPress={noop}
          disabled={true}
          color={PALE_GREY}
          textColor={SHIP_GREY_CALM}
          textWeight={400}
          style={style.option}
          forceBorderColor={true}
          text={roleType || 'Role'}
        />

        {_renderModal({
          component: (
            <ButtonV2
              onPress={noop}
              color={!isEmpty(selectedCity) ? CLEAR_BLUE : null}
              textColor={!isEmpty(selectedCity) ? WHITE : SHIP_GREY}
              textWeight={400}
              style={style.option}
              forceBorderColor={true}
              text={selectedCity || 'Semua Lokasi'}
              rightComponent={
                (
                  !isEmpty(selectedCity) &&
                  <View style={{ marginLeft: WP1 }}>
                    <Icon
                      name={'chevron-down'}
                      type={'Entypo'}
                      color={WHITE}
                    />
                  </View>

                )
              }
            />
          )
        })}

      </>
    )
  }

  return (
    <View>
      <ScrollView
        style={style.scrollView}
        showsHorizontalScrollIndicator={false}
        horizontal
      >
        <Spacer horizontal size={WP5} />
        <View style={style.container}>
          <Text
            color={SHIP_GREY}
            weight={500}
            size={'mini'}
            type={'Circular'}
          >
            {'Filter:  '}
          </Text>
          {_renderOption()}
        </View>
        <Spacer horizontal size={WP3} />
      </ScrollView>
    </View>
  )
}

export default Filters
