import { noop } from 'lodash-es'
import React from 'react'
import { KeyboardAvoidingView, Platform, View } from 'react-native'
import Modal from 'react-native-modal'
import Icon from 'sf-components/Icon'
import Touchable from 'sf-components/Touchable'
import { GUN_METAL, NAVY_DARK, PALE_GREY, REDDISH, SHIP_GREY, SHIP_GREY_CALM, TRANSPARENT, WHITE } from 'sf-constants/Colors'
import { WP1, WP100, WP2, WP3, WP4, WP5 } from 'sf-constants/Sizes'
import { MODAL_STYLE } from 'sf-constants/Styles'
import { LinearGradient } from 'expo-linear-gradient'
import Text from '../Text'
import Avatar from '../Avatar'
import Spacer from '../Spacer'
import ButtonV2 from '../ButtonV2'
import { SHADOW_GRADIENT } from '../../constants/Colors'
import { HEADER } from '../../constants/Styles'

const PersonModal = ({ isVisible, onPressClose, role, selectedPerson, onPressInvite, followers, followings, city, province, description }) => {

  const isInvited = selectedPerson?.is_invited || false

  const _renderGunMetalText = (text) => {
    return (
      <Text
        type='Circular'
        size={'mini'}
        // lineHeight={10}
        style={{ textTransform: 'capitalize' }}
        weight={400}
        color={GUN_METAL}
      >
        {text}
      </Text>
    )
  }

  const _renderShipGreyText = (text) => {
    return (
      <Text
        type='Circular'
        size={'mini'}
        style={{ textTransform: 'capitalize' }}
        // lineHeight={10}
        weight={300}
        color={SHIP_GREY_CALM}
      >
        {text}
      </Text>
    )
  }

  return (
    <Modal
      onBackdropPress={noop}
      transparent
      isVisible={isVisible}
      style={[MODAL_STYLE['bottom']]}
      propagateSwipe={Platform.OS === 'ios'}
      useNativeDriver={true}
      avoidKeyboard={Platform.OS === 'ios'}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{ flex: 1, justifyContent: 'flex-end' }}
      >
        <View
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          style={{ flex: 1, justifyContent: 'flex-end' }}
        >
          <View
            style={{
              width: WP100,
              backgroundColor: WHITE,
              // height: WP100,
              justifyContent: 'flex-start',
              borderTopLeftRadius: WP4,
              borderTopRightRadius: WP4,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingHorizontal: WP4,
                paddingVertical: WP4,
                alignItems: 'center'
              }}
            >
              <Touchable
                disabled
              // onPress={onClearText}
              // style={style.clearText}
              >
                <Icon
                  centered
                  background='dark-circle'
                  size='large'
                  color={TRANSPARENT}
                  name='close'
                  type='MaterialCommunityIcons'
                // style={style.headerSearchIcon}
                />
              </Touchable>

              <Text
                style={{ textTransform: 'capitalize' }}
                type='Circular'
                size='small'

                weight={400}
                color={SHIP_GREY}
              >
                {selectedPerson?.full_name}
              </Text>

              <Touchable
                onPress={onPressClose}
              // style={style.clearText}
              >
                <Icon
                  centered
                  background='dark-circle'
                  size='large'
                  color={SHIP_GREY_CALM}
                  name='close'
                  type='MaterialCommunityIcons'
                // style={style.headerSearchIcon}
                />
              </Touchable>
            </View>
            <View style={{ height: 0.75, backgroundColor: PALE_GREY }} />
            <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />

            <View
              key={Math.random()}
              style={{
                flexDirection: 'row',
                paddingHorizontal: WP5,
                paddingVertical: WP5,
                alignItems: 'center',
                // backgroundColor: BLUE_CHAT
              }}>
              <Avatar size={'massive'} image={{ uri: selectedPerson?.profile_picture }} />
              <View style={{ width: WP3 }} />
              <View style={{
                flex: 1,
                // backgroundColor: REDDISH
              }}>
                <Text
                  type='Circular'
                  size={'small'}
                  // lineHeight={10}
                  style={{ textTransform: 'capitalize' }}
                  weight={500}
                  color={NAVY_DARK}
                >
                  {selectedPerson?.full_name}
                </Text>
                <Text
                  type='Circular'
                  size={'mini'}
                  // lineHeight={10}
                  weight={400}
                  color={SHIP_GREY}
                >
                  {role}
                </Text>
                <Spacer size={WP3} />
                <View style={{ flexDirection: 'row' }} >
                  {_renderGunMetalText(selectedPerson?.total_followers || followers || '0' )}
                  {_renderShipGreyText(' Followers')}
                  <Spacer horizontal size={WP3} />
                  {_renderGunMetalText(selectedPerson?.total_following || followings || '0')}
                  {_renderShipGreyText(' Following')}
                </View>
              </View>

            </View>
            <View style={{ height: 1, backgroundColor: PALE_GREY }} />
            <View style={{ padding: WP4 }}>
              <Text
                type='Circular'
                size={'mini'}
                lineHeight={20}
                weight={300}
                color={SHIP_GREY}
              >
                {selectedPerson?.about_me || description || 'Tidak ada deskripsi'}
              </Text>
              <Spacer size={WP2} />
              {_renderShipGreyText(`${selectedPerson?.location?.city_name || city}, ${selectedPerson?.location?.province_name || province}`)}

            </View>
            {onPressInvite && <ButtonV2
              style={{
                // paddingHorizontal: WP3,
                paddingVertical: WP1,
                marginVertical: WP4,
                marginHorizontal: WP4,
              }}
              color={isInvited ? WHITE : REDDISH}
              // key={index}
              textColor={isInvited ? REDDISH : WHITE}
              forceBorderColor={true}
              borderColor={isInvited ? REDDISH : TRANSPARENT}
              icon={isInvited ? 'check' : 'account-plus'}
              // iconType={'Entypo'}
              text={isInvited ? ' Invited' : ' Invite'}
              iconColor={isInvited ? REDDISH : WHITE}
              onPress={isInvited ? noop : onPressInvite}
            />}
          </View>
        </View>
      </KeyboardAvoidingView>

    </Modal>
  )
}

export default PersonModal
