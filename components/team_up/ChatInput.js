import React from 'react'
import { useState } from 'react'
import { Dimensions } from 'react-native'
import { Image, TouchableOpacity } from 'react-native'
import { TextInput } from 'react-native'
import { View, StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white'
  },
  separator: {
    height: 1,
    backgroundColor: '#E4E6E7',
    width: '100%',
    marginBottom: 12
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },
  textInput: {
    width: '70%',
    maxHeight: Dimensions.get('screen').height / 4,
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: '#F4F6F8',
    marginRight: 20,
    borderRadius: 10
  },
  sendButtonContainer: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    backgroundColor: '#FF651F',
    alignItems: 'center',
    justifyContent: 'center'
  },
  sendButton: {
      width: 50,
      height: 50,
      tintColor: 'white'
  }
})

const ChatInput = ({ placeholder, onSend }) => {
  const [message, setMessage] = useState('')

  return (
    <View style={styles.container}>
        <View style={styles.separator}/>
        <View style={styles.inputContainer}>
            <TextInput
                style={styles.textInput}
                placeholder={placeholder}
                multiline
                value={message}
                onChangeText={(text) => setMessage(text)}
            />
            <TouchableOpacity
              style={styles.sendButtonContainer} onPress={() => {
                onSend(message)
                setMessage('')
              }}
            >
                <Image style={styles.sendButton} source={require('../../assets/icons/icMessage.png')}/>
            </TouchableOpacity>
        </View>
    </View>
  )
}

export default ChatInput
