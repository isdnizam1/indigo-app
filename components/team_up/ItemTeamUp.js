import { isEmpty } from 'lodash-es'
import React from 'react'
import { TouchableOpacity } from 'react-native'
import { View } from 'react-native'
import { GUN_METAL, SHIP_GREY_CALM, PALE_GREY, WHITE } from 'sf-constants/Colors'
import { WP3, WP5 } from 'sf-constants/Sizes'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import { WP1 } from '../../constants/Sizes'
import Avatar from '../Avatar'
import Text from '../Text'

const ItemTeamUp = ({ title, subtitle, extraSubtitle, button, profilePicture, index, onPressItem }) => {

  const _renderButton = ({ backgroundColor, onPress, text, borderColor, textColor }) => {
    return (
      <TouchableOpacity
        key={Math.random()}
        activeOpacity={TOUCH_OPACITY}
        onPress={onPress}
      >
        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
          paddingHorizontal: WP3,
          paddingVertical: WP1,
          borderRadius: 5,
          alignSelf: 'center',
          backgroundColor,
          borderWidth: 1,
          borderColor,
          marginLeft: WP1,
          borderStyle: 'solid'
        }}
        >
          <Text
            type='Circular'
            size={'xmini'}
            // lineHeight={10}
            weight={400}
            color={textColor || WHITE}
          >
            {text}
          </Text>
        </View>

      </TouchableOpacity>
    )
  }

  return (
    <TouchableOpacity
      activeOpacity={TOUCH_OPACITY}
      onPress={onPressItem}
    >
      <View
        key={index || Math.random()}
        style={{
          flexDirection: 'row',
          paddingHorizontal: WP5,
          paddingVertical: WP5,
          alignItems: 'center',
          // backgroundColor: BLUE_CHAT
        }}
      >
        <Avatar image={{ uri: profilePicture }} />
        <View style={{ width: WP3 }} />

        <View style={{
          flex: 1,
          // backgroundColor: REDDISH
        }}
        >
          <Text
            type='Circular'
            size={'small'}
            style={{ textTransform: 'capitalize' }}
            // lineHeight={10}
            weight={500}
            color={GUN_METAL}
          >
            {title || 'Title'}
          </Text>
          <Text
            type='Circular'
            size={'mini'}
            style={{ textTransform: 'capitalize' }}
            // lineHeight={10}
            numberOfLines={1}
            weight={400}
            color={SHIP_GREY_CALM}
          >
            {subtitle || 'Subtitle'}
            {!isEmpty(extraSubtitle) &&
              <Text
                type='Circular'
                size={'xtiny'}
                style={{ textTransform: 'capitalize' }}
                // lineHeight={10}
                numberOfLines={1}
                weight={400}
                color={SHIP_GREY_CALM}
              >
                {'  •  '}
              </Text>
            }
            {!isEmpty(extraSubtitle) && extraSubtitle}
          </Text>
        </View>
        <View style={{ width: WP5 }} />
        {button && button.map(_renderButton)}
      </View>
      <View style={{ height: 0.75, backgroundColor: PALE_GREY }} />
    </TouchableOpacity>
  )
}

export default ItemTeamUp
