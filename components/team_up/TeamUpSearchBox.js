import React, { useRef } from 'react'
import {
  View,
  TextInput,
  StyleSheet,
} from 'react-native'
import { isEmpty } from 'lodash-es'
import Touchable from 'sf-components/Touchable'
import Icon from 'sf-components/Icon'
import Spacer from 'sf-components/Spacer'
import Text from 'sf-components/Text'
import {
  REDDISH,
  SHIP_GREY_CALM,
  SHADOW_GRADIENT,
  PALE_BLUE,
  SHIP_GREY
} from 'sf-constants/Colors'
import { WP05, WP1, WP2, WP3, WP4 } from 'sf-constants/Sizes'
import { LinearGradient } from 'expo-linear-gradient'
import { HEADER } from 'sf-constants/Styles'

const TeamUpSearchBox = ({ ref, onChangeText, onPressBack, onClearText, text }) => {

  const textInputSearch = useRef(null)

  return (
    <View style={style.wrapper}>
      <View style={style.container}>
        <Touchable style={style.backButton}>
          <Icon
            centered
            onPress={onPressBack}
            background='dark-circle'
            size='large'
            color={SHIP_GREY_CALM}
            name='chevron-left'
            type='Entypo'
          />
        </Touchable>
        <View style={style.headerSearch}>
          <Icon
            centered
            background='dark-circle'
            size='large'
            color={SHIP_GREY_CALM}
            name='magnify'
            type='MaterialCommunityIcons'
            style={style.headerSearchIcon}
          />
          <TextInput
            ref={textInputSearch}
            onChangeText={onChangeText}
            autoFocus={!__DEV__}
            placeholder={'Cari nama member'}
            style={style.headerSearchInput}
          />
          {!isEmpty(text) &&
            <Touchable
              onPress={() => {
                textInputSearch.current.clear()
                onClearText()
              }}
              style={style.clearText}
            >
              <Icon
                centered
                background='dark-circle'
                size='mini'
                color={SHIP_GREY_CALM}
                name='close'
                type='MaterialCommunityIcons'
                style={style.headerSearchIcon}
              />
            </Touchable>
          }
        </View>
        <Spacer horizontal size={WP4} />

        <Touchable
          onPress={onPressBack}
          style={style.cancel}
        >
          <Text
            // style={{ backgroundColor:BLUE_CHAT }}
            type='Circular'
            size='xmini'
            weight={400}
            color={REDDISH}
          >
            Cancel
          </Text>
        </Touchable>
      </View>
      <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
    </View>
  )
}

const style = StyleSheet.create({
  wrapper: {
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  container: {
    paddingVertical: WP2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerSearch: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    backgroundColor: 'rgb(244, 246, 248)',
    paddingLeft: WP2,
    borderRadius: WP2,
  },
  headerSearchInput: {
    padding: WP1,
    flex: 1,
    color: SHIP_GREY
  },
  backButton: {
    paddingVertical: WP1,
    paddingHorizontal: WP3,
  },
  tabs: {
    flexDirection: 'row',
    paddingHorizontal: WP2,
    borderTopWidth: 1,
    borderTopColor: PALE_BLUE,
  },
  tab: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: WP3 + WP05,
  },
  indicator: {
    height: WP1 - WP05,
    backgroundColor: REDDISH,
    position: 'absolute',
    bottom: -1,
  },
  cancel: {
    paddingVertical: WP2 + WP05 + 0.5,
    paddingRight: WP3,
  },
  clearText: {
    padding: WP2,
  },
  loader: {
    paddingHorizontal: WP2,
  },
})

export default TeamUpSearchBox
