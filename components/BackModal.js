import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { connect } from 'react-redux'
import Modal from 'react-native-modal'
import {
  HP2, WP4, WP30, WP100
} from '../constants/Sizes'
import {
  GREY,
  WHITE,
  ORANGE } from '../constants/Colors'
import Text from './Text'
import Button from './Button'
import Image from './Image'

const propsType = {
  navigateBack: PropTypes.func,
  dismiss: PropTypes.func,
  modalVisible: PropTypes.bool,
}

const propsDefault = {
  navigateBack: () => {},
  dismiss: () => {},
  modalVisible: false,
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

class BackModal extends Component {
  constructor(props) {
    super(props)
  }

  _toggleModal = () => {
    this.modal._toggleModal()
  }

  render() {
    const { navigateBack, modalVisible, dismiss } = this.props

    return (
      <View>
        <Modal
          onBackButtonPress={dismiss}
          onBackdropPress={dismiss}
          isVisible={modalVisible}
          animationType={'slide'}
          animationIn='slideInUp'
          animationOut='slideOutDown'
          style={{
            margin: 0,
            justifyContent: 'flex-end',
          }}
        >
          <View
            style={{
              backgroundColor: WHITE,
              width: WP100,
              padding: WP4,
              justifyContent: 'center',
            }}
          >
            <Image
              source={require('./../assets/images/areYouSure.png')}
              style={{ marginVertical: HP2 }}
              imageStyle={{ height: WP30 }}
              aspectRatio={150 / 74}
            />
            <Text
              centered
              type='NeoSans'
              color={GREY}
              size='large'
              weight={500}
            >
              Discard Changes?
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginVertical: HP2,
                alignItems: 'center',
                justifyContent: 'space-around'
              }}
            >
              <Button
                onPress={() => {
                  dismiss()
                }}
                width={WP30}
                shadow='none'
                backgroundColor={WHITE}
                centered
                bottomButton
                radius={WP4}
                textType='NeoSans'
                textSize='small'
                textColor={ORANGE}
                textWeight={500}
                text='Cancel'
                contentStyle={{
                  borderWidth: 1,
                  borderColor: ORANGE,
                }}
              />
              <Button
                onPress={() => {
                  dismiss()
                  navigateBack()
                }}
                width={WP30}
                backgroundColor={ORANGE}
                centered
                bottomButton
                radius={WP4}
                shadow='none'
                textType='NeoSans'
                textSize='small'
                textColor={WHITE}
                textWeight={500}
                text='Discard'
              />
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

BackModal.propTypes = propsType

BackModal.defaultProps = propsDefault

export default connect(mapStateToProps, {})(BackModal)
