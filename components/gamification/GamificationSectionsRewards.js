import React from 'react'
import { LinearGradient } from 'expo-linear-gradient'
import { isEmpty, split, upperCase } from 'lodash-es'
import { ScrollView, TouchableOpacity, View } from 'react-native'
import { TOMATO_CALM, WHITE, WHITE_MILK } from '../../constants/Colors'
import { WP100, WP12, WP205, WP3, WP6, WP7 } from '../../constants/Sizes'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { postCheckClaim } from '../../actions/api'
import Image from '../Image'
import Text from '../Text'
import ModalMessageTrigger from '../ModalMessage/ModalMessageTrigger'

export const REWARDS_STYLE = {
  PRO: {
    colors: ['rgba(76,76,76, 0.5)', 'rgba(143,143,143,0.3)'],
    start: [0, 0],
    end: [1, 0],
  },
  MASTER: {
    colors: ['rgba(0,35,78,0.5)', 'rgba(38,128,235, 0.3)'],
    start: [0, 0],
    end: [1, 0],
  },
  LEGEND: {
    colors: ['rgba(77, 0, 4, 0.5)', 'rgba(201, 37, 45, 0.3)'],
    start: [0, 0],
    end: [1, 0],
  },
}

const GamificationSectionsRewards = ({ navigateTo, user, available_reward, isLoading = false }) => {
  let [message, setMessage] = React.useState(null)
  return (
    <View>
      <View style={{ paddingHorizontal: WP6 }}>
        <Text size='mini' type='NeoSans' weight={500}>Rewards</Text>
      </View>
      {
        available_reward?.length > 0 ? (
          <ScrollView
            style={{ flexGrow: 0 }}
            contentContainerStyle={{ paddingHorizontal: WP6, paddingBottom: WP7, paddingTop: WP3 }}
            showsVerticalScrollIndicator={false}
          >
            {
              available_reward.map((item, index) => {
                const widthItem = WP100 - WP12
                const heightImage = widthItem / 3.17
                const styles = REWARDS_STYLE[upperCase(item.type)]
                const splittedLink = split(item.redirect, '/')
                const params = { category: splittedLink[1] || item.type, reward: item, userReward: user }
                const action = () => { navigateTo(`${splittedLink[0]}`, params) }
                return (
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    onPress={action}
                    key={index}
                    style={{
                      width: widthItem,
                      backgroundColor: WHITE,
                      borderRadius: 12,
                      marginBottom: WP3,
                      ...SHADOW_STYLE.shadowThin,
                    }}
                  >
                    <View style={{ borderRadius: 12, overflow: 'hidden', }}>
                      <View style={{ backgroundColor: WHITE_MILK, height: heightImage, width: '100%' }}>
                        <Image
                          source={{ uri: item.background_img }}
                          imageStyle={{ width: widthItem, aspectRatio: 3 }}
                        />
                        <LinearGradient
                          colors={styles.colors}
                          start={styles.start}
                          end={styles.end}
                          style={{
                            width: '100%',
                            height: heightImage,
                            position: 'absolute',
                            zIndex: 2,
                            justifyContent: 'flex-end',
                            padding: WP3,
                          }}
                        >
                          <Text size='mini' weight={400} color={WHITE} style={{ ...SHADOW_STYLE['shadow'] }}>{item.label}</Text>
                        </LinearGradient>
                      </View>
                      <View style={{ backgroundColor: WHITE, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'flex-end' }}>
                        <View style={{ paddingHorizontal: WP3, paddingVertical: WP205 }}>
                          <Text size='xmini'>{item.type}</Text>
                        </View>

                        <ModalMessageTrigger
                          title={'Sorry'}
                          subtitle={message ?? 'You’re not there yet..\nEarn more stars to claim this reward'}
                          buttonPrimaryText={'Get My Stars'}
                          buttonPrimaryAction={() => { }}
                          triggerComponent={(togle) => (

                            <TouchableOpacity
                              activeOpacity={TOUCH_OPACITY}
                              onPress={() => {
                                if (Number(user?.current_stage.stars) >= Number(item.stars)) {
                                  const type = upperCase(item.type)
                                  const routes = item.feature.includes('sf_play') ? 'ClaimAlbumList' : item.feature.includes('sf_connect') ? 'ClaimSessionList' : type == 'LEGEND' ? 'ClaimRewardForm' : ''
                                  postCheckClaim({ id_user: user.id_user, feature: item.feature }).then((response) => {
                                    if (!isEmpty(routes) && response.data.code == 200) navigateTo(routes, { userReward: user, selectedReward: item })
                                    else {
                                      setMessage(response.data.message)
                                      togle()
                                    }
                                  })
                                } else {
                                  togle()
                                }
                              }}
                              style={{ backgroundColor: TOMATO_CALM, paddingHorizontal: WP6, paddingVertical: WP205 }}
                            >
                              <Text color={WHITE} type='NeoSans' weight={500} size='mini'>Claim</Text>
                            </TouchableOpacity>
                          )}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                )
              })
            }
          </ScrollView>
        ) : (
          <View style={{ paddingHorizontal: WP6 }}>
            <Text size='small'>Sorry, there are no reward available</Text>
          </View>
        )
      }
    </View>
  )
}

export default GamificationSectionsRewards
