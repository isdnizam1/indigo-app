import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import { WP2, WP3, WP6 } from '../../constants/Sizes'
import { TOMATO_CALM, WHITE } from '../../constants/Colors'
import { TOUCH_OPACITY } from '../../constants/Styles'
import Text from '../Text'
import Modal from '../Modal'
import Container from '../Container'
import Button from '../Button'
import HeaderNormal from '../HeaderNormal'
import { getBottomSpace, ifIphoneX } from '../../utils/iphoneXHelper'

const propsType = {
  header: PropTypes.string,
  title: PropTypes.string,
  triggerComponent: PropTypes.any,
  description: PropTypes.string,
  buttonText: PropTypes.string,
  buttonAction: PropTypes.func,
}

const propsDefault = {
  header: 'About Rewards',
  title: 'Description',
  description: '',
  buttonText: 'Continue',
  buttonAction: () => { }
}

const AboutRewardModal = (props) => {
  const {
    triggerComponent,
    header,
    title,
    description,
    buttonText,
    buttonAction
  } = props

  return (
    <Modal
      style={{ flex: 1, backgroundColor: WHITE }}
      isVisible={false}
      renderModalContent={({ toggleModal }) => (
        <Container
          theme='dark'
          isReady={true}
          scrollable
          scrollBackgroundColor={WHITE}
          style={{ paddingBottom: getBottomSpace() }}
          renderHeader={() => (
            <HeaderNormal
              iconLeftColor={'transparent'}
              text={header}
              centered
            />
          )}
          outsideScrollContent={() => (
            <View style={{ width: '100%', backgroundColor: WHITE, paddingHorizontal: WP6, paddingBottom: ifIphoneX(WP3, WP6), paddingTop: WP2 }}>
              <Button
                colors={[TOMATO_CALM, TOMATO_CALM]}
                compact='center'
                radius={15}
                textStyle={{ marginVertical: WP3 }}
                marginless
                text={buttonText}
                textSize='medium'
                textType='NeoSans'
                textColor={WHITE}
                textWeight={500}
                centered
                onPress={() => {
                  buttonAction()
                  toggleModal()
                }}
                shadow='none'
              />
            </View>
          )}
        >
          <View style={{ padding: WP6 }}>
            <Text size='mini' weight={500} >{title}</Text>
            <Text size='mini'>{description}</Text>
          </View>
        </Container>
      )}
    >
      {({ toggleModal }, M) => (
        <View>
          <TouchableOpacity
            onPress={toggleModal}
            activeOpacity={TOUCH_OPACITY}
          >
            {triggerComponent}
          </TouchableOpacity>
          {M}
        </View>
      )}
    </Modal>
  )
}

AboutRewardModal.propTypes = propsType

AboutRewardModal.defaultProps = propsDefault

export default AboutRewardModal