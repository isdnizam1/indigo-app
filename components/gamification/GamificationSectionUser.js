import React, { memo } from 'react'
import { View, Image as RNImage } from 'react-native'
import { first, isEmpty, map, upperCase } from 'lodash-es'
import { LinearGradient } from 'expo-linear-gradient'
import { AZUL, GREY, GREY_CHAT, GREY_WARM, ICE_BLUE, LIPSTICK, LIPSTICK_DARK, LIPSTICK_LIGHT, PUMPKIN, PUMPKIN_DARK, PUMPKIN_LIGHT, SOFT_BLUE } from '../../constants/Colors'
import { WP1, WP105, WP135, WP15, WP2, WP205, WP3, WP308, WP4, WP6, WP7 } from '../../constants/Sizes'
import Text from '../Text'
import Image from '../Image'
import { thousandSeparator } from '../../utils/helper'

const REWARDS_STYLE = {
  ROOKIE: {
    colors: [GREY, GREY_WARM],
    start: [0, 0],
    end: [1, 0],
    background: GREY_CHAT,
    image: null
  },
  PRO: {
    colors: [AZUL, SOFT_BLUE],
    start: [0, 0],
    end: [1, 0],
    background: ICE_BLUE,
    image: require('../../assets/icons/shieldPro.png'),
    imageRatio: 41 / 44,
    imageOpacity: 1,
  },
  MASTER: {
    colors: [LIPSTICK_DARK, LIPSTICK],
    start: [0, 0],
    end: [1, 0],
    background: LIPSTICK_LIGHT,
    image: require('../../assets/icons/shieldMaster.png'),
    imageRatio: 40 / 44,
    imageOpacity: 1,
  },
  LEGEND: {
    colors: [PUMPKIN_DARK, PUMPKIN],
    start: [0, 0],
    end: [1, 0],
    background: PUMPKIN_LIGHT,
    image: require('../../assets/icons/shieldLegend.png'),
    imageRatio: 43 / 44,
    imageOpacity: 0.5,
  },
}

const generatePercent = (curent, max) => {
  return (Number(curent) / Number(max)) * 100
}

const GamificationSectionUser = ({ user, isLoading = false }) => {
  const nextStyle = REWARDS_STYLE[upperCase(first(user?.next_stage)?.type)]
  const currentStyle = REWARDS_STYLE[upperCase(user?.current_stage.type)]
  const sizeNext = user?.next_stage.length
  const styles = sizeNext > 0 ? nextStyle : currentStyle
  return (
    <View style={{ paddingHorizontal: WP6, marginBottom: WP7 }}>
      <View style={{ paddingHorizontal: WP3, marginBottom: WP3, alignItems: 'center' }}>
        <Text centred size='small' weight={500}>{user?.full_name}</Text>
        <View style={{ width: '90%', height: 1, backgroundColor: GREY_CHAT, marginVertical: WP105 }} />
        {
          sizeNext > 0 && (
            <Text centred size='mini'>
              <RNImage
                style={{ width: WP308, height: WP308 }}
                source={require('../../assets/icons/icStarYellow.png')}
              />
              <Text size='mini' weight={500}>{`   ${isEmpty(user?.current_stage.stars) ? '0' : thousandSeparator(user?.current_stage.stars)} / ${thousandSeparator(first(user?.next_stage).stars)}`}</Text>
              <Text weight={300} size='mini'>{` stars (${user?.current_stage.type})`}</Text>
            </Text>
          )
        }
      </View>

      {
        /* Progress Bar Section */
        sizeNext > 0 && (
          <View style={{ width: '100%', marginBottom: WP205 }}>
            <View style={{ flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between', marginBottom: WP105 }}>
              <Text size='mini'>{user?.current_stage.type}</Text>
              <Text size='mini'>{first(user?.next_stage).type}</Text>
            </View>
            <View style={{ width: '100%' }}>
              <View style={{ width: '100%', height: WP7, borderRadius: WP7 / 2, backgroundColor: GREY_CHAT }} />
              <LinearGradient
                colors={styles.colors}
                start={styles.start}
                end={styles.end}
                style={{
                  width: `${generatePercent(user?.current_stage.stars, first(user?.next_stage).stars)}%`,
                  height: WP7,
                  borderRadius: WP7 / 2,
                  zIndex: 2,
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                }}
              />
            </View>
          </View>
        )
      }

      {/* Status Shield Section */}
      <View style={{ width: '100%', overflow: 'hidden', borderRadius: 12, flexDirection: 'row' }}>
        {
          sizeNext > 1 ? (
            map(user.next_stage, (item, index) => {
              const stageStyle = REWARDS_STYLE[upperCase(item.type)]
              const sizes = user.next_stage.length
              return (
                <View key={`${index}`} style={{ flex: 1, backgroundColor: stageStyle.background, height: WP15 }}>
                  <Image
                    aspectRatio={stageStyle.imageRatio}
                    style={{ position: 'absolute', left: 0, bottom: 0, }}
                    imageStyle={{ opacity: stageStyle.imageOpacity, height: WP135, }}
                    source={stageStyle.image}
                  />
                  <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                    paddingHorizontal: sizes == 2 ? WP3 : WP2
                  }}
                  >
                    <Text size='tiny' weight={500} style={{ textAlign: 'right' }}>{item.type}</Text>
                    <Text size='xtiny' style={{ textAlign: 'right' }}>{thousandSeparator(item.stars)} stars</Text>
                  </View>
                </View>
              )
            })
          ) : (
            <View style={{ flex: 1, backgroundColor: styles?.background, height: WP15 }}>
              <Image
                aspectRatio={styles?.imageRatio}
                style={{ position: 'absolute', left: 0, bottom: 0, }}
                imageStyle={{ opacity: sizeNext == 1 ? styles?.imageOpacity : 1, height: WP15, }}
                source={styles?.image}
              />
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center',
                paddingHorizontal: WP4
              }}
              >
                <Text size='tiny' weight={500} style={{ textAlign: 'right' }}>{sizeNext == 1 ? thousandSeparator(first(user?.next_stage).type) : thousandSeparator(user?.current_stage.type) }</Text>
                {sizeNext == 1 && (<Text size='tiny' style={{ marginLeft: WP1, textAlign: 'right' }}>{thousandSeparator(first(user?.next_stage).stars)} stars</Text>)}
              </View>
            </View>
          )
        }
      </View>
    </View>
  )
}

export default memo(GamificationSectionUser)
