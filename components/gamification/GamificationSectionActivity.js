import React, { memo } from 'react'
import { View, ScrollView, TouchableOpacity, Image as RNImage } from 'react-native'
import { isEmpty, split } from 'lodash-es'
import { WP100, WP15, WP3, WP301, WP6, WP7 } from '../../constants/Sizes'
import Text from '../Text'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { PUMPKIN, WHITE } from '../../constants/Colors'
import { thousandSeparator } from '../../utils/helper'

const GamificationSectionActivity = ({ navigateTo, acitvity, isLoading = false }) => {
  return (
    <View>
      <View style={{ paddingHorizontal: WP6 }}>
        <Text size='mini' type='NeoSans' weight={500}>Available Activity</Text>
      </View>
      {
        acitvity?.length > 0 ? (
          <ScrollView
            horizontal
            style={{ flexGrow: 0 }}
            contentContainerStyle={{ paddingHorizontal: WP6, paddingBottom: WP7, paddingTop: WP3 }}
            showsHorizontalScrollIndicator={false}
          >
            {
              acitvity.map((item, index) => {
                const widthItem = ((WP100 - WP15) / 2)
                const heightItem = widthItem / 1.67
                const splittedLink = split(item.redirect, '/')
                const action = () => { navigateTo(`${splittedLink[0]}`, !isEmpty(splittedLink[1]) ? { category: splittedLink[1] } : {}) }
                return (
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    onPress={action}
                    key={index}
                    style={{
                      width: widthItem,
                      height: heightItem,
                      backgroundColor: WHITE,
                      borderRadius: 12,
                      padding: WP3,
                      marginRight: index == acitvity.length - 1 ? 0 : WP3,
                      marginLeft: 1,
                      justifyContent: 'space-between',
                      ...SHADOW_STYLE['shadowThin']
                    }}
                  >
                    <Text numberOfLines={2} weight={400} size='xmini'>{item.label}</Text>
                    <Text color={PUMPKIN} size='tiny' weight={500}>
                      <RNImage
                        style={{ width: WP301, height: WP301 }}
                        source={require('../../assets/icons/icStarYellow.png')}
                      />
                      {`  Earn ${thousandSeparator(item.stars)} stars`}
                    </Text>
                  </TouchableOpacity>
                )
              })
            }
          </ScrollView>
        ) : (
          <View style={{ paddingHorizontal: WP6 }}>
            <Text size='small'>Sorry, there are no available Activity</Text>
          </View>
        )
      }
    </View>
  )
}

export default memo(GamificationSectionActivity)
