import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { Image, Text } from '..'
import { PUMPKIN, SKELETON_COLOR, SKELETON_HIGHLIGHT, WHITE } from '../../constants/Colors'
import { WP100, WP12, WP2, WP3, WP308, WP6 } from '../../constants/Sizes'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { postLogMenuReward } from '../../actions/api'

const ExploreRewardButton = ({ loading, navigateTo, idUser }) => {
  return (
    <View style={{ width: '100%', paddingHorizontal: WP6, marginBottom: WP6 }}>
      {
        loading ? (
          <SkeletonContent
            containerStyle={{ flex: 1 }}
            layout={[
              {
                width: WP100-WP12,
                borderRadius: 12,
                height: WP12
              }
            ]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          />
        ) : (
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            style={{
              backgroundColor: PUMPKIN,
              width: '100%',
              height: WP12,
              borderRadius: 12,
              paddingVertical: WP2,
              paddingHorizontal: WP6,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              ...SHADOW_STYLE['shadowThin'],
            }}
            onPress={() => {
              Promise.resolve(navigateTo('MyRewardScreen')).then(() => {
                postLogMenuReward({ id_user: idUser })
              })
            }}
          >
            <Image
              size={WP308}
              aspectRatio={1}
              source={require('../../assets/icons/icStarWhite.png')}
              style={{ marginRight: WP3 }}
            />
            <Text line={1} type='NeoSans' size='mini' weight={500} color={WHITE}>Get Rewards Today!</Text>
          </TouchableOpacity>
        )
      }
    </View>
  )
}

export default ExploreRewardButton
