import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, ScrollView } from 'react-native'
import { includes, remove, noop, reduce, isEmpty, find } from 'lodash-es'
import RNModal from 'react-native-modal'
import {
  WP2,
  WP4,
  WP100,
  HP100,
  WP105,
  WP6,
  WP3,
  WP05,
  WP1
} from 'sf-constants/Sizes'
import {
  WHITE,
  ORANGE_BRIGHT,
  RED_GOOGLE,
  GREY_CALM_SEMI,
  TOMATO
} from 'sf-constants/Colors'
import { getListTopics } from 'sf-actions/api'
import { BORDER_STYLE, TOUCH_OPACITY } from 'sf-constants/Styles'
import Text from './Text'
import Container from './Container'
import Button from './Button'
import HeaderNormal from './HeaderNormal'
import Image from './Image'

const propsType = {
  onChange: PropTypes.func,
  isError: PropTypes.bool
}

const propsDefault = {
  onChange: (value, replace) => {
  },
  isError: false
}

const MAX_TOPIC = 2

class SelectTopic extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selected: [],
      suggestions: [],
      isModalVisible: false,
      isLoading: true,
      isReady: false
    }
  }

  componentDidMount = () => {
    this._onFetchSuggestion()
  }

  _selectItem(item) {
    const {
      selected
    } = this.state
    if (includes(selected, item)) {
      remove(selected, (data) => data === item)
    } else {
      if (this.state.selected.length < MAX_TOPIC) selected.push(item)
    }
    this.setState({ selected })
  }

  _onFetchSuggestion = async () => {
    const { topicIds, dispatch } = this.props
    const responseTopics = await dispatch(getListTopics, { show: true }, noop, true, true)
    const suggestions = responseTopics.result || []
    const selected = isEmpty(topicIds) ? [] : reduce(topicIds, (acc, topicId) => {
      acc.push(find(suggestions, ['id_topic', topicId.toString()]))
      return acc
    }, [])
    this.setState({
      suggestions,
      showSuggestion: true,
      selected,
      isLoading: false,
      isReady: true
    })
    this._onChangeValue(selected, true)
  }

  _onChangeValue = (value, replace = false) => {
    const {
      onChange
    } = this.props
    if (replace) this.setState({ isEditing: false, selected: value })
    onChange(value, replace)
  }

  _toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible })
  }

  _triggerComponent() {
    if (this.props.isError) {
      return (<View>
        <Text size='small' style={{ marginHorizontal: WP2 }} color={RED_GOOGLE}>Please select at least one topic for
          your post </Text>
        <View style={{
          elevation: 2, backgroundColor: WHITE,
          borderWidth: 1, borderColor: RED_GOOGLE,
          paddingHorizontal: WP2,
          paddingVertical: WP2,
          bottom: 0,
          width: WP100, alignItems: 'center'
        }}
        >
          <Button
            soundfren
            rounded
            marginless
            centered
            textCentered
            iconName='plus'
            iconSize='large'
            text='Select Topic'
            textSize='mini'
            textWeight={500}
            style={{ alignSelf: 'center' }}
            contentStyle={{ paddingHorizontal: WP4, paddingVertical: WP105 }}
            onPress={() => this._toggleModal()}
          />
        </View>
      </View>)
    } else {
      return this.state.selected.length < MAX_TOPIC ? (
        <View style={{
          elevation: 2, backgroundColor: WHITE,
          ...BORDER_STYLE['top'],
          paddingHorizontal: WP2,
          paddingVertical: WP4,
          bottom: 0,
          width: WP100, alignItems: 'center'
        }}
        >
          <Button
            soundfren
            rounded
            marginless
            centered
            textCentered
            iconName='plus'
            iconSize='large'
            text='Select Topic'
            textSize='mini'
            textWeight={500}
            style={{ alignSelf: 'center' }}
            contentStyle={{ paddingHorizontal: WP4, paddingVertical: WP105 }}
            onPress={() => this._toggleModal()}
          />
        </View>) : ''
    }
  }

  _suggestionList(suggestions) {
    const {
      selected
    } = this.state

    return (
      <ScrollView
        bounces={false}
        contentContainerStyle={{ paddingHorizontal: WP6, paddingTop: WP6 }}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps='handled'
      >
        <Text size='tiny' weight={500}>
          Select topic about you post
          <Text size='tiny' color={TOMATO}>{` (${MAX_TOPIC-selected.length} remaining)`}</Text>
        </Text>
        {
          (suggestions || []).map((item, index) => (
            this._suggestionItem(index, item)
          ))
        }
      </ScrollView>
    )
  }

  _suggestionItem = (index, item) => {
    const {
      selected
    } = this.state
    const isSelected = includes(selected, item)

    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY}
        key={index}
        style={{
          paddingHorizontal: WP3,
          marginTop: WP6
        }}
        onPress={() => {
          this._selectItem(item)
        }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
          <View style={{ paddingTop: WP05, marginRight: WP3 }}>
            <Image
              imageStyle={{ width: WP4, aspectRatio: 1 }}
              source={isSelected ? require('sf-assets/icons/icCheckboxActive.png') : require('sf-assets/icons/icCheckboxInactive.png')}
            />
          </View>
          <View>
            <Text size='mini' type='NeoSans' color={isSelected ? TOMATO : null} weight={500}>{item.topic_name}</Text>
            <Text size='xtiny'>{item.description}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const {
      isLoading,
      suggestions,
      showSuggestion,
      selected
    } = this.state
    const isRemaining = selected.length < MAX_TOPIC
    return (
      <View>
        <TouchableOpacity
          disabled={!isRemaining}
          activeOpacity={TOUCH_OPACITY}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: WP3,
            paddingVertical: WP2,
            borderRadius: 8,
            borderWidth: 2,
            borderColor: isRemaining ? TOMATO : GREY_CALM_SEMI,
            backgroundColor: isRemaining ? TOMATO : WHITE
          }}
          onPress={() => this._toggleModal()}
        >
          <Text size='mini' type='NeoSans' weight={500} color={isRemaining ? WHITE : GREY_CALM_SEMI}>Select Topic</Text>
          <Image
            style={{ marginLeft: WP2 }}
            imageStyle={{ width: WP4, aspectRatio: 1 }}
            source={isRemaining ? require('sf-assets/icons/icPlusCircleActive.png') : require('sf-assets/icons/icPlusCircleInactive.png')}
          />
        </TouchableOpacity>

        <RNModal
          isVisible={this.state.isModalVisible}
          useNativeDriver={true}
          transparent={false}
          style={{
            margin: 0,
            justifyContent: 'flex-start'
          }}
          onBackdropPress={(() => {
          })}
          onBackButtonPress={() => {
            this._onChangeValue(selected, true)
            this._toggleModal()
          }}
          animationIn='slideInUp'
          animationOut='slideOutDown'
        >
          <View style={{
            height: HP100, width: WP100,
            backgroundColor: WHITE
          }}
          >
            <Container
              isLoading={isLoading}
              renderHeader={() => (
                <HeaderNormal
                  iconLeftOnPress={() => {
                    this._onChangeValue(selected, true)
                    this._toggleModal()
                  }}
                  iconLeftSize='small'
                  text='Select Topic'
                  rightComponent={(
                    <Button
                      onPress={() => {
                        this._onChangeValue(selected, true)
                        this._toggleModal()
                      }}
                      colors={[ORANGE_BRIGHT, ORANGE_BRIGHT]}
                      compact='center'
                      style={{ alignItems: 'flex-end' }}
                      contentStyle={{ paddingHorizontal: WP3, paddingVertical: WP1 }}
                      marginless
                      toggle
                      toggleActive={false}
                      disable={!(selected.length > 0)}
                      toggleInactiveTextColor={selected.length > 0 ? ORANGE_BRIGHT : GREY_CALM_SEMI}
                      shadow='none'
                      text='Done'
                      textType='NeoSans'
                      textSize='tiny'
                      textWeight={500}
                      radius={6}
                    />
                  )}
                />
              )}
            >
              <Image
                source={require('sf-assets/images/illTopics.png')}
                style={{ position: 'absolute', bottom: 0, left: 0, right: 0 }}
                imageStyle={{ width: '100%', height: undefined, aspectRatio: 320/184.7, }}
              />
              {
                showSuggestion && (
                  <View style={{ flex: 1 }}>
                    {
                      this._suggestionList(suggestions)
                    }
                  </View>
                )
              }
            </Container>
          </View>
        </RNModal>
      </View>
    )
  }
}

SelectTopic.propTypes = propsType

SelectTopic.defaultProps = propsDefault

export default SelectTopic
