import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { keys } from 'lodash-es'
import { WHITE, SILVER_CALM } from '../constants/Colors'
import { WP105 } from '../constants/Sizes'
import { SHADOW_STYLE } from '../constants/Styles'

const propsType = {
  key: PropTypes.any,
  top: PropTypes.number,
  left: PropTypes.number,
  width: PropTypes.number,
  style: PropTypes.object,
  containerStyle: PropTypes.object,
  radius: PropTypes.number,
  shadow: PropTypes.oneOf(keys(SHADOW_STYLE))
}

const propsDefault = {
  top: 0,
  left: 0,
  style: {},
  containerStyle: {},
  shadow: 'shadowThin'
}

const Dropdown = (props) => {
  const {
    left,
    width,
    style,
    containerStyle,
    shadow,
    radius,
    children
  } = props
  return (
    <View
      style={
        [
          {
            left, width,
            position: 'relative',
            zIndex: 999
          },
          containerStyle
        ]
      }
    >
      <View
        style={
          [
            SHADOW_STYLE[shadow],
            {
              backgroundColor: WHITE,
              borderColor: SILVER_CALM,
              borderRadius: radius || WP105
            },
            style
          ]
        }
      >
        {children}
      </View>
    </View>
  )
}

Dropdown.propTypes = propsType

Dropdown.defaultProps = propsDefault

export default Dropdown
