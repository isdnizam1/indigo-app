import React from 'react'
import HTMLElement from 'react-native-render-html'
import Hyperlink from 'react-native-hyperlink'
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils'
import { noop } from 'lodash-es'
import { HTML_ENTITY_TYPES } from '../constants/Html'
import {
  getUserIdFromLink,
  getYoutbeId,
  NavigateToInternalBrowser,
} from '../utils/helper'
import {
  CLEAR_BLUE,
  ORANGE_BRIGHT,
  SHIP_GREY,
  WHITE,
} from '../constants/Colors'
import { WP405 } from '../constants/Sizes'
import NavigationService from '../navigation/_serviceNavigation'
import Text from './Text'

export const fromHtmlEntities = (htmlAttribs, children) => {
  const { type, label, href } = htmlAttribs
  const data = children[0][0].props.children[0].props.children
  switch (type) {
    case HTML_ENTITY_TYPES.MENTION:
      return (
        <Text
          onPress={() =>
            NavigationService.navigate('ProfileScreen', {
              idUser: getUserIdFromLink(href),
            })
          }
          color={WHITE}
          style={{ backgroundColor: ORANGE_BRIGHT }}
        >
          {label}
        </Text>
      )
    case HTML_ENTITY_TYPES.LINK:
      return (
        <Text
          onPress={() => {
            NavigateToInternalBrowser({
              url: href,
            })
          }}
          type='Circular'
          size='mini'
          weight={300}
          color={CLEAR_BLUE}
        >
          {label}
        </Text>
      )
    default:
      return (
        <Text type='Circular' size='mini' weight={300} color={SHIP_GREY}>
          {data}
        </Text>
      )
  }
}

export const renderRichHtml = (textHtml) => (
  <HTMLElement
    textSelectable
    containerStyle={{ flexDirection: 'row' }}
    html={textHtml}
    customWrapper={(children) => <Text>{children}</Text>}
    baseFontStyle={{
      fontSize: WP405,
      fontFamily: 'Circular',
      color: SHIP_GREY,
    }}
    ignoredTags={[...IGNORED_TAGS, 'mention']}
    renderers={{
      // eslint-disable-next-line react/display-name
      span: (htmlAttribs, children, convertedCSSStyles, passProps) => {
        return <Text>{children}</Text>
      },
      a: (htmlAttribs, children, convertedCSSStyles, passProps) => {
        return fromHtmlEntities(htmlAttribs, children, passProps)
      },
    }}
  />
)

export const renderHyperLink = (element, params = {}) => {
  const { navigateTo = noop } = params
  return (
    <Hyperlink
      onPress={(url) => {
        const youtubeId = getYoutbeId(url)
        if (youtubeId) navigateTo('YoutubePreviewScreen', { youtubeId })
        else NavigateToInternalBrowser({ url })
      }}
      linkStyle={{ color: CLEAR_BLUE }}
    >
      {element}
    </Hyperlink>
  )
}
