import React from 'react'
import { getLinkPreview } from 'link-preview-js'
import PropTypes from 'prop-types'
import {
  Platform,
  TouchableOpacity,
  View,
  ViewPropTypes,
} from 'react-native'
import LinkifyIt from 'linkify-it'
import { get } from 'lodash-es'
import { BORDER_WIDTH } from '../constants/Styles'
import { WP105, WP25, WP3 } from '../constants/Sizes'
import { GUN_METAL, PALE_BLUE, SHIP_GREY } from '../constants/Colors'
import { getYoutubeId } from '../utils/helper'
import Text from './Text'
import Image from './Image'

// eslint-disable-next-line no-useless-escape
// const REGEX = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/g

export default class LinkPreviewCard extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      isUri: false,
      linkTitle: undefined,
      linkDesc: undefined,
      linkFavicon: undefined,
      linkImg: undefined,
    }
    this.getPreview(props.text)
  }

  getUrl = (text) => {
    const Linkify = new LinkifyIt()
    const urls = Linkify.match(text)
    const url = get(urls, '[0].url', '')
    return url
  };

  getPreview = (text) => {
    const url = this.getUrl(text)
    getLinkPreview(url, {
      headers: {
        'user-agent': 'googlebot',
        'Accept-Language': 'id-ID',
      },
    })
      .then((data) => {
        this.setState({
          isUri: true,
          linkTitle: data.title ? data.title : undefined,
          linkDesc: data.description ? data.description : undefined,
          linkImg:
            data.images && data.images.length > 0
              ? data.images[0]
              : undefined,
          linkFavicon:
            data.favicons && data.favicons.length > 0
              ? data.favicons[data.favicons.length - 1]
              : undefined,
        })
      })
      .catch(() => {
        this.setState({ isUri: false })
      })
  };

  _onLinkPressed() {
    const { text, navigateTo } = this.props
    const url = this.getUrl(text)
    const youtubeId = getYoutubeId(url)
    if (!youtubeId) navigateTo('BrowserScreenNoTab', { url })
    else navigateTo('YoutubePreviewScreen', { youtubeId })
  }

  renderImage = (
    imageLink,
    faviconLink,
    imageStyle,
    faviconStyle,
    imageProps,
  ) => {
    return imageLink ? (
      <Image
        imageStyle={imageStyle}
        source={{ uri: imageLink }}
        {...imageProps}
      />
    ) : faviconLink ? (
      <Image
        imageStyle={faviconStyle}
        source={{ uri: faviconLink }}
        {...imageProps}
      />
    ) : null
  };
  renderText = (
    showTitle,
    title,
    description,
    textContainerStyle,
    titleStyle,
    descriptionStyle,
    titleNumberOfLines,
    descriptionNumberOfLines,
  ) => {
    return (
      <View style={textContainerStyle}>
        {showTitle && (
          <Text
            numberOfLines={titleNumberOfLines}
            size='mini'
            style={titleStyle}
          >
            {title}
          </Text>
        )}
        {description && (
          <Text
            size='tiny'
            numberOfLines={descriptionNumberOfLines}
            style={descriptionStyle}
          >
            {description}
          </Text>
        )}
      </View>
    )
  };

  renderLinkPreview = ({
    text,
    containerStyle,
    imageLink,
    faviconLink,
    imageStyle,
    faviconStyle,
    showTitle,
    title,
    description,
    textContentStyle,
  }) => {
    return (
      <TouchableOpacity
        style={[styles.containerStyle, containerStyle]}
        activeOpacity={0.9}
        onPress={this._onLinkPressed.bind(this)}
      >
        <Image
          centered
          source={{ uri: imageLink || faviconLink }}
          style={{
            borderWidth: BORDER_WIDTH,
            borderColor: PALE_BLUE,
            borderRadius: 6,
            flexGrow: 0,
          }}
          imageStyle={{
            height: undefined,
            width: WP25,
            aspectRatio: 1,
            borderRadius: 6,
          }}
        />
        <View
          style={[
            { paddingHorizontal: WP3, flexGrow: 1, flex: 1 },
            textContentStyle,
          ]}
        >
          <Text
            numberOfLines={1}
            ellipsizeMode='tail'
            type='Circular'
            size='mini'
            weight={500}
            color={GUN_METAL}
            style={{ marginVertical: WP105 }}
          >
            {title}
          </Text>
          <Text
            numberOfLines={2}
            ellipsizeMode='tail'
            size='xmini'
            weight={300}
            color={SHIP_GREY}
          >
            {description}
          </Text>
        </View>
      </TouchableOpacity>
    )
  };

  render() {
    const {
      text,
      containerStyle,
      imageStyle,
      faviconStyle,
      textContainerStyle,
      title,
      titleStyle,
      titleNumberOfLines,
      descriptionStyle,
      descriptionNumberOfLines,
      imageProps,
      textContentStyle,
      imageOnly,
    } = this.props
    return this.state.isUri
      ? imageOnly
        ? this.renderImage(
            this.state.linkImg,
            this.state.linkFavicon,
            imageStyle,
            faviconStyle,
            imageProps,
          )
        : this.renderLinkPreview({
            text,
            containerStyle,
            imageLink: this.state.linkImg,
            faviconLink: this.state.linkFavicon,
            imageStyle,
            faviconStyle,
            showTitle: title,
            title: this.state.linkTitle,
            description: this.state.linkDesc,
            textContainerStyle,
            titleStyle,
            descriptionStyle,
            titleNumberOfLines,
            descriptionNumberOfLines,
            imageProps,
            textContentStyle,
          })
      : null
  }
}

const styles = {
  containerStyle: {
    flexDirection: 'row',
  },
}

LinkPreviewCard.defaultProps = {
  text: null,
  containerStyle: {
    backgroundColor: 'rgba(239, 239, 244,0.62)',
    alignItems: 'center',
  },
  imageStyle: {},
  faviconStyle: {
    width: 40,
    height: 40,
    paddingRight: 10,
    paddingLeft: 10,
  },
  textContainerStyle: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    padding: 10,
  },
  title: true,
  titleStyle: {},
  titleNumberOfLines: 2,
  descriptionStyle: {},
  descriptionNumberOfLines: Platform.isPad ? 4 : 3,
  imageProps: { resizeMode: 'contain' },
  imageOnly: false,
}

LinkPreviewCard.propTypes = {
  text: PropTypes.string,
  containerStyle: ViewPropTypes.style,
  imageStyle: ViewPropTypes.style,
  faviconStyle: ViewPropTypes.style,
  textContainerStyle: ViewPropTypes.style,
  title: PropTypes.bool,
  titleStyle: PropTypes.any,
  titleNumberOfLines: PropTypes.any,
  descriptionStyle: PropTypes.any,
  descriptionNumberOfLines: PropTypes.any,
  imageOnly: PropTypes.bool,
}
