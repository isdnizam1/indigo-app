import React from 'react'
import PropTypes from 'prop-types'
import Highlighter from 'react-native-highlight-words'
import { Text as RNText } from 'react-native'
import { keys, isString } from 'lodash-es'
import { FONT_SIZE } from '../constants/Sizes'
import { GREY } from '../constants/Colors'
import { FONTS } from '../constants/Fonts'

const propsType = {
  key: PropTypes.any,
  size: PropTypes.oneOf(keys(FONT_SIZE)),
  color: PropTypes.string,
  type: PropTypes.oneOf(keys(FONTS)),
  weight: PropTypes.number,
  lineHeight: PropTypes.number,
  centered: PropTypes.bool,
  highlight: PropTypes.bool,
  onPress: PropTypes.func,
  style: PropTypes.any,
  searchWords: PropTypes.array,
}

const propsDefault = {
  key: undefined,
  size: 'small',
  color: GREY,
  type: 'Circular',
  weight: 300,
  centered: false,
  onPress: null,
  lineHeight: null,
  searchWords: [],
}

class Text extends React.PureComponent {
  render() {
    const {
      size,
      color,
      type,
      weight,
      centered,
      style,
      searchWords,
      children,
      fontFamily,
      lineHeight,
    } = this.props
    const currentFontFamily =
      fontFamily ||
      FONTS[type][weight] ||
      FONTS[this.propsDefault.type][this.propsDefault.weight]
    // console.log({ currentFontFamily })
    return isString(children) ? (
      <Highlighter
        {...this.props}
        searchWords={searchWords || []}
        maxFontSizeMultiplier={1}
        textToHighlight={children || ''}
        highlightStyle={{ fontFamily: FONTS[type][500] }}
        style={[
          {
            textAlign: centered ? 'center' : 'left',
            color,
            fontSize: FONT_SIZE[size],
            fontFamily: currentFontFamily,
            lineHeight,
          },
          style,
        ]}
      />
    ) : (
      <RNText
        {...this.props}
        maxFontSizeMultiplier={1}
        style={[
          {
            textAlign: centered ? 'center' : 'left',
            color,
            fontSize: FONT_SIZE[size] || FONT_SIZE[this.propsDefault.size],
            fontFamily: currentFontFamily,
            lineHeight,
          },
          style,
        ]}
      >
        {children}
      </RNText>
    )
  }
}

Text.propTypes = propsType

Text.defaultProps = propsDefault

export default React.memo(Text)
