import React from "react";
import PropTypes from "prop-types";
import { REDDISH, REDDISH_LIGHT, WHITE } from "../../constants/Colors";
import { WP105, WP2, WP20 } from "../../constants/Sizes";
import { Button } from "../index";

const propsType = {
  idUser: PropTypes.any,
  onPress: PropTypes.func,
  isMessage: PropTypes.bool,
  fullName: PropTypes.string,
  profilePicture: PropTypes.string,
  idGroupMessage: PropTypes.string,
};

const propsDefault = {
  isMessage: false,
};

class FollowButton extends React.PureComponent {
  state = {
    followed: this.props.followed,
  };

  render() {
    const {
      onPress,
      idUser,
      isMessage,
      fullName,
      profilePicture,
      idGroupMessage,
    } = this.props;
    const { followed } = this.state;
    return (
      <Button
        onPress={async () => {
          await this.setState((state) => ({
            followed: isMessage ? true : !state.followed,
          }));
          await onPress(
            !followed,
            idUser,
            false,
            fullName,
            profilePicture,
            idGroupMessage
          );
        }}
        shadow="none"
        text={followed ? (isMessage ? "Send Messages" : "Following") : "Follow"}
        textColor={followed ? WHITE : WHITE}
        textType={"Circular"}
        textWeight={400}
        backgroundColor={followed ? REDDISH : REDDISH}
        style={{ marginVertical: 0, padding: 0 }}
        radius={6}
        textSize="xmini"
        centered
        contentStyle={{
          minWidth: WP20,
          paddingVertical: WP105,
          paddingHorizontal: WP2,
          borderWidth: 1,
          borderColor: REDDISH,
        }}
      />
    );
  }
}

FollowButton.propTypes = propsType;
FollowButton.defaultProps = propsDefault;
export default FollowButton;
