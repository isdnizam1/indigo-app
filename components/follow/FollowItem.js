import React from "react";
import { isEmpty, isFunction, join } from "lodash-es";
import { TouchableOpacity, View } from "react-native";
import { GUN_METAL, PALE_GREY, SHIP_GREY_CALM } from "../../constants/Colors";
import { TOUCH_OPACITY } from "../../constants/Styles";
import { WP05, WP2, WP3, WP4, WP6 } from "../../constants/Sizes";
import Avatar from "../Avatar";
import Text from "../Text";
import TextName from "../TextName";
import FollowButton from "./FollowButton";

const propsType = {};

const propsDefault = {};

const FollowItem = (props) => {
  const {
    type,
    user,
    onFollow,
    onPressItem,
    idUser,
    isMessage,
    imageSize = "small",
    style,
    label,
    isMine = false,
    onCollab,
  } = props;
  const profession = [];
  if (!isEmpty(user?.job_title)) profession.push(user.job_title);
  // if (!isEmpty(user?.company_name)) profession.push(user.company_name)
  const subtitle =
    user.account_type == "premium"
      ? join(user?.interest, ", ") || user?.interest_name || user?.job_title
      : join(profession, ", ") || join(user?.interest, ", ");
  return (
    <TouchableOpacity
      activeOpacity={TOUCH_OPACITY}
      onPress={onPressItem}
      disabled={!isFunction(onPressItem)}
      style={{
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: WP4,
        marginHorizontal: WP4,
        borderBottomColor: PALE_GREY,
        borderBottomWidth: 1,
        flex: 1,
        ...style,
      }}
    >
      <View style={{ marginRight: WP3 }}>
        <Avatar
          onPress={onPressItem}
          image={user.profile_picture || user.avatar}
          verifiedStatus={user.verified_status}
          size={imageSize}
        />
      </View>
      <View style={{ flex: 1, paddingRight: WP2 }}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          <View>
            {isMine ? (
              <Text
                centered={false}
                type="Circular"
                size="mini"
                weight={500}
                color={GUN_METAL}
                user={user}
                style={{ lineHeight: WP6, marginBottom: WP05 }}
              >
                You
              </Text>
            ) : (
              <TextName
                centered={false}
                type="Circular"
                size="mini"
                weight={500}
                color={GUN_METAL}
                user={user}
                style={{ lineHeight: WP6, marginBottom: WP05 }}
              />
            )}
          </View>
          {label}
        </View>
        <Text
          numberOfLines={1}
          color={SHIP_GREY_CALM}
          type={"Circular"}
          size="xmini"
          weight={300}
        >
          {subtitle}
        </Text>
        {onCollab && (
          <Text
            numberOfLines={1}
            color={SHIP_GREY_CALM}
            type={"Circular"}
            size="xmini"
            weight={300}
            style={{ lineHeight: WP6 }}
          >
            {user?.city_name}
          </Text>
        )}
      </View>
      {onFollow && (
        <FollowButton
          followed={
            Number(user.followed_by_viewer) ||
            user.is_followed_by_viewer ||
            user.status === "followed" ||
            user.followed_by
          }
          idUser={
            idUser
              ? idUser
              : type === "followers"
              ? user.followed_by
              : user.id_user || user.id
          }
          onPress={onFollow}
          isMessage={isMessage}
          fullName={user.name || user.full_name}
          profilePicture={user.avatar || user.profile_picture}
          idGroupMessage={user.id_groupmessage}
        />
      )}
    </TouchableOpacity>
  );
};

FollowItem.propTypes = propsType;
FollowItem.defaultProps = propsDefault;
export default FollowItem;
