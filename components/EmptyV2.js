import React from 'react'
import PropTypes from 'prop-types'
import { View, ImageBackground } from 'react-native'
import * as Updates from 'expo-updates'
import { WP65, WP100 } from '../constants/Sizes'
import { HP2 } from '../constants/Sizes'
import Text from './Text'
import Button from './Button'

const propsType = {
  image: PropTypes.any,
  icon: PropTypes.any,
  imageHeight: PropTypes.number,
  imageWidth: PropTypes.number,
  title: PropTypes.string,
  full: PropTypes.bool,
  message: PropTypes.any.isRequired,
  actions: PropTypes.any,
  style: PropTypes.object,
  textContainerStyle: PropTypes.object,
  textType: PropTypes.string,
  textStyle: PropTypes.object,
  aspectRatio: PropTypes.number
}

const propsDefault = {
  image: require('../assets/images/ill1.jpg'),
  message: 'This is default message',
  textType: 'NeoSans',
  imageWidth: WP100,
  full: true,
  aspectRatio: 213 / 175
}

const EmptyV2 = (props) => {
  const {
    image: propImage,
    full,
    title: propTitle,
    message: propMessage,
    actions: propActions,
    style,
    imageWidth,
    textContainerStyle,
    textType,
    textStyle,
    aspectRatio
  } = props
  let title = propTitle
  let message = propMessage
  let actions = propActions
  let image = propImage
  if (props.error) {
    title = 'Sorry'
    message = 'We do apologize, please give us a time to fix this issue'
    image = require('../assets/images/illNotConnected.png')
    actions = (<Button
      onPress={() => Updates.reloadAsync()}
      rounded
      centered
      soundfren
      width={WP65}
      text='Reload'
      textWeight={300}
      shadow='none'
               />)
  }
  return (
    <ImageBackground
      source={image}
      aspectRatio={aspectRatio}
      style={[
        {
          width: imageWidth,
          aspectRatio,
          paddingVertical: full ? 0 : HP2,
          alignItems: 'center'
        },
        style
      ]}
    >
      <View style={[{ paddingVertical: HP2, alignItems: 'flex-start', flex: 1 }, textContainerStyle]}>
        <Text size='mini' type={textType} style={textStyle}>{message}</Text>
        <Text size='mini' type={textType} style={textStyle} weight={500}>{title}</Text>
      </View>
      {
        React.isValidElement(actions) && actions
      }
    </ImageBackground>
  )
}

EmptyV2.propTypes = propsType
EmptyV2.defaultProps = propsDefault
export default EmptyV2