import React from 'react'
import PropTypes from 'prop-types'
import { keys } from 'lodash-es'
import { View, Modal as RNModal, TouchableOpacity } from 'react-native'
import noop from 'lodash-es/noop'
import { MODAL_STYLE } from '../constants/Styles'
import { WHITE } from '../constants/Colors'
import { HP1 } from '../constants/Sizes'

const propsType = {
  key: PropTypes.any,
  position: PropTypes.oneOf(keys(MODAL_STYLE)),
  backgroundColor: PropTypes.string,
  isVisible: PropTypes.bool,
  closeOnBackdrop: PropTypes.bool,
  unstyled: PropTypes.bool,
  renderModalContent: PropTypes.func,
  style: PropTypes.object,
  modalStyle: PropTypes.object,
  closeBackdropCallback: PropTypes.func,
  onClose: PropTypes.func,
}

const propsDefault = {
  position: 'bottom',
  backgroundColor: WHITE,
  isVisible: false,
  style: {},
  closeOnBackdrop: false,
  modalStyle: {},
  animationType: 'slide',
  unstyled: false,
  closeBackdropCallback: noop,
  onClose: noop
}

class ModalV2 extends React.PureComponent {
  state = {
    isVisible: this.props.isVisible,
    payload: {}
  }

  _toggleModal = async (payload) => {
    if (this.state.isVisible) this.props.onClose()
    await this.setState((state) => ({ isVisible: !state.isVisible, payload }))
  }

  render() {
    const {
      position,
      backgroundColor,
      renderModalContent,
      children,
      style,
      modalStyle,
      closeOnBackdrop,
      unstyled,
      animationType,
      closeBackdropCallback
    } = this.props
    const {
      isVisible,
      payload
    } = this.state

    return children({ toggleModal: this._toggleModal, isVisible }, (
      <RNModal
        transparent
        onRequestClose={closeOnBackdrop ? closeBackdropCallback : this._toggleModal}
        animationType={animationType}
        visible={isVisible}
      >
        <TouchableOpacity
          activeOpacity={1}
          onPress={closeOnBackdrop ? closeBackdropCallback : this._toggleModal}
          disabled={closeOnBackdrop}
          style={[
            MODAL_STYLE[position],
            { flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.75)' },
            modalStyle
          ]}
        >
          <View
            style={[
              unstyled ? { width: '100%', height: '100%' } : {
                backgroundColor,
                borderTopRightRadius: HP1,
                borderTopLeftRadius: HP1
              },
              style
            ]}
          >
            {renderModalContent({ toggleModal: this._toggleModal, payload: payload ? payload : {}, isVisible })}
          </View>
        </TouchableOpacity>
      </RNModal>
    ))
  }
}

ModalV2.propTypes = propsType

ModalV2.defaultProps = propsDefault

export default ModalV2
