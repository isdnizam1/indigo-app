import React from 'react'
import PropTypes from 'prop-types'
import { Platform } from 'react-native'
import { LinearGradient as RNLinearGradient } from 'expo-linear-gradient'
import { PINK_RED, ORANGE_BRIGHT } from '../constants/Colors'
import { WP1, WP100 } from '../constants/Sizes'

const propsType = {
  style: PropTypes.any,
  colors: PropTypes.array,
  start: PropTypes.array,
  end: PropTypes.array,
  rounded: PropTypes.bool,
  radius: PropTypes.number
}

const propsDefault = {
  style: {},
  colors: [PINK_RED, ORANGE_BRIGHT],
  start: [0, 0],
  end: [1, 0],
  rounded: false,
  radius: undefined,
}

class LinearGradient extends React.Component {
  state = {
    borderRadius: 0,
  }
  render() {
    const {
      style,
      colors,
      start,
      end,
      children,
      radius,
      rounded
    } = this.props
    return (
      <RNLinearGradient
        onLayout={(event) => {
          const { height } = event.nativeEvent.layout
          this.setState({ borderRadius: height/2 })
        }}
        colors={colors}
        start={start} end={end}
        style={
          [
            {
              borderRadius: radius >= 0 ? radius : rounded ? Platform.select({ ios: this.state.borderRadius, android: WP100 }) : WP1,
            },
            style
          ]
        }
      >
        {children}
      </RNLinearGradient>
    )
  }
}

LinearGradient.propTypes = propsType

LinearGradient.defaultProps = propsDefault

export default LinearGradient
