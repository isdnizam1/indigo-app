import React from 'react'
import { View } from 'react-native'
import { PALE_WHITE } from '../../../constants/Colors'
import { WP3, WP4, WP44 } from '../../../constants/Sizes'
import Image from '../../Image'
import Text from '../../Text'

const imgItem = [
  {
    bgItem: require('../../../assets/images/bgTabLearnWebinar.png')
  },
  {
    bgItem: require('../../../assets/images/bgTabLearnVideo.png')
  },
  {
    bgItem: require('../../../assets/images/bgTabLearnPodcast.png')
  },
  {
    bgItem: require('../../../assets/images/bgTabLearnForum.png')
  },
  {
    bgItem: require('../../../assets/images/bgTabLearnKonsultasi.png')
  },
  {
    bgItem: require('../../../assets/images/bgTabLearnArtikel.png')
  },
]

const LearnTabItem = ({ title, subtitle, index }) => {
  return (
    <View style={{ flex: 1, paddingRight: WP4 }}>

        <Image
          source={imgItem[index].bgItem}
          imageStyle={{ height: (WP44 * 100) / 156, aspectRatio: 156 / 100, width: (WP44 * 100) / 156, borderRadius: 10 }}
        />
        <View
          style={{
            width: '100%',
            height: '100%',
            position: 'absolute',
            paddingVertical: WP3,
            paddingHorizontal: WP3,
          }}
        >
          <View style={{
            flex: 1, flexDirection: 'column', justifyContent: 'space-between',
          }}
          >
            <Text numberOfLines={2} type='Circular' size='medium' weight={700} color={PALE_WHITE} style={{ fontSize: 15 }}>{title}</Text>
            <Text numberOfLines={4} type='Circular' size='tiny' weight={400} color={PALE_WHITE} style={{ fontSize: 10 }}>{subtitle} <Text type='Circular' size='tiny' weight={700} color={PALE_WHITE} style={{ fontSize: 10 }}>1000 Startup Indonesia Timur</Text></Text>
          </View>
        </View>
    </View>
  )
}

export default LearnTabItem
