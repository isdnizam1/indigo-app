import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { ImageBackground } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { PALE_WHITE, TRANSPARENT, WHITE } from '../../../constants/Colors'
import {
  WP1,
  WP10,
  WP2,
  WP205,
  WP25,
  WP3,
  WP4,
  WP44,
} from '../../../constants/Sizes'
import Image from '../../Image'
import Text from '../../Text'
import { TOUCH_OPACITY } from '../../../constants/Styles'
import Spacer from '../../Spacer'

const GeneralTabItem = ({
  title,
  icon,
  bgItem,
  id,
  navigateTo,
  gradient = [TRANSPARENT, TRANSPARENT],
  toggleModalMaintenance,
}) => {
  const borderRadius = 8
  const [gradientStart, gradientEnd] = gradient
  return (
    <View>
      <TouchableOpacity
        style={{ paddingHorizontal: WP205 * 0.8 }}
        activeOpacity={TOUCH_OPACITY}
        onPress={() => {
          if (['rewards', 'terms', 'faq'].indexOf(id) >= 0)
            navigateTo('DetailGeneralContentScreen', { id })
          if (id == 'forum') {
            navigateTo('Forum1000StartupScreen')
          }
          if( id == 'mentor')
            toggleModalMaintenance()
          if( id === 'team' )
            navigateTo('TeamUpScreen')
        }}
      >
        <ImageBackground
          source={bgItem}
          style={{
            flex: 1,
            resizeMode: 'cover',
            justifyContent: 'center',
            elevation: 2,
            backgroundColor: WHITE,
            borderRadius,
          }}
          imageStyle={{ borderRadius }}
        >
          <LinearGradient
            colors={[gradientStart, gradientEnd]}
            start={[0, 1]}
            end={[0.8, 0.2]}
            locations={[0.25, 0.8]}
            style={{ borderRadius }}
          >
            <View style={{ paddingVertical: WP4 }}>
              <Image size={'xmini'} source={icon} />
              <Spacer size={WP1} />
              <Text size={'slight'} weight={700} color={WHITE} centered>
                {title}
              </Text>
            </View>
          </LinearGradient>
        </ImageBackground>
      </TouchableOpacity>
    </View>
  )
}

export default GeneralTabItem
