import React from 'react'
import { View } from 'react-native'
import {
  PALE_WHITE,
} from '../../../constants/Colors'
import { WP2, WP4, WP44 } from '../../../constants/Sizes'
import Image from '../../Image'
import Text from '../../Text'
import { Icon } from '../../index'

const MissionTabItem = ({ category, total, subtitle, img, index, total_mission, complete_mission }) => {

  return (
    <View style={{ flex: 1, paddingRight: WP4 }}>

        <Image
          source={img}
          imageStyle={{ height: (WP44 * 100) / 160, aspectRatio: 160 / 100, width: (WP44 * 100) / 160, borderRadius: 10 }}
        />
        <View
          style={{
            width: '100%',
            height: '100%',
            position: 'absolute',
            paddingVertical: WP2,
            paddingHorizontal: WP2
          }}
        >
          <View style={{
            flex: 1, flexDirection: 'column', justifyContent: 'space-between'
          }}
          >
            <Text numberOfLines={2} type='Circular' size='massive' weight={700} color={PALE_WHITE}>{complete_mission}/{total_mission}</Text>
            <Text numberOfLines={2} type='Circular' size='xmini' weight={700} color={PALE_WHITE}>{subtitle}</Text>
           <View style={{ flexDirection: 'row' }}>
             <Text numberOfLines={2} type='Circular' size='tiny' weight={400} color={PALE_WHITE}>Ikuti misi {category === 'daily' ? 'harian' : 'spesial' }</Text>
             <Icon
               size='large'
               name='keyboard-arrow-right'
               type='MaterialIcons'
               centered
               color={PALE_WHITE}
             />
           </View>
          </View>
        </View>
    </View>
  )
}

export default MissionTabItem
