import React, { isValidElement } from 'react'
import PropTypes from 'prop-types'
import { TextInput, View } from 'react-native'
import { keys } from 'lodash-es'
import noop from 'lodash-es/noop'
import { BLACK, GREY, GREY_DISABLE, GREY_PLACEHOLDER, GREY_WARM, NO_COLOR, RED_GOOGLE, SILVER, TOMATO, GUN_METAL, PALE_LIGHT_BLUE_TWO, SHIP_GREY } from '../constants/Colors'
import { FONT_SIZE, HP1, HP105, WP1, WP105, WP2, WP305, WP4 } from '../constants/Sizes'
import { BORDER_COLOR, BORDER_COLOR_ACTIVE, BORDER_WIDTH } from '../constants/Styles'
import { FONTS } from '../constants/Fonts'
import Text from './Text'
import Icon from './Icon'
import Image from './Image'
import RequiredMark from './RequiredMark'

const LINE_HEIGHT = 5

export const propsType = {
  color: PropTypes.string,
  key: PropTypes.any,
  rounded: PropTypes.bool,
  backgroundColor: PropTypes.string,
  label: PropTypes.string,
  labelv2: PropTypes.string,
  size: PropTypes.oneOf(keys(FONT_SIZE)),
  type: PropTypes.oneOf(keys(FONTS)),
  weight: PropTypes.number,
  radius: PropTypes.number,
  required: PropTypes.bool,
  bold: PropTypes.bool,
  multiline: PropTypes.bool,
  lineHeight: PropTypes.number,
  maxLine: PropTypes.number,
  numberOfLines: PropTypes.number,
  bordered: PropTypes.bool,
  boxed: PropTypes.bool,
  disabled: PropTypes.bool,
  value: PropTypes.string.isRequired,
  valueColor: PropTypes.string,
  icon: PropTypes.string,
  iconImage: PropTypes.any,
  returnKeyType: PropTypes.string,
  wording: PropTypes.string,
  onChangeText: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  secureTextEntry: PropTypes.bool,
  toggleSecureTextEntry: PropTypes.bool,
  style: PropTypes.object,
  editable: PropTypes.bool,
  textInputStyle: PropTypes.objectOf(PropTypes.any),
  keyboardType: PropTypes.string,
  labelWeight: PropTypes.number,
  labelColor: PropTypes.string,
  withLabel: PropTypes.bool,
  maxLength: PropTypes.number,
  textAlignVertical: PropTypes.string,
  textIcon: PropTypes.string,
  onRemoveAble: PropTypes.func,
  onRBlur: PropTypes.func,
  autoCapitalize: PropTypes.string,
  showLength: PropTypes.bool,
  error: PropTypes.string,
  labelSize: PropTypes.string,
  iconSize: PropTypes.string,
  iconRightCustom: PropTypes.any,
  containerStyle: PropTypes.object,
  innerRef: PropTypes.any,
  onSubmit: PropTypes.func,
  maxLengthFocus: PropTypes.bool,
  removeIcon: PropTypes.objectOf(PropTypes.any),
  subLabel: PropTypes.string
}

export const propsDefault = {
  style: {},
  bold: false,
  disabled: false,
  size: 'small',
  type: 'Circular',
  weight: 300,
  rounded: false,
  bordered: false,
  boxed: false,
  required: false,
  returnKeyType: 'done',
  backgroundColor: NO_COLOR,
  multiline: false,
  lineHeight: LINE_HEIGHT,
  value: '',
  valueColor: GREY,
  placeholder: undefined,
  placeholderTextColor: SILVER,
  secureTextEntry: false,
  toggleSecureTextEntry: false,
  editable: true,
  textInputStyle: {},
  keyboardType: 'default',
  labelWeight: 500,
  labelColor: GREY,
  withLabel: true,
  textAlignVertical: 'top',
  autoCapitalize: 'none',
  showLength: true,
  onBlur: noop,
  error: null,
  labelSize: 'mini',
  iconSize: 'mini',
  maxLengthFocus: false,
}

class InputTextLight extends React.Component {
  constructor(props) {
    super(props)
  }

  state = {
    value: this.props.value,
    isEditing: false,
    showValue: this.props.secureTextEntry
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.value !== this.props.value && this.state.value !== this.props.value) {
      this.setState({
        value: this.props.value
      })
    }
  }

  _onChangeText = (text) => {
    const { onChangeText } = this.props
    onChangeText(text)
  }

  _onEndEditing = () => {
    this.setState({ isEditing: false })
    this.props.onBlur()
  }

  _onFocus = () => {
    this.setState({ isEditing: true })
  }

  _onBlur = () => {
    this.setState({ isEditing: false })
    this.props.onBlur()
  }

  render() {
    const {
      color,
      disabled,
      size,
      type,
      weight,
      backgroundColor,
      bold,
      multiline,
      lineHeight,
      maxLine,
      numberOfLines,
      icon,
      iconImage,
      label,
      placeholder,
      placeholderTextColor,
      returnKeyType,
      bordered,
      valueColor,
      toggleSecureTextEntry,
      required,
      value,
      editable,
      style,
      textInputStyle,
      keyboardType,
      labelWeight,
      labelColor,
      labelSize,
      maxLength,
      wording,
      textIcon,
      textAlignVertical,
      onRemoveAble,
      autoCapitalize,
      showLength,
      error,
      autoFocus,
      iconSize,
      iconRightCustom,
      containerStyle,
      innerRef,
      onSubmit,
      maxLengthFocus,
      labelv2,
      subLabel,
      removeIcon
    } = this.props
    const {
      showValue,
      isEditing
    } = this.state
    const valueLength = value?.length
    return (
      <View
        style={[
          {
            marginTop: HP105,
            marginBottom: HP1,
            flexGrow: 1,
          },
          style
        ]}
      >
        {
          !!label && (
            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
              <Text size={labelSize || 'mini'} color={labelColor || BLACK} weight={bold ? 500 : labelWeight}>{label}</Text>
              {
                required && <RequiredMark />
              }
            </View>
          )
        }
        {
          !!labelv2 && (
            <Text weight={400} type='Circular' size={'mini'} color={SHIP_GREY} style={{ marginBottom: WP1 }}>
              {`${labelv2} `}
              {
                subLabel && (
                  <Text weight={200} type='Circular' size={'mini'} color={PALE_LIGHT_BLUE_TWO}>
                    {subLabel}
                  </Text>
                )
              }
            </Text>
          )
        }
        <View
          style={
            [
              {
                backgroundColor,
                padding: bordered ? WP2 : WP1,
                paddingHorizontal: bordered ? WP2 : 0,
                borderRadius: bordered ? 10 : 0,
                marginBottom: 0,
                borderWidth: bordered ? BORDER_WIDTH : 0,
                borderBottomWidth: !bordered ? BORDER_WIDTH : undefined,
                borderColor: color ? color : isEditing ? BORDER_COLOR_ACTIVE : BORDER_COLOR,
                flexDirection: 'row',
                alignItems: 'center'
              },
              textInputStyle
            ]
          }
        >
          {
            !!icon && (
              <Icon
                style={{ marginRight: WP305 }}
                centered
                name={icon} size='large'
              />
            )
          }
          {
            !!iconImage && (
              <Image
                size={iconSize || 'mini'}
                source={iconImage}
                style={{ marginRight: WP4 }}
              />
            )
          }
          {
            !!textIcon && (
              <Text
                centered
                style={{
                  fontSize: FONT_SIZE[size],
                  fontFamily: FONTS[type][valueLength > 0 ? weight : 200],
                }}
              >{textIcon}</Text>
            )
          }
          <TextInput
            ref={innerRef}
            onSubmitEditing={onSubmit}
            editable={editable}
            returnKeyType={returnKeyType}
            value={value}
            autoFocus={autoFocus}
            style={[
              {
                flex: 1,
                fontSize: FONT_SIZE[size],
                fontFamily: FONTS[type][valueLength > 0 ? weight : 200],
                color: disabled ? GREY_WARM : color || valueColor,
                padding: 0,
                paddingTop: 0,
                margin: 0,
                borderWidth: 0,
                minHeight: multiline ? (FONT_SIZE[size] * lineHeight) + (lineHeight == 1 ? 0 : ((bordered ? WP2 : WP1) * 2)) : undefined,
                maxHeight: multiline && maxLine ? (FONT_SIZE[size] * maxLine) + ((bordered ? WP2 : WP1) * 2) : undefined
              },
              containerStyle
            ]}
            underlineColorAndroid='transparent'
            onFocus={this._onFocus}
            onBlur={this._onBlur}
            onEndEditing={this._onEndEditing}
            secureTextEntry={showValue}
            onChangeText={this._onChangeText}
            placeholder={placeholder}
            placeholderTextColor={placeholderTextColor ? placeholderTextColor : SILVER}
            multiline={multiline}
            textAlignVertical={textAlignVertical}
            numberOfLines={numberOfLines ? numberOfLines : multiline ? lineHeight : 1}
            keyboardType={keyboardType}
            maxLength={maxLength}
            autoCapitalize={autoCapitalize}
          />
          {isValidElement(iconRightCustom) && (iconRightCustom)}
          {
            toggleSecureTextEntry && (
              <Icon
                style={{ marginLeft: WP305 }}
                centered
                color={color ? color : GREY}
                onPress={() => this.setState((state) => ({ showValue: !state.showValue }))}
                name={showValue ? 'eye' : 'eye-with-line'} type='Entypo' size='large'
              />
            )
          }
          {
            onRemoveAble && (
              removeIcon ||
              <Icon
                style={{ marginLeft: WP305 }}
                centered
                size='xmini'
                color={GREY_PLACEHOLDER}
                onPress={onRemoveAble}
                name='close-circle' type='MaterialCommunityIcons'
              />
            )
          }
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 3 }}>
          {
            !!wording && !error && (
              <View style={{ flexDirection: 'row' }}>
                <Text size='xtiny' color={GREY_DISABLE}>{wording}</Text>
                {required && <RequiredMark size={WP105} />}
              </View>
            )
          }
          {
            !!error && (
              <Text size='xtiny' color={RED_GOOGLE}>{error}</Text>
            )
          }
          {
            (!!maxLength && showLength) && (
              <Text
                size={maxLengthFocus ? 'xmini' : 'xtiny'}
                type={maxLengthFocus ? 'Circular' : 'OpenSans'}
                weight={maxLengthFocus ? isEditing ? 400 : 300 : valueLength === maxLength ? 500 : 300}
                color={maxLengthFocus ? isEditing ? GUN_METAL : PALE_LIGHT_BLUE_TWO : valueLength === maxLength ? TOMATO : SILVER}
              >
                {valueLength}/{maxLength}
              </Text>
            )
          }
        </View>
      </View>
    )
  }
}

InputTextLight.propTypes = propsType
InputTextLight.defaultProps = propsDefault
export default InputTextLight
