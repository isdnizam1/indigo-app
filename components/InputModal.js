import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Keyboard,
  TouchableOpacity,
  View,
  Platform,
  TextInput,
  ScrollView,
} from 'react-native'
import { isFunction, isEqual, isEmpty, reduce, isArray, includes } from 'lodash-es'
import { connect } from 'react-redux'
import {
  HP1,
  HP2,
  HP4,
  HP50,
  HP90,
  WP1,
  WP2,
  WP20,
  WP4,
  WP405,
} from '../constants/Sizes'
import {
  GREY,
  GREY_SEMI,
  GREY_WARM,
  WHITE,
  WHITE_MILK,
  GREY_PLACEHOLDER,
} from '../constants/Colors'
import { BORDER_STYLE, TOUCH_OPACITY } from '../constants/Styles'
import Modal from './Modal'
import Text from './Text'
import Icon from './Icon'
import Button from './Button'

const propsType = {
  triggerComponent: PropTypes.objectOf(PropTypes.any),
  keyword: PropTypes.string,
  onChange: PropTypes.func,
  header: PropTypes.string,
  suggestion: PropTypes.func,
  renderItem: PropTypes.func,
  reformatFromApi: PropTypes.func,
  suggestionKey: PropTypes.string,
  suggestionPathResult: PropTypes.string,
  suggestionPathValue: PropTypes.string,
  asObject: PropTypes.bool,
  placeholder: PropTypes.string,
  refreshOnSelect: PropTypes.bool,
  extraResult: PropTypes.bool,
  disabled: PropTypes.bool,
  titleCenter: PropTypes.bool,
}

const propsDefault = {
  keyword: '',
  triggerComponent: {},
  onChange: () => {},
  header: '',
  suggestion: () => {},
  suggestionKey: '',
  suggestionPathResult: '',
  suggestionPathValue: '',
  asObject: false,
  createNew: true,
  placeholder: '',
  refreshOnSelect: false,
  extraResult: false,
  disabled: false,
  titleCenter: true,
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
})

class InputModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      keyword: '',
      inputKey: '',
      suggestions: [],
      suggestionsRecent: [],
      showSuggestion: false,
      modalHeight: HP90,
    }
  }

  componentDidMount = () => {
    // this._onFetchSuggestion()
    this.keyboardDidShowListener = Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      () => {
        this.setState({ modalHeight: HP50 })
      },
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      () => {
        this.setState({ modalHeight: HP90 })
      },
    )
  };

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  _onFocus = () => {
    this.setState({ showSuggestion: true }, this._onFetchSuggestion)
  };

  _onBlur = () => {
    this.setState({ showSuggestion: false })
  };

  _onChangeText = (text) => {
    this.setState({ keyword: text }, this._onFetchSuggestion)
  };

  _onSelectSuggestion = (value, closeModalCallback) => {
    const { onChange, suggestionPathResult, asObject, refreshOnSelect } = this.props

    if (asObject) {
      if (value[suggestionPathResult] !== this.props.selected) {
        if (!refreshOnSelect)
          this.setState({ keyword: value[suggestionPathResult] })
        onChange(value)
      } else {
        this.setState({ keyword: '' })
        onChange({})
      }
    } else {
      if (value[suggestionPathResult] !== this.props.selected) {
        if (!refreshOnSelect)
          this.setState({ keyword: value[suggestionPathResult] })
        else this.setState({ keyword: '' })
        onChange(value[suggestionPathResult])
      } else {
        this.setState({ keyword: '' })
        onChange('')
      }
    }
    closeModalCallback()
  };

  _onCreateNewOption = (closeModalCallback) => {
    const { keyword } = this.state
    const { onChange } = this.props

    onChange(keyword)
    closeModalCallback()
  };

  _onClearInput = () => {
    this._onFocus()
  };

  _onFetchSuggestion = () => {
    const {
      suggestion,
      suggestionKey,
      userData: { id_user },
      suggestionPathResult,
      extraResult,
      reformatFromApi,
      suggestionPathValue,
    } = this.props
    const { keyword } = this.state
    if (isFunction(suggestion)) {
      suggestion({
        [suggestionKey]: keyword,
        id_user,
        limit: 6,
        start: 0,
      })
        .then(({ data }) => {
          let suggestionData = data.result || []
          if (reformatFromApi) {
            suggestionData = reduce(
              suggestionData,
              (result, item) => {
                result.push({
                  [suggestionPathResult]: reformatFromApi(
                    item[suggestionPathResult],
                  ),
                  [suggestionPathValue]: item[suggestionPathValue],
                })
                return result
              },
              [],
            )
          }
          let suggestionDataRecentlySearch = []
          if (extraResult) {
            suggestionDataRecentlySearch = data.recentlySearch || []
            if (!isEmpty(suggestionDataRecentlySearch)) {
              suggestionDataRecentlySearch = reduce(
                data.recentlySearch,
                (result, item) => {
                  result.push({
                    [suggestionPathResult]: reformatFromApi
                      ? reformatFromApi(item.value)
                      : item.value,
                    [suggestionPathValue]: item[suggestionPathValue],
                  })
                  return result
                },
                [],
              )
            }
          }
          this.setState({
            suggestions: suggestionData,
            suggestionsRecent: suggestionDataRecentlySearch,
            showSuggestion: true,
          })
        })
        .catch((e) => {})
    }
  };

  _isSelected = (item) => {
    const { selected, suggestionPathResult } = this.props

    if (isArray(selected)) return includes(selected, item[suggestionPathResult])
    else return isEqual(selected, item[suggestionPathResult])
  };

  render() {
    const {
      triggerComponent,
      header,
      suggestionPathResult,
      placeholder,
      disabled,
      hideSearch,
      titleCenter,
    } = this.props

    const {
      keyword,
      suggestions,
      suggestionsRecent,
      modalHeight,
      showSuggestion,
    } = this.state
    return (
      <View style={{ flexGrow: 1 }}>
        <Modal
          renderModalContent={({ toggleModal, payload }) => (
            <View
              style={{
                height: modalHeight,
              }}
            >
              {/*header*/}
              <View
                style={{
                  backgroundColor: GREY_SEMI,
                  width: WP20,
                  borderRadius: 5,
                  height: 5,
                  marginTop: 10,
                  alignSelf: 'center',
                }}
              />
              <View
                style={{
                  paddingHorizontal: WP4,
                  paddingTop: HP1,
                  paddingBottom: HP2,
                  ...BORDER_STYLE['bottom'],
                }}
              >
                <Text centered={titleCenter} type='SansPro' weight={500}>
                  {header}
                </Text>
              </View>

              {/*search input*/}
              {!hideSearch && (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: WHITE,
                    marginVertical: HP1,
                    paddingHorizontal: WP4,
                  }}
                >
                  <View
                    style={{
                      backgroundColor: WHITE_MILK,
                      borderRadius: 100,
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      paddingHorizontal: WP2,
                      paddingVertical: WP1,
                    }}
                  >
                    <Icon centered color={GREY_WARM} name='search1' size='large' />
                    <TextInput
                      returnKeyType='search'
                      value={keyword}
                      style={{
                        height: HP4,
                        flex: 1,
                        fontFamily: 'SansProRegular',
                        fontSize: WP405,
                        paddingHorizontal: WP1,
                        color: GREY,
                      }}
                      placeholderTextColor={GREY_PLACEHOLDER}
                      onChangeText={this._onChangeText}
                      onFocus={this._onFocus}
                      placeholder={placeholder}
                    />
                    {!isEmpty(keyword) && (
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({ keyword: '' }, this._onClearInput)
                        }
                      >
                        <Icon centered color={GREY_WARM} name='close' size='large' />
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
              )}

              {/*Suggestion*/}
              {showSuggestion &&
                this._suggestionList(
                  suggestions,
                  toggleModal,
                  suggestionPathResult,
                  suggestionsRecent,
                )}
            </View>
          )}
        >
          {({ toggleModal, isVisible }, M) => (
            <View>
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() => {
                  if (!isVisible) this._onFetchSuggestion()
                  toggleModal()
                }}
                disabled={disabled}
              >
                <View pointerEvents='none'>{triggerComponent}</View>
              </TouchableOpacity>
              {M}
            </View>
          )}
        </Modal>
      </View>
    )
  }

  _suggestionList(
    suggestions,
    toggleModal,
    suggestionPathResult,
    suggestionsRecent,
  ) {
    const { extraResult } = this.props
    const { keyword } = this.state
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps='handled'
      >
        {!isEmpty(suggestionsRecent) && isEmpty(keyword) && extraResult && (
          <View style={{ ...BORDER_STYLE['bottom'], padding: WP2 }}>
            <Text type='SansPro' weight={500}>
              Recent Searches
            </Text>
          </View>
        )}
        {!isEmpty(suggestionsRecent) &&
          isEmpty(keyword) &&
          extraResult &&
          suggestionsRecent.map((item, index) =>
            this._renderItem(index, item, toggleModal, suggestionPathResult),
          )}
        {!this.props.hideSearch && isEmpty(keyword) && extraResult && (
          <View style={{ ...BORDER_STYLE['bottom'], padding: WP2 }}>
            <Text type='SansPro' weight={500}>
              Popular Searches
            </Text>
          </View>
        )}
        {(suggestions || []).map((item, index) =>
          this._renderItem(index, item, toggleModal, suggestionPathResult),
        )}
        {!isEmpty(keyword) && this._createNewOption(toggleModal)}
      </ScrollView>
    )
  }

  _renderItem = (index, item, toggleModal, suggestionPathResult) => {
    return (
      <TouchableOpacity
        key={index}
        style={{
          ...BORDER_STYLE['bottom'],
          paddingVertical: HP1,
          paddingHorizontal: WP4,
        }}
        onPress={() => {
          this._onSelectSuggestion(item, toggleModal)
        }}
      >
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          {this._renderItemText(item, suggestionPathResult)}
          {this._isSelected(item) && <Icon name='check' />}
        </View>
      </TouchableOpacity>
    )
  };

  _renderItemText = (item, suggestionPathResult) => {
    const { renderItem } = this.props
    if (renderItem) {
      const itemEl = renderItem(item[suggestionPathResult])
      if (React.isValidElement(itemEl)) return itemEl
      else
        return (
          <Text size='mini' color={GREY} weight={this._isSelected(item) ? 400 : 300}>
            {itemEl}
          </Text>
        )
    } else
      return (
        <Text size='mini' color={GREY} weight={this._isSelected(item) ? 400 : 300}>
          {item[suggestionPathResult]}
        </Text>
      )
  };

  _createNewOption(toggleModal) {
    const { createNew } = this.props
    const { keyword, suggestions } = this.state

    if (!createNew && isEmpty(suggestions)) {
      return (
        <View>
          <View
            style={{
              ...BORDER_STYLE['bottom'],
              paddingVertical: HP1,
            }}
          >
            <Text centered size='mini'>
              {'No suggestion found!'}
            </Text>
          </View>
        </View>
      )
    }

    if (createNew) {
      return (
        <View>
          <View
            style={{
              ...BORDER_STYLE['bottom'],
              paddingHorizontal: WP4,
              paddingVertical: HP1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <Text size='mini' color={GREY} style={{ flex: 1 }}>
              {keyword}
            </Text>
            <Button
              onPress={() => {
                this._onCreateNewOption(toggleModal)
              }}
              nativeEffect
              rounded
              centered
              soundfren
              compact='flex-end'
              shadow='none'
              textColor={WHITE}
              textSize='tiny'
              textWeight={500}
              style={{ alignSelf: 'center', marginHorizontal: WP4 }}
              marginless
              text='Create New'
            />
          </View>
        </View>
      )
    }
  }
}

InputModal.propTypes = propsType

InputModal.defaultProps = propsDefault

export default connect(mapStateToProps, {})(InputModal)
