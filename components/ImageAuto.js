import React from 'react'
import PropTypes from 'prop-types'
import { Image as RNImage } from 'react-native'
import { WP100 } from '../constants/Sizes'

const propsType = {
  width: PropTypes.any,
  style: PropTypes.any,
  source: PropTypes.any
}

const propsDefault = {
  width: WP100,
  style: {},
  source: require('../assets/icons/user.png'),
  borderRadius: 0
}

class ImageAuto extends React.Component {
  state = {
    width: 1,
    height: 1
  }

  componentDidMount() {
    RNImage.getSize(this.props.source.uri, (width, height) => {
      this.setState({ width, height })
    })
  }

  render() {
    const {
      source,
      width,
      style,
      borderRadius
    } = this.props
    return (
      <RNImage style={[{ borderRadius, width, aspectRatio: this.state.width / this.state.height }, style]} source={source}/>
    )
  }
}

ImageAuto.propTypes = propsType
ImageAuto.defaultProps = propsDefault
export default ImageAuto
