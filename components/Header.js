import React from 'react'
import PropTypes from 'prop-types'
import { LinearGradient } from 'expo-linear-gradient'
import { ORANGE_BRIGHT, PINK_RED } from '../constants/Colors'
import { WP4 } from '../constants/Sizes'

const propsType = {
  key: PropTypes.any,
  backgroundColor: PropTypes.string,
  colors: PropTypes.array,
  style: PropTypes.object,
  soundfren: PropTypes.bool
}

const propsDefault = {
  style: {},
  backgroundColor: 'transparent',
  soundfren: false
}

const Header = (props) => {
  const {
    backgroundColor,
    colors,
    soundfren,
    style,
    children
  } = props

  return (
    <LinearGradient
      colors={colors || soundfren ? [PINK_RED, ORANGE_BRIGHT] : [backgroundColor, backgroundColor]}
      start={[0, 0]} end={[1, 0]}
      style={[{
        backgroundColor, paddingHorizontal: WP4, paddingVertical: WP4, flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
      }, style]}
    >
      {children}
    </LinearGradient>
  )
}

Header.propTypes = propsType
Header.defaultProps = propsDefault
export default Header
