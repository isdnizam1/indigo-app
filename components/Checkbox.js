import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { WP2 } from '../constants/Sizes'
import { ORANGE, GREY_CALM_SEMI, GREY_WARM } from '../constants/Colors'
import Icon from './Icon'
import Text from './Text'

const propsType = {
  onPress: PropTypes.func,
  checked: PropTypes.bool,
  text: PropTypes.string,
  disabled: PropTypes.bool
}

const propsDefault = {
  checked: false,
  disabled: false
}

const Checkbox = (props) => {
  const {
    onPress,
    checked,
    text,
    disabled
  } = props

  let pressHandler = checked === '1' ?
    () => onPress('0') : () => onPress('1')

  if (disabled) pressHandler = () => {}

  return (
    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
      {
        checked === '1' ? (
          <Icon
            color={ORANGE}
            size='large'
            name='check-square'
            type='FontAwesome'
            onPress={pressHandler}
          />
        ) : (
          <Icon
            color={GREY_CALM_SEMI}
            size='large'
            name='square-o'
            type='FontAwesome'
            onPress={pressHandler}
          />
        )
      }
      {
        !!text && <Text onPress={pressHandler} size='xtiny' centered color={GREY_WARM} style={{ marginLeft: WP2 }}>{text}</Text>
      }
    </View>
  )
}

Checkbox.propTypes = propsType
Checkbox.defaultProps = propsDefault
export default Checkbox
