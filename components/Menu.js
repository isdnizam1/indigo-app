import React from 'react'
import { View } from 'react-native'
import { map } from 'lodash-es'
import PropTypes from 'prop-types'
import { WP4, WP305, WP105, WP1 } from 'sf-constants/Sizes'
import Spacer from 'sf-components/Spacer'
import { WP2 } from '../constants/Sizes'
import ListItem from './ListItem'
import Icon from './Icon'
import Text from './Text'
import Modal from './Modal'

import Divider from './Divider'

const propsType = {
  options: PropTypes.array.isRequired,
  triggerComponent: PropTypes.func.isRequired
}

class Menu extends React.Component {
  render() {
    const {
      options,
      triggerComponent
    } = this.props
    return (
      <Modal
        renderModalContent={({ toggleModal }) => (
          <View>
            <Spacer size={WP105} />
            {
              map(options, (option, key) => (
                <ListItem
                  inline
                  onPress={async () => {
                    await option.onPress()
                    toggleModal()
                  }}
                  style={{ paddingVertical: WP305 }}
                >
                  {option.iconName && <Icon centered type={option.iconType || 'MaterialIcons'} size={option.iconSize || 'large'} name={option.iconName || 'ac-unit'}/>}
                  <Text type={'Circular'} centered style={{ paddingLeft: WP2 }}>{option.name}</Text>
                </ListItem>
              ))
            }
            <Spacer size={WP105} />
            <Divider />
            <ListItem
              inline
              onPress={toggleModal}
              style={{ paddingVertical: WP4 + WP1 }}
            >
              <Icon centered type={'MaterialIcons'} size={'large'} name={'close'}/>
              <Text type={'Circular'} centered style={{ paddingLeft: WP2 }}>Tutup</Text>
            </ListItem>
          </View>
        )}
      >
        {({ toggleModal }, M) => (
          <>
            {
              triggerComponent(toggleModal)
            }
            {M}
          </>
        )}
      </Modal>
    )
  }
}

Menu.propTypes = propsType
export default Menu
