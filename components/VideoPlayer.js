import React from 'react'
import * as ScreenOrientation from 'expo-screen-orientation'
import { Video } from 'expo-av'
import { connect } from 'react-redux'
import { View, StyleSheet, Slider, Dimensions, TouchableWithoutFeedback, TouchableOpacity, Animated, ActivityIndicator, Platform } from 'react-native'
import { getTime } from '../utils/helper'
import {
  WHITE,
  REDDISH,
  PALE_LIGHT_BLUE_TWO
} from '../constants/Colors'
import { FullscreenEnterIcon, PauseIcon, RewindIcon, ForwardIcon, ReplayIcon } from '../assets/icons/video_player/icons'
import {
  WP3, WP5, WP205, WP4, WP6, WP12
} from '../constants/Sizes'
import { SHADOW_STYLE } from '../constants/Styles'
import Icon from './Icon'
import Text from './Text'

const PlayIcon = () => {
  return (<Icon name='play-arrow' type='MaterialIcons' size={WP12} color={WHITE} centered />
  )
}

class VideoPlayer extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      durationMillis: this.props.durationMillis || 0,
      positionMillis: this.props.positionMillis || 0,
      animation: new Animated.Value(0),
      isPlaying: false,
      isBuffering: false,
      controlShown: false
    }
  }

  componentDidMount() {
    this.toggleControls()
  }
  toggleControls() {
    typeof this.timeout !== 'undefined' && clearTimeout(this.timeout)
    this.setState({ controlShown: true })
    Animated.timing(this.state.animation, {
      toValue: 1,
      duration: 400,
      timing: 200,
      useNativeDriver: true
    }).start(() => {
      this.timeout = setTimeout(() => {
        Animated.timing(this.state.animation, {
          toValue: 0,
          duration: 500,
          timing: 100,
          useNativeDriver: true
        }).start(() => this.setState({ controlShown: false }))
      }, 2000)
    })
  }

  onVideoRef = (component) => {
    // component.setStatusAsync({pitchCorrectionQuality: Audio.PitchCorrectionQuality.Low})
    // if(component) component._nativeOnPlaybackStatusUpdate = (this.onPlaybackStatusUpdate.bind(this))
    const { onRef, actionVideoPlaying } = this.props
    if (component) {
      this.videoRef = component
      onRef && onRef(component)
      component._nativeOnPlaybackStatusUpdate = (event) => {
        let {
          nativeEvent: {
            playableDurationMillis,
            positionMillis,
            durationMillis,
            isPlaying,
            isBuffering,
            didJustFinish,
          }
        } = event
        this.setState({ isPlaying, isBuffering, playableDurationMillis, positionMillis, durationMillis })
        this.positionMillis = positionMillis
        if (didJustFinish) {
          actionVideoPlaying('completed')
        }
      }
    }
  }

  onTimeSliding(value) {
    // this.setState({positionMillis: parseInt(value)})
    this.videoRef && this.videoRef.setPositionAsync(parseInt(value))
  }

  async enterFullscreen() {
    this.videoRef && this.videoRef.presentFullscreenPlayer()
    // const { positionMillis, durationMillis } = this.state
    // const { source, navigate } = this.props
  }

  togglePlaying() {
    if (this.state.controlShown) {
      !this.state.isPlaying && this.videoRef && this.videoRef.playAsync()
      this.state.isPlaying && this.videoRef && this.videoRef.pauseAsync()
    } else this.toggleControls()
  }

  async onFullscreenUpdate(value) {
    value.fullscreenUpdate == 1 && await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE)
    value.fullscreenUpdate == 2 && await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT)
    const { watchFullscreenUpdate } = this.props
    typeof watchFullscreenUpdate !== 'undefined' && watchFullscreenUpdate({
      fullScreen: value.fullscreenUpdate == 1
    })
  }

  async onRewind() {
    if (this.state.controlShown) {
      let positionMillis = this.state.positionMillis - (this.state.durationMillis / 10)
      if (positionMillis < 0) positionMillis = 0
      await this.setState({ positionMillis })
      await this.videoRef.setStatusAsync({ positionMillis, shouldPlay: true })
    } else this.toggleControls()
  }

  async onForward() {
    if (this.state.controlShown) {
      let positionMillis = this.state.positionMillis + (this.state.durationMillis / 10)
      if (positionMillis > this.state.durationMillis) positionMillis = this.state.durationMillis
      await this.setState({ positionMillis })
      await this.videoRef.setStatusAsync({ positionMillis, shouldPlay: true })
    } else this.toggleControls()
  }

  async replay() {
    await this.videoRef.setStatusAsync({ positionMillis: 0, shouldPlay: true })
  }

  render() {
    const {
      durationMillis,
      positionMillis,
      isPlaying,
      isBuffering
    } = this.state
    const { onBackPressed, actionVideoPlaying } = this.props
    const animatedStyle = {
      opacity: this.state.animation
    }
    return (<View>
      <TouchableWithoutFeedback onPress={this.toggleControls.bind(this)}>
        <View>
          <Video
            useNativeControls={false}
            onFullscreenUpdate={this.onFullscreenUpdate.bind(this)}
            ref={this.onVideoRef}
            shouldPlay={true}
            positionMillis={0}
            inFullScreen={true}
            resizeMode={Video.RESIZE_MODE_CONTAIN}
            {...this.props}
          />
          <Animated.View style={[playerStyle.controls, !isBuffering && positionMillis !== durationMillis ? animatedStyle : null]}>
            {positionMillis === durationMillis && positionMillis > 0 && durationMillis > 0 && (<View style={{ flexDirection: 'row', marginLeft: 'auto', marginRight: 'auto', alignItems: 'center' }}>
              <TouchableOpacity style={{ paddingHorizontal: 20 }} onPress={this.replay.bind(this)}>
                <ReplayIcon />
              </TouchableOpacity>
            </View>)}
            {positionMillis !== durationMillis && (<View style={{ flexDirection: 'row', marginLeft: 'auto', marginRight: 'auto', alignItems: 'center' }}>
              {!isBuffering && isPlaying && (
                <TouchableOpacity style={{ paddingHorizontal: 20 }} onPress={this.onRewind.bind(this)}>
                  <RewindIcon />
                </TouchableOpacity>
              )}
              {isBuffering && (<ActivityIndicator size={'large'} color={WHITE} />)}
              {!isBuffering && (
                <TouchableOpacity style={{ paddingHorizontal: 20 }} onPress={this.togglePlaying.bind(this)}>
                  {isPlaying ? <PauseIcon /> : <PlayIcon />}
                </TouchableOpacity>
              )}
              {!isBuffering && isPlaying && (
                <TouchableOpacity style={{ paddingHorizontal: 20 }} onPress={this.onForward.bind(this)}>
                  <ForwardIcon />
                </TouchableOpacity>
              )}
            </View>)}
            {onBackPressed && (<Icon
              style={{ position: 'absolute', top: 30, left: WP3 }}
              size={'large'} name={'chevron-left'} type={'Entypo'}
              onPress={onBackPressed} color={WHITE}
              marginRight
            />)}
          </Animated.View>
          <Animated.View style={[playerStyle.timer, positionMillis !== durationMillis ? animatedStyle : null]}>
            <Text color={WHITE} size={'tiny'}>{getTime(positionMillis)}</Text>
            <Text style={{ marginLeft: 'auto', marginRight: 'auto' }} color={WHITE} size={'tiny'} />
            <Text style={{ marginRight: 10 }} color={WHITE} size={'tiny'}>{getTime(durationMillis)}</Text>
            <TouchableOpacity onPress={this.enterFullscreen.bind(this)}>
              <FullscreenEnterIcon />
            </TouchableOpacity>
          </Animated.View>
        </View>
      </TouchableWithoutFeedback>
      <View style={playerStyle.slider}>
        <Slider
          color={WHITE}
          minimumValue={0}
          maximumValue={durationMillis}
          minimumTrackTintColor={REDDISH}
          maximumTractTintColor={PALE_LIGHT_BLUE_TWO}
          step={1}
          value={positionMillis}
          // value={typeof positionMillis === 'number' ? positionMillis : null}
          style={{ flex: 1 }}
          onValueChange={this.onTimeSliding.bind(this)}
          thumbTintColor={REDDISH}
        />
      </View>
    </View>)
  }

}

const playerStyle = StyleSheet.create({
  timer: {
    position: 'absolute',
    bottom: Platform.OS == 'android' ? 0 : WP4,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: 999,
    paddingHorizontal: WP3,
    ...SHADOW_STYLE['shadowBold']
  },
  controls: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,0.05)',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: WP3,
    ...SHADOW_STYLE['shadowBold']
  },
  slider: {
    width: Dimensions.get('window').width + 32,
    marginLeft: -16,
    zIndex: 999,
    position: 'absolute',
    bottom: Platform.OS == 'android' ? -WP205 : -WP5,
    paddingHorizontal: WP6
  }
})

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

export default connect(mapStateToProps, {})(VideoPlayer)
