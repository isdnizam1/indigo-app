import React from "react";
import PropTypes from "prop-types";
import { Image } from "react-native";
import { FONT_SIZE } from "../constants/Sizes";
import Text from "./Text";

const TextName = (props) => {
  const fontSize = props.size ? FONT_SIZE[props.size] : FONT_SIZE["mini"];
  const user = props.user;
  const type = props.type || "Circular";
  const weight = props.weight || 500;
  return (
    <Text
      numberOfLines={1}
      centered={props.centered}
      type={type}
      weight={weight}
      {...props}
    >
      {user.full_name || user.name}

      {/* disable premium user badge */}
      {/* {user.account_type === 'premium' && <Text>{' '}</Text>}
      {
        user.account_type === 'premium' && (
          <Image
            style={{ width: fontSize, height: fontSize }}
            source={require('../assets/icons/badge.png')}
          />
        )
      } */}
    </Text>
  );
};

TextName.propTypes = {
  ...Text.propTypes,
  user: PropTypes.any,
  centered: PropTypes.bool,
  style: PropTypes.objectOf(PropTypes.any),
};

TextName.defaultProps = {
  centered: true,
  style: {},
};

export default TextName;
