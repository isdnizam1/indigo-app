import { isEmpty } from 'lodash-es'
import React from 'react'
import { memo } from 'react'
import { TouchableOpacity, View } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { PALE_BLUE, PALE_WHITE, SKELETON_COLOR, SKELETON_HIGHLIGHT, WHITE, WHITE30 } from '../../constants/Colors'
import { WP100, WP3, WP4, WP6, WP7, WP8 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import Icon from '../Icon'
import Image from '../Image'
import Text from '../Text'

const GamificationButtonSection = (props) => {
  const { data, loading, navigateTo } = props
  const itemWidth = WP100 - WP8
  return (!isEmpty(data) || loading) && (
    <View style={{ width: '100%', borderTopColor: PALE_BLUE, borderTopWidth: 1 }}>
      {
        loading ? (
          <SkeletonContent
            containerStyle={{ flex: 1 }}
            layout={[
              {
                width: itemWidth,
                borderRadius: 12,
                height: (itemWidth * 68) / 328,
                marginHorizontal: WP4,
                marginTop: WP6,
                marginBottom: WP7,
              }
            ]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          />
        ) : (
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            onPress={() => navigateTo('LuckyFrenScreen')}
            style={{ paddingTop: WP4, paddingBottom: WP3, paddingHorizontal: WP4 }}
          >
            <Image
              source={require('../../assets/images/bgGamificationButton.png')}
              imageStyle={{ height: (itemWidth * 100) / 360, aspectRatio: 3.95 }}
            />
            <View
              style={{
                width: '100%',
                height: (itemWidth * 68) / 328,
                position: 'absolute',
                zIndex: 2,
                justifyContent: 'center',
                flexDirection: 'row',
                alignItems: 'center',
                marginHorizontal: WP4,
                padding: WP4,
                marginTop: WP6,
              }}
            >
              <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column' }}>
                <Text numberOfLines={1} type='Circular' size='mini' weight={500} color={PALE_WHITE}>{data.title}</Text>
                <Text numberOfLines={1} type='Circular' size='xmini' weight={300} color={PALE_WHITE}>{data.subtitle+'ssss'}</Text>
              </View>
              <View style={{ width: WP6, height: WP6, borderRadius: 6, backgroundColor: WHITE30, justifyContent: 'center', alignItems: 'center' }}>
                <Icon size='small' name='chevron-right' type='Entypo' color={WHITE} centered />
              </View>
            </View>
          </TouchableOpacity>
        )
      }
    </View>
  )
}

export default memo(GamificationButtonSection)
