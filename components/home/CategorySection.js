import React, { memo } from "react";
import { View, FlatList, TouchableOpacity } from "react-native";
import { isEmpty } from "lodash-es";
import SkeletonContent from "react-native-skeleton-content";
import { postLogActivity } from "sf-actions/api";
import {
  NAVY_DARK,
  PALE_BLUE,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
  WHITE_MILK,
} from "../../constants/Colors";
import { WP2, WP4, WP35, WP44, WP45, WP5, WP6 } from "../../constants/Sizes";
import Image from "../Image";
import Text from "../Text";

export const _mapResponseCategory = (categoryList) => {
  let newData = [];
  let topData = categoryList.splice(0, Math.ceil(categoryList.length / 2));
  let bottomData = categoryList;
  while (topData.length > 0) {
    const itemData = [...topData.splice(0, 1), ...bottomData.splice(0, 1)];
    newData.push(itemData);
  }
  return newData;
};

const CategorySection = (props) => {
  const { list, loading, navigateTo } = props;
  const tmpList = list ? [...list] : [];
  const dummyData = [
      [1, 2],
      [3, 4],
      [5, 6],
    ],
    data = loading
      ? dummyData
      : list?.length > 0 && _mapResponseCategory(tmpList);

  const _renderItem = ({ item, index }) => {
    const itemWidth = data.length > 2 ? WP44 : WP45;
    const wrapperStyle = {
      width: itemWidth,
      marginRight: WP2,
    };
    const itemView = (itemData) => (
      <View
        key={`${Math.random()}`}
        style={{ width: itemWidth, marginBottom: WP2 }}
      >
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[
            { width: itemWidth, height: (itemWidth * 9) / 16, borderRadius: 6 },
          ]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <View
            style={{
              borderRadius: 6,
              overflow: "hidden",
              backgroundColor: WHITE_MILK,
              height: (itemWidth * 9) / 16,
            }}
          >
            {!loading && (
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => _onPressItem(itemData)}
              >
                <View>
                  <Image
                    centered
                    source={{ uri: itemData.image_web }}
                    imageStyle={{
                      height: (itemWidth * 9) / 16,
                      aspectRatio: 17 / 9,
                    }}
                    style={{
                      backgroundSize: "content",
                    }}
                  />
                  <View
                    style={{
                      position: "absolute",
                      alignSelf: "center",
                      top: 5,
                      left: 10,
                    }}
                  >
                    <Image
                      centered
                      source={{ uri: itemData.icon }}
                      imageStyle={{
                        height: (itemWidth * 3) / 20,
                      }}
                      style={{
                        backgroundSize: "content",
                        aspectRatio: 16 / 4,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      position: "absolute",
                      alignSelf: "center",
                      bottom: 10,
                      left: 10,
                    }}
                  >
                    <Text
                      type="Circular"
                      size="mini"
                      color={WHITE}
                      weight={500}
                      centered
                    >
                      {itemData.label}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          </View>
        </SkeletonContent>
      </View>
    );
    return (
      <View key={`${index}-category`} style={wrapperStyle}>
        {item.map((itemMap, indexMap) => itemView(itemMap))}
      </View>
    );
  };

  const _onPressItem = (item) => {
    const category = item.category_name;
    let tracking_value;
    if (category === "band" || category === "artist") {
      tracking_value = "artist_spotlight";
      navigateTo("ArtistSpotlightScreen");
    } else if (category === "sc_session") {
      tracking_value = "soundfren_connect";
      navigateTo("SoundconnectScreen");
    } else if (category === "sc_learn") {
      tracking_value = "soundfren_learn";
      navigateTo("SoundfrenLearnScreen");
    } else if (category === "collaboration") {
      tracking_value = "collaboration";
      navigateTo("CollaborationScreenV3");
    } else if (category === "event_audition" || category === "submission") {
      tracking_value = "event_audition";
      navigateTo("Submission");
    } else if (category === "news_update") {
      tracking_value = "news";
      navigateTo("TimelineScreen", { topicId: 13, allowBack: true }, "push");
    }
    !!tracking_value &&
      postLogActivity({
        id_user: props.idUser,
        value: tracking_value,
      }).then(({ data }) => {
        // __DEV__ && console.log({ data })
      });
  };

  return (
    !isEmpty(data) && (
      <View
        style={{
          paddingTop: WP6,
          paddingBottom: WP4,
          borderTopColor: PALE_BLUE,
          borderTopWidth: 1,
        }}
      >
        <SkeletonContent
          containerStyle={{ flex: 1, paddingHorizontal: WP4 }}
          layout={[{ width: WP35, height: WP5 }]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text type="Circular" size="small" color={NAVY_DARK} weight={600}>
              Aktivitas Pilihan
            </Text>
          </View>
        </SkeletonContent>

        <View>
          <FlatList
            showsHorizontalScrollIndicator={false}
            bounces={false}
            horizontal
            data={data}
            contentContainerStyle={{
              paddingTop: WP4,
              paddingLeft: WP4,
              paddingRight: WP2,
              paddingBottom: WP2,
            }}
            renderItem={_renderItem}
          />
        </View>
      </View>
    )
  );
};

export default memo(CategorySection);
