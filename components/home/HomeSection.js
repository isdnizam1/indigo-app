import React, { Component, memo } from "react";
import { FlatList, TouchableOpacity, View } from "react-native";
import PropTypes from "prop-types";
import { LinearGradient } from "expo-linear-gradient";
import { isEmpty, noop } from "lodash-es";
import SkeletonContent from "react-native-skeleton-content";
import { postLogActivity } from "sf-actions/api";
import {
  NAVY_DARK,
  NO_COLOR,
  PALE_BLUE,
  PALE_SALMON,
  PALE_WHITE,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
} from "../../constants/Colors";
import { WP2, WP20, WP305, WP4, WP40, WP6, WP8 } from "../../constants/Sizes";
import Text from "../Text";
import { TOUCH_OPACITY } from "../../constants/Styles";
import EventItem from "../explore/items/EventItem";
import { adsConfig } from "../home/HomeConstant";
import Layout from "../home/HomeSkeletonLayout";
import { getYoutbeId } from "../../utils/helper";
import SessionItemV2 from "./items/SessionItemV2";
import SoundplayItemV2 from "./items/SoundplayItemV2";
import VideoItemV2 from "./items/VideoItemV2";
import SubmissionItemV2 from "./items/SubmissionItemV2";
import CollaborationItemV2 from "./items/CollaborationItemV2";

class HomeSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adList: [],
    };
  }

  _onPressItem = (ads) => () => {
    const { navigateTo, category, activeLearn, idUser } = this.props;
    if (!idUser) navigateTo("AuthNavigator");
    else {
      if (category === "soundconnect") {
        navigateTo("SessionDetail", { id_ads: ads.id_ads });
      } else if (category === "learn") {
        if (activeLearn === "podcast") {
          navigateTo("SoundplayDetail", { id_ads: ads.id_ads });
        } else if (activeLearn === "video") {
          const additional = JSON.parse(ads.additional_data);
          const youtubeId = getYoutbeId(additional.url_video);
          youtubeId &&
            navigateTo("PremiumVideoTeaser", { youtubeId, id_ads: ads.id_ads });
          !youtubeId &&
            navigateTo("PremiumVideoTeaser", { id_ads: ads.id_ads });
        }
      } else if (category === "event_audition" || category === "submission") {
        navigateTo("SubmissionPreview", { id_ads: ads.id_ads });
      } else if (category === "collaboration") {
        navigateTo("CollaborationPreview", { id: ads.id_ads });
      } else {
        navigateTo("AdsDetailScreen", { id_ads: ads.id_ads });
      }
    }
  };
  _renderHeader = () => {
    const { loading, category, navigateTo, navigation } = this.props;
    if (loading) {
      return (
        <View style={{ paddingHorizontal: WP4 }}>
          <SkeletonContent
            containerStyle={Layout.container}
            layout={Layout.header}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          />
        </View>
      );
    }
    return (
      <>
        <View
          style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingHorizontal: WP4,
          }}
        >
          <Text type="Circular" size="small" color={NAVY_DARK} weight={600}>
            {adsConfig[category].title}
          </Text>
          <TouchableOpacity
            disabled={loading}
            activeOpacity={TOUCH_OPACITY}
            onPress={() => {
              let tracking_value;
              if (category === "soundconnect") {
                tracking_value = "soundfren_connect";
                navigateTo("SoundconnectScreen");
              } else if (category === "learn") {
                tracking_value = "soundfren_learn";
                navigateTo("SoundfrenLearnScreen");
              } else if (category === "collaboration") {
                tracking_value = "collaboration";
                navigateTo("CollaborationScreen");
              } else if (
                category === "event_audition" ||
                category === "submission"
              ) {
                tracking_value = "event_audition";
                navigateTo("Submission");
              }
              !!tracking_value &&
                postLogActivity({
                  id_user: this.props.idUser,
                  value: tracking_value,
                }).then(({ data }) => {
                  // __DEV__ && console.log({ data })
                });
            }}
          >
            <Text type="Circular" size="xmini" color={REDDISH} weight={400}>
              Lihat Semua
            </Text>
          </TouchableOpacity>
        </View>

        {!isEmpty(adsConfig[category].subtitle) && (
          <Text
            type="Circular"
            size="mini"
            color={SHIP_GREY}
            style={{ paddingHorizontal: WP4 }}
          >
            {adsConfig[category].subtitle}
          </Text>
        )}
      </>
    );
  };

  _adItem = (ads, i, category, loading = true) => {
    const { activeLearn } = this.props;
    if (loading && category !== "collaboration") {
      const skeleton =
        category === "learn" ? Layout.item(activeLearn) : Layout.item(category);
      return (
        <View
          key={`${Math.random()}`}
          style={{
            borderRadius: 6,
            marginRight: WP4,
            width: WP40,
            paddingVertical: WP2,
            ...skeleton.wrapperStyle,
          }}
        >
          <SkeletonContent
            containerStyle={{
              ...Layout.container,
              alignItems: category === "artist" ? "center" : "flex-start",
            }}
            layout={skeleton.layout}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          />
        </View>
      );
    }

    return (
      <TouchableOpacity
        disabled={loading}
        style={{ paddingVertical: WP2 }}
        key={`${i}${Math.random()}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={this._onPressItem(ads)}
      >
        {category === "soundconnect" && (
          <SessionItemV2 ads={ads} loading={loading} />
        )}
        {category === "learn" && activeLearn === "podcast" && (
          <SoundplayItemV2 ads={ads} loading={loading} />
        )}
        {category === "learn" && activeLearn === "video" && (
          <VideoItemV2 ads={ads} loading={loading} />
        )}
        {(category === "submission" || category === "event_audition") && (
          <SubmissionItemV2 ads={ads} loading={loading} />
        )}
        {category === "collaboration" && (
          <CollaborationItemV2 ads={ads} loading={loading} />
        )}
        {category === "event" && <EventItem ads={ads} loading={loading} />}
      </TouchableOpacity>
    );
  };

  _adListGroup = () => {
    const { category, adList, loading = true, activeLearn } = this.props;

    const bgColor = [WHITE, WHITE],
      dummyData = category == "news" ? [1, 2, 3, 4] : [1, 2, 3],
      data = loading
        ? dummyData
        : category == "learn"
        ? adList[activeLearn]
        : adList;

    return (
      <View style={{ borderTopColor: PALE_BLUE, borderTopWidth: 1 }}>
        <LinearGradient
          colors={bgColor}
          start={[0, 0]}
          end={[0, 1]}
          style={{
            paddingVertical: WP6,
          }}
        >
          {this._renderHeader()}

          {category == "learn" && this._renderHeaderLearn()}

          <FlatList
            bounces={false}
            bouncesZoom={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            horizontal={adsConfig[category].horizontal}
            numColumns={adsConfig[category].numColumns}
            style={{ flexGrow: 0 }}
            contentContainerStyle={{
              paddingLeft: WP4,
              paddingTop: WP2,
            }}
            data={data}
            extraData={data}
            renderItem={({ item, index }) =>
              this._adItem(item, index, category, loading)
            }
          />
        </LinearGradient>
      </View>
    );
  };

  _renderHeaderLearn = () => {
    const { adList, activeLearn, onSelectCategory, loading } = this.props;

    const _itemButton = (categoryLearn, title) => {
      const isActive = categoryLearn == activeLearn;
      return (
        <SkeletonContent
          containerStyle={{ backgroundColor: NO_COLOR }}
          layout={[
            {
              width: WP20,
              height: WP8,
              marginRight: WP2,
              borderRadius: 6,
            },
          ]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          {!loading && (
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onPress={() => onSelectCategory(categoryLearn)}
              style={{
                paddingHorizontal: WP305,
                paddingVertical: WP2,
                marginRight: WP2,
                justifyContent: "center",
                alignItems: "center",
                borderRadius: 6,
                backgroundColor: isActive ? REDDISH : PALE_SALMON,
                minWidth: WP20,
              }}
            >
              <Text
                type="Circular"
                size="mini"
                color={isActive ? PALE_WHITE : REDDISH}
                weight={isActive ? 500 : 400}
              >
                {title}
              </Text>
            </TouchableOpacity>
          )}
        </SkeletonContent>
      );
    };
    return (
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          paddingTop: WP4,
          paddingHorizontal: WP4,
        }}
      >
        {(adList["podcast"].length > 0 || loading) &&
          _itemButton("podcast", "Podcast")}
        {(adList["video"].length > 0 || loading) &&
          _itemButton("video", "Video")}
      </View>
    );
  };

  render() {
    const { adList, loading, category } = this.props;
    if (
      (category === "learn"
        ? !isEmpty(adList["podcast"]) || !isEmpty(adList["video"])
        : !isEmpty(adList)) ||
      loading
    ) {
      return this._adListGroup();
    }
    return <View />;
  }
}

HomeSection.propTypes = {
  category: PropTypes.string,
  adList: PropTypes.any,
  availableAd: PropTypes.func,
};

HomeSection.defaultProps = {
  category: "",
  adList: [],
  availableAd: noop,
};

export default memo(HomeSection);
