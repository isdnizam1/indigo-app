import React, { Component } from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import SkeletonContent from "react-native-skeleton-content";
import { WP100, WP2, WP3, WP4, WP6, WP90 } from "../../constants/Sizes";
// import { SHADOW_STYLE } from '../../constants/Styles'
import {
  GREY_CALM_SEMI,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  REDDISH,
  WHITE_MILK,
  NAVY_DARK,
} from "../../constants/Colors";
import Image from "../Image";
import Text from "../Text";
import ModalMessageView from "../ModalMessage/ModalMessageView";
import {
  getShowModalOnboarding1000Startup,
  setShowModalOnboarding1000Startup,
} from "../../utils/storage";
import { isEmpty } from "lodash-es";

class AnnouncementSectionV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeSlider: 0,
      modalIsVisible: false,
      showModalOnboarding: true,
    };
    // this._getShowModalOnboarding1000Startup = this._getShowModalOnboarding1000Startup.bind(this)
  }

  componentDidMount() {
    // this._getShowModalOnboarding1000Startup()
  }

  // _getShowModalOnboarding1000Startup() {
  //   getShowModalOnboarding1000Startup().then((value) => {
  //     if (JSON.parse(value) === null) {
  //       this.setState({ showModalOnboarding: true })
  //     } else if (JSON.parse(value) === true) {
  //       this.setState({ showModalOnboarding: true })
  //     } else if (JSON.parse(value) === false) {
  //       this.setState({ showModalOnboarding: false })
  //     }
  //   })
  // }

  _renderItem = ({ item, index }) => {
    const { loading, navigateTo, navigateBack } = this.props;
    const wrapperStyle = {
      width: WP90,
      borderRadius: 6,
      marginRight: WP2,
      marginBottom: WP4,
      // ...SHADOW_STYLE['shadowThin']
    };

    if (loading) {
      return (
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[
            {
              width: WP90,
              borderRadius: 6,
              marginRight: WP2,
              marginBottom: WP4,
              height: (WP90 * 9) / 16,
            },
          ]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
      );
    }
    return (
      <>
        <View style={wrapperStyle} key={`${index}-announcement`}>
          <View
            style={{
              borderRadius: 6,
              overflow: "hidden",
              backgroundColor: WHITE_MILK,
              height: (WP90 * 9) / 16,
            }}
          >
            {!loading && (
              <Image
                centered
                onPress={async () => {
                  if (item.title === "1000 Startup Portal") {
                    // setShowModalOnboarding1000Startup(JSON.stringify(true))
                    // this._getShowModalOnboarding1000Startup()

                    if (!this.props.idUser) {
                      this.props.navigation.navigate("AuthNavigator", {
                        // screen: 'LoginScreen',
                        screen: "LoginEmailScreen",
                        params: {
                          forceReload: true,
                        },
                      });
                    } else {
                      // if (this.state.showModalOnboarding === true) {
                      //   this.setState({ modalIsVisible: true })
                      // } else {
                      navigateTo("HomeStartupScreen");
                      // }
                    }
                  } else {
                    navigateTo("AdsDetailScreenNoTab", { id_ads: item.id_ads });
                  }
                }}
                source={{ uri: item.image }}
                imageStyle={{ height: (WP90 * 9) / 16, aspectRatio: 16 / 9 }}
              />
            )}
          </View>
        </View>
      </>
    );
  };

  _moveBanner1000StartupToFirst({ list }) {
    list.forEach(function (item, i) {
      if (item.title === "1000 Startup Portal") {
        list.splice(i, 1);
        list.unshift(item);
      }
    });
    return list;
  }
  render() {
    const { list, loading, navigateTo } = this.props;

    const { activeSlider } = this.state;
    const { modalIsVisible } = this.state;
    const dummyData = [1, 2, 3, 4];
    let data = loading ? dummyData : list;
    data = this._moveBanner1000StartupToFirst({ list: data });

    return (
      <>
        {!isEmpty(data) && (
          <View style={{ paddingBottom: WP6, paddingTop: WP2 }}>
            {/* <View
            style={{ paddingHorizontal: WP4, paddingTop: WP2, paddingBottom: WP3 }}
          >
            <Text type='Circular' size='small' color={NAVY_DARK} weight={600}>
              Portal
            </Text>
          </View> */}
            <Carousel
              onSnapToItem={(i) => this.setState({ activeSlider: i })}
              data={data}
              firstItem={activeSlider}
              inactiveSlideScale={1}
              activeSlideAlignment="start"
              sliderWidth={WP100}
              itemWidth={WP90 + WP2}
              containerCustomStyle={{ paddingLeft: WP4 }}
              extraData={this.state}
              renderItem={this._renderItem}
            />
            <Pagination
              dotsLength={data.length}
              activeDotIndex={activeSlider}
              dotColor={loading ? GREY_CALM_SEMI : REDDISH}
              inactiveDotColor={GREY_CALM_SEMI}
              inactiveDotScale={1}
              containerStyle={{
                justifyContent: "flex-start",
                paddingVertical: 0,
                paddingLeft: WP4 + 1,
                paddingRight: 0,
              }}
              dotContainerStyle={{ marginLeft: 1 }}
              dotStyle={{
                width: WP4,
                height: WP2,
                borderRadius: WP2 / 2,
              }}
              inactiveDotStyle={{
                width: WP2,
                height: WP2,
                borderRadius: WP2 / 2,
              }}
            />
          </View>
        )}
        {/* <ModalMessageView
          aspectRatio={124 / 26}
          fullImage={true}
          toggleModal={() => this.setState({ modalIsVisible: !modalIsVisible })}
          imageStyle={{ width: 150, marginTop: 33 }}
          isVisible={modalIsVisible}
          image={require('../../assets/images/sfHorizontalPrimaryBrandmark.png')}
          buttonPrimaryAction={() => {
            setShowModalOnboarding1000Startup(JSON.stringify(false)).then(
              (response) => {
                this._getShowModalOnboarding1000Startup()
                navigateTo('HomeStartupScreen')
              },
            )
            this.setState({ modalIsVisible: false })
          }}
          titleSize={'extraMassive'}
          buttonPrimaryTextWeight={700}
          buttonPrimaryTextSize={'small'}
          buttonPrimaryBgColor={REDDISH}
          buttonPrimaryText={'Let’s start!'}
          title={'Welcome,\nFounders!'}
          subtitle={
            'Mari kembangkan kapasitas diri dan\njuga pengetahuan mengenai Startup\nagar kamu bisa berkontribusi secara\nnyata melalui aplikasi digital!'
          }
        /> */}
      </>
    );
  }
}

AnnouncementSectionV2.propTypes = {
  list: PropTypes.arrayOf(PropTypes.any),
};

AnnouncementSectionV2.defaultProps = {
  list: [],
};

export default AnnouncementSectionV2;
