import React from 'react'
import moment from 'moment'
import { View } from 'react-native-animatable'
import { WP05, WP105, WP2, WP70 } from '../../../constants/Sizes'
import Text from '../../Text'
import { SHIP_GREY_CALM } from '../../../constants/Colors'
import HomeAdsItem from './HomeAdsItem'

const VideoItemV2 = ({ ads, loading = true }) => {
  const additionalData = loading ? {} : JSON.parse(ads.additional_data),
    title = loading ? 'Video Title' : ads.title,
    date = loading ? 'Tanggal' : moment(ads.created_at).format('DD MMM YYYY'),
    duration = loading ? '00:00' : additionalData.video_duration
  return (
    <HomeAdsItem
      loading={loading}
      title={title}
      subtitle={() => (
        <View
          style={{
            marginTop: WP105,
            flexDirection: 'row',
            alignItems: 'center',
          }}
        >
          <Text
            numberOfLines={1}
            ellipsizeMode='tail'
            type='Circular'
            size='xmini'
            color={SHIP_GREY_CALM}
            weight={300}
          >
            {date}
          </Text>
          <View
            style={{
              width: WP05,
              height: WP05,
              borderRadius: WP05 / 2,
              backgroundColor: SHIP_GREY_CALM,
              marginHorizontal: WP2,
            }}
          />
          <Text
            numberOfLines={1}
            ellipsizeMode='tail'
            type='Circular'
            size='xmini'
            color={SHIP_GREY_CALM}
            weight={300}
          >
            {duration}
          </Text>
        </View>
      )}
      image={ads.image}
      imageWidth={WP70}
      imageHeight={(WP70 * 9) / 16}
      aspectRatio={16 / 9}
    />
  )
}

export default VideoItemV2
