import { isFunction, upperFirst } from 'lodash-es'
import React from 'react'
import { View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import {
  GUN_METAL,
  SHIP_GREY_CALM,
  TOMATO,
  WHITE,
  WHITE_MILK,
  PALE_LIGHT_BLUE_TWO,
  PALE_WHITE,
} from '../../../constants/Colors'
import { WP1, WP105, WP2, WP4, WP40 } from '../../../constants/Sizes'
import Image from '../../Image'
import Text from '../../Text'

const HomeAdsItem = ({
  title,
  titleLine = 2,
  subtitle,
  subtitleLine = 1,
  image,
  imageWidth = WP40,
  imageHeight = WP40,
  aspectRatio = 1,
  radius = 6,
  label,
  loading,
  labelColor = [TOMATO, TOMATO],
  labelBottom,
  labelBottomColor = [PALE_LIGHT_BLUE_TWO, PALE_LIGHT_BLUE_TWO],
}) => {
  const titleAds = loading ? 'Title' : title,
    subtitleAds = subtitle,
    labelAds = label

  return (
    <View
      style={{
        marginRight: WP4,
        width: imageWidth,
      }}
    >
      <View
        style={{
          width: imageWidth,
          height: imageHeight,
          backgroundColor: WHITE_MILK,
          borderRadius: radius,
        }}
      >
        {!loading && (
          <View>
            <View
              style={{
                borderRadius: radius,
                overflow: 'hidden',
              }}
            >
              <Image
                source={{ uri: image }}
                imageStyle={{ height: imageHeight, aspectRatio }}
              />
              {labelAds && (
                <View
                  style={{
                    position: 'absolute',
                    alignSelf: 'center',
                    top: 0,
                    left: 0,
                  }}
                >
                  <View
                    style={{
                      overflow: 'hidden',
                      borderBottomRightRadius: 6,
                    }}
                  >
                    <LinearGradient
                      colors={labelColor}
                      start={[1, 0]}
                      end={[0, 0]}
                      style={{
                        paddingHorizontal: WP105,
                        paddingVertical: WP1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Text
                        type='Circular'
                        size='tiny'
                        color={WHITE}
                        weight={500}
                        centered
                      >
                        {labelAds}
                      </Text>
                    </LinearGradient>
                  </View>
                </View>
              )}
              {labelBottom && (
                <View
                  style={{
                    position: 'absolute',
                    alignSelf: 'center',
                    bottom: 0,
                    left: 0,
                  }}
                >
                  <View
                    style={{
                      overflow: 'hidden',
                      borderTopEndRadius: 6,
                    }}
                  >
                    <LinearGradient
                      colors={labelBottomColor}
                      start={[1, 0]}
                      end={[0, 0]}
                      style={{
                        paddingHorizontal: WP105,
                        paddingVertical: WP1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Text
                        type='Circular'
                        size='tiny'
                        color={PALE_WHITE}
                        weight={500}
                        centered
                      >
                        {labelBottom}
                      </Text>
                    </LinearGradient>
                  </View>
                </View>
              )}
            </View>
          </View>
        )}
      </View>
      <View>
        <Text
          numberOfLines={titleLine}
          type='Circular'
          size='mini'
          color={GUN_METAL}
          weight={500}
          style={{ marginTop: WP2 }}
        >
          {upperFirst(titleAds)}
        </Text>
        {subtitleAds &&
          (isFunction(subtitleAds) ? (
            subtitleAds()
          ) : (
            <Text
              numberOfLines={subtitleLine}
              ellipsizeMode='tail'
              type='Circular'
              size='xmini'
              color={SHIP_GREY_CALM}
              weight={300}
              style={{ marginTop: WP105 }}
            >
              {upperFirst(subtitleAds)}
            </Text>
          ))}
      </View>
    </View>
  )
}

export default HomeAdsItem
