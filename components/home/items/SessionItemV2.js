import React from 'react'
import { speakerDisplayName } from '../../../utils/transformation'
import HomeAdsItem from './HomeAdsItem'

const SessionItemV2 = ({ ads, loading = true }) => {
  const title = loading ? 'Soundfren Connect Title' : ads.title,
    speaker = loading ? 'Speaker Name' : speakerDisplayName(ads.speaker),
    seats = loading
      ? 'Kursi habis'
      : `${ads.available_seats > 10 ? '10+' : ads.available_seats} kursi lagi`
  return (
    <HomeAdsItem
      loading={loading}
      title={title}
      subtitle={speaker}
      label={
        ads?.status == 'expired'
          ? null
          : ads.available_seats == 0
          ? 'Full Booked'
          : seats
      }
      image={ads.image}
      labelBottom={ads?.status == 'expired' ? 'Podcast Tersedia' : null}
    />
  )
}

export default SessionItemV2
