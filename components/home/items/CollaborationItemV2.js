import React from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { isEmpty } from "lodash-es";
import SkeletonContent from "react-native-skeleton-content";
import { connect } from "react-redux";
import {
  GUN_METAL,
  PALE_GREY,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
  WHITE_MILK,
} from "../../../constants/Colors";
import {
  WP05,
  WP1,
  WP100,
  WP105,
  WP2,
  WP3,
  WP4,
  WP6,
  WP75,
  WP8,
} from "../../../constants/Sizes";
import { SHADOW_STYLE } from "../../../constants/Styles";
import Avatar from "../../Avatar";
import Icon from "../../Icon";
import Text from "../../Text";
import HomeSkeletonLayout from "../HomeSkeletonLayout";

const CollaborationItemV2 = ({ ads, loading = true, deletedCollabIds }) => {
  const additionalData = loading ? {} : JSON.parse(ads.additional_data),
    title = loading ? "Collaboration Title" : ads.title,
    image = ads?.image,
    createdBy = loading ? "Created By" : ads?.created_by,
    authorJobTitle = loading ? "Job Title" : ads?.job_title,
    authorCityName = !isEmpty(additionalData.location)
      ? additionalData.location.name
      : "Location",
    daysUntil = loading ? "1 Menit Lalu" : ads?.time_ago,
    // daysUntil = !isEmpty(additionalData.date) ? `${daysRemaining(additionalData.date.end)} hari lagi` : '0 hari lagi',
    dataProfession = loading
      ? [1, 2, 3]
      : additionalData.profession !== undefined
      ? additionalData.profession.slice(0, 3)
      : [];

  return deletedCollabIds.indexOf(ads.id_ads) >= 0 ? (
    <View />
  ) : (
    <View
      style={{
        width: WP100 - WP8,
        marginRight: WP4,
        marginLeft: 1,
        borderRadius: 12,
        backgroundColor: WHITE,
        ...SHADOW_STYLE["shadowThin"],
      }}
    >
      <View
        style={{
          borderRadius: 12,
          overflow: "hidden",
        }}
      >
        {/* profile section */}
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            alignItems: "flex-start",
            justifyContent: "space-between",
            paddingHorizontal: WP4,
            paddingTop: WP4,
            paddingBottom: WP2,
          }}
        >
          <View
            style={{ flex: 1, flexDirection: "row", alignItems: "flex-start" }}
          >
            <View style={{ marginRight: WP3 }}>
              <Avatar image={image} isLoading={loading} />
            </View>
            <SkeletonContent
              containerStyle={{ flex: 1 }}
              layout={[
                HomeSkeletonLayout.layout.title,
                HomeSkeletonLayout.layout.subtitle,
                HomeSkeletonLayout.layout.subtitle,
              ]}
              isLoading={loading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View>
                <Text
                  numberOfLines={1}
                  type="Circular"
                  color={GUN_METAL}
                  size="mini"
                  weight={500}
                  style={{ marginBottom: WP05 }}
                >
                  {createdBy}
                </Text>
                <Text
                  numberOfLines={1}
                  type="Circular"
                  color={SHIP_GREY_CALM}
                  size="xmini"
                  weight={300}
                  style={{ marginBottom: WP05 }}
                >
                  {authorJobTitle}
                </Text>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    type="Circular"
                    size="xmini"
                    color={SHIP_GREY_CALM}
                    weight={300}
                  >
                    {authorCityName}
                  </Text>
                  <View
                    style={{
                      width: WP05,
                      height: WP05,
                      borderRadius: WP05 / 2,
                      backgroundColor: SHIP_GREY_CALM,
                      marginHorizontal: WP2,
                    }}
                  />
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    type="Circular"
                    size="xmini"
                    color={SHIP_GREY_CALM}
                    weight={300}
                  >
                    {daysUntil}
                  </Text>
                </View>
              </View>
            </SkeletonContent>
          </View>
          <View
            style={{
              backgroundColor: REDDISH,
              paddingHorizontal: WP4,
              paddingVertical: WP1 + 2,
              borderRadius: 6,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              numberOfLines={1}
              type="Circular"
              size="xmini"
              color={WHITE}
              weight={400}
              centered
            >
              {"Join"}
            </Text>
          </View>
        </View>

        {/* description section */}
        <View
          style={{
            paddingHorizontal: WP4,
            paddingTop: WP2,
            paddingBottom: WP4,
          }}
        >
          <SkeletonContent
            containerStyle={{ flex: 1 }}
            layout={[
              HomeSkeletonLayout.layout.textContent(WP75),
              { ...HomeSkeletonLayout.layout.textContent(), marginBottom: 0 },
            ]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <Text
              numberOfLines={3}
              type="Circular"
              color={SHIP_GREY}
              size="mini"
            >
              {title}
            </Text>
          </SkeletonContent>
        </View>

        {/* profession section*/}
        <View
          style={{
            borderTopWidth: 1,
            width: "100%",
            borderTopColor: PALE_GREY,
            paddingVertical: WP3,
            paddingHorizontal: WP4,
          }}
        >
          <ScrollView
            horizontal
            bounces={false}
            bouncesZoom={false}
            showsHorizontalScrollIndicator={false}
          >
            {dataProfession?.map((item, index) => {
              if (loading) {
                return (
                  <SkeletonContent
                    key={index}
                    containerStyle={{ flex: 1 }}
                    layout={[HomeSkeletonLayout.layout.profesi]}
                    isLoading={loading}
                    boneColor={SKELETON_COLOR}
                    highlightColor={SKELETON_HIGHLIGHT}
                  />
                );
              }
              return (
                <View key={index} style={styles.btnProfession}>
                  <Text
                    type="Circular"
                    color={SHIP_GREY_CALM}
                    size="xmini"
                    weight={400}
                  >
                    {index === 2 && additionalData.profession.length > 3
                      ? "More"
                      : item.name}
                  </Text>
                </View>
              );
            })}
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnProfession: {
    borderRadius: 6,
    paddingVertical: WP1,
    paddingHorizontal: WP105,
    marginRight: WP105,
    backgroundColor: PALE_GREY,
  },
});

export default connect(
  ({ helper: { deletedCollabIds } }) => ({
    deletedCollabIds,
  }),
  {}
)(CollaborationItemV2);
