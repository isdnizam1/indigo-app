import React from 'react'
import { View } from 'react-native-animatable'
import { WP2, WP44 } from '../../../constants/Sizes'
import HomeAdsItem from './HomeAdsItem'

const NewsItemV2 = ({ ads, loading = true }) => {
  const title = loading ? 'News Title' : ads.title,
    subtitle = loading ? 'duration read' : ads.mins_read
  return (
    <View style={{ paddingBottom: WP2 }}>
      <HomeAdsItem
        loading={loading}
        title={title}
        subtitle={subtitle}
        imageWidth={WP44}
        imageHeight={(WP44 * 116) / 156}
        aspectRatio={156 / 116}
        image={ads.image}
      />
    </View>
  )
}

export default NewsItemV2
