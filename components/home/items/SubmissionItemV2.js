import React from 'react'
import { TOMATO } from '../../../constants/Colors'
import HomeAdsItem from './HomeAdsItem'

const SubmissionItemV2 = ({ ads, loading = true }) => {
  const
    title = loading ? 'Event & Audition Title' : ads.title,
    days = loading ? '' : ads.category == 'event' ? 'Events' : ads?.day_left == 0 ? 'Hari Terakhir' : `${ads?.day_left} hari lagi`,
    labelColor = loading ? [] : ads?.category == 'event' ? ['rgb(20, 158, 142)', 'rgb(66, 223, 144)'] : [TOMATO, TOMATO]
    return (
    <HomeAdsItem
      loading={loading}
      title={title}
      label={ads.category == 'submission' && ads?.status == 'expired' ? null : days}
      image={ads.image}
      labelColor={labelColor}
      labelBottom={ads?.status == 'expired' ? 'Telah Berakhir' : null}
    />
  )
}

export default SubmissionItemV2
