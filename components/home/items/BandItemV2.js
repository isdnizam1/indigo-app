import React from 'react'
import { View } from 'react-native'
import moment from 'moment'
import { WP1, WP105, WP2, WP3, WP305, WP4, WP40 } from '../../../constants/Sizes'
import Image from '../../Image'
import Text from '../../Text'
import { CLEAR_BLUE, GUN_METAL, SHIP_GREY_CALM, WHITE, WHITE_MILK } from '../../../constants/Colors'

const BandItem = ({ ads: artist, loading = true }) => {
  const title = loading ? 'Artist Name' : artist.full_name
  const genre = loading ? 'Genre' : artist.interest.join(', ')

  return (
    <View style={{
      marginRight: WP4,
      marginLeft: 1,
      width: WP40,
    }}
    >
      <View style={{ height: WP40, width: WP40, backgroundColor: WHITE_MILK, borderRadius: WP40 / 2 }}>
        {
          !loading && (
            <View>
              <View style={{
                borderRadius: WP40 / 2,
                overflow: 'hidden',
              }}
              >
                <Image
                  source={{ uri: artist.profile_picture }}
                  imageStyle={{ height: WP40, aspectRatio: 1 }}
                />
              </View>
              {
                moment().diff(moment(artist.created_at), 'days') <= 3 && (
                  <View
                    style={{
                      position: 'absolute',
                      alignSelf: 'center',
                      bottom: -WP105,
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: CLEAR_BLUE,
                        paddingHorizontal: WP3,
                        paddingVertical: WP1,
                        borderRadius: WP2,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Text type='Circular' size='tiny' color={WHITE} weight={500} centered>NEW</Text>
                    </View>
                  </View>
                )
              }
            </View>
          )
        }
      </View>
      <View style={{ marginBottom: WP1, marginTop: WP305 }}>
        <Text numberOfLines={1} type='Circular' size='mini' color={GUN_METAL} weight={500} centered>{title}</Text>
      </View>
      <Text numberOfLines={1} ellipsizeMode='tail' type='Circular' size='xmini' color={SHIP_GREY_CALM} weight={300} centered>{genre}</Text>
    </View>
  )
}

BandItem.propTypes = {}

BandItem.defaultProps = {}

export default BandItem
