import React from 'react'
import { speakerDisplayName } from '../../../utils/transformation'
import HomeAdsItem from './HomeAdsItem'

const SoundplayItemV2 = ({ ads, loading = true }) => {
  console.log()
  const title = loading ? 'Soundfren Play Title' : ads.title;
  if(loading){
    var speaker = 'Speaker Name';
  }else if(JSON.parse(ads.additional_data).episodeList[0]?.speaker_name){
    var speaker = JSON.parse(ads.additional_data).episodeList[0].speaker_name;
  }else{
    var speaker = 'Speaker Name';
  }
  return (
    <HomeAdsItem
      loading={loading}
      title={title}
      subtitle={speaker}
      image={ads.image}
    />
  )
}

export default SoundplayItemV2
