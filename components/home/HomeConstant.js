export const adsConfig = {
  band: {
    title: "Artist Ssssspotlight",
    subtitle: "Musisi & artist terbaru",
    horizontal: true,
    numColumns: undefined,
  },
  soundconnect: {
    title: "Agenda",
    subtitle: "Lihat agenda aktivitas event kamu disini",
    horizontal: true,
    numColumns: undefined,
  },
  learn: {
    title: "Media Learning",
    subtitle: "Edukasi dari podcasts & videos",
    horizontal: true,
    numColumns: undefined,
  },
  event_audition: {
    title: "Submission",
    subtitle: "Beragam aktivitas Telkom Athon ada disini",
    horizontal: true,
    numColumns: undefined,
  },
  collaboration: {
    title: "Colllaboration",
    subtitle: "Berkolaborasi berkarya bersama",
    horizontal: false,
    numColumns: 1,
  },
  news: {
    title: "Article & News",
    subtitle: "Berita musik pilihan untuk kamu",
    horizontal: false,
    numColumns: 2,
  },
};

export default {
  adsConfig,
};
