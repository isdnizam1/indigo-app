import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { View, TouchableOpacity, StyleSheet } from 'react-native'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { useColorScheme } from 'react-native-appearance'
import { isEmpty } from 'lodash-es'
import { SHIP_GREY, SHIP_GREY_CALM, PALE_BLUE_TWO } from '../constants/Colors'
import { WP1, WP2, WP4 } from '../constants/Sizes'
import { TOUCH_OPACITY } from '../constants/Styles'
import Text from './Text'
import Icon from './Icon'
import RequiredMark from './RequiredMark'

const propsType = {
  timeFormat: PropTypes.string,
  mode: PropTypes.oneOf(['date', 'time', 'datetime']),
  onChangeDate: PropTypes.func,
  required: PropTypes.bool,
  bold: PropTypes.bool,
  label: PropTypes.string,
  value: PropTypes.string,
  style: PropTypes.any,
  labelWeight: PropTypes.number,
  labelColor: PropTypes.string,
  wording: PropTypes.string,
  disabled: PropTypes.bool,
}

const propsDefault = {
  timeFormat: 'hh:mm:ss',
  mode: 'time',
  required: false,
  labelWeight: 400,
  labelColor: SHIP_GREY,
}

const styles = StyleSheet.create({
  input: {
    padding: WP2,
    width: '100%',
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    color: SHIP_GREY_CALM,
    borderRadius: 6,
  }
})

const SelectTime = (props) => {
  const {
    onChangeDate,
    bold,
    label,
    required,
    style,
    labelWeight,
    labelColor,
    wording,
    timeFormat,
    mode,
    disabled
  } = props
  const [isDateTimePickerVisible, setShow] = useState(false)
  const [value, setValue] = useState(props.value)

  useEffect(() => {
    onChangeDate(value)
  }, [value])

  const isDarkMode = () => {
    const colorScheme = useColorScheme()
    return colorScheme == 'dark'
  }

  const _showDateTimePicker = () => setShow(true)

  const _hideDateTimePicker = () => setShow(false)

  const _handleDatePicked = (time) => {
    _hideDateTimePicker()
    const newTime = moment(time).format(timeFormat)
    setValue(newTime)
    onChangeDate(newTime)
  }

  const initialTime = moment(value, timeFormat)
  return (
    <View
      style={[
        {
          marginBottom: WP4,
          flexGrow: 1,
        },
        style
      ]}
    >
      {
        !!label && (
          <View style={{ flexDirection: 'row', marginBottom: WP1 }}>
            <Text type='Circular' size={'xmini'} color={labelColor || SHIP_GREY} weight={bold ? 500 : labelWeight}>{label}</Text>
            {
              required && <RequiredMark />
            }
          </View>
        )
      }

      <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          style={{ flex: 1, marginRight: WP2 }}
          onPress={_showDateTimePicker}
          disabled={disabled}
        >
          <View style={[styles.input, { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }]}>
            <Text type='Circular' size='xmini' weight={300} color={SHIP_GREY_CALM}>{!isEmpty(value) ? moment(value, timeFormat).format(timeFormat) : '12:00'}</Text>
            <Icon
              background='dark-circle'
              size='small'
              color={SHIP_GREY_CALM}
              name={'chevron-down'}
              type={'MaterialCommunityIcons'}
            />
          </View>
        </TouchableOpacity>
      </View>

      <DateTimePicker
        mode={mode}
        date={!isEmpty(value) ? new Date(initialTime) : new Date()}
        isVisible={isDateTimePickerVisible}
        minimumDate={new Date()}
        onConfirm={(time) => _handleDatePicked(time)}
        onCancel={_hideDateTimePicker}
        isDarkModeEnabled={isDarkMode()}
      />
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        {
          !!wording && (
            <View style={{ flexDirection: 'row' }}>
              <Text type='Circular' size='xtiny' color={SHIP_GREY_CALM}>{wording}</Text>
            </View>
          )
        }
      </View>
    </View>
  )
}

SelectTime.propTypes = propsType
SelectTime.defaultProps = propsDefault
export default SelectTime
