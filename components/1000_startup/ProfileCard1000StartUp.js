import { Image, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { PALE_WHITE, REDDISH } from '../../constants/Colors'
import { SHADOW_STYLE } from '../../constants/Styles'
import { Icon, Text } from '../index'
import Avatar from '../Avatar'

const ProgressProfileCard100StartUp = ({
  image,
  name,
  total_point,
  leaderboard_position,
  onPress,
}) => {
  return (
    <View
      style={{
        backgroundColor: PALE_WHITE,
        borderRadius: 10,
        paddingVertical: 8,
        ...SHADOW_STYLE['shadowThin'],
      }}
    >
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: 19,
          paddingTop: 11,
        }}
      >
        <Avatar
          size={'huge'}
          shadow
          image={image}
          style={{
            marginRight: 12,
            overflow: 'hidden',
          }}
        />
        <View style={{ flexDirection: 'column' }}>
          <Text
            type='Circular'
            color={'#212B36'}
            weight={700}
            style={{ fontSize: 16, lineHeight: 18 }}
          >
            {name ? name : 'Title'}
          </Text>
          <View
            style={{
              backgroundColor: '#F4F6F8',
              alignSelf: 'flex-start',
              alignItems: 'center',
              flexDirection: 'row',
              marginVertical: 7,
              paddingVertical: 5,
              paddingHorizontal: 7,
              borderRadius: 6,
            }}
          >
            <Image
              source={require('sf-assets/images/uil_trophy.png')}
              style={{
                height: 16,
                width: 16,
              }}
            />
            <Text
              type='Circular'
              color={'#919EAB'}
              weight={500}
              style={{ fontSize: 12, marginLeft: 5.3 }}
            >
              #{leaderboard_position}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
              source={require('sf-assets/images/uil_point.png')}
              style={{
                height: 20,
                width: 20,
              }}
            />
            <Text
              type='Circular'
              color={'#637381'}
              weight={500}
              style={{ fontSize: 12, marginLeft: 6 }}
            >
              {total_point}
            </Text>
          </View>
        </View>
      </View>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          flex: 1,
          justifyContent: 'flex-end',
          marginRight: 13,
          paddingHorizontal: 5,
        }}
        onPress={onPress}
      >
        <Text
          type='Circular'
          color={REDDISH}
          weight={500}
          style={{ fontSize: 11, alignSelf: 'center', marginBottom: 2 }}
        >
          {' '}
          Ikuti misi sekarang{' '}
        </Text>
        <Icon
          size='large'
          name='keyboard-arrow-right'
          type='MaterialIcons'
          centered
          color={REDDISH}
        />
      </TouchableOpacity>
    </View>
  )
}

export default ProgressProfileCard100StartUp
