import React from 'react'
import PropTypes from 'prop-types'
import { isEmpty, isEqual, merge, get, set } from 'lodash-es'

const propsType = {
  key: PropTypes.any,
  initialValue: PropTypes.object,
  onSubmit: PropTypes.func,
  validation: PropTypes.any,
  onChangeInterceptor: PropTypes.func
}

const propsDefault = {
  initialValue: {},
  onSubmit: () => {
  },
  onChangeInterceptor: () => {
  }
}

class Form extends React.Component {

  constructor(props) {
    super(props)
  }

  state = {
    ...this.props.initialValue,
    isValid: false,
    isDirty: false,
    errors: {},
    avoidScreenUpdate: true
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { optimized } = this.props
    return !nextState.avoidScreenUpdate || !isEqual(nextProps, this.props) || !optimized
  }

  static getDerivedStateFromProps = (props, state) => {
    const { initialValue, validation } = props
    const initialValueTemp = { ...initialValue }
    let isValid = false
    const mergedState = merge(initialValueTemp, state)
    if (validation) isValid = validation.isValidSync(mergedState)
    if(validation && !isValid) validation.validate(mergedState).catch((err) => {
      return { ...mergedState, isValid, errors: err.errors }
    })
    return { ...mergedState, isValid }
  }

  handleChange = (key, multiple = false, index) => (text, replace = false) => {
    const {
      validation,
      optimized
    } = this.props
    this.setState((state) => {
      let currentValue = get(state, key) && multiple ? get(state, key) : multiple ? [] : text
      if (multiple && (!isEmpty(text) || replace || index >= 0)) {
        if (replace) currentValue = text
        else if (index >= 0) currentValue[index] = text
        else currentValue.push(text)
      } else if(multiple) currentValue.push(text)
      const newState = {}
      set(newState, 'isDirty', true)
      set(newState, key, currentValue)
      return newState
    }, () => {
      if (validation) {
        const isValid = validation.isValidSync(this.state)
        if(!isValid) validation.validate(this.state, { abortEarly: false }).catch((yupError) => {
          const errors = {}
          if (yupError.inner) {
            if (yupError.inner.length === 0) errors[yupError.path] = yupError.message
            for (let err of yupError.inner) errors[err.path] = err.message
          }
          this.setState({ isValid, errors }, () => this.props.onChangeInterceptor({ key, text, state: this.state, onChange: this.handleChange }))
        }); else this.setState({ isValid }, () => this.props.onChangeInterceptor({ key, text, state: this.state, onChange: this.handleChange }))
      } else this.props.onChangeInterceptor({ key, text, state: this.state, onChange: this.handleChange })
    })
    if(optimized) {
      try {
        if(typeof this.timeout !== 'undefined') {
          clearTimeout(this.timeout)
        }
      } catch(e) {
        // Silent is gold
      } finally {
        this.timeout = setTimeout(() => {
          this.setState({
            avoidScreenUpdate: false
          }, () => this.setState({ avoidScreenUpdate: true }))
        }, 500)
      }
    }
  }

  handleSubmit = async () => {
    return await this.props.onSubmit({ formData: this.state, isDirty: this.state.isDirty })
  }

  isDirty = () => !isEqual(this.state, this.props.initialValue)

  isDirtyState = () => this.state.isDirty

  render() {
    const {
      children
    } = this.props
    return children({
      onChange: this.handleChange,
      onSubmit: this.handleSubmit,
      formData: this.state,
      isValid: this.state.isValid,
      isDirty: this.state.isDirty,
      errors: this.state.errors
    })
  }
}

Form.propTypes = propsType
Form.defaultProps = propsDefault
export default Form
