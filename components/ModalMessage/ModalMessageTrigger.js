import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'
import Modal from '../Modal'
import ModalMessage from './ModalMessage'

const propsType = {
  triggerComponent: PropTypes.any.isRequired,
  isVisible: PropTypes.bool,
  style: PropTypes.any,
  contentStyle: PropTypes.any,
  image: PropTypes.any,
  aspectRatio: PropTypes.number,
  imageSize: PropTypes.any,
  fullImage: PropTypes.bool,
  customImage: PropTypes.any,
  imageStyle: PropTypes.any,

  titleCustom: PropTypes.any,
  title: PropTypes.string,
  titleSize: PropTypes.string,
  titleType: PropTypes.string,
  titleWeight: PropTypes.number,
  titleColor: PropTypes.string,
  titleStyle: PropTypes.any,
  subtitle: PropTypes.string,
  subtitleSize: PropTypes.string,
  subtitleType: PropTypes.string,
  subtitleWeight: PropTypes.number,
  subtitleColor: PropTypes.string,
  subtitleStyle: PropTypes.any,

  buttonPrimaryText: PropTypes.string,
  buttonPrimaryAction: PropTypes.func,
  buttonPrimaryBgColor: PropTypes.string,
  buttonPrimaryTextColor: PropTypes.string,
  buttonPrimaryTextSize: PropTypes.string,
  buttonPrimaryTextType: PropTypes.string,
  buttonPrimaryTextWeight: PropTypes.number,
  buttonPrimaryStyle: PropTypes.any,
  buttonPrimaryContentStyle: PropTypes.any,
  buttonSecondaryText: PropTypes.string,
  buttonSecondaryAction: PropTypes.func,
  buttonSecondaryTextColor: PropTypes.string,
  buttonSecondaryTextSize: PropTypes.string,
  buttonSecondaryTextType: PropTypes.string,
  buttonSecondaryTextWeight: PropTypes.number,
  buttonSecondaryStyle: PropTypes.any,
  buttonSecondaryTextStyle: PropTypes.any,
}

const ModalMessageTrigger = (props) => {
  const { triggerComponent } = props
  return (
    <Modal
      unstyled
      swipeDirection={null}
      isVisible={false}
      position='center'
      animationIn='fadeIn'
      animationOut='fadeOut'
      style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}
      renderModalContent={({ toggleModal }) => (
        <ModalMessage toggleModal={toggleModal} {...props}/>
      )}
    >
      {({ toggleModal }, M) => (
        <View>
          {triggerComponent(toggleModal)}
          {M}
        </View>
      )}
    </Modal>
  )
}

ModalMessageTrigger.propTypes = propsType

export default ModalMessageTrigger
