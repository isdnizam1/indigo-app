import React from 'react'
import { Modal, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import ModalMessage from './ModalMessage'

const propsType = {
  toggleModal: PropTypes.any.isRequired,
  isVisible: PropTypes.bool.isRequired,
  isCancel: PropTypes.bool,
  style: PropTypes.any,
  contentStyle: PropTypes.any,
  image: PropTypes.any,
  aspectRatio: PropTypes.number,
  imageSize: PropTypes.any,
  fullImage: PropTypes.bool,
  customImage: PropTypes.any,
  imageStyle: PropTypes.any,

  titleCustom: PropTypes.any,
  title: PropTypes.string,
  titleSize: PropTypes.string,
  titleType: PropTypes.string,
  titleWeight: PropTypes.number,
  titleLineHeight: PropTypes.number,
  titleColor: PropTypes.string,
  titleStyle: PropTypes.any,
  subtitle: PropTypes.string,
  subtitleSize: PropTypes.string,
  subtitleType: PropTypes.string,
  subtitleWeight: PropTypes.number,
  subtitleLineHeight: PropTypes.number,
  subtitleColor: PropTypes.string,
  subtitleStyle: PropTypes.any,

  buttonPrimaryText: PropTypes.string,
  buttonPrimaryAction: PropTypes.func,
  buttonPrimaryBgColor: PropTypes.string,
  buttonPrimaryTextColor: PropTypes.string,
  buttonPrimaryTextSize: PropTypes.string,
  buttonPrimaryTextType: PropTypes.string,
  buttonPrimaryTextWeight: PropTypes.number,
  buttonPrimaryStyle: PropTypes.any,
  buttonPrimaryContentStyle: PropTypes.any,
  buttonSecondaryText: PropTypes.string,
  buttonSecondaryAction: PropTypes.func,
  buttonSecondaryTextColor: PropTypes.string,
  buttonSecondaryTextSize: PropTypes.string,
  buttonSecondaryTextType: PropTypes.string,
  buttonSecondaryTextWeight: PropTypes.number,
  buttonSecondaryStyle: PropTypes.any,
  buttonSecondaryTextStyle: PropTypes.any,
  customFooter: PropTypes.any,
}

const ModalMessageView = (props) => {
  const { toggleModal, isVisible, isCancel = false } = props
  return (
    <Modal
      transparent
      onRequestClose={isCancel ? () => toggleModal : () => { }}
      animationType='fade'
      visible={isVisible}
    >
      <TouchableOpacity activeOpacity={1} onPress={toggleModal} disabled={!isCancel} style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.75)', justifyContent: 'center', alignItems: 'center', }}>
        <ModalMessage toggleModal={toggleModal} {...props} />
      </TouchableOpacity>
    </Modal>
  )
}

ModalMessageView.propTypes = propsType

export default ModalMessageView
