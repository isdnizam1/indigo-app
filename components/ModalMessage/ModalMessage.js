import React, { isValidElement } from "react";
import { View, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import { isEmpty, isFunction } from "lodash-es";
import { WP90, WP6, WP5, WP405 } from "sf-constants/Sizes";
import { GREY, PUMPKIN, WHITE } from "../../constants/Colors";
import { WP105, WP12, WP2, WP3, WP4, WP80 } from "../../constants/Sizes";
import Button from "../Button";
import Image from "../Image";
import Text from "../Text";

const propsType = {
  toggleModal: PropTypes.func,
  style: PropTypes.any,
  contentStyle: PropTypes.any,
  image: PropTypes.any,
  aspectRatio: PropTypes.number,
  imageSize: PropTypes.any,
  fullImage: PropTypes.bool,
  customImage: PropTypes.any,
  imageStyle: PropTypes.any,

  titleCustom: PropTypes.any,
  title: PropTypes.string,
  titleSize: PropTypes.string,
  titleType: PropTypes.string,
  titleWeight: PropTypes.number,
  titleLineHeight: PropTypes.number,
  titleColor: PropTypes.string,
  titleStyle: PropTypes.any,
  subtitle: PropTypes.string,
  subtitleSize: PropTypes.string,
  subtitleType: PropTypes.string,
  subtitleWeight: PropTypes.number,
  subtitleLineHeight: PropTypes.number,
  subtitleColor: PropTypes.string,
  subtitleStyle: PropTypes.any,
  extraSubtitleWeight: PropTypes.number,
  extraSubtitle: PropTypes.string,
  extraComponent: PropTypes.any,
  emailText: PropTypes.string,

  buttonPrimaryText: PropTypes.string,
  buttonPrimaryAction: PropTypes.func,
  buttonPrimaryBgColor: PropTypes.string,
  buttonPrimaryTextColor: PropTypes.string,
  buttonPrimaryTextSize: PropTypes.string,
  buttonPrimaryTextType: PropTypes.string,
  buttonPrimaryTextWeight: PropTypes.number,
  buttonPrimaryStyle: PropTypes.any,
  buttonPrimaryContentStyle: PropTypes.any,
  buttonSecondaryText: PropTypes.string,
  buttonSecondaryAction: PropTypes.func,
  buttonSecondaryTextColor: PropTypes.string,
  buttonSecondaryTextSize: PropTypes.string,
  buttonSecondaryTextType: PropTypes.string,
  buttonSecondaryTextWeight: PropTypes.number,
  buttonSecondaryStyle: PropTypes.any,
  buttonSecondaryTextStyle: PropTypes.any,
  buttonSecondaryBgColor: PropTypes.any,
};

const ModalMessage = (props) => {
  const {
    toggleModal = () => {},
    style,
    contentStyle,
    image = image,
    aspectRatio = 1,
    imageSize = WP12,
    fullImage = false,
    customImage,
    imageStyle,

    titleCustom,
    title,
    titleSize = "small",
    titleType = "Circular",
    titleWeight = 500,
    titleColor = GREY,
    titleStyle = { marginBottom: WP105 },
    subtitle,
    subtitleSize = "xmini",
    subtitleType = "Circular",
    subtitleWeight = 300,
    subtitleColor = GREY,
    subtitleStyle,
    extraSubtitle,
    extraSubtitleWeight,
    extraComponent,
    emailText,

    buttonPrimaryText,
    buttonPrimaryAction = () => {},
    buttonPrimaryBgColor = PUMPKIN,
    buttonPrimaryTextColor = WHITE,
    buttonPrimaryTextSize = "mini",
    buttonPrimaryTextType = "Circular",
    buttonPrimaryTextWeight = 500,
    buttonPrimaryStyle,
    buttonPrimaryContentStyle,
    buttonSecondaryText,
    buttonSecondaryAction = () => {},
    buttonSecondaryTextColor = GREY,
    buttonSecondaryTextSize = "mini",
    buttonSecondaryTextType = "Circular",
    buttonSecondaryTextWeight = 500,
    buttonSecondaryStyle,
    buttonSecondaryTextStyle,
    titleLineHeight,
    subtitleLineHeight,
    buttonSecondaryBgColor,
    customFooter,
  } = props;
  return props.children ? (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => {}}
      style={[
        {
          alignItems: "center",
          width: WP80,
          justifyContent: "center",
          backgroundColor: WHITE,
          borderRadius: 6,
          overflow: "hidden",
          paddingVertical: WP12,
        },
        style,
      ]}
    >
      {props.children}
    </TouchableOpacity>
  ) : (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => {}}
      style={[
        {
          alignItems: "center",
          width: WP90,
          justifyContent: "center",
          backgroundColor: WHITE,
          borderRadius: 6,
          overflow: "hidden",
        },
        style,
      ]}
    >
      {isValidElement(customImage) && customImage}
      {fullImage && (
        <Image
          source={image}
          imageStyle={[{ width: WP80 }, imageStyle]}
          aspectRatio={aspectRatio}
        />
      )}
      <View
        style={[
          {
            width: "100%",
            paddingTop: fullImage || isValidElement(customImage) ? WP3 : WP6,
            paddingHorizontal: WP6,
            paddingBottom: WP6,
          },
          contentStyle,
        ]}
      >
        {!fullImage && isEmpty(customImage) && image && (
          <Image
            source={image}
            imageStyle={[{ width: imageSize }, imageStyle]}
            aspectRatio={aspectRatio}
            style={{ marginBottom: WP6 }}
          />
        )}
        <View
          style={[
            {
              width: "100%",
              marginBottom: image ? WP6 : WP4,
              alignItems: "center",
            },
          ]}
        >
          {isValidElement(titleCustom) && titleCustom}
          {!isEmpty(title) && (
            <Text
              lineHeight={titleLineHeight}
              centered
              style={titleStyle}
              type={titleType}
              size={titleSize}
              weight={titleWeight}
              color={titleColor}
            >
              {title}
            </Text>
          )}
          {!isEmpty(subtitle) && (
            <Text
              centered
              style={subtitleStyle}
              type={subtitleType}
              size={subtitleSize}
              weight={subtitleWeight}
              lineHeight={subtitleLineHeight}
              color={subtitleColor}
              lineHeight={WP405}
            >
              {subtitle}
            </Text>
          )}
          {!isEmpty(emailText) && (
            <Text
              centered
              style={subtitleStyle}
              type={subtitleType}
              size={subtitleSize}
              weight={subtitleWeight}
              lineHeight={subtitleLineHeight}
              color={buttonPrimaryBgColor}
              lineHeight={WP405}
            >
              {emailText}
            </Text>
          )}
          {!isEmpty(extraSubtitle) && (
            <Text
              centered
              style={subtitleStyle}
              type={subtitleType}
              size={subtitleSize}
              weight={extraSubtitleWeight ?? subtitleWeight}
              lineHeight={subtitleLineHeight}
              color={subtitleColor}
              lineHeight={WP405}
            >
              {extraSubtitle}
            </Text>
          )}
          {!isEmpty(extraComponent) && extraComponent}
        </View>

        {!isEmpty(buttonPrimaryText) && (
          <Button
            style={[
              { alignSelf: "center", width: "100%", marginBottom: WP2 },
              buttonPrimaryStyle,
            ]}
            onPress={() => {
              buttonPrimaryAction();
              if (isFunction(toggleModal)) toggleModal();
            }}
            textColor={buttonPrimaryTextColor}
            marginless
            textSize={buttonPrimaryTextSize}
            textWeight={buttonPrimaryTextWeight}
            textType={buttonPrimaryTextType}
            text={buttonPrimaryText}
            centered
            radius={WP2}
            backgroundColor={buttonPrimaryBgColor}
            contentStyle={[
              { paddingVertical: WP3, paddingHorizontal: WP3 },
              buttonPrimaryContentStyle,
            ]}
            width={"100%"}
            shadow="none"
          />
        )}

        {!isEmpty(buttonSecondaryText) && (
          <TouchableOpacity
            style={[
              { marginTop: WP2, backgroundColor: buttonSecondaryBgColor },
              buttonSecondaryStyle,
            ]}
            onPress={() => {
              buttonSecondaryAction();
              if (isFunction(toggleModal)) toggleModal();
            }}
          >
            <Text
              centered
              style={buttonSecondaryTextStyle}
              type={buttonSecondaryTextType}
              weight={buttonSecondaryTextWeight}
              size={buttonSecondaryTextSize}
              color={buttonSecondaryTextColor}
            >
              {buttonSecondaryText}
            </Text>
          </TouchableOpacity>
        )}
      </View>
      {isValidElement(customFooter) && customFooter}
    </TouchableOpacity>
  );
};

ModalMessage.propTypes = propsType;

export default ModalMessage;
