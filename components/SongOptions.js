import React, { Component } from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import { isObject, upperFirst, isUndefined, isFunction, noop } from 'lodash-es'
import MenuOptions from 'sf-components/MenuOptions'
import Text from 'sf-components/Text'
import Icon from 'sf-components/Icon'
import { isEqualObject } from 'sf-utils/helper'
import PropTypes from 'prop-types'
import {
  postFeedLike,
  postProfileFollow,
  postProfileUnfollow,
  postProfileDelJourney,
} from 'sf-actions/api'
import {
  REDDISH,
  SHIP_GREY_CALM,
  GUN_METAL,
  NO_COLOR,
  ORANGE_BRIGHT,
  PALE_BLUE_TWO,
} from 'sf-constants/Colors'
import { WP3, WP4, WP5, WP6 } from 'sf-constants/Sizes'
import styles from 'sf-styles/components/Song'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'

class SongOptions extends Component {
  static propTypes = {
    song: PropTypes.object,
    add: PropTypes.object,
    isProfileScreen: PropTypes.bool,
    isMine: PropTypes.bool,
    navigateTo: PropTypes.func,
    refreshData: PropTypes.func,
  };

  static defaultProps = {
    song: {},
    add: {},
    isProfileScreen: false,
    isMine: false,
    navigateTo: () => {},
    refreshData: () => {},
  };

  constructor(props) {
    super(props)
    this.state = {
      song: props.song,
      refreshOnClose: false,
      deleteModalVisible: false,
    }
    this._cancelDelete = this._cancelDelete.bind(this)
    this._confirmDelete = this._confirmDelete.bind(this)
  }

  _cancelDelete = () => {
    this.setState({
      deleteModalVisible: false,
    })
  };

  _confirmDelete = () => {
    const {
      refreshData,
      song: { id_journey },
    } = this.props
    this.setState(
      {
        deleteModalVisible: false,
      },
      () => {
        postProfileDelJourney({
          id_journey,
        }).then(isFunction(refreshData) ? refreshData : noop)
      },
    )
  };

  shouldComponentUpdate(nextProps, nextState) {
    const shouldUpdate = !isEqualObject(nextState, this.state)
    return shouldUpdate
  }

  _postSongLike = async (idJourney) => {
    const {
      userData: { id_user },
    } = this.props

    const { song } = this.state

    this.setState({
      song: {
        ...song,
        is_liked_by_viewer: song.is_liked_by_viewer == 0 ? 1 : 0,
      },
    })

    try {
      const params = {
        related_to: 'id_journey',
        id_related_to: idJourney,
        id_user,
      }
      await postFeedLike(params)
    } catch (error) {
      //
    }
  };

  _onFollow = async () => {
    const { userData } = this.props
    const { song } = this.state
    const isFollowAction = song.is_followed_by_viewer == 0
    this.setState({
      song: {
        ...song,
        is_followed_by_viewer: isFollowAction ? 1 : 0,
      },
      refreshOnClose: true,
    })
    try {
      const params = {
        [isFollowAction ? 'id_user' : 'id_user_following']: song.id_user,
        [isFollowAction ? 'followed_by' : 'id_user']: userData.id_user,
      }
      await (isFollowAction ? postProfileFollow : postProfileUnfollow)(params)
    } catch (error) {
      //
    }
  };

  _triggerModal = (toggleModal) => (
    <TouchableOpacity
      activeOpacity={TOUCH_OPACITY}
      onPress={toggleModal}
      style={styles.dotsWrapper}
    >
      <Icon
        background='dark-circle'
        size='mini'
        color={SHIP_GREY_CALM}
        name='dots-three-horizontal'
        type='Entypo'
        centered
      />
    </TouchableOpacity>
  );

  render() {
    let { isProfileScreen, isMine, userData, refreshData, navigateTo } = this.props
    if (isUndefined(isProfileScreen)) isProfileScreen = false
    if (isUndefined(isMine)) isMine = false
    const { song, refreshOnClose, deleteModalVisible } = this.state
    const add = isObject(song.additional_data)
        ? song.additional_data
        : JSON.parse(song.additional_data),
      image = add.cover_image,
      title = upperFirst(song.title),
      artistName = add.artist_name,
      isLiked = song?.is_liked_by_viewer == 1,
      isFollowed = song?.is_followed_by_viewer == 1
    const menuOptions = [
      {
        onPress: () => this._postSongLike(Number(song?.id_journey)),
        title: 'Like',
        iconName: isLiked ? 'heart' : 'heart-outline',
        iconColor: isLiked ? REDDISH : SHIP_GREY_CALM,
        iconSize: 'huge',
        not_close: true,
      },
      {
        onPress: () =>
          navigateTo('ProfileScreenNoTab', {
            idUser: song?.id_user,
            navigateBackOnDone: true,
          }),
        title: 'Go to Artist Profile',
        iconName: 'account',
        iconColor: SHIP_GREY_CALM,
        iconSize: 'huge',
        validation: !isProfileScreen,
      },
      {
        onPress: () =>
          navigateTo('SongFileForm', {
            id: song?.id_journey,
            refreshProfile: refreshData,
          }),
        title: 'Edit',
        iconName: 'pencil',
        iconColor: SHIP_GREY_CALM,
        iconSize: 'huge',
        validation: isProfileScreen && isMine && parseInt(add.in_app_player) == 1,
      },
      {
        onPress: () =>
          this.setState({
            deleteModalVisible: true,
          }),
        title: 'Hapus',
        iconName: 'delete',
        iconColor: SHIP_GREY_CALM,
        iconSize: 'huge',
        validation: isProfileScreen && isMine,
      },
      {
        onPress: () => this._onFollow(),
        title: isFollowed ? 'Following' : 'Follow This Artist',
        titleColor: isFollowed ? REDDISH : GUN_METAL,
        iconName: isFollowed ? 'check-bold' : 'account-plus',
        iconColor: isFollowed ? REDDISH : SHIP_GREY_CALM,
        iconBackground: isFollowed ? 'rgba(255, 101, 31, 0.2)' : NO_COLOR,
        iconSize: isFollowed ? 'mini' : 'huge',
        not_close: true,
        validation: !isMine,
      },
      {
        onPress: () =>
          navigateTo('ShareScreen', {
            id: song?.id_journey,
            type: 'song',
            title: 'Bagikan Lagu',
          }),
        iconName: 'share-variant',
        iconColor: SHIP_GREY_CALM,
        iconSize: 'huge',
        title: 'Share',
      },
      {
        onPress: () => {
          navigateTo('MembershipScreen', {
            paymentSource: 'Song Popup',
          })
        },
        title: 'Upgrade Premium',
        titleWeight: 500,
        titleColor: ORANGE_BRIGHT,
        image: require('../assets/icons/badgeSoundfrenPremium.png'),
        imageSize: WP5,
        imageStyle: { alignSelf: 'flex-start' },
        isPremiumMenu: true,
        validation: userData.account_type != 'premium',
      },
    ]
    return (
      <View>
        <MenuOptions
          options={menuOptions}
          customHeader={
            <View
              style={[
                styles.firstSong,
                {
                  width: '100%',
                  paddingHorizontal: WP4,
                  paddingBottom: WP6,
                  borderBottomWidth: 1,
                  borderBottomColor: PALE_BLUE_TWO,
                },
              ]}
            >
              <Image style={styles.songCover} source={{ uri: image }} />
              <View style={{ paddingLeft: WP3, flex: 1 }}>
                <Text
                  numberOfLines={1}
                  style={{ flex: 1 }}
                  type={'Circular'}
                  color={GUN_METAL}
                  weight={600}
                  size={'slight'}
                >
                  {title}
                </Text>
                <Text
                  numberOfLines={1}
                  style={{ flex: 1 }}
                  type={'Circular'}
                  color={SHIP_GREY_CALM}
                  weight={300}
                  size={'xmini'}
                >
                  {artistName}
                </Text>
              </View>
            </View>
          }
          triggerComponent={this._triggerModal}
          closableAction={async () => {
            if (!isUndefined(refreshData) && refreshOnClose) await refreshData()
          }}
        />
        <ModalMessageView
          style={styles.modal}
          contentStyle={styles.modalContent}
          toggleModal={this._cancelDelete}
          isVisible={deleteModalVisible}
          title={'Hapus Lagu'}
          titleType='Circular'
          titleSize={'small'}
          titleColor={GUN_METAL}
          titleStyle={styles.modalTitle}
          subtitle={'Apakah kamu yakin ingin menghapus lagu yang telah kamu upload ini?'}
          subtitleType='Circular'
          subtitleSize={'xmini'}
          subtitleWeight={300}
          subtitleLineHeight={WP5}
          subtitleColor={SHIP_GREY_CALM}
          subtitleStyle={styles.modalSubtitle}
          image={null}
          buttonPrimaryText={'Tidak'}
          buttonPrimaryContentStyle={styles.modalButtonPrimary}
          buttonPrimaryTextType='Circular'
          buttonPrimaryTextWeight={400}
          buttonPrimaryBgColor={REDDISH}
          buttonSecondaryText={'Ya'}
          buttonSecondaryStyle={styles.modalButtonSecondary}
          buttonSecondaryTextType='Circular'
          buttonSecondaryTextWeight={400}
          buttonSecondaryTextColor={SHIP_GREY_CALM}
          buttonSecondaryAction={this._confirmDelete}
        />
      </View>
    )
  }
}

export default SongOptions
