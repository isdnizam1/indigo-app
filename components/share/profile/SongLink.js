import React, { useState, useEffect, useCallback } from 'react'
import { TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import axios from 'axios'
import _enhancedNavigation from '../../../navigation/_enhancedNavigation'
import { TOUCH_OPACITY } from '../../../constants/Styles'
import ItemShareDefault from '../ItemShareDefault'

const SongLink = (props) => {

  const { data, navigateTo } = props
  const [remoteImage, setRemoteImage] = useState('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=')
  let { description, additional_data: { url_song } } = data

  const findCover = useCallback(() => {
    axios.get(url_song).then(({ data }) => {
      const regMatch = data.match(/(og|twitter)?:image.+?>/ig)
      if (regMatch && typeof regMatch === 'object' && regMatch.length > 0) {
        // eslint-disable-next-line no-useless-escape
        const urls = regMatch[0].match(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig)
        if (urls && typeof urls === 'object' && urls.length > 0) {
          setRemoteImage(urls[0])
        }
      }
    })
  }, [setRemoteImage])

  const onPress = () => {
    navigateTo('BrowserScreenNoTab', { url: url_song })
  }

  useEffect(() => {
    findCover()
  }, [data])

  return (
    <TouchableOpacity onPress={onPress} activeOpacity={TOUCH_OPACITY}>
      <ItemShareDefault
        image={remoteImage}
        label={'Play Song'}
        title={data.title}
        subtitle={description || 'No description'}
      />
    </TouchableOpacity>
  )

}

const mapStateToProps = () => ({})

export default _enhancedNavigation(connect(mapStateToProps, {})(SongLink))