import React from 'react'
import { TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import _enhancedNavigation from '../../../navigation/_enhancedNavigation'
import { TOUCH_OPACITY } from '../../../constants/Styles'
import ItemShareLarge from '../ItemShareLarge'

const Video = (props) => {

  const { data, navigateTo } = props
  let { title, additional_data: { url_video } } = data

  const isYoutube = () => {
    return url_video.toLowerCase().match(/youtu\.be|youtube\.com/g) != null
  }

  // eslint-disable-next-line no-useless-escape
  const domain = url_video.match(/(([a-z0-9\-]+|[a-z0-9\-]+.\[a-z0-9\-]+)?((\.[a-z]+)|(\.[a-z]+\.[a-z]+)))/g).join('')
  const onPress = () => {
    navigateTo('BrowserScreenNoTab', { url: url_video })
  }

  return (
    <TouchableOpacity onPress={onPress} activeOpacity={TOUCH_OPACITY}>
      <ItemShareLarge
        isVideo={true}
        isLink={true}
        image={url_video}
        title={title}
        subtitle={isYoutube() ? 'youtube.com' : domain}
      />
    </TouchableOpacity>
  )

}

const mapStateToProps = () => ({})

export default _enhancedNavigation(connect(mapStateToProps, {})(Video))