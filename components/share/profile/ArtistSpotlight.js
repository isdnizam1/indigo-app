import React from 'react'
import { TouchableOpacity } from 'react-native'
import ItemShareDefault from '../ItemShareDefault'

class ArtistSpotlight extends React.PureComponent {
  render() {
    const { data, navigateTo } = this.props
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => {
          navigateTo('ProfileScreenNoTab', {
            idUser: data.id_user,
            navigateBackOnDone: true,
          })
        }}
      >
        <ItemShareDefault
          image={data.image}
          imageRound={true}
          label={'Visit Profile'}
          title={data.full_name}
          subtitle={data.interest?.join(', ')}
        />
      </TouchableOpacity>
    )
  }
}

export default ArtistSpotlight
