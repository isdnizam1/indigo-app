import React, { useState, useEffect } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { noop } from 'lodash-es'
import { Loader, Text } from '../..'
import { WHITE, GREY_WARM } from '../../../constants/Colors'
import _enhancedNavigation from '../../../navigation/_enhancedNavigation'
import style from '../../../styles/components/ProfileShareComponent'
import { getProfileDataShare } from '../../../actions/api'
import FeedItemDeleted from '../../feed/FeedItemDeleted'
import VisualArt from './VisualArt'
import Video from './Video'
import Experience from './Experience'
import SongFile from './SongFile'
import SongLink from './SongLink'
import ArtistSpotlight from './ArtistSpotlight'
import Schedule from './Schedule'

const ProfileComponent = (props) => {

  const [data, setData] = useState(null)
  let { dispatch, navigation, navigateTo, userData: { id_user }, id, id_artist, id_schedule, loaderHeight, showTopics } = props

  const getData = async () => {
    const related_to = id_artist ? 'id_user' : id_schedule ? 'id_schedule' : 'id_journey'
    const id_params = id_artist ? id_artist : id_schedule ? id_schedule : id
    const params = {
      id: id_params,
      id_user,
      related_to
    }
    let responseData = await dispatch(getProfileDataShare, params, noop, true, false)
    if (!responseData.result) {
      responseData.empty = true
      setData(responseData)
      return
    }
    responseData = responseData.result
    if (responseData.additional_data && typeof responseData.additional_data === 'string') {
      responseData.additional_data = JSON.parse(responseData.additional_data)
    } else {
      responseData.additional_data = {}
    }
    setData(responseData)
  }

  useEffect(() => { getData() }, [id])

  const renderCard = () => {
    if (data) {
      if (['visual_art', 'video', 'performance_journey', 'music_journey', 'song', 'artist_spotlight', 'schedule'].indexOf(data.type) >= 0) {
        let CardComponent = {
          visual_art: VisualArt,
          video: Video,
          music_journey: Experience,
          performance_journey: Experience,
          song: data.additional_data.in_app_player ? SongFile : SongLink,
          artist_spotlight: ArtistSpotlight,
          schedule: Schedule
        }[data.type]
        return (<CardComponent navigateTo={navigateTo} navigation={navigation} data={data} />)
      } else return <Text>{data.type}</Text>
    }
  }

  if (data && data.empty) {
    return (<View style={{ marginHorizontal: 0 }}>
      <FeedItemDeleted />
    </View>)
  }

  return (
    <View>
      <View style={[style.cardWrapper, data == null ? { height: loaderHeight || 104 } : { backgroundColor: WHITE, borderWidth: 0 }]}>
        <Loader size={'mini'} isLoading={data == null}>
          {renderCard()}
          {data && showTopics && (<View style={style.topicWrapper}>
            {(data.topic_name || []).map((topic) => (<View style={style.topic} key={Math.random()}>
              <Text color={GREY_WARM} weight={400} size={'tiny'}>{topic}</Text>
            </View>))}
          </View>)}
        </Loader>
      </View>
      {!data && showTopics && (<View style={style.topicWrapper}>
        {(['···']).map((topic) => (<View style={{ ...style.topic, paddingHorizontal: 30 }} key={Math.random()}>
          <Text color={GREY_WARM} weight={400} size={'tiny'}>{topic}</Text>
        </View>))}
      </View>)}
    </View>
  )
}

const mapStateToProps = ({ auth }) => {
  return {
    userData: auth.user
  }
}

export default _enhancedNavigation(connect(mapStateToProps, {})(ProfileComponent))
