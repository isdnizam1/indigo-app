import React from 'react'
import { TouchableOpacity } from 'react-native'
import { upperFirst } from 'lodash-es'
import { TOUCH_OPACITY } from '../../../constants/Styles'
import { monthToMonthYear } from '../../../utils/date'
import ItemShareDefault from '../ItemShareDefault'

class Experience extends React.PureComponent {

  render() {
    const { data, navigateTo } = this.props
    let { additional_data: { media, event_location, event_date } } = data

    const onPress = () => {
      navigateTo('DetailExperienceScreen', { data })
    }

    return (
      <TouchableOpacity onPress={onPress} activeOpacity={TOUCH_OPACITY}>
        <ItemShareDefault
          image={media[0]}
          label={'Experience'}
          title={data.title}
          subtitle={`${monthToMonthYear(`${event_date}`)}  ∙  ${upperFirst(event_location)}`}
        />
      </TouchableOpacity>
    )
  }

}

export default Experience