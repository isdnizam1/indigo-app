import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { isObject, upperFirst } from 'lodash-es'
import Text from 'sf-components/Text'
import Icon from 'sf-components/Icon'
import Image from 'sf-components/Image'
import Touchable from 'sf-components/Touchable'
import { postLogViewJourney } from 'sf-actions/api'
import { getTime, NavigateToInternalBrowser } from 'sf-utils/helper'
import { REDDISH, WHITE } from 'sf-constants/Colors'
import { setIdJourney as setNowPlaying, setPlayback } from 'sf-services/song/actionDispatcher'
import { WP05, WP1, WP10, WP105, WP15, WP4, WP5, WP6 } from '../../../constants/Sizes'
import { SHADOW_STYLE } from '../../../constants/Styles'
import { GUN_METAL, PALE_BLUE, PALE_GREY_TWO, SHIP_GREY_CALM } from '../../../constants/Colors'

class SongFile extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      timerWidth: WP10,
      isActiveSong: false
    }
    this._onUpdatingTrack = this._onUpdatingTrack.bind(this)
    this._playThisSong = this._playThisSong.bind(this)
    this._onTimerLayout = this._onTimerLayout.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    const shouldUpdate =
      nextState.isActiveSong ||
      nextProps.playback.isPlaying != this.props.playback.isPlaying ||
      this.state.timerWidth != nextState.timerWidth
    return shouldUpdate
  }

  _onUpdatingTrack = (value) => {
    const { isActiveSong } = this.state
    const { soundObject } = this.props
    isActiveSong && soundObject.setPositionAsync(value)
  }

  static getDerivedStateFromProps(props, state) {
    const { nowPlaying, data: { id: id_journey } } = props
    return {
      isActiveSong: nowPlaying == id_journey,
    }
  }

  _playThisSong = async () => {
    const {
      data: song,
      data: { id: id_journey },
      soundObject,
      nowPlaying,
      setNowPlaying,
      setPlayback,
      playback: { positionMillis, durationMillis, isPlaying, isLoaded },
      userData: { id_user: id_viewer },
    } = this.props
    const isPlayThisSong = isPlaying && id_journey == nowPlaying
    if (isPlaying && id_journey == nowPlaying) {
      soundObject.pauseAsync()
    } else if (positionMillis / durationMillis >= 0.985) {
      await soundObject.setPositionAsync(0)
      soundObject.playAsync()
    } else {
      if (nowPlaying != id_journey) setNowPlaying(id_journey)
      try {
        if (!isPlayThisSong && nowPlaying != id_journey && isLoaded) {
          await soundObject.pauseAsync()
          await soundObject.unloadAsync()
        }
      } catch (err) {
        // silent is gold
      } finally {
        const add = isObject(song.additional_data)
          ? song.additional_data
          : JSON.parse(song.additional_data)
        const { in_app_player: isFileSong } = add
        const logBody = {
          id_user: id_viewer,
          id_journey,
        }
        if (nowPlaying != id_journey && isFileSong == 1) {
          const param = {
            uri: add.url_song,
            overrideFileExtensionAndroid: 'm3u8',
          }
          await soundObject.loadAsync(param)
          postLogViewJourney(logBody).then(({ data: responseFromServer }) => {
            song.total_view++
          })
        }
        if (isFileSong == 1) {
          soundObject.setOnPlaybackStatusUpdate(setPlayback)
          soundObject.playAsync()
        } else {
          NavigateToInternalBrowser({ url: add.url_song })
          postLogViewJourney(logBody).then(({ data: responseFromServer }) => {
            song.total_view++
          })
        }
      }
    }
  }

  _onTimerLayout = ({
    nativeEvent: {
      layout: { width: timerWidth },
    },
  }) => {
    this.setState({ timerWidth: timerWidth + WP1 })
  };

  render() {
    const { isActiveSong, timerWidth } = this.state
    const { data, playback: { isPlaying, positionMillis, durationMillis, isBuffering } } = this.props
    let { title, additional_data: { cover_image, in_app_player: isFileSong }, total_view } = data
    const subtitle = `  ${total_view ? total_view : '0'}${durationMillis ? `  ∙  ${getTime(durationMillis)}` : ''}`
    return (
      <Touchable onPress={this._playThisSong}>
        <View style={{
          padding: WP4,
          paddingRight: WP5,
          backgroundColor: WHITE,
          borderRadius: 6,
          flexDirection: 'row',
          alignItems: 'flex-start',
          ...SHADOW_STYLE['shadowThin']
        }}
        >
          <Touchable onPress={this._playThisSong}>
            <View style={{ width: WP15, height: WP15, marginRight: WP4, backgroundColor: PALE_GREY_TWO, borderRadius: 6, ...SHADOW_STYLE['shadowThin'] }}>
              <View style={{ borderRadius: 6, overflow: 'hidden' }}>
                <Image
                  source={{ uri: cover_image }}
                  imageStyle={{ height: WP15, aspectRatio: 1 }}
                />
                {(!isBuffering) && isActiveSong && isPlaying && (
                  <View style={{ width: WP15, height: WP15, backgroundColor: 'rgba(0,0,0,0.5)', position: 'absolute', top: 0, left: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                      imageStyle={{ height: WP6 }}
                      source={require('sf-assets/icons/spPauseCircle.png')}
                    />
                  </View>
                )}
                {isBuffering && isActiveSong && (
                  <View style={{ width: WP15, height: WP15, backgroundColor: 'rgba(0,0,0,0.5)', position: 'absolute', top: 0, left: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator color={WHITE} size={'small'} />
                  </View>
                )}
              </View>
            </View>
          </Touchable>
          <View style={{ flex: 1 }}>
            <Text numberOfLines={1} type='Circular' size='xmini' color={REDDISH} weight={300} style={{ marginBottom: WP05 }}>Play Song</Text>
            <Text numberOfLines={2} type='Circular' size='mini' color={GUN_METAL} weight={500} style={{ marginBottom: WP1 }}>{upperFirst(title)}</Text>
            <View>
              {
                isActiveSong && isPlaying ? (
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ marginRight: WP105 }}>
                      <Image
                        imageStyle={{ height: WP4 }}
                        source={require('sf-assets/icons/v3/playingIllustration.png')}
                      />
                    </View>
                    <View style={{ width: timerWidth, alignItems: 'flex-start' }}>
                      {timerWidth > 0 && isActiveSong && isPlaying && (
                        <Text
                          type={'Circular'}
                          weight={300}
                          color={REDDISH}
                          size={'tiny'}
                        >
                          {positionMillis ? getTime(positionMillis) : '00:00'}
                        </Text>
                      )}
                    </View>
                    <View style={{ width: WP10, height: WP1 / 2, backgroundColor: PALE_BLUE }}>
                      <View
                        style={{
                          width: `${(positionMillis / durationMillis) * 100}%`,
                          height: WP1 / 2, backgroundColor: REDDISH
                        }}
                      />
                    </View>
                    <View style={{ width: timerWidth, alignItems: 'flex-end' }}>
                      {timerWidth > 0 && isFileSong && (
                        <Text
                          type={'Circular'}
                          weight={300}
                          color={REDDISH}
                          size={'tiny'}
                        >
                          {durationMillis ? getTime(durationMillis) : '00:00'}
                        </Text>
                      )}
                    </View>
                  </View>
                ) : (
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View>
                      <Icon color={SHIP_GREY_CALM} size={'tiny'} name={'play'} />
                    </View>
                    <Text
                      numberOfLines={1}
                      style={{ flex: 1 }}
                      type={'Circular'}
                      weight={300}
                      color={SHIP_GREY_CALM}
                      size={'tiny'}
                    >
                      {subtitle}
                    </Text>
                  </View>
                )
              }
            </View>
          </View>
          {isActiveSong && isPlaying && (
            <View
              onLayout={this._onTimerLayout}
              style={{ position: 'absolute', opacity: 0, }}
            >
              <Text type={'Circular'} weight={300} color={REDDISH} size={'tiny'}>
                00:00
              </Text>
            </View>
          )}
        </View>
      </Touchable>
    )
  }

}

const mapStateToProps = ({
  auth,
  song: { soundObject, playback, id_journey: nowPlaying }
}) => ({
  soundObject,
  playback,
  nowPlaying,
  userData: auth.user
})

const mapDispatchToProps = {
  setNowPlaying,
  setPlayback,
}

export default connect(mapStateToProps, mapDispatchToProps)(SongFile)
