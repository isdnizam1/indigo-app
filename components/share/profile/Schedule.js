import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import moment from 'moment/min/moment-with-locales'
import { WP3, WP4, WP6, WP8 } from '../../../constants/Sizes'
import ScheduleDate from '../../../screens/profile/v2/schedule/ScheduleDate'
import Image from '../../Image'
import Modal from '../../Modal'
import Text from '../../Text'
import ItemShareDefault from '../ItemShareDefault'
import { GUN_METAL, SHIP_GREY } from '../../../constants/Colors'

class Schedule extends React.PureComponent {

  _scheduleDetail = ({ toggleModal }) => {
    const schedule = this.props.data
    return (
      <View style={{
        alignItems: 'center',
        padding: WP6,
        paddingTop: WP8
      }}
      >
        <Image
          style={{ position: 'absolute', right: WP3, top: WP3 }}
          source={require('../../../assets/icons/close.png')}
          imageStyle={{ width: WP6 }}
          onPress={toggleModal}
        />
        <ScheduleDate
          date={schedule.event_date}
          size='large'
          style={{}}
        />
        <View style={{}}>
          <View style={{ marginVertical: WP4 }}>
            <Text centered color={GUN_METAL} weight={500} type='Circular' size='medium'>{schedule.event_name}</Text>
          </View>
          <View style={{ marginBottom: WP6 }}>
            <Text centered type='Circular' color={SHIP_GREY} size='mini'>{moment(schedule.event_date).locale('id').format('ddd, DD MMM YYYY')}</Text>
            <Text centered type='Circular' color={SHIP_GREY} size='mini'>{schedule.event_location}</Text>
            <Text centered type='Circular' color={SHIP_GREY} size='mini'>{moment(schedule.event_date).locale('id').format('hh:mm A')}</Text>
          </View>

          <Text centered type='Circular' color={SHIP_GREY} size='xmini'>Performance by</Text>
          <Text centered type='Circular' color={GUN_METAL} size='slight' weight={400}>{schedule.full_name}</Text>
        </View>
      </View>
    )
  }

  render() {
    const { data } = this.props
    return (
      <Modal
        position='center'
        swipeDirection='none'
        renderModalContent={this._scheduleDetail}
        style={{
          borderBottomRightRadius: 6,
          borderBottomLeftRadius: 6
        }}
      >
        {
          ({ toggleModal }, M) => (
            <>
              <TouchableOpacity
                activeOpacity={0.5}
                onPress={toggleModal}
              >
                <ItemShareDefault
                  image={data.profile_picture}
                  imageRound={true}
                  label={'See Schedule'}
                  title={data.event_name}
                  subtitle={`${data.full_name}’s Performance`}
                />
              </TouchableOpacity>
              {M}
            </>
          )
        }
      </Modal>
    )
  }
}

export default Schedule
