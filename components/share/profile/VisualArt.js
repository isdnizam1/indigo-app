import React from 'react'
import { TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import _enhancedNavigation from '../../../navigation/_enhancedNavigation'
import { TOUCH_OPACITY } from '../../../constants/Styles'
import ItemShareLarge from '../ItemShareLarge'

const VisualArt = (props) => {

  const { data, navigateTo } = props

  const onPress = () => {
    navigateTo('GalleryScreen', {
      images: [
        {
          url: data.attachment[0].attachment_file,
          title: data.title,
        },
      ],
    })
  }

  return (
    <TouchableOpacity onPress={onPress} activeOpacity={TOUCH_OPACITY}>
      <ItemShareLarge
        image={data.attachment[0].attachment_file}
        title={data.title}
        subtitle={'Artwork & Gallery'}
      />
    </TouchableOpacity>
  )

}

const mapStateToProps = ({ auth }) => ({})

export default _enhancedNavigation(connect(mapStateToProps, {})(VisualArt))