import React from 'react'
import { View } from 'react-native'
import { isFunction, upperFirst } from 'lodash-es'
import { GUN_METAL, SHIP_GREY_CALM, WHITE, REDDISH, PALE_GREY_TWO } from '../../constants/Colors'
import { WP1, WP4, WP15, WP5, WP05 } from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'
import Image from '../Image'
import Text from '../Text'

const ItemShareDefault = ({
  label,
  title,
  titleLine = 2,
  subtitle,
  subtitleLine = 1,
  showImage = true,
  image,
  imageWidth = WP15,
  imageHeight = WP15,
  imageRound = false,
  aspectRatio = 1,
  radius = 6,
  loading
}) => {
  const titleAds = loading ? 'Title' : title,
    subtitleAds = subtitle,
    labelAds = label,
    imageRadius = imageRound ? imageWidth / 2 : radius

  return (
    <View style={{
      padding: WP4,
      paddingRight: WP5,
      backgroundColor: WHITE,
      borderRadius: radius,
      flexDirection: 'row',
      alignItems: 'flex-start',
      ...SHADOW_STYLE['shadowThin']
    }}
    >
      {
        showImage ? (
          <View style={{ width: imageWidth, height: imageHeight, marginRight: WP4, backgroundColor: PALE_GREY_TWO, borderRadius: imageRadius, ...SHADOW_STYLE['shadowThin'] }}>
            {
              !loading && (
                <View style={{ borderRadius: imageRadius, overflow: 'hidden' }}>
                  <Image
                    source={{ uri: image }}
                    imageStyle={{ height: imageHeight, aspectRatio }}
                  />
                </View>
              )
            }
          </View>
        ) : null
      }
      <View style={{ flex: 1 }}>
        {labelAds && (
          <Text numberOfLines={1} type='Circular' size='xmini' color={REDDISH} weight={300} style={{ marginBottom: WP05 }}>{upperFirst(labelAds)}</Text>
        )}
        <Text numberOfLines={titleLine} type='Circular' size='mini' color={GUN_METAL} weight={500} style={{ marginBottom: WP1 }}>{upperFirst(titleAds)}</Text>
        {subtitleAds && (
          isFunction(subtitleAds) ?
            subtitleAds() :
            (<Text numberOfLines={subtitleLine} type='Circular' size='xmini' color={SHIP_GREY_CALM} weight={300}>{upperFirst(subtitleAds)}</Text>)
        )}
      </View>
    </View>
  )
}

export default ItemShareDefault