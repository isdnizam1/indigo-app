import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { MAIZE, GREY, NO_COLOR, WHITE, GREY_CHAT } from '../../../constants/Colors'
import Image from '../../Image'
import { WP25, WP3, WP5, WP105, WP05, WP1, WP35 } from '../../../constants/Sizes'
import Text from '../../Text'
import { getGenre } from '../../../utils/helper'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareBand extends React.PureComponent {
  render() {
    const { data, navigateTo, isFeed } = this.props
    const { ads_status } = data
    const is_expired = ads_status === 'expired'
    const additionalData = JSON.parse(data.additional_data)
    return (
      <TouchableOpacity
        style={{
          backgroundColor: WHITE,
          // marginHorizontal: WP5,
          // marginTop: HP2,
          borderRadius: 12,
          borderColor: MAIZE,
          borderWidth: WP1,
          overflow: 'hidden'
        }}
        disabled={!isFeed}
        activeOpacity={is_expired ? 1 : 0.8}
        onPress={is_expired ? null : () => navigateTo('AdsDetailScreenNoTab', { id_ads: data.id })}
      >
        <View
          style={{
            backgroundColor: WHITE,
            alignItems: 'center',
            flexDirection: 'row',
          }}
        >
          <LinearGradient
            colors={[WHITE, NO_COLOR]}
            start={[0.6, 0]}
            end={[1, 0]}
            style={{
              paddingHorizontal: WP3,
              paddingRight: WP35,
              height: WP25,
              flex: 1,
              justifyContent: 'space-between',
              zIndex: 99,
            }}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                imageStyle={{ width: WP5, height: null }}
                style={{ marginRight: WP105 }} source={require('../../../assets/icons/icBadgeBand.png')}
              />
              <Text numberOfLines={1} size='xtiny' color={GREY}>Artist Spotlight</Text>
            </View>
            <View style={{ flex: 1, paddingVertical: WP105 }}>
              <Text style={{ marginVertical: WP05 }} numberOfLines={2} ellipsizeMode='tail' size='medium' weight={500} color={GREY}>{data.title}</Text>
              <Text style={{ flex: 1 }} numberOfLines={1} size='xtiny' color={GREY}>{getGenre(additionalData.genre)}</Text>
              {/* <Text centered size='xtiny' numberOfLines={2}>{}</Text> */}
            </View>
          </LinearGradient>
          <Image
            centered
            source={{ uri: data.image }}
            style={{ backgroundColor: GREY_CHAT, alignSelf: 'flex-end', position: 'absolute', right: 0, top: 0 }}
            imageStyle={{ height: undefined, width: WP35, aspectRatio: 1 }}
          />
        </View>
      </TouchableOpacity>
    )
  }
}

ItemShareBand.propTypes = propsType
ItemShareBand.defaultProps = propsDefault
export default ItemShareBand
