import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import { upperFirst } from 'lodash-es'
import ItemShareDefault from '../ItemShareDefault'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareEpisode extends React.PureComponent {
  render() {
    const { data, navigateTo, isFeed } = this.props
    const { ads_status, episode } = data
    const is_expired = ads_status === 'expired'
    const subtitle = `${episode.speaker_name.length <= 25 ? episode.speaker_name : `${episode.speaker_name.substring(0, !episode ? episode.speaker_name.length : 25)}${episode.speaker_name.length > 25 ? '...' : ''}`}${episode.length ? `  ∙  ${upperFirst(episode.length)}` : ''}`
    return (
      <TouchableOpacity
        disabled={!isFeed}
        activeOpacity={is_expired ? 1 : 0.5}
        onPress={is_expired ? null : () => navigateTo('SoundplayDetail', { id_ads: data.id, id_episode: episode.id_episode })}
      >
        <ItemShareDefault
          image={data.image}
          label={'Play Podcast'}
          title={data.title}
          subtitle={subtitle}
        />
      </TouchableOpacity>
    )
  }
}

ItemShareEpisode.propTypes = propsType
ItemShareEpisode.defaultProps = propsDefault
export default ItemShareEpisode
