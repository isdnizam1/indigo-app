import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import ItemShareDefault from '../ItemShareDefault'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareAlbum extends React.PureComponent {
  render() {
    const { data, navigateTo, isFeed } = this.props
    const { ads_status, speaker } = data
    const is_expired = ads_status === 'expired'
    return (
      <TouchableOpacity
        disabled={!isFeed}
        activeOpacity={is_expired ? 1 : 0.5}
        onPress={is_expired ? null : () => navigateTo('SoundplayDetail', { id_ads: data.id })}
      >
        <ItemShareDefault
          image={data.image}
          label={'Play Podcast'}
          title={data.title}
          subtitle={speaker.title}
        />
      </TouchableOpacity>
    )
  }
}

ItemShareAlbum.propTypes = propsType
ItemShareAlbum.defaultProps = propsDefault
export default ItemShareAlbum
