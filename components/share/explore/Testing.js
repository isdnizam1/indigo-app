import React, { Component } from 'react'
import { View, ScrollView } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { WP12, WP6, WP3, WP4 } from '../../../constants/Sizes'
import { Text, _enhancedNavigation } from '../..'
import { GREY, WHITE, BLUE_LIGHT } from '../../../constants/Colors'
import ItemShareCollaboration from './ItemShareCollaboration'
import ItemShareSubmission from './ItemShareSubmission'
import ItemShareEvent from './ItemShareEvent'
import ItemShareNews from './ItemShareNews'
import ItemShareSession from './ItemShareSession'
import ItemShareVideo from './ItemShareVideo'
import ItemShareAlbum from './ItemShareAlbum'
import ItemShareEpisode from './ItemShareEpisode'
import ItemShareBand from './ItemShareBand'

const mapStateToProps = ({ auth, message }) => ({
  userData: auth.user
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
})

class Testing extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [
        {
          'id': '285',
          'related_to': 'id_ads',
          'type': 'collaboration',
          'image': 'https://apidev.soundfren.id/userfiles/images/ads/322_banner_picture_1576384634.png',
          'title': 'Cari pemain strings untuk music cover yang enak di dengar ?',
          'description': 'adsadf',
          'topic_name': [
            'Find Music Actors'
          ],
          'additional_data': '{"profession":[{"name":"Bassist","notes":"hgjhgjhg jg gjgjgjg. jgjhgg"}],"post_duration":"14 days","date":{"start":"2019-12-15","end":"2019-12-29"},"location":{"name":"Jakarta"},"specification":"adsaf"}',
          'id_user': '972',
          'full_name': 'dempetin url shortener  test',
          'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/1581822432_972_profile_picture.jpg'
        },
        {
          'id': '223',
          'related_to': 'id_ads',
          'type': 'submission',
          'image': 'https://apidev.soundfren.id/userfiles/images/ads/972_artist_picture_1574665721.png',
          'title': 'Submission ios',
          'description': 'Submission123\nCity\nSpasm\n',
          'topic_name': [
            'Find Music Actors'
          ],
          'additional_data': '{"date":{"start":"2019-11-25","end":"2019-11-25"},"location":{"name":"Jakarta"},"requirement":["Lolas I di jakarta","Lokasi di mana","Req 3"],"benefit":["Kontrakbdrmain","Ketutupan","Yah"]}',
          'id_user': '972',
          'full_name': 'dempetin url shortener  test',
          'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/1581822432_972_profile_picture.jpg'
        },
        {
          'id': '337',
          'related_to': 'id_ads',
          'type': 'band',
          'image': 'https://apidev.soundfren.id/userfiles/images/ads/978_artist_picture_1582620907.png',
          'title': 'Band IT',
          'description': 'Lorem ipsum dolor sit ammet',
          'topic_name': [
            'Music Promo'
          ],
          'additional_data': '{"genre":[{"name":"Jazz"},{"name":"Indihome Indigo"},{"name":"Folk JAzz asdfasdfsdf ad adf asdfasfa fasdfsdds"}],"contact":{"phone":"082223668488"},"location":{"name":"JAKARTA SELATAN","link":"https:\\/\\/www.google.com\\/maps\\/place\\/JAKARTA SELATAN"},"members":[{"name":"Aang Dev 2","link":"1049-Aang-Dev-2"},{"name":"Muhammad Raafi Aldzaki ","link":"1053-Muhammad-Raafi-Aldzaki-"}],"photos":[{"name":"1053_banner_picture_1582643869.png","link":"https:\\/\\/apidev.soundfren.id\\/userfiles\\/images\\/ads\\/1053_banner_picture_1582643869.png"}],"song":[{"name":"Muse Live","description":"Lorem ipsum dolor sit ammet","link":"https:\\/\\/youtu.be\\/8eTggVG5wsc","image":"https:\\/\\/soundfren.id\\/assets\\/image\\/icon-song.png"}],"social_media":[{"name":"youtube","link":"https:\\/\\/123","username":"123"}]}',
          'id_user': '972',
          'full_name': 'dempetin url shortener  test',
          'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/1581822432_972_profile_picture.jpg'
        },
        {
          'id': '359',
          'related_to': 'id_ads',
          'type': 'event',
          'image': 'https://apidev.soundfren.id/userfiles/images/ads/425_artist_picture_1583771933.png',
          'title': 'test create viewer',
          'description': '<p>event</p>\r\n',
          'topic_name': [
            'Event & Gigs'
          ],
          'additional_data': '{"date":{"start":"2020-03-13","end":"2020-03-13"},"location":{"name":"pasak","link":"#"},"requested_by":{"name":"Viewer 1","link":"user\\/profile\\/view\\/1083","image":null},"specification":"ok","photos":[{"name":"","link":"https:\\/\\/apidev.soundfren.id\\/userfiles\\/images\\/ads\\/1083_event_1583977643.png"}],"redirect":{"name":"Detail","link":"","internal_link":"true"}}',
          'id_user': '1',
          'full_name': 'User 3',
          'profile_picture': 'https://soundfren.id/assets/image/logo_mini_nav.png'
        },
        {
          'id': '13',
          'related_to': 'id_ads',
          'type': 'news_update',
          'image': 'https://soundfren.id/assets/image/event/Kuassa_320x160.jpg',
          'title': 'Kuassa Introduces the second Amplifikation using the 3rd gen Tube Simulation engine: Amplifikation Matchlock, and virtual pedalboard Amplifikation 360',
          'description': 'Along with other Indonesian craft, pro-sound and exotic music instruments, Kuassa exhibit their product range at <a style="text-decoration:underline;color:blue;" href="https://musik.messefrankfurt.com/frankfurt/en.html">Frankfurt Musikmesse 2019</a>. <br/>Also introducing their latest prototypes:\r\n<ul>\r\n<br/>\r\n<li type="number">Amplifikation Matchlock: the second product from Kuassa\'s 3rd gen Tube Simulation technology following the much-hyped <a style="text-decoration:underline;color:blue;" href="https://www.kuassa.com/products/amplifikation-caliburn/">Amplifikation Caliburn</a>. Amplifikation Matchlock inspired by Fender* combo amps and thanks to the 3rd gen Tube Simulation engine guarantees a more dynamic natural sounding, realistic amp recording experience for guitarists.</li>\r\n<br/>\r\n<li type="number">Amplifikation 360: Includes an array of modules taken from <a style="text-decoration:underline;color:blue;" href="https://www.kuassa.com/products">Kuassas Amplifikation</a> and <a style="text-decoration:underline;color:blue;" href="https://www.kuassa.com/products">Efektor series</a>. The much anticipated virtual pedalboard was featuring a fast, intuitive sound design workflow for a guitarist.\r\nKuassa is also celebrating their 9th Birthday this April. Pricing their Amplifikation & Studio FXs at 49% discount, and Efektors at 24% discount during this April.</li>\r\n</ul>\r\n<br/>\r\n\r\n<a style="color:blue;text-decoration:underline;" href="https://www.kuassa.com/kuassa-9th-anniversary-sale/">Kuassa is also celebrating their 9th Birthday this April. Pricing their Amplifikation & Studio FXs at 49% discount, and Efektors at 24% discount during this April.</a>',
          'total_view': 16,
          'topic_name': [
            'Discussion'
          ],
          'additional_data': null,
          'id_user': '972',
          'full_name': 'dempetin url shortener  test',
          'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/1581822432_972_profile_picture.jpg'
        },
        {
          'id': '425',
          'related_to': 'id_ads',
          'type': 'sp_album',
          'image': 'https://soundfren.id/assets/image/event/Suggestion-App-Banner.jpg',
          'title': 'Grow your community: Indomusikgram in the Music Industry of Indonesia',
          'description': 'Lorem <b>ipsum</b> <i>dolor</i> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'speaker': {
            'id_ads': '372',
            'title': 'Speaker 2'
          },
          'topic_name': [
            'Discussion'
          ],
          'additional_data': '{\r\n\t"id_speaker": "372",\r\n\t"id_sp_album_category": 1,\r\n\t"episodeList": [{\r\n\t\t\t"id_episode": 1,\r\n\t\t\t"speaker_name": "Aziz",\r\n\t\t\t"title": "Perjuangan Payung Teduh",\r\n\t\t\t"link": "https://soundfren-audio.s3-ap-southeast-1.amazonaws.com/converted/test-upload.m3u8",\r\n\t\t\t"length": "10:00",\r\n\t\t\t"is_premium": 0\r\n\t\t},\r\n\t\t{\r\n\t\t\t"id_episode": 2,\r\n\t\t\t"speaker_name": "Nabila",\r\n\t\t\t"title": "Pemilihan Kualitas Audio",\r\n\t\t\t"link": "https://soundfren-audio.s3-ap-southeast-1.amazonaws.com/converted/test-upload.m3u8",\r\n\t\t\t"length": "05:00",\r\n\t\t\t"is_premium": 1\r\n\t\t},\r\n\t\t{\r\n\t\t\t"id_episode": 3,\r\n\t\t\t"speaker_name": "Annisa",\r\n\t\t\t"title": "Menciptakan Band kelas wahid",\r\n\t\t\t"link": "https://soundfren-audio.s3-ap-southeast-1.amazonaws.com/converted/test-upload.m3u8",\r\n\t\t\t"length": "05:00",\r\n\t\t\t"is_premium": 1\r\n\t\t}\r\n\t]\r\n}',
          'id_user': '972',
          'full_name': 'dempetin url shortener  test',
          'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/1581822432_972_profile_picture.jpg'
        },
        {
          'id': '425',
          'related_to': 'id_ads',
          'type': 'sp_album',
          'image': 'https://soundfren.id/assets/image/event/Suggestion-App-Banner.jpg',
          'title': 'Perjuangan Payung Teduh',
          'description': 'Lorem <b>ipsum</b> <i>dolor</i> sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          'episode': {
            'id_episode': 1,
            'speaker_name': 'Aziz',
            'title': 'Perjuangan Payung Teduh',
            'link': 'https://soundfren-audio.s3-ap-southeast-1.amazonaws.com/converted/test-upload.m3u8',
            'length': '10:00',
            'is_premium': 0
          },
          'topic_name': [
            'Discussion'
          ],
          'id_user': '972',
          'full_name': 'dempetin url shortener  test',
          'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/1581822432_972_profile_picture.jpg'
        },
        {
          'id': '412',
          'related_to': 'id_ads',
          'type': 'video',
          'image': 'https://eventeer.id/assets/image/event/sf_meetup_vol3.jpeg',
          'title': 'Soundfren Meetup Vol.3',
          'description': 'Soundfren bekerjasama dengan Collabs Asia mempersembahkan Soundfren Meet Up Vol.3 dengan mengangkat topik diskusi <b>“Memaksimalkan Youtube untuk Musisi”</b>.\r\n<br/><br/>\r\nPembicara:<br/> \r\n- Komang Adhyatma (Collab Asia Music)<br/>\r\n- Christophe B. Renato (Collab Asia Music)<br/>\r\n- Daffa Andika (Kolibri Rekords/Music Label)<br/>\r\n- Eric Wirjanata (Deathrockstar/Media)\r\n<br/><br/>\r\nModerator : Hazbi Faizasyah (Sunyata Session)\r\n<br/><br/>\r\nDiskusi ini sangat cocok bagi kamu yang ingin memaksimalkan Youtube untuk musik kamu, beberapa poin utama pembahasan di sesi ini adalah<br/>\r\n1. Memaksimalkan Youtube sebagai media promosi musik dan perkembang karir sebagai musisi.<br/>\r\n2. Pentingnya memberikan perlindungan hak cipta pada karya-karya digital, baik secara audio maupun visual.<br/>\r\n3. Promosi melalui label musik dan juga penerapannya di berbagai platform digital.<br/>\r\n4. Melakukan monetisasi dari musikmu di Youtube<br/>\r\n5. Pengalaman para speakers dalam bergelut di media musik digital sejak tahun 2002.  \r\n<br/><br/>\r\nYuks langsung tonton Soundfren Meet Up Vol. 3 ini melalui Sounds-Edu',
          'topic_name': [
            'Discussion'
          ],
          'additional_data': '{\r\n\t"url_video": "https://s3.soundfren.id/converted/Sf-Meetup-Vol3-Full-1-conv.m3u8",\r\n\t"url_video_teaser": "https://s3.soundfren.id/converted/Teaser-SFMeetUp3-3.m3u8",\r\n\t"thumbnail":"https://eventeer.id/assets/image/event/sf_meetup_vol3.jpeg",\r\n\t"video_duration": "1:44:02",\r\n\t"category": ["Copyright & Related", "Strategi & Pemasaran Musik", "Publikasi Musik"]\r\n}',
          'id_user': '972',
          'full_name': 'dempetin url shortener  test',
          'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/1581822432_972_profile_picture.jpg'
        },
        {
          'id': '390',
          'related_to': 'id_ads',
          'type': 'sc_session',
          'image': 'https://apidev.soundfren.id/userfiles/images/ads/425_artist_picture_1586589167.png',
          'title': 'session Z',
          'description': '<p>DESC</p>\r\n',
          'speaker': {
            'id_ads': '376',
            'title': 'Speaker 3'
          },
          'available_seats': 20,
          'topic_name': [
            'Event & Gigs',
            'Introduction'
          ],
          'additional_data': '{"url":"https:\\/\\/soundfren.id\\/","date":{"start":"2020-12-12 12:12:00","end":"2020-12-12 13:12:00"},"total_seats":"21","id_speaker":"376","format":"Video conference","price":{"normal_price":"20","discount_price":"10","currency":"Rp","discount":"50%"},"photos":[{"name":"-","link":"https:\\/\\/apidev.soundfren.id\\/userfiles\\/images\\/ads\\/425_sc_session_1586589167-1.png"},{"name":"-","link":"https:\\/\\/apidev.soundfren.id\\/userfiles\\/images\\/ads\\/425_sc_session_1586589167-2.png"},{"name":"-","link":"https:\\/\\/apidev.soundfren.id\\/userfiles\\/images\\/ads\\/425_sc_session_1586589167-3.png"}],"redirect":{"name":"Join","link":"","internal_link":true}}',
          'id_user': '972',
          'full_name': 'dempetin url shortener  test',
          'profile_picture': 'https://apidev.soundfren.id/userfiles/images/profile_picture/1581822432_972_profile_picture.jpg'
        }
      ]
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ paddingTop: WP12, paddingBottom: WP3 }}>
          <Text centered numberOfLines={2} type='NeoSans' size='miny' weight={500} color={GREY}>Testing Card</Text>
        </View>
        <ScrollView
          style={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
        >
          <View style={{ paddingHorizontal: WP3, paddingVertical: WP4, backgroundColor: BLUE_LIGHT, marginBottom: WP3 }}>
            <Text numberOfLines={2} type='NeoSans' size='miny' weight={500} color={WHITE}>Share Simpplify</Text>
          </View>
          {
            this.state.data.map((ads, index) =>
              (<View key={index} style={{ marginBottom: WP6 }}>
                <Text style={{ marginHorizontal: WP6, }} type='NeoSans' size='tiny' weight={500} color={WHITE}>{ads.type === 'sp_album' ? 'speaker' in ads ? 'Album Card' : 'Episode Card' : `${ads.type} Card`}</Text>
                {ads.type === 'collaboration' && <ItemShareCollaboration data={ads} />}
                {ads.type === 'submission' && <ItemShareSubmission data={ads} />}
                {ads.type === 'event' && <ItemShareEvent data={ads} />}
                {ads.type === 'news_update' && <ItemShareNews data={ads} />}
                {ads.type === 'sc_session' && <ItemShareSession data={ads} />}
                {ads.type === 'video' && <ItemShareVideo data={ads} />}
                {ads.type === 'sp_album' && 'speaker' in ads && <ItemShareAlbum data={ads} />}
                {ads.type === 'sp_album' && 'episode' in ads && <ItemShareEpisode data={ads} />}
                {ads.type === 'band' && <ItemShareBand data={ads} />}
              </View>)
            )
          }

          <View style={{ paddingHorizontal: WP3, paddingVertical: WP4, backgroundColor: BLUE_LIGHT, marginBottom: WP3 }}>
            <Text numberOfLines={2} type='NeoSans' size='miny' weight={500} color={WHITE}>Share Timeline</Text>
          </View>
          {
            this.state.data.map((ads, index) =>
              (<View key={index} style={{ marginBottom: WP6 }}>
                <Text style={{ marginHorizontal: WP6, }} type='NeoSans' size='tiny' weight={500} color={GREY}>{ads.type === 'sp_album' ? 'speaker' in ads ? 'Album Card' : 'Episode Card' : `${ads.type} Card`}</Text>
                {ads.type === 'collaboration' && <ItemShareCollaboration data={ads} isFeed navigateTo={this.props.navigateTo} />}
                {ads.type === 'submission' && <ItemShareSubmission data={ads} isFeed navigateTo={this.props.navigateTo} />}
                {ads.type === 'event' && <ItemShareEvent data={ads} isFeed navigateTo={this.props.navigateTo} />}
                {ads.type === 'news_update' && <ItemShareNews data={ads} isFeed navigateTo={this.props.navigateTo} />}
                {ads.type === 'sc_session' && <ItemShareSession data={ads} isFeed navigateTo={this.props.navigateTo} />}
                {ads.type === 'video' && <ItemShareVideo data={ads} isFeed navigateTo={this.props.navigateTo} />}
                {ads.type === 'sp_album' && 'speaker' in ads && <ItemShareAlbum data={ads} isFeed navigateTo={this.props.navigateTo} />}
                {ads.type === 'sp_album' && 'episode' in ads && <ItemShareEpisode data={ads} isFeed navigateTo={this.props.navigateTo} />}
                {ads.type === 'band' && <ItemShareBand data={ads} isFeed navigateTo={this.props.navigateTo} />}
              </View>)
            )
          }
        </ScrollView >
      </View>
    )
  }
}

Testing.propTypes = {
  navigateTo: PropTypes.func
}

Testing.defaultProps = {
  navigateTo: () => {
  }
}

export default _enhancedNavigation(
  connect(mapStateToProps, {})(Testing),
  mapFromNavigationParam
)
