import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import { get } from 'lodash-es'
import { getSpeaker } from '../../../utils/helper'
import ItemShareDefault from '../ItemShareDefault'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareSession extends React.PureComponent {
  render() {
    const { data, navigateTo, isFeed } = this.props
    const { ads_status, speaker } = data
    const is_expired = ads_status === 'expired'
    let connectWith = getSpeaker(speaker)

    return (
      <TouchableOpacity
        disabled={!isFeed}
        activeOpacity={is_expired ? 1 : 0.5}
        onPress={() => navigateTo('SessionDetail', { id_ads: get(data, 'id') })}
      >
        <ItemShareDefault
          image={data.image}
          label={'Join Webinar'}
          title={data.title}
          subtitle={connectWith}
        />
      </TouchableOpacity>
    )
  }
}

ItemShareSession.propTypes = propsType
ItemShareSession.defaultProps = propsDefault
export default ItemShareSession
