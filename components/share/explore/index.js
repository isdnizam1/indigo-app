import React, { useState, useEffect } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { Loader, Text } from '../..'
import { WHITE, GREY_WARM, RED_GOOGLE } from '../../../constants/Colors'
import _enhancedNavigation from '../../../navigation/_enhancedNavigation'
import style from '../../../styles/components/ExploreShareComponent'
import { getExploreDataShare } from '../../../actions/api'
import ItemShareCollaboration from './ItemShareCollaboration'
import ItemShareSubmission from './ItemShareSubmission'
import ItemShareEvent from './ItemShareEvent'
import ItemShareNews from './ItemShareNews'
import ItemShareSession from './ItemShareSession'
import ItemShareVideo from './ItemShareVideo'
import ItemShareAlbum from './ItemShareAlbum'
import ItemShareEpisode from './ItemShareEpisode'
import ItemShareBand from './ItemShareBand'
import ItemShareSpeaker from './ItemShareSpeaker'

const ExploreComponent = (props) => {

  let [ads, setAds] = useState(null)
  const { id, navigation, navigateTo, showTopics, dispatch, userData: { id_user }, id_episode } = props

  const getAds = async () => {
    if (props.data) {
      setAds({ ...props.data, id })
    } else {
      let responseData = await dispatch(getExploreDataShare, { id, id_user, id_episode })
      setAds(responseData)
    }
  }

  const renderCard = () => {
    if(ads) {
      const { type } = ads
      const selector = props.data ? props.data.category : type
      const knownTypes = [
        'collaboration',
        'submission',
        'band',
        'event',
        'news_update',
        'sp_album',
        'video',
        'sc_session',
        'sc_speaker',
      ]
      if(knownTypes.indexOf(selector) >= 0) {
        const CardComponent = {
          collaboration: ItemShareCollaboration,
          submission: ItemShareSubmission,
          band: ItemShareBand,
          event: ItemShareEvent,
          news_update: ItemShareNews,
          sp_album: id_episode ? ItemShareEpisode : ItemShareAlbum,
          video: ItemShareVideo,
          sc_session: ItemShareSession,
          sc_speaker: ItemShareSpeaker,
        }[selector]
        return (<CardComponent isFeed navigateTo={navigateTo} navigation={navigation} data={ads} />)
      } else {
        return <Text>{selector}</Text>
      }
    }
  }

  useEffect(() => {
    getAds()
  }, [id])

  return (!navigation || !id) && __DEV__ ? (<View style={{ ...style.cardWrapper, padding: 20 }}><Text color={RED_GOOGLE} size={'tiny'}>Please make sure the following props exists :{'\n\n'}id, navigation</Text></View>) : (
    <View>
      <View style={[style.cardWrapper, ads == null ? { height: 90 } : { backgroundColor: WHITE, borderWidth: 0 }]}>
        <Loader size={'mini'} isLoading={!ads}>
          {renderCard()}
          {ads && showTopics && (<View style={style.topicWrapper}>
            {(ads.topic_name || []).map((topic) => (<View style={style.topic} key={Math.random()}>
              <Text color={GREY_WARM} weight={400} size={'tiny'}>{topic}</Text>
            </View>))}
          </View>)}
        </Loader>
      </View>
      {!ads && showTopics && (<View style={style.topicWrapper}>
        {(['···']).map((topic) => (<View style={{ ...style.topic, paddingHorizontal: 30 }} key={Math.random()}>
          <Text color={GREY_WARM} weight={400} size={'tiny'}>{topic}</Text>
        </View>))}
      </View>)}
    </View>
  )
}

const mapStateToProps = ({ auth }) => {
  return {
    userData: auth.user
  }
}

export default _enhancedNavigation(connect(mapStateToProps, {})(ExploreComponent))
