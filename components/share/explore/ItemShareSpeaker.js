import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import ItemShareDefault from '../ItemShareDefault'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareSpeaker extends React.PureComponent {
  render() {
    const { data, navigateTo, isFeed } = this.props
    const { ads_status, id_ads, data_session } = data
    const is_expired = ads_status === 'expired'
    const additionalData = JSON.parse(data.additional_data)
    return (
      <TouchableOpacity
        disabled={!isFeed}
        activeOpacity={is_expired ? 1 : 0.5}
        onPress={is_expired ? null : () => {
          navigateTo('SpeakerDetail', { id_ads })
        }}
      >
        <ItemShareDefault
          image={data.image}
          imageRound={true}
          label={data_session.length < 0 ? 'Join Webinar' : 'Visit Speaker'}
          title={data.title}
          subtitle={additionalData.profession}
        />
      </TouchableOpacity>
    )
  }
}

ItemShareSpeaker.propTypes = propsType
ItemShareSpeaker.defaultProps = propsDefault
export default ItemShareSpeaker
