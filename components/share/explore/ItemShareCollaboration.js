import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import ItemShareDefault from '../ItemShareDefault'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareCollaboration extends React.PureComponent {
  render() {
    const { data, navigateTo } = this.props
    const { ads_status } = data
    const is_expired = ads_status === 'expired'
    const additionalData = JSON.parse(data.additional_data)
    return (
      <TouchableOpacity
        disabled={is_expired}
        activeOpacity={is_expired ? 1 : 0.5}
        onPress={is_expired ? null : () => navigateTo('CollaborationPreview', { id: data.id })}
      >
        <ItemShareDefault
          showImage={false}
          label={'Join Collaboration'}
          title={data.title}
          subtitle={additionalData.location.name}
        />
      </TouchableOpacity>
    )
  }
}

ItemShareCollaboration.propTypes = propsType
ItemShareCollaboration.defaultProps = propsDefault
export default ItemShareCollaboration
