import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import { upperFirst } from 'lodash-es'
import ItemShareDefault from '../ItemShareDefault'
import { toNormalDate } from '../../../utils/date'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareSubmission extends React.PureComponent {
  render() {
    const { data, navigateTo, isFeed } = this.props
    const { ads_status } = data
    const is_expired = ads_status === 'expired'
    const additionalData = JSON.parse(data.additional_data)
    return (
      <TouchableOpacity
        disabled={!isFeed}
        activeOpacity={is_expired ? 1 : 0.5}
        onPress={is_expired ? null : () => navigateTo('SubmissionPreview', { id_ads: data.id })}
      >
        <ItemShareDefault
          image={data.image}
          label={'Join Audition'}
          title={data.title}
          subtitle={`${toNormalDate(additionalData.date.start)}  ∙  ${upperFirst(additionalData.location.name)}`}
        />
      </TouchableOpacity>
    )
  }
}

ItemShareSubmission.propTypes = propsType
ItemShareSubmission.defaultProps = propsDefault
export default ItemShareSubmission
