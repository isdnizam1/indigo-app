import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import ItemShareLarge from '../ItemShareLarge'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareVideo extends React.PureComponent {
  render() {
    const { data, navigateTo, isFeed } = this.props
    const { ads_status } = data
    const is_expired = ads_status === 'expired'
    return (
      <TouchableOpacity
        disabled={!isFeed}
        activeOpacity={is_expired ? 1 : 0.8}
        onPress={is_expired ? null : () => navigateTo('PremiumVideoTeaser', { id_ads: data.id })}
      >
        <ItemShareLarge
          isVideo={true}
          image={data.image}
          title={data.title}
          subtitle={'Eventeer'}
        />
      </TouchableOpacity>
    )
  }
}

ItemShareVideo.propTypes = propsType
ItemShareVideo.defaultProps = propsDefault
export default ItemShareVideo
