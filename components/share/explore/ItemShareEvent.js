import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import { upperFirst } from 'lodash-es'
import { toNormalDate } from '../../../utils/date'
import ItemShareDefault from '../ItemShareDefault'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareEvent extends React.PureComponent {
  render() {
    const { data, navigateTo, isFeed } = this.props
    const { ads_status } = data
    const is_expired = ads_status === 'expired'
    const additionalData = JSON.parse(data.additional_data)
    const subtitle = `${additionalData.location.name.length <= 25 ? additionalData.location.name : `${upperFirst(additionalData.location.name.substring(0, !episode ? additionalData.location.name.length : 25))}${additionalData.location.name.length > 25 ? '...' : ''}`}  ∙  ${upperFirst(is_expired ? 'Expired' : toNormalDate(additionalData.date.start))}`
    return (
      <TouchableOpacity
        disabled={!isFeed}
        activeOpacity={is_expired ? 1 : 0.5}
        onPress={is_expired ? null : () => navigateTo('AdsDetailScreenNoTab', { id_ads: data.id })}
      >
        <ItemShareDefault
          image={data.image}
          label={'Join Event'}
          title={data.title}
          subtitle={subtitle}
        />
      </TouchableOpacity>
    )
  }
}

ItemShareEvent.propTypes = propsType
ItemShareEvent.defaultProps = propsDefault
export default ItemShareEvent
