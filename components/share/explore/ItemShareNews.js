import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import ItemShareDefault from '../ItemShareDefault'

const propsType = {
  data: PropTypes.object,
  isFeed: PropTypes.bool
}

const propsDefault = {
  data: {},
  isFeed: false
}

class ItemShareNews extends React.PureComponent {
  render() {
    const { data, navigateTo, isFeed } = this.props
    const { ads_status } = data
    const is_expired = ads_status === 'expired'
    return (
      <TouchableOpacity
        disabled={!isFeed}
        activeOpacity={is_expired ? 1 : 0.5}
        onPress={is_expired ? null : () => navigateTo('AdsDetailScreenNoTab', { id_ads: data.id })}
      >
        <ItemShareDefault
          image={data.image}
          label={'Read Article'}
          title={data.title}
          subtitle={data.mins_read}
        />
      </TouchableOpacity>
    )
  }
}

ItemShareNews.propTypes = propsType
ItemShareNews.defaultProps = propsDefault
export default ItemShareNews
