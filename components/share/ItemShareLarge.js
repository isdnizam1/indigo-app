import React from 'react'
import { View } from 'react-native'
import { isFunction, upperFirst } from 'lodash-es'
import { GUN_METAL, PALE_BLUE, SHIP_GREY_CALM, WHITE } from '../../constants/Colors'
import { WP1, WP4, WP2, WP10 } from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'
import Image from '../Image'
import Text from '../Text'
import Icon from '../Icon'
import LinkPreviewCard from '../LinkPreviewCard'

const ItemShareLarge = ({
  title,
  titleLine = 2,
  subtitle,
  subtitleLine = 1,
  isVideo = false,
  isLink = false,
  image,
  aspectRatio = isVideo ? 1.7 : 1,
  radius = 6,
  loading
}) => {
  const titleAds = loading ? 'Title' : title,
    subtitleAds = subtitle

  return (
    <View style={{
      backgroundColor: WHITE,
      borderRadius: radius,
      ...SHADOW_STYLE['shadowThin']
    }}
    >
      <View
        style={{
          borderRadius: radius,
          overflow: 'hidden',
        }}
      >
        <View style={{ flex: 1, borderBottomColor: PALE_BLUE, borderBottomWidth: .7 }}>
          {isLink ? (
            <LinkPreviewCard
              text={image}
              imageOnly
              imageStyle={{ width: '100%', aspectRatio }}
            />
          ) : (
            <Image
              imageStyle={{ width: '100%' }}
              source={{ uri: image }}
              aspectRatio={aspectRatio}
            />
          )}
          {isVideo && (
            <View style={{ flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }} >
              <View>
                <Icon
                  name='play-circle'
                  size={WP10}
                  color={WHITE}
                  type='MaterialCommunityIcons'
                />
              </View>
            </View>
          )}
        </View>

        <View style={{ padding: WP4, paddingTop: WP2 }}>
          <Text numberOfLines={titleLine} type='Circular' size='mini' color={GUN_METAL} weight={500} style={{ marginBottom: WP1 }}>{upperFirst(titleAds)}</Text>
          {subtitleAds && (
            isFunction(subtitleAds) ?
              subtitleAds() :
              (<Text numberOfLines={subtitleLine} type='Circular' size='xmini' color={SHIP_GREY_CALM} weight={300}>{isVideo ? subtitleAds : upperFirst(subtitleAds)}</Text>)
          )}
        </View>

      </View>
    </View>
  )
}

export default ItemShareLarge