import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StatusBar, TouchableOpacity, View } from 'react-native'
import { noop } from 'lodash-es'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import * as ImageManipulator from 'expo-image-manipulator'
import Modal from '../Modal'
import { WP3 } from '../../constants/Sizes'
import MenuItem from '../../screens/create_session/component/MenuItem'

class ImageUploadModal extends Component {
  constructor(props) {
    super(props)
  }

  _selectPhoto = async () => {
    const { resizeWidth, resizeHeight, aspectRatio } = this.props
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCameraRoll === 'granted') {
      const { uri } = await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: true,
        aspect: aspectRatio
      })
      return await ImageManipulator.manipulateAsync(uri, [{
        resize: {
          width: resizeWidth,
          height: resizeHeight
        }
      }], {
        base64: true,
        compress: 0.9,
        format: ImageManipulator.SaveFormat.JPG
      })
    }
  }

  _takePhoto = async () => {
    const { resizeWidth, resizeHeight, aspectRatio } = this.props
    if (Platform.OS === 'ios') StatusBar.setHidden(true)
    const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
      const { uri } = await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: true,
        aspect: aspectRatio
      })
      return await ImageManipulator.manipulateAsync(uri, [{
        resize: {
          width: resizeWidth,
          height: resizeHeight
        }
      }], {
        base64: true,
        compress: 0.9,
        format: ImageManipulator.SaveFormat.JPG
      })
    }
  }

  _handlePhoto = async (result) => {
    if (Platform.OS === 'ios') StatusBar.setHidden(false)
    if (!result) {
      return
    }

    this.props.onChange(result)
  }

  render() {
    const {
      onChange, removable, children, contentWrapperStyle, disabled
    } = this.props

    return (
      <Modal
        renderModalContent={({ toggleModal }) => (
          <View
            style={{
              paddingVertical: WP3
            }}
          >
            <MenuItem
              onPress={() => {
                this._selectPhoto().then((result) => {
                  toggleModal()
                  this._handlePhoto(result)
                })
              }}
              label='Choose from Library'
            />
            <MenuItem
              label='Take Photo'
              onPress={() => {
                this._takePhoto().then((result) => {
                  toggleModal()
                  this._handlePhoto(result)
                })
              }}
            />

            {
              removable && (
                <MenuItem
                  label='Remove Current Photo'
                  onPress={() => {
                    toggleModal()
                    onChange({})
                  }}
                />
              )
            }
            <MenuItem
              type='separator'
            />
            <MenuItem
              image={require('../../assets/icons/close.png')}
              label='Tutup'
              onPress={toggleModal}
            />
          </View>
        )}
      >
        {
          ({ toggleModal }, M) => (
            <>
              <TouchableOpacity
                onPress={disabled ? noop : toggleModal} activeOpacity={0.8}
                style={{ flexDirection: 'row', alignItems: 'center', marginTop: WP3, ...contentWrapperStyle }}
              >
                {children}
              </TouchableOpacity>
              {M}
            </>
          )
        }
      </Modal>
    )
  }
}

ImageUploadModal.propTypes = {
  onChange: PropTypes.func,
  removable: PropTypes.bool,
  children: PropTypes.objectOf(PropTypes.any),
  contentWrapperStyle: PropTypes.objectOf(PropTypes.any),
  resizeWidth: PropTypes.number,
  resizeHeight: PropTypes.number,
  aspectRatio: PropTypes.arrayOf(PropTypes.number),
  disabled: PropTypes.bool,
}

ImageUploadModal.defaultProps = {
  onChange: noop,
  removable: false,
  contentWrapperStyle: {},
  resizeWidth: 620,
  resizeHeight: 620,
  aspectRatio: [1, 1],
  disabled: false
}

export default ImageUploadModal
