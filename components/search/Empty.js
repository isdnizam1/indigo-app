import React from 'react'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { Text, Image } from 'sf-components'
import Spacer from 'sf-components/Spacer'
import { WP4, WP5, WP20 } from 'sf-constants/Sizes'
import { SHIP_GREY_CALM } from 'sf-constants/Colors'

const style = StyleSheet.create({
  container: { paddingHorizontal: WP20 }
})

class Empty extends React.PureComponent {

  render() {
    const { query } = this.props
    return (
      <View style={style.container}>
        <Spacer size={WP20} />
        <Image size={'extraMassive'} source={require('sf-assets/images/emptyMagnifier.png')} />
        <Spacer size={WP4} />
        <Text
          lineHeight={WP5}
          size={'slight'}
          color={SHIP_GREY_CALM}
          type={'Circular'}
          centered
        >
          {query ? `Maaf, kami tidak menemukan pencarian untuk “${query}”` : 'Maaf, kami tidak menemukan hasil untuk pencarian terkait'}
        </Text>
      </View>
    )
  }

}

export default connect(
  ({ search: { query } }) => ({ query }),
  {}
)(Empty)
