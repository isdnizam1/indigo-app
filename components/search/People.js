import React from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { startCase, toLower } from 'lodash-es'
import { Text, Avatar } from '../'
import { WP1, WP4, HP2 } from '../../constants/Sizes'
import { GREY_WARM, GREY_LABEL } from '../../constants/Colors'

class People extends React.PureComponent {

  render() {
    const {
      navigateTo,
      user: { id_user, full_name, job_title, city_name, country_name, profile_picture }
    } = this.props
    return (<TouchableOpacity onPress={() => navigateTo('ProfileScreenNoTab', { idUser: id_user })}>
      <View style={[style.wrapper, this.props.style]}>
        <Avatar size={'small'} image={profile_picture} />
        <View style={style.descriptions}>
          <Text color={GREY_LABEL} weight={500} size={'tiny'}>{full_name}</Text>
          <Text color={GREY_WARM} weight={300} size={'xtiny'} style={style.job}>{job_title}</Text>
          <Text color={GREY_WARM} weight={300} size={'xtiny'}>{startCase(toLower(city_name))}, {country_name}</Text>
        </View>
      </View>
    </TouchableOpacity>)
  }

}

const style = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: HP2,
    paddingLeft: WP1
  },
  descriptions: {
    paddingLeft: WP4
  },
  job: {
    marginVertical: 2
  }
})

export default React.memo(People)
