import React from 'react'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { GREY20, GREY_LABEL, TRANSPARENT } from '../../constants/Colors'
import { Text } from '../../components'
import { TABS } from '../../constants/Search'
import { WP1, WP2, WP4, HP1, HP2 } from '../../constants/Sizes'

class SearchTabs extends React.PureComponent {

  render() {
    const { onTabPress, activeTab } = this.props

    return (<View style={style.wrapper}>
      {(TABS).map((tab, index) => {
        const active = tab.type == activeTab
        console.log(tab.type)
        return (
          <TouchableOpacity key={index} onPress={() => alert(tab.type)} style={[style.tab, active ? style.active : null]}>
            <Text
              centered
              type={'NeoSans'}
              key={tab.type}
              color={active ? GREY_LABEL : GREY20}
              weight={500}
              size={'xmini'}
            >
              {'ssss'}
            </Text>
          </TouchableOpacity>)
      })}
    </View>)
  }

}

const style = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: WP4
  },
  tab: {
    paddingTop: HP2 - 6,
    paddingBottom: HP1,
    paddingHorizontal: WP2,
    marginHorizontal: WP1 - 1,
    borderBottomWidth: 3,
    borderBottomColor: TRANSPARENT,
    flexGrow: 1
  },
  active: {
    borderBottomColor: GREY_LABEL
  }
})

export default SearchTabs