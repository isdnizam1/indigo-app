import React, { Component } from 'react'
import { View, StyleSheet, FlatList } from 'react-native'
import Spacer from 'sf-components/Spacer'
import Text from 'sf-components/Text'
import Icon from 'sf-components/Icon'
import Touchable from 'sf-components/Touchable'
import InteractionManager from 'sf-utils/InteractionManager'
import { isEmpty, upperFirst } from 'lodash'
import { GUN_METAL, SHIP_GREY, SHIP_GREY_CALM } from 'sf-constants/Colors'
import { WP1, WP3, WP5, WP6 } from 'sf-constants/Sizes'
import { connect } from 'react-redux'
import { getRecent, setRecent } from 'sf-utils/storage'
import { KEY_RECENT_SEARCH } from 'sf-constants/Storage'
import {
  setRecentSearch,
  setQuery,
  setSearchingState,
} from 'sf-services/search/actionDispatcher'

const style = StyleSheet.create({
  recents: {
    paddingVertical: WP6,
    paddingHorizontal: WP5,
  },
  recent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgb(244, 246, 248)',
  },
  recentQuery: {
    paddingVertical: WP3,
    flex: 1,
  },
  recentDelete: {
    paddingVertical: WP3,
    paddingHorizontal: WP1,
  },
})

const mapStateToProps = ({ search: { recents } }) => ({
  recents,
})

const mapDispatchToProps = {
  setRecentSearch,
  setQuery,
  setSearchingState,
}

class Recents extends Component {
  constructor(props) {
    super(props)
    this._readLocalStorage = this._readLocalStorage.bind(this)
    this._onPress = this._onPress.bind(this)
    this._onDelete = this._onDelete.bind(this)
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(this._readLocalStorage)
  }

  _readLocalStorage = () => {
    const { setRecentSearch } = this.props
    getRecent(KEY_RECENT_SEARCH).then((recents) => {
      setRecentSearch(recents.filter((recent) => !!recent))
    })
  };

  _onPress = (item) => {
    const { setSearchingState, setQuery } = this.props
    setSearchingState(true)
    setQuery(item)
  };

  _onDelete = (item) => {
    const { setRecentSearch, recents } = this.props
    const newRecents = recents.filter((q) => q != item)
    setRecentSearch(newRecents)
    setRecent(KEY_RECENT_SEARCH, newRecents)
  };

  _renderRecent = ({ item, index }) => {
    return (
      <View style={style.recent}>
        <Touchable
          onPress={() => this._onPress(item)}
          style={style.recentQuery}
        >
          <Text size={'slight'} color={SHIP_GREY}>
            {upperFirst(item)}
          </Text>
        </Touchable>
        <Touchable onPress={() => this._onDelete(item)} style={style.recentDelete}>
          <Icon size={'xmini'} color={SHIP_GREY_CALM} name={'close'} />
        </Touchable>
      </View>
    )
  };

  _keyExtractor = (item) => `recent-${item}`;

  render() {
    const { recents: recentSearch } = this.props
    return (
      <View style={style.recents}>
        {!isEmpty(recentSearch) && (
          <Text color={GUN_METAL} type={'Circular'} weight={600}>
            Pencarian Terakhir
          </Text>
        )}
        <Spacer size={WP3} />
        <FlatList
          data={recentSearch}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderRecent}
        />
      </View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Recents)
