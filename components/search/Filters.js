import React, { isValidElement } from 'react'
import { ScrollView, View, StyleSheet, TouchableOpacity } from 'react-native'
import { WP05, WP105, WP2, WP3, WP5 } from 'sf-constants/Sizes'
import { SHIP_GREY, CLEAR_BLUE, WHITE, PALE_BLUE } from 'sf-constants/Colors'
import { connect } from 'react-redux'
import { isFunction, get, isEmpty, isNil, pick, noop } from 'lodash-es'
import produce from 'immer'
import Text from '../Text'
import Spacer from '../Spacer'
import ButtonV2 from '../ButtonV2'
import VisibleView from '../VisibleView'

const style = StyleSheet.create({
  scrollView: {
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  container: {
    paddingVertical: WP3,
    flexDirection: 'row',
    alignItems: 'center',
  },
  option: {
    paddingVertical: WP105,
    paddingHorizontal: WP3,
    marginRight: WP2,
    marginLeft: WP05,
  },
})

class Filters extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      initialPosition: props.filterPosition
    }
  }

  layoutPosition = []

  _onLayout = (index) => ({ nativeEvent: { layout } }) => {
    this.layoutPosition[index] = layout
  }

  _renderOption = (item, index) => {
    const isActive = get(item, 'active')
    return isValidElement(item) ? (<View onLayout={this._onLayout(index)} key={`people-filter-${index}`}>
      <TouchableOpacity onPressIn={() => alert(1)}>
        {item}
      </TouchableOpacity>
    </View>) : (
      <View onLayout={this._onLayout(index)}>
        <ButtonV2
          onPress={() => get(item, 'onPress')(pick(this.layoutPosition[index], ['x', 'y']))}
          color={isActive ? CLEAR_BLUE : null}
          textColor={isActive ? WHITE : SHIP_GREY}
          textWeight={400}
          style={style.option}
          text={get(item, 'text')}
        />
      </View>
    )
  };

  componentDidMount() {
    const { currentTab, peopleFilters } = this.props
    if(!isNil(this.scrollView)) {
      try {
        if(currentTab === 'people' && !!peopleFilters.interest_name) {
          setTimeout(isFunction(get(this.scrollView, 'scrollToEnd')) ? () => this.scrollView.scrollToEnd() : noop, 0)
        } else {
          setTimeout(isFunction(get(this.scrollView, 'scrollTo')) ? () => {
            try {
              this.scrollView.scrollTo(produce(this.state.initialPosition, (draft) => {
                draft.animated = false
              }))
            } catch(e) {
              // keep silent
            }
          } : noop, 0)
        }
      } catch (e) {
        // keep silent
      }
    }
  }

  _onRef = (ref) => {
    this.scrollView = ref
  }

  render() {
    const { options } = this.props
    return (
      <VisibleView visible={!isEmpty(options)}>
        <ScrollView
          ref={this._onRef}
          style={style.scrollView}
          showsHorizontalScrollIndicator={false}
          horizontal
        >
          <Spacer horizontal size={WP5} />
          <View style={style.container}>
            <Text
              color={SHIP_GREY}
              weight={500}
              size={'mini'}
              type={'Circular'}
            >
              {'Filter:  '}
            </Text>
            {options.map(this._renderOption)}
          </View>
          <Spacer horizontal size={WP3} />
        </ScrollView>
      </VisibleView>
    )
  }
}

export default connect((
  {
    search: { currentTab, peopleFilters }
  }
) => ({ currentTab, peopleFilters }), {})(Filters)
