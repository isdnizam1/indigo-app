import SearchBox from './SearchBox'
import SearchTabs from './SearchTabs'
import Peoples from './Peoples'
import People from './People'
import Activities from './Activities'
import Recents from './Recents'

export {
  SearchBox,
  SearchTabs,
  Peoples,
  People,
  Activities,
  Recents
}
