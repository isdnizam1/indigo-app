import React from 'react'
import { View, FlatList, StyleSheet } from 'react-native'
import { KeyboardAwareScrollView as ScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'
import { isEmpty, get } from 'lodash-es'
import { WP2, WP3, WP4, WP5, WP30 } from 'sf-constants/Sizes'
import { NAVY_DARK, PALE_BLUE, REDDISH } from 'sf-constants/Colors'
import { isEqualObject } from 'sf-utils/helper'
import {
  setActivityFilter,
  setFilterPosition,
} from 'sf-services/search/actionDispatcher'
import { LEVEL1 } from 'sf-constants/Search'
import { Text } from '../'
import VisibleView from '../VisibleView'
import Spacer from '../Spacer'
import Touchable from '../Touchable'
import ExploreComponent from '../share/explore'
import Filters from './Filters'
import Empty from './Empty'

const style = StyleSheet.create({
  section: {
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
  heading: {
    paddingLeft: WP5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: WP3,
  },
  seeAll: {
    paddingHorizontal: WP5,
    paddingVertical: WP2,
  },
  ad: {
    marginBottom: WP3,
    paddingHorizontal: WP5,
  },
})

class Activities extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      filters: [],
      filterKey: Math.random().toString(),
      filterVisible: false,
      noResults: false,
    }
    this._renderSection = this._renderSection.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    // const shouldUpdate = nextProps.isVisible
    return true
  }

  static getDerivedStateFromProps(props, state) {
    const data = get(props, 'data')
    if (data) {
      const filters = get(props, 'data')
        .filter(({ data }) => !isEmpty(data))
        .map(({ heading: text, id }) => ({
          text,
          onPress: (filterPosition) => {
            props.setActivityFilter(props.filter == id ? null : id)
            props.setFilterPosition({
              key: 'activity',
              value: filterPosition,
            })
          },
          active: props.filter == id,
        }))
      const noResults = isEmpty(filters)
      const filterVisible = filters.length > 1 && !!props.currentTab
      const filterKey =
        data.length > 1 && !isEqualObject(state.filters, filters)
          ? Math.random().toString()
          : state.filterKey

      return {
        filterKey,
        filterVisible,
        filters,
        noResults,
      }
    }
    return null
  }

  _sectionKey = ({ id }) => id;

  _adKey = ({ id_ads: id }) => id;

  _renderAd = ({ item }, index) => {
    return (
      <View style={style.ad}>
        <ExploreComponent
          id={get(item, 'id_ads')}
          navigation={this.props.navigation}
          data={item}
        />
      </View>
    )
  };

  _renderSection = ({ item }, index) => {
    const { filter, navigation } = this.props
    const visible =
      !isEmpty(get(item, 'data')) && (!filter || filter == get(item, 'id'))
    const level1 = get(LEVEL1, `${get(item, 'id')}.screen`)
    const params = get(LEVEL1, `${get(item, 'id')}.params`)
    return (
      <VisibleView visible={visible}>
        <View style={style.section}>
          <Spacer size={WP4} />
          <View style={style.heading}>
            <Text type={'Circular'} weight={600} color={NAVY_DARK}>
              {get(item, 'heading')
                .replace('Sp Album', 'Podcast')
                .replace('News Update', 'News')
                .replace('Submission', 'Audition')}
            </Text>
            <VisibleView visible={!!level1}>
              <Touchable
                onPress={() => navigation.push(level1, params, 'push')}
                style={style.seeAll}
              >
                <Text size={'xmini'} weight={400} color={REDDISH} type={'Circular'}>
                  Lihat Semua
                </Text>
              </Touchable>
            </VisibleView>
          </View>
          <FlatList
            keyExtractor={this._adKey}
            renderItem={this._renderAd}
            data={get(item, 'data')}
          />
          <Spacer size={WP4} />
        </View>
      </VisibleView>
    )
  };

  render() {
    const { data, filter, isSearching, currentTab, filterPosition } = this.props
    const { filters, filterKey, filterVisible, noResults } = this.state
    const ParentView = !currentTab ? View : ScrollView
    return (
      <ParentView keyboardShouldPersistTaps='handled'>
        <VisibleView visible={noResults && !isSearching && !!currentTab}>
          <Empty />
        </VisibleView>
        <VisibleView key={filterKey} visible={filterVisible}>
          <Filters
            filterPosition={filterPosition}
            key={filter || 'activity-filter'}
            options={filters}
          />
        </VisibleView>
        <FlatList
          extraData={this.props}
          keyExtractor={this._sectionKey}
          renderItem={this._renderSection}
          data={data || []}
        />
        <Spacer size={WP30} />
      </ParentView>
    )
  }
}

export default connect(
  ({
    search: {
      activityFilter: filter,
      isSearching,
      currentTab,
      filterPosition: { activity: filterPosition },
    },
  }) => ({
    filter,
    isSearching,
    currentTab,
    filterPosition,
  }),
  { setActivityFilter, setFilterPosition },
)(Activities)
