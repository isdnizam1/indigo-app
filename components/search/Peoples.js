import React from 'react'
import { View, StyleSheet, FlatList } from 'react-native'
import { KeyboardAwareScrollView as ScrollView } from 'react-native-keyboard-aware-scroll-view'
import Text from 'sf-components/Text'
import { connect } from 'react-redux'
import Touchable from 'sf-components/Touchable'
import Avatar from 'sf-components/Avatar'
import Icon from 'sf-components/Icon'
import Spacer from 'sf-components/Spacer'
import ButtonV2 from 'sf-components/ButtonV2'
import SelectModalV3 from 'sf-components/SelectModalV3'
import VisibleView from 'sf-components/VisibleView'
import { get, startCase, toLower, isEmpty } from 'lodash-es'
import { WP05, WP105, WP2, WP3, WP5 } from 'sf-constants/Sizes'
import {
  CITY_SUGGESTIONS,
  JOB_SUGGESTIONS,
  GENRE_SUGGESTIONS,
} from 'sf-constants/Search'
import { getCity, getGenreInterest, getJob } from 'sf-actions/api'
import {
  REDDISH,
  GUN_METAL,
  SHIP_GREY_CALM,
  PALE_BLUE,
  CLEAR_BLUE,
  SHIP_GREY,
  WHITE,
} from 'sf-constants/Colors'
import { setPeopleFilter, setFilterPosition } from 'sf-services/search/actionDispatcher'
import Filters from './Filters'
import Empty from './Empty'

const style = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: WP3,
    paddingHorizontal: WP5,
  },
  rowDescription: {
    flex: 1,
    paddingLeft: WP3,
  },
  heading: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: WP5,
    justifyContent: 'space-between',
  },
  seeAll: {
    paddingHorizontal: WP5,
    paddingVertical: WP2,
  },
  section: {
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
    paddingBottom: WP5,
  },
  title: {
    paddingVertical: WP2,
  },
  option: {
    paddingVertical: WP105,
    paddingHorizontal: WP3,
    marginRight: WP2,
    marginLeft: WP05,
  },
  chevron: {
    marginLeft: WP05 * 2,
  },
})

class Peoples extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      filters: [],
      peopleFiltered: false,
    }
    this._renderLoadMorePeople = this._renderLoadMorePeople.bind(this)
    this._renderLoadMoreArtist = this._renderLoadMoreArtist.bind(this)
  }

  static getDerivedStateFromProps(props, state) {
    const { city_name, job_title, interest_name } = props.peopleFilters
    return {
      peopleFiltered:
        [city_name, job_title, interest_name].filter((value) => !!value)
          .length > 0,
      filters: [
        // eslint-disable-next-line react/jsx-key
        <SelectModalV3
          selection={city_name}
          onRemoveSelection={() => {
            props.setPeopleFilter({ city_name: '' })
          }}
          showSelection={!!city_name}
          refreshOnSelect
          quickSearchTitle={'Saran Pencarian Kota'}
          quickSearchList={CITY_SUGGESTIONS}
          quickSearchKey={'city_name'}
          triggerComponent={
            <ButtonV2
              color={city_name ? CLEAR_BLUE : null}
              textColor={city_name ? WHITE : SHIP_GREY}
              textWeight={400}
              style={style.option}
              rightComponent={() => (
                <View style={style.chevron}>
                  <Icon
                    color={city_name ? WHITE : SHIP_GREY_CALM}
                    type={'Entypo'}
                    name={'chevron-down'}
                  />
                </View>
              )}
              text={city_name ? city_name : 'Semua Kota'}
            />
          }
          header='Kota/Lokasi'
          suggestion={getCity}
          suggestionKey='city_name'
          suggestionPathResult='city_name'
          suggestionPathValue='id_city'
          onChange={(city_name) => {
            if (city_name === 'Semua Kota') city_name = ''
            props.setPeopleFilter({ city_name })
          }}
          CITY_SUGGESTIONS
          suggestionCreateNewOnEmpty={true}
          createNew={false}
          asObject={false}
          showSuggestion={true}
          reformatFromApi={(text) => startCase(toLower(text || ''))}
          placeholder='Coba cari Kota'
        />,
        // eslint-disable-next-line react/jsx-key
        <SelectModalV3
          selection={job_title}
          showSelection={!!job_title}
          onRemoveSelection={() => {
            props.setPeopleFilter({ job_title: '' })
          }}
          refreshOnSelect
          quickSearchTitle={'Saran Pencarian Profesi'}
          quickSearchList={JOB_SUGGESTIONS}
          quickSearchKey={'job_title'}
          triggerComponent={
            <ButtonV2
              color={job_title ? CLEAR_BLUE : null}
              textColor={job_title ? WHITE : SHIP_GREY}
              textWeight={400}
              style={style.option}
              rightComponent={() => (
                <View style={style.chevron}>
                  <Icon
                    color={job_title ? WHITE : SHIP_GREY_CALM}
                    type={'Entypo'}
                    name={'chevron-down'}
                  />
                </View>
              )}
              text={job_title ? job_title : 'Semua Profesi'}
            />
          }
          header='Profesi'
          suggestion={getJob}
          suggestionKey='job_title'
          suggestionPathResult='job_title'
          onChange={(job_title) => {
            if (job_title === 'Semua Profesi') job_title = ''
            props.setPeopleFilter({ job_title })
          }}
          suggestionCreateNewOnEmpty={true}
          createNew={false}
          asObject={false}
          showSuggestion={true}
          reformatFromApi={(text) => startCase(toLower(text || ''))}
          placeholder='Coba cari Profesi'
        />,
        // eslint-disable-next-line react/jsx-key
        <SelectModalV3
          selection={interest_name}
          showSelection={!!interest_name}
          onRemoveSelection={() => {
            props.setPeopleFilter({ interest_name: '' })
          }}
          refreshOnSelect
          quickSearchTitle={'Saran Pencarian Interest'}
          quickSearchList={GENRE_SUGGESTIONS}
          quickSearchKey={'interest_name'}
          triggerComponent={
            <ButtonV2
              color={interest_name ? CLEAR_BLUE : null}
              textColor={interest_name ? WHITE : SHIP_GREY}
              textWeight={400}
              style={style.option}
              rightComponent={() => (
                <View style={style.chevron}>
                  <Icon
                    color={interest_name ? WHITE : SHIP_GREY_CALM}
                    type={'Entypo'}
                    name={'chevron-down'}
                  />
                </View>
              )}
              text={interest_name ? interest_name : 'Semua Interest'}
            />
          }
          header='Interest'
          suggestion={getGenreInterest}
          suggestionKey='interest_name'
          suggestionPathResult='interest_name'
          onChange={(interest_name) => {
            if (interest_name === 'Semua Interest') interest_name = ''
            props.setPeopleFilter({ interest_name })
          }}
          suggestionCreateNewOnEmpty={true}
          createNew={false}
          asObject={false}
          showSuggestion={true}
          reformatFromApi={(text) => startCase(toLower(text || ''))}
          placeholder='Coba cari Interest'
        />,
      ],
    }
  }

  _keyPeople = (item) => `people-${item.id_user}`;

  _keyArtist = (item) => `artist-${item.id_user}`;

  _renderPeople = ({ item: people }, index) => {
    return (
      <Touchable
        onPress={() =>
          this.props.navigateTo('ProfileScreenNoTab', {
            idUser: get(people, 'id_user'),
          })
        }
      >
        <View style={style.row}>
          <View>
            <Avatar verifiedStatus={get(people, 'verified_status')} image={get(people, 'profile_picture')} />
          </View>
          <View style={style.rowDescription}>
            <Text
              size={'slight'}
              weight={500}
              color={GUN_METAL}
              type={'Circular'}
            >
              {startCase(get(people, 'full_name'))}
            </Text>
            <VisibleView visible={!isEmpty(get(people, 'interest'))}>
              <Text
                size={'xmini'}
                weight={300}
                color={SHIP_GREY_CALM}
                type={'Circular'}
              >
                {(get(people, 'interest') || []).join(', ')}
              </Text>
            </VisibleView>
          </View>
        </View>
      </Touchable>
    )
  };

  _findArtist = () => {
    this.props.navigateTo('FindFriendScreen', { initialTab: 0 })
  };

  _findPeople = () => {
    this.props.navigateTo('FindFriendScreen', { initialTab: 1 })
  };

  _renderLoadMorePeople = () => {
    return (
      <ButtonV2
        onPress={() => this.props.onMore('people')}
        borderColor={'transparent'}
        text={'See More'}
        textColor={REDDISH}
        rightComponent={
          <Icon color={REDDISH} type={'Entypo'} name={'chevron-down'} />
        }
      />
    )
  };

  _renderLoadMoreArtist = () => {
    return (
      <ButtonV2
        onPress={() => this.props.onMore('artist')}
        borderColor={'transparent'}
        text={'See More'}
        textColor={REDDISH}
        rightComponent={
          <Icon color={REDDISH} type={'Entypo'} name={'chevron-down'} />
        }
      />
    )
  };

  _onFilterPress = (value) => {
    this.props.setFilterPosition({
      key: 'people',
      value
    })
  }

  render() {
    const { filters } = this.state
    const {
      peoples = [],
      artists = [],
      isSearching = false,
      currentTab = null,
      loadMorePeople = false,
      loadMoreArtist = false,
      filterPosition
    } = this.props
    const ParentView = !currentTab ? View : ScrollView
    return (
      <ParentView keyboardShouldPersistTaps='handled'>
        <VisibleView key={JSON.stringify({ filters })} visible={!!currentTab}>
          <Filters filterPosition={filterPosition} options={filters} />
        </VisibleView>
        <VisibleView
          visible={
            isEmpty(peoples) && isEmpty(artists) && !isSearching && !!currentTab
          }
        >
          <Empty />
        </VisibleView>
        <VisibleView
          key={`people-section-${JSON.stringify(peoples)}`}
          visible={!isEmpty(peoples)}
        >
          <View style={style.section}>
            <Spacer size={WP3} />
            <View style={style.heading}>
              <Text style={style.title} weight={600} type={'Circular'}>
                People
              </Text>
              <Touchable onPress={this._findPeople} style={style.seeAll}>
                <Text
                  size={'xmini'}
                  weight={400}
                  color={REDDISH}
                  type={'Circular'}
                >
                  Lihat Semua
                </Text>
              </Touchable>
            </View>
            <FlatList
              ListFooterComponent={
                loadMorePeople ? this._renderLoadMorePeople : null
              }
              keyExtractor={this._keyPeople}
              renderItem={this._renderPeople}
              data={peoples}
            />
          </View>
        </VisibleView>
        {/* <VisibleView
          key={`artist-section-${JSON.stringify(artists)}`}
          visible={!isEmpty(artists)}
        >
          <View style={style.section}>
            <Spacer size={WP3} />
            <View style={style.heading}>
              <Text style={style.title} weight={600} type={'Circular'}>
                Artist
              </Text>
              <Touchable onPress={this._findArtist} style={style.seeAll}>
                <Text
                  size={'xmini'}
                  weight={400}
                  color={REDDISH}
                  type={'Circular'}
                >
                  Lihat Semua
                </Text>
              </Touchable>
            </View>
            <FlatList
              ListFooterComponent={
                loadMoreArtist ? this._renderLoadMoreArtist : null
              }
              keyExtractor={this._keyArtist}
              renderItem={this._renderPeople}
              data={artists}
            />
          </View>
        </VisibleView> */}
      </ParentView>
    )
  }
}

export default connect(
  (
    {
      search: {
        isSearching,
        currentTab,
        peopleFilters,
        filterPosition: {
          people: filterPosition
        }
      }
    }
  ) => (
    {
      isSearching,
      currentTab,
      peopleFilters,
      filterPosition
    }
  ),
  { setPeopleFilter, setFilterPosition }
)(Peoples)
