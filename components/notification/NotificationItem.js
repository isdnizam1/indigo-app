import React from 'react'
import { StyleSheet, View } from 'react-native'
import moment from 'moment'
import { get, upperFirst } from 'lodash-es'
import HTMLElement from 'react-native-render-html'
import Avatar from '../Avatar'
import { FONT_SIZE, WP05, WP4, WP3, WP2, WP1, WP105, WP5, WP7 } from '../../constants/Sizes'
import { PALE_BLUE, SHIP_GREY_CALM, GUN_METAL, PALE_GREY_THREE } from '../../constants/Colors'
import Text from '../Text'
import Spacer from '../Spacer'
import Image from '../Image'
import ButtonV2 from '../ButtonV2'

const STYLE = StyleSheet.create({
  notificationItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: PALE_GREY_THREE,
    borderBottomWidth: 1,
    paddingVertical: WP4
  },
  avatarContainer: {
    marginRight: WP3
  },
  contentContainer: {
    flex: 1
  },
  username: {
    fontWeight: 'bold'
  },
  message: {},
  timestamp: {
    color: SHIP_GREY_CALM
  }
})

const NotificationItem = ({ imageSrc, username, message, timestamp, status, isActivity, notification }) => {
  const greenLabel = isActivity && ['Terdaftar', 'Terpilih', 'Aktif'].indexOf(get(notification, 'ads.label_status')) >= 0
  return (<View style={[STYLE.notificationItem, { alignItems: isActivity ? 'flex-start' : 'center' }]}>
    <View style={[STYLE.avatarContainer]}>
      {isActivity
        ? <Image imageStyle={{ borderRadius: WP2, borderWidth: 1, borderColor: PALE_BLUE }} size={'massive'} source={{ uri: get(notification, 'ads.image') }} />
        : <Avatar
          newItem={status == 'unread'}
          shadow={false}
          passValidation
          image={(!!imageSrc && imageSrc.indexOf('logo') >= 0) || !imageSrc ? require('sf-assets/icons/v3/informationCircleGray.png') : imageSrc}
          />
      }
    </View>
    {isActivity
      ? <View style={[STYLE.contentContainer]}>
        <Text color={SHIP_GREY_CALM} type={'Circular'} size='xmini'>{message}</Text>
        <Spacer size={WP05} />
        <Text weight={600} numberOfLines={2} color={GUN_METAL} type={'Circular'} size='slight'>{upperFirst(get(notification, 'ads.title'))}</Text>
        <Spacer size={WP1} />
        <View style={{ flexDirection: 'row' }}>
          <ButtonV2
            textColor={greenLabel ? 'rgb(80, 184, 60)' : SHIP_GREY_CALM}
            color={greenLabel ? 'rgb(237, 248, 235)' : PALE_GREY_THREE}
            borderColor={greenLabel ? 'rgb(237, 248, 235)' : PALE_GREY_THREE}
            forceBorderColor
            style={{ height: WP7, paddingVertical: 0 }}
            text={get(notification, 'ads.label_status')}
          />
        </View>
        <Spacer size={WP105} />
        <Text type={'Circular'} size='xmini' style={[STYLE.timestamp]}>{upperFirst(moment(timestamp).locale('id').fromNow(true))} lalu</Text>
      </View>
      : <View style={[STYLE.contentContainer]}>
        <HTMLElement
          html={message}
          baseFontStyle={{ color: GUN_METAL, fontFamily: 'CircularBook', lineHeight: WP5, fontSize: FONT_SIZE['xmini'] }}
        />
        <Spacer size={WP05} />
        <Text type={'Circular'} size='xmini' style={[STYLE.timestamp]}>{upperFirst(moment(timestamp).locale('id').fromNow(true))} lalu</Text>
      </View>
    }
  </View>)
}

NotificationItem.propTypes = {}
NotificationItem.defaultProps = {}
export default React.memo(NotificationItem)
