import { split } from 'lodash-es'
import { NavigateToInternalBrowser } from '../../utils/helper'
import { getNotifMappingList } from '../../utils/storage'

/*
  url_mobile: oneOf(['internal_link', 'external_link'])
  url: string
 */
export const getNotifMappingAction = async ({ url_mobile, url }) => {
  const actionList = await getNotifMappingList()

  if (url_mobile === 'external_link') {
    return {
      direct: () => {
        NavigateToInternalBrowser({
          url,
        })
      },
    }
  } else {
    const splitUrl = split(url, '/')
    const key = splitUrl[0]
    const activeNotif = actionList[key] || actionList[url_mobile]
    // console.log(actionList)
    if (activeNotif) {
      let payload = {}
      activeNotif.payloadKey.map((item, index) => {
        payload[item] = splitUrl[index + 1]
      })
      if (activeNotif.screen === 'SoundfrenLearnScreen') {
        payload['tabToShow'] =
          key === 'soundfren_video' || key === '1000startup_video' ? 1 : 0
      }
      if (key === '1000startup_video') {
        payload['tabToShow'] = 1
        payload['isStartup'] = true
      }
      if (key === '1000startup_learn') {
        payload['tabToShow'] = 0
        payload['isStartup'] = true
      }
      if (key === '1000startup_add_post') {
        payload['addPost'] = true
      }
      if (key === '1000startup_find_friends') {
        payload['isStartup'] = true
      }
      if (key === '1000startup_submit_submission') {
        payload['id_ads'] = '7'
      }
      if (key === 'mission_gamification') {
        payload['isStartup'] = true
        activeNotif.screen = 'StartupMissionHomeScreen'
      }
      // __DEV__ && console.log({ key })
      // if (activeNotif.screen === 'Forum1000Startup') {
      //   payload['tabToShow'] = key === 'soundfren_video' ? 1 : 0
      // }
      return {
        to: activeNotif.screen.replace(/ProfileScreen/g, 'ProfileScreenNoTab'),
        payload,
      }
    }
  }
}

export default {
  getNotifMappingAction,
}
