import React from 'react'
import { Image, StyleSheet, View } from 'react-native'
import Text from '../Text'
import { WP405 } from '../../constants/Sizes'

const style = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexGrow: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
  },
  headerText: {
    marginVertical: WP405,
  },
  contentText: {

  },
})

const EmptyNotification = () => (
  <View style={[style.wrapper]}>
    <Image style={[style.image]} source={require('../../assets/images/nonotif.jpg')} />
    <Text size='large' weight={500} style={[style.headerText]}>No Notification Yet</Text>
    <Text size='small' centered style={[style.contentText]}>Stay tuned! Notification about your activity will show up here</Text>
  </View>
)

EmptyNotification.propTypes = {}

EmptyNotification.defaultProps = {}

export default EmptyNotification
