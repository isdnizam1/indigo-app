import React from "react";
import { View } from "react-native";
import SkeletonContent from "react-native-skeleton-content";
import { startCase, toLower } from "lodash-es";
import { WP40, WP1, WP4, WP305, WP3, WP05 } from "../../constants/Sizes";
import Image from "../Image";
import { SHADOW_STYLE, TOUCH_OPACITY } from "../../constants/Styles";
import Text from "../Text";
import {
  GUN_METAL,
  SHIP_GREY_CALM,
  WHITE_MILK,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
} from "../../constants/Colors";
import Avatar from "../Avatar";
import CollaborationSkeleton from "./CollaborationSkeleton";
import { TouchableOpacity } from "react-native-gesture-handler";

const ColalboratorItem = ({
  ads,
  loading = true,
  simpleListItem = false,
  onPressItem,
}) => {
  const title = loading ? "Collaborator Name" : ads.full_name,
    genre = loading ? "Job Title" : ads.job_title;

  if (simpleListItem) {
    return (
      <View style={{ flex: 1, flexDirection: "row", alignItems: "flex-start" }}>
        <View style={{ marginRight: WP3 }}>
          <Avatar image={ads.profile_picture} isLoading={loading} />
        </View>
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[
            CollaborationSkeleton.layout.title,
            CollaborationSkeleton.layout.subtitle,
            CollaborationSkeleton.layout.subtitle,
          ]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <View>
            <Text
              numberOfLines={1}
              type="Circular"
              color={GUN_METAL}
              size="mini"
              weight={500}
              style={{ marginBottom: WP05 }}
            >
              {ads?.full_name}
            </Text>
            <Text
              numberOfLines={1}
              type="Circular"
              color={SHIP_GREY_CALM}
              size="xmini"
              weight={300}
              style={{ marginBottom: WP05 }}
            >
              {ads?.job_title}
            </Text>
            <Text
              numberOfLines={1}
              type="Circular"
              color={SHIP_GREY_CALM}
              size="xmini"
              weight={300}
              style={{ marginBottom: WP05 }}
            >
              {startCase(toLower(ads?.city_name))}
            </Text>
          </View>
        </SkeletonContent>
      </View>
    );
  }

  return (
    <View
      style={{
        marginRight: WP4,
        marginLeft: 1,
        width: WP40,
      }}
    >
      <View
        style={{
          height: WP40,
          width: WP40,
          backgroundColor: WHITE_MILK,
          borderRadius: WP40 / 2,
          ...SHADOW_STYLE[loading ? "none" : "shadowThin"],
        }}
      >
        {!loading && (
          <View>
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onPress={onPressItem}
              style={{
                borderRadius: WP40 / 2,
                overflow: "hidden",
              }}
            >
              <Image
                source={{ uri: ads.profile_picture }}
                imageStyle={{ height: WP40, aspectRatio: 1 }}
              />
            </TouchableOpacity>
          </View>
        )}
      </View>
      <View style={{ marginBottom: WP1, marginTop: WP305 }}>
        <Text
          numberOfLines={1}
          type="Circular"
          size="mini"
          color={GUN_METAL}
          weight={500}
          centered
        >
          {title}
        </Text>
      </View>
      <Text
        numberOfLines={1}
        ellipsizeMode="tail"
        type="Circular"
        size="xmini"
        color={SHIP_GREY_CALM}
        weight={300}
        centered
      >
        {genre}
      </Text>
    </View>
  );
};

ColalboratorItem.propTypes = {};

ColalboratorItem.defaultProps = {};

export default ColalboratorItem;
