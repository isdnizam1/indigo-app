import React from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import { isEmpty } from 'lodash-es'
import SkeletonContent from 'react-native-skeleton-content'
import { GUN_METAL, PALE_GREY, REDDISH, SHIP_GREY, SHIP_GREY_CALM, SKELETON_COLOR, SKELETON_HIGHLIGHT, WHITE, WHITE_MILK, PALE_GREY_TWO } from '../../constants/Colors'
import { WP05, WP1, WP100, WP105, WP2, WP3, WP4, WP6, WP75, WP8, WP15, WP5 } from '../../constants/Sizes'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import Avatar from '../Avatar'
import Icon from '../Icon'
import Text from '../Text'
import { thousandSeparator } from '../../utils/helper'
import CollaborationSkeleton from './CollaborationSkeleton'

const CollaborationItem = ({ ads, loading = true, navigateTo, myId }) => {
  const additionalData = loading ? {} : JSON.parse(ads.additional_data),
    title = loading ? 'Collaboration Title' : ads.title,
    image = ads?.profile_picture,
    createdBy = loading ? 'Created By' : ads?.created_by,
    authorJobTitle = loading ? 'Job Title' : ads?.job_title,
    authorCityName = !isEmpty(ads.city_name) ? ads.city_name : 'Location',
    daysUntil = !isEmpty(ads.time_ago) ? ads.time_ago : '0 hari lalu',
    dataProfession = loading ? [1, 2, 3] : !isEmpty(additionalData.profession) && additionalData.profession.slice(0, 3),
    myAds = myId == ads.id_user
  return (
    <View
      style={{
        width: WP100 - WP8,
        marginRight: WP4,
        marginLeft: 1,
        borderRadius: 12,
        backgroundColor: WHITE,
        ...SHADOW_STYLE['shadowThin'],
      }}
    >
      <View
        style={{
          borderRadius: 12,
          overflow: 'hidden',
        }}
      >
        {/* profile section */}
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'space-between',
            paddingHorizontal: WP4,
            paddingTop: WP4,
            paddingBottom: WP2
          }}
        >
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start' }}>
            <View style={{ marginRight: WP3 }}>
              <Avatar image={image} isLoading={loading} />
            </View>
            <SkeletonContent
              containerStyle={{ flex: 1 }}
              layout={[
                CollaborationSkeleton.layout.title,
                CollaborationSkeleton.layout.subtitle,
                CollaborationSkeleton.layout.subtitle,
              ]}
              isLoading={loading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View>
                <Text numberOfLines={1} type='Circular' color={GUN_METAL} size='mini' weight={500} style={{ marginBottom: WP05 }}>{createdBy}</Text>
                <Text numberOfLines={1} type='Circular' color={SHIP_GREY_CALM} size='xmini' weight={300} style={{ marginBottom: WP05 }}>{authorJobTitle}</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text numberOfLines={1} ellipsizeMode='tail' type='Circular' size='xmini' color={SHIP_GREY_CALM} weight={300}>{authorCityName}</Text>
                  <View style={{ width: WP05, height: WP05, borderRadius: WP05 / 2, backgroundColor: SHIP_GREY_CALM, marginHorizontal: WP2 }} />
                  <Text numberOfLines={1} ellipsizeMode='tail' type='Circular' size='xmini' color={SHIP_GREY_CALM} weight={300}>{daysUntil}</Text>
                </View>
              </View>
            </SkeletonContent>
          </View>
          <SkeletonContent
            containerStyle={{ flexGrow: 0 }}
            layout={[
              { width: WP15, height: WP6 }
            ]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <View style={{ backgroundColor: loading ? WHITE_MILK : REDDISH, paddingHorizontal: WP4, paddingVertical: WP1 + 2, borderRadius: 6, justifyContent: 'center', alignItems: 'center' }}>
              <Text numberOfLines={1} type='Circular' size='xmini' color={WHITE} weight={400} centered>{myAds ? 'Lihat' : 'Join'}</Text>
            </View>
          </SkeletonContent>
        </View>

        {/* description section */}
        <View style={{ paddingHorizontal: WP4, paddingTop: WP2, paddingBottom: WP4 }}>
          <SkeletonContent
            containerStyle={{ flex: 1 }}
            layout={[
              CollaborationSkeleton.layout.textContent(WP75),
              { ...CollaborationSkeleton.layout.textContent(), marginBottom: 0 },
            ]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <Text numberOfLines={3} type='Circular' color={SHIP_GREY} size='mini'>{title}</Text>
          </SkeletonContent>
        </View>

        {/* profession section*/}
        <View style={{ borderTopWidth: 1, width: '100%', borderTopColor: PALE_GREY, paddingVertical: WP3, paddingHorizontal: WP4 }}>
          <ScrollView
            horizontal
            bounces={false}
            bouncesZoom={false}
            showsHorizontalScrollIndicator={false}
          >
            {
              dataProfession.map((item, index) => {
                if (loading) {
                  return (
                    <SkeletonContent
                      key={index}
                      containerStyle={{ flex: 1 }}
                      layout={[
                        CollaborationSkeleton.layout.profesi
                      ]}
                      isLoading={loading}
                      boneColor={SKELETON_COLOR}
                      highlightColor={SKELETON_HIGHLIGHT}
                    />
                  )
                }
                return (
                  <View
                    key={index}
                    style={styles.btnProfession}
                  >
                    <Text type='Circular' color={SHIP_GREY_CALM} size='xmini' weight={400}>{index === 2 && additionalData.profession.length > 3 ? 'More' : item.name}</Text>
                  </View>
                )
              })
            }
          </ScrollView>
        </View>

        {/* counter section*/}
        <View
          style={{
            width: '100%',
            backgroundColor: PALE_GREY_TWO,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderTopWidth: 1,
            borderTopColor: PALE_GREY,
            padding: WP4
          }}
        >
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <SkeletonContent
              containerStyle={{ flexGrow: 0 }}
              layout={[
                { width: WP15, height: WP5, marginRight: WP2 }
              ]}
              isLoading={loading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: WP2 }}>
                <Icon
                  centered
                  background='dark-circle'
                  size='mini'
                  color={SHIP_GREY_CALM}
                  name='eye'
                  type='MaterialCommunityIcons'
                  style={{ marginRight: WP05 }}
                />
                <Text type='Circular' size='mini' weight={300} color={SHIP_GREY_CALM}>{thousandSeparator(ads.total_view)}</Text>
              </View>
            </SkeletonContent>
            <SkeletonContent
              containerStyle={{ flexGrow: 0 }}
              layout={[
                { width: WP15, height: WP5, marginRight: WP2 }
              ]}
              isLoading={loading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: WP2 }}>
                <Icon
                  centered
                  background='dark-circle'
                  size='mini'
                  color={SHIP_GREY_CALM}
                  name='comment'
                  type='MaterialIcons'
                  style={{ marginRight: WP05 }}
                />
                <Text type='Circular' size='mini' weight={300} color={SHIP_GREY_CALM}>{ads.total_comment == 0 ? 'Berikan Komentar' : `${thousandSeparator(ads.total_comment)} Komentar`}</Text>
              </View>
            </SkeletonContent>
          </View>
          <SkeletonContent
            containerStyle={{ flexGrow: 0 }}
            layout={[
              { width: WP15, height: WP5 }
            ]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onPress={() => navigateTo('ShareScreen', {
                id: ads.id_ads,
                type: 'explore',
                title: 'Bagikan Collaboration'
              })}
              style={{ flexDirection: 'row', alignItems: 'center' }}
            >
              <Icon
                centered
                background='dark-circle'
                size='mini'
                color={SHIP_GREY_CALM}
                name='share'
                type='MaterialIcons'
                style={{ marginRight: WP05 }}
              />
              <Text type='Circular' size='mini' weight={300} color={SHIP_GREY_CALM}>Share</Text>
            </TouchableOpacity>
          </SkeletonContent>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  btnProfession: {
    borderRadius: 6,
    paddingVertical: WP1,
    paddingHorizontal: WP105,
    marginRight: WP105,
    backgroundColor: PALE_GREY,
  }
})

export default CollaborationItem
