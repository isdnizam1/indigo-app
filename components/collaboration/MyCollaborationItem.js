import React from "react";
import { View, StyleSheet, ScrollView, TouchableOpacity } from "react-native";
import { isEmpty } from "lodash-es";
import SkeletonContent from "react-native-skeleton-content";
import {
  GUN_METAL,
  PALE_GREY,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
  WHITE_MILK,
  PALE_GREY_TWO,
  BLUE_MAIN,
  BLUE_FOCUS,
  GREEN_SOFT,
  GREEN_FLOWKIT,
  PALE_WHITE,
} from "../../constants/Colors";
import {
  WP05,
  WP1,
  WP100,
  WP105,
  WP2,
  WP3,
  WP4,
  WP6,
  WP75,
  WP8,
  WP15,
  WP5,
  HP2,
  WP30,
  WP40,
  WP50,
  WP20,
} from "../../constants/Sizes";
import { SHADOW_STYLE, TOUCH_OPACITY } from "../../constants/Styles";
import Avatar from "../Avatar";
import Icon from "../Icon";
import Text from "../Text";
import { thousandSeparator } from "../../utils/helper";
import CollaborationSkeleton from "./CollaborationSkeleton";
import Image from "../Image";
import moment from "moment";

const MyCollaborationItem = ({
  ads,
  loading,
  navigateTo,
  myAds,
  userData,
  type,
  image,
}) => {
  const time = moment
    .utc(ads.updated_at || ads.created_at)
    .local()
    .startOf("seconds")
    .fromNow();

  const data =
    ads.profession !== undefined
      ? typeof ads.profession !== "object"
        ? JSON.parse(ads.profession)
        : ads.profession
      : ["Front End", "Back End", "UI/UX"];

  const statusBtnColor =
    ads.status == "active"
      ? GREEN_SOFT
      : ads.status == "In progress"
      ? BLUE_FOCUS
      : SHIP_GREY;

  const statusBtnTitleColor =
    ads.status == "active"
      ? GREEN_FLOWKIT
      : ads.status == "In progress"
      ? BLUE_MAIN
      : WHITE;

  const statusBtnTitle =
    ads.status == "active"
      ? "Opened"
      : ads.status == "In progress"
      ? "In Progress"
      : "Finished Project";

  const buttonTitle =
    type == "home"
      ? myAds
        ? "Lihat"
        : "Join"
      : type == "myProject"
      ? `${ads.total_register} Registrants`
      : "In progress";

  const buttonColor =
    type !== "joined" ? (loading ? WHITE_MILK : REDDISH) : statusBtnColor;

  return (
    <View
      style={{
        marginRight: WP4,
        width: WP100 - WP8,
        borderRadius: 12,
        backgroundColor: WHITE,
        ...SHADOW_STYLE["shadowThin"],
      }}
    >
      <View
        style={{
          borderRadius: 12,
          overflow: "hidden",
        }}
      >
        {/* profile section */}
        <View
          style={{
            width: "100%",
            flexDirection: "row",
            alignItems: "flex-start",
            justifyContent: "space-between",
            paddingHorizontal: WP4,
            paddingTop: WP4,
            paddingBottom: WP2,
          }}
        >
          <View
            style={{ flex: 1, flexDirection: "row", alignItems: "flex-start" }}
          >
            <View style={{ marginRight: WP3 }}>
              <Avatar
                image={myAds ? userData.profile_picture : ads.profile_picture}
                isLoading={loading}
              />
            </View>
            <SkeletonContent
              containerStyle={{ flex: 1 }}
              layout={[
                CollaborationSkeleton.layout.title,
                CollaborationSkeleton.layout.subtitle,
                CollaborationSkeleton.layout.subtitle,
              ]}
              isLoading={loading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View>
                <Text
                  numberOfLines={1}
                  type="Circular"
                  color={GUN_METAL}
                  size="mini"
                  weight={500}
                  style={{ marginBottom: WP05 }}
                >
                  {myAds ? userData.full_name : ads.full_name}
                </Text>
                <Text
                  numberOfLines={1}
                  type="Circular"
                  color={SHIP_GREY_CALM}
                  size="xmini"
                  weight={300}
                  style={{ marginBottom: WP05 }}
                >
                  {myAds ? userData.job_title : ads.job_title}
                </Text>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    type="Circular"
                    size="xmini"
                    color={SHIP_GREY_CALM}
                    weight={300}
                  >
                    {myAds ? userData.location.city_name : ads.city_name}
                  </Text>
                  <View
                    style={{
                      width: WP05,
                      height: WP05,
                      borderRadius: WP05 / 2,
                      backgroundColor: SHIP_GREY_CALM,
                      marginHorizontal: WP2,
                    }}
                  />
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    type="Circular"
                    size="xmini"
                    color={SHIP_GREY_CALM}
                    weight={300}
                  >
                    {time}
                  </Text>
                </View>
              </View>
            </SkeletonContent>
          </View>
          {type !== "explore" && (
            <SkeletonContent
              containerStyle={{ flexGrow: 0 }}
              layout={[{ width: WP15, height: WP6 }]}
              isLoading={loading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View
                style={{
                  backgroundColor: buttonColor,
                  paddingHorizontal: WP4,
                  paddingVertical: WP1 + 2,
                  borderRadius: 6,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text
                  numberOfLines={1}
                  type="Circular"
                  size="xmini"
                  color={type !== "joined" ? WHITE : statusBtnTitleColor}
                  weight={400}
                  centered
                >
                  {buttonTitle}
                </Text>
              </View>
            </SkeletonContent>
          )}
        </View>

        {/* description section */}
        {type !== "explore" ? (
          <View
            style={{
              paddingHorizontal: WP4,
              paddingTop: WP2,
              paddingBottom: WP4,
            }}
          >
            <SkeletonContent
              containerStyle={{ flex: 1 }}
              layout={[
                CollaborationSkeleton.layout.textContent(WP75),
                {
                  ...CollaborationSkeleton.layout.textContent(),
                  marginBottom: 0,
                },
              ]}
              isLoading={loading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <Text
                numberOfLines={3}
                type="Circular"
                color={SHIP_GREY}
                size="mini"
              >
                {ads.description}
              </Text>
            </SkeletonContent>
          </View>
        ) : (
          <SkeletonContent
            containerStyle={{ flexGrow: 0 }}
            layout={[{ width: WP50 * 2, height: WP50 }]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <View>
              <Image
                source={{ uri: image }}
                imageStyle={{ height: WP50 }}
                aspectRatio={140 / 74}
              />
              <View
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  backgroundColor: statusBtnColor,
                  paddingHorizontal: WP4,
                  paddingVertical: WP1 + 2,
                  borderBottomEndRadius: 10,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text
                  numberOfLines={1}
                  type="Circular"
                  size="xmini"
                  color={statusBtnTitleColor}
                  weight={400}
                  centered
                >
                  {statusBtnTitle}
                </Text>
              </View>
            </View>
          </SkeletonContent>
        )}
        {/* profession section*/}
        <View
          style={{
            borderTopWidth: 1,
            width: "100%",
            borderTopColor: PALE_GREY,
            paddingVertical: WP3,
            paddingHorizontal: WP4,
          }}
        >
          <ScrollView
            horizontal
            bounces={false}
            bouncesZoom={false}
            showsHorizontalScrollIndicator={false}
          >
            {data.map((item, index) => {
              if (loading) {
                return (
                  <SkeletonContent
                    key={index}
                    containerStyle={{ flex: 1 }}
                    layout={[CollaborationSkeleton.layout.profesi]}
                    isLoading={loading}
                    boneColor={SKELETON_COLOR}
                    highlightColor={SKELETON_HIGHLIGHT}
                  />
                );
              }
              return (
                <View key={index} style={styles.btnProfession}>
                  <Text
                    type="Circular"
                    color={SHIP_GREY_CALM}
                    size="xmini"
                    weight={400}
                  >
                    {index === 2 && data.length > 3 ? "More" : item.name}
                  </Text>
                </View>
              );
            })}
          </ScrollView>
        </View>

        {/* counter section*/}
        <View
          style={{
            width: "100%",
            backgroundColor: PALE_GREY_TWO,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            borderTopWidth: 1,
            borderTopColor: PALE_GREY,
            padding: WP4,
          }}
        >
          <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
            <SkeletonContent
              containerStyle={{ flexGrow: 0 }}
              layout={[{ width: WP15, height: WP5, marginRight: WP2 }]}
              isLoading={loading}
              boneColor={SKELETON_COLOR}
              highlightColor={SKELETON_HIGHLIGHT}
            >
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginRight: WP2,
                }}
              >
                <Icon
                  centered
                  background="dark-circle"
                  size="mini"
                  color={SHIP_GREY_CALM}
                  name="eye"
                  type="MaterialCommunityIcons"
                  style={{ marginRight: WP05 }}
                />
                <Text
                  type="Circular"
                  size="mini"
                  weight={300}
                  color={SHIP_GREY_CALM}
                >
                  {thousandSeparator(ads.total_view)}
                </Text>
              </View>
            </SkeletonContent>
            {type !== "explore" && (
              <SkeletonContent
                containerStyle={{ flexGrow: 0 }}
                layout={[{ width: WP15, height: WP5, marginRight: WP2 }]}
                isLoading={loading}
                boneColor={SKELETON_COLOR}
                highlightColor={SKELETON_HIGHLIGHT}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginRight: WP2,
                  }}
                >
                  <Icon
                    centered
                    background="dark-circle"
                    size="mini"
                    color={SHIP_GREY_CALM}
                    name="comment"
                    type="MaterialIcons"
                    style={{ marginRight: WP05 }}
                  />
                  <Text
                    type="Circular"
                    size="mini"
                    weight={300}
                    color={SHIP_GREY_CALM}
                  >
                    {ads.total_comment == 0
                      ? "Berikan Komentar"
                      : `${thousandSeparator(ads.total_comment)} Comments`}
                  </Text>
                </View>
              </SkeletonContent>
            )}
          </View>

          <SkeletonContent
            containerStyle={{ flexGrow: 0 }}
            layout={[{ width: WP15, height: WP5 }]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            {type == "explore" || type == "home" ? (
              <TouchableOpacity
                activeOpacity={TOUCH_OPACITY}
                onPress={() =>
                  navigateTo("ShareScreen", {
                    id: ads.id_ads,
                    type: "explore",
                    title: "Bagikan Collaboration",
                  })
                }
                style={{ flexDirection: "row", alignItems: "center" }}
              >
                {type == "explore" && (
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      marginRight: WP2,
                    }}
                  >
                    <Icon
                      centered
                      background="dark-circle"
                      size="mini"
                      color={SHIP_GREY_CALM}
                      name="comment"
                      type="MaterialIcons"
                      style={{ marginRight: WP05 }}
                    />
                  </View>
                )}
                <Icon
                  centered
                  background="dark-circle"
                  size="mini"
                  color={SHIP_GREY_CALM}
                  name="share"
                  type="MaterialIcons"
                  style={{ marginRight: WP05 }}
                />
                {type == "home" && (
                  <Text
                    type="Circular"
                    size="mini"
                    weight={300}
                    color={SHIP_GREY_CALM}
                  >
                    Share
                  </Text>
                )}
              </TouchableOpacity>
            ) : type === "myProject" ? (
              <View
                style={{
                  backgroundColor: statusBtnColor,
                  paddingHorizontal: WP4,
                  paddingVertical: WP1 + 2,
                  borderRadius: 6,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text
                  numberOfLines={1}
                  type="Circular"
                  size="xmini"
                  color={statusBtnTitleColor}
                  weight={400}
                  centered
                >
                  {statusBtnTitle}
                </Text>
              </View>
            ) : null}
          </SkeletonContent>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnProfession: {
    borderRadius: 6,
    paddingVertical: WP1,
    paddingHorizontal: WP105,
    marginRight: WP105,
    backgroundColor: PALE_GREY,
  },
});

export default MyCollaborationItem;
