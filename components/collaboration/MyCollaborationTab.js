import React from "react";
import { Image as RNImage, TouchableOpacity, View } from "react-native";
import Image from "../../components/Image";
import { TOUCH_OPACITY, SHADOW_STYLE } from "sf-constants/Styles";
import {
  WHITE,
  NAVY_DARK,
  REDDISH,
  SHIP_GREY,
  PALE_BLUE,
} from "../../constants/Colors";
import Text from "../Text";
import {
  WP100,
  WP50,
  WP3,
  WP4,
  WP6,
  WP2,
  WP5,
  WP11,
  HP50,
} from "../../constants/Sizes";
import EmptyState from "../EmptyState";

const propsType = {};

const propsDefault = {};

const MyCollaborationTab = (props) => {
  const {
    content,
    label,
    onPressGroup,
    onPressDetail,
    isEmptyCollab,
    emptyMessageTitle,
    data,
    navigateTo,
  } = props;

  return isEmptyCollab ? (
    <View
      style={{
        justifyContent: "center",
        alignItems: "center",
        height: HP50 * 0.85,
      }}
    >
      <EmptyState
        image={require(`sf-assets/icons/ic_collab_collaboration_emptystate.png`)}
        title={emptyMessageTitle}
        message={"Start collaborating with other participants right now!"}
      />
    </View>
  ) : (
    <View>
      <View>
        <Image
          tint={"black"}
          imageStyle={{ width: WP100, height: undefined }}
          aspectRatio={360 / 182}
          source={{ uri: data.image }}
        />
      </View>
      <View
        style={{
          paddingLeft: WP4,
          paddingRight: WP2,
          alignItems: "center",
          marginVertical: WP5,
          borderBottomWidth: 1,
          borderColor: PALE_BLUE,
          paddingBottom: WP5,
        }}
      >
        <Text type="Circular" size="small" color={NAVY_DARK} weight={500}>
          {data.description}
        </Text>
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          onPress={onPressGroup}
          style={{
            width: WP50,
            height: WP11,
            backgroundColor: WHITE,
            borderWidth: 1,
            borderRadius: 6,
            borderColor: PALE_BLUE,
            justifyContent: "center",
            alignItems: "center",
            marginTop: WP6,
            paddingVertical: WP2,
            flexDirection: "row",
          }}
        >
          <Image
            source={require("../../assets/icons/icGroupChat.png")}
            size="xtiny"
            centered
            style={{ marginRight: WP2 }}
          />
          <Text
            type="Circular"
            size="mini"
            weight={500}
            color={SHIP_GREY}
            centered
          >
            Group Chat
          </Text>
        </TouchableOpacity>
      </View>

      <View style={{ paddingLeft: WP4, paddingRight: WP2 }}>
        {data.joined !== null && (
          <Text type="Circular" size="xmini" weight={500} color={SHIP_GREY}>
            {label}
          </Text>
        )}
        {content}
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          onPress={() =>
            navigateTo("CollaborationPreview", {
              id: data.id_ads,
              previousScreen: "MyCollaboration",
            })
          }
          style={{
            minWidth: WP50,
            backgroundColor: REDDISH,
            borderRadius: 6,
            justifyContent: "center",
            alignItems: "center",
            marginVertical: WP4,
            paddingVertical: WP3,
            ...SHADOW_STYLE["shadowThin"],
          }}
        >
          <Text type="Circular" size="mini" weight={500} color={WHITE} centered>
            View Collaboration Details
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

MyCollaborationTab.propTypes = propsType;
MyCollaborationTab.defaultProps = propsDefault;

export default MyCollaborationTab;
