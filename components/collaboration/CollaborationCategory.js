import React, { memo } from "react";
import { View, FlatList, TouchableOpacity } from "react-native";
import { isEmpty } from "lodash-es";
import SkeletonContent from "react-native-skeleton-content";
import { postLogActivity } from "sf-actions/api";
import {
  PALE_BLUE,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
  WHITE_MILK,
} from "../../constants/Colors";
import { WP2, WP4, WP44, WP45, WP6 } from "../../constants/Sizes";
import Image from "../Image";
import Text from "../Text";
import { postLogCreateCollab } from "../../actions/api";
import { restrictedAction } from "../../utils/helper";

export const _mapResponseCategory = (categoryList) => {
  let newData = [];
  let topData = categoryList.splice(0, Math.ceil(categoryList.length / 2));
  let bottomData = categoryList;
  while (topData.length > 0) {
    const itemData = [...topData.splice(0, 1), ...bottomData.splice(0, 1)];
    newData.push(itemData);
  }
  return newData;
};

const CollaborationCategory = (props) => {
  const { list, loading, navigateTo, navigation, userData } = props;
  const tmpList = list ? [...list] : [];
  const dummyData = [
      [1, 2],
      [3, 4],
      [5, 6],
    ],
    data = loading
      ? dummyData
      : list?.length > 0 && _mapResponseCategory(tmpList);

  const _renderItem = ({ item, index }) => {
    const itemWidth = data.length > 2 ? WP44 : WP45;
    const wrapperStyle = {
      width: itemWidth,
      marginRight: WP2,
    };
    const itemView = (itemData) => (
      <View
        key={`${Math.random()}`}
        style={{ width: itemWidth, marginBottom: WP2 }}
      >
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[
            { width: itemWidth, height: (itemWidth * 9) / 16, borderRadius: 6 },
          ]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <View
            style={{
              borderRadius: 6,
              overflow: "hidden",
              backgroundColor: WHITE_MILK,
              height: (itemWidth * 9) / 16,
            }}
          >
            {!loading && (
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => _onPressItem(itemData)}
                // onPress={_onPressItem(itemData)}
              >
                <View>
                  <Image
                    centered
                    source={itemData.image}
                    imageStyle={{
                      height: (itemWidth * 9) / 16,
                      aspectRatio: 17 / 9,
                    }}
                    style={{
                      backgroundSize: "content",
                    }}
                  />
                  <View
                    style={{
                      position: "absolute",
                      alignSelf: "center",
                      top: 5,
                      left: 10,
                      right: 5,
                    }}
                  >
                    <Text
                      type="Circular"
                      size="mini"
                      color={WHITE}
                      weight={500}
                      // centered
                    >
                      {itemData.label}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          </View>
        </SkeletonContent>
      </View>
    );
    return (
      <View key={`${index}-category`} style={wrapperStyle}>
        {item.map((itemMap, indexMap) => itemView(itemMap))}
      </View>
    );
  };

  const _createCollab = () => {
    restrictedAction({
      action: Promise.resolve(navigateTo("CollaborationForm")).then(() => {
        postLogCreateCollab({ id_user: this.props.userData.id_user });
      }),
      userData,
      navigation,
    });
    Promise.resolve(navigateTo("CollaborationForm")).then(() => {
      postLogCreateCollab({ id_user: this.props.userData.id_user });
    });
  };

  const _onPressItem = (item) => {
    const category = item.category_name;
    let tracking_value;
    if (category === "create_collab") {
      _createCollab();
      tracking_value = "create_collab";
    } else if (category === "explore_collab") {
      tracking_value = "explore_collab";
      navigateTo("ExploreCollaborationScreen");
    } else if (category === "my_network") {
      tracking_value = "my_network";
      navigateTo("MyNetworkScreen");
    } else if (category === "my_collab_project") {
      tracking_value = "my_collab_project";
      navigateTo("MyCollaborationScreen");
    }
    !!tracking_value &&
      postLogActivity({
        id_user: props.idUser,
        value: tracking_value,
      }).then(({ data }) => {
        // __DEV__ && console.log({ data })
      });
  };

  return (
    !isEmpty(data) && (
      <View
        style={{
          paddingTop: WP6,
          paddingBottom: WP4,
          borderTopColor: PALE_BLUE,
          borderTopWidth: 1,
        }}
      >
        <View>
          <FlatList
            showsHorizontalScrollIndicator={false}
            bounces={false}
            horizontal
            data={data}
            keyExtractor={(item, index) => `key${index}`}
            contentContainerStyle={{
              paddingTop: WP4,
              paddingLeft: WP4,
              paddingRight: WP2,
              paddingBottom: WP2,
            }}
            renderItem={_renderItem}
          />
        </View>
      </View>
    )
  );
};

export default memo(CollaborationCategory);
