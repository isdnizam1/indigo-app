import {
  WP1,
  WP100,
  WP105,
  WP2,
  WP20,
  WP205,
  WP25,
  WP3,
  WP30,
  WP305,
  WP35,
  WP4,
  WP40,
  WP5,
  WP50,
  WP505,
  WP6,
  WP60,
  WP70,
  WP8,
  WP80,
  WP90,
} from "../../constants/Sizes";
import { NO_COLOR } from "../../constants/Colors";
import { DrawerLayoutAndroid } from "react-native-gesture-handler";

const defaultItem = {
  width: WP40,
  height: WP40,
  marginRight: WP4,
  marginBottom: WP2,
};

const defaultItemTitle = { width: WP35, height: WP4, marginBottom: WP2 };
const defaultItemSubtitle = { width: WP30, height: WP205 };

const container = { flex: 1 };

const header = [
  { width: WP35, height: WP5, marginBottom: WP2 },
  { width: WP50, height: WP3 },
];

const body = [];

const item = (category) => {
  let layout = [];
  let wrapperStyle = { backgroundColor: NO_COLOR };
  switch (category) {
    case "collaborator":
      layout = [
        {
          ...defaultItem,
          borderRadius: WP40 / 2,
          marginLeft: WP6,
          marginBottom: WP305,
        },
        { ...defaultItemTitle, width: WP25 },
        { ...defaultItemSubtitle, width: WP30 },
      ];
      break;
    case "myproject":
      layout = [
        {
          ...defaultItem,
          borderRadius: WP40 / 2,
          marginLeft: WP6,
          marginBottom: WP305,
        },
        { ...defaultItemTitle, width: WP25 },
        { ...defaultItemSubtitle, width: WP30 },
        layout.image,
      ];
      break;
    default:
      layout = [
        { ...defaultItem, borderRadius: 6 },
        { ...defaultItemTitle, width: WP40 },
        defaultItemTitle,
        defaultItemSubtitle,
      ];
      wrapperStyle = {
        width: WP40,
      };
  }
  return {
    layout,
    wrapperStyle,
  };
};

const layout = {
  title: {
    width: WP35,
    height: WP4,
    marginBottom: WP105,
  },
  subtitle: {
    width: WP50,
    height: WP3,
    marginBottom: WP1,
  },
  textContent: (width = WP60) => ({
    width,
    height: WP4,
    marginBottom: WP105,
  }),
  profesi: {
    width: WP20,
    height: WP505,
    marginRight: WP105,
    borderRadius: 6,
  },
  image: { width: WP100, height: undefined, aspectRatio: 360 / 182 },
  label: {
    width: WP90,
    height: WP8,
    marginVertical: WP5,
    marginHorizontal: WP4,
  },
  button: {
    width: WP40,
    height: WP8,
    marginVertical: WP4,
    marginHorizontal: WP4,
  },
};

export default {
  container,
  header,
  body,
  item,
  layout,
};
