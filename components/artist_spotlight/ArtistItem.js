import React from 'react'
import { View, StyleSheet } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import { WP40, WP1, WP4, WP305, WP3, WP05, WP35, WP50, WP105, WP25, WP205, WP30, WP2 } from '../../constants/Sizes'
import Image from '../Image'
import { SHADOW_STYLE } from '../../constants/Styles'
import Text from '../Text'
import { GUN_METAL, SHIP_GREY_CALM, WHITE_MILK, SKELETON_COLOR, SKELETON_HIGHLIGHT } from '../../constants/Colors'
import Avatar from '../Avatar'

const style = StyleSheet.create({
  artist: { flex: 1, flexDirection: 'row', alignItems: 'center' },
  artistImage: { marginRight: WP3 },
  artistSkeletonContainer: { flex: 1 },
  artistTitle: { marginBottom: WP05 },
  artistInterest: { marginBottom: WP05 }
})

const simpleListSkeleton = [
  { width: WP35, height: WP4, marginBottom: WP105 },
  { width: WP50, height: WP3, marginBottom: WP1 }
]

const ArtistItem = ({ ads, loading = true, simpleListItem = false }) => {
  const title = loading ? 'Artist Name' : ads.full_name,
    interest = loading ? 'Interest' : `${ads.interest?.join(', ')}`

  if (simpleListItem) {
    return (
      <View style={style.artist}>
        <View style={style.artistImage}>
          <Avatar image={ads.profile_picture} isLoading={loading} />
        </View>
        <SkeletonContent
          containerStyle={style.artistSkeletonContainer}
          layout={simpleListSkeleton}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <View>
            <Text numberOfLines={1} type='Circular' color={GUN_METAL} size='mini' weight={500} style={style.artistTitle}>{title}</Text>
            <Text numberOfLines={1} type='Circular' color={SHIP_GREY_CALM} size='xmini' weight={300} style={style.artistInterest}>{interest}</Text>
          </View>
        </SkeletonContent>
      </View>
    )
  } else {
    return (
      <SkeletonContent
        containerStyle={{ flex: 1, alignItems: 'center' }}
        layout={[
          { width: WP40, height: WP40, borderRadius: WP40 / 2, marginRight: WP4, marginBottom: WP305 },
          { width: WP25, height: WP4, marginBottom: WP2 },
          { width: WP30, height: WP205 }
        ]}
        isLoading={loading}
        boneColor={SKELETON_COLOR}
        highlightColor={SKELETON_HIGHLIGHT}
      >
        <View style={{
          marginRight: WP4,
          marginLeft: 1,
          width: WP40,
        }}
        >
          <View style={{ height: WP40, width: WP40, backgroundColor: WHITE_MILK, borderRadius: WP40 / 2, ...SHADOW_STYLE[loading ? 'none' : 'shadowThin'] }}>

            {
              !loading && (
                <View>
                  <View style={{
                    borderRadius: WP40 / 2,
                    overflow: 'hidden',
                  }}
                  >
                    <Image
                      source={{ uri: ads.profile_picture }}
                      imageStyle={{ height: WP40, aspectRatio: 1 }}
                    />
                  </View>
                </View>
              )
            }
          </View>
          <View style={{ marginBottom: WP1, marginTop: WP305 }}>
            <Text numberOfLines={1} type='Circular' size='mini' color={GUN_METAL} weight={500} centered>{title}</Text>
          </View>
          <Text numberOfLines={1} ellipsizeMode='tail' type='Circular' size='xmini' color={SHIP_GREY_CALM} weight={300} centered>{interest}</Text>
        </View>
      </SkeletonContent>
    )
  }

}

ArtistItem.propTypes = {}

ArtistItem.defaultProps = {}

export default React.memo(ArtistItem)
