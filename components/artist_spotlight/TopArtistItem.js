import React, { useState, useEffect } from 'react'
import { View, TouchableWithoutFeedback } from 'react-native'
import SkeletonContent from 'react-native-skeleton-content'
import Carousel from 'react-native-snap-carousel'
import {
  GUN_METAL,
  PALE_BLUE,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
} from '../../constants/Colors'
import {
  WP05,
  WP100,
  WP2,
  WP3,
  WP35,
  WP4,
  WP50,
  WP6,
  WP8,
} from '../../constants/Sizes'
import Avatar from '../Avatar'
import Song from '../Song'
import Text from '../Text'

const _reformatSongs = (data) => {
  let new_song = []
  while (data.length > 0) {
    const innderData = data.splice(0, 4)
    new_song.push(innderData)
  }
  return new_song
}

const TopArtistItem = (props) => {
  const {
    ads,
    profile,
    loading = true,
    navigateTo,
    line,
    refreshData,
    idViewer,
  } = props
  const image = ads?.profile_picture,
    title = loading ? 'Artist Name' : ads.full_name,
    subtitle = loading ? 'Interest' : `${ads.interest}`

  const [activeSlider, setActiveSlider] = useState(0)
  const [dataSong, setDataSong] = useState([
    [1, 2, 3, 4],
    [1, 2, 3, 4],
  ])
  const [loadingItem, setLoadingItem] = useState(true)

  useEffect(() => {
    setLoadingItem(true)
    if (!loading) {
      const tmpSong = [...ads.song]
      setDataSong(_reformatSongs(tmpSong))
      setLoadingItem(false)
    }
  }, [loading])
  return (
    <View>
      {line && (
        <View
          style={{
            height: 1,
            backgroundColor: PALE_BLUE,
            marginBottom: WP6,
            marginTop: WP2,
            marginHorizontal: WP4,
          }}
        />
      )}
      <TouchableWithoutFeedback
        onPress={() =>
          navigateTo('ProfileScreenNoTab', {
            idUser: ads?.id_user,
            navigateBackOnDone: true,
          })
        }
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-start',
            paddingHorizontal: WP4,
          }}
        >
          <View style={{ marginRight: WP4 }}>
            <Avatar isLoading={loading} size='small' image={image} />
          </View>
          <SkeletonContent
            containerStyle={{ flex: 1 }}
            layout={[
              { width: WP50, height: WP6, marginBottom: WP2 },
              { width: WP35, height: WP3 },
            ]}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            <View>
              <Text
                numberOfLines={1}
                type='Circular'
                color={GUN_METAL}
                size='small'
                weight={500}
                style={{ marginBottom: WP05 }}
              >
                {title}
              </Text>
              <Text
                numberOfLines={1}
                type='Circular'
                color={SHIP_GREY_CALM}
                size='xmini'
                weight={300}
              >
                {subtitle}
              </Text>
            </View>
          </SkeletonContent>
        </View>
      </TouchableWithoutFeedback>
      <View>
        <Carousel
          onSnapToItem={(i) => setActiveSlider(i)}
          data={dataSong}
          extraData={dataSong}
          firstItem={activeSlider}
          inactiveSlideScale={1}
          activeSlideAlignment='start'
          // autoplay={!loading}
          // autoplayInterval={5000}
          // autoplayDelay={0}
          // loop={!loading}
          bouncesZoom={false}
          bounces={false}
          sliderWidth={WP100}
          itemWidth={WP100 - WP8}
          containerCustomStyle={{ paddingLeft: WP4, paddingTop: WP4 }}
          renderItem={({ item, index }) => (
            <View key={`${index}`} style={{ width: '100%' }}>
              {item.map((itemSong, indexSong) => (
                <Song
                  idViewer={idViewer}
                  key={`top-song-${indexSong}`}
                  firstSong={indexSong == 0}
                  song={itemSong}
                  style={{
                    paddingRight: WP4,
                    paddingVertical: WP2,
                    borderTopWidth: 0,
                  }}
                  styleFirst={{ paddingRight: WP4, paddingVertical: WP2 }}
                  loading={loadingItem}
                  borderInner={indexSong !== item.length - 1}
                  navigateTo={navigateTo}
                  isMine={itemSong.id_user == profile?.id_user}
                  refreshData={refreshData}
                />
              ))}
            </View>
          )}
        />
      </View>
    </View>
  )
}

export default TopArtistItem
