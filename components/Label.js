import React from 'react'
import PropTypes from 'prop-types'
import { PALE_LIGHT_BLUE_TWO, SHIP_GREY } from '../constants/Colors'
import Text from './Text'

const Label = ({ title, subtitle, style }) => (
  <Text weight={400} type='Circular' size={'xmini'} color={SHIP_GREY} style={style}>
    {title}
    {subtitle && (<Text weight={300} type='Circular' size={'xmini'} color={PALE_LIGHT_BLUE_TWO}> {subtitle}</Text>)}
  </Text>
)

Label.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  textType: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.any)
}

Label.defaultProps = {
  subtitle: '',
  title: '',
}

export default Label
