import React, { memo } from 'react'
import { View, StyleSheet } from 'react-native'
import { WP2, WP40, WP4, WP6, WP90 } from 'sf-constants/Sizes'
import Image from './Image'
import Touchable from './Touchable'

const closeIcon = require('sf-assets/icons/closeDarkRoundedSquare.png')

const style = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  image: {
    marginBottom: WP2,
  },
  imageStyle: {
    borderRadius: WP2,
  },
  closeIcon: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: WP2,
  },
})

const ratio = [[1], [2 / 1, 2 / 1], [2 / 1, 1, 1]]

const size = [[WP90], [WP90, WP90], [WP90, WP40 + WP4, WP40 + WP4]]

const CreatePostImages = (props) => {
  const { images, onDelete } = props
  return (
    <View style={style.container}>
      {images.map((uri, index) => {
        return (
          <View style={style.image} key={`img-${index}`}>
            <Image
              imageStyle={style.imageStyle}
              size={size[images.length - 1][index]}
              aspectRatio={ratio[images.length - 1][index]}
              source={{ uri }}
            />
            <Touchable onPress={() => onDelete(index)} style={style.closeIcon}>
              <Image size={WP6} source={closeIcon} />
            </Touchable>
          </View>
        )
      })}
    </View>
  )
}

export default memo(CreatePostImages)
