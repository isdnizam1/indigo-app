import React from 'react'
import { connect } from 'react-redux'
import { PALE_GREY_THREE } from '../constants/Colors'
import { REVIEW_SETTER } from '../services/review/actionTypes'
import { storeReviewAction } from '../utils/review'
import { isIOS } from '../utils/helper'
import ConfirmationModalV3 from './ConfirmationModalV3'

class ReviewModal extends React.Component {
  render() {
    if (true) return null
    if (!isIOS()) return null
    return (<ConfirmationModalV3
      visible={this.props.isReview}
      title={'Gimana sejauh ini pengalamanmu\nbersama Soundfren?'}
      primary={{
        text: 'Berikan penilaianmu',
        onPress: async () => {
          this.props.setReview(false)
          await storeReviewAction()
        }
      }}
      secondary={{
        text: 'Nanti Saja',
        onPress: () => {
          this.props.setReview(false)
        },
        backgroundColor: PALE_GREY_THREE
      }}
            />)
  }
}

export default connect((state) => ({ isReview: state.review.review }), {
  setReview: (data) => ({
    type: REVIEW_SETTER,
    response: data,
  }) })(ReviewModal)
