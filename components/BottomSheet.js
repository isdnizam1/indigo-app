import * as React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import Animated from 'react-native-reanimated'
import BottomSheet from 'reanimated-bottom-sheet'
import { WHITE, BLACK, GREY_CALM_SEMI } from '../constants/Colors'
import { TOUCH_OPACITY } from '../constants/Styles'

const { View, Value, interpolateNode } = Animated

class BottomSheetComponent extends React.PureComponent {
  state = {
    isOpen: false,
  }

  fall = new Value(1)

  renderShadow = ({ innerRef, hidePoint }) => {
    const animatedShadowOpacity = interpolateNode(this.fall, {
      inputRange: [0, 1],
      outputRange: [0.5, 0]
    })

    return (
      <View
        pointerEvents={this.state.isOpen ? undefined : 'none'}
        style={[
          styles.shadowContainer,
          {
            opacity: animatedShadowOpacity
          }
        ]}
      >
        <TouchableOpacity
          style={styles.shadowContainer}
          activeOpacity={this.props.backdropClose ? TOUCH_OPACITY : 1}
          onPress={this.props.backdropClose ? () => innerRef.current?.snapTo(hidePoint) : null}
        />
      </View>
    )
  }

  render() {
    const { innerRef, snapPoints, renderContent, delay, showPanel, enabledContentTapInteraction, onCloseEnd, borderRadius, onOpenStart } = this.props
    const hidePoint = snapPoints.length - 1
    return (
      <>
        {
          (
            <BottomSheet
              ref={innerRef}
              snapPoints={snapPoints}
              initialSnap={hidePoint}
              callbackNode={this.fall}
              enabledContentTapInteraction={enabledContentTapInteraction || false}
              renderContent={() => (
                <View style={styles.contentContainer}>
                  {renderContent({
                    closeBottomSheet: () => {
                      setTimeout(() => {
                        innerRef.current?.snapTo(hidePoint)
                      }, delay || 0)
                    }
                  })}
                </View>
              )}
              renderHeader={showPanel ? () => <View style={styles.panelHandle}/> : null}
              onOpenStart={() => {
                typeof onOpenStart !== 'undefined' && setTimeout(() => {
                  onOpenStart()
                }, 0)
              }}
              onOpenEnd={() => this.setState({ isOpen: true })}
              onCloseEnd={() => this.setState({
                isOpen: false
              }, () => {
                typeof onCloseEnd !== 'undefined' && setTimeout(() => {
                  onCloseEnd()
                }, 0)
              })}
              borderRadius={borderRadius ? borderRadius : 0}
            />)
        }
        {this.renderShadow({ innerRef, hidePoint })}
        {/*this.state.isOpen ? <View style={{ backgroundColor: '#ffffff', height: 50 }} /> : null*/}
      </>
    )
  }
}

export default BottomSheetComponent

const styles = StyleSheet.create({
  contentContainer: {
    height: '100%',
    backgroundColor: WHITE,
  },
  shadowContainer: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: BLACK
  },
  panelHandle: {
    width: 40,
    height: 6,
    borderRadius: 4,
    alignSelf: 'center',
    backgroundColor: GREY_CALM_SEMI,
    marginBottom: 8
  }
})
