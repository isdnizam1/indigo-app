import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ImageBackground, TouchableOpacity, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { GREY, NO_COLOR, WARM_PURPLE, ORANGE_BRIGHT, BLUE_LIGHT, WHITE } from '../constants/Colors'
import { WP3, HP9, WP6, WP2 } from '../constants/Sizes'
import Text from './Text'
import Image from './Image'
import Icon from './Icon'

const SUBMISSION_BACKGROUND = require('../assets/images/bannerSubmission.png')
const SUBMISSION_ICON = require('../assets/icons/icCreateSubmission.png')
const COLLAB_BACKGROUND = require('../assets/images/bannerCollaboration.png')
const COLLAB_ICON = require('../assets/icons/icCreateCollab.png')
const ARTIST_BACKGROUND = require('../assets/images/bannerArtist.png')
const ARTIST_ICON = require('../assets/icons/icCreateArtist.png')

const propsType = {
  navigateTo: PropTypes.func,
  showButtonCreate: PropTypes.func,
  isKeyboardOpen: PropTypes.bool
}

const propsDefault = {
  navigateTo: () => {},
  showButtonCreate: () => {},
  isKeyboardOpen: false
}

class CreateAddButton extends Component {
  state = {
    isOpen: false
  }

  _renderActionButton = (label, link, image, icon, iconRatio, color, isPremium) => {
    const { navigateTo } = this.props
    return (
      <TouchableOpacity
        onPress={() => {
          navigateTo(link)
        }}
      >
        <ImageBackground
          source={image}
          imageStyle={{
            borderRadius: 12,
            aspectRatio: 320/120
          }}
          style={{
            width: '100%',
            height: HP9,
            marginTop: WP3,
          }}
        >
          <LinearGradient
            colors={[NO_COLOR, color]}
            start={[0, 0]}
            end={[0.9, 1]}
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingHorizontal: WP3,
              borderRadius: 12,
            }}
          >
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start'
                }}
              >
                <Text type='NeoSans' weight={500} size='mini' color={WHITE}>{label}</Text>
                {isPremium && (
                  <Image
                    size={WP6}
                    aspectRatio={1}
                    source={require('../assets/icons/badgePremium.png')}
                    style={{ marginLeft: WP2 }}
                  />
                )}
              </View>
              {isPremium && <Text size='xtiny' color={WHITE}>for premium user</Text>}
            </View>

            <Image
              source={icon}
              aspectRatio={iconRatio}
              imageStyle={{
                width: WP6,
              }}
            />
          </LinearGradient>
        </ImageBackground>
      </TouchableOpacity>
    )
  }

  _showButtonCreate = () => {
    const {
      isKeyboardOpen,
      showButtonCreate
    } = this.props
    if (isKeyboardOpen) {
      this.setState({ isOpen: true }, () => showButtonCreate())
    } else {
      this.setState({ isOpen: !this.state.isOpen })
    }
  }

  render() {
    const { isOpen } = this.state
    const { isKeyboardOpen, showButtonsOnly, isShowButtons } = this.props
    const showButton = (isOpen && !isKeyboardOpen) || isShowButtons

    return (
      <View style={{
        paddingTop: WP3
      }}
      >
        {!showButtonsOnly ? <TouchableOpacity
          onPress={this._showButtonCreate}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
                            >
          <Text size='xtiny'>You can also post about:</Text>
          <Icon
            size='mini'
            name={showButton ? 'ios-arrow-up' : 'ios-arrow-down'}
            type='Ionicons'
            color={GREY}
            centered
          />
        </TouchableOpacity> : null}
        {showButton && (
          <View>
            {this._renderActionButton('Create Collaboration', 'Collaboration', COLLAB_BACKGROUND, COLLAB_ICON, 24/22, BLUE_LIGHT)}
            {this._renderActionButton('Create Submission', 'CreateSubmissionDetailForm', SUBMISSION_BACKGROUND, SUBMISSION_ICON, 24/25, ORANGE_BRIGHT)}
            {this._renderActionButton('Create Artist Spotlight', 'PromoteBandScreen', ARTIST_BACKGROUND, ARTIST_ICON, 20/24, WARM_PURPLE, true)}
          </View>
        )}
      </View>
    )
  }
}

CreateAddButton.propType = propsType

CreateAddButton.defaultProps = propsDefault

export default CreateAddButton
