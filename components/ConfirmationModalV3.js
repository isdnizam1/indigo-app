import React, { Component } from "react";
import ModalMessageView from "sf-components/ModalMessage/ModalMessageView";
import {
  GUN_METAL,
  PALE_WHITE,
  SHIP_GREY_CALM,
  REDDISH,
} from "sf-constants/Colors";
import { WP05, WP2, WP305, WP5, WP6, WP90 } from "sf-constants/Sizes";
import { get, noop } from "lodash-es";
import PropTypes from "prop-types";
import { WP30 } from "../constants/Sizes";

class ConfirmationModalV3 extends Component {
  render() {
    const {
      visible,
      title,
      subtitle,
      extraSubtitle,
      titleWeight,
      extraComponent,
      emailText,
      primary,
      secondary,
      customFooter,
      image,
    } = this.props;
    return (
      <ModalMessageView
        customFooter={customFooter}
        style={{ width: 360, maxWidth: WP90 }}
        titleStyle={{
          lineHeight: WP6,
          marginBottom: WP305,
        }}
        subtitleStyle={{
          lineHeight: WP5,
          marginBottom: WP2,
        }}
        buttonPrimaryStyle={{
          backgroundColor: REDDISH,
          paddingVertical: WP05,
          borderRadius: WP2,
        }}
        buttonSecondaryStyle={[
          {
            paddingVertical: WP305,
            borderRadius: WP2,
          },
          get(secondary, "style"),
        ]}
        toggleModal={noop}
        isVisible={visible}
        title={title}
        titleType="Circular"
        titleSize={"small"}
        titleWeight={titleWeight}
        titleColor={GUN_METAL}
        subtitle={subtitle}
        subtitleType="Circular"
        subtitleSize={"xmini"}
        subtitleWeight={400}
        subtitleColor={SHIP_GREY_CALM}
        extraSubtitle={extraSubtitle}
        extraComponent={extraComponent}
        emailText={emailText}
        image={image}
        imageSize={WP30}
        buttonPrimaryText={get(primary, "text")}
        buttonPrimaryAction={get(primary, "onPress")}
        buttonPrimaryTextType="Circular"
        buttonPrimaryTextWeight={400}
        buttonPrimaryBgColor={REDDISH}
        buttonSecondaryBgColor={get(secondary, "backgroundColor") || PALE_WHITE}
        buttonSecondaryText={get(secondary, "text")}
        buttonSecondaryTextType="Circular"
        buttonSecondaryTextWeight={400}
        buttonSecondaryTextColor={get(secondary, "textColor") || SHIP_GREY_CALM}
        buttonSecondaryAction={get(secondary, "onPress")}
      />
    );
  }
}

ConfirmationModalV3.propTypes = {
  visible: PropTypes.bool,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  primary: PropTypes.objectOf(PropTypes.any),
  secondary: PropTypes.objectOf(PropTypes.any),
  customFooter: PropTypes.any,
};

export default ConfirmationModalV3;
