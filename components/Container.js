import React from 'react'
import PropTypes from 'prop-types'
import { RefreshControl, StatusBar, View, ScrollView } from 'react-native'
import Constants from 'expo-constants'
import { LinearGradient } from 'expo-linear-gradient'
import { withNavigation } from '@react-navigation/compat'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { isFunction, keys, noop } from 'lodash-es'
import { PALE_WHITE, WHITE, WHITE50 } from '../constants/Colors'
import { HP100, WP100, WP20 } from '../constants/Sizes'
import {
  BORDER_COLOR,
  BORDER_WIDTH,
  LINEAR_TYPE,
} from '../constants/Styles'
import Loader from './Loader'
import Network from './Network'
import ReviewModal from './ReviewModal'

const propsType = {
  borderedHeader: PropTypes.bool,
  colors: PropTypes.array,
  headerBackground: PropTypes.string,
  isAvoidingView: PropTypes.bool,
  isLoading: PropTypes.bool,
  isReady: PropTypes.bool,
  isTimeline: PropTypes.bool,
  hasBottomNavbar: PropTypes.bool,
  key: PropTypes.any,
  keyboardShouldPersistTaps: PropTypes.string,
  loadingContent: PropTypes.func,
  noStatusBar: PropTypes.bool,
  noStatusBarPadding: PropTypes.bool,
  onPullDownToRefresh: PropTypes.func,
  onPullUpToLoad: PropTypes.func,
  onRefocusScreen: PropTypes.func,
  onScroll: PropTypes.func,
  onScrollViewRef: PropTypes.func,
  outsideContent: PropTypes.any,
  outsideScrollContent: PropTypes.any,
  outsideScrollContentTop: PropTypes.any,
  renderHeader: PropTypes.func,
  scrollable: PropTypes.bool,
  scrollBackgroundColor: PropTypes.string,
  scrollContentContainerStyle: PropTypes.any,
  style: PropTypes.object,
  theme: PropTypes.oneOf(['dark', 'light']),
  type: PropTypes.oneOf(keys(LINEAR_TYPE)),
  scrollEventThrottle: PropTypes.number,
  scrollBottomPadding: PropTypes.number,
  bounces: PropTypes.bool,
}

const propsDefault = {
  borderedHeader: false,
  colors: [WHITE, WHITE],
  end: [0, 1],
  headerBackground: 'transparent',
  isAvoidingView: false,
  isLoading: false,
  isReady: true,
  isRefreshing: false,
  isTimeline: false,
  hasBottomNavbar: false,
  key: undefined,
  keyboardShouldPersistTaps: 'handled',
  noStatusBar: false,
  noStatusBarPadding: false,
  onRefocusScreen: noop,
  onScroll: (nativeEvent) => {},
  onScrollViewRef: (ref) => {},
  outsideContent: noop,
  outsideScrollContent: noop,
  outsideScrollContentTop: noop,
  scrollable: false,
  scrollBackgroundColor: PALE_WHITE,
  scrollContentContainerStyle: {},
  start: [0, 0],
  style: {},
  theme: 'dark',
  type: 'vertical',
  scrollEventThrottle: 0,
  scrollBottomPadding: 0,
  bounces: true,
}

class Container extends React.PureComponent {
  state = {
    isLoadMore: false,
    isRefreshing: false,
  };

  componentDidMount() {
    const { theme, navigation } = this.props
    this._navDidFocusListenerUnsubs = navigation.addListener(
      'focus',
      () => {
        this.props.onRefocusScreen()
        StatusBar.setBarStyle(`${theme}-content`)
      },
    )
  }

  componentWillUnmount() {
    this._navDidFocusListenerUnsubs()
  }

  isCloseToBottom = ({
    layoutMeasurement,
    contentOffset,
    contentSize,
  }) => {
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - 20 + this.props.scrollBottomPadding
    )
  };

  render() {
    const {
      borderedHeader,
      children,
      colors,
      headerBackground,
      isAvoidingView,
      isLoading,
      isReady,
      isTimeline,
      hasBottomNavbar,
      keyboardShouldPersistTaps,
      loadingContent,
      noStatusBar,
      noStatusBarPadding,
      onPullDownToRefresh,
      onPullUpToLoad,
      onScroll,
      onScrollViewRef,
      outsideContent,
      outsideScrollContent,
      outsideScrollContentTop,
      renderHeader,
      scrollable,
      scrollBackgroundColor,
      scrollContentContainerStyle,
      statusBarBackground,
      style,
      theme,
      type,
      scrollEventThrottle,
      bounces,
      onBackOnline = noop,
    } = this.props
    const { isLoadMore, isRefreshing } = this.state
    const ScrollViewContainer = isAvoidingView
      ? KeyboardAwareScrollView
      : ScrollView
    return (
      <LinearGradient
        colors={colors}
        {...LINEAR_TYPE[type]}
        style={[
          {
            flex: 1,
            paddingTop: noStatusBarPadding ? 0 : Constants.statusBarHeight,
          },
          style,
        ]}
      >
        <StatusBar
          animated
          translucent={true}
          hidden={noStatusBar}
          backgroundColor={statusBarBackground || 'transparent'}
          barStyle={`${theme}-content`}
        />
        <View
          style={{
            borderColor: BORDER_COLOR,
            backgroundColor: headerBackground,
            borderBottomWidth: borderedHeader ? BORDER_WIDTH : 0,
            zIndex: isTimeline ? 9990 : 1,
          }}
        >
          {isFunction(renderHeader) ? renderHeader() : null}
        </View>
        <Network onBackOnline={onBackOnline}>
          {!isReady ? (
            isFunction(loadingContent) ? (
              loadingContent()
            ) : (
              <View
                style={{
                  flexGrow: 1,
                  backgroundColor: isReady ? scrollBackgroundColor : WHITE,
                }}
              >
                <Loader isLoading />
              </View>
            )
          ) : !scrollable ? (
            children
          ) : (
            <View
              style={{
                flex: 1,
                flexGrow: 1,
                backgroundColor: scrollBackgroundColor,
              }}
            >
              {outsideScrollContentTop()}
              <ScrollViewContainer
                ref={onScrollViewRef}
                keyboardShouldPersistTaps={keyboardShouldPersistTaps}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={[
                  { backgroundColor: scrollBackgroundColor, flexGrow: 1 },
                  scrollContentContainerStyle,
                ]}
                bounces={bounces}
                refreshControl={
                  onPullDownToRefresh ? (
                    <RefreshControl
                      refreshing={isRefreshing}
                      onRefresh={
                        onPullDownToRefresh
                          ? () => {
                              this.setState(
                                { isRefreshing: true },
                                async () => {
                                  await onPullDownToRefresh()
                                  this.setState({
                                    isRefreshing: false,
                                  })
                                },
                              )
                            }
                          : undefined
                      }
                    />
                  ) : undefined
                }
                scrollEventThrottle={scrollEventThrottle}
                onScroll={async ({ nativeEvent }) => {
                  onScroll(nativeEvent)
                  if (
                    this.isCloseToBottom(nativeEvent) &&
                    onPullUpToLoad
                  ) {
                    this.setState({ isLoadMore: true })
                    await onPullUpToLoad()
                    setTimeout(async () => {
                      this.setState({ isLoadMore: false })
                    }, 1000)
                  }
                }}
              >
                {children}
                {isLoadMore && <Loader size='mini' isLoading />}
                {hasBottomNavbar ? (
                  <View style={{ height: WP20 }} />
                ) : null}
              </ScrollViewContainer>
              {outsideScrollContent()}
            </View>
          )}
          {outsideContent()}
        </Network>
        {isLoading && isReady && (
          <View
            style={{
              backgroundColor: WHITE50,
              position: 'absolute',
              width: WP100,
              height: HP100,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            {isFunction(loadingContent) ? (
              loadingContent()
            ) : (
              <Loader isLoading />
            )}
          </View>
        )}

        <ReviewModal />
      </LinearGradient>
    )
  }
}

Container.defaultProps = propsDefault
Container.propTypes = propsType
export default withNavigation(Container)
