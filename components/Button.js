import React from 'react'
import PropTypes from 'prop-types'
import { Platform, TouchableOpacity } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { keys } from 'lodash-es'
import { GREY, GREY_CALM_SEMI, ORANGE_BRIGHT, PINK_RED, WHITE } from '../constants/Colors'
import { HP105, HP205, WP1, WP100, WP2, WP205, WP3, WP4, WP5 } from '../constants/Sizes'
import { SHADOW_STYLE } from '../constants/Styles'
import Text from './Text'
import Icon from './Icon'
import Image from './Image'

const propsType = {
  key: PropTypes.any,
  disable: PropTypes.bool,
  disableColor: PropTypes.string,
  shadow: PropTypes.oneOf(keys(SHADOW_STYLE)),
  backgroundColor: PropTypes.string,
  iconType: PropTypes.string,
  iconName: PropTypes.string,
  iconColor: PropTypes.string,
  iconSize: PropTypes.string,
  iconPosition: PropTypes.oneOf(['left', 'right']),
  imageIcon: PropTypes.any,
  imageSize: PropTypes.string,
  text: PropTypes.string,
  textColor: PropTypes.string,
  textType: PropTypes.string,
  textSize: PropTypes.string,
  textWeight: PropTypes.number,
  onPress: PropTypes.func,
  opacity: PropTypes.number,
  defaultOpacity: PropTypes.number,
  radius: PropTypes.number,
  width: PropTypes.any,
  centered: PropTypes.bool,
  bottomButton: PropTypes.bool,
  contentStyle: PropTypes.any,
  textCentered: PropTypes.bool,
  rounded: PropTypes.bool,
  marginless: PropTypes.bool,
  soundfren: PropTypes.bool,
  compact: PropTypes.oneOf(['flex-start', 'center', 'flex-end']),
  style: PropTypes.any,
  colors: PropTypes.array,
  start: PropTypes.array,
  end: PropTypes.array,
  textStyle: PropTypes.objectOf(PropTypes.any),
  toggle: PropTypes.bool,
  toggleActive: PropTypes.bool,
  toggleInactiveColor: PropTypes.string,
  toggleActiveColor: PropTypes.string,
  borderless: PropTypes.bool
}

const propsDefault = {
  key: undefined,
  disable: false,
  disableColor: GREY_CALM_SEMI,
  shadow: 'shadowThin',
  backgroundColor: WHITE,
  iconName: undefined,
  iconType: undefined,
  iconColor: GREY,
  iconSize: 'huge',
  iconPosition: 'left',
  imageIcon: undefined,
  imageSize: undefined,
  textColor: GREY,
  textSize: 'large',
  textType: 'Circular',
  textWeight: 400,
  opacity: .8,
  defaultOpacity: 1,
  radius: undefined,
  centered: false,
  marginless: false,
  textCentered: false,
  rounded: false,
  start: [0, 0],
  end: [1, 0],
  bottomButton: false,
  style: {},
  textStyle: {},
  toggleActive: false,
  toggleInactiveColor: WHITE,
  borderless: false
}

class Button extends React.Component {
  state = {
    borderRadius: 0
  }

  _generateBorder = (element, linearGradientColors) => {
    const {
      toggle,
      radius,
      rounded,
      marginless,
      borderless
    } = this.props

    if (borderless) return element

    return toggle ? (
      <LinearGradient
        colors={linearGradientColors}
        start={[0, 0]} end={[1, 0]}
        style={{
          marginHorizontal: marginless ? 0 : WP1,
          marginVertical: marginless ? 0 : WP1,
          padding: 1,
          borderRadius: radius >= 0 ? radius : rounded ? Platform.select({
            ios: this.state.borderRadius,
            android: WP100
          }) : WP1
        }}
      >
        {element}
      </LinearGradient>
    ) : element
  }

  render() {
    const {
      disable,
      disableColor,
      shadow,
      bottomButton,
      backgroundColor,
      iconName,
      iconType,
      iconColor,
      iconSize,
      iconPosition,
      text,
      textSize,
      textWeight,
      onPress,
      opacity,
      radius,
      centered,
      marginless,
      width,
      style,
      soundfren,
      textCentered,
      textType,
      colors,
      contentStyle,
      rounded,
      compact,
      start,
      end,
      textStyle,
      defaultOpacity,
      imageIcon,
      imageSize,
      toggle,
      toggleActive,
      toggleInactiveColor,
      toggleInactiveTextColor,
      toggleActiveColor,
      keyIndex
    } = this.props
    const linearGradientColors = disable ? [disableColor, disableColor] : colors || (soundfren ? [PINK_RED, ORANGE_BRIGHT] : [backgroundColor, backgroundColor])
    const textColor = soundfren ? WHITE : this.props.textColor
    return (
      <TouchableOpacity
        key={keyIndex}
        disabled={disable}
        onPress={onPress}
        activeOpacity={opacity}
        style={[{ width, marginVertical: marginless ? 0 : HP105 }, style, SHADOW_STYLE[shadow]]}
      >
        {
          this._generateBorder(
            <LinearGradient
              onLayout={(event) => {
                const { height } = event.nativeEvent.layout
                this.setState({ borderRadius: height / 2 })
              }}
              opacity={defaultOpacity}
              colors={toggle ? toggleActive ? linearGradientColors : [toggleInactiveColor, toggleInactiveColor] : linearGradientColors}
              start={start} end={end}
              style={
                [
                  {
                    backgroundColor,
                    paddingHorizontal: text ? iconName ? WP4 : compact ? WP3 : WP5 : undefined,
                    paddingVertical: bottomButton ? HP205 : text ? compact ? WP1 : WP205 : undefined,
                    borderRadius: radius >= 0 ? radius : rounded ? Platform.select({
                      ios: this.state.borderRadius,
                      android: WP100
                    }) : bottomButton ? 0 : WP1,
                    justifyContent: centered ? 'center' : 'flex-start',
                    overflow: 'hidden',
                    flexDirection: 'row',
                  },
                  contentStyle,
                  SHADOW_STYLE[shadow]
                ]
              }
            >
              {
                iconPosition === 'left' && iconName &&
                <Icon centered size={iconSize || textSize} name={iconName} color={soundfren ? WHITE : iconColor} type={iconType}/>
              }
              {
                imageIcon &&
                <Image source={imageIcon} size={imageSize || null} />
              }
              {
                text && (
                  <Text
                    style={[
                      iconName ? textCentered ? {} : iconPosition === 'left' ? { paddingLeft: WP2 } : { paddingRight: WP2 } : {},
                      textStyle
                    ]
                    }
                    type={textType}
                    weight={textWeight}
                    centered={textCentered || centered}
                    size={textSize}
                    color={toggle ? toggleActive ? toggleActiveColor : toggleInactiveTextColor || toggleInactiveColor : textColor}
                  >
                    {text}
                  </Text>
                )
              }
              {
                iconPosition === 'right' && iconName &&
                <Icon centered size={iconSize || textSize} name={iconName} color={soundfren ? WHITE : iconColor} type={iconType}/>
              }
            </LinearGradient>, linearGradientColors
          )
        }
      </TouchableOpacity>
    )
  }
}

Button.propTypes = propsType

Button.defaultProps = propsDefault

export default Button
