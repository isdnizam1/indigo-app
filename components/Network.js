import React from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { noop } from 'lodash'
import { HP30, HP1, WP10, HP2 } from '../constants/Sizes'
import { WHITE } from '../constants/Colors'
import { NETWORK_ONLINE, NETWORK_OFFLINE, NETWORK_BUSY } from '../constants/Network'
import Text from './Text'
import Image from './Image'

const MAP_ERROR = {
  [NETWORK_OFFLINE]: {
    image: require('../assets/images/illNotConnected.png'),
    title: 'You seem to be offline',
    message:
      'Slow or no internet connection. Please check your connection and try again.',
  },
  [NETWORK_BUSY]: {
    image: require('../assets/images/illServerBusy.png'),
    title: 'You seem to be offline',
    message:
      'Slow or no internet connection. Please check your connection and try again.',
  },
}
const propsType = {}

const propsDefault = {
  network: NETWORK_ONLINE,
  backgroundColor: WHITE,
  imageHeight: HP30,
  full: true,
  aspectRatio: 176 / 175,
}

class Network extends React.Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { onBackOnline = noop } = this.props
    prevProps.network != NETWORK_ONLINE &&
      this.props.network == NETWORK_ONLINE &&
      onBackOnline()
  }

  render() {
    const {
      network,
      full,
      imageHeight,
      actions,
      style,
      backgroundColor,
      aspectRatio,
      children,
    } = this.props
    const networkError = MAP_ERROR[network ? network : NETWORK_BUSY]
    return network === NETWORK_ONLINE ? (
      children
    ) : (
      <View
        style={[
          {
            justifyContent: 'center',
            flexGrow: full ? 1 : 0,
            paddingVertical: full ? 0 : HP2,
            alignItems: 'center',
            backgroundColor,
            paddingHorizontal: WP10,
          },
          style,
        ]}
      >
        <Image
          source={networkError.image}
          style={{ marginVertical: HP1 }}
          imageStyle={{ height: imageHeight, aspectRatio }}
        />
        <Text size='large' weight={500}>
          {networkError.title}
        </Text>
        <Text centered>{networkError.message}</Text>
        {React.isValidElement(actions) && actions}
      </View>
    )
  }
}

Network.propTypes = propsType
Network.defaultProps = propsDefault
export default connect((state) => ({ network: state.setting.network }), {})(Network)
