import React, { Component } from 'react'
import { ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native'
import PropTypes from 'prop-types'
import { LinearGradient } from 'expo-linear-gradient'
import { isEmpty, noop } from 'lodash-es'
import SkeletonContent from 'react-native-skeleton-content'
import {
  BLUE_AZURE,
  BLUE_LIGHT,
  GREY,
  GREY_WARM,
  ORANGE,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  WHITE,
} from '../../constants/Colors'
import { WP105, WP2, WP35, WP405, WP6 } from '../../constants/Sizes'
import Text from '../Text'
import { BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'
import SessionItem from '../../screens/soundconnect/SessionItem'
import SoundplayItem from '../../screens/soundplay/SoundplayItem'
import { postLogSoundfrenPlaySeeAllButton } from '../../actions/api'
import VisibleView from '../VisibleView'
import CollaborationItem from './items/CollaborationItem'
import VideoItem from './items/VideoItem'
import SubmissionItem from './items/SubmissionItem'
import BandItem from './items/BandItem'
import EventItem from './items/EventItem'
import { adsConfig } from './ExploreConstant'
import Layout from './ExploreSkeletonLayout'

function _elevationShadowStyle(elevation) {
  return {
    elevation,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0.5 * elevation },
    shadowOpacity: 0.4,
    shadowRadius: 5,
  }
}

const styles = StyleSheet.create({
  shadow1: _elevationShadowStyle(5),
  shadow2: _elevationShadowStyle(10),
  shadow3: _elevationShadowStyle(20),
  box: {
    borderRadius: 12,
    backgroundColor: 'white',
    padding: 24,
  },
  showAll: { marginRight: WP6 },
  showAllBand: {
    borderRadius: 8,
    padding: WP105,
    borderWidth: BORDER_WIDTH,
    borderColor: WHITE,
    marginRight: WP405,
  },
})

class ExploreSection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      adList: [],
    }
  }

  _onPressItem = (ads) => () => {
    const { navigateTo, category } = this.props

    if (category === 'collaboration') {
      navigateTo('CollaborationPreview', { id: ads.id_ads })
    } else if (category === 'submission') {
      navigateTo('SubmissionPreview', { id_ads: ads.id_ads })
    } else if (category === 'soundconnect') {
      navigateTo('SessionDetail', { id_ads: ads.id_ads })
    } else if (category === 'learn') {
      navigateTo('SoundplayDetail', { id_ads: ads.id_ads })
    } else if (category === 'band') {
      navigateTo('ProfileScreenNoTab', { idUser: ads.id_user })
    } else {
      navigateTo(
        ['event', 'submission', 'band'].includes(category)
          ? 'AdsDetailScreenNoTab'
          : category === 'video'
            ? 'PremiumVideoTeaser'
            : 'AdsDetailScreen',
        { id_ads: ads.id_ads }
      )
    }
  };

  _renderHeader = () => {
    const { loading, category, navigateTo, availableAd, idUser } = this.props
    const isBand = category === 'band'
    if (loading) {
      return (
        <View style={{ paddingHorizontal: WP6 }}>
          <SkeletonContent
            containerStyle={Layout.container}
            layout={Layout.header}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          />
        </View>
      )
    }
    return (
      <>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'flex-end',
            paddingLeft: WP6,
          }}
        >
          <Text
            size='small'
            color={isBand ? WHITE : GREY}
            weight={500}
            type='NeoSans'
          >
            {adsConfig[category].title}
          </Text>
          <VisibleView visible={category != 'event'}>
            <TouchableOpacity
              disabled={loading}
              activeOpacity={TOUCH_OPACITY}
              style={isBand ? styles.showAllBand : styles.showAll}
              onPress={() => {
                if (category === 'video') {
                  navigateTo('VideoListScreen')
                } else if (category === 'soundconnect') {
                  navigateTo('SoundconnectScreen')
                } else if (category === 'soundplay') {
                  Promise.resolve(navigateTo('SoundplayScreen')).then(() => {
                    postLogSoundfrenPlaySeeAllButton({ id_user: idUser })
                  })
                } else if (category === 'learn') {
                  navigateTo('SoundfrenLearnScreen')
                } else if (category === 'collaboration') {
                  navigateTo('CollaborationScreen')
                } else if (category === 'band') {
                  navigateTo('ArtistSpotlightScreen')
                } else if (category === 'submission') {
                  navigateTo('Submission')
                } else {
                  navigateTo('AdsScreen', {
                    category,
                    availableAd: availableAd(),
                  })
                }
              }}
            >
              <Text
                size='tiny'
                type='NeoSans'
                weight={500}
                color={isBand ? WHITE : ORANGE}
              >
                See All
              </Text>
            </TouchableOpacity>
          </VisibleView>
        </View>

        {!isEmpty(adsConfig[category].subtitle) && (
          <Text
            size='xtiny'
            color={isBand ? WHITE : GREY_WARM}
            style={{ paddingLeft: WP6 }}
          >
            {adsConfig[category].subtitle}
          </Text>
        )}
      </>
    )
  };

  _adItem = (ads, i, category, loading = true) => {
    if (loading) {
      const skeleton = Layout.item(category)
      return (
        <View
          style={{
            borderRadius: 12,
            marginRight: WP6,
            width: WP35,
            backgroundColor: WHITE,
            ...skeleton.wrapperStyle,
          }}
        >
          <SkeletonContent
            containerStyle={{
              ...Layout.container,
              alignItems: category === 'band' ? 'center' : 'flex-start',
            }}
            layout={skeleton.layout}
            isLoading={loading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          />
        </View>
      )
    }

    return (
      <TouchableOpacity
        disabled={loading}
        style={{ paddingVertical: WP2 }}
        key={`${category}-${ads.id_ads ?? ads.id_user}`}
        activeOpacity={TOUCH_OPACITY}
        onPress={this._onPressItem(ads)}
      >
        {category === 'soundconnect' && (
          <SessionItem horizontal ads={ads} loading={loading} />
        )}
        {category === 'learn' && (
          <SoundplayItem ads={ads} loading={loading} badge />
        )}
        {category === 'collaboration' && (
          <CollaborationItem ads={ads} loading={loading} />
        )}
        {category === 'video' && <VideoItem ads={ads} loading={loading} />}
        {category === 'submission' && (
          <SubmissionItem ads={ads} loading={loading} />
        )}
        {category === 'band' && <BandItem ads={ads} loading={loading} />}
        {category === 'event' && <EventItem ads={ads} loading={loading} />}
      </TouchableOpacity>
    )
  };

  _adListGroup = () => {
    const { category, adList, loading = true } = this.props

    const isBand = category === 'band',
      bgColor = isBand ? [BLUE_AZURE, BLUE_LIGHT] : [WHITE, WHITE],
      dummyData = [1, 2, 3],
      data = loading ? dummyData : adList

    return (
      <LinearGradient
        colors={bgColor}
        start={[0, 0]}
        end={[0, 1]}
        style={{
          marginBottom: WP6,
          paddingVertical: isBand ? WP6 : 0,
        }}
      >
        {this._renderHeader()}

        <ScrollView
          horizontal
          style={{ flexGrow: 0 }}
          contentContainerStyle={{ paddingLeft: WP6, marginTop: WP2 }}
          showsHorizontalScrollIndicator={false}
        >
          {data.map((ads, i) => this._adItem(ads, i, category, loading))}
        </ScrollView>
      </LinearGradient>
    )
  };

  render() {
    const { adList, loading } = this.props
    if (!isEmpty(adList) || loading) {
      return this._adListGroup()
    }
    return <View />
  }
}

ExploreSection.propTypes = {
  category: PropTypes.string,
  adList: PropTypes.arrayOf(PropTypes.any),
  availableAd: PropTypes.func,
}

ExploreSection.defaultProps = {
  category: '',
  adList: [],
  availableAd: noop,
}

export default ExploreSection
