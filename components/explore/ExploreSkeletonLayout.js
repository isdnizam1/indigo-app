import { WP2, WP205, WP25, WP3, WP30, WP35, WP4, WP40, WP50, WP6, WP80 } from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'
import { NO_COLOR } from '../../constants/Colors'

const defaultItem = {
  width: WP80, height: WP35,
  marginRight: WP6, marginBottom: WP2
}

const defaultItemTitle = { width: WP50, height: WP4, marginBottom: WP2, marginLeft: WP3 }
const defaultItemSubtitle = { width: WP35, height: WP205, marginBottom: WP3, marginLeft: WP3 }

const container = { flex: 1 }

const header = [
  { width: WP30, height: WP6, marginBottom: WP2 },
  { width: WP40, height: WP205, marginBottom: WP2 }
]

const body = []

const item = (category) => {
  let layout = []
  let wrapperStyle = {}
  switch (category) {
  case 'soundplay', 'learn':
    layout = [
      { ...defaultItem, width: WP35 },
      { ...defaultItemTitle, width: WP25, marginLeft: 0 },
      { ...defaultItemSubtitle, width: WP30, marginLeft: 0 },
    ]
    break
  case 'band':
    layout = [
      { ...defaultItem, width: WP35, borderRadius: WP35/2, marginLeft: WP6 },
      { ...defaultItemTitle, width: WP25, marginLeft: 0 },
      { ...defaultItemSubtitle, width: WP30, marginLeft: 0 },
    ]

    wrapperStyle = {
      backgroundColor: NO_COLOR
    }
    break
  default:
    layout = [
      { ...defaultItem, borderTopRightRadius: 12, borderTopLeftRadius: 12 },
      defaultItemTitle,
      defaultItemSubtitle
    ]
    wrapperStyle = {
      width: WP80,
      ...SHADOW_STYLE.shadowThin,
      marginBottom: WP3
    }
  }
  return {
    layout,
    wrapperStyle
  }
}

export default {
  container,
  header,
  body,
  item
}
