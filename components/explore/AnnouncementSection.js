import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import Carousel from 'react-native-snap-carousel'
import Pagination from 'react-native-snap-carousel/src/pagination/Pagination'
import SkeletonContent from 'react-native-skeleton-content'
import { WP100, WP3, WP40, WP6, WP80 } from '../../constants/Sizes'
import { SHADOW_STYLE } from '../../constants/Styles'
import { GREY_CALM_SEMI, SKELETON_COLOR, SKELETON_HIGHLIGHT, TOMATO, WHITE_MILK } from '../../constants/Colors'
import Image from '../Image'

class AnnouncementSection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeSlider: 0
    }
  }

  _renderItem = ({ item, index }) => {
    const { loading, navigateTo } = this.props
    const wrapperStyle = {
      width: WP80,
      borderRadius: 12,
      marginRight: WP6,
      marginBottom: WP3,
      ...SHADOW_STYLE.shadowThin,
    }

    if (loading) {
      return (
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[
            { width: WP80,
              borderRadius: 12,
              marginRight: WP6,
              marginBottom: WP3, height: WP40 }
          ]}
          isLoading={loading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        />
      )
    }
    return (
      <View
        style={wrapperStyle}
        key={`${index}-announcement`}
      >
        <View style={{
          borderRadius: 12,
          overflow: 'hidden',
          backgroundColor: WHITE_MILK,
          height: WP40,
        }}
        >
          {
            !loading && (
              <Image
                centered
                onPress={() => navigateTo('AdsDetailScreenNoTab', { id_ads: item.id_ads })}
                source={{ uri: item.image }}
                imageStyle={{ height: WP40, aspectRatio: 2 / 1 }}
              />
            )
          }
        </View>
      </View>
    )
  }

  render() {
    const {
      list, loading
    } = this.props

    const {
      activeSlider
    } = this.state
    const dummyData = [1, 2, 3, 4],
      data = loading ? dummyData : list

    return (
      <View style={{ marginBottom: WP6 }}>
        <Carousel
          onSnapToItem={(i) => this.setState({ activeSlider: i })}
          data={data}
          firstItem={activeSlider}
          inactiveSlideScale={1}
          activeSlideAlignment='start'
          sliderWidth={WP100}
          itemWidth={WP80 + WP6}
          containerCustomStyle={{ paddingLeft: WP6 }}
          extraData={this.state}
          renderItem={this._renderItem}
        />
        <Pagination
          dotsLength={data.length}
          activeDotIndex={activeSlider}
          dotColor={TOMATO}
          inactiveDotColor={GREY_CALM_SEMI}
          inactiveDotScale={1}
          containerStyle={{
            justifyContent: 'flex-start',
            paddingVertical: 0,
            paddingLeft: WP6 + 1,
            paddingRight: 0,
          }}
          dotContainerStyle={{ marginLeft: 1 }}
          dotStyle={{
            width: 8,
            height: 8,
            borderRadius: 4,
          }}
        />
      </View>
    )
  }
}

AnnouncementSection.propTypes = {
  list: PropTypes.arrayOf(PropTypes.any)
}

AnnouncementSection.defaultProps = {
  list: []
}

export default AnnouncementSection
