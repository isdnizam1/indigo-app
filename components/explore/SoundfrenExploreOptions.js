import React, { Component } from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, View, Platform } from "react-native";
import noop from "lodash-es/noop";
import Text from "../Text";
import { WP3, WP4, WP6, WP7 } from "../../constants/Sizes";
import {
  GREY_CALM,
  SHIP_GREY_CALM,
  PALE_GREY,
  REDDISH,
} from "../../constants/Colors";
import Image from "../Image";
import { getSubscriptionDetail } from "../../actions/api";

class SoundfrenExploreOptions extends Component {
  constructor(props) {
    super(props);
    const { withPromoteUser, menus } = this.props.menuOptions(props);
    this.state = {
      withPromoteUser,
      menus,
    };
  }

  componentDidMount() {
    const { userData } = this.props;

    let { menus, withPromoteUser } = this.state;

    if (withPromoteUser) {
      getSubscriptionDetail({ id_user: userData.id_user }).then((response) => {
        const { package_type } = response.data.result;
        let isPremium = package_type && package_type !== "free";
        if (!isPremium) {
          // menus[0].name = 'Upgrade Premium'
          menus[0].onPress = () => {
            this.props.onClose();
            this.props.navigateTo("MembershipScreen", {
              paymentSource: "Soundfren Learn Popup",
            });
          };
          // menus[0].joinButton = true
          this.setState({ menus });
        }
      });
    }
  }

  _renderItem = (menu) => {
    if (menu.type === "separator") {
      return (
        <View
          style={{ borderBottomColor: GREY_CALM, borderBottomWidth: 1 }}
          key={`${Math.random()}`}
        />
      );
    }
    if (Platform.OS !== "android" && menu.isPremiumMenu) return null;

    return (
      <TouchableOpacity
        key={`${Math.random()}`}
        onPress={menu.onPress}
        style={{
          flexDirection: "row",
          paddingVertical: WP4,
          alignItems: "center",
          paddingHorizontal: WP6,
        }}
      >
        {!!menu.image && (
          <Image
            source={menu.image}
            size={menu.size || "xsmall"}
            imageStyle={{ marginRight: WP6 }}
            centered
          />
        )}
        <View>
          <Text type="Circular" size="medium" style={menu.textStyle}>
            {menu.name}
          </Text>
          {!!menu.desc && (
            <Text
              type="Circular"
              size="xmini"
              color={REDDISH}
              style={menu.textStyle}
            >
              {menu.desc}
            </Text>
          )}
        </View>
        {menu.joinButton && (
          <View
            style={{
              borderRadius: 5,
              paddingHorizontal: WP3,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: PALE_GREY,
              textAlign: "center",
              textAlignVertical: "center",
              lineHeight: 16,
              marginLeft: "auto",
              paddingVertical: 4,
            }}
          >
            <Text
              color={SHIP_GREY_CALM}
              weight={400}
              type={"Circular"}
              size={"xmini"}
            >
              Join Now
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  };

  render() {
    const { menus } = this.state;
    return (
      <View
        style={{
          paddingVertical: WP3,
        }}
      >
        {menus.map((menu, index) => this._renderItem(menu, index))}
      </View>
    );
  }
}
SoundfrenExploreOptions.propTypes = {
  onClose: PropTypes.func,
  menuOptions: PropTypes.func,
};

SoundfrenExploreOptions.defaultProps = {
  onClose: noop,
  menuOptions: () => ({
    menus: [],
  }),
};

export default SoundfrenExploreOptions;
