import React from 'react'
import { View } from 'react-native'
import { WP05, WP100, WP2, WP35, WP6 } from '../../../constants/Sizes'
import Image from '../../Image'
import { SHADOW_STYLE } from '../../../constants/Styles'
import Text from '../../Text'
import { WHITE, BLACK10 } from '../../../constants/Colors'

const BandItem = ({ ads: artist, loading = true }) => {
  const title = loading ? 'Artist Name' : artist.full_name
  const genre = loading ? 'Genre' : artist.interest.join(', ')

  return (
    <View style={{
      marginRight: WP6,
      marginLeft: 1,
      width: WP35,
    }}
    >
      <View style={{ height: WP35, width: WP35, backgroundColor: BLACK10, borderRadius: WP100, ...SHADOW_STYLE[loading ? 'none' : 'shadowThin'] }}>
        {
          !loading && (
            <View>
              <View style={{
                borderRadius: WP100,
                overflow: 'hidden',
              }}
              >
                <Image
                  source={{ uri: artist.profile_picture }}
                  imageStyle={{ height: WP35, aspectRatio: 1 }}
                />
              </View>
            </View>
          )
        }
      </View>
      <View style={{ marginBottom: WP05, marginTop: WP2 }}>
        <Text color={WHITE} numberOfLines={1} size='mini' type='NeoSans' weight={500} centered>{title}</Text>
      </View>
      <Text numberOfLines={1} ellipsizeMode='tail' size='tiny' color={WHITE} centered>{genre}</Text>
    </View>
  )
}

BandItem.propTypes = {}

BandItem.defaultProps = {}

export default BandItem
