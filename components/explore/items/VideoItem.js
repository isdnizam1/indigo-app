import React from 'react'
import { View, StyleSheet, Image as RNImage } from 'react-native'
import moment from 'moment'
import { isEmpty } from 'lodash-es'
import { WP05, WP1, WP100, WP105, WP2, WP35, WP6, WP3, WP80, WP11, WP405 } from '../../../constants/Sizes'
import Image from '../../Image'
import { SHADOW_STYLE } from '../../../constants/Styles'
import Text from '../../Text'
import { GREY, TOMATO, WHITE, WHITE_MILK, WHITE20 } from '../../../constants/Colors'

const VideoItem = ({ ads, loading = true }) => {
  const additionalData = loading ? {} : JSON.parse(ads.additional_data),
    title = loading ? 'Video Title' : ads.title

  return (
    <View style={{
      width: WP80,
      marginRight: WP6,
      marginLeft: 1,
      borderRadius: 12,
      backgroundColor: WHITE,
      ...SHADOW_STYLE['shadowThin'],
    }}
    >
      <View style={{
        borderRadius: 12,
        overflow: 'hidden',
      }}
      >
        <View style={{
          backgroundColor: WHITE_MILK,
          height: WP35
        }}
        >
          {
            !loading && (
              <View>
                <Image
                  source={{ uri: ads.image }}
                  imageStyle={{ height: WP35, aspectRatio: 2.5 }}
                />
                <View style={{ flex: 1, position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center' }} >
                  <View style={{ width: WP11, height: WP11, borderRadius: WP100, backgroundColor: WHITE20, ...SHADOW_STYLE['shadow'] }}>
                    <Image centered imageStyle={{ width: WP11, }} aspectRatio={1} source={require('../../../assets/icons/video_player/play.png')} />
                  </View>
                </View>
                {
                  moment().diff(moment(ads.updated_at), 'days') <= 3 &&
                  <Image style={{ height: WP2, position: 'absolute', left: 0, bottom: 0, marginVertical: WP1 }} aspectRatio={48 / 16} source={require('../../../assets/images/labelNew3.png')} />
                }
              </View>
            )
          }
        </View>
        <View style={{ paddingHorizontal: WP3, paddingTop: WP105, paddingBottom: WP3 }}>
          <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
            <Text color={GREY} numberOfLines={2} ellipsizeMode='tail' size='tiny' weight={500}>
              {
                ads.is_premium === 1 ? (
                  <RNImage
                    source={require('../../../assets/icons/badgePremium.png')}
                    style={{ height: WP405, width: WP405 }}
                  />
                ) : null
              }
              {' '}
              {title}
            </Text>
          </View>
          {
            loading ? (
              <View style={{ marginTop: WP105, flexDirection: 'row', flexWrap: 'wrap' }}>
                <View style={styles.btnCategory}>
                  <Text color={TOMATO} size='xtiny' style={{ flexGrow: 0 }}>Category</Text>
                </View>
              </View>
            ) : (
              <View style={{ marginTop: WP105, flexDirection: 'row', flexWrap: 'wrap' }}>
                {
                  !isEmpty(additionalData.category) && additionalData.category.slice(0, 3).map((item, index) => (
                    <View
                      key={index}
                      style={styles.btnCategory}
                    >
                      <Text color={TOMATO} size='xtiny' style={{ flexGrow: 0 }}>{index === 2 && additionalData.category.length > 3 ? 'More...' : item}</Text>
                    </View>
                  ))
                }
              </View>
            )
          }
        </View>

      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  btnCategory: {
    borderRadius: WP100,
    borderColor: TOMATO,
    borderWidth: 1,
    paddingVertical: WP05,
    paddingHorizontal: WP105,
    flexGrow: 0,
    marginVertical: WP05,
    marginRight: WP105
  }
})

VideoItem.propTypes = {}

VideoItem.defaultProps = {}

export default VideoItem
