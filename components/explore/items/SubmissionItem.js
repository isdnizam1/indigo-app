import React from 'react'
import { View } from 'react-native'
import moment from 'moment'
import Image from '../../Image'
import { WP1, WP2, WP305, WP35, WP6, WP3, WP105, WP80 } from '../../../constants/Sizes'
import { SHADOW_STYLE } from '../../../constants/Styles'
import { GREY, GREY_WARM, WHITE, WHITE_MILK } from '../../../constants/Colors'
import Text from '../../Text'
import { daysRemaining, toNormalDate } from '../../../utils/date'
import Icon from '../../Icon'

const SubmissionItem = ({ ads, loading = true }) => {
  const additionalData = loading ? {} : JSON.parse(ads.additional_data),
    title = loading ? 'Submission Title' : ads.title,
    date = new Date()

  return (
    <View style={{
      width: WP80,
      marginRight: WP6,
      marginLeft: 1,
      borderRadius: 12,
      backgroundColor: WHITE,
      ...SHADOW_STYLE.shadowThin,
    }}
    >
      <View style={{
        borderRadius: 12,
        overflow: 'hidden',
      }}
      >
        <View style={{
          backgroundColor: WHITE_MILK,
          height: WP35
        }}
        >
          {
            !loading && (
              <View>
                <Image
                  source={{ uri: ads.image }}
                  imageStyle={{ height: WP35, aspectRatio: 2.5 }}
                />
                {
                  moment().diff(moment(ads.updated_at), 'days') <= 3 &&
                  <Image style={{ height: WP2, position: 'absolute', left: 0, bottom: 0, marginVertical: WP1 }} aspectRatio={48 / 16} source={require('../../../assets/images/labelNew3.png')} />
                }
              </View>
            )
          }
        </View>
        <View style={{ paddingHorizontal: WP3, paddingTop: WP105, paddingBottom: WP3 }}>
          <Text color={GREY} numberOfLines={2} size='tiny' weight={500}>{title}</Text>
          {
            loading ? (
              <View style = {{ marginTop: WP105, flexDirection: 'row', alignItems: 'center' }}>
                <Text size='tiny' color={GREY_WARM}>{`${toNormalDate(date)}`}</Text>
                <Icon centered color={GREY_WARM} type='Entypo' name='dot-single'/>
                <Text size='tiny' color={GREY_WARM}>{`${daysRemaining(date.setDate(date.getDate()+1))} Days Left`}</Text>
              </View>
            ) : (
              <View style = {{ marginTop: WP105, flexDirection: 'row', alignItems: 'center' }}>
                {
                  ads.is_premium === 1 ? (
                    <View style = {{ padding: WP1 }}>
                      <Image
                        source={require('../../../assets/icons/badgePremium.png')}
                        imageStyle={{ height: WP305, aspectRatio: 1 }}
                      />
                    </View>
                  ) : null
                }
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text size='tiny' color={GREY_WARM}>{`${toNormalDate(additionalData.date.start)}`}</Text>
                  <Icon centered color={GREY_WARM} type='Entypo' name='dot-single'/>
                  <Text size='tiny' color={GREY_WARM}>{`${daysRemaining(additionalData.date.end)} Days Left`}</Text>
                </View>
              </View>
            )
          }
        </View>
      </View>
    </View>
  )
}

SubmissionItem.propTypes = {}

SubmissionItem.defaultProps = {}

export default SubmissionItem
