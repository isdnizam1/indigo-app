import React from 'react'
import { View, StyleSheet } from 'react-native'
import moment from 'moment'
import { isEmpty } from 'lodash-es'
import { WP05, WP1, WP100, WP105, WP2, WP35, WP6, WP3, WP80 } from '../../../constants/Sizes'
import Image from '../../Image'
import { SHADOW_STYLE } from '../../../constants/Styles'
import Text from '../../Text'
import { GREY, ORANGE, TOMATO, WHITE, WHITE_MILK } from '../../../constants/Colors'

const CollaborationItem = ({ ads, loading = true }) => {
  const additionalData = loading ? {} : JSON.parse(ads.additional_data),
    title = loading ? 'Collaboration Title' : ads.title

  return (
    <View style={{
      width: WP80,
      marginRight: WP6,
      marginLeft: 1,
      borderRadius: 12,
      backgroundColor: WHITE,
      ...SHADOW_STYLE.shadowThin,
    }}
    >
      <View style={{
        borderRadius: 12,
        overflow: 'hidden',
      }}
      >
        <View style={{
          backgroundColor: WHITE_MILK,
          height: WP35
        }}
        >
          {
            !loading && (
              <View>
                <Image
                  source={isEmpty(ads.image) ? require('../../../assets/images/bgDefaultCollab.png') : { uri: ads.image }}
                  imageStyle={{ height: WP35, aspectRatio: 2.5 }}
                />
                {
                  moment().diff(moment(ads.updated_at), 'days') <= 3 &&
                  <Image style={{ height: WP2, position: 'absolute', left: 0, bottom: 0, marginVertical: WP1 }} aspectRatio={48 / 16} source={require('../../../assets/images/labelNew3.png')} />
                }
              </View>
            )
          }
        </View>
        <View style={{ paddingHorizontal: WP3, paddingTop: WP105, paddingBottom: WP3 }}>
          <Text color={GREY} numberOfLines={2} ellipsizeMode='tail' size='tiny' weight={500}>{title}</Text>
          {
            loading ? (
              <View style={{ marginTop: WP105, flexDirection: 'row', flexWrap: 'wrap' }}>
                <View style={styles.btnProfession}>
                  <Text color={TOMATO} size='xtiny' style={{ flexGrow: 0 }}>Profession</Text>
                </View>
              </View>
            ) : (
              <View style={{ marginTop: WP105, flexDirection: 'row', flexWrap: 'wrap' }}>
                {
                  !isEmpty(additionalData.profession) && additionalData.profession.slice(0, 3).map((item, index) => (
                    <View
                      key={index}
                      style={styles.btnProfession}
                    >
                      <Text color={TOMATO} size='xtiny' style={{ flexGrow: 0 }}>{index === 2 && additionalData.profession.length > 3 ? 'More...' : item.name}</Text>
                    </View>
                  ))
                }
              </View>
            )
          }
        </View>

      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  btnProfession: {
    borderRadius: WP100,
    borderColor: ORANGE,
    borderWidth: 1,
    paddingVertical: WP05,
    paddingHorizontal: WP105,
    flexGrow: 0,
    marginVertical: WP05,
    marginRight: WP105
  }
})

CollaborationItem.propTypes = {}

CollaborationItem.defaultProps = {}

export default CollaborationItem
