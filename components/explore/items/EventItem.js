import React from 'react'
import { View } from 'react-native'
import moment from 'moment'
import { WP1, WP2, WP35, WP6, WP80, WP3, WP105 } from '../../../constants/Sizes'
import Image from '../../Image'
import { SHADOW_STYLE } from '../../../constants/Styles'
import Text from '../../Text'
import { GREY, GREY_WARM, WHITE, WHITE_MILK } from '../../../constants/Colors'
import { toNormalDate } from '../../../utils/date'
import Icon from '../../Icon'

const EventItem = ({ ads, loading = true }) => {
  const additionalData = loading ? {} : JSON.parse(ads.additional_data),
    title = loading ? 'Event Title' : ads.title,
    date = loading ? new Date() : additionalData.date.start,
    location = loading ? 'Location Name' : additionalData.location.name

  return (
    <View style={{
      width: WP80,
      marginRight: WP6,
      marginLeft: 1,
      borderRadius: 12,
      backgroundColor: WHITE,
      ...SHADOW_STYLE.shadowThin,
    }}
    >
      <View style={{
        borderRadius: 12,
        overflow: 'hidden',
      }}
      >
        <View style={{
          backgroundColor: WHITE_MILK,
          height: WP35
        }}
        >
          {
            !loading && (
              <View>
                <Image
                  source={{ uri: ads.image }}
                  imageStyle={{ height: WP35, aspectRatio: 2.5 }}
                />
                {
                  moment().diff(moment(ads.updated_at), 'days') <= 3 &&
                  <Image style={{ height: WP2, position: 'absolute', left: 0, bottom: 0, marginVertical: WP1 }} aspectRatio={48 / 16} source={require('../../../assets/images/labelNew3.png')} />
                }
              </View>
            )
          }
        </View>

        <View style={{ paddingHorizontal: WP3, paddingTop: WP105, paddingBottom: WP3 }}>
          <Text color={GREY} numberOfLines={2} size='tiny' weight={500}>{title}</Text>
          <View style = {{ marginTop: WP105, flexDirection: 'row', alignItems: 'center' }}>
            <Text size='tiny' color={GREY_WARM}>{`${toNormalDate(date)}`}</Text>
            <Icon centered color={GREY_WARM} type='Entypo' name='dot-single'/>
            <Text size='tiny' color={GREY_WARM}>{location}</Text>
          </View>
        </View>
      </View>
    </View>
  )
}

EventItem.propTypes = {}

EventItem.defaultProps = {}

export default EventItem
