import SessionItem from '../../screens/soundconnect/SessionItem'
import SoundplayItem from '../../screens/soundplay/SoundplayItem'
import CollaborationItem from './items/CollaborationItem'
import VideoItem from './items/VideoItem'
import SubmissionItem from './items/SubmissionItem'
import BandItem from './items/BandItem'
import EventItem from './items/EventItem'

export const adsConfig = {
  soundplay: {
    title: 'Soundfren Play',
    subtitle: 'Music education through podcast'
  },
  soundconnect: {
    title: 'Soundfren Connect',
    subtitle: 'Video call with music experts'
  },
  collaboration: {
    title: 'Collaboration',
    subtitle: 'Collaborate and work together'
  },
  video: {
    title: 'Soundfren Edu',
    subtitle: 'Get access to premium educational videos'
  },
  submission: {
    title: 'Submission',
    subtitle: 'Find best opportunities to scale up your music career'
  },
  band: {
    title: 'Artist Spotlight',
    subtitle: 'Discover new artists'
  },
  event: {
    title: 'Events',
    subtitle: 'Find concert, workshop, and activity about music here'
  },
  news: {
    title: 'News',
    subtitle: 'Browse newest music updates here'
  },
  learn: {
    title: 'Soundfren Learn',
    subtitle: 'Music education from podcasts & videos'
  },
}

export const AdItemComponent = {
  'soundconnect': SessionItem,
  'soundplay': SoundplayItem,
  'collaboration': CollaborationItem,
  'video': VideoItem,
  'submission': SubmissionItem,
  'band': BandItem,
  'event': EventItem
}

export default {
  adsConfig,
  AdItemComponent
}
