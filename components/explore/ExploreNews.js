import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { HP1, WP105, WP2, WP24, WP3, WP44, WP6 } from '../../constants/Sizes'
import Text from '../Text'
import { DARK_INDIGO, DEEP_ROSE, GREY, GREY_WARM, ROYAL_BLUE, ROYAL_PURPLE, WHITE } from '../../constants/Colors'
import Image from '../Image'
import { NavigateToInternalBrowser } from '../../utils/helper'
import { adsConfig } from './ExploreConstant'

const styles = {
  box: {
    borderRadius: 12,
    backgroundColor: 'white',
    padding: 24
  }
}

const ExploreNews = ({ navigateTo }) => (
  <View style={{
    marginBottom: HP1,
    paddingHorizontal: WP6,
  }}
  >
    <View style={{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'flex-end',
    }}
    >
      <Text size='small' weight={500} color={GREY} type='NeoSans'>{adsConfig.news.title}</Text>
    </View>
    <Text size='xtiny' color={GREY_WARM}>{adsConfig.news.subtitle}</Text>
    <View style={{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'flex-end',
      paddingVertical: WP2, marginTop: WP2
    }}
    >
      <TouchableOpacity
        onPress={() => navigateTo('SoundfrenBlogScreen')}
        style={{
          paddingRight: WP6
        }}
      >
        <LinearGradient
          colors={[DEEP_ROSE, ROYAL_PURPLE]}
          start={[0, 0]} end={[0, 1]}
          style={[{
            width: WP44 - WP3,
            height: WP44 - WP3,
            alignItems: 'center',
          }, styles.box]}
        >
          <Image
            centered
            source={require('../../assets/images/soundfrenNewsWhite.png')}
            imageStyle={{ height: undefined, width: WP24, aspectRatio: 1 }}
          />
          <Text size='tiny' weight={500} type='NeoSans' color={WHITE} style={{ marginTop: WP105 }}>Soundfren Blog</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          NavigateToInternalBrowser({
            url: 'https://news.google.com/topics/CAAqJggKIiBDQkFTRWdvSUwyMHZNREpxYW5RU0FtbGtHZ0pKUkNnQVAB/sections/CAQiRkNCQVNMZ29JTDIwdk1ESnFhblFTQW1sa0dnSkpSQ0lPQ0FRYUNnb0lMMjB2TURSeWJHWXFDaElJTDIwdk1EUnliR1lvQUEqKggAKiYICiIgQ0JBU0Vnb0lMMjB2TURKcWFuUVNBbWxrR2dKSlJDZ0FQAVAB?hl=id&gl=ID&ceid=ID%3Aid',
            title: 'Google News',
            handleBack: true
          })
        }}
      >
        <LinearGradient
          colors={[ROYAL_BLUE, DARK_INDIGO]}
          start={[0, 0]} end={[0, 1]}
          style={[{
            width: WP44 - WP3,
            height: WP44 - WP3,
            alignItems: 'center',
          }, styles.box]}
        >
          <Image
            centered
            source={require('../../assets/images/googleNewsWhite.png')}
            imageStyle={{ height: undefined, width: WP24, aspectRatio: 1 }}
          />
          <Text size='tiny' weight={500} type='NeoSans' color={WHITE} style={{ marginTop: WP105 }}>Google News</Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  </View>
)

ExploreNews.propTypes = {}

ExploreNews.defaultProps = {}

export default ExploreNews
