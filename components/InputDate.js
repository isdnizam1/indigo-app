import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { TextInput, View } from 'react-native'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { BLACK, GREY, GREY_DISABLE, GREY_PLACEHOLDER, GREY_WARM, SILVER, TOMATO } from '../constants/Colors'
import { FONT_SIZE, HP1, HP105, WP1, WP105, WP2, WP305 } from '../constants/Sizes'
import { BORDER_COLOR, BORDER_WIDTH } from '../constants/Styles'
import { FONTS } from '../constants/Fonts'
import Text from './Text'
import Icon from './Icon'
import { propsDefault as propsDefaultInputTextLight, propsType as propsTypeInputTextLight } from './InputTextLight'
import RequiredMark from './RequiredMark'

const propsType = {
  ...propsTypeInputTextLight,
  dateFormat: PropTypes.string,
  mode: PropTypes.oneOf(['date', 'time', 'datetime']),
  maximumDate: PropTypes.string,
  minimumDate: PropTypes.string,
  required: PropTypes.bool
}

const propsDefault = {
  ...propsDefaultInputTextLight,
  dateFormat: 'DD MMMM YYYY',
  mode: 'date',
  required: false
}

class InputDate extends React.Component {
  state = {
    isDateTimePickerVisible: false,
    value: this.props.value,
    showValue: this.props.secureTextEntry
  }

  _onEndEditing = () => {
    this.setState({
      isDateTimePickerVisible: false
    })
  }

  _onFocus = () => {
    this.refs['date_input'].blur()
    this.setState({
      isDateTimePickerVisible: true
    })
  }

  _onBlur = () => {
    this.setState({
      isDateTimePickerVisible: false
    })
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  _handleDatePicked = (date) => {
    const { onChangeText, dateFormat } = this.props
    const newDate = moment(date).local('id').format(dateFormat)
    this.setState({ value: newDate }, () => {
      onChangeText(newDate)
      this._hideDateTimePicker()
    })
  }

  render() {
    const {
      color,
      disabled,
      size,
      type,
      weight,
      backgroundColor,
      bold,
      multiline,
      icon,
      label,
      placeholder,
      placeholderTextColor,
      returnKeyType,
      bordered,
      valueColor,
      toggleSecureTextEntry,
      required,
      value,
      editable,
      style,
      textInputStyle,
      keyboardType,
      labelWeight,
      labelColor,
      maxLength,
      wording,
      textIcon,
      textAlignVertical,
      onRemoveAble,
      dateFormat,
      mode,
      minimumDate,
      maximumDate
    } = this.props
    const {
      showValue,
      isDateTimePickerVisible,
    } = this.state
    const valueLength = value.length
    return (
      <View
        style={[
          {
            marginTop: HP105,
            marginBottom: HP1,
            flexGrow: 1,
          },
          style
        ]}
      >
        {
          !!label && (
            <View style={{ flexDirection: 'row' }}>
              <Text size={'mini'} color={labelColor || BLACK} weight={bold ? 500 : labelWeight}>{label}</Text>
              {
                required && <RequiredMark/>
              }
            </View>
          )
        }
        <View
          style={
            [
              {
                backgroundColor,
                padding: bordered ? WP2 : WP1,
                paddingHorizontal: bordered ? WP2 : 0,
                borderRadius: bordered ? 10 : 0,
                marginBottom: 0,
                borderWidth: bordered ? BORDER_WIDTH : 0,
                borderBottomWidth: !bordered ? BORDER_WIDTH : undefined,
                borderColor: color ? color : BORDER_COLOR,
                flexDirection: 'row',
                alignItems: 'center'
              },
              textInputStyle
            ]
          }
        >
          {
            !!icon && (
              <Icon
                style={{ marginRight: WP305 }}
                centered
                name={icon} size='large'
              />
            )
          }
          {
            !!textIcon && (
              <View style={{ flexDirection: 'row', padding: WP1 }}>
                <Text>{textIcon}</Text>
              </View>
            )
          }
          <TextInput
            editable={editable}
            returnKeyType={returnKeyType}
            value={value}
            style={{
              flex: 1,
              fontSize: FONT_SIZE[size],
              fontFamily: FONTS[type][valueLength > 0 ? weight : 200],
              color: disabled ? GREY_WARM : color || valueColor,
              padding: 0,
              marginLeft :10,
              paddingTop: 0,
              margin: 0,
              borderWidth: 0,
            }}
            underlineColorAndroid='transparent'
            onFocus={this._onFocus}
            secureTextEntry={showValue}
            placeholder={placeholder}
            placeholderTextColor={placeholderTextColor ? placeholderTextColor : SILVER}
            multiline={multiline}
            textAlignVertical={textAlignVertical}
            keyboardType={keyboardType}
            ref='date_input'
          />
          <DateTimePicker
            mode={mode}
            minimumDate={minimumDate ? moment(minimumDate, dateFormat).toDate() : undefined}
            maximumDate={maximumDate ? moment(maximumDate, dateFormat).toDate() : undefined}
            isVisible={isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}
          />
          {
            toggleSecureTextEntry && (
              <Icon
                style={{ marginLeft: WP305 }}
                centered
                color={color ? color : GREY}
                onPress={() => this.setState((state) => ({ showValue: !state.showValue }))}
                name={showValue ? 'eye' : 'eye-with-line'} type='Entypo' size='large'
              />
            )
          }
          {
            onRemoveAble && (
              <Icon
                style={{ marginLeft: WP305 }}
                centered
                size='xmini'
                color={GREY_PLACEHOLDER}
                onPress={onRemoveAble}
                name='close-circle' type='MaterialCommunityIcons'
              />
            )
          }
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          {
            !!wording && (
              <View style={{ flexDirection: 'row' }}>
                <Text size='xtiny' color={GREY_DISABLE}>{wording}</Text>
                { required && <RequiredMark size={WP105}/> }
              </View>
            )
          }
          {
            !!maxLength && (
              <Text
                size='xtiny'
                weight={valueLength === maxLength ? 500 : 300}
                color={valueLength === maxLength ? TOMATO : SILVER}
              >
                {valueLength}/{maxLength}
              </Text>
            )
          }
        </View>
      </View>
    )
  }
}

InputDate.propTypes = propsType
InputDate.defaultProps = propsDefault
export default InputDate
