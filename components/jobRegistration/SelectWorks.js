import React from 'react'
import { noop, includes, reduce } from 'lodash-es'
import { connect } from 'react-redux'
import { TouchableOpacity, View } from 'react-native'
import { map, isEmpty, pull } from 'lodash-es'
import { DEFAULT_PAGING } from '../../constants/Routes'
import {
  GUN_METAL,
  NAVY_DARK,
  PALE_GREY,
  PALE_LIGHT_BLUE_TWO,
  PALE_SALMON,
  PALE_WHITE,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
} from '../../constants/Colors'
import { getProfileSongVideo } from '../../actions/api'
import { HP1, HP2, WP1, WP10, WP100, WP2, WP4, WP5, WP80 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { jobDispatcher } from '../../services/job'
import { _enhancedNavigation, Container, Icon, Text, Button, Image } from '..'
import HeaderNormal from '../HeaderNormal'
import EmptyV3 from '../EmptyV3'
import { getYoutbeId } from '../../utils/helper'
import HorizontalLine from '../HorizontalLine'

const mapStateToProps = ({ auth, job }) => ({
  userData: auth.user,
  jobRegistrationForm: job.jobRegistrationForm,
})

const mapFromNavigationParam = (getParam) => ({
  initialLoaded: getParam('initialLoaded', false),
  onChange: getParam('onChange', noop),
})

const mapDispatchToProps = {
  jobRegistrationSave: jobDispatcher.jobRegistrationSave,
}

const IMAGE_TYPES = {
  song: require('../../assets/icons/songCircle.png'),
  video: require('../../assets/icons/videoRegistration.png'),
}

class SelectWorks extends React.Component {
  state = {
    selectedWorks: reduce(
      this.props.jobRegistrationForm.works,
      (result, work) => {
        result.push(work.id_journey)
        return result
      },
      [],
    ),
    userWorks: [],
    isReady: false,
    isRefreshing: false,
  };

  async componentDidMount() {
    await this._getInitialScreenData()
  }

  _getInitialScreenData = async (...args) => {
    await this._getData(...args)
    this.setState({
      isReady: true,
      isRefreshing: false,
    })
  };

  _onSubmit = () => {
    const { selectedWorks, userWorks } = this.state
    const works = reduce(
      userWorks,
      (result, work) => {
        const isActive = includes(selectedWorks, work.id_journey)
        if (isActive) result.push(work)
        return result
      },
      [],
    )
    this.props.onChange(works)
    this.props.navigateBack()
  };

  _getData = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const { userData, dispatch } = this.props
    const dataResponse = await dispatch(
      getProfileSongVideo,
      {
        ...params,
        id_user: userData.id_user,
      },
      noop,
      true,
      isLoading,
    )
    await this.setState({ userWorks: dataResponse.result })
  };

  _getImageSong = (work, additionalData) => {
    if (additionalData.cover_image) return additionalData.cover_image
    if (additionalData.icon) return additionalData.icon
    if (additionalData.url_video)
      return `https://img.youtube.com/vi/${getYoutbeId(additionalData.url_video)}/hqdefault.jpg`
    return IMAGE_TYPES[work.type]
  };

  _renderWorks = ({ work, index, selectedWorks }) => {
    const isActive = includes(selectedWorks, work.id_journey)
    const additionalData = JSON.parse(work.additional_data)
    return (
      <>
        <TouchableOpacity
          key={index}
          activeOpacity={TOUCH_OPACITY}
          onPress={() => {
            const newSelectedWorks = selectedWorks
            if (isActive) pull(newSelectedWorks, work.id_journey)
            else newSelectedWorks.push(work.id_journey)
            this.setState({
              selectedWorks: newSelectedWorks,
            })
          }}
          style={{ paddingVertical: HP1, flexDirection: 'row', alignItems: 'center' }}
        >
          {isActive ? (
            <Icon
              centered
              size='small'
              color={REDDISH}
              name='checkbox-marked'
              type='MaterialCommunityIcons'
            />
          ) : (
            <Icon
              centered
              size='small'
              color={PALE_LIGHT_BLUE_TWO}
              name='checkbox-blank-outline'
              type='MaterialCommunityIcons'
            />
          )}
          <Image
            size='small'
            style={{ marginHorizontal: WP4 }}
            imageStyle={{ borderRadius: WP2 }}
            source={{ uri: this._getImageSong(work, additionalData) }}
          />
          <View style={{ flex: 1 }}>
            <Text type='Circular' size='small' color={GUN_METAL} weight={500} style={{ flex: 1 }}>
              {work.title}
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <View>
                <Icon color={SHIP_GREY_CALM} size={'tiny'} name={'play'} />
              </View>
              <Text
                numberOfLines={1}
                style={{ marginLeft: WP1 }}
                type={'Circular'}
                weight={300}
                color={SHIP_GREY_CALM}
                size={'xmini'}
              >
                {work.total_view ?? 0}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
        <HorizontalLine
          width={WP80}
          style={{ marginVertical: HP1, alignSelf: 'center' }}
          color={PALE_GREY}
        />
      </>
    )
  };

  render() {
    const { navigateTo, navigateBack, isLoading } = this.props
    const { isReady, userWorks, selectedWorks } = this.state

    return (
      <Container
        scrollable
        scrollBackgroundColor={PALE_WHITE}
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            textType='Circular'
            textColor={SHIP_GREY}
            textWeight={400}
            text={'Import lagu'}
            centered
          />
        )}
        outsideScrollContent={() =>
          !isEmpty(userWorks) && (
            <View
              style={{
                backgroundColor: WHITE,
                width: WP100,
                alignItems: 'center',
                paddingHorizontal: WP4,
                elevation: 20,
              }}
            >
              <Button
                onPress={this._onSubmit}
                width='100%'
                centered
                shadow='none'
                text='Import Lagu'
                colors={[REDDISH, REDDISH]}
                compact='center'
                radius={5}
                textStyle={{ marginVertical: 10, fontWeight: '500', textTransform: 'none' }}
                disable={selectedWorks.length > 0 == false}
                disableColor={PALE_SALMON}
                toggle
                autoFocus
                toggleActive={false}
                toggleInactiveTextColor={WHITE}
                toggleInactiveColor={selectedWorks.length > 0 ? REDDISH : PALE_SALMON}
                textSize='small'
                textType='Circular'
              />
            </View>
          )
        }
      >
        {isEmpty(userWorks) ? (
          <EmptyV3
            title='Belum ada lagu di profilemu'
            message='Silahkan upload file lagu'
            icon={
              <Icon
                centered
                style={{ borderRadius: WP100, marginBottom: HP1 }}
                padding={WP5}
                type='Ionicons'
                name='ios-musical-note'
                size={WP10}
                color={WHITE}
                background='pale-light-circle'
                backgroundColor={PALE_LIGHT_BLUE_TWO}
              />
            }
            actions={
              <Button
                onPress={() => {
                  navigateTo('SongFileForm', { refreshProfile: noop }, 'replace')
                }}
                radius={WP2}
                centered
                backgroundColor={REDDISH}
                text='Upload lagu'
                textWeight={500}
                textSize='small'
                textColor={WHITE}
                shadow='none'
              />
            }
          />
        ) : (
          <View style={{ padding: WP5 }}>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: HP2 }}
            >
              <Text size={'medium'} color={NAVY_DARK} type={'Circular'} weight={600}>
                Semua Lagu
              </Text>
              <Text
                size={'mini'}
                color={REDDISH}
                type={'Circular'}
                weight={400}
                onPress={() => {
                  this.setState({ selectedWorks: userWorks.map((item) => item.id_journey) })
                }}
              >
                Pilih Semua
              </Text>
            </View>
            {map(userWorks, (work, index) => {
              return this._renderWorks({ work, index, selectedWorks })
            })}
            <View style={{ marginTop: HP2 }} />
          </View>
        )}
      </Container>
    )
  }
}

SelectWorks.defaultProps = {
  userData: {},
  jobRegistrationForm: {
    works: [],
  },
  onRefresh: noop,
}
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SelectWorks),
  mapFromNavigationParam,
)
