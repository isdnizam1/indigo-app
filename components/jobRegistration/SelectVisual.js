import React from 'react'
import { noop, includes, reduce } from 'lodash-es'
import { connect } from 'react-redux'
import { TouchableOpacity, View } from 'react-native'
import { map, isEmpty, pull } from 'lodash-es'
import { DEFAULT_PAGING } from '../../constants/Routes'
import { WHITE, GREY_CHAT, SHIP_GREY, PALE_LIGHT_BLUE_TWO, REDDISH, NAVY_DARK, PALE_WHITE, GUN_METAL, SHIP_GREY_CALM, PALE_GREY, PALE_SALMON } from '../../constants/Colors'
import { getProfileArts } from '../../actions/api'
import { WP100, WP4, WP6, WP3, HP1, WP5, WP10, WP2, HP2, WP80 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { jobDispatcher } from '../../services/job'
import { _enhancedNavigation, Container, HeaderNormal, Text, Button, Image } from '..'
import EmptyV3 from '../EmptyV3'
import Icon from '../Icon'
import HorizontalLine from '../HorizontalLine'

const mapStateToProps = ({ auth, job }) => ({
  userData: auth.user,
  jobRegistrationForm: job.jobRegistrationForm
})

const mapFromNavigationParam = (getParam) => ({
  onChange: getParam('onChange', noop),
  initialLoaded: getParam('initialLoaded', false)
})

const mapDispatchToProps = {
  jobRegistrationSave: jobDispatcher.jobRegistrationSave
}

const MAX_VISUAL = 1

class SelectVisual extends React.Component {
  state = {
    selectedWorks: reduce(this.props.jobRegistrationForm.works, (result, work) => {
      result.push(work.id_file_attachment)
      return result
    }, []),
    userWorks: [],
    isReady: false,
    isRefreshing: false
  }

  async componentDidMount() {
    await this._getInitialScreenData()
  }

  _getInitialScreenData = async (...args) => {
    await this._getData(...args)
    this.setState({
      isReady: true,
      isRefreshing: false
    })
  }

  _getData = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const {
      userData,
      dispatch
    } = this.props
    const dataResponse = await dispatch(getProfileArts, {
      ...params,
      id_user: userData.id_user
    }, noop, true, isLoading)
    await this.setState({ userWorks: dataResponse.result })
  }

  _onSubmit = () => {
    const { selectedWorks, userWorks } = this.state
    const works = reduce(userWorks, (result, work) => {
      const isActive = includes(selectedWorks, work.id_file_attachment)
      if (isActive) result.push(work)
      return result
    }, [])
    this.props.onChange(works)
    this.props.navigateBack()
  }

  _selectItem = (item) => {
    const { selectedWorks } = this.state
    const isActive = includes(selectedWorks, item.id_file_attachment)
    const newSelectedWorks = selectedWorks
    if (isActive) pull(newSelectedWorks, item.id_file_attachment)
    else {
      if (selectedWorks.length < MAX_VISUAL) newSelectedWorks.push(item.id_file_attachment)
    }
    this.setState({
      selectedWorks: newSelectedWorks
    })
  }

  _visualItem = (work, index) => {
    const { selectedWorks } = this.state
    const isActive = includes(selectedWorks, work.id_file_attachment)
    return (
      <>
      <TouchableOpacity
        key={index}
        activeOpacity={TOUCH_OPACITY}
        onPress={() => {
          const newSelectedWorks = selectedWorks
          if (isActive) pull(newSelectedWorks, work.id_file_attachment)
          else newSelectedWorks.push(work.id_file_attachment)
          this.setState({
            selectedWorks: newSelectedWorks
          })
        }}
        style={{ paddingVertical: HP1, flexDirection: 'row', alignItems: 'center' }}
      >
        {
          isActive
            ? <Icon centered size='small' color={REDDISH} name='checkbox-marked' type='MaterialCommunityIcons'/>
            : <Icon centered size='small' color={PALE_LIGHT_BLUE_TWO} name='checkbox-blank-outline' type='MaterialCommunityIcons'/>
        }
        <Image size='small' style={{ marginHorizontal: WP4 }} imageStyle={{ borderRadius: WP2 }} source={{ uri: work.attachment_file }}/>
        <View style={{ flex: 1 }}>
          <Text type='Circular' size='small' color={GUN_METAL} weight={500}>{work.title}</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text
              numberOfLines={1}
              type={'Circular'}
              weight={300}
              color={SHIP_GREY_CALM}
              size={'xmini'}
            >Photo
            </Text>
          </View>
        </View>
      </TouchableOpacity>
      <HorizontalLine width={WP80} style={{ marginVertical: HP1, alignSelf: 'center' }} color={PALE_GREY} />
      </>
    )
  }

  render() {
    const {
      navigateTo,
      navigateBack,
      isLoading
    } = this.props
    const {
      isReady,
      userWorks,
      selectedWorks
    } = this.state
    return (
      <Container
        scrollable
        scrollBackgroundColor={PALE_WHITE}
        isLoading={isLoading}
        isReady={isReady}
        renderHeader={() => (
          <HeaderNormal
            iconLeftOnPress={navigateBack}
            textType='Circular'
            textColor={SHIP_GREY}
            textWeight={400}
            text={'Import artwork'}
            centered
          />
        )}
        outsideScrollContent={() => (
          !isEmpty(userWorks) && <View style={{
            backgroundColor: WHITE,
            width: WP100,
            alignItems: 'center',
            paddingHorizontal: WP6,
            paddingBottom: WP6,
            elevation: 20
          }}
                                 >
            <Button
              onPress={this._onSubmit}
              width='100%'
              centered
              shadow='none'
              text='Import Artwork'
              colors={[REDDISH, REDDISH]}
              compact='center'
              radius={5}
              textStyle={{ marginVertical: 10, fontWeight: '500', textTransform: 'none' }}
              disable={selectedWorks.length > 0 == false}
              disableColor={PALE_SALMON}
              toggle
              autoFocus
              toggleActive={false}
              toggleInactiveTextColor={WHITE}
              toggleInactiveColor={selectedWorks.length > 0 ? REDDISH : PALE_SALMON}
              textSize='small'
              textType='Circular'
            />
          </View>
        )}
      >
        {
          isEmpty(userWorks) ? (
            <EmptyV3
              title='Belum ada artwork di profilemu'
              message='Silahkan upload file artwork'
              icon={(
                <Icon
                  centered
                  style={{ borderRadius: WP100, marginBottom: HP1 }}
                  padding={WP5}
                  type='MaterialIcons'
                  name='insert-photo'
                  size={WP10}
                  color={WHITE}
                  background='pale-light-circle'
                  backgroundColor={PALE_LIGHT_BLUE_TWO}
                />
              )}
              actions={(
                <Button
                  onPress={
                    () => {
                      navigateTo('ProfileAddArtScreen', { refreshProfile: noop }, 'replace')
                    }
                  }
                  radius={WP2}
                  centered
                  backgroundColor={REDDISH}
                  text='Upload artwork'
                  textWeight={500}
                  textSize='small'
                  textColor={WHITE}
                  shadow='none'
                />
              )}
            />
          ) : (
            <View
              style={{ paddingHorizontal: WP5, paddingTop: HP2 }}
            >
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: HP2 }} >
                <Text
                  size={'medium'}
                  color={NAVY_DARK}
                  type={'Circular'}
                  weight={600}
                >
                  Semua Artwork
                </Text>
                <Text
                  size={'mini'}
                  color={REDDISH}
                  type={'Circular'}
                  weight={400}
                  onPress={() => {
                    this.setState({ selectedWorks: userWorks.map((item) => item.id_file_attachment) })
                  }}
                >
                  Pilih Semua
                </Text>
              </View>
              <View style={{
                width: '100%',
                borderTopWidth: 1,
                borderTopColor: GREY_CHAT,
                marginTop: WP3
              }}
              >
                {
                  map(userWorks, this._visualItem)
                }
              </View>
            </View>
          )
        }
      </Container>
    )
  }
}

SelectVisual.defaultProps = {
  userData: {},
  jobRegistrationForm: {
    works: []
  },
  onRefresh: noop
}
export default _enhancedNavigation(
  connect(mapStateToProps, mapDispatchToProps)(SelectVisual),
  mapFromNavigationParam
)
