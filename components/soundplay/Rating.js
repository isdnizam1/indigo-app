import React from 'react'
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native'
import Text from '../Text'
import { GREY_WARM_1 } from '../../constants/Colors'
import { WP75, WP6 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'

const starSelected = require('../../assets/icons/spRatingSelected.png')
const starUnselected = require('../../assets/icons/spRatingUnselected.png')

class Rating extends React.PureComponent {

  render() {
    const { rating, onRate } = this.props
    return (<View style={style.wrapper}>
      {(Array.from(Array(5), (_, i) => i + 1)).map((n) => (
        <TouchableOpacity activeOpacity={TOUCH_OPACITY} onPress={() => onRate(n)} style={style.ratingAction} key={n}>
          {/*<Icon style={style.star} type={'MaterialCommunityIcons'} size={34} color={GREY_CALMER} name={'star'} />*/}
          <Image style={style.star} resizeMode={'cover'} source={n-1 < rating ? starSelected : starUnselected} />
        </TouchableOpacity>
      ))}
      <Text centered color={GREY_WARM_1} type={'Circular'} style={style.ratingText}>
        {
          [
            '',
            'Tidak baik',
            'Kurang baik',
            'Cukup',
            'Baik',
            'Sangat baik',
          ][rating]
        }
      </Text>
    </View>)
  }

}

const style = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    width: WP75,
    alignSelf: 'center',
    maxWidth: 328,
    paddingBottom: 50
  },
  ratingAction: {
    flexGrow: 1,
    paddingVertical: WP6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  star: {
    width: 40,
    height: 40
  },
  ratingText: {
    width: WP75,
    position: 'absolute',
    left: 0,
    bottom: 25
  }
})

export default Rating