import React, { useEffect, useRef, useState } from 'react'
import {
  Animated,
  Image as RNImage,
  Modal,
  Slider,
  TouchableOpacity,
  View,
  TextInput,
  Keyboard,
  Dimensions,
} from 'react-native'
import { LinearGradient } from 'expo-linear-gradient'
import { get, pick, isEqual, isNil, noop } from 'lodash-es'
import * as Notifications from 'expo-notifications'
import Button from '../Button'
import Text from '../Text'
// import Image from '../Image'
import Icon from '../Icon'
import { postFeedbackAlbum } from '../../actions/api'
// import { Button, Image as ImageComponent, Text } from '../../components'
import style from '../../styles/soundplay/SoundplayPlayer'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { HP1, HP2, WP2, HP4, WP100, WP5, WP90, WP7 } from '../../constants/Sizes'
import { getTime, isIOS } from '../../utils/helper'
import { localNotification } from '../../utils/soundplay'
import {
  GREY80,
  GREY_CHAT,
  TOMATO,
  WHITE,
  SHIP_GREY_CALM,
  GREY_DARKER,
  GREY_CALMER,
  REDDISH,
  TRANSPARENT,
  PALE_WHITE,
} from '../../constants/Colors'
import { speakerDisplayName } from '../../utils/transformation'
import Rating from './Rating'

const SoundplayPlayer = (props) => {
  let premiumFadeAnim = useRef(new Animated.Value(0)).current
  let premiumSlideAnim = useRef(new Animated.Value(-1500)).current
  let premiumVisibleAnim = useRef(new Animated.Value(-1500)).current
  let [showPremiumPopup, togglePremiumPopup] = useState(false)

  let ratingFadeAnim = useRef(new Animated.Value(0)).current
  let ratingSlideAnim = useRef(new Animated.Value(-1500)).current
  let ratingVisibleAnim = useRef(new Animated.Value(-1500)).current
  let ratingHeightAnim = useRef(new Animated.Value(WP100 * 1.4)).current
  let [showRatingPopup, toggleRatingPopup] = useState(false)
  let [timerWidth, setTimerWidth] = useState(36)

  let { sp, togglePlayer, visible, soundObject, soundIndex, onUpgrade, isPremium, playByIndex, idUser, setIsPlaybackPlaying, showMiniPlayer, actionPodcastPlaying } = props
  let {
    image,
    speaker,
    additional_data: { episodeList },
  } = sp
  let [episodeIndex, setIndex] = useState(null)
  let [hasRated, setHasRated] = useState(false)
  let [id_episode, setIdEpisode] = useState(null)
  let [ratingValue, setRatingValue] = useState(0)
  let [ratingText, setRatingText] = useState(null)
  let [playback, setPlayback] = useState({
    isLoaded: false,
    positionMillis: 0,
    durationMillis: 0,
  })
  const [isKeyboardVisible, setKeyboardVisible] = useState(false)

  const id = get(props, 'sp.id_ads')
  const feedback = get(props, 'sp.feedback')
  let maximumValue = 500

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardVisible(true) // or some other action
    })
    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardVisible(false) // or some other action
    })
    return () => {
      keyboardDidHideListener.remove()
      keyboardDidShowListener.remove()
    }
  }, [])

  useEffect(() => {
    Animated.timing(ratingHeightAnim, {
      toValue: isKeyboardVisible ? WP100 * 1.575 : WP100 * 1.4,
      duration: 250,
      useNativeDriver: false,
    }).start()
  }, [isKeyboardVisible])

  useEffect(() => {
    setTimeout(
      () => {
        Animated.timing(premiumFadeAnim, {
          toValue: showPremiumPopup ? 1 : 0,
          duration: showPremiumPopup ? 250 : 400,
          useNativeDriver: true,
        }).start()
      },
      showPremiumPopup ? 50 : 500,
    )
    setTimeout(
      () => {
        Animated.timing(premiumSlideAnim, {
          toValue: showPremiumPopup ? 0 : -1500,
          duration: showPremiumPopup ? 550 : 450,
          useNativeDriver: false,
        }).start()
      },
      showPremiumPopup ? 260 : 50,
    )
    setTimeout(
      () => {
        Animated.timing(premiumVisibleAnim, {
          toValue: showPremiumPopup ? 0 : -1500,
          duration: 0,
          useNativeDriver: false,
        }).start()
      },
      showPremiumPopup ? 0 : 850,
    )
  }, [showPremiumPopup])

  useEffect(() => {
    setTimeout(
      () => {
        Animated.timing(ratingFadeAnim, {
          toValue: showRatingPopup ? 1 : 0,
          duration: showRatingPopup ? 250 : 400,
          useNativeDriver: true,
        }).start()
      },
      showRatingPopup ? 50 : 500,
    )
    setTimeout(
      () => {
        Animated.timing(ratingSlideAnim, {
          toValue: showRatingPopup ? 0 : -1500,
          duration: showRatingPopup ? 550 : 450,
          useNativeDriver: false,
        }).start()
      },
      showRatingPopup ? 260 : 50,
    )
    setTimeout(
      () => {
        Animated.timing(ratingVisibleAnim, {
          toValue: showRatingPopup ? 0 : -1500,
          duration: 0,
          useNativeDriver: false,
        }).start()
      },
      showRatingPopup ? 0 : 850,
    )
  }, [showRatingPopup])

  const renderSound = async () => {
    try {
      if (soundIndex !== episodeIndex && episodeIndex != null) await soundObject.unloadAsync()
    } catch (error) {
      // Silent is gold
    } finally {
      if (soundIndex !== null) {
        setIndex(soundIndex)
        try {
          soundObject.loadAsync({ uri: episodeList[soundIndex].link }).then(() => {
            setIdEpisode(episodeList[soundIndex].id_episode)
            soundIndex != null &&
              episodeList[soundIndex] &&
              togglePremiumPopup(episodeList[soundIndex].is_premium && !isPremium)
            soundObject.playAsync()
            setIsPlaybackPlaying(true)
          })
        } catch (error) {
          // Silent is gold
        }
      }
    }
  }

  const onPlaybackUpdate = (pb) => {
    const episode = episodeList[soundIndex || 0]
    if (pb.isLoaded) {
      try {
        const pm1 = parseInt(pb.positionMillis),
          pm2 = parseInt(playback.positionMillis)
        // improve performance by prevent unecessary re-render
        if ((pm1 != pm2 && Math.abs(pm1 - pm2) > 975) || pb.didJustFinish || pb.isPlaying != playback.isPlaying) {
          setPlayback(pb)
        }
        if (pb.isPlaying && soundIndex != null && episodeList[soundIndex].is_premium == 1 && !isPremium) {
          pause()
          togglePopups()
        }
        if (pb.didJustFinish) {
          // console.log(pb.durationMillis, pb.positionMillis)
          if (pb.positionMillis === pb.durationMillis && pb.positionMillis > 0 && pb.durationMillis > 0)
            actionPodcastPlaying(episode.id_episode, 'played')
          // console.log(episode.id_episode)
          next()
        }
      } catch (error) {
        // Silent is gold
      }
    }
  }

  soundObject.setOnPlaybackStatusUpdate(onPlaybackUpdate)

  useEffect(() => {
    if (soundIndex != null) {
      localNotification(sp, soundIndex, soundIndex == 0 ? 'spCategory1' : 'spCategory2')
    }
    renderSound()
  }, [soundIndex])

  const closePlayer = async () => {
    await togglePremiumPopup(false)
    if (playback.isLoaded) {
      await soundObject.pauseAsync()
      setIsPlaybackPlaying(false)
    }
    togglePlayer(false)
    Notifications.dismissAllNotificationsAsync()
  }

  const play = () => {
    if (playback.isLoaded) {
      !showPremiumPopup && soundObject.playAsync()
      !showPremiumPopup && localNotification(sp, soundIndex, soundIndex == 0 ? 'spCategory1' : 'spCategory2')
    } else setTimeout(play, 1000)
    setIsPlaybackPlaying(true)
  }

  const pause = () => {
    soundObject.pauseAsync()
    localNotification(sp, soundIndex, 'spCategory3')
    setIsPlaybackPlaying(false)
    return true
  }

  const next = async () => {
    let nextIndex = episodeIndex + 1
    if (episodeList.length - nextIndex == 0) nextIndex = 0
    if (isPremium || !episodeList[nextIndex].is_premium) {
      if (isPremium && nextIndex == 0 && isNil(feedback) && !hasRated) toggleRatingPopup(true)
      else playByIndex(nextIndex)
      togglePremiumPopup(false)
    } else {
      togglePopups()
    }
  }

  const prev = async () => {
    let prevIndex = episodeIndex - 1
    if (prevIndex < 0) prevIndex = episodeList.length - 1
    if (isPremium || !episodeList[prevIndex].is_premium) {
      playByIndex(prevIndex)
      togglePremiumPopup(false)
    } else {
      togglePopups()
    }
  }

  const togglePopups = () => {
    isNil(feedback) && !hasRated ? toggleRatingPopup(true) : togglePremiumPopup(true)
  }

  useEffect(() => {
    showPremiumPopup && Notifications.dismissAllNotificationsAsync()
    !showPremiumPopup &&
      playback.isPlaying &&
      Notifications.dismissAllNotificationsAsync() &&
      setTimeout(() => {
        localNotification(sp, soundIndex, soundIndex == 0 ? 'spCategory1' : 'spCategory2')
      }, 750)
  }, [showPremiumPopup])

  const onUpdatingTrack = async (value) => {
    if (playback.durationMillis && playback.isLoaded) {
      try {
        await soundObject.pauseAsync()
        setIsPlaybackPlaying(false)
      } catch (error) {
        // Silent is gold
      } finally {
        try {
          await soundObject.setPositionAsync((value / maximumValue) * playback.durationMillis)
        } catch (error) {
          // Silent is gold
        } finally {
          soundObject.playAsync()
          setIsPlaybackPlaying(true)
        }
      }
    }
  }

  const forward = (milliseconds) => {
    soundObject.setPositionAsync(playback.positionMillis + milliseconds)
  }

  const onRequestUpgrade = () => {
    Promise.all([togglePremiumPopup(false), pause(), onUpgrade()])
  }

  const onRate = (value) => {
    setRatingValue(value)
  }

  const ratingable = () => {
    return ratingValue > 0
  }

  const submitRating = () => {
    if (ratingable()) {
      toggleRatingPopup(false)
      postFeedbackAlbum({
        id_user: idUser,
        id_ads: id,
        stars: ratingValue,
        description: ratingText,
      }).then(() => {
        setHasRated(true)
        !isPremium && togglePremiumPopup(true)
      })
    }
  }

  const _renderMiniPlayer = () => {
    const title = episodeList[soundIndex || 0].title
    return (
      <LinearGradient
        style={{
          justifyContent: 'space-between',
          width: '100%',
          alignItems: 'flex-start',
          alignSelf: 'center',
          paddingVertical: HP2,
          paddingHorizontal: WP5,
          flexDirection: 'row'
        }}
        colors={['#212B36', '#1a222b']}
        start={[0, 0]}
        end={[0, 1.4]}
      >
        <TouchableOpacity activeOpacity={TOUCH_OPACITY} onPress={() => togglePlayer(true)}>
          <View style={{ flex: 1 }}>
            <Text color={SHIP_GREY_CALM} type={'Circular'} size={'xmini'}>
              Now Playing
            </Text>
            <Text
              numberOfLines={1}
              color={PALE_WHITE}
              type={'Circular'}
              weight={500}
              size={'mini'}
              style={{
                marginBottom: 3,
                paddingRight: 10,
              }}
            >
              {title.trim()}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          onPress={playback.isPlaying ? pause : play}
        >
          <View style={{ alignSelf: 'center', width: WP7, height: WP7, borderRadius: WP7 * 2, backgroundColor: REDDISH, justifyContent: 'center' }}>
            <Icon
              color={WHITE}
              size='mini'
              name={playback.isPlaying ? 'pause' : 'play'}
              type='Ionicons'
              style={{ alignSelf: 'center' }}
            />
          </View>
        </TouchableOpacity>

      </LinearGradient>
    )
  }

  return (
    <>
      {showMiniPlayer && _renderMiniPlayer()}
      <Modal animationType={'slide'} onRequestClose={() => togglePlayer(false)} transparent={true} onShow={play} visible={visible}>
        <LinearGradient colors={['#3b659c', '#1b252f', '#1a222b']} style={style.wrapper}>
          <View style={style.actionBar}>
            <TouchableOpacity onPress={togglePlayer} style={{ padding: HP2, width: 100 }}>
              <RNImage
                style={{ width: 12, height: 12 }}
                resizeMode={'contain'}
                source={require('../../assets/icons/spChevronDown.png')}
              />
            </TouchableOpacity>
            <Text type={'Circular'} style={{ flex: 1 }} weight={700} size={'medium'} centered color={WHITE}>
              {playback.isPlaying ? 'Now Playing' : 'Podcast'}
            </Text>
            <TouchableOpacity
              onPress={props.isStartup ? noop : () => {
                closePlayer()
                props.navigateTo('ShareScreen', {
                  id,
                  type: 'explore',
                  title: 'Bagikan Album',
                  id_episode,
                })
              }}
              style={{ width: 100, flexDirection: 'row', justifyContent: 'flex-end', padding: HP2 }}
            >
              <Icon type={'MaterialCommunityIcons'} name='share-variant' size={24} color={props.isStartup ? TRANSPARENT : WHITE} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              paddingVertical: HP4,
              alignItems: 'center',
              backgroundColor: '#00000000',
              justifyContent: 'center',
            }}
          >
            <View style={[style.posterWrapper, showPremiumPopup ? { elevation: 0 } : null]}>
              <RNImage resizeMode={'cover'} style={style.poster} source={{ uri: image }} />
            </View>
            {episodeList.length > 0 && (
              <Text type={'Circular'} centered weight={700} size={'small'} color={WHITE}>
                {episodeList[soundIndex || 0].title}
              </Text>
            )}
            <Text type={'Circular'} color={SHIP_GREY_CALM} size={'mini'} weight={400} style={style.spSpeaker}>
              {episodeList[soundIndex || 0].speaker_name}
            </Text>
            <View style={style.durationWrapper}>
              <Text style={{ width: timerWidth, textAlign: 'center' }} type={'Circular'} color={GREY_CHAT} size={'tiny'}>
                {getTime(playback.positionMillis || 0)}
              </Text>
              <Slider
                minimumValue={0}
                maximumValue={maximumValue}
                minimumTrackTintColor={TOMATO}
                maximumTrackTintColor={WHITE}
                color={WHITE}
                onValueChange={onUpdatingTrack}
                step={1}
                value={parseInt(
                  playback.positionMillis && playback.durationMillis
                    ? (playback.positionMillis / playback.durationMillis) * maximumValue
                    : 0,
                )}
                thumbTintColor={TOMATO}
                style={style.slider}
              />
              <Text style={{ width: timerWidth, textAlign: 'center' }} type={'Circular'} color={GREY_CHAT} size={'tiny'}>
                {getTime(playback.durationMillis || 0)}
              </Text>
            </View>
            <View style={style.controlsWrapper}>
              <TouchableOpacity onPress={prev} style={style.controlButton} activeOpacity={TOUCH_OPACITY / 2}>
                <RNImage
                  style={{ ...style.controlIcon, width: 18, height: 18 }}
                  source={require('../../assets/icons/spPrev.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => forward(-10000)}
                style={style.controlButton}
                activeOpacity={TOUCH_OPACITY / 2}
              >
                <RNImage
                  style={{ ...style.controlIcon, width: 34, height: 34 }}
                  source={require('../../assets/icons/spBackward10.png')}
                />
              </TouchableOpacity>
              {playback.isPlaying && (
                <TouchableOpacity onPress={pause} style={style.controlButton} activeOpacity={TOUCH_OPACITY / 2}>
                  <RNImage style={style.controlIcon} source={require('../../assets/icons/spPauseCircle.png')} />
                </TouchableOpacity>
              )}
              {!playback.isPlaying && (
                <TouchableOpacity onPress={play} style={style.controlButton} activeOpacity={TOUCH_OPACITY / 2}>
                  <RNImage style={style.controlIcon} source={require('../../assets/icons/spPlayCircle.png')} />
                </TouchableOpacity>
              )}
              <TouchableOpacity
                onPress={() => forward(10000)}
                style={style.controlButton}
                activeOpacity={TOUCH_OPACITY / 2}
              >
                <RNImage
                  style={{ ...style.controlIcon, width: 34, height: 34 }}
                  source={require('../../assets/icons/spForward10.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={next} style={style.controlButton} activeOpacity={TOUCH_OPACITY / 2}>
                <RNImage
                  style={{ ...style.controlIcon, width: 18, height: 18 }}
                  source={require('../../assets/icons/spNext.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
          <Text centered type={'Circular'} color={SHIP_GREY_CALM} size={'mini'} weight={400} style={style.powered}>
            Powered by Eventeer
        </Text>
          <Animated.View style={{ ...style.premiumPopup, bottom: premiumVisibleAnim }}>
            <Animated.View style={{ ...style.premiumModal, opacity: premiumFadeAnim }} />
            <Animated.View style={{ ...style.premiumWrapper, bottom: premiumSlideAnim }}>
              <View style={{ width: WP100 }}>
                <TouchableOpacity
                  onPress={() => togglePremiumPopup(false)}
                  style={{ paddingBottom: HP1, marginBottom: HP2, paddingHorizontal: WP5 }}
                >
                  <RNImage
                    style={{ width: 20, height: 20 }}
                    source={require('../../assets/icons/close_circle_gray.png')}
                  />
                </TouchableOpacity>
              </View>
              <RNImage style={{ marginBottom: HP1 }} source={require('../../assets/images/notPremiumUser.png')} />
              <Text style={{ marginVertical: HP1 }} weight={700} color={GREY80} size={'huge'}>
                Oops...!
            </Text>
              <Text size={'tiny'}>You have reached your free access.</Text>
              {!isIOS() && (
                <Button
                  width={WP90}
                  style={{ marginTop: HP4 }}
                  radius={6}
                  shadow={'none'}
                  colors={[REDDISH, REDDISH]}
                  onPress={onRequestUpgrade}
                  text={'Upgrade'}
                  textColor={WHITE}
                  textSize={'small'}
                  textCentered={true}
                  textType={'Circular'}
                  textStyle={{ width: '100%', paddingVertical: HP1 }}
                  contentStyle={{ paddingVertical: WP2 + 2 }}
                  textWeight={700}
                />
              )}
            </Animated.View>
          </Animated.View>
          <Animated.View style={{ ...style.ratingPopup, bottom: ratingVisibleAnim }}>
            <Animated.View style={{ ...style.ratingModal, opacity: ratingFadeAnim }}>
              <TouchableOpacity
                onPress={() => toggleRatingPopup(false)}
                style={{ flex: 1, width: Dimensions.get('window').width, height: Dimensions.get('window').height }}
              />
            </Animated.View>
            <Animated.View style={{ ...style.ratingWrapper, bottom: ratingSlideAnim, height: ratingHeightAnim }}>
              <View>
                <Text type={'Circular'} weight={600} color={GREY_DARKER} size={'small'} centered>
                  Bagaimana podcast ini?
              </Text>
                <Rating onRate={onRate} rating={ratingValue} />
                <View style={style.ratingForm}>
                  <View>
                    <Icon color={GREY80} type={'MaterialCommunityIcons'} size={24} name='square-edit-outline' />
                  </View>
                  <TextInput
                    value={ratingText}
                    placeholderTextColor={GREY_CALMER}
                    onChangeText={(newText) => setRatingText(newText)}
                    style={style.ratingInput}
                    placeholder='Bantu kami jadi lebih baik..'
                  />
                </View>
              </View>
              <Button
                width={WP90}
                style={{ marginTop: HP4 }}
                radius={6}
                shadow={'none'}
                colors={
                  ratingable() ? [REDDISH, REDDISH] : ['rgb(242, 242, 242)', 'rgb(242, 242, 242)'] /*[TOMATO, TOMATO]*/
                }
                onPress={submitRating}
                contentStyle={{ paddingVertical: WP2 + 2 }}
                text={'Kirim Ulasan'}
                textColor={ratingable() ? WHITE : 'rgb(208, 212, 217)' /* GREY_DARKER*/}
                textSize={'small'}
                textCentered={true}
                textStyle={{ width: '100%', paddingVertical: HP1, fontFamily: 'CircularBlack' }}
                textWeight={700}
              />
            </Animated.View>
          </Animated.View>
          {/*Invisible element hanya untuk menghitung lebar timer yang sesungguhnya
        karena jenis fontnya ngga monospace. Jika tidak didefinisikan lebar timernya,
        lebar slidernya akan menjadi tidak konsisten.*/}
          <View style={{ flexDirection: 'row', position: 'absolute', opacity: 0, bottom: 0 }}>
            <Text
              type={'Circular'}
              color={GREY_CHAT}
              size={'tiny'}
              onLayout={(nativeEvent) => {
                setTimerWidth(nativeEvent.nativeEvent.layout.width + 2)
              }}
            >
              00:00
          </Text>
          </View>
          {/*End invisible element*/}
        </LinearGradient>
      </Modal>
    </>
  )
}

export default React.memo(SoundplayPlayer, (prevProps, nextProps) => {
  const propFields = ['soundIndex', 'isPremium', 'visible']
  return isEqual(pick(prevProps, propFields), pick(nextProps, propFields))
})
