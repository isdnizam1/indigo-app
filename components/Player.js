import React from 'react'
import { connect } from 'react-redux'
import { isNil } from 'lodash-es'
import Audio from '../utils/audio'
import { PLAY, STOP } from '../services/player/actionTypes'

const mapStateToProps = ({ player }) => ({
  playerIsPlaying: player.isPlaying
})

const mapDispatchToProps = {
  playerPlay: () => ({ type: PLAY }),
  playerStop: () => ({ type: STOP })
}

class Player extends React.Component {

  constructor(props) {
    super(props)
    this.playbackInstance = null
    this.state = {
      playbackInstancePosition: null,
      playbackInstanceDuration: null,
      isSliding: false,
      isPlaying: false,
      isBuffering: false,
      shouldCorrectPitch: true,
      songId: undefined,
      songUrl: undefined,
      isLoading: false
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const {
      playerIsPlaying
    } = this.props
    const {
      playbackInstancePosition,
      isSliding,
      isPlaying,
      isBuffering,
      songId,
      songUrl,
      playbackInstance,
      isLoading
    } = this.state
    return (
      playerIsPlaying != nextProps.playerIsPlaying ||
      isBuffering != nextState.isBuffering ||
      songId != nextState.songId ||
      songUrl != nextState.songUrl ||
      isPlaying != nextState.isPlaying ||
      isLoading != nextState.isLoading ||
      isSliding != nextState.isSliding ||
      playbackInstance != nextState.playbackInstance ||
      playbackInstancePosition != nextState.playbackInstancePosition
    )
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { playerIsPlaying } = this.props
    playerIsPlaying && !nextProps.playerIsPlaying && !isNil(this.playbackInstance) && this.playbackInstance.pauseAsync()
  }

  async componentDidMount() {
    this._loadNewPlaybackInstance(this.props.songUrl)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.songUrl !== prevProps.songUrl) this._loadNewPlaybackInstance(this.props.songUrl)
  }

  componentWillUnmount() {
    this._onStopPressed()
  }

  async _loadNewPlaybackInstance(url, songId) {
    await this.setState({ songUrl: url, songId, isLoading: true })
    if (this.playbackInstance != null) {
      await this.playbackInstance.unloadAsync()
      this.playbackInstance = null
    }

    // const source = { uri: 'https://soundfren-music-dev.s3.ap-southeast-1.amazonaws.com/uploads/322-2020-01-06-93e6f7ac-debe-4e4b-fdf1-b6316f588c5d.mp3', overrideFileExtensionAndroid: true }
    let source = { uri: url, overrideFileExtensionAndroid: 'm3u8' }
    const initialStatus = { shouldPlay: false, shouldCorrectPitch: true }
    try {
      this.soundObject = new Audio.Sound()
      this.soundObject.setOnPlaybackStatusUpdate(this._onPlaybackStatusUpdate)
      this.soundObject.setIsLoopingAsync(true)
      await this.soundObject.loadAsync(source, initialStatus, false)
      this.playbackInstance = this.soundObject
    } catch (error) {
      try {
        source = { uri: url, overrideFileExtensionAndroid: 'mp3' }
        this.soundObject = new Audio.Sound()
        this.soundObject.setOnPlaybackStatusUpdate(this._onPlaybackStatusUpdate)
        this.soundObject.setIsLoopingAsync(true)
        await this.soundObject.loadAsync(source, initialStatus, false)
        this.playbackInstance = this.soundObject
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(`ERROR: ${error}`)
      }
    }
    this.setState({ isLoading: false })
  }

  _toggleSliding = (isSliding) => {
    this.setState((state) => ({
      isSliding: isSliding || !state.isSliding,
    }))
  }

  _onChangeMillis = async (value) => {
    const shouldPlay = this.state.isPlaying
    await this.playbackInstance.pauseAsync()
    this.playbackInstance.setStatusAsync({
      shouldPlay,
      positionMillis: value
    })
  }

  _onPlaybackStatusUpdate = (status) => {
    if (status.isLoaded) {
      Math.abs(this.state.playbackInstancePosition - status.positionMillis) > 450 && this.setState((state) => ({
        playbackInstancePosition: state.isSliding ? state.playbackInstancePosition : status.positionMillis,
        playbackInstanceDuration: status.durationMillis,
        isPlaying: status.isPlaying,
        isBuffering: status.isBuffering,
        shouldCorrectPitch: status.shouldCorrectPitch
      }))
      if(status.didJustFinish) {
        this.setState((state) => ({
          playbackInstancePosition: state.isSliding ? state.playbackInstancePosition : 0
        }))
        this.playbackInstance.setPositionAsync(0)
      }
    } else {
      if (status.error) {
        // eslint-disable-next-line no-console
        console.log(`ERROR: ${error}`)
      }
    }
  }

  _onPlayPausePressed = async () => {
    try {
      if (this.playbackInstance != null) {
        if (this.state.isPlaying) {
          await this.playbackInstance.pauseAsync()
          this.setState({ isPlaying: false })
        } else {
          await this.playbackInstance.playAsync()
          this.setState({ isPlaying: true })
          this.props.playerPlay()
        }
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(`ERROR: ${error}`)
    }
  }

  _onStopPressed = async () => {
    if (this.playbackInstance != null) {
      await this.playbackInstance.stopAsync()
    }
  }

  _onChangeSong = async (songUrl) => {
    await this._onStopPressed()
    await this._loadNewPlaybackInstance(songUrl)
    await this._onPlayPausePressed()
  }

  render() {
    const {
      children
    } = this.props
    return children({
      isPlaying: this.state.isPlaying,
      isBuffering: this.state.isBuffering,
      isSliding: this.state.isSliding,
      playbackInstancePosition: this.state.playbackInstancePosition,
      playbackInstanceDuration: this.state.playbackInstanceDuration,
      playbackInstance: this.state.playbackInstance,
      onChangeMillis: this._onChangeMillis,
      toggleSliding: this._toggleSliding,
      toggle: this._onPlayPausePressed,
      stop: this._onStopPressed,
      changeAndPlay: this._onChangeSong,
      songId: this.state.songId,
      songUrl: this.state.songUrl,
      isLoading: this.state.isLoading
    })
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Player)
