import React from 'react'
import PropTypes from 'prop-types'
import { Image, StyleSheet, View } from 'react-native'
import { keys } from 'lodash-es'
import { LOADER_SIZE } from '../constants/Sizes'
import Text from './Text'

const propsType = {
  key: PropTypes.any,
  isLoading: PropTypes.bool,
  size: PropTypes.oneOf(keys(LOADER_SIZE)),
}

const propsDefault = {
  key: undefined,
  isLoading: false,
  size: 'small'
}

const Loader = (props) => (
  props.isLoading
    ? (
      <View style={style.container}>
        <Image source={require('../assets/loading.gif')} style={[style.loader, { width: LOADER_SIZE[props.size] }]}/>
      </View>
    ) : props.children || (
      <View style={style.container}>
        <Text size='mini'>Drummer!!!</Text>
      </View>
    )
)
Loader.propTypes = propsType

Loader.defaultProps = propsDefault

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loader: {
    height: undefined,
    aspectRatio: 1
  }
})

export default Loader
