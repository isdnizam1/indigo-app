import React from 'react'
import { View } from 'react-native'
import { ORANGE_BRIGHT, WHITE } from '../../constants/Colors'
import { HP05, HP2, WP100 } from '../../constants/Sizes'

const NewMessageIndicator = () => (
  <View style={{
    backgroundColor: WHITE,
    borderColor: ORANGE_BRIGHT,
    borderRadius: WP100,
    borderWidth: 2.5,
    position: 'absolute',
    right: -(HP05),
    top: -(HP05),
    width: HP2,
    height: HP2
  }}
  />
)

NewMessageIndicator.propTypes = {}

NewMessageIndicator.defaultProps = {}

export default NewMessageIndicator
