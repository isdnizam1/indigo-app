import React, { useEffect, useRef } from 'react'
import { View, Animated, TouchableOpacity, Dimensions, StyleSheet } from 'react-native'
import { TOMATO, GREY_CALM, WHITE, GREY80 } from '../constants/Colors'
import Text from './Text'

const { width, height } = Dimensions.get('window')

const style = StyleSheet.create({
  popup: {
    width: 280,
    height: 160,
    backgroundColor: WHITE,
    position: 'absolute',
    top: height / 2 - 80,
    left: width / 2 - 140,
    borderRadius: 15,
    elevation: 5
  },
  button: {
    flex: 1,
    height: 58,
    backgroundColor: GREY_CALM,
    justifyContent: 'center'
  }
})

const DeletePost = ({ onConfirm, onCancel, containerStyle }) => {

  let fadeAnim = useRef(new Animated.Value(0)).current

  useEffect(() => {
    setTimeout(() => {
      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 200,
        useNativeDriver: true
      }).start()
    }, 300)
  }, [])

  return (
    <Animated.View style={{ ...style.popup, opacity: fadeAnim, ...containerStyle }}>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text color={GREY80} size={'mini'} centered weight={500}>Are you sure to delete this?</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity activeOpacity={0.9} onPress={onConfirm} style={{ ...style.button, borderBottomLeftRadius: 15 }}>
          <Text centered size={'mini'} weight={500}>Yes</Text>
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={0.9} onPress={onCancel} style={{ ...style.button, borderBottomRightRadius: 15, backgroundColor: TOMATO }}>
          <Text color={WHITE} centered size={'mini'} weight={500}>Cancel</Text>
        </TouchableOpacity>
      </View>
    </Animated.View>
  )

}

DeletePost.defaultProps = {
  onConfirm: null,
  onCancel: null,
  containerStyle: {}
}

export default React.memo(DeletePost)
