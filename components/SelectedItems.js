import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { noop, map } from 'lodash-es'
import { PALE_BLUE_TWO, SHIP_GREY, WHITE } from '../constants/Colors'
import { WP2, WP205, WP3 } from '../constants/Sizes'
import Text from './Text'
import Icon from './Icon'

const SelectedItems = ({ items, onClose, style }) => (
  <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
    {
      map(items, (item, index) => (
        <View
          style={[{
            marginRight: WP2,
            marginBottom: WP2,
            flexDirection: 'row',
            alignItems: 'center',
            paddingHorizontal: WP3,
            paddingVertical: WP2,
            borderRadius: 6,
            backgroundColor: WHITE,
            borderWidth: 1,
            borderColor: PALE_BLUE_TWO
          }, style]}
        >
          <Text size='xmini' type='Circular' weight={400} color={SHIP_GREY} style={{ marginRight: WP205 }}>{item}</Text>
          <Icon
            onPress={() => {
              onClose(index)
            }}
            centered
            size='small'
            color={PALE_BLUE_TWO}
            name={'close'}
            type='MaterialCommunityIcons'
          />
        </View>
      ))
    }
  </View>
)

SelectedItems.propTypes = {
  items: PropTypes.array,
  onClose: PropTypes.func,
  style: PropTypes.objectOf(PropTypes.any)
}

SelectedItems.defaultProps = {
  items: [],
  onClose: noop,
}

export default SelectedItems
