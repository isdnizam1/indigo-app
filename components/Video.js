import React from 'react'
import {
  View,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
} from 'react-native'
import { upperFirst } from 'lodash-es'
import PropTypes from 'prop-types'
import {
  GUN_METAL,
  PALE_BLUE,
  PALE_GREY,
  REDDISH,
  SHIP_GREY_CALM,
  TRANSPARENT,
} from 'sf-constants/Colors'
import {
  WP1,
  WP2,
  WP3,
  WP305,
  WP5,
  WP6,
  WP7,
  WP70,
} from 'sf-constants/Sizes'
import { NavigateToInternalBrowser } from 'sf-utils/helper'
import { postProfileDelVideo, postLogViewJourney } from 'sf-actions/api'
import ModalMessageView from 'sf-components/ModalMessage/ModalMessageView'
import Text from './Text'
import ButtonV2 from './ButtonV2'

const style = StyleSheet.create({
  firstVideo: {
    paddingVertical: WP2,
    width: WP70,
    marginRight: WP7,
    marginLeft: WP5,
  },
  video: {
    paddingVertical: WP2,
    width: WP70,
    marginRight: WP7,
  },
  videoCoverWrapper: {
    width: WP70,
    height: WP70 * 0.5615,
    borderRadius: WP2,
    backgroundColor: PALE_BLUE,
    marginBottom: WP3,
  },
  videoCover: {
    width: WP70,
    height: WP70 * 0.5615,
    borderRadius: WP2,
  },
  videoContent: {
    flexGrow: 1,
  },
  videoContentMeta: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: WP2,
  },
  videoContentAction: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: PALE_BLUE,
    marginTop: WP3,
  },
  videoContentActionButton: {
    paddingVertical: 0,
    paddingRight: WP2,
    marginRight: WP3,
  },
  modalTitle: {
    marginTop: WP2,
    marginBottom: WP5
  },
  modalSubtitle: {

  },
  modalButtonPrimary: { borderRadius: 8, paddingVertical: WP305 },
  modalButtonSecondary: { backgroundColor: PALE_GREY, marginTop: WP1, borderRadius: 8, paddingVertical: WP305 },
})

class Video extends React.Component {
  static propTypes = {
    isMine: PropTypes.bool,
    firstVideo: PropTypes.bool,
    refreshProfile: PropTypes.func,
    navigateBack: PropTypes.func,
    navigateTo: PropTypes.func.isRequired,
    video: PropTypes.object.isRequired,
    idViewer: PropTypes.any.isRequired,
  };

  constructor(props) {
    super(props)
    this.state = {
      deleteModalVisible: false,
      totalView: parseInt(props.video.total_view)
    }
    this._onEdit = this._onEdit.bind(this)
    this._beforeDelete = this._beforeDelete.bind(this)
    this._confirmDelete = this._confirmDelete.bind(this)
    this._cancelDelete = this._cancelDelete.bind(this)
  }

  _navInternalBrowser = (url) => () => {
    const { video: { id_journey }, idViewer: id_viewer } = this.props
    this.setState({
      totalView: this.state.totalView + 1
    }, () => {
      const body = {
        id_journey,
        id_user: id_viewer
      }
      postLogViewJourney(body)
      NavigateToInternalBrowser({ url })
    })
  };

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.deleteModalVisible != this.state.deleteModalVisible || nextProps.firstVideo != this.state.firstVideo || nextState.totalView != this.state.totalView
  }

  _onEdit = (initialValues) => () => this.props.navigateTo('ProfileAddVideoScreen', {
    refreshProfile: this.props.refreshProfile || (() => {}),
    initialValues
  })

  _beforeDelete = () => this.setState({ deleteModalVisible: true })

  _confirmDelete = () => {
    this.setState({
      deleteModalVisible: false
    }, () => {
      const { refreshProfile, video: { id_journey } } = this.props
      postProfileDelVideo({ id_journey }).then(refreshProfile)
    })
  }

  _cancelDelete = () => this.setState({ deleteModalVisible: false })

  render() {
    const {
      firstVideo,
      video,
      isMine,
      video: { additional_data: add },
    } = this.props
    const { deleteModalVisible, totalView } = this.state
    return (
      <View style={firstVideo ? style.firstVideo : style.video}>
        <TouchableWithoutFeedback
          onPress={this._navInternalBrowser(add.url_video)}
        >
          <View style={style.videoCoverWrapper}>
            <Image style={style.videoCover} source={{ uri: add.cover_image, cache: 'force-cache' }} />
          </View>
        </TouchableWithoutFeedback>
        <View style={style.videoContent}>
          <Text
            onPress={this._navInternalBrowser(add.url_video)}
            lineHeight={WP6}
            numberOfLines={2}
            type={'Circular'}
            color={GUN_METAL}
            weight={600}
          >
            {upperFirst(video.title)}
          </Text>
          <View style={style.videoContentMeta}>
            <Text
              type={'Circular'}
              weight={300}
              color={SHIP_GREY_CALM}
              size={'xmini'}
            >
              {`${video.created_id}`}{totalView ? `   ·   ${totalView} Ditonton` : ''}
            </Text>
          </View>
          {isMine && (
            <View style={style.videoContentAction}>
              <ButtonV2
                onPress={this._onEdit({
                  ...video,
                  videoUrl: add.url_video,
                })}
                borderColor={TRANSPARENT}
                style={style.videoContentActionButton}
                icon={'pencil'}
                iconSize={'xmini'}
                text={'Edit'}
              />
              <ButtonV2
                onPress={this._beforeDelete}
                borderColor={TRANSPARENT}
                style={style.videoContentActionButton}
                icon={'delete'}
                iconSize={'xmini'}
                text={'Hapus'}
              />
            </View>
          )}
        </View>
        <ModalMessageView
          style={style.modal}
          contentStyle={style.modalContent}
          toggleModal={this._cancelDelete}
          isVisible={deleteModalVisible}
          title={'Hapus Video'}
          titleType='Circular'
          titleSize={'small'}
          titleColor={GUN_METAL}
          titleStyle={style.modalTitle}
          subtitle={'Apakah kamu yakin ingin menghapus video yang telah kamu upload ini?'}
          subtitleType='Circular'
          subtitleSize={'xmini'}
          subtitleWeight={300}
          subtitleLineHeight={WP5}
          subtitleColor={SHIP_GREY_CALM}
          subtitleStyle={style.modalSubtitle}
          image={null}
          buttonPrimaryText={'Tidak'}
          buttonPrimaryContentStyle={style.modalButtonPrimary}
          buttonPrimaryTextType='Circular'
          buttonPrimaryTextWeight={400}
          buttonPrimaryBgColor={REDDISH}
          buttonSecondaryText={'Ya'}
          buttonSecondaryStyle={style.modalButtonSecondary}
          buttonSecondaryTextType='Circular'
          buttonSecondaryTextWeight={400}
          buttonSecondaryTextColor={SHIP_GREY_CALM}
          buttonSecondaryAction={this._confirmDelete}
        />
      </View>
    )
  }
}

export default React.memo(Video)