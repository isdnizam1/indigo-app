import React from 'react'
import { View } from 'react-native'

class Spacer extends React.PureComponent {
  render() {
    const { horizontal, size, color = 'transparent' } = this.props
    return (
      <View
        style={{
          [horizontal ? 'width' : 'height']: size,
          backgroundColor: color,
        }}
      />
    )
  }
}

export default React.memo(Spacer)
