import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  FlatList,
  KeyboardAvoidingView,
  Modal as RNModal,
  Platform,
  TouchableOpacity,
  View,
} from 'react-native'
import { concat, isEmpty, isNil, isObject, map, noop } from 'lodash-es'
import HTMLElement from 'react-native-render-html'
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils'
import moment from 'moment'
import Text from '../Text'
import Icon from '../Icon'
import {
  GUN_METAL,
  PALE_GREY,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  WHITE,
  REDDISH_LIGHT,
} from '../../constants/Colors'
import {
  FONT_SIZE,
  WP1,
  WP10,
  WP100,
  WP2,
  WP205,
  WP3,
  WP4,
  WP5,
  WP6,
  HP100,
  HP50,
  HP5,
} from '../../constants/Sizes'
import MenuOptions from '../MenuOptions'
import Card from '../Card'
import Avatar from '../Avatar'
import MentionTextInput from '../MentionTextInput'
import {
  deleteStartUpComment,
  getMentionSuggestions,
  postStartUpComment,
  getAdsDetail,
} from '../../actions/api'
import { TOUCH_OPACITY } from '../../constants/Styles'
import ButtonV2 from '../ButtonV2'
import DeletePost from '../DeletePost'
import { DEFAULT_PAGING } from '../../constants/Routes'
import Loader from '../Loader'
import { isIOS } from '../../utils/helper'
import ModalMessageView from '../ModalMessage/ModalMessageView'

class StartUpCommentSection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      comment: '',
      comments: this.props.comments || [],
      isShowKeyboard: false,
      mentionComponentKey: Math.random(),
      mentions: [],
      mentionSuggestions: [],
      onConfirmDelete: null,
      openPanel: false,
      reply: '',
      replyMentions: [],
      replyTo: null,
      startComment: 0,
      limitComment: 50,
      loading: false,
      reachToEnd: false,
      isReply: false,
      commentHeight: 0,
    }
  }

  componentDidMount = async () => {
  };

  _getComments = async () => {
    const {
      dispatch,
      feedId,
      userData: { id_user },
      releaseBottomReachedState,
    } = this.props
    const { loading, comments } = this.state
    this.props.getAdsDetail()
  };

  _onPressOverlay = () => {
    this.setState({ replyTo: null })
  };

  _profileSreen = (idUser) => () => {
    this.props.navigateTo('ProfileScreenNoTab', {
      idUser,
      navigateBackOnDone: true,
    })
  };

  componentDidUpdate(nextProps, nextState) {
    
  }

  _onSelectSuggestion = (suggestion) => {
    // hidePanel()
    let { comment, mentions, replyMentions, replyTo, reply } = this.state
    if (!isNil(replyTo)) comment = reply
    isNil(replyTo) && mentions.push(suggestion)
    !isNil(replyTo) && replyMentions.push(suggestion)
    this.setState({ mentionSuggestions: [] })
    const formattedComment = `${
      comment.substring(0, comment.lastIndexOf('@')) + suggestion.name.trim()
    } `
    this.setState({
      [isNil(replyTo) ? 'comment' : 'reply']: formattedComment,
      mentions,
      replyMentions,
      mentionComponentKey: Math.random(),
    })
    // updateText(formattedComment, suggestion)
  };

  _getMentionSuggestion = async (full_name) => {
    if (!full_name) return
    full_name = full_name.substring(1, full_name.length)
    const { dispatch } = this.props

    const params = {
      start: 0,
      limit: 5,
      full_name,
      platform: '1000startup'
    }

    const suggestions = await dispatch(
      getMentionSuggestions,
      params,
      noop,
      true,
      false,
    )

    this.setState({ mentionSuggestions: suggestions.result || [] })
  };

  _renderSuggestionsRow = (suggestion, index) => {
    const { mentionSuggestions } = this.state
    return (
      <TouchableOpacity
        key={`suggestion-${suggestion.id}`}
        activeOpacity={TOUCH_OPACITY}
        onPressIn={() => this._onSelectSuggestion(suggestion)}
      >
        <View
          style={[
            {
              paddingVertical: WP2,
              paddingHorizontal: WP4,
              flexDirection: 'row',
              backgroundColor: WHITE,
              alignItems: 'center',
              borderTopWidth: 1,
              borderTopColor: PALE_GREY,
            },
            index == 0
              ? {
                  borderTopLeftRadius: 8,
                  borderTopRightRadius: 8,
                }
              : {},
            mentionSuggestions.length - 1 == index
              ? {
                  borderBottomLeftRadius: 8,
                  borderBottomRightRadius: 8,
                }
              : {},
          ]}
        >
          <Avatar size={'mini'} image={suggestion.avatar} />
          <Text type={'Circular'} size={'xmini'} style={{ marginLeft: WP2 }}>
            {suggestion.name}
          </Text>
        </View>
      </TouchableOpacity>
    )
  };

  _postComment = async (isReply = false) => {
    const {
      mentions,
      comment: commentValue,
      reply,
      replyMentions,
      id_parent,
    } = this.state
    const {
      dispatch,
      feedId,
      onSubmitComment,
      userData: { id_user },
    } = this.props
    let comment = isReply ? reply : commentValue
    let mentionedUser = isReply ? replyMentions : mentions
    mentionedUser.forEach((element) => {
      comment = comment.replace(
        element.name,
        `<mention id=${element.id}>${element.name}</mention>`,
      )
    })
    comment = comment.trim()
    this.setState({
      mentions: [],
      comment: '',
      reply: '',
      replyMentions: [],
      mentionComponentKey: Math.random(),
      replyTo: null,
      id_parent: undefined,
    })

    await dispatch(
      postStartUpComment,
      {
        related_to: isReply ? 'id_comment' : 'id_ads',
        id_related_to: isReply ? id_parent : feedId,
        id_user,
        comment,
      },
      noop,
      false,
      false,
    )
    onSubmitComment(isReply)
    this.props.getAdsDetail()
    this.setState({
      isReply: false
    })
  };

  _onDeleteComment = async (id_comment) => {
    const { dispatch, getAdsDetail } = this.props
    const onConfirmDelete = async () => {
      await dispatch(deleteStartUpComment, { id_comment }, noop, false, false)
      await this.setState({
        onConfirmDelete: null,
      })
      this.props.getAdsDetail()
    }

    setTimeout(() => {
      this.setState({ onConfirmDelete })
    }, 500)
  };

  _onReplyComment = (comment, id_parent) => async () => {
    const replyTo = {
      id: comment.id_user,
      name: comment.full_name,
    }
    this.setState({
      isReply: true,
      reply: `${comment.full_name} `,
      replyTo,
      id_parent,
      replyMentions: [replyTo],
    })
  };

  _parseComment = (textHtml) => {
    return (
      <HTMLElement
        textSelectable
        containerStyle={{ flexDirection: 'row' }}
        html={textHtml}
        customWrapper={(children) => (
          <Text weight={400} color={SHIP_GREY_CALM} type={'Circular'} size='xmini'>
            {children}
          </Text>
        )}
        baseFontStyle={{ fontFamily: 'OpenSansRegular' }}
        ignoredTags={[...IGNORED_TAGS]}
        renderers={{
          mention: (attbs, children, convertedCSSStyles, passProps) => {
            return (
              <Text
                type={'Circular'}
                weight={500}
                onPress={this._profileSreen(attbs.id)}
                style={{ color: REDDISH }}
                key={Math.random()}
                size='xmini'
              >
                {children}{' '}
              </Text>
            )
          },
        }}
      />
    )
  };

  _renderComment = (comment, id_parent) => {
    const isParent = isObject(comment.reply_comment)
    const myComment = comment.id_user == this.props.userData.id_user
    return (
      <View
        key={`comment-${comment.id_comment}`}
        style={{
          flexDirection: 'row',
          paddingHorizontal: WP5,
          // paddingLeft: WP5,
          [isParent ? 'marginBottom' : 'marginTop']: WP3,
        }}
      >
        <Avatar
          size={isParent ? 'xsmall' : 'mini'}
          shadow
          onPress={this._profileSreen(comment.id_user)}
          image={comment.profile_picture}
        />
        <View style={{ flex: 1 }}>
          <View style={{ paddingLeft: WP3 }}>
            <View
              style={{
                paddingHorizontal: WP3,
                paddingTop: WP2,
                paddingBottom: WP3,
                borderRadius: 5.5,
                backgroundColor: PALE_GREY,
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  marginBottom: 1.5,
                }}
              >
                <Text style={{marginRight: 10}}>
                  <Text
                    onPress={this._profileSreen(comment.id_user)}
                    size='xmini'
                    type={'Circular'}
                    color={GUN_METAL}
                    weight={500}
                  >
                    {comment?.full_name}
                  </Text>
                </Text>
                {comment?.access == 'admin' && <View style={{ borderWidth: 1, borderColor: REDDISH, backgroundColor: REDDISH_LIGHT, paddingHorizontal: 3}}>
                  <Text
                    onPress={this._profileSreen(comment.id_user)}
                    size='xtiny'
                    type={'Circular'}
                    color={REDDISH}
                    weight={400}
                  >
                    {`Admin`}
                  </Text>
                </View>}
              </View>
              {this._parseComment(comment?.comment)}
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: WP1 + (isParent ? 3 : 0),
              }}
            >
              <Text
                    type={'Circular'}
                    size='xmini'
                    color={SHIP_GREY_CALM}
                    weight={400}
                  >
                    {`${moment(comment.created_at)
                      .locale('id')
                      .fromNow(true)
                      .replace('beberapa detik', 'baru saja')} · `}
                  </Text>
              <Text
                onPress={this._onReplyComment(comment, id_parent)}
                size='xmini'
                type={'Circular'}
                color={SHIP_GREY_CALM}
                weight={400}
              >
                {'Balas'}
                {/* {!isEmpty(comment.reply_comment)
                  ? `   ·   ${comment.reply_comment.length} balasan`
                  : ''} */}
              </Text>
              {myComment && <Text
                size='xmini'
                type={'Circular'}
                color={SHIP_GREY_CALM}
                weight={400}
              >
                {'   │   '}
              </Text>}
              {myComment && <Text
                onPress={() => this._onDeleteComment(comment.id_comment)}
                size='xmini'
                type={'Circular'}
                color={SHIP_GREY_CALM}
                weight={400}
              >
                {'Hapus'}
              </Text>}
            </View>
            {isParent && (
              <FlatList
                style={{ marginHorizontal: -WP5 }}
                data={comment.reply_comment}
                renderItem={({ item: reply }) =>
                  this._renderComment(reply, id_parent)
                }
                keyExtractor={(item) => item.id_comment}
                extraData={null}
              />
            )}
          </View>
        </View>
      </View>
    )
  };

  _renderPopup = () => {
    const { userData } = this.props
    const {
      onConfirmDelete,
    } = this.state
    return (
      <View>
        <ModalMessageView
          style={{}}
          subtitleStyle={{marginTop: 15}}
          buttonSecondaryStyle={{
            backgroundColor: PALE_GREY,
            marginTop: WP1,
            borderRadius: 8,
            paddingVertical: WP3
          }}
          toggleModal={() => this.setState({ onConfirmDelete: null })}
          isVisible={!isNil(onConfirmDelete)}
          title={'Hapus Komentar?'}
          titleType='Circular'
          titleSize={'small'}
          titleColor={GUN_METAL}
          subtitle={'Apakah kamu yakin untuk menghapus secara permanen komentar ini?'}
          subtitleType='Circular'
          subtitleSize={'xmini'}
          subtitleWeight={400}
          subtitleColor={SHIP_GREY_CALM}
          image={null}
          buttonPrimaryText={'Batalkan'}
          buttonPrimaryTextType='Circular'
          buttonPrimaryTextWeight={400}
          buttonPrimaryBgColor={REDDISH}
          buttonPrimaryAction={() => this.setState({ onConfirmDelete: null })}
          buttonSecondaryText={'Hapus'}
          buttonSecondaryTextType='Circular'
          buttonSecondaryTextWeight={400}
          buttonSecondaryTextColor={SHIP_GREY_CALM}
          buttonSecondaryBgColor={PALE_GREY}
          buttonSecondaryAction={onConfirmDelete}
        />
      </View>
    )
  };

  _renderCommentForm = () => {
    const { userData, onFocusCommentInput, comments } = this.props

    const {
      comment,
      mentionComponentKey,
      mentionSuggestions,
      replyTo,
      isReply
    } = this.state
    return (
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: WP5,
          alignItems: 'flex-start',
        }}
      >
        <Avatar
          size={'xsmall'}
          shadow={false}
          onPress={this._profileSreen(userData.id_user)}
          image={userData.profile_picture}
        />
        <View style={{ flex: 1, paddingLeft: WP3, paddingRight: WP2 }}>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
          >
            {!!userData.id_user && (
              <MentionTextInput
                key={mentionComponentKey}
                onFocus={onFocusCommentInput}
                below
                placeholder=''
                textInputStyle={{
                  fontSize: FONT_SIZE['mini'],
                  color: GUN_METAL,
                  backgroundColor: PALE_GREY,
                  paddingTop: isIOS() ? WP205 : WP1,
                  paddingBottom: isIOS() ? WP205 : WP1,
                  paddingHorizontal: WP3,
                  maxHeight: FONT_SIZE['mini'] * 5,
                  lineHeight: FONT_SIZE['mini'] * 1.28,
                  borderRadius: 5.5,
                  minHeight:
                    this.state.replyInputMinHeight || FONT_SIZE['mini'] * 2.75,
                }}
                suggestionsPanelStyle={{
                  backgroundColor: 'rgba(100,100,100,0.1)',
                }}
                onChangedSelected={(selected) => {
                  this.setState({ mentions: selected })
                }}
                trigger={'@'}
                triggerLocation={'new-word-only'} // 'new-word-only', 'anywhere'
                value={isReply ? this.state.reply : this.state.comment}
                onChangeText={(val) => {
                  this.setState({ comment: val })
                }}
                onOpenSuggestionsPanel={() => {
                  this.setState({ openPanel: true })
                }}
                onCloseSuggestionsPanel={() => {
                  this.setState({
                    openPanel: false,
                    mentionSuggestions: [],
                  })
                }}
                triggerDelay={2}
                triggerCallback={this._getMentionSuggestion}
                _renderSuggestionsRow={this._renderSuggestionsRow}
                suggestionsData={mentionSuggestions}
                keyExtractor={(item, index) => {
                  if (item != null) {
                    return item.id
                  }
                }}
                suggestionRowHeight={45}
                horizontal={false}
                MaxVisibleRowCount={5}
              />
            )}
            {!userData.id_user && (
              <TouchableOpacity
                onPress={() => this.props.navigateTo('AuthNavigator')}
              >
                <View
                  style={{
                    flex: 1,
                    backgroundColor: PALE_GREY,
                    paddingTop: isIOS() ? WP205 : WP1,
                    paddingBottom: isIOS() ? WP205 : WP1,
                    paddingHorizontal: WP3,
                    maxHeight: FONT_SIZE['mini'] * 5,
                    lineHeight: FONT_SIZE['mini'] * 1.28,
                    borderRadius: 5.5,
                    minHeight: FONT_SIZE['mini'] * 2.75,
                  }}
                />
              </TouchableOpacity>
            )}
          </KeyboardAvoidingView>
        </View>
        <ButtonV2
          disabled={comment.length == 0}
          color={REDDISH}
          textColor={WHITE}
          style={{ paddingHorizontal: WP5 }}
          onPress={() => this._postComment(false)}
          onLayout={({
            nativeEvent: {
              layout: { height: replyInputMinHeight },
            },
          }) => this.setState({ replyInputMinHeight })}
          text={'Post'}
        />
      </View>
    )
  }

  _renderReplyCommentForm = () => {
    const { userData, onFocusCommentInput, comments } = this.props

    const {
      comment,
      mentionComponentKey,
      mentionSuggestions,
      replyTo,
      isReply
    } = this.state
    return (
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: WP5,
          alignItems: 'flex-start',
        }}
      >
        <Avatar
          size={'xsmall'}
          shadow={false}
          onPress={this._profileSreen(userData.id_user)}
          image={userData.profile_picture}
        />
        <View style={{ flex: 1, paddingLeft: WP3, paddingRight: WP2 }}>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
          >
            <MentionTextInput
              key={`${mentionComponentKey}-reply`}
              autoFocus
              above
              placeholder=''
              textInputStyle={{
                backgroundColor: PALE_GREY,
                borderRadius: 5.5,
                color: GUN_METAL,
                fontSize: FONT_SIZE['mini'],
                lineHeight: FONT_SIZE['mini'] * 1.28,
                maxHeight: FONT_SIZE['mini'] * 5,
                minHeight:
                  this.state.commentInputMinHeight ||
                  FONT_SIZE['mini'] * 2.75,
                paddingHorizontal: WP3,
                paddingTop: isIOS() ? WP205 : WP1,
                paddingBottom: isIOS() ? WP205 : WP1,
              }}
              suggestionsPanelStyle={{
                backgroundColor: 'rgba(100,100,100,0.1)',
              }}
              onChangedSelected={(selected) => {
                this.setState({ mentions: selected })
              }}
              trigger={'@'}
              triggerLocation={'new-word-only'} // 'new-word-only', 'anywhere'
              value={this.state.reply}
              onChangeText={(val) => {
                this.setState({ reply: val })
              }}
              onOpenSuggestionsPanel={() => {
                this.setState({ openPanel: true })
              }}
              onCloseSuggestionsPanel={() => {
                this.setState({
                  openPanel: false,
                  mentionSuggestions: [],
                })
              }}
              triggerDelay={2}
              triggerCallback={this._getMentionSuggestion}
              _renderSuggestionsRow={this._renderSuggestionsRow}
              suggestionsData={mentionSuggestions}
              keyExtractor={(item, index) => {
                if (item != null) {
                  return item.id
                }
              }}
              suggestionRowHeight={45}
              horizontal={false}
              MaxVisibleRowCount={5}
            />
          </KeyboardAvoidingView>
        </View>
        <ButtonV2
          disabled={this.state.reply.length == 0}
          color={REDDISH}
          textColor={WHITE}
          style={{ paddingHorizontal: WP5 }}
          onPress={() => this._postComment(true)}
          onLayout={({
            nativeEvent: {
              layout: { height: commentInputMinHeight },
            },
          }) => this.setState({ commentInputMinHeight })}
          text={'Post'}
        />
      </View>
    )
  }

  render() {
    const { userData, onFocusCommentInput, comments } = this.props

    const {
      comment,
      mentionComponentKey,
      mentionSuggestions,
      replyTo,
      isReply
    } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: '#FCFCFC', paddingVertical: WP5 }}>
        {this._renderPopup()}
        {/* <Card
          onLayout={({
            nativeEvent: {
              layout: { y: offsetTop },
            },
          }) => this.setState({ offsetTop })}
          style={{ paddingHorizontal: 0, marginTop: 1, flex: 1 }}
        > */}
          <Text
            size='xmini'
            color={SHIP_GREY}
            weight={400}
            style={{ paddingHorizontal: WP5, marginBottom: WP3 }}
          >
            Komentar ({this.props.totalComment})
          </Text>
          {isReply ? this._renderReplyCommentForm() : this._renderCommentForm()}
          {mentionSuggestions.length == 0 && isReply && replyTo !== null && (
            <View style={{
              paddingHorizontal: WP5,
              marginTop: WP5
              }}>
              <Text
                size='mini'
                color={SHIP_GREY_CALM}
                weight={400}
                style={{ marginBottom: WP3 }}
              >
                Membalas {replyTo.name}
              </Text>
            </View>
          )}
          {comments.length != '0' && (
            <View
              style={{
                height: 1.0,
                marginVertical: WP3,
                backgroundColor: PALE_GREY,
                marginHorizontal: WP5,
              }}
            />
          )}
          {comments.length > 0 && (
            <View style={{ flex: 1 }}>
              <FlatList
                data={this.props.comments}
                onLayout={(event) => {
                  const {x, y, width, height} = event.nativeEvent.layout;
                  this.setState({commentHeight: height})
                }}
                style={{ flex: 1 }}
                renderItem={({ item }) => this._renderComment(item, item.id_comment)}
                ListFooterComponent={
                  this.state.loading && !this.state.reachToEnd ? (
                    <Loader isLoading size={'mini'} />
                  ) : null
                }
                keyExtractor={(item) => item.id_comment}
                extraData={null}
              />
            </View>
          )}
          {console.log(`comment height ${this.state.commentHeight}`)}
          {<View>
            <View
              style={{
                position: 'absolute',
                backgroundColor: WHITE,
                top: -(this.state.commentHeight + 20),
                width: WP100 - WP10,
                elevation: 5,
                marginLeft: WP5,
                borderRadius: 5,
                borderWidth: mentionSuggestions.length > 0 ? 0.5 : 0,
                zIndex: 999,
              }}
            >
              {map(mentionSuggestions, this._renderSuggestionsRow)}
            </View>
          </View>}
        {/* </Card> */}
      </View>
    )
  }
}

StartUpCommentSection.propTypes = {
  userData: PropTypes.objectOf(PropTypes.any),
  dispatch: PropTypes.func,
  commentCount: PropTypes.number,
  comments: PropTypes.arrayOf(PropTypes.any),
  onFocusCommentInput: PropTypes.func,
  onSubmitComment: PropTypes.func,
}

StartUpCommentSection.defaultProps = {
  userData: {},
  commentCount: 0,
  dispatch: noop,
  comments: [],
  onFocusCommentInput: noop,
  onSubmitComment: noop,
}

export default StartUpCommentSection
