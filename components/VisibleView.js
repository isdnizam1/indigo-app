import React from 'react'
import { View } from 'react-native'
import { isFunction } from 'lodash-es'

class VisibleView extends React.Component {

  constructor(props) {
    super(props)
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.visible != this.props.visible
  }

  componentDidUpdate(prevProps, prevState) {
    if(!prevProps.visible && this.props.visible && isFunction(this.props.onVisible)) this.props.onVisible()
    if(prevProps.visible && !this.props.visible && isFunction(this.props.onHidden)) this.props.onHidden()
    if(prevProps.visible != this.props.visible && isFunction(this.props.onVisibleChange)) this.props.onVisibleChange()
  }

  render() {
    const { visible, children } = this.props
    return (<View style={{ display: visible ? 'flex' : 'none' }}>
      {children}
    </View>)
  }

}

export default VisibleView
