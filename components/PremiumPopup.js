import React, { useEffect, useRef } from 'react'
import { View, Animated, Dimensions, TouchableOpacity, Image as RNImage } from 'react-native'
import PropTypes from 'prop-types'
import { isIOS } from '../utils/helper'
import { TOMATO, GREY80, WHITE } from '../constants/Colors'
import { HP1, HP2, HP4, WP5, WP80, WP100 } from '../constants/Sizes'
import style from '../styles/components/PremiumPopup'
import Button from './Button'
import Text from './Text'

const PremiumPopup = (props) => {

  let { height } = Dimensions.get('window')
  let { visible, title, message, onPress, onClose } = props
  let fadeAnim = useRef(new Animated.Value(0)).current
  let slideAnim = useRef(new Animated.Value(0)).current
  let visibleAnim = useRef(new Animated.Value(height)).current

  useEffect(() => {
    setTimeout(() => {
      Animated.timing(fadeAnim, {
        toValue: visible ? 1 : 0,
        duration: visible ? 250 : 400,
        useNativeDriver: true
      }).start()
    }, visible ? 50 : 450)
    setTimeout(() => {
      Animated.timing(slideAnim, {
        toValue: visible ? 0 : -height * 2,
        duration: visible ? 500 : 350,
        useNativeDriver: false
      }).start()
    }, visible ? 300 : 50)
    setTimeout(() => {
      Animated.timing(visibleAnim, {
        toValue: visible ? 0 : -height,
        duration: 0,
        useNativeDriver: false
      }).start()
    }, visible ? 0 : 850)
  }, [visible])

  return (
    <Animated.View style={[style.wrapper, { top: visibleAnim }]}>
      <Animated.View style={[style.modal, { opacity: fadeAnim }]} />
      <Animated.View style={[style.content, { bottom: slideAnim }]}>
        <View style={{ width: WP100 }}>
          <TouchableOpacity onPress={onClose} style={{ paddingBottom: HP1, marginBottom: HP2, paddingHorizontal: WP5 }}>
            <RNImage style={style.closeIcon} source={require('../assets/icons/close_circle_gray.png')} />
          </TouchableOpacity>
        </View>
        <RNImage style={{ marginBottom: HP1 }} source={require('../assets/images/notPremiumUser.png')} />
        <Text style={{ marginVertical: HP1 }} weight={700} color={GREY80} size={'huge'}>{title}</Text>
        <Text size={'tiny'}>{message}</Text>
        {!isIOS() && (<Button
          width={WP80}
          style={{ marginTop: HP4 }}
          radius={15}
          shadow={'none'}
          colors={[TOMATO, TOMATO]}
          onPress={onPress}
          text={'Upgrade'}
          textColor={WHITE}
          textCentered={true}
          textStyle={{ width: '100%', paddingVertical: HP1 }}
          textWeight={600}
                      />)}
      </Animated.View>
    </Animated.View>
  )

}

PremiumPopup.defaultProps = {
  title: null,
  message: null,
  onPress: null,
  onClose: null,
  visible: false
}

PremiumPopup.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  onPress: PropTypes.funct,
  onClose: PropTypes.funct,
  visible: PropTypes.bool
}

export default PremiumPopup