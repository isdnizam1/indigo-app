import React, { useEffect, useRef } from 'react'
import { View, Animated, TouchableOpacity, Dimensions, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import { TOMATO_CALM, GREY_CALM, WHITE } from '../constants/Colors'
import { WP3 } from '../constants/Sizes'
import Text from './Text'

const { width, height } = Dimensions.get('window')

const style = StyleSheet.create({
  overlay: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.75)'
  },
  popup: {
    backgroundColor: WHITE,
    position: 'absolute',
    borderRadius: 15,
    elevation: 5
  },
  button: {
    flex: 1,
    height: 58,
    backgroundColor: GREY_CALM,
    justifyContent: 'center'
  },
  option: {
    paddingVertical: WP3,
    borderTopWidth: 1,
    borderTopColor: GREY_CALM
  }
})

const Dialog = (props) => {

  let fadeAnim = useRef(new Animated.Value(0)).current

  const {
    children,
    onConfirm,
    onCancel,
    isConfirmation,
    options,
    cancelText,
    confirmText
  } = props

  useEffect(() => {
    fadeAnim.setValue(0)
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 150,
      delay: 100,
      useNativeDriver: true
    }).start()
  }, [])

  const renderOptions = () => {
    return options.map(({
      text,
      enabled,
      color,
      onPress
    }) => (<TouchableOpacity
      key={Math.random()}
      style={style.option}
      onPress={enabled ? onPress : null}
      activeOpacity={enabled ? 0.8 : 1}
      enabled={enabled}
           >
      <Text color={color} centered size={'mini'} weight={500}>{text}</Text>
    </TouchableOpacity>))
  }

  return (
    <View style={style.overlay}>
      <Animated.View style={{ ...style.popup, width: props.width, height: props.height, top: (height / 2) - (props.height / 2), left: (width / 2) - (props.width / 2), opacity: fadeAnim }}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          {children}
        </View>
        {isConfirmation && (<View style={{ flexDirection: 'row' }}>
          <TouchableOpacity activeOpacity={0.8} onPress={onCancel} style={{ ...style.button, borderBottomLeftRadius: 15 }}>
            <Text centered size={'mini'} weight={500}>{cancelText || 'Cancel'}</Text>
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.8} onPress={onConfirm} style={{ ...style.button, borderBottomRightRadius: 15, backgroundColor: TOMATO_CALM }}>
            <Text color={WHITE} centered size={'mini'} weight={500}>{confirmText || 'Yes'}</Text>
          </TouchableOpacity>
        </View>)}
        {renderOptions()}
      </Animated.View>
    </View>
  )

}

Dialog.defaultProps = {
  cancelText: 'Cancel',
  confirmText: 'Yes',
  onConfirm: null,
  onCancel: null,
  width: 280,
  height: 160,
  isConfirmation: true,
  options: []
}

Dialog.propTypes = {
  onConfirm: PropTypes.func,
  onCancel: PropTypes.func,
  width: PropTypes.number,
  height: PropTypes.number,
  isConfirmation: PropTypes.boolean,
  options: PropTypes.array,
  cancelText: PropTypes.string,
  confirmText: PropTypes.string,
}

export default Dialog