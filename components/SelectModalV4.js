import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, TextInput, ScrollView, SafeAreaView } from 'react-native'
import { isFunction, isEqual, isEmpty, reduce, isArray, includes } from 'lodash-es'
import { connect } from 'react-redux'
import {
  WP1,
  WP2, WP4, HP100, WP3, WP105, WP308, WP40, HP1, HP2, WP20, HP40, HP50
} from '../constants/Sizes'
import {
  GREY,
  WHITE,
  TOMATO,
  SHIP_GREY_CALM,
  REDDISH,
  NAVY_DARK,
  PALE_GREY,
  GUN_METAL,
  PALE_GREY_TWO,
  GREY_SEMI,
  TRANSPARENT
} from '../constants/Colors'
import { TOUCH_OPACITY, BORDER_STYLE } from '../constants/Styles'
import Modal from './Modal'
import Text from './Text'
import Icon from './Icon'
import Button from './Button'

const propsType = {
  triggerComponent: PropTypes.objectOf(PropTypes.any),
  onChange: PropTypes.func,
  renderItem: PropTypes.func,
  reformatFromApi: PropTypes.func,
  asObject: PropTypes.bool,
  placeholder: PropTypes.string,
  refreshOnSelect: PropTypes.bool,
  extraResult: PropTypes.bool,
  iconName: PropTypes.string,
  iconType: PropTypes.string,
  iconSize: PropTypes.string,
  items: PropTypes.array,
  disabled: PropTypes.bool
}

const propsDefault = {
  triggerComponent: {},
  onChange: () => {
  },
  asObject: false,
  createNew: true,
  placeholder: '',
  refreshOnSelect: false,
  extraResult: false,
  iconName: '',
  iconType: 'AntDesign',
  iconSize: 'small',
  items: [],
  disabled: false
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

class SelectModalV4 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      keyword: '',
      inputKey: '',
      modalHeight: HP100
    }
  }

  componentDidMount = () => {

  }

  _onFocus = () => {
    this.setState({ showSuggestion: true })
  }

  _onBlur = () => {
    this.setState({ showSuggestion: false })
  }

  // RENDER LIST
  _suggestionList(items, toggleModal) {
    const { extraResult } = this.props
    const { keyword } = this.state
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps='handled'
        contentContainerStyle={{
          paddingHorizontal: WP4,
          paddingTop: WP4,
        }}
      >
        {!isEmpty(items) &&
          items.map((item, index) => (
            this._renderItem(index, item, toggleModal)
          ))
        }
      </ScrollView>
    )
  }

  _renderItem = (index, item, toggleModal) => {
    const { iconName, iconType, iconSize } = this.props
    return (
      <TouchableOpacity
        key={index}
        style={{
          paddingVertical: WP2,
          paddingHorizontal: WP1,
          borderBottomWidth: 1,
          borderBottomColor: PALE_GREY
        }}
        onPress={() => {
          this.props.onChange(item)
          toggleModal()
        }}
      >
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between'
          }}
        >
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            {!isEmpty(iconName) && (<Icon style={{ marginRight: WP3 }} centered color={this._isSelected(item) ? PALE_GREY : GUN_METAL} type={iconType} name={iconName} size={iconSize} />)}
            {
              this._renderItemText(item)
            }
          </View>
        </View>
      </TouchableOpacity>)
  }

  _renderItemText = (item) => {
    const { renderItem } = this.props
    return (
      <Text
        size='mini' type='Circular' color={GUN_METAL}
        weight={300}
      >{item}</Text>)
  }

  _isSelected = (item) => {
    return item
  }

  // END RENDER LIST

  render() {
    const {
      triggerComponent,
      suggestionPathResult,
      placeholder,
      header,
      items,
      disabled
    } = this.props

    const {
      keyword,
      suggestions,
      suggestionsRecent,
      showSuggestion
    } = this.state
    return (
      <Modal
        style={{
          borderTopRightRadius: 0,
          borderTopLeftRadius: 0,
          backgroundColor: TRANSPARENT
        }}
        renderModalContent={({ toggleModal }) => (
          <SafeAreaView style={{ backgroundColor: TRANSPARENT, }}>
            <View style={{
              backgroundColor: PALE_GREY_TWO,
              borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
              height: HP50
            }}
            >
              <View style={{
                backgroundColor: GREY_SEMI,
                width: WP20,
                borderRadius: 10,
                height: 5,
                marginTop: 10,
                alignSelf: 'center'
              }}
              />
              <View style={{
                paddingHorizontal: WP4,
                paddingTop: HP1,
                paddingBottom: HP2,
                ...BORDER_STYLE['bottom']
              }}
              >
                <Text centered type='SansPro' weight={500}>{header}</Text>
              </View>
              {/*list item*/}
              {
                this._suggestionList(items, toggleModal)
              }
            </View>
          </SafeAreaView>
        )}
      >
        {({ toggleModal, isVisible }, M) => (
          <View style={{ flexGrow: 1 }}>
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY} onPress={() => {
                 !disabled && toggleModal()
              }}
            >
              <View pointerEvents='none'>
                {triggerComponent}
              </View>
            </TouchableOpacity>
            {M}
          </View>
        )}
      </Modal>
    )
  }
}

SelectModalV4.propTypes = propsType

SelectModalV4.defaultProps = propsDefault

export default connect(mapStateToProps, {})(SelectModalV4)
