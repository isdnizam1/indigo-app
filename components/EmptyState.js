/* eslint-disable react/sort-comp */
import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
// import PropTypes from "prop-types";
import { HP100, WP10, WP100, WP162, WP4, WP80, HP50, HP3, HP1 } from "sf-constants/Sizes";
import { _enhancedNavigation, Image, Text } from "sf-components";

// const propsType = {
//   image: PropTypes.any,
//   title: PropTypes.string,
//   message: PropTypes.string
// };

const EmptyState = (props) => {
  const {
    image,
    title,
    message
  } = props;

  return (
    <View
      style={{
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
      }}
    >
      <Image
        size={"huge"}
        source={image}
        style={{ paddingBottom: HP3 * 0.9 }}
      />
      <Text
        weight={400}
        color={"#454f5b"}
        size={"medium"}
        style={{ paddingBottom: HP1 * 0.6 }}
      >
        {title}
      </Text>
      <Text
        weight={400}
        color={"#919eab"}
        size={"slight"}
        style={{
          textAlign: "center",
          width: WP100 * 0.85,
          lineHeight: HP3 * 0.9,
        }}
      >
        {message}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({})

// EmptyState.propTypes = propsType;
export default EmptyState;
