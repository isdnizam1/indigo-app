import React, { Component } from 'react'
import { View, StyleSheet, Animated } from 'react-native'
import { get } from 'lodash-es'
import Text from 'sf-components/Text'
import Touchable from 'sf-components/Touchable'
import {
  REDDISH,
  SHIP_GREY_CALM,
  SHADOW_GRADIENT,
  PALE_BLUE,
} from 'sf-constants/Colors'
import { WP305, WP100 } from 'sf-constants/Sizes'
import { HEADER } from 'sf-constants/Styles'
import { LinearGradient } from 'expo-linear-gradient'

const style = StyleSheet.create({
  tabs: {
    flexDirection: 'row',
  },
  tab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: WP305,
    borderBottomWidth: 1,
    borderBottomColor: PALE_BLUE,
  },
})

class CustomTabController extends Component {
  constructor(props) {
    super(props)
    this.state = {
      borderPosition: new Animated.Value(0),
    }
  }

  static getDerivedStateFromProps(props, state) {
    Animated.timing(state.borderPosition, {
      duration: 100,
      toValue: (WP100 / props.routes.length) * props.activeIndex,
      useNativeDriver: false
    }).start()
    return null
  }

  _renderTab = (tab, index) => {
    const { onPress, activeIndex } = this.props
    return (
      <Touchable
        onPress={() => onPress(index)}
        style={style.tab}
        key={`custom-tab-bar-${index}`}
      >
        <Text
          type='Circular'
          weight={600}
          centered
          size='xmini'
          color={index == activeIndex ? REDDISH : SHIP_GREY_CALM}
        >
          {get(tab, 'title') || get(tab, 'text')}
        </Text>
      </Touchable>
    )
  };

  render() {
    const { routes } = this.props
    const { borderPosition } = this.state
    const tabWidth = WP100 / routes.length
    return (
      <View>
        <View style={style.tabs}>
          {routes.map(this._renderTab)}
          <Animated.View
            style={{
              width: tabWidth,
              backgroundColor: REDDISH,
              position: 'absolute',
              height: 2,
              bottom: 0,
              left: borderPosition,
            }}
          />
        </View>
        <LinearGradient colors={SHADOW_GRADIENT} style={HEADER.shadow} />
      </View>
    )
  }
}

export default CustomTabController
