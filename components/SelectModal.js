import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, TextInput, ScrollView, SafeAreaView, Image } from 'react-native'
import { isFunction, isEqual, isEmpty, reduce, isArray, includes } from 'lodash-es'
import { connect } from 'react-redux'
import {
  HP1,
  WP1,
  WP2, WP4, HP100, WP6, WP3, WP308, WP205
} from '../constants/Sizes'
import {
  GREY,
  WHITE,
  GREY_CALM_SEMI,
  GREY_DARK90,
  TOMATO,
  GREY_PLACEHOLDER
} from '../constants/Colors'
import { TOUCH_OPACITY } from '../constants/Styles'
import Modal from './Modal'
import Text from './Text'
import Icon from './Icon'
import HeaderNormal from './HeaderNormal'

const propsType = {
  triggerComponent: PropTypes.objectOf(PropTypes.any),
  keyword: PropTypes.string,
  onChange: PropTypes.func,
  header: PropTypes.string,
  suggestion: PropTypes.func,
  renderItem: PropTypes.func,
  reformatFromApi: PropTypes.func,
  suggestionKey: PropTypes.string,
  suggestionPathResult: PropTypes.string,
  suggestionPathValue: PropTypes.string,
  asObject: PropTypes.bool,
  placeholder: PropTypes.string,
  refreshOnSelect: PropTypes.bool,
  extraResult: PropTypes.bool,
  iconName: PropTypes.string,
  iconType: PropTypes.string,
  iconSize: PropTypes.string,
}

const propsDefault = {
  keyword: '',
  triggerComponent: {},
  onChange: () => {
  },
  header: '',
  suggestion: () => {
  },
  suggestionKey: '',
  suggestionPathResult: '',
  suggestionPathValue: '',
  asObject: false,
  createNew: true,
  placeholder: '',
  refreshOnSelect: false,
  extraResult: false,
  iconName: 'search1',
  iconType: 'AntDesign',
  iconSize: 'small',
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

class SelectModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      keyword: '',
      inputKey: '',
      suggestions: [],
      suggestionsRecent: [],
      showSuggestion: false,
      modalHeight: HP100
    }
    this.textInput = React.createRef()
    this._onShow = this._onShow.bind(this)
  }

  componentDidMount = () => {
    this._onFetchSuggestion()
  }

  _onFocus = () => {
    // this.setState({ showSuggestion: true }, this._onFetchSuggestion)
  }

  _onBlur = () => {
    this.setState({ showSuggestion: false })
  }

  _onChangeText = (text) => {
    this.setState({ keyword: text }, this._onFetchSuggestion)
  }

  _onSelectSuggestion = (value, closeModalCallback) => {
    const {
      onChange, suggestionPathResult, asObject, refreshOnSelect
    } = this.props
    if (asObject) {
      if (value[suggestionPathResult] !== this.props.selected) {
        if (!refreshOnSelect) this.setState({ keyword: value[suggestionPathResult] })
        onChange(value)
      } else {
        this.setState({ keyword: '' })
        onChange({})
      }
    } else {
      if (value[suggestionPathResult] !== this.props.selected) {
        if (!refreshOnSelect) this.setState({ keyword: value[suggestionPathResult] })
        else this.setState({ keyword: '' })
        onChange(value[suggestionPathResult])
      } else {
        this.setState({ keyword: '' })
        onChange('')
      }
    }
    closeModalCallback()
  }

  _onCreateNewOption = (closeModalCallback) => {
    const { keyword } = this.state
    const { onChange } = this.props

    onChange(keyword)
    closeModalCallback()
  }

  _onClearInput = () => {
    // this._onFocus()
    this.setState({ suggestions: [] })
  }

  _onFetchSuggestion = () => {
    const { suggestion, suggestionKey, userData: { id_user }, suggestionPathResult, extraResult, reformatFromApi, suggestionPathValue } = this.props
    const { keyword } = this.state
    if (isFunction(suggestion)) {
      try {
        typeof this.timeout !== 'undefined' && clearTimeout(this.timeout)
      } finally {
        this.timeout = setTimeout(() => {
          suggestion({
            [suggestionKey]: keyword,
            id_user,
            limit: 6,
            start: 0
          }).then(({ data }) => {
            let suggestionData = data.result || []
            if (reformatFromApi) {
              suggestionData = reduce(suggestionData, (result, item) => {
                result.push({
                  [suggestionPathResult]: reformatFromApi(item[suggestionPathResult]),
                  [suggestionPathValue]: item[suggestionPathValue],
                })
                return result
              }, [])
            }
            let suggestionDataRecentlySearch = []
            if (extraResult) {
              suggestionDataRecentlySearch = data.recentlySearch || []
              if (!isEmpty(suggestionDataRecentlySearch)) {
                suggestionDataRecentlySearch = reduce(data.recentlySearch, (result, item) => {
                  result.push({
                    [suggestionPathResult]: reformatFromApi ? reformatFromApi(item.value) : item.value,
                    [suggestionPathValue]: item[suggestionPathValue],
                  })
                  return result
                }, [])
              }
            }
            this.setState({
              suggestions: suggestionData,
              suggestionsRecent: suggestionDataRecentlySearch,
              showSuggestion: true
            })
          }).catch(() => {

          })
        }, 500)
      }
    }
  }

  _isSelected = (item) => {
    const {
      selected,
      suggestionPathResult
    } = this.props

    if (isArray(selected)) return includes(selected, item[suggestionPathResult])
    else return isEqual(selected, item[suggestionPathResult])
  }

  // RENDER LIST
  _suggestionList(suggestions, toggleModal, suggestionPathResult, suggestionsRecent) {
    const { recentSearch } = this.props
    const { keyword } = this.state
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps='handled'
        contentContainerStyle={{
          paddingHorizontal: WP6,
          paddingVertical: HP1
        }}
      >
        {
          // !isEmpty(suggestionsRecent) && isEmpty(keyword) && extraResult && (
          //   <Text type='NeoSans' size='mini' weight={500}>Recent Searches</Text>
          // )
        }
        {
          // !isEmpty(suggestionsRecent) && isEmpty(keyword) && extraResult && (
          //   <View style={{ marginTop: WP2, marginBottom: WP3 }}>
          //     {suggestionsRecent.map((item, index) => (
          //       this._renderItem(index, item, toggleModal, suggestionPathResult)
          //     ))}
          //   </View>
          // )
        }
        {
          isEmpty(keyword) && !isEmpty(recentSearch) && (
            <View style={{ paddingBottom: WP2 }}>
              <Text type='NeoSans' weight={500}>Recent</Text>
            </View>
          )
        }
        {
          (isEmpty(keyword) ? (recentSearch || []) : suggestions).map((item, index) => (
            this._renderItem(index, item, toggleModal, suggestionPathResult)
          ))
        }
        {
          !isEmpty(keyword) && this._createNewOption(toggleModal)
        }
      </ScrollView>
    )
  }

  _renderItem = (index, item, toggleModal, suggestionPathResult) => {
    const { iconName, iconType, iconSize } = this.props
    return !isEmpty(item[suggestionPathResult]) ? (
      <TouchableOpacity
        key={index}
        style={{
          paddingVertical: WP2,
          paddingHorizontal: WP1
        }}
        onPress={() => {
          this._onSelectSuggestion(item, toggleModal)
        }}
      >
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between'
          }}
        >
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <Icon style={{ marginRight: WP3 }} centered color={this._isSelected(item) ? GREY : GREY_CALM_SEMI} type={iconType} name={iconName} size={iconSize} />
            {
              this._renderItemText(item, suggestionPathResult)
            }
          </View>
          {
            this._isSelected(item) && <Icon type='MaterialCommunityIcons' name='check' size='large' />
          }
        </View>
      </TouchableOpacity>) : null
  }

  _renderItemText = (item, suggestionPathResult) => {
    const { renderItem } = this.props
    if (renderItem) {
      const itemEl = renderItem(item[suggestionPathResult])
      if (React.isValidElement(itemEl)) return itemEl
      else return <Text size='mini' color={GREY} weight={this._isSelected(item) ? 400 : 300}>{itemEl}</Text>
    } else return (
      <Text
        size='mini' color={GREY}
        weight={this._isSelected(item) ? 400 : 300}
      >{item[suggestionPathResult]}</Text>)
  }

  _createNewOption(toggleModal) {
    const { createNew, createNewText } = this.props
    const { keyword, suggestions } = this.state

    if (!createNew && isEmpty(suggestions)) {
      return (
        <View style={{ paddingTop: WP3, paddingHorizontal: WP2 }}>
          <Text centered size='slight' weight={500}>{`Sorry, no results found for “${keyword}”`}</Text>
        </View>
      )
    }

    if (createNew && isEmpty(suggestions)) {
      return (
        <View style={{ paddingTop: WP3 }}>
          <Text size='mini' color={GREY}>{keyword}</Text>
          <View style={{ flexDirection: 'row', marginTop: WP4 }}>
            <TouchableOpacity
              onPress={() => {
                this._onCreateNewOption(toggleModal)
              }}
              backgroundColor={TOMATO}
              nativeEffect rounded centered
              compact='flex-end'
              shadow='none'
              textColor={WHITE}
              textSize='tiny'
              textType='NeoSans'
              textWeight={500}
              style={{ backgroundColor: GREY_DARK90, height: 38, borderRadius: 19, paddingHorizontal: WP4, justifyContent: 'center' }}
            >
              <Text
                size={'xmini'}
                type={'NeoSans'}
                weight={500}
                color={WHITE}
              >{createNewText || 'Create New'}</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
  }
  // END RENDER LIST

  _onShow = () => {
    setTimeout(() => this.textInput.focus(), 1000)
  }

  render() {
    const {
      triggerComponent,
      header,
      suggestionPathResult,
      placeholder
    } = this.props

    const {
      keyword,
      suggestions,
      suggestionsRecent,
      showSuggestion
    } = this.state
    return (
      <Modal
        onShow={this._onShow}
        style={{
          flex: 1,
          borderTopRightRadius: 0,
          borderTopLeftRadius: 0,
        }}
        renderModalContent={({ toggleModal }) => (
          <SafeAreaView style={{ flex: 1, }}>
            <View style={{
              backgroundColor: WHITE,
              height: '100%'
            }}
            >
              <HeaderNormal
                iconLeftOnPress={toggleModal}
                text={header}
                centered
                style={{ paddingHorizontal: WP6 }}
              />

              {/*search input*/}
              <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: WP2,
                paddingHorizontal: WP4
              }}
              >
                <View style={{
                  backgroundColor: WHITE,
                  borderRadius: 100,
                  flexDirection: 'row',
                  elevation: 6,
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingHorizontal: WP3,
                  borderColor: GREY,
                  borderWidth: 1,
                }}
                >
                  {/*<Icon centered color={GREY} name='search1' size='small' />*/}
                  <Image style={{ width: 14, height: 14, marginHorizontal: WP1 }} source={require('../assets/icons/newMagnifierGlass.png')} />
                  <TextInput
                    returnKeyType='search'
                    value={keyword}
                    ref={(ref) => { this.textInput = ref }}
                    numberOfLines={1}
                    style={{
                      flex: 1,
                      fontFamily: 'OpenSansRegular',
                      fontSize: WP308,
                      marginLeft: WP205,
                      marginRight: WP2,
                      paddingVertical: WP1,
                      color: GREY
                    }}
                    placeholderTextColor={GREY_PLACEHOLDER}
                    onChangeText={this._onChangeText}
                    onFocus={this._onFocus}
                    placeholder={placeholder}
                  />
                  {
                    !isEmpty(keyword) && (
                      <TouchableOpacity onPress={() => this.setState({ keyword: '' }, this._onClearInput)}>
                        <Icon centered color={GREY} type='MaterialCommunityIcons' name='close-circle' size='large' />
                      </TouchableOpacity>
                    )
                  }
                </View>
              </View>

              {/*Suggestion*/}
              {
                showSuggestion && this._suggestionList(suggestions, toggleModal, suggestionPathResult, suggestionsRecent)
              }
            </View>
          </SafeAreaView>
        )}
      >
        {({ toggleModal, isVisible }, M) => (
          <View>
            <TouchableOpacity
              activeOpacity={this.props.disabled ? 1 : TOUCH_OPACITY} onPress={() => {
                if(!this.props.disabled) {
                  if (!isVisible) this._onFetchSuggestion()
                  toggleModal()
                }
              }}
            >
              <View pointerEvents='none'>
                {triggerComponent}
              </View>
            </TouchableOpacity>
            {M}
          </View>
        )}
      </Modal>
    )
  }
}

SelectModal.propTypes = propsType

SelectModal.defaultProps = propsDefault

export default connect(mapStateToProps, {})(SelectModal)
