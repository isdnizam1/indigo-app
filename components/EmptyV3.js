import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import * as Updates from 'expo-updates'
import { HP30, WP1, WP5, WP60, WP65 } from '../constants/Sizes'
import { GUN_METAL, PALE_WHITE, SHIP_GREY_CALM } from '../constants/Colors'
import { HP1 } from '../constants/Sizes'
import { WP10 } from '../constants/Sizes'
import { HP2 } from '../constants/Sizes'
import Text from './Text'
import Image from './Image'
import Button from './Button'

const propsType = {
  image: PropTypes.any,
  icon: PropTypes.any,
  imageHeight: PropTypes.number,
  imageWidth: PropTypes.number,
  backgroundColor: PropTypes.string,
  title: PropTypes.string,
  full: PropTypes.bool,
  message: PropTypes.any.isRequired,
  actions: PropTypes.any,
  style: PropTypes.object,
  aspectRatio: PropTypes.number
}

const propsDefault = {
  image: require('../assets/images/ill1.jpg'),
  message: 'This is default message',
  backgroundColor: PALE_WHITE,
  imageHeight: HP30,
  imageWidth: WP60,
  full: true,
  aspectRatio: 213 / 175
}

const EmptyV3 = (props) => {
  const {
    image: propImage,
    icon,
    full,
    hideTitle,
    hideMessage,
    imageHeight,
    imageWidth,
    title: propTitle,
    message: propMessage,
    actions: propActions,
    style,
    backgroundColor,
    aspectRatio
  } = props
  let title = propTitle
  let message = propMessage
  let actions = propActions
  let image = propImage
  if (props.error) {
    title = 'Sorry'
    message = 'We do apologize, please give us a time to fix this issue'
    image = require('../assets/images/illNotConnected.png')
    actions = (<Button
      onPress={() => Updates.reloadAsync()}
      rounded
      centered
      soundfren
      width={WP65}
      text='Reload'
      textWeight={300}
      shadow='none'
               />)
  }
  return (
    <View
      style={[
        {
          justifyContent: "center",
          flexGrow: full ? 1 : 0,
          paddingVertical: full ? 0 : HP2,
          alignItems: "center",
          backgroundColor,
          paddingHorizontal: WP10,
        },
        style,
      ]}
    >
      {React.isValidElement(icon) ? (
        icon
      ) : (
        <Image
          source={image}
          style={{ marginVertical: HP1 }}
          imageStyle={{
            width: imageWidth,
            height: imageWidth ? undefined : imageHeight,
            aspectRatio,
          }}
        />
      )}
      <View style={{ paddingVertical: HP1, alignItems: "center" }}>
        {!hideTitle ? (
          <Text size="medium" type="Circular" color={GUN_METAL} weight={500}>
            {title}
          </Text>
        ) : null}
        <View style={{ height: WP1 }} />
        {!hideMessage ? (
          <View>
            {React.isValidElement(message) ? (
              message
            ) : (
              <Text
                lineHeight={WP5}
                type="Circular"
                size="slight"
                color={SHIP_GREY_CALM}
                centered
              >
                {message}
              </Text>
            )}
          </View>
        ) : null}
      </View>
      {React.isValidElement(actions) && actions}
    </View>
  );
}

EmptyV3.propTypes = propsType
EmptyV3.defaultProps = propsDefault
export default EmptyV3
