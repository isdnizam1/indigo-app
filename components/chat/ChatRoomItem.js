import React from "react";
import { isEmpty, isFunction } from "lodash-es";
import { TouchableOpacity, View } from "react-native";
import moment from "moment";
import {
  GUN_METAL,
  PALE_GREY,
  REDDISH,
  SHIP_GREY_CALM,
  SILVER_GREY,
  WHITE,
} from "../../constants/Colors";

import { TOUCH_OPACITY } from "../../constants/Styles";
import {
  WP05,
  WP1,
  WP2,
  WP3,
  WP4,
  WP5,
  WP55,
  WP6,
} from "../../constants/Sizes";
import Avatar from "../Avatar";
import Text from "../Text";

import env from "../../utils/env";

const propsType = {};

const propsDefault = {};

const ChatRoomItem = (props) => {
  const {
    user,
    userData,
    onPressItem,
    onLongPress,

    timeStamp,
    imageSize = "small",
    style,
  } = props;
  const avatar = `${env.apiUrl}assets/img/default-chatgroup-avatar.png`;
  const name = user.title !== "" ? user.title : user.full_name;

  if (user.message !== null) {
    if (user.message.includes("http") == true) {
      var subtitle;
      user.id_user_sender === userData.id_user
        ? (subtitle = "You send a attachment")
        : (subtitle = user.full_name_sender + " send a attachment");
    } else if (user.message_type === "notification") {
      var subtitle;
      user.id_user_sender === userData.id_user
        ? (subtitle = user.message)
        : (subtitle = user.message.replace("You", `${user.full_name_sender}`));
    } else {
      var subtitle = user.message;
    }
  } else {
    var subtitle = null;
  }

  const profession = [];
  if (!isEmpty(user?.job_title)) profession.push(user.job_title);
  return (
    <TouchableOpacity
      activeOpacity={TOUCH_OPACITY}
      onPress={onPressItem}
      onLongPress={onLongPress}
      disabled={!isFunction(onPressItem)}
      style={{
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: WP4,
        marginHorizontal: WP4,
        borderBottomColor: PALE_GREY,
        borderBottomWidth: 1,
        flex: 1,
        ...style,
      }}
    >
      <View style={{ marginRight: WP3 }}>
        <Avatar
          onPress={onPressItem}
          image={
            user.image !== null
              ? user.image
              : user.type === "group"
              ? avatar
              : user.avatar
          }
          verifiedStatus={user.verified_status}
          size={imageSize}
          isGroup={user.type === "group" ? true : false}
        />
      </View>
      <View style={{ flex: 1, paddingRight: WP2 }}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          <Text
            centered={false}
            type="Circular"
            size="mini"
            weight={500}
            color={GUN_METAL}
            user={user}
            style={{ lineHeight: WP6, marginBottom: WP05 }}
          >
            {name}
          </Text>
        </View>
        <Text
          numberOfLines={1}
          color={SHIP_GREY_CALM}
          type={"Circular"}
          size="xmini"
          weight={300}
        >
          {subtitle}
        </Text>
      </View>
      <View>
        <Text
          size="tiny"
          style={{
            alignSelf: "flex-start",
            color: SILVER_GREY,
          }}
        >
          {moment(timeStamp).format("HH:mm")}
        </Text>
        {user.unread_count !== null ? (
          <View
            style={{
              marginTop: WP1,
              backgroundColor: REDDISH,
              borderRadius: WP55,
              // borderWidth: 2,
              // borderColor: SILVER_GREY,
              alignSelf: "flex-end",
              justifyContent: "center",
              alignItems: "flex-end",
              width: WP5,
              height: WP5,
            }}
          >
            <Text
              size="tiny"
              style={{
                alignSelf: "center",
                color: WHITE,
              }}
            >
              {user.unread_count !== null ? user.unread_count : null}
            </Text>
          </View>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

ChatRoomItem.propTypes = propsType;
ChatRoomItem.defaultProps = propsDefault;
export default ChatRoomItem;
