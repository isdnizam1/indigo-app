import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import { HP105, HP2, WP2, WP3, WP4, WP6 } from '../../constants/Sizes'
import { NO_COLOR, WHITE } from '../../constants/Colors'
import Modal from '../Modal'
import Text from '../Text'
import Image from '../Image'

const GROUP_ICON = require('../../assets/icons/buttonGroup1.png')
const PERSONAL_ICON = require('../../assets/icons/buttonPersonal1.png')

const styles = {
  modal: {
    marginHorizontal: WP2,
    marginBottom: HP105
  },
  actionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginVertical: HP2
  },
  buttonAdd: {
    position: 'absolute',
    right: WP4,
    bottom: HP2
  }
}

class NewChatButton extends Component {
  _toggleButton = (toggleModal) => {
    toggleModal()
  }

  _newChatHandler = (isGroupChat) => {
    const screen = isGroupChat ? 'NewGroupChatScreen' : 'NewChatScreen'
    this.props.navigateTo(screen, { onRefresh: this.props.onRefresh, isGroupChat })
  }

  _renderActionButton = (label, image, isGroupChat, toggleModal) => {
    return (
      <TouchableOpacity
        style={styles.actionButton}
        onPress={() => {
          this._toggleButton(toggleModal)
          this._newChatHandler(isGroupChat)
        }}
        activeOpacity={1}
      >
        <Text weight={500} color={WHITE}>{label}</Text>
        <Image
          source={image}
          aspectRatio={1}
          imageStyle={{
            width: 50,
            height: 50,
            marginLeft: WP3,
            marginRight: WP2
          }}
        />
      </TouchableOpacity>
    )
  }

  _renderModalContent = ({ toggleModal }) => (
    <TouchableOpacity
      style={{
        alignItems: 'flex-end',
        paddingRight: WP2,
        paddingBottom: WP6
      }}
      onPress={toggleModal}
    >
      {this._renderActionButton('Add personal message', PERSONAL_ICON, false, toggleModal)}
      {this._renderActionButton('Add group message', GROUP_ICON, true, toggleModal)}
      <TouchableOpacity
        style={{ marginTop: WP3 }}
        onPress={toggleModal}
      >
        <Image
          source={require('../../assets/icons/newCloseTomato.png')}
          aspectRatio={1}
          imageStyle={{
            width: 70,
            height: 70,
          }}
        />
      </TouchableOpacity>
    </TouchableOpacity>
  )

  render() {
    const { style } = this.props
    return (
      <View style={{ style }}>
        <Modal
          position='bottom'
          closeOnBackdrop={false}
          renderModalContent={this._renderModalContent}
          modalStyle={styles.modal}
          backgroundColor={NO_COLOR}
          swipeDirection={null}
        >
          {({ toggleModal, isVisible }, M) => (
            <View>
              {
                !isVisible && (
                  <TouchableOpacity
                    style={{
                      position: 'absolute',
                      right: WP4,
                      bottom: WP6
                    }}
                    onPress={() => {
                      this._toggleButton(toggleModal)
                    }}
                  >
                    <Image
                      source={require('../../assets/icons/newAddTomato.png')}
                      aspectRatio={1}
                      onPress={() => {
                        this._toggleButton(toggleModal)
                      }}
                      imageStyle={{
                        width: 70,
                        height: 70,
                      }}
                    />
                  </TouchableOpacity>
                )
              }
              {M}
            </View>
          )}
        </Modal>
      </View>
    )
  }
}

NewChatButton.propTypes = {}

NewChatButton.defaultProps = {}

export default NewChatButton
