import { REDDISH } from "../../constants/Colors";
import { WP10 } from "../../constants/Sizes";

export const chatRoomOption = (props) => ({
  menus: [
    {
      type: "menu",
      image: require("../../assets/icons/v3/icPicture.png"),
      size: "xsmall",
      imageStyle: {},
      name: "Choose from library",
      desc: "max. 10mb",
      onPress: () => {
        props.onSelectPhoto().then((result) => {
          props.onClose();
          props.onUploadPhoto(result);
        });
      },
      joinButton: false
    },
    {
      type: "menu",
      image: require("../../assets/icons/v3/icCamera.png"),
      size: "xsmall",
      imageStyle: {},
      name: "Take Photo",
      textStyle: {},
      onPress: () => {
        props.onTakePhoto().then((result) => {
          props.onClose();
          props.onUploadPhoto(result);
        });
      },
    },
    // {
    //   type: "menu",
    //   image: require("../../assets/icons/v3/icAttachmentFile.png"),
    //   imageStyle: {},
    //   size: "xsmall",
    //   name: "Attach .pdf file",
    //   desc: "max. 30mb",
    //   textStyle: {},
    //   onPress: () => {
    //     props.onChooseFile().then((result) => {
    //       props.onClose();
    //       props.onUploadFile(result);
    //     });
    //   },
    // },
    {
      type: "separator",
    },
    {
      type: "menu",
      image: require("../../assets/icons/close.png"),
      size: "xmini",
      imageStyle: {},
      name: "Tutup",
      textStyle: {},
      onPress: props.onClose,
    },
  ],
});

export default chatRoomOption;
