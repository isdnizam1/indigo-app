import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View } from 'react-native'
import { NAVY_DARK, PALE_GREY_TWO } from '../../constants/Colors'
import { toDateHistory } from '../../utils/date'
import Text from '../Text'
import { WP105, WP2, WP3 } from '../../constants/Sizes'

export const chatDateStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 5,
  },
  divider: {
    flexGrow: 1,
    height: 1,
    marginHorizontal: WP3
  },
  dateWrapper: {
    flexGrow: 0,
    paddingHorizontal: WP3,
    paddingVertical: WP105,
    backgroundColor: NAVY_DARK,
    borderRadius: WP2
  },
  date: {
  }
})

const ChatDate = ({ date }) => (
  <View style={chatDateStyle.container}>
    <View style={chatDateStyle.divider} />
    <View style={chatDateStyle.dateWrapper} >
      <Text
        centered size='mini' weight={400} color={PALE_GREY_TWO}
        style={chatDateStyle.date}
      >
        {toDateHistory(date)}
      </Text>
    </View>
    <View style={chatDateStyle.divider} />
  </View>
)

ChatDate.propTypes = {
  date: PropTypes.string
}

ChatDate.defaultProps = {
  date: undefined
}

export default ChatDate
