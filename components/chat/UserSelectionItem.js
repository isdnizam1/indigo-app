import React from 'react'
import PropTypes from 'prop-types'
import { CheckBox } from 'react-native-elements'
import { TouchableOpacity } from 'react-native'
import { noop } from 'lodash-es'
import { WP4, WP7 } from '../../constants/Sizes'
import Image from '../Image'
import FollowItem from '../follow/FollowItem'

const UserSelectionItem = (props) => {
  const {
    selected,
    user,
    onPress
  } = props
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: WP4 }}
    >
      <CheckBox
        checked={selected}
        onPress={onPress}
        containerStyle={{ margin: 0, padding: 0, marginRight: WP4 }}
        checkedIcon={
          <Image
            source={require('../../assets/icons/mdi_check_circle_clear_blue.png')}
            aspectRatio={1}
            imageStyle={{ width: WP7 }}
          />
        }
        uncheckedIcon={
          <Image
            source={require('../../assets/icons/mdi_radio_button_unchecked.png')}
            aspectRatio={1}
            imageStyle={{ width: WP7 }}
          />
        }
      />
      <FollowItem
        imageSize='xsmall'
        isMine={false}
        onPressItem={onPress}
        user={user}
        style={{ marginHorizontal: 0 }}
      />
    </TouchableOpacity>
  )
}

UserSelectionItem.propTypes = {
  selected: PropTypes.bool,
  user: PropTypes.objectOf(PropTypes.any),
  onPress: PropTypes.func
}

UserSelectionItem.defaultProps = {
  selected: false,
  user: {},
  onPress: noop
}

export default UserSelectionItem
