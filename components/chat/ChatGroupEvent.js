import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View } from 'react-native'
import { PALE_BLUE_TWO, SHIP_GREY } from '../../constants/Colors'
import Text from '../Text'
import { WP105, WP2, WP3 } from '../../constants/Sizes'

export const chatDateStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 5,
  },
  divider: {
    flexGrow: 1,
    height: 1,
    marginHorizontal: WP3
  },
  dateWrapper: {
    flexGrow: 0,
    paddingHorizontal: WP3,
    paddingVertical: WP105,
    borderColor: PALE_BLUE_TWO,
    borderWidth: 1,
    borderRadius: WP2
  },
  date: {
  }
})

const ChatGroupEvent = ({ text }) => (
  <View style={chatDateStyle.container}>
    <View style={chatDateStyle.divider} />
    <View style={chatDateStyle.dateWrapper} >
      <Text
        centered size='mini' weight={400} color={SHIP_GREY}
        style={chatDateStyle.date}
      >
        {text}
      </Text>
    </View>
    <View style={chatDateStyle.divider} />
  </View>
)

ChatGroupEvent.propTypes = {
  text: PropTypes.string
}

ChatGroupEvent.defaultProps = {
  text: undefined
}

export default ChatGroupEvent
