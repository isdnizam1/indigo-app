import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Clipboard, StyleSheet, TouchableOpacity, View } from "react-native";
import moment from "moment";
import Hyperlink from "react-native-hyperlink";
import { noop, isString, get, isEmpty } from "lodash-es";
import LinkifyIt from "linkify-it";
import { HP2, WP1, WP2, WP3, WP30, WP75 } from "../../constants/Sizes";
import Text from "../Text";
import Image from '../Image'

import {
  BLUE_CHAT,
  GREY_CHAT,
  PALE_GREY_THREE,
  PALE_GREY_TWO,
  SHIP_GREY,
  SILVER_GREY,
  WHITE,
  WHITE_MILK,
} from "../../constants/Colors";
import Loader from "../Loader";
import ImageAuto from "../ImageAuto";
import { TOUCH_OPACITY } from "../../constants/Styles";
import LinkPreviewCard from "../LinkPreviewCard";
import Modal from "../Modal";
import ListItem from "../ListItem";
import Icon from "../Icon";
import { CHAT_EVENT } from "../../screens/chat/Constants";
import ChatGroupEvent from "./ChatGroupEvent";

export const chatBubbleStyle = StyleSheet.create({
  chatBubble: {
    flexDirection: "row",
    paddingHorizontal: WP2,
    marginVertical: WP1,
  },
  chatContent: {
    maxWidth: WP75,
    paddingVertical: WP2,
    paddingHorizontal: WP3,
    borderRadius: WP2,
  },
  linkContent: {
    width: WP75,
    marginBottom: HP2,
  },
  timestamp: {
    alignSelf: "flex-end",
    marginHorizontal: WP2,
    color: SILVER_GREY,
  },
  left: {
    justifyContent: "flex-start",
  },
  right: {
    justifyContent: "flex-end",
    alignSelf: "flex-end",
  },
});

const onLinkPressed = (text, navigateTo) => {
  navigateTo("BrowserScreenNoTab", { url });
};

class ChatBubble extends Component {
  constructor(props) {
    super(props);
  }

  _loadingMessage = (message) => (
    <View style={[chatBubbleStyle.chatBubble, chatBubbleStyle.right]}>
      <View style={{ alignSelf: "center" }}>
        <View
          style={[
            chatBubbleStyle.chatContent,
            { backgroundColor: WHITE_MILK, padding: WP2 },
          ]}
        >
          <Text size="mini" color={SHIP_GREY} style={{ textAlign: "right" }}>
            {message}
          </Text>
          {/* <Loader size="mini" isLoading /> */}
        </View>
      </View>
    </View>
  );

  _getUrl = (text) => {
    const Linkify = new LinkifyIt();
    const urls = Linkify.match(text);
    const url = get(urls, "[0].url", "");
    return url;
  };
  _ownMessage = ({
    message,
    username,
    avatar,
    timeStamp,
    payload,
    navigateTo,
    onDelete,
    isGroup,
    color,
  }) => (
    <>
      <View style={[chatBubbleStyle.chatBubble, chatBubbleStyle.right]}>
        <Text size="tiny" style={chatBubbleStyle.timestamp}>
          {moment(timeStamp).format("HH:mm")}
        </Text>
        <View style={{ alignSelf: "center" }}>
          {payload.type === "image" && isString(payload.content.url) && (
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onPress={() =>
                navigateTo("ImagePreviewScreen", {
                  images: [{ url: payload.content.url }],
                  withIndicator: false,
                })
              }
            >
              <ImageAuto
                source={{ uri: payload.content.url }}
                style={{
                  width: WP30,
                  marginBottom: WP1,
                  marginHorizontal: WP1,
                }}
              />
            </TouchableOpacity>
          )}
          {message && isEmpty(this._getUrl(message)) ? (
            <Modal renderModalContent={this._renderModalContent}>
              {({ toggleModal }, M) => (
                <Fragment>
                  <TouchableOpacity
                    onLongPress={toggleModal}
                    style={[
                      chatBubbleStyle.chatContent,
                      { backgroundColor: BLUE_CHAT, borderTopRightRadius: 0 },
                    ]}
                  >
                    <Hyperlink
                      onPress={(link) => onLinkPressed(link, navigateTo)}
                      linkStyle={{ textDecorationLine: "underline" }}
                    >
                      <Text
                        size="mini"
                        color={PALE_GREY_TWO}
                        style={{ textAlign: "right" }}
                      >
                        {message}
                      </Text>
           
                    </Hyperlink>
                  </TouchableOpacity>
                  {M}
                </Fragment>
              )}
            </Modal>
          ) : null}
        </View>
      </View>
      {!isEmpty(message) && !isEmpty(this._getUrl(message)) && (
        <View style={[chatBubbleStyle.linkContent, chatBubbleStyle.right]}>
              <Image
          source={{ uri: message}}
          imageStyle={{ height: 200}}
        />
           
          {/* <LinkPreviewCard
            text={message}
            navigateTo={navigateTo}
            containerStyle={[
              chatBubbleStyle.chatContent,
              { backgroundColor: GREY_CHAT, marginTop: 10 },
            ]}
          /> */}
        </View>
      )}
    </>
  );

  _otherMessage = ({
    message,
    username,
    avatar,
    timeStamp,
    navigateTo,
    participant,
    payload,
    isGroup,
    color,
  }) => (
    <>
      <View style={[chatBubbleStyle.chatBubble, chatBubbleStyle.left]}>
        <View style={{ alignSelf: "center" }}>
          {payload.type === "image" && isString(payload.content.url) && (
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onPress={() =>
                navigateTo("ImagePreviewScreen", {
                  images: [{ url: payload.content.url }],
                })
              }
            >
              <ImageAuto
                source={{ uri: payload.content.url || payload.user_avatar_url }}
                style={{
                  width: WP30,
                  marginBottom: WP1,
                  marginHorizontal: WP1,
                }}
              />
            </TouchableOpacity>
          )}
          {message ? (
            <Fragment>
              <View
                style={[
                  chatBubbleStyle.chatContent,
                  { backgroundColor: PALE_GREY_THREE, borderTopLeftRadius: 0 },
                ]}
              >
                {isGroup && (
                  <Text
                    size="mini"
                    weight={500}
                    style={{ marginBottom: 3 }}
                    color={color}
                  >
                    {username}
                  </Text>
                )}
                <Hyperlink
                  onPress={(link) => onLinkPressed(link, navigateTo)}
                  linkStyle={{ textDecorationLine: "underline" }}
                >
                  <Text size="mini" color={SHIP_GREY}>
                    {message}
                  </Text>
                </Hyperlink>
              </View>
            </Fragment>
          ) : null}
        </View>
        <Text size="tiny" style={chatBubbleStyle.timestamp}>
          {moment(timeStamp).format("HH:mm")}
        </Text>
      </View>
      {!isEmpty(message) && !isEmpty(this._getUrl(message)) && (
        <View style={[chatBubbleStyle.linkContent, chatBubbleStyle.left]}>
           <Image
          source={{ uri: message}}
          imageStyle={{ height: 200}}
        />
        </View>
      )}
    </>
  );

  _renderModalContent = ({ toggleModal }) => (
    <View>
      <ListItem
        inline
        onPress={async () => {
          toggleModal();
          await Clipboard.setString(this.props.message);
        }}
      >
        <Icon size="large" type="Entypo" name="copy" />
        <Text style={{ paddingLeft: WP2 }}>Copy</Text>
      </ListItem>
      <ListItem
        inline
        onPress={async () => {
          toggleModal();
          await this.props.onDelete();
        }}
      >
        <Icon size="large" name="delete" />
        <Text style={{ paddingLeft: WP2 }}>Delete</Text>
      </ListItem>
    </View>
  );
  render() {
    const { payload, message, username, ownComment, userData } = this.props;
    if (this.props.isSendingFile) return this._loadingMessage(message);

    if (payload.type === "notification") {
      let result;
      payload.id_user === userData.id_user
        ? (result = payload.message)
        : (result = payload.message.replace("You", `${payload.full_name}`));
      return <ChatGroupEvent text={result} />;
    }

    if (payload.type === CHAT_EVENT.SF_GROUP_EVENT) {
      const name = ownComment ? "Kamu" : username;
      return <ChatGroupEvent text={`${name} ${message}`} />;
    }

    const MessageBubble = ownComment ? this._ownMessage : this._otherMessage;
    return <MessageBubble {...this.props} />;
  }
}

ChatBubble.propTypes = {
  payload: PropTypes.objectOf(PropTypes.any),
  onDelete: PropTypes.func,
};

ChatBubble.defaultProps = {
  payload: {},
  onDelete: noop,
};

export default React.memo(ChatBubble);
