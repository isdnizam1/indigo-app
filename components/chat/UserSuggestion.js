import React from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import { CheckBox } from 'react-native-elements'
import { isFunction } from 'lodash-es'
import PropTypes from 'prop-types'
import { WP2, WP3, WP5, WP6 } from '../../constants/Sizes'
import Avatar from '../Avatar'
import Text from '../Text'
import { GREY } from '../../constants/Colors'

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: WP6,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: WP3,
    flex: 1
  },
  avatar: {
  },
  detail: {
    flexGrow: 1,
    marginLeft: WP5
  },
  name: {
    flex: 1,
    flexWrap: 'wrap'
  },
  location: {

  }
})

const UserSuggestion = ({
  avatar, fullName, city, onPress, disabled = false, selectable, selected, rightComponent, info, label
}) => (
  <TouchableOpacity style={styles.container} onPress={onPress} disabled={disabled}>
    <Avatar image={avatar} imageStyle={styles.avatar}/>
    <View style={styles.detail}>
      <View style={{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', flex: 1, paddingRight: WP2 }}>
        <Text style={styles.name} weight={400} size='slight'>{fullName}</Text>
        { label && label }
      </View>
      { city && <Text style={styles.location} size='tiny' color={GREY}>{city}</Text> }
      { info && info }
    </View>
    {
      selectable && <CheckBox checked={selected} onPress={onPress} />
    }
    {
      isFunction(rightComponent) && rightComponent()
    }
  </TouchableOpacity>
)

UserSuggestion.propTypes = {
  rightComponent: PropTypes.func
}

UserSuggestion.defaultProps = {
  rightComponent: undefined
}

export default UserSuggestion
