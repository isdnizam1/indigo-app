import { WP1, WP10, WP3, WP30, WP405, WP5, WP70 } from '../../constants/Sizes'

const layouts = {
  avatar: {
    width: WP10,
    height: WP10,
    marginRight: WP3,
    borderRadius: WP10/2
  },
  roomName: {
    width: WP30,
    height: WP5,
    marginBottom: WP1
  },
  lastComment: {
    width: WP70,
    height: WP405
  }
}

export default {
  layouts
}
