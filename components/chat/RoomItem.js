import React from 'react'
import { StyleSheet, View } from 'react-native'
import { upperFirst } from 'lodash-es'
import Avatar from '../Avatar'
import { WP05, WP105, WP3, WP4, WP505, WP6 } from '../../constants/Sizes'
import { GUN_METAL, PALE_GREY_THREE, REDDISH, SHIP_GREY_CALM, WHITE } from '../../constants/Colors'
import Text from '../Text'
import { toDateHistory } from '../../utils/date'
import TextName from '../TextName'

const STYLE = StyleSheet.create({
  roomWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginHorizontal: WP4,
    borderBottomWidth: 1,
    borderBottomColor: PALE_GREY_THREE
  },
  avatarContainer: {
    marginRight: WP3
  },
  contentWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: WP4,
    width: '100%',
  },
  timestamp: {
    marginBottom: WP105
  },
  message: {},
  unRead: (count) => ({
    width: count < 100 ? WP505 : WP6 + WP105,
    height: WP505,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: REDDISH,
    borderRadius: WP505 / 2
  }),
  messageWrapper: {
    flex: 1,
  },
  statusWrapper: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    paddingRight: WP05,
    paddingLeft: WP05,
  },
})

const RoomItem = ({ imageSrc, username, message, timestamp, isRead, unReadMessage, messageId, isPremium, isVerified, isGroup }) => (
  <View style={[STYLE.roomWrapper]}>
    <View style={[STYLE.avatarContainer]}>
      <Avatar size='xsmall' image={imageSrc} verifiedSize={WP4} verifiedStatus={isGroup ? false : isVerified} isGroup={isGroup} />
    </View>
    <View style={STYLE.contentWrapper}>
      <View style={STYLE.messageWrapper}>
        <TextName centered={false} type='Circular' size='mini' weight={500} color={GUN_METAL} user={{ full_name: upperFirst(username), account_type: isPremium ? 'premium' : '' }} style={{ lineHeight: WP6, marginBottom: WP05 }} />
        <Text numberOfLines={1} color={SHIP_GREY_CALM} type={'Circular'} size='mini' weight={300}>{message || (messageId ? 'Attachment(s)' : '')}</Text>
      </View>
      <View style={STYLE.statusWrapper}>
        <Text type={'Circular'} size='tiny' weight={300} style={[STYLE.timestamp]} color={isRead ? SHIP_GREY_CALM : REDDISH}>
          {toDateHistory(timestamp, true)}
        </Text>
        {
          !isRead && (
            <View style={STYLE.unRead(unReadMessage)}>
              <Text type={'Circular'} size='tiny' color={WHITE}>{`${unReadMessage}${unReadMessage < 100 ? '' : '+'}`}</Text>
            </View>
          )
        }
      </View>
    </View>
  </View>
)

RoomItem.propTypes = {}

RoomItem.defaultProps = {}

export default RoomItem
