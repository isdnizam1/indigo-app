import React from "react";
import PropTypes from "prop-types";
import { TextInput, Keyboard, TouchableOpacity, View } from "react-native";
import { isEmpty } from "lodash-es";
import {
  PURPLE_DARKER,
  REDDISH,
  REDDISH_DISABLED,
  SILVER_CALMER,
  WHITE,
} from "../../constants/Colors";
import {
  FONT_SIZE,
  WP1,
  WP100,
  WP2,
  WP3,
  WP70,
  WP8,
  WP95,
} from "../../constants/Sizes";
import Form from "../Form";
import Icon from "../Icon";
import { TOUCH_OPACITY } from "../../constants/Styles";
import Modal from "../Modal";
import HorizontalLine from "../HorizontalLine";
import SoundfrenExploreOptions from "../explore/SoundfrenExploreOptions";
import { chatRoomOption } from "./ChatOptionMenus";

const propsType = {
  key: PropTypes.any,
  profilePicture: PropTypes.string,
  template: PropTypes.string,
  onSubmit: PropTypes.func,
  onTakePhoto: PropTypes.func,
  onSelectPhoto: PropTypes.func,
  onUploadPhoto: PropTypes.func,
};

const propsDefault = {
  onSubmit: () => {},
  onTakePhoto: () => {},
  onSelectPhoto: () => {},
  onUploadPhoto: () => {},
  template: "",
};

class ChatCommentForm extends React.Component {
  render() {
    const {
      onSubmit,
      onTakePhoto,
      onSelectPhoto,
      onUploadPhoto,
      onChooseFile,
      onUploadFile,
      template,
    } = this.props;
    return (
      <View
        style={{
          paddingHorizontal: WP2,
          backgroundColor: WHITE,
        }}
      >
        <HorizontalLine style={{ marginLeft: -WP2 }} />
        <Form onSubmit={onSubmit} initialValue={{ comment: template }}>
          {({ onChange, onSubmit, formData }) => (
            <View
              style={{
                flexDirection: "row",
                padding: WP3,
                width: WP95,
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Modal
                renderModalContent={({ toggleModal }) => (
                  <SoundfrenExploreOptions
                    menuOptions={chatRoomOption}
                    onClose={toggleModal}
                    onSelectPhoto={onSelectPhoto}
                    onTakePhoto={onTakePhoto}
                    onUploadPhoto={onUploadPhoto}
                    onChooseFile={onChooseFile}
                    onUploadFile={onUploadFile}
                  />
                )}
              >
                {({ toggleModal }, M) => (
                  <View>
                    <TouchableOpacity activeOpacity={TOUCH_OPACITY}>
                      <Icon
                        onPress={toggleModal}
                        size="huge"
                        color={REDDISH}
                        type="Entypo"
                        name="plus"
                      />
                    </TouchableOpacity>
                    {M}
                  </View>
                )}
              </Modal>
              <View
                style={{
                  flexGrow: 1,
                  marginHorizontal: WP2,
                  paddingVertical: WP1,
                  paddingHorizontal: WP3,
                  flexDirection: "row",
                  justifyContent: "space-between",
                  backgroundColor: SILVER_CALMER,
                  alignItems: "center",
                  borderRadius: WP2,
                  minHeight: WP8,
                  width: WP70,
                }}
              >
                <TextInput
                  ref={(input) => {
                    this.textInput = input;
                  }}
                  multiline
                  style={{
                    fontSize: FONT_SIZE["mini"],
                    paddingTop: 0,
                    color: PURPLE_DARKER,
                    maxHeight: FONT_SIZE["mini"] * 4,
                    flex: 1,
                  }}
                  value={formData.comment}
                  onChangeText={onChange("comment")}
                  placeholder="Type a message"
                />
              </View>
              <Icon
                centered
                background="pale-light-circle"
                style={{ alignSelf: "center", borderRadius: WP100 }}
                color={WHITE}
                backgroundColor={
                  !isEmpty(formData.comment) ? REDDISH : REDDISH_DISABLED
                }
                onPress={async () => {
                  if (!isEmpty(formData.comment)) {
                    try {
                      Keyboard.dismiss();
                    } catch (e) {
                      // just keep silent
                    } finally {
                      onChange("comment")("");
                      onSubmit();
                    }
                  }
                }}
                type="FontAwesome"
                name="send"
              />
            </View>
          )}
        </Form>
      </View>
    );
  }
}

ChatCommentForm.propTypes = propsType;
ChatCommentForm.defaultProps = propsDefault;
export default ChatCommentForm;
