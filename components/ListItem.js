import React from 'react'
import PropTypes from 'prop-types'
import Touchable from 'sf-components/Touchable'
import { View } from 'react-native'
import { WP205, WP305 } from '../constants/Sizes'
import LinkPreviewCard from './LinkPreviewCard'

const propsType = {
  onPress: PropTypes.func,
  centered: PropTypes.bool,
  inline: PropTypes.bool,
  data: PropTypes.bool,
}

const propsDefault = {
  onPress: () => {},
  centered: false,
  inline: false,
}

const ListItem = (props) => {
  const {
    onPress,
    children,
    inline,
    style,
    data,
  } = props;
  return (
    <Touchable
      onPress={onPress}
    >
      <LinkPreviewCard
            text={'ssssss'}
            imageOnly
            imageProps={{
              centered: true,
              resizeMode: 'cover'
            }}
          />
    </Touchable>
  )
}

ListItem.propTypes = propsType

ListItem.defaultProps = propsDefault

export default ListItem
