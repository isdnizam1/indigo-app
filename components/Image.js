import React from 'react'
import PropTypes from 'prop-types'
import { TouchableWithoutFeedback, Image as RNImage, Animated, Easing, View } from 'react-native'
import { isFunction, isObject, isEmpty } from 'lodash-es'
import { IMAGE_SIZE } from '../constants/Sizes'

const propsType = {
  onPress: PropTypes.func,
  centered: PropTypes.bool,
  spinning: PropTypes.bool,
  aspectRatio: PropTypes.number,
  source: PropTypes.any,
  style: PropTypes.any,
  imageStyle: PropTypes.any,
  defaultImage: PropTypes.any,
  size: PropTypes.any
}

const propsDefault = {
  spinning: false,
  size: 'small',
  centered: true,
  aspectRatio: 1,
  source: require('../assets/icons/user.png')
}

class Image extends React.PureComponent {

  constructor() {
    super()
    this.spinValue = new Animated.Value(0)
  }

  componentDidMount() {
    const {
      spinning
    } = this.props
    if(spinning) {
      this.spin()
    }
  }

  spin() {
    this.spinValue.setValue(0)
    Animated.loop(
      Animated.timing(
        this.spinValue,
        {
          toValue: 1,
          duration: 1000,
          easing: Easing.linear,
          useNativeDriver: false
        }
      )
    ).start()
  }

  render() {
    const {
      onPress,
      centered,
      aspectRatio,
      source,
      style,
      size,
      imageStyle,
      spinning
    } = this.props
    const currentSize = IMAGE_SIZE[size] || size
    const validImage = isObject(source) ? !isEmpty(source.uri) ? { ...source, cache: 'force-cache' } : require('../assets/icons/user.png') : source
    const RImage = spinning ? Animated.Image : RNImage

    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })

    return (
      <TouchableWithoutFeedback
        onPress={onPress}
        disabled={!isFunction(onPress)}
      >
        <View style={[{
          alignSelf: centered ? 'center' : 'flex-start',
          justifyContent: 'center'
        }, style]}
        >
          <RImage style={[{ width: currentSize, height: undefined, aspectRatio }, imageStyle, spinning && { transform: [{ rotate: spin }] }]} source={validImage}/>
        </View>
      </TouchableWithoutFeedback>
    )
  }

}

Image.propTypes = propsType

Image.defaultProps = propsDefault

export default React.memo(Image)
