import React from 'react'
import { TouchableOpacity } from 'react-native'
import { REDDISH } from '../../../constants/Colors'
import Icon from '../../Icon'
import Text from '../../Text'

const ButtonIcon = ({ onPress = () => { }, title, color = REDDISH, titleType = 'Circular', titleSize = 'xmini', titleWeight = 400, iconName, iconType = 'MaterialCommunityIcons', iconSize = 'mini', iconPosition = 'left' }) => (
  <TouchableOpacity onPress={onPress} activeOpacity={0.75} style={{ flexDirection: 'row', alignItems: 'center' }}>
    {iconPosition == 'right' ? (<Text type={titleType} size={titleSize} weight={titleWeight} color={color}>{title}</Text>) : null}
    <Icon
      centered
      background='dark-circle'
      size={iconSize}
      color={color}
      name={iconName}
      type={iconType}
    />
    {iconPosition == 'left' ? (<Text type={titleType} size={titleSize} weight={titleWeight} color={color}>{title}</Text>) : null}
  </TouchableOpacity>
)

export default ButtonIcon
