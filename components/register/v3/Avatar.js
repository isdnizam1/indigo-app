import React from 'react'
import { View, Image } from 'react-native'
import { connect } from 'react-redux'
import { registerDispatcher } from 'sf-services/register'
import style from 'sf-styles/register'
import ImageUploadModal from 'sf-components/upload_image/ImageUploadModal'

const cameraIcon = require('sf-assets/icons/ic_camera_clear_blue.png')

const mapStateToProps = ({ auth, register }) => ({
  profile_picture: register.data.profile_picture,
  profile_type: register.data.profile_type,
})

const mapDispatchToProps = {
  setProfilePicture: (base64) =>
    registerDispatcher.setData({ profile_picture: base64 }),
}

class Avatar extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    const { profile_picture, profile_type, setProfilePicture } = this.props
    return (
      <ImageUploadModal
        onChange={async (imageResult) => {
          await setProfilePicture(`data:image/${imageResult.uri.split('.')[imageResult.uri.split('.').length - 1]};base64,${imageResult.base64}`)
        }}
        removable={false}
      >
        <View style={style.avatarWrapper}>
          <Image
            style={style.avatar} source={profile_picture ? { uri: profile_picture } : (
              profile_type === 'group' ? require('sf-assets/images/default_avatar_group.png') : require('sf-assets/images/default_avatar.png')
            )}
          />
          <Image style={style.cameraIcon} source={cameraIcon} />
        </View>
      </ImageUploadModal>
    )
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Avatar)
