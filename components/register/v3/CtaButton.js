import React from 'react'
import { Animated, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import Text from '../../Text'
import style from '../../../styles/register'
import { isTabletOrIpad } from '../../../utils/helper'
import { WHITE } from '../../../constants/Colors'

const mapStateToProps = ({ helper, register }) => ({
  step: register.behaviour.step,
  isKeyboardVisible: helper.keyboard.visible,
  ctaV3MarginBottom: helper.keyboard.ctaV3MarginBottom
})

const mapDispatchToProps = {

}

class CtaButton extends React.PureComponent {

  constructor(props) {
    super(props)
  }

  render() {
    const { enabled, text, onPress } = this.props
    return (<Animated.View style={style.ctaWrapper}>
      <TouchableOpacity
        activeOpacity={0.8}
        disabled={!enabled}
        onPress={onPress}
        style={[style.cta, enabled ? style.ctaEnabled : null, { marginBottom: this.props.step == 4 ? 10 : 0 }]}
      >
        <Text
          centered
          type={'NeoSans'}
          color={WHITE}
          style={style.ctaText}
          weight={500}
          size={isTabletOrIpad() ? 'tiny' : 'medium'}
        >
          {text}
        </Text>
      </TouchableOpacity>
    </Animated.View>)
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(CtaButton)
