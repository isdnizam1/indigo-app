import React from 'react'
import { PALE_LIGHT_BLUE_TWO, SHIP_GREY } from 'sf-constants/Colors'
import { WP1 } from 'sf-constants/Sizes'
import { isObject } from 'lodash-es'
import Text from '../../Text'

const Label = ({ title, size = 'xmini', titleWeight = 400, titleColor = SHIP_GREY, subtitle, subtitleWeight = 300, subtitlecolor = PALE_LIGHT_BLUE_TWO, marginBottom = WP1, lineHeight }) => (
  <Text weight={titleWeight} type='Circular' size={size} color={titleColor} style={{ marginBottom, lineHeight }}>
    {isObject(title) ? title.join(', ') : title}
    {subtitle && (<Text weight={subtitleWeight} type='Circular' size={size} color={subtitlecolor}> {subtitle}</Text>)}
  </Text>
)

export default Label
