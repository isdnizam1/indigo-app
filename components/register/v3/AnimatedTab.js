import React, { useRef, useLayoutEffect } from 'react'
import { Animated, View, Dimensions } from 'react-native'
import Text from '../../Text'
import style from '../../../styles/register'
import { WHITE, GREY } from '../../../constants/Colors'
import { WP8 } from '../../../constants/Sizes'
import { isTabletOrIpad } from '../../../utils/helper'

const tabWidth = (Dimensions.get('window').width - WP8*2) / 2

const AnimatedTab = (props) => {

  const { labels, activeIndex, onTabPress } = props

  const left = useRef(new Animated.Value(0))

  useLayoutEffect(() => {
    const toValue = tabWidth * activeIndex
    left.current.setValue(activeIndex == 1 ? 0 : tabWidth)
    Animated.timing(left.current, {
      toValue,
      duration: 100,
      delay: 50,
      useNativeDriver: false
    }).start()
  }, [activeIndex])

  return (<View key={Math.random()} style={style.tabsWrapper}>
    <Animated.View style={{ ...style.tabIndicator, left: left.current }} />
    {labels.map((label, index) => (<Text
      onPress={() => onTabPress(index)}
      key={Math.random()}
      centered
      color={activeIndex == index ? WHITE : GREY}
      style={style.tab}
      type={'NeoSans'}
      size={isTabletOrIpad() ? 'tiny' : 'mini'}
      weight={500}
                                   >{label}</Text>))}
  </View>)

}

export default AnimatedTab