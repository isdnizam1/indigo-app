import React from 'react'
import { TouchableOpacity, View, StyleSheet } from 'react-native'
import { ORANGE, WHITE, GREY50, GREY_CALM_SEMI } from '../constants/Colors'
import { HP1, WP4, FONT_SIZE } from '../constants/Sizes'
import { Text } from './'

const AlternativeButton = (props) => {

  let { text, size, buttonStyle, textStyle, background, color, disabled, onPress } = props

  return (<TouchableOpacity activeOpacity={disabled ? 1 : 0.8} onPress={disabled ? null : onPress}>
    <View style={[buttonStyles[size || 'tiny'], buttonStyle || {}, {
      backgroundColor: disabled ? GREY50 : (background || ORANGE)
    }]}
    >
      <Text
        weight={500} style={[textStyles[size || 'tiny'], textStyle || {}, {
          color: disabled ? GREY_CALM_SEMI : (color || WHITE)
        }]}
      >{text || 'No Text Yet'}</Text>
    </View>
  </TouchableOpacity>)

}

const buttonStyles = {
  tiny: StyleSheet.create({
    paddingHorizontal: WP4,
    paddingVertical: HP1,
    borderRadius: 9,
    elevation: 2
  })
}

const textStyles = {
  tiny: StyleSheet.create({
    paddingVertical: 2,
    fontSize: FONT_SIZE.tiny
  })
}

export default AlternativeButton