import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { noop } from 'lodash-es'
import { PINK_WHITE, WHITE } from '../constants/Colors'
import { HP2, WP100, WP3, WP4, WP40, WP7 } from '../constants/Sizes'
import { ORANGE } from '../constants/Colors'
import Button from './Button'
import Text from './Text'
import Image from './Image'

const propsType = {
  isActive: PropTypes.bool,
  coachmark: PropTypes.object,
  backgroundStyle: PropTypes.any,
  action: PropTypes.func
}

const propsDefault = {
  isActive: true,
  action: noop
}

const Coachmark = (props) => {
  const {
    isActive,
    coachmark,
    children,
    backgroundStyle,
    action
  } = props
  return (
    isActive ? (
      <View style={[{ zIndex: 999 }, children ? {} : backgroundStyle]}>
        <View style={[{
          position: children ? 'absolute' : 'relative',
          backgroundColor: WHITE,
          borderRadius: children ? WP3 : WP7
        }, children ? backgroundStyle : {}]}
        >
          {children ? null : (
            <View style={{ alignItems: 'center', padding: WP4, justifyContent: 'center' }}>
              {
                coachmark.image
                  ? (<Image
                    source={coachmark.image} style={{ marginVertical: HP2 }} imageStyle={{ height: WP40 }}
                    aspectRatio={1}
                     />)
                  : (<View style={{
                    marginVertical: HP2,
                    height: WP40,
                    width: WP40,
                    backgroundColor: PINK_WHITE,
                    borderRadius: WP100
                  }}
                     />)
              }
              <Text centered size='massive' weight={500} color={ORANGE}>{coachmark.title}</Text>
              <Text centered size='mini'>{coachmark.description}</Text>
              <View style={{ height: HP2 }}/>
              <Button
                style={{ alignSelf: coachmark.actionPosition || 'flex-end' }}
                onPress={action}
                compact='center'
                textSize='small'
                rounded
                centered
                soundfren
                width={WP40}
                text={coachmark.actionText}
                textWeight={300}
                shadow='none'
              />
            </View>
          )}
        </View>
        {
          children
        }
      </View>
    ) : children
  )
}

Coachmark.propTypes = propsType
Coachmark.defaultProps = propsDefault
export default Coachmark
