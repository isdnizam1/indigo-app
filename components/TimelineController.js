import React from 'react'
import { connect } from 'react-redux'
import { isEmpty, isFunction, map, get } from 'lodash-es'
import {
  Animated,
  ScrollView,
  Platform,
  TouchableOpacity,
  View,
} from 'react-native'
import { HP1, HP3, WP100, WP2, WP3, WP4, WP5, WP6 } from 'sf-constants/Sizes'
import {
  GREY_WARM,
  REDDISH,
  WHITE,
  TRANSPARENT,
  NAVY_DARK,
  SHIP_GREY,
  PALE_BLUE_TWO,
  PALE_BLUE,
  GUN_METAL,
} from 'sf-constants/Colors'
import { Text, Button, Image } from 'sf-components'
import { TOUCH_OPACITY } from 'sf-constants/Styles'
import ProgressAnim from 'sf-components/ProgressAnim'
import Touchable from 'sf-components/Touchable'
import produce from 'immer'
import { WP405 } from '../constants/Sizes'
import Icon from './Icon'
import HorizontalLine from './HorizontalLine'

const mapStateToProps = ({ auth, timeline: { scrollDirection } }) => ({
  userData: auth.user,
  scrollDirection,
})

const mapDispatchToProps = {}

class TimelineController extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      expand: false,
      offsets: {},
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const buttonPosition = get(this.state.offsets, this.props.topicId)
    if (buttonPosition) {
      const scrollFunction = get(this.topicScroll, 'scrollTo')
      if (isFunction(scrollFunction)) {
        scrollFunction({ x: buttonPosition - WP4 })
      }
    }
  }

  renderTopicButton = (topic) => {
    const { topicId, onTopicPress } = this.props
    const { id_topic } = topic
    const onLayout = ({
      nativeEvent: {
        layout: { x: horizontalPosition },
      },
    }) => {
      this.setState(
        produce((draft) => {
          draft.offsets[id_topic] = horizontalPosition
        }),
      )
    }
    return (
      <View onLayout={onLayout} key={`topic-${id_topic}`}>
        <Button
          keyIndex={Math.random()}
          onPress={() => onTopicPress(topic)}
          centered
          colors={[NAVY_DARK, NAVY_DARK]}
          compact='center'
          marginless
          toggle
          shadow='none'
          toggleActive={topicId == topic.id_topic}
          toggleActiveColor={WHITE}
          toggleInactiveColor={WHITE}
          toggleInactiveTextColor={SHIP_GREY}
          style={{
            backgroundColor: PALE_BLUE_TWO,
            marginRight: WP2,
            padding: 1,
            borderRadius: 4,
          }}
          radius={4}
          text={topic.topic_name}
          textSize='xmini'
          textWeight={400}
          borderless
        />
      </View>
    )
  };

  render() {
    const {
      index,
      topics,
      onTabPress,
      accountType,
      paymentSourceSet,
      navigateTo,
      newMessageCount,
      is1000Startup = false,
    } = this.props
    const { expand } = this.state
    return (
      <Animated.View style={{ backgroundColor: WHITE, zIndex: 100, width: WP100 }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingRight: WP5,
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            {is1000Startup ? (
              <TouchableOpacity
                onPress={() => this.props.navigateBack()}
                style={{ paddingHorizontal: WP5 }}
              >
                <Icon type='Entypo' name='chevron-left' />
              </TouchableOpacity>
            ) : (
              <View style={{ width: WP5 }} />
            )}
            <TouchableOpacity
              onPress={index == 1 ? () => onTabPress(0) : null}
              activeOpacity={TOUCH_OPACITY}
              style={{
                marginRight: WP5,
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomWidth: 1.5,
                borderBottomColor: index === 0 ? REDDISH : TRANSPARENT,
              }}
            >
              <Text
                color={index == 0 ? REDDISH : GREY_WARM}
                size={'mini'}
                weight={500}
                centered
              >
                COMMUNITY
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={index == 0 ? () => onTabPress(1) : null}
              activeOpacity={TOUCH_OPACITY}
              style={{
                height: 48,
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomWidth: 1.5,
                borderBottomColor: index === 1 ? REDDISH : TRANSPARENT,
              }}
            >
              <Text
                color={index == 1 ? REDDISH : GREY_WARM}
                size={'mini'}
                weight={500}
                centered
              >
                CIRCLE
              </Text>
            </TouchableOpacity>
          </View>

          { !is1000Startup && <View style={{ flexDirection: 'row' }}>
            {/* {Platform.OS == 'android' && (
              <Image
                imageStyle={{
                  width: undefined,
                  height: HP3,
                  aspectRatio: 1,
                  marginRight: WP4,
                }}
                source={
                  accountType === 'free'
                    ? require('sf-assets/icons/badgeSoundfrenFree.png')
                    : require('sf-assets/icons/badgeSoundfrenPremium.png')
                }
                onPress={() => {
                  accountType === 'free' &&
                    paymentSourceSet('timeline') &&
                    navigateTo('MembershipScreen')
                  accountType !== 'free' && navigateTo('MembershipScreen')
                }}
              />
            )} */}
            {/* <Image
              imageStyle={{ width: undefined, height: WP5, aspectRatio: 1 }}
              source={
                newMessageCount > 0
                  ? require('sf-assets/icons/icPaperPlaneWithCircle.png')
                  : require('sf-assets/icons/icPaperPlaneGrey.png')
              }
              style={{ margin: -WP3, padding: WP3 }}
              onPress={() => {
                navigateTo('ChatScreen', { fromScreen: 'TimelineScreen' })
              }}
            /> */}
          </View>}
        </View>
        <HorizontalLine
          width={WP100}
          style={{ alignSelf: 'center' }}
          color={PALE_BLUE}
        />
        {index == 0 && !isEmpty(topics) && !is1000Startup && (
          <View style={{ backgroundColor: WHITE, flexDirection: 'row' }}>
            <View
              style={{
                flex: 1,
                height: 44,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              {expand ? (
                <Text style={{ marginLeft: WP5 }} weight={500} color={GUN_METAL}>
                  Kategori Topik
                </Text>
              ) : (
                <ScrollView
                  scrollEnabled
                  ref={(ref) => (this.topicScroll = ref)}
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  style={{ flex: 1 }}
                  contentContainerStyle={{
                    flexGrow: 1,
                    alignItems: 'center',
                    flexDirection: 'row',
                    height: 44,
                  }}
                >
                  <View style={{ width: WP4 }} />
                  {map(topics, this.renderTopicButton)}
                </ScrollView>
              )}
              <Touchable
                onPress={() => this.setState({ expand: !expand })}
                style={{ padding: WP3 }}
              >
                <Icon
                  centered
                  type='MaterialIcons'
                  name={expand ? 'expand-less' : 'expand-more'}
                  color={SHIP_GREY}
                  size='large'
                />
              </Touchable>
            </View>
            <ProgressAnim isTimeline />
          </View>
        )}
        {!is1000Startup && (
          <HorizontalLine
            width={WP100}
            style={{ alignSelf: 'center' }}
            color={PALE_BLUE}
          />
        )}
        {expand ? (
          <>
            <View
              style={{
                backgroundColor: WHITE,
                flexGrow: 1,
                alignItems: 'center',
                flexDirection: 'row',
                flexWrap: 'wrap',
                paddingVertical: HP1,
                paddingLeft: WP4,
              }}
            >
              {map(topics, (item, index) => (
                <View style={{ marginTop: HP1 }}>
                  {this.renderTopicButton(item, index)}
                </View>
              ))}
            </View>
            <HorizontalLine
              width={WP100}
              style={{ alignSelf: 'center' }}
              color={PALE_BLUE}
            />
          </>
        ) : null}
      </Animated.View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TimelineController)
