import { View, Platform } from 'react-native'
import React from 'react'
import { map } from 'lodash-es'
import { WP100, WP3, WP4, WP6, WP8 } from '../constants/Sizes'
import { GREY, GREY_WARM, PINK_PURPLE, PURPLE, WHITE } from '../constants/Colors'
import { BORDER_COLOR, BORDER_STYLE, BORDER_WIDTH } from '../constants/Styles'
import Text from './Text'
import { LinearGradient } from './index'

const IS_IOS = Platform.OS === 'ios'

const StepItem = ({ index, label, isDone }) => (
  <View style={{ flex: 1 }}>
    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 5 }}>
      <LinearGradient
        rounded
        colors={isDone ? [PURPLE, PINK_PURPLE] : [WHITE, WHITE]}
        start={[0, .3]}
        end={[1, 1.4]}
        locations={[.2, 1]}
        style={{
          justifyContent: 'center', alignItems: 'center',
          borderWidth: isDone ? 0 : BORDER_WIDTH, borderColor: BORDER_COLOR, width: WP8, height: WP8
        }}
      >
        <Text weight={600} size='tiny' color={isDone ? WHITE : GREY}>{index}</Text>
      </LinearGradient>
    </View>
    <Text size='xtiny' color={GREY_WARM} centered>{label}</Text>
  </View>
)

const Steps = ({ steps, currentIndex, style = {} }) => {
  const lineWidth = WP100 - (WP100 / steps.length)
  return (
    <View style={{
      backgroundColor: WHITE,
      paddingVertical: WP3,
      paddingHorizontal: WP6,
      ...style
    }}
    >
      <View style={{
        ...BORDER_STYLE['bottom'],
        position: 'absolute',
        width: lineWidth,
        alignSelf: 'center',
        marginTop: WP3 + WP4
      }}
      />
      <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
        {
          map(steps, (step) => (
            <StepItem index={step.index} label={step.label} isDone={step.index <= currentIndex}/>
          ))
        }
      </View>
    </View>
  )
}

export const ArtistPageSteps = ({ index, isPremium, style }) => {
  const steps = [
    { index: 1, label: 'Artist Data' },
    { index: 2, label: 'Select Songs' },
    { index: 3, label: 'Preview' }
  ]
  if (!isPremium && !IS_IOS) steps.push({ index: 4, label: 'Payment' })
  return <Steps style={style} steps={steps} currentIndex={index}/>
}

export const ExperienceSteps = ({ index, style }) => {
  const steps = [
    { index: 1, label: 'Music/Performance Journey' },
    { index: 2, label: 'Fill in Form' },
    { index: 3, label: 'Finish' }
  ]
  return <Steps style={style} steps={steps} currentIndex={index}/>
}
