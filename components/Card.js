import React from 'react'
import PropTypes from 'prop-types'
import { View, TouchableOpacity } from 'react-native'
import { isFunction } from 'lodash-es'
import { HP05, WP6 } from '../constants/Sizes'
import { WHITE } from '../constants/Colors'
import { BORDER_STYLE } from '../constants/Styles'

const propsType = {
  first: PropTypes.bool,
  backgroundColor: PropTypes.string,
  style: PropTypes.any,
  onLayout: PropTypes.func,
  onPress: PropTypes.func,
  borderBottom: PropTypes.bool
}

const propsDefault = {
  first: false,
  backgroundColor: WHITE,
  borderBottom: false,
  onLayout: (event) => {}
}

class Card extends React.Component {
  render() {
    const {
      first,
      backgroundColor,
      style,
      borderBottom,
      children,
      onLayout,
      onPress
    } = this.props
    const Wrapper = !isFunction(onPress) ? View : TouchableOpacity
    return (
      <Wrapper
        onLayout={onLayout}
        onPress={onPress}
        style={[
          { marginTop: first ? 0 : HP05, backgroundColor, padding: WP6 },
          borderBottom && { ...BORDER_STYLE['bottom'] },
          style
        ]}
      >
        {children}
      </Wrapper>
    )
  }
}

Card.propTypes = propsType
Card.defaultProps = propsDefault
export default Card
