import React from 'react'
import PropTypes from 'prop-types'
import FeedbackFeed from '../../screens/timeline/FeedbackFeed'
import FeedItemGeneral from './FeedItemGeneral'
import FeedItemAdsBand from './FeedItemAdsBand'
import FeedItemCreateCollab from './FeedItemCreateCollab'
import TrendingSection from './TrendingSection'
import FeedItemChallenge from './FeedItemChallenge'

const propsType = {
  feed: PropTypes.object,
  shadow: PropTypes.string,
  fullText: PropTypes.bool,
  onRefresh: PropTypes.func,
  onPressOption: PropTypes.func,
  onPressItem: PropTypes.func,
  onPressTitle: PropTypes.func,
  onUpdateFeed: PropTypes.func
}

const propsDefault = {
  feed: {},
  fullText: false,
  onRefresh: async () => {
  },
  onUpdateFeed: async () => {
  }
}

const FeedType = {
  'ads': FeedItemAdsBand,
  'post': FeedItemGeneral,
  'feedback': FeedbackFeed,
  'bannerCollab': FeedItemCreateCollab,
  'trending': TrendingSection,
  'challenge': FeedItemChallenge
}

class FeedItem extends React.PureComponent {
  render() {
    const { feed: { type } } = this.props
    const FeedComponent = FeedType[type]
    if(!FeedComponent) return null
    return (
      <React.Fragment>
        <FeedComponent {...this.props}/>
      </React.Fragment>
    )
  }
}

FeedItem.propTypes = propsType
FeedItem.defaultProps = propsDefault
export default FeedItem
