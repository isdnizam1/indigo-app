import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import { upperCase } from 'lodash-es'
import moment from 'moment'
import { GREY_CHAT, BLUE_LIGHT, GREY, TOMATO, WHITE } from '../../constants/Colors'
import Image from '../Image'
import { WP25, WP65, WP3, WP5, HP2, HP1, WP2, WP105 } from '../../constants/Sizes'
import Text from '../Text'
import Button from '../Button'

const propsType = {
  data: PropTypes.object
}

const propsDefault = {
  data: {},
}

class FeedContentAdsCollab extends React.Component {
  _onPressItem = () => {
    const {
      navigateTo,
      onRefresh
    } = this.props
    navigateTo('Collaboration', {
      onRefresh
    })
  }

  render() {
    const { data, navigateTo } = this.props
    const { ads_status } = data
    const is_expired = ads_status === 'expired'
    const additionalData = JSON.parse(data.additional_data)
    return (
      <TouchableOpacity
        style={{
          backgroundColor: GREY_CHAT,
          alignItems: 'center',
          flexDirection: 'row',
          marginHorizontal: WP5,
          marginTop: HP2,
          borderRadius: 20
        }}
        activeOpacity={is_expired ? 1 : 0.8}
        onPress={is_expired ? null : () => navigateTo('CollaborationPreview', { id: data.id_ads })}
      >
        <Image
          centered
          source={{ uri: data.image }}
          style={{ borderBottomLeftRadius: 20, borderTopLeftRadius: 20 }}
          imageStyle={{ height: undefined, width: WP25, aspectRatio: 1, borderBottomLeftRadius: 20, borderTopLeftRadius: 20 }}
        />
        <View style={{ paddingVertical: HP1, paddingHorizontal: WP3, height: WP25, width: WP65, justifyContent: 'space-between' }}>
          <Text numberOfLines={1} size='xtiny' color={BLUE_LIGHT}>Collaboration</Text>
          <Text numberOfLines={2} ellipsizeMode='tail' size='tiny' weight={500} color={GREY}>{data.title}</Text>
          <Text numberOfLines={1} size='xtiny' color={is_expired ? GREY : TOMATO}>{is_expired ? 'Expired' : `${Math.abs(moment().startOf('day').diff(moment(additionalData.date.end), 'days'))+1} days left`}</Text>
          <Text numberOfLines={1} size='xtiny' color={GREY}>{upperCase(additionalData.location.name)}</Text>
        </View>
        <Image
          imageStyle={{ width: WP5, height: null }}
          style={{ alignSelf: 'flex-end', position: 'absolute', right: 0, top: 0, marginRight: WP5 }} source={require('../../assets/images/icCollaboration.png')}
        />
        {!is_expired ? <Button
          text='Join'
          textColor={WHITE}
          textSize='tiny'
          textWeight={500}
          backgroundColor={TOMATO}
          rounded
          marginless
          style={{ alignSelf: 'flex-end', position: 'absolute', right: 0, bottom: 0, marginRight: WP2, marginBottom: HP1 }}
          contentStyle={{ paddingHorizontal: WP3, paddingVertical: WP105 }}
          onPress={() => navigateTo('CollaborationPreview', { id: data.id_ads })}
                       /> : null}
      </TouchableOpacity>
    )
  }
}

FeedContentAdsCollab.propTypes = propsType
FeedContentAdsCollab.defaultProps = propsDefault
export default FeedContentAdsCollab
