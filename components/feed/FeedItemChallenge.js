import React from 'react'
import { ImageBackground } from 'react-native'
import { isEmpty } from 'lodash-es'
import { WP100 } from '../../constants/Sizes'

const propsType = {}
const propsDefault = {}

class FeedItemChallenge extends React.Component {

  render() {
    const { feed } = this.props,
      image = !isEmpty(feed.image) ? { uri: feed.image } : require('../../assets/images/bannerChallenge.png')
    return (
      <ImageBackground
        source={image}
        style={{ width: WP100, aspectRatio: 540/220 }}
      />
    )
  }
}

FeedItemChallenge.propTypes = propsType
FeedItemChallenge.defaultProps = propsDefault
export default FeedItemChallenge
