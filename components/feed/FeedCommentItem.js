import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { TouchableOpacity, View, } from 'react-native'
import { isFunction, noop } from 'lodash-es'
import HTMLElement from 'react-native-render-html'
import { IGNORED_TAGS } from 'react-native-render-html/src/HTMLUtils'
import { BLUE_LINK, GREY_WARM, WHITE, ORANGE_BRIGHT } from '../../constants/Colors'
import { HP1, HP2, WP2, WP3, WP4, WP7 } from '../../constants/Sizes'
import { TOUCH_OPACITY } from '../../constants/Styles'
import Text from '../Text'
import Avatar from '../Avatar'
import { postFeedCommentLike } from '../../actions/api'
import Icon from '../Icon'
import { NavigateToInternalBrowser } from '../../utils/helper'
import TextName from '../TextName'
import CounterButton from './CounterButton'

const propsType = {
  comment: PropTypes.object,
  onPressOption: PropTypes.func,
  onPressTitle: PropTypes.func
}

const propsDefault = {
  comment: {}
}

class FeedCommentItem extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      parsedText: ''
    }
  }

  componentDidMount() {
    const {
      comment
    } = this.props
    if (comment && comment.comment) {
      let parsedText = comment.comment
      let linkify = require('linkify-it')()
      let result = linkify.match(comment.comment)
      result && result.forEach(({ index, lastIndex, text, url }) => {
        parsedText = parsedText.replace(text, `<linktext url="${ url }">${ text }</linktext>`)
      })
      this.setState({ parsedText })
    }
  }

  _postFeedCommentLike = (idComment) => async () => {
    const {
      userData: { id_user },
      dispatch,
      onRefresh,
    } = this.props
    await dispatch(
      postFeedCommentLike,
      {
        related_to: 'id_comment',
        id_related_to: idComment,
        id_user
      },
      noop,
      false,
      false
    )
    await onRefresh()
  }

  _onNavigateToProfile = () => {
    const {
      navigation,
      comment,
      userData: { id_user }
    } = this.props
    navigation.navigate({
      routeName: 'ProfileScreenNoTab',
      params: { idUser: comment.id_user },
      key: `ProfileScreen${id_user}`
    })
  }

  _renderFeedCommentText = (textHtml, comment) => {
    const {
      navigateTo
    } = this.props
    return (
      <View style={{ alignContent: 'flex-start' }}>
        <TextName user={comment} style={{ marginRight: WP3, textAlign: 'left' }}/>
        <HTMLElement
          textSelectable
          containerStyle={{ flexDirection: 'row' }}
          html={textHtml}
          customWrapper={(children) => <Text size='mini'>{children}</Text>}
          baseFontStyle={{ fontFamily: 'OpenSansRegular' }}
          ignoredTags={[...IGNORED_TAGS]}
          renderers={{
            span: (htmlAttribs, children, convertedCSSStyles, passProps) => {
              return <Text size='mini'>{children}</Text>
            },
            a: (htmlAttribs, children, convertedCSSStyles, passProps) => {
              return fromHtmlEntities(htmlAttribs, children, passProps)
            },
            mention: (attbs, children, convertedCSSStyles, passProps) => {
              return (<Text
                onPress={() => {
                  navigateTo('ProfileScreenNoTab', { idUser: attbs.id })
                }} color={ORANGE_BRIGHT} key={Math.random()} size='mini'
                      >{children}</Text>)
            },
            linktext: (attbs, children, convertedCSSStyles, passProps) => {
              return (<Text
                onPress={() => {
                  NavigateToInternalBrowser({ url: attbs.url })
                }} color={BLUE_LINK} key={Math.random()} size='mini'
                      >{children}</Text>)
            }
          }}
        />
      </View>
    )
  }

  render() {
    const {
      comment,
      onPressOption,
      onReply,
    } = this.props
    const {
      parsedText
    } = this.state
    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY}
        onLongPress={() => {
          comment.optionType = 'comment'
          onPressOption(comment)
        }}
        disabled={!isFunction(onPressOption)}
        style={[
          {
            backgroundColor: WHITE,
            marginBottom: HP1
          }
        ]}
      >
        <View style={{ paddingHorizontal: WP4, flexDirection: 'row', marginBottom: HP2 }}>
          <View style={{ marginRight: WP2 }}>
            <Avatar onPress={this._onNavigateToProfile} image={comment.profile_picture}/>
          </View>
          <View style={{ flexGrow: 1, flex: 1, marginRight: WP7 }}>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center' }}>
              {
                this._renderFeedCommentText(parsedText, comment)
              }
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text size='tiny' color={GREY_WARM}>
                {moment(comment.created_at).fromNow(true)}
              </Text>
              {
                Number(comment.total_like) !== 0 ? (
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon centered color={GREY_WARM} type='Entypo' name='dot-single'/>
                    <Text size='tiny' color={GREY_WARM}>
                      {comment.total_like} likes
                    </Text>
                  </View>
                ) : null
              }
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon centered color={GREY_WARM} type='Entypo' name='dot-single' />
                <Text size='tiny' color={GREY_WARM} onPress={() => onReply(comment.id_user, comment.full_name)}>
                  Reply
                </Text>
              </View>
            </View>
          </View>
          <CounterButton
            style={{ flex: 1, height: '100%', alignItems: 'center', marginHorizontal: 0, position: 'absolute', right: WP4 }}
            onPress={this._postFeedCommentLike(Number(comment.id_comment))}
            active={Number(comment.is_liked)}
          />
        </View>
      </TouchableOpacity>
    )
  }
}

FeedCommentItem.propTypes = propsType
FeedCommentItem.defaultProps = propsDefault
export default FeedCommentItem
