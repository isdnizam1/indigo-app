import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import FeedContentAdsCollab from './FeedContentAdsCollab'

const propsType = {
  data: PropTypes.object
}

const propsDefault = {
  data: {},
}

const FeedType = {
  'collaboration': FeedContentAdsCollab,
}

class FeedContentAds extends React.Component {
  render() {
    const { data, navigateTo } = this.props
    const FeedComponent = FeedType[data.category]
    if(!FeedComponent) return null

    return (
      <View>
        <FeedComponent data={data} navigateTo={navigateTo}/>
      </View>
    )
  }
}

FeedContentAds.propTypes = propsType
FeedContentAds.defaultProps = propsDefault
export default FeedContentAds
