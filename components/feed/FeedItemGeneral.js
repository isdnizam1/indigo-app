import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import 'moment/min/locales'
import { ScrollView, TouchableOpacity, View } from 'react-native'
import {
  filter,
  get,
  isEmpty,
  isObject,
  isString,
  map,
  noop,
  split,
  words,
} from 'lodash-es'
import LinkifyIt from 'linkify-it'
import SkeletonContent from 'react-native-skeleton-content'
import RNModal from 'react-native-modal'
import {
  CLEAR_BLUE,
  GUN_METAL,
  PALE_BLUE,
  PALE_GREY_TWO,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  SKELETON_COLOR,
  SKELETON_HIGHLIGHT,
  PALE_WHITE,
} from 'sf-constants/Colors'
import { FONT_SIZE, WP1, WP2, WP3, WP4, WP70, WP80 } from 'sf-constants/Sizes'
import { SHADOW_STYLE, TOUCH_OPACITY } from 'sf-constants/Styles'
import {
  getFeedDetail,
  postFeedLike,
  postProfileFollow,
  postProfileUnfollow,
} from 'sf-actions/api'
import Text from 'sf-components/Text'
import Icon from 'sf-components/Icon'
import Avatar from 'sf-components/Avatar'
import ImageAuto from 'sf-components/ImageAuto'
import { renderHyperLink } from 'sf-components/Html'
import { getImageFromFeed } from 'sf-utils/helper'
import { setTopicId } from 'sf-services/timeline/actionDispatcher'
import TextName from 'sf-components/TextName'
import LinkPreviewCard from 'sf-components/LinkPreviewCard'
import ReadMore from 'sf-components/ReadMore'
import ExploreComponent from 'sf-components/share/explore'
import ProfileComponent from 'sf-components/share/profile'
import { relativeTime } from 'sf-utils/date'
import FollowButton from 'sf-components/follow/FollowButton'
import MenuOptions from 'sf-components/MenuOptions'
import DeletePost from 'sf-components/DeletePost'
import { connect } from 'react-redux'
import { DEFAULT_PAGING } from '../../constants/Routes'
import CounterButton from './CounterButton'
import FeedItemGeneralMultiple from './FeedItemGeneralMultiple'
import FeedItemDeleted from './FeedItemDeleted'
import FeedItemSkeletonConfig from './FeedItemSkeletonConfig'

class FeedItemGeneral extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      feed: this.props.feed,
      showDeleteModal: false,
      totalComment: 0,
    }
  }

  UNSAFE_componentWillReceiveProps = (nextProps) => {
    this.setState({
      feed: nextProps.feed,
    })
  };

  _postFeedLike = (idTimeline) => async () => {
    const {
      userData: { id_user },
      dispatch,
      onRefresh,
    } = this.props

    if (!id_user) {
      this.props.navigateTo('AuthNavigator')
    } else {
      const { feed } = this.state
      feed.is_liked = 1
      this.setState(feed)

      await dispatch(
        postFeedLike,
        {
          related_to: 'id_timeline',
          id_related_to: idTimeline,
          id_user,
        },
        noop,
        false,
        false,
      )
      await this._getFeed(null, null, false)
      if (this.props.isFromDetail) {
        onRefresh(null, null, false)
      }
    }
  };

  _onFollowed = async (follow, destinationId, isLoading = true) => {
    const {
      userData: { id_user },
      dispatch,
    } = this.props
    const currentAction = follow ? postProfileFollow : postProfileUnfollow
    const currentPayload = follow
      ? { id_user: destinationId, followed_by: id_user }
      : {
          id_user,
          id_user_following: destinationId,
        }
    await dispatch(currentAction, currentPayload, noop, true, isLoading)
  };

  _getFeed = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const {
      dispatch,
      idTimeline,
      onUpdateFeed,
      userData: { id_user },
    } = this.props
    const { feed } = this.state
    if (idTimeline || isObject(feed)) {
      const dataResponse = await dispatch(
        getFeedDetail,
        {
          id_timeline: idTimeline ? idTimeline : feed.id_timeline,
          id_user,
        },
        noop,
        false,
        isLoading,
      )
      dataResponse.id_timeline = idTimeline ? idTimeline : feed.id_timeline
      await this._updateFeed(dataResponse)
      try {
        onUpdateFeed(dataResponse)
      } catch (e) {
        //
      }
    }
  };

  _updateFeed = async (item) => {
    await this.setState({ feed: item })
  };

  _getAspectRatio = (i) => {
    const { feed } = this.state
    const attImages = filter(feed.attachment, ['attachment_type', 'photo'])

    if (attImages.length === 1)
      return {
        aspectRatio: 1,
        width: '100%',
      }
    else if (attImages.length <= 2 || i === 0)
      return {
        aspectRatio: 2.5,
        width: '100%',
      }
    else {
      return {
        aspectRatio: 1,
        width: '49.5%',
      }
    }
  };

  _onNavigateToProfile = () => {
    const { navigateTo } = this.props
    const { feed } = this.state
    navigateTo('ProfileScreenNoTab', { idUser: feed.id_user }, 'push')
  };

  _getFirstName = (fullName) => words(fullName)[0];

  _onPressItem = () => {
    const { navigateTo, onRefresh } = this.props
    const { feed } = this.state
    if (isObject(feed)) {
      navigateTo('FeedDetailScreen', {
        idTimeline: feed.id_timeline,
        onRefresh,
        onUpdateFeed: this._updateFeed,
        onCommentCount: (totalComment) => {
          this.setState({ totalComment })
        },
        onPressTopic: this.props.onPressTopic,
        is1000Startup: this.props.is1000Startup,
      })
    }
  };

  _isLiked(feed) {
    return Number(feed.is_liked_by_viewer) === 1 || Number(feed.is_liked) === 1
  }

  renderViewMore() {
    return (
      <Text type='Circular' size='mini' weight={300} color={REDDISH}>
        selebihnya
      </Text>
    )
  }

  _renderTextContent = () => {
    const { fullText, isLoading, navigateTo, is1000Startup } = this.props
    const { feed } = this.state

    const isTrending = !isEmpty(feed.id_trending)

    if (isEmpty(feed.description)) return null
    return (
      <View
        style={{
          marginHorizontal: this._getMarginHorizontal(),
          marginBottom: WP2,
        }}
      >
        {is1000Startup ? (
          <></>
        ) : (
          isTrending && (
            <Text
              style={{ lineHeight: FONT_SIZE['mini'] * 1.28 }}
              type='Circular'
              size='mini'
              weight={300}
              color={CLEAR_BLUE}
            >
              #Trending
            </Text>
          )
        )}
        <SkeletonContent
          containerStyle={{ flex: 1 }}
          layout={[
            FeedItemSkeletonConfig.layout.textContent(WP80),
            FeedItemSkeletonConfig.layout.textContent(WP70),
            FeedItemSkeletonConfig.layout.textContent(),
          ]}
          isLoading={isLoading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          {renderHyperLink(
            fullText ? (
              <Text
                style={{ lineHeight: FONT_SIZE['mini'] * 1.28 }}
                type='Circular'
                size='mini'
                weight={300}
                color={SHIP_GREY}
              >
                {feed.description}
              </Text>
            ) : (
              <ReadMore
                numberOfLines={5}
                style={{
                  flexDirection: 'row',
                  marginBottom: get(feed, 'attachment.length', 0) > 0 ? WP2 : 0,
                }}
                renderViewMore={this.renderViewMore.bind(this)}
              >
                <Text
                  style={{ lineHeight: FONT_SIZE['mini'] * 1.28 }}
                  type='Circular'
                  size='mini'
                  weight={300}
                  color={SHIP_GREY}
                >
                  {feed.description}
                </Text>
              </ReadMore>
            ),
            { navigateTo },
          )}
        </SkeletonContent>
      </View>
    )
  };

  _renderAttachment = () => {
    const { navigateTo, ownFeed } = this.props
    const { feed } = this.state

    if (isEmpty(feed.attachment)) return null

    let attachments = [...feed.attachment]

    const wrapperStyle = {}

    const onPress = () =>
      navigateTo('GalleryScreen', { images: getImageFromFeed(feed), ownFeed })

    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY}
        onPress={onPress}
        style={[
          {
            flexDirection: 'row',
            alignItems: 'flex-start',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
          },
          wrapperStyle,
        ]}
      >
        {map(attachments, (image, i) =>
          image.attachment_type === 'photo' ? (
            <ImageAuto
              key={`${i}`}
              style={[
                this._getAspectRatio(i),
                { marginVertical: 2, borderRadius: 0 },
              ]}
              source={{ uri: image.attachment_file }}
            />
          ) : null,
        )}
      </TouchableOpacity>
    )
  };

  _renderTopics = () => {
    const { onPressTopic, isLoading } = this.props
    const { feed } = this.state

    const topics = feed.topic_name ? split(feed.topic_name, ',') : []
    const topicIds = feed.id_topic
      ? isString(feed.id_topic)
        ? JSON.parse(feed.id_topic.replace(/'/g, '"'))
        : feed.id_topic
      : []

    return (
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        bouncesZoom={false}
        bounces={false}
        style={{
          marginHorizontal: this._getMarginHorizontal(),
          flexGrow: 0,
          marginTop: WP1,
          marginBottom: WP2,
        }}
      >
        <View>
          <SkeletonContent
            containerStyle={{
              flex: 1,
              flexDirection: 'row',
              marginHorizontal: -WP1,
              flexWrap: 'wrap',
            }}
            layout={[
              FeedItemSkeletonConfig.layout.topic,
              FeedItemSkeletonConfig.layout.topic,
            ]}
            isLoading={isLoading}
            boneColor={SKELETON_COLOR}
            highlightColor={SKELETON_HIGHLIGHT}
          >
            {map(topics, (topic, i) => (
              <TouchableOpacity
                key={`feedTopic${i}`}
                style={{
                  margin: WP1,
                  padding: WP1,
                  paddingHorizontal: WP2,
                  backgroundColor: PALE_GREY_TWO,
                  borderRadius: 6,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}
                onPress={() => {
                  // this.props.navigation.popToTop()
                  this.props.navigateTo('TimelineScreen')
                  this.props.setTopicId(topicIds[i])
                }}
              >
                <Text
                  type='Circular'
                  size='xmini'
                  weight={400}
                  color={SHIP_GREY_CALM}
                >
                  {topic}
                </Text>
              </TouchableOpacity>
            ))}
          </SkeletonContent>
        </View>
      </ScrollView>
    )
  };

  _renderLikeCommentButton = () => {
    const { isLoading } = this.props
    const { feed } = this.state
    return (
      <>
        <SkeletonContent
          containerStyle={{ flexGrow: 0 }}
          layout={[FeedItemSkeletonConfig.layout.counterButton]}
          isLoading={isLoading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <CounterButton
            onPress={this._postFeedLike(Number(feed.id_timeline))}
            active={this._isLiked(feed)}
            value={counterLabel(feed.total_like, '', 'Like')}
            disable={isLoading}
          />
        </SkeletonContent>
        <SkeletonContent
          containerStyle={{ flexGrow: 0 }}
          layout={[FeedItemSkeletonConfig.layout.counterButton]}
          isLoading={isLoading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <CounterButton
            onPress={this._onPressItem}
            images={[null, require('sf-assets/icons/icComment.png')]}
            value={counterLabel(
              this.state.totalComment || feed.total_comment,
              'Komentar',
              'Komentar',
            )}
            disable={isLoading}
          />
        </SkeletonContent>
      </>
    )
  };

  _renderLikeInfo = () => {
    const { isLoading, navigateTo } = this.props
    const { feed } = this.state
    const isLiked = feed.total_like != 0
    return (
      <View style={{ flexDirection: 'row' }}>
        <SkeletonContent
          containerStyle={{ flexGrow: 0, flexDirection: 'row' }}
          layout={[FeedItemSkeletonConfig.layout.likeInfo]}
          isLoading={isLoading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <CounterButton
            onPress={this._postFeedLike(Number(feed.id_timeline))}
            active={this._isLiked(feed)}
            disable={isLoading}
          />
          <Text
            onPress={() => {
              if (isLiked) {
                navigateTo('LikesScreen', {
                  idTimeline: feed.id_timeline,
                })
              }
            }}
            size='xmini'
            type='Circular'
            weight={400}
            color={this._isLiked(feed) ? CLEAR_BLUE : SHIP_GREY_CALM}
          >
            {isLiked ? `${feed.total_like} orang menyukai` : 'Like'}
          </Text>
        </SkeletonContent>
      </View>
    )
  };

  _renderCounter = () => {
    const { isLoading, isFromDetail, is1000Startup } = this.props
    const { feed } = this.state

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: WP4,
          paddingTop: WP2,
          paddingBottom: WP1,
        }}
      >
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
          {isFromDetail ? this._renderLikeInfo() : this._renderLikeCommentButton()}
        </View>
        <SkeletonContent
          containerStyle={{ flexGrow: 0 }}
          layout={[FeedItemSkeletonConfig.layout.counterButton]}
          isLoading={isLoading}
          boneColor={SKELETON_COLOR}
          highlightColor={SKELETON_HIGHLIGHT}
        >
          <CounterButton
            onPress={() => {
              this.props.navigateTo('ShareScreen', {
                id: feed.id_timeline,
                type: 'timeline',
                title: 'Bagikan post',
                is1000Startup,
              })
            }}
            images={[null, require('sf-assets/icons/icShareInactive.png')]}
            value='Share'
            disable={isLoading}
          />
        </SkeletonContent>
      </View>
    )
  };

  _renderSharedContent = () => {
    const { navigation } = this.props
    const { feed } = this.state

    if (!feed.additional_data) return undefined
    const additionalData = JSON.parse(feed.additional_data)

    let CardComponent
    if (feed.multiple_post) {
      const idTimeline =
        feed.source === 'ads'
          ? feed.data_ads.id_ads
          : feed.source === 'journey'
          ? feed.data_journey.id_journey
          : null
      if (
        feed[`data_${feed.source}`] &&
        !isEmpty(feed[`data_${feed.source}`].deleted_at)
      ) {
        CardComponent = <FeedItemDeleted />
      } else {
        CardComponent = (
          <FeedItemGeneralMultiple
            {...this.props}
            idTimeline={idTimeline}
            navigation={navigation}
            feed={feed[`data_${feed.source}`]}
            parentTimeline={feed}
          />
        )
      }
    } else {
      if (additionalData.type_post === 'share') {
        if (feed.source === 'ads' || additionalData.id_ads) {
          if (
            feed[`data_${feed.source}`] &&
            !isEmpty(feed[`data_${feed.source}`].deleted_at)
          ) {
            CardComponent = <FeedItemDeleted />
          } else {
            CardComponent = (
              <ExploreComponent
                showTopics={false}
                navigation={navigation}
                id={additionalData.id_ads}
                id_episode={additionalData.id_episode}
                data={!additionalData.id_episode ? feed.data_ads : null}
              />
            )
          }
        } else if (feed.source === 'journey' || additionalData.id_journey) {
          if (
            feed[`data_${feed.source}`] &&
            !isEmpty(feed[`data_${feed.source}`].deleted_at)
          ) {
            CardComponent = <FeedItemDeleted />
          } else {
            CardComponent = (
              <ProfileComponent
                showTopics={false}
                navigation={navigation}
                id={additionalData.id_journey}
                id_artist={additionalData.id_user}
                id_schedule={additionalData.id_schedule}
              />
            )
          }
        }
      }
    }

    return (
      <View
        style={{
          paddingHorizontal: this._getMarginHorizontal(),
          marginBottom: WP2,
        }}
      >
        {CardComponent}
      </View>
    )
  };

  _renderLinkPreviewCard = () => {
    const { feed } = this.state

    return (
      <View
        style={{
          marginHorizontal: this._getMarginHorizontal(),
          paddingTop: WP2,
        }}
      >
        <LinkPreviewCard
          {...this.props}
          text={feed.description || ''}
          containerStyle={{ marginBottom: WP2 }}
        />
      </View>
    )
  };

  _getUrl = (text) => {
    const Linkify = new LinkifyIt()
    const urls = Linkify.match(text)
    const url = get(urls, '[0].url', '')
    return url
  };

  _getMarginHorizontal = () => WP4;

  _setDeleteModalVisible = (isVisible) => async () => {
    const showModalFunc = async () =>
      await this.setState({ showDeleteModal: isVisible })
    if (isVisible) {
      setTimeout(showModalFunc, 400)
    } else {
      await showModalFunc()
    }
  };

  render() {
    const {
      shadow,
      isLoading,
      isFromDetail,
      ownFeed,
      onDeleteFeed,
      userData,
      is1000Startup,
    } = this.props
    const { feed, showDeleteModal } = this.state

    const containerStyle = {}
    const textLimitProfession = 33

    return (
      <View
        style={[
          shadow ? SHADOW_STYLE[shadow] : null,
          {
            backgroundColor: PALE_WHITE,
            flexGrow: 1,
            paddingVertical: WP4,
            borderTopWidth: 1,
            borderTopColor: PALE_BLUE,
          },
          containerStyle,
        ]}
      >
        <View style={{ flexGrow: 1 }}>
          <TouchableOpacity
            onPress={this._onPressItem}
            activeOpacity={TOUCH_OPACITY}
            style={{ flexGrow: 1 }}
          >
            <View
              style={{
                flexDirection: 'row',
                marginHorizontal: this._getMarginHorizontal(),
              }}
            >
              <TouchableOpacity
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginBottom: WP3,
                  alignItems: 'flex-start',
                }}
                activeOpacity={TOUCH_OPACITY}
                onPress={this._onNavigateToProfile}
              >
                <View style={{ marginRight: WP3 }}>
                  <Avatar
                    size='small'
                    image={feed.profile_picture}
                    verifiedStatus={feed.verified_status}
                    isLoading={isLoading}
                  />
                </View>
                <View style={{ flex: 1 }}>
                  <SkeletonContent
                    containerStyle={{ flex: 1 }}
                    layout={[
                      FeedItemSkeletonConfig.layout.userName,
                      FeedItemSkeletonConfig.layout.jobTitle,
                    ]}
                    isLoading={isLoading}
                    boneColor={SKELETON_COLOR}
                    highlightColor={SKELETON_HIGHLIGHT}
                  >
                    <TextName
                      type='Circular'
                      size='mini'
                      weight={500}
                      color={GUN_METAL}
                      user={feed}
                      centered={false}
                    />
                    <View>
                      <View style={{ paddingRight: WP2 }}>
                        {feed.job_title && (
                          <Text
                            numberOfLines={1}
                            type='Circular'
                            size='xmini'
                            weight={300}
                            color={SHIP_GREY_CALM}
                          >{`${
                            feed.job_title.length <= textLimitProfession
                              ? feed.job_title
                              : `${feed.job_title.substring(
                                  0,
                                  !feed.company_name
                                    ? feed.job_title.length
                                    : textLimitProfession,
                                )}${
                                  feed.job_title.length > textLimitProfession
                                    ? '...'
                                    : ''
                                }`
                          } ${
                            feed.company_name &&
                            feed.company_name !== 'Self Employed'
                              ? `at ${feed.company_name}`
                              : ''
                          }`}</Text>
                        )}
                        <Text
                          numberOfLines={1}
                          type='Circular'
                          size='xmini'
                          weight={300}
                          color={SHIP_GREY_CALM}
                        >
                          {moment(feed.created_at)
                            .locale('id', { relativeTime })
                            .fromNow()
                            .replace('semenit', '1 menit')
                            .replace('sejam', '1 jam')
                            .replace('sehari', '1 hari')
                            .replace('seminggu', '1 minggu')
                            .replace('sebulan', '1 bulan')
                            .replace('setahun', '1 tahun')
                            .replace('yang lalu', 'lalu')
                            .replace('beberapa detik lalu', 'baru saja')}
                        </Text>
                      </View>
                    </View>
                  </SkeletonContent>
                </View>
              </TouchableOpacity>
              <View>
                {ownFeed && (
                  <MenuOptions
                    options={[
                      {
                        onPress: this._setDeleteModalVisible(true),
                        iconName: 'delete',
                        iconColor: SHIP_GREY_CALM,
                        iconSize: 'huge',
                        title: 'Hapus',
                      },
                    ]}
                    customHeader={<View />}
                    triggerComponent={(toggleModal) => (
                      <TouchableOpacity
                        onPress={toggleModal}
                        style={{
                          padding: 5,
                        }}
                      >
                        <Icon
                          centered
                          name={'dots-three-horizontal'}
                          type='Entypo'
                          color={SHIP_GREY_CALM}
                        />
                      </TouchableOpacity>
                    )}
                  />
                )}
                {!ownFeed && isFromDetail && !isLoading && userData.id_user && (
                  <FollowButton
                    followed={feed.is_followed_by_viewer}
                    idUser={feed.id_user}
                    onPress={this._onFollowed}
                  />
                )}
              </View>
            </View>

            {this._renderTextContent()}

            {!isLoading && this._renderAttachment()}

            {!isLoading && this._renderSharedContent()}

            {!isLoading && this._renderLinkPreviewCard()}
          </TouchableOpacity>
        </View>

        {this._renderTopics()}
        {this._renderCounter()}

        <RNModal
          isVisible={showDeleteModal}
          onBackdropPress={this._setDeleteModalVisible(false)}
          onBackButtonPress={this._setDeleteModalVisible(false)}
          style={{ justifyContent: 'center', alignItems: 'center' }}
        >
          <DeletePost
            containerStyle={{ top: 0, left: 0, position: undefined }}
            onCancel={this._setDeleteModalVisible(false)}
            onConfirm={async () => {
              await this._setDeleteModalVisible(false)()
              onDeleteFeed(Number(feed.id_journey))
            }}
          />
        </RNModal>
      </View>
    )
  }
}

const counterLabel = (value, label, empty) => {
  let labelText = value < 1 ? empty : label
  let valueText = value < 1 ? '' : `${value} `
  return `${valueText}${labelText}`
}

FeedItemGeneral.propTypes = {
  onPressTopic: PropTypes.func,
  isLoading: PropTypes.bool,
  isFromDetail: PropTypes.bool,
  ownFeed: PropTypes.bool,
  onDeleteFeed: PropTypes.func,
  is1000Startup: PropTypes.bool,
}

FeedItemGeneral.defaultProps = {
  onPressTopic: () => {},
  isLoading: false,
  isFromDetail: false,
  ownFeed: false,
  onDeleteFeed: noop,
  is1000Startup: false,
}

export default connect(null, { setTopicId })(FeedItemGeneral)
