import React from 'react'
import { View } from 'react-native-animatable'
import Text from '../Text'
import { PALE_GREY_THREE, SHIP_GREY, SHIP_GREY_CALM } from '../../constants/Colors'
import { HP05, WP1, WP4 } from '../../constants/Sizes'

function FeedItemDeleted() {
  return (
    <View style={{ borderRadius: 6, backgroundColor: PALE_GREY_THREE, padding: WP4, marginBottom: HP05 }}>
      <Text style={{ marginBottom: WP1 }} type='Circular' color={SHIP_GREY} size={'xmini'} weight={500}>Halaman sudah tidak tersedia</Text>
      <Text type='Circular' color={SHIP_GREY_CALM} size={'tiny'} weight={300}>Soundfren</Text>
    </View>
  )
}
export default FeedItemDeleted