import React from 'react'
import { View, ImageBackground, TouchableOpacity } from 'react-native'
import Text from '../Text'
import { TOUCH_OPACITY } from '../../constants/Styles'
import { TOMATO } from '../../constants/Colors'
import { WP100 } from '../../constants/Sizes'
import { adsTabStyle } from '../ads/AdsTab'

const propsType = {}
const propsDefault = {}

class FeedItemCreateCollab extends React.Component {
  _onPressItem = () => {
    const {
      navigateTo,
      onRefresh
    } = this.props
    navigateTo('Collaboration', {
      onRefresh
    })
  }

  render() {
    return (
      <ImageBackground
        source={require('../../assets/images/bannerCollaboration.png')}
        style={{ width: WP100, aspectRatio: 320/130 }}
      >
        <View style={[adsTabStyle.bannerContainer]}>
          <View style={[adsTabStyle.bannerContentWrapper]}>
            <Text size='tiny' weight={500} style={[adsTabStyle.bannerContentText]}>Looking for someone to collaborate with? Find them here!</Text>
            <TouchableOpacity
              style={[adsTabStyle.bannerButton, { backgroundColor: TOMATO }]}
              activeOpacity={TOUCH_OPACITY}
              onPress={this._onPressItem}
            >
              <Text size='tiny' weight={600} type='NeoSans' style={[adsTabStyle.buttonLabel]}>Create Collaboration Project!</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    )
  }
}

FeedItemCreateCollab.propTypes = propsType
FeedItemCreateCollab.defaultProps = propsDefault
export default FeedItemCreateCollab
