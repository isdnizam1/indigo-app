import React, { Component } from 'react'
import { LinearGradient } from 'expo-linear-gradient'
import { ScrollView, View, TouchableOpacity } from 'react-native'
import { GUN_METAL, NAVY_DARK, ORANGE, PALE_WHITE, PURPLE_DARKER_GRADIENT } from '../../constants/Colors'
import { LINEAR_TYPE } from '../../constants/Styles'
import { HP05, HP2, WP1, WP100, WP105, WP2, WP4, WP6, WP75 } from '../../constants/Sizes'
import Text from '../Text'
import { Avatar } from '..'

class TrendingSection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeIndex: 0
    }
  }

  _handleScroll = ({ nativeEvent }) => {
    const {
      contentOffset
    } = nativeEvent

    const offset = WP75
    const activeIndex = Math.floor(contentOffset.x / offset)

    this.setState({ activeIndex })
  }

  _renderIndicator = () => {
    const {
      trending
    } = this.props

    const {
      activeIndex
    } = this.state

    return (
      <View style={{ flexDirection: 'row' }}>
        {
          trending.map((item, index) => {
            return (
              <View
                key={index}
                style={{
                  backgroundColor: activeIndex === index ? ORANGE : PURPLE_DARKER_GRADIENT,
                  borderRadius: WP1,
                  width: WP2,
                  height: WP2,
                  marginRight: WP105
                }}
              />
            )
          })
        }
      </View>
    )
  }

  _renderTrendingImage = (feed) => (
    <View style={{ width: (WP100-WP4*2)/5, alignItems: 'center', padding: WP1 }}>
      <Avatar size={'regular'} image={feed.profile_picture} verifiedStatus={feed.verified_status} />
      <Text style={{ marginTop: HP05 }} centered color={GUN_METAL} size='tiny' numberOfLines={1}>{feed.full_name}</Text>
    </View>
  )

  render() {
    const {
      navigateTo,
      trending
    } = this.props

    if (this.props.isTrendingShown) return null
    return (
      <LinearGradient
        colors={[PALE_WHITE, PALE_WHITE]} {...LINEAR_TYPE.vertical}
      >
        <View
          style={{
            flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: WP4
          }}
        >
          <Text
            style={{ marginVertical: HP2 }}
            color={NAVY_DARK} weight={500} size='small'
          >
            Trending Now
          </Text>
        </View>
        <ScrollView
          horizontal
          onScroll={this._handleScroll}
          style={{
            paddingHorizontal: WP4, paddingBottom: WP6
          }}
          showsHorizontalScrollIndicator={false}
        >
          {
            trending.slice(0, 5).map((item) => (
              <TouchableOpacity
                onPress={() =>
                  navigateTo('FeedDetailScreen', {
                    idTimeline: item.id_timeline
                  })}
                key={Math.random()}
              >
                {this._renderTrendingImage(item)}
              </TouchableOpacity>
            ))
          }
          <View style={{ width: WP6 }} />
        </ScrollView>
      </LinearGradient>
    )
  }
}

TrendingSection.propTypes = {}

TrendingSection.defaultProps = {}

export default TrendingSection
