import React from 'react'
import moment from 'moment'
import 'moment/min/locales'
import { TouchableOpacity, View } from 'react-native'
import { filter, isEmpty, map, noop, words, get } from 'lodash-es'
import LinkifyIt from 'linkify-it'
import {
  GUN_METAL,
  PALE_BLUE,
  REDDISH,
  SHIP_GREY,
  SHIP_GREY_CALM,
  PALE_WHITE,
} from '../../constants/Colors'
import { WP2, WP3, WP6, WP80, WP305, FONT_SIZE } from '../../constants/Sizes'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { getFeedDetail } from '../../actions/api'
import Text from '../Text'
import Avatar from '../Avatar'
import ImageAuto from '../ImageAuto'
import { renderHyperLink } from '../Html'
import { getImageFromFeed } from '../../utils/helper'
import TextName from '../TextName'
import LinkPreviewCard from '../LinkPreviewCard'
import ReadMore from '../ReadMore'
import ExploreComponent from '../share/explore'
import ProfileComponent from '../share/profile'
import { relativeTime } from '../../utils/date'
import FeedItemDeleted from './FeedItemDeleted'

class FeedItemGeneralMultiple extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      feed: this.props.feed,
    }
  }

  _getFeed = async (params = DEFAULT_PAGING, loadMore = false, isLoading = true) => {
    const {
      dispatch,
      idTimeline,
      onUpdateFeed,
      userData: { id_user },
    } = this.props
    const { feed } = this.state
    if (!feed) return
    const dataResponse = await dispatch(
      getFeedDetail,
      {
        id_timeline: idTimeline ? idTimeline : feed.id_timeline,
        id_user,
      },
      noop,
      false,
      isLoading,
    )
    dataResponse.id_timeline = idTimeline ? idTimeline : feed.id_timeline
    await this._updateFeed(dataResponse)
    try {
      onUpdateFeed(dataResponse)
    } catch (e) {
      //
    }
  };

  _updateFeed = async (item) => {
    await this.setState({ feed: item })
  };

  _getAspectRatio = (i) => {
    const { feed, isTrending } = this.props
    const attImages = filter(feed.attachment, ['attachment_type', 'photo'])

    if (isTrending)
      return {
        width: '100%',
        aspectRatio: 2.45,
      }

    if (attImages.length === 1)
      return {
        aspectRatio: 1,
        width: '100%',
      }
    else if (attImages.length <= 2 || i === 0)
      return {
        aspectRatio: 2.5,
        width: '100%',
      }
    else {
      return {
        aspectRatio: 1,
        width: '49.5%',
      }
    }
  };

  _onNavigateToProfile = () => {
    const { navigateTo, feed } = this.props
    navigateTo('ProfileScreenNoTab', { idUser: feed.id_user })
  };

  _getFirstName = (fullName) => words(fullName)[0];

  _onPressItem = () => {
    const { feed, navigation, onRefresh, idTimeline } = this.props
    navigation.push('FeedDetailScreen', {
      idTimeline: feed.id_timeline || idTimeline,
      onRefresh,
      onUpdateFeed: this._updateFeed,
      key: `FeedDetailScreen${feed.id_timeline || idTimeline}`,
    })
  };

  renderViewMore() {
    return (
      <Text type='Circular' size='mini' weight={300} color={REDDISH}>
        selebihnya
      </Text>
    )
  }

  _renderTextContent = () => {
    const { fullText, isTrending } = this.props

    const { feed } = this.state

    if (isTrending && !isEmpty(feed.attachment)) return null
    if (isEmpty(feed.description)) return null
    return (
      <View
        style={{
          marginHorizontal: this._getMarginHorizontal(),
          marginBottom: WP2,
        }}
      >
        {renderHyperLink(
          fullText ? (
            <Text
              style={{ lineHeight: FONT_SIZE['mini'] * 1.28 }}
              type='Circular'
              size='mini'
              weight={300}
              color={SHIP_GREY}
            >
              {feed.description}
            </Text>
          ) : (
            <ReadMore
              numberOfLines={5}
              style={{
                flexDirection: 'row',
                marginBottom: feed.attachment.length > 0 ? WP2 : 0,
              }}
              // style={{ flexDirection: 'row', marginBottom: 0 }}
              renderViewMore={this.renderViewMore.bind(this)}
            >
              <Text
                style={{ lineHeight: FONT_SIZE['mini'] * 1.28 }}
                type='Circular'
                size='mini'
                weight={300}
                color={SHIP_GREY}
              >
                {feed.description}
              </Text>
            </ReadMore>
          ),
        )}
      </View>
    )
  };

  _renderAttachment = () => {
    const { isTrending, navigateTo } = this.props

    const { feed } = this.state

    if (isEmpty(feed.attachment)) return null

    let attachments = [...feed.attachment]
    if (isTrending) attachments = [attachments[0]]

    const wrapperStyle = isTrending
      ? {
          marginHorizontal: this._getMarginHorizontal(),
          borderRadius: WP2,
          overflow: 'hidden',
        }
      : {}

    const onPress = isTrending
      ? this._onPressItem
      : () =>
          navigateTo('GalleryScreen', {
            images: getImageFromFeed(feed),
          })

    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY}
        onPress={onPress}
        style={[
          {
            flexDirection: 'row',
            alignItems: 'flex-start',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
          },
          wrapperStyle,
        ]}
      >
        {map(attachments, (image, i) =>
          image.attachment_type === 'photo' ? (
            <ImageAuto
              key={`${i}`}
              style={[
                this._getAspectRatio(i),
                {
                  marginVertical: 2,
                  borderRadius: isTrending ? WP2 : 0,
                },
              ]}
              source={{ uri: image.attachment_file }}
            />
          ) : null,
        )}
      </TouchableOpacity>
    )
  };

  _renderSharedContent = () => {
    const { navigation, feed } = this.props
    if (!feed.additional_data) return undefined
    const additionalData = JSON.parse(feed.additional_data)

    let CardComponent

    if (additionalData.type_post === 'share') {
      if (feed.source === 'ads' || additionalData.id_ads) {
        if (feed[`data_${feed.source}`] && !isEmpty(feed[`data_${feed.source}`].deleted_at)) {
          CardComponent = <FeedItemDeleted />
        } else {
          CardComponent = (
            <ExploreComponent
              showTopics={false}
              navigation={navigation}
              id={additionalData.id_ads}
              data={feed.data_ads}
            />
          )
        }
      } else if (feed.source === 'journey' || additionalData.id_journey) {
        if (feed[`data_${feed.source}`] && !isEmpty(feed[`data_${feed.source}`].deleted_at)) {
          CardComponent = <FeedItemDeleted />
        } else {
          CardComponent = (
            <ProfileComponent
              showTopics={false}
              navigation={navigation}
              id={additionalData.id_journey}
              id_artist={additionalData.id_user}
              id_schedule={additionalData.id_schedule}
            />
          )
        }
      }

      return (
        <View
          style={{
            paddingHorizontal: this._getMarginHorizontal(),
            marginBottom: WP2,
          }}
        >
          {CardComponent}
        </View>
      )
    }
  };

  _getMarginHorizontal = () => WP2;

  _getUrl = (text) => {
    const Linkify = new LinkifyIt()
    const urls = Linkify.match(text)
    const url = get(urls, '[0].url', '')
    return url
  };

  render() {
    const { shadow, isTrending, parentTimeline } = this.props

    const { feed } = this.state

    const linkPreviewParent = this._getUrl(parentTimeline.description)
    const containerStyle = isTrending ? { width: WP80 - WP6 } : {}
    const textLimitProfession = isTrending ? 19 : 28

    return (
      <View
        style={[
          shadow ? SHADOW_STYLE[shadow] : null,
          {
            backgroundColor: PALE_WHITE,
            borderRadius: 6,
            flexGrow: 1,
            paddingVertical: WP2,
            borderWidth: 1,
            borderColor: PALE_BLUE,
          },
          containerStyle,
        ]}
      >
        <View style={{ flexGrow: 1 }}>
          <TouchableOpacity
            onPress={this._onPressItem}
            activeOpacity={TOUCH_OPACITY}
            style={{ flexGrow: 1 }}
          >
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                marginBottom: WP3,
                alignItems: 'flex-start',
                marginHorizontal: this._getMarginHorizontal(),
              }}
              activeOpacity={TOUCH_OPACITY}
              disabled
              onPress={this._onNavigateToProfile}
            >
              <View style={{ marginRight: WP3 }}>
                <Avatar
                  size='small'
                  image={feed.profile_picture}
                  verifiedSize={WP305}
                  verifiedStatus={feed.verified_status}
                />
              </View>
              <View style={{ flex: 1 }}>
                <TextName
                  type='Circular'
                  size='mini'
                  weight={500}
                  color={GUN_METAL}
                  user={feed}
                  centered={false}
                />
                <View>
                  <View style={{ paddingRight: WP2 }}>
                    {feed.job_title && (
                      <Text
                        numberOfLines={1}
                        type='Circular'
                        size='xmini'
                        weight={300}
                        color={SHIP_GREY_CALM}
                      >{`${feed.job_title.length <= textLimitProfession
                        ? feed.job_title
                        : `${feed.job_title.substring(
                          0,
                          !feed.company_name ? feed.job_title.length : textLimitProfession,
                        )}${feed.job_title.length > textLimitProfession ? '...' : ''}`
                        } ${feed.company_name ? `at ${feed.company_name}` : ''}`}</Text>
                    )}
                    <Text
                      numberOfLines={1}
                      type='Circular'
                      size='xmini'
                      weight={300}
                      color={SHIP_GREY_CALM}
                    >
                      {moment(feed.created_at)
                        .locale('id', { relativeTime })
                        .fromNow()
                        .replace('semenit', '1 menit')
                        .replace('sejam', '1 jam')
                        .replace('sehari', '1 hari')
                        .replace('seminggu', '1 minggu')
                        .replace('sebulan', '1 bulan')
                        .replace('setahun', '1 tahun')
                        .replace('yang lalu', 'lalu')
                        .replace('beberapa detik lalu', 'baru saja')}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>

            {this._renderTextContent()}

            {this._renderAttachment()}

            {!isTrending && this._renderSharedContent()}
          </TouchableOpacity>

          {!isTrending && isEmpty(linkPreviewParent) && (
            <View
              style={{
                marginHorizontal: this._getMarginHorizontal(),
              }}
            >
              <LinkPreviewCard
                {...this.props}
                text={feed.description}
                containerStyle={{ marginBottom: WP2 }}
              />
            </View>
          )}
        </View>
      </View>
    )
  }
}

FeedItemGeneralMultiple.propTypes = {}

FeedItemGeneralMultiple.defaultProps = {}

export default FeedItemGeneralMultiple
