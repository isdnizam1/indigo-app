import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import { WHITE, GREY_WARM } from '../../constants/Colors'
import { HP2, WP2, WP4, WP75 } from '../../constants/Sizes'
import { BORDER_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import Text from '../Text'
import Avatar from '../Avatar'

const propsType = {
  key: PropTypes.any,
  profilePicture: PropTypes.string,
  onPress: PropTypes.func
}

const propsDefault = {
  profilePicture: null,
  onPress: () => {
  },
  shadow: 'shadowThin'
}

class FeedPostButton extends React.Component {
  render() {
    const {
      profilePicture,
      onPress,
      onPressAvatar
    } = this.props
    return (
      <View
        style=
          {[
            {
              backgroundColor: WHITE,
              paddingHorizontal: WP4, paddingVertical: HP2
            }
          ]}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <Avatar onPress={onPressAvatar} image={profilePicture}/>
          <TouchableOpacity
            activeOpacity={TOUCH_OPACITY}
            style={{
              paddingVertical: WP2,
              paddingHorizontal: WP2,
              width: WP75,
              borderRadius: 100,
              ...BORDER_STYLE['rounded']
            }}
            onPress={onPress}
          >
            <Text size='mini' color={GREY_WARM}>What’s on your mind?</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

FeedPostButton.propTypes = propsType
FeedPostButton.defaultProps = propsDefault
export default FeedPostButton
