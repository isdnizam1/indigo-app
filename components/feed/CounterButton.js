import React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, Image } from "react-native";
import { isFunction } from "lodash-es";
import { WP2, WP4 } from "../../constants/Sizes";
import Text from "../Text";
import { TOUCH_OPACITY } from "../../constants/Styles";
import { REDDISH, SHIP_GREY_CALM } from "../../constants/Colors";

const propsType = {
  key: PropTypes.any,
  active: PropTypes.bool,
  counter: PropTypes.any,
  images: PropTypes.array,
  onPress: PropTypes.func,
  iconComponent: PropTypes.any,
  withImages: PropTypes.bool,
  disable: PropTypes.bool,
};

const propsDefault = {
  active: false,
  images: [
    require("../../assets/icons/icLikeActive.png"),
    require("../../assets/icons/icLikeInactive.png"),
  ],
  withImages: true,
  disable: false,
};

class CounterButton extends React.Component {
  render() {
    const {
      onPress,
      images,
      active,
      counter,
      style,
      value,
      iconComponent,
      withImages,
      disable,
    } = this.props;

    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY}
        disabled={!isFunction(onPress) || disable}
        style={[
          { flexDirection: "row", alignItems: "center", marginHorizontal: WP2 },
          style,
        ]}
        onPress={onPress}
      >
        {withImages && (
          <Image
            source={active ? images[0] : images[1]}
            style={{ width: WP4, aspectRatio: 1, marginRight: WP2 }}
          />
        )}
        {iconComponent && iconComponent}
        {counter && (
          <Text
            size="xmini"
            type="Circular"
            weight={400}
            color={active ? REDDISH : SHIP_GREY_CALM}
          >
            {counter}
          </Text>
        )}
        {value && (
          <Text
            size="xmini"
            type="Circular"
            weight={400}
            color={active ? REDDISH : SHIP_GREY_CALM}
          >
            {value}
          </Text>
        )}
      </TouchableOpacity>
    );
  }
}

CounterButton.propTypes = propsType;
CounterButton.defaultProps = propsDefault;
export default CounterButton;
