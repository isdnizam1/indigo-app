import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import Text from '../Text'
import { SHADOW_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { GREY_BLUE, GREY_WARM, WHITE } from '../../constants/Colors'
import { HP05, WP3, WP4, WP46 } from '../../constants/Sizes'
import Icon from '../Icon'
import ImageAuto from '../Image'
import Button from '../Button'

const propsType = {}
const propsDefault = {}

class FeedItemAdsBand extends React.Component {
  state = {
    feed: this.props.feed,
    additionalData: JSON.parse(this.props.feed.additional_data)
  }

  _onPressItem = () => {
    const {
      navigateTo,
      onRefresh
    } = this.props
    const {
      feed
    } = this.state
    navigateTo('AdsDetailScreenNoTab', {
      id_ads: feed.id_ads,
      onRefresh
    })
  }

  render() {
    const {
      shadow
    } = this.props
    const {
      feed
    } = this.state
    return <View />
    return (
      <View
        style={[
          shadow ? SHADOW_STYLE[shadow] : null,
          {
            backgroundColor: WHITE,
            marginBottom: HP05
          }
        ]}
      >
        <View style={{ padding: WP3 }}>
          <TouchableOpacity
            onPress={this._onPressItem}
            activeOpacity={TOUCH_OPACITY}
          >
            <View style={{ flexDirection: 'row' }}>
              <Icon size='xtiny' centered type='FontAwesome' name='share-square-o'/>
              <Text size='xtiny' weight={500}>Promoted</Text>
            </View>
            <View style={{ flexDirection: 'row', marginVertical: 2 }}>
              <ImageAuto
                imageStyle={{ width: WP46 }}
                aspectRatio={1.5}
                source={{ uri: feed.image }}
              />
              <View style={{ flex: 1, justifyContent: 'center', marginLeft: WP4 }}>
                <Text size='mini' weight={500}>{feed.title}</Text>
                <View
                  style={{ flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap', marginVertical: 2 }}
                  numberOfLines={2}
                >
                  <Text size='tiny' color={GREY_WARM}>Pop</Text>
                  <Icon centered color={GREY_WARM} type='Entypo' name='dot-single'/>
                  <Text size='tiny' color={GREY_WARM}>Deep House</Text>
                  <Icon centered color={GREY_WARM} type='Entypo' name='dot-single'/>
                  <Text size='tiny' color={GREY_WARM}>Deep House</Text>
                </View>
                <Text size='tiny' color={GREY_BLUE}>Jakarta</Text>
                <Button
                  rounded
                  onPress={this._onPressItem}
                  width='100%'
                  soundfren
                  centered
                  shadow='none'
                  textColor={WHITE}
                  textSize='tiny'
                  textWeight={500}
                  compact='center'
                  style={{ marginVertical: 0, marginTop: 10 }}
                  text='Check Out Their Works!'
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

FeedItemAdsBand.propTypes = propsType
FeedItemAdsBand.defaultProps = propsDefault
export default FeedItemAdsBand
