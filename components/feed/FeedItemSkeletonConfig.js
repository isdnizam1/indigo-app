import { HP1, WP105, WP17, WP2, WP25, WP3, WP35, WP4, WP40, WP5, WP50, WP505, WP6, WP60 } from '../../constants/Sizes'
import { WHITE } from '../../constants/Colors'

const styles = {
  container: {
    flexGrow: 1,
    backgroundColor: WHITE,
    paddingBottom: HP1,
    paddingHorizontal: WP6,
    alignItems: 'flex-start'
  }
}

const layout = {
  userName: {
    width: WP35,
    height: WP6,
    marginBottom: WP105
  },
  jobTitle: {
    width: WP50,
    height: WP3
  },
  textContent: (width = WP60) => ({
    width,
    height: WP4,
    marginBottom: WP105
  }),
  topic: {
    width: WP25,
    height: WP505,
    marginRight: WP3,
    borderRadius: 6,
    marginBottom: WP105
  },
  counterButton: {
    width: WP17, height: WP5, marginHorizontal: WP2, marginTop: -WP105
  },
  likeInfo: {
    width: WP40, height: WP5, marginHorizontal: WP2, marginTop: -WP105
  }
}

export default {
  layout,
  styles
}
