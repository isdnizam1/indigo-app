import React from 'react'
import PropTypes from 'prop-types'
import { FlatList, Keyboard, KeyboardAvoidingView, Platform, TouchableOpacity, View } from 'react-native'
import { isEmpty, noop } from 'lodash-es'
import { GREY, SILVER_CALMER, WHITE, TOMATO } from '../../constants/Colors'
import { FONT_SIZE, WP2, WP3, WP4, WP6 } from '../../constants/Sizes'
import Text from '../Text'
import Avatar from '../Avatar'
import Form from '../Form'
import { getMentionSuggestions } from '../../actions/api'
import MentionsTextInput from '../MentionTextInput'
import { BORDER_STYLE, TOUCH_OPACITY } from '../../constants/Styles'
import { isIphoneX, getBottomSpace } from '../../utils/iphoneXHelper'

const propsType = {
  key: PropTypes.any,
  profilePicture: PropTypes.string,
  onSubmit: PropTypes.func,
  dispatch: PropTypes.func
}

const propsDefault = {
  profilePicture: null,
  onSubmit: () => {
  },
  dispatch: () => {
  },
  onReply: () => {
  }
}

class FeedPostCommentForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      mentionedName: '',
      mentionSuggestions: [],
      comments: '',
      mentions: [],
      openPanel: false,
      isShowKeyboard: false,
    }
  }
  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => this.setState({ isShowKeyboard: true })
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => this.setState({ isShowKeyboard: false })
    )
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  setMention = (id, full_name) => {
    this.setState({
      comments: full_name
    })
    this.textInput._setText(`${full_name} `, { id, name: full_name })
    this.textInput.focus()
  }

  _getMentionSuggestion = async (full_name, triggerMatrix) => {
    if (!full_name) return
    full_name = full_name.substring(1, full_name.length)
    const {
      dispatch
    } = this.props

    const params = {
      start: 0,
      limit: 3,
      full_name
    }

    const suggestions = await dispatch(getMentionSuggestions, params, noop, true, false)
    this.setState({ mentionSuggestions: suggestions.result || [] })
  }

  _changeFormatOnSubmit = async (callbackFn) => {
    const {
      mentions,
      comments
    } = this.state
    let result = comments
    mentions.forEach((element) => {
      result = result.replace(element.name, `<mention id=${element.id}>${element.name}</mention>`)
    })
    callbackFn(result)
  }

  renderSuggestionsRow(suggestion, hidePanel, updateText) {
    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY} key={suggestion.id}
        onPress={() => this._onSelectSuggestion(suggestion, hidePanel, updateText)}
      >
        <View style={{
          padding: WP3,
          flexDirection: 'row',
          backgroundColor: WHITE,
          alignItems: 'center',
          ...BORDER_STYLE['top']
        }}
        >
          <Avatar image={suggestion.avatar}/>
          <Text style={{ marginLeft: WP2 }}>{suggestion.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  _onSelectSuggestion = (suggestion, hidePanel, updateText) => {
    hidePanel()
    const { comments } = this.state
    this.setState({ mentionSuggestions: [] })
    const formattedComment = comments.substring(0, comments.lastIndexOf('@')) + suggestion.name
    this.setState({ comments: formattedComment })
    updateText(formattedComment, suggestion)
  }

  render() {
    const {
      onSubmit
    } = this.props

    const {
      mentionSuggestions,
      openPanel,
      isShowKeyboard
    } = this.state

    return (
      <Form onSubmit={onSubmit}>
        {({ onChange, onSubmit, formData }) => (
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
          >
            {
              openPanel && (
                <FlatList
                  keyboardShouldPersistTaps={'handled'}
                  ListEmptyComponent={() => <View accessible={false}/>}
                  ItemSeparatorComponent={() => {
                    return <View/>
                  }}
                  enableEmptySections={true}
                  data={mentionSuggestions}
                  keyExtractor={(item, index) => {
                    if (item != null) {
                      return item.id
                    }
                  }}
                  renderItem={(rowData) => {
                    return this.renderSuggestionsRow(rowData.item, this.textInput.stopTracking.bind(this.textInput), this.textInput._setText.bind(this.textInput))
                  }}
                />
              )
            }

            <View
              style={{
                flexDirection: 'row', backgroundColor: WHITE,
                paddingVertical: WP2, paddingHorizontal: WP6,
                alignItems: 'center',
                marginBottom: !isShowKeyboard && isIphoneX ? getBottomSpace() : 0
              }}
            >
              <View style={{
                flex: 1,
                backgroundColor: SILVER_CALMER, borderRadius: WP6,
                paddingHorizontal: WP4, paddingTop: WP2, paddingBottom: WP2,
              }}
              >
                <MentionsTextInput
                  maxLength={150}
                  ref={(input) => {
                    this.textInput = input
                  }}
                  above
                  placeholder='Write a comment'
                  textInputStyle={{
                    fontSize: FONT_SIZE['mini'], color: GREY,
                    paddingVertical: 0,
                    paddingTop: 0,
                    paddingBottom: 0,
                    paddingHorizontal: 0,
                    maxHeight: (FONT_SIZE['mini'] * 5)
                  }}
                  suggestionsPanelStyle={{ backgroundColor: 'rgba(100,100,100,0.1)' }}
                  textInputMinHeight={30}
                  textInputMaxHeight={80}
                  onChangedSelected={(selected) => {
                    this.setState({ mentions: selected })
                  }}
                  trigger={'@'}
                  triggerLocation={'new-word-only'} // 'new-word-only', 'anywhere'
                  value={this.state.comments}
                  onChangeText={(val) => {
                    this.setState({ comments: val })
                  }}
                  onOpenSuggestionsPanel={() => {
                    this.setState({ openPanel: true })
                  }}
                  onCloseSuggestionsPanel={() => {
                    this.setState({ openPanel: false, mentionSuggestions: [] })
                  }}
                  triggerDelay={2}
                  triggerCallback={this._getMentionSuggestion}
                  renderSuggestionsRow={this.renderSuggestionsRow.bind(this)}
                  suggestionsData={mentionSuggestions} // array of objects
                  keyExtractor={(item, index) => {
                    if (item != null) {
                      return item.id
                    }
                  }}
                  suggestionRowHeight={45}
                  horizontal={false} // defaut is true, change the orientation of the list
                  MaxVisibleRowCount={5}
                />
              </View>
              {
                !isEmpty(this.state.comments) && (
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    style={{ flexGrow: 0, marginLeft: WP3 }}
                    onPress={async () => {
                      if (!this.state.comments) return
                      Keyboard.dismiss()
                      await this._changeFormatOnSubmit(onChange('comment'))
                      this.textInput.resetTextbox()
                      onSubmit()
                    }}
                  >
                    <Text type='NeoSans' size='xmini' weight={500} color={TOMATO}>Post</Text>
                  </TouchableOpacity>
                )
              }
            </View>
          </KeyboardAvoidingView>
        )}
      </Form>
    )
  }
}

FeedPostCommentForm.propTypes = propsType
FeedPostCommentForm.defaultProps = propsDefault
export default FeedPostCommentForm
