import { isString } from "util";
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  TouchableOpacity,
  View,
  TextInput,
  ScrollView,
  SafeAreaView,
  FlatList,
} from "react-native";
import {
  isFunction,
  isEqual,
  isEmpty,
  reduce,
  isArray,
  includes,
  noop,
} from "lodash-es";
import { connect } from "react-redux";
import { WP5 } from "sf-constants/Sizes";
import {
  WP2,
  WP4,
  HP100,
  WP3,
  WP105,
  WP308,
  WP24,
  HP50,
} from "../constants/Sizes";
import {
  WHITE,
  SHIP_GREY_CALM,
  NAVY_DARK,
  SHIP_GREY,
  PALE_BLUE_TWO,
  GREEN_SOFT,
  GREEN30,
} from "../constants/Colors";
import { TOUCH_OPACITY } from "../constants/Styles";
import Modal from "./Modal";
import Text from "./Text";
import Icon from "./Icon";
import ButtonV2 from "./ButtonV2";
import { CheckBox } from "react-native-elements/dist/checkbox/CheckBox";
import Image from "./Image";

const propsType = {
  triggerComponent: PropTypes.objectOf(PropTypes.any),
  keyword: PropTypes.string,
  onChange: PropTypes.func,
  header: PropTypes.string,
  suggestion: PropTypes.func,
  renderItem: PropTypes.func,
  reformatFromApi: PropTypes.func,
  suggestionKey: PropTypes.string,
  suggestionPathResult: PropTypes.string,
  suggestionPathValue: PropTypes.string,
  asObject: PropTypes.bool,
  placeholder: PropTypes.string,
  refreshOnSelect: PropTypes.bool,
  extraResult: PropTypes.bool,
  iconName: PropTypes.string,
  iconType: PropTypes.string,
  iconSize: PropTypes.string,
  selection: PropTypes.any,
  selectionKey: PropTypes.string,
  selectionWording: PropTypes.string,
  showSelection: PropTypes.bool,
  onRemoveSelection: PropTypes.func,
  quickSearchTitle: PropTypes.string,
  quickSearchList: PropTypes.any,
  quickSearchKey: PropTypes.string,
  actionNext: PropTypes.func,
  quickSearch: PropTypes.func,
  disabled: PropTypes.bool,
};

const propsDefault = {
  keyword: "",
  triggerComponent: {},
  onChange: () => {},
  header: "",
  suggestion: () => {},
  suggestionKey: "",
  suggestionPathResult: "",
  suggestionPathValue: "",
  asObject: false,
  createNew: true,
  placeholder: "",
  refreshOnSelect: false,
  extraResult: false,
  iconName: "",
  iconType: "AntDesign",
  iconSize: "small",
  selection: [],
  selectionWording: "",
  showSelection: false,
  onRemoveSelection: () => {},
  quickSearchTitle: "",
  quickSearchList: [],
  actionNext: () => {},
  quickSearch: noop,
  disabled: false,
};

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

class SelectModalFilterV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyword: "",
      inputKey: "",
      suggestions: [],
      suggestionsRecent: [],
      showSuggestion: false,
      selectedJob: this.props.selection.length == 0 ? [] : this.props.selection,
      modalHeight: HP100,
      quickSearchList: this.props.quickSearchList || [],
    };
  }

  componentDidMount = () => {
    this._getQuickSearch();
    this._onFetchSuggestion();
  };

  _onFocus = () => {
    this.setState({ showSuggestion: true }, this._onFetchSuggestion);
  };

  _onBlur = () => {
    this.setState({ showSuggestion: false });
  };

  _onChangeText = (text) => {
    this.setState({ keyword: text }, this._onFetchSuggestion);
  };

  _getQuickSearch = async () => {
    const { quickSearch } = this.props;
    try {
      if (isFunction(quickSearch)) {
        const { data } = await quickSearch({});
        await this.setState({
          quickSearchList: data.result,
        });
      }
    } catch (e) {
      // silent
    }
  };

  _onClearInput = () => {
    this._onFocus();
  };

  _onFetchSuggestion = () => {
    const {
      suggestion,
      suggestionKey,
      suggestionType,
      userData: { id_user },
      suggestionPathResult,
      extraResult,
      reformatFromApi,
      suggestionPathValue,
      id_province,
    } = this.props;
    const { keyword } = this.state;
    if (isFunction(suggestion)) {
      let params = {
        [suggestionKey]: keyword,
        id_user,
        limit: 6,
        start: 0,
      };
      suggestion(params)
        .then(({ data }) => {
          let suggestionData = data.result;
          if (reformatFromApi) {
            suggestionData = reduce(
              suggestionData,
              (result, item) => {
                result.push({
                  [suggestionPathResult]: reformatFromApi(
                    item[suggestionPathResult]
                  ),
                  [suggestionPathValue]: item[suggestionPathValue],
                });
                return result;
              },
              []
            );
          }
          let suggestionDataRecentlySearch = [];
          if (extraResult) {
            suggestionDataRecentlySearch = data.recentlySearch || [];
            if (!isEmpty(suggestionDataRecentlySearch)) {
              suggestionDataRecentlySearch = reduce(
                data.recentlySearch,
                (result, item) => {
                  result.push({
                    [suggestionPathResult]: reformatFromApi
                      ? reformatFromApi(item.value)
                      : item.value,
                    [suggestionPathValue]: item[suggestionPathValue],
                  });
                  return result;
                },
                []
              );
            }
          }
          this.setState({
            suggestions: suggestionData,
            suggestionsRecent: suggestionDataRecentlySearch,
            showSuggestion: true,
          });
        })
        .catch(() => {});
    }
  };

  _onPressSuggestion = (job) => async () => {
    const { selectedJob } = this.state;

    const isJobSelected = selectedJob.find(
      (item) => item.id_job === job.id_job
    );
    let selected = [...selectedJob];
    if (isJobSelected) {
      selected = selectedJob.filter((item) => item.id_job !== job.id_job);
    } else {
      selected.push(job);
    }
    this.setState({ selectedJob: selected });
  };

  // RENDER LIST
  _suggestionList(suggestions) {
    const { selectedJob } = this.state;
    return (
      <FlatList
        data={suggestions}
        renderItem={({ item }) => {
          return (
            <View
              style={{
                paddingHorizontal: WP4,
                paddingVertical: WP4,
                flexDirection: "row",
                borderBottomColor: PALE_BLUE_TWO,
                borderBottomWidth: 1,
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Text size="mini" type="Circular" color={SHIP_GREY} weight={300}>
                {item.job_title}
              </Text>
              <CheckBox
                checked={selectedJob.find((job) => job.id_job === item.id_job)}
                onPress={this._onPressSuggestion(item)}
                containerStyle={{
                  margin: 0,
                  padding: 0,
                  marginRight: WP4,
                }}
                checkedIcon={
                  <Image
                    source={require("../assets/icons/icCheckboxActiveGreen.png")}
                    aspectRatio={1}
                    imageStyle={{ width: WP5 }}
                  />
                }
                uncheckedIcon={
                  <Image
                    source={require("../assets/icons/icCheckboxInactive.png")}
                    aspectRatio={1}
                    imageStyle={{ width: WP5 }}
                  />
                }
              />
            </View>
          );
        }}
        keyExtractor={(item) => item.id}
      />
    );
  }
  // END RENDER LIST

  render() {
    const {
      triggerComponent,
      suggestionPathResult,
      placeholder,
      header,
      disabled,
      onChange,
    } = this.props;

    const { keyword, suggestions, suggestionsRecent, selectedJob } = this.state;
    return (
      <Modal
        renderModalContent={({ toggleModal }) => (
          <SafeAreaView
            style={{
              height: (HP50 * 12) / 10,
            }}
          >
            <View
              style={{
                backgroundColor: WHITE,
                height: (HP50 * 12) / 10,
                borderTopStartRadius: 50,
                borderTopEndRadius: 50,
              }}
            >
              <View style={{ flex: 1 }}>
                {/* header */}
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    paddingHorizontal: WP4,
                    paddingVertical: WP3,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Icon
                      centered
                      onPress={toggleModal}
                      background="dark-circle"
                      size="large"
                      color={SHIP_GREY_CALM}
                      name="chevron-left"
                      type="Entypo"
                    />
                    <Text
                      style={{ marginLeft: WP2 }}
                      type="Circular"
                      size="small"
                      weight={600}
                      color={NAVY_DARK}
                    >
                      {header}
                    </Text>
                  </View>
                  <Icon
                    centered
                    onPress={toggleModal}
                    background="dark-circle"
                    size="large"
                    color={SHIP_GREY_CALM}
                    name="close"
                    type="MaterialCommunityIcons"
                  />
                </View>

                {/* body */}
                <View
                  style={{ flex: 1, paddingHorizontal: WP4, paddingTop: WP2 }}
                >
                  {/* search input */}
                  <View
                    style={{
                      backgroundColor: "rgba(145, 158, 171, 0.1)",
                      borderRadius: 6,
                      flexDirection: "row",
                      alignItems: "center",
                      paddingHorizontal: WP2,
                      paddingVertical: WP105,
                      marginBottom: WP3,
                    }}
                  >
                    <Icon
                      centered
                      background="dark-circle"
                      size="large"
                      color={SHIP_GREY_CALM}
                      name="magnify"
                      type="MaterialCommunityIcons"
                    />
                    <TextInput
                      returnKeyType="search"
                      value={keyword}
                      numberOfLines={1}
                      style={{
                        flex: 1,
                        fontFamily: "CircularBook",
                        color: SHIP_GREY_CALM,
                        fontSize: WP308,
                        marginRight: WP2,
                        paddingVertical: WP105,
                      }}
                      onChangeText={this._onChangeText}
                      placeholderTextColor={SHIP_GREY_CALM}
                      onFocus={this._onFocus}
                      placeholder={placeholder}
                    />
                    {!isEmpty(keyword) && (
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({ keyword: "" }, this._onClearInput)
                        }
                      >
                        <Icon
                          centered
                          color={SHIP_GREY_CALM}
                          type="MaterialCommunityIcons"
                          name="close"
                          size="slight"
                        />
                      </TouchableOpacity>
                    )}
                  </View>

                  {/*Checkbox */}
                  {this._suggestionList(
                    suggestions,
                    toggleModal,
                    suggestionPathResult,
                    suggestionsRecent
                  )}
                </View>
              </View>

              {/* footer */}
              <View
                style={{
                  backgroundColor: WHITE,
                  flexDirection: "row",
                  width: "100%",
                  paddingHorizontal: WP4,
                  paddingVertical: WP5,
                  height: WP24,
                  alignItems: "center",
                }}
              >
                <ButtonV2
                  color={GREEN_SOFT}
                  textColor={GREEN30}
                  textSize={"slight"}
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: WP5,
                    marginRight: WP2,
                  }}
                  onPress={() => {}}
                  text={"Reset"}
                />
                <ButtonV2
                  color={GREEN30}
                  textColor={WHITE}
                  textSize={"slight"}
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    paddingHorizontal: WP5,
                  }}
                  onPress={() => {
                    onChange(selectedJob);
                    toggleModal();
                  }}
                  text={"Save"}
                />
              </View>
            </View>
          </SafeAreaView>
        )}
      >
        {({ toggleModal, isVisible }, M) => (
          <View style={{ flexGrow: 1 }}>
            <TouchableOpacity
              activeOpacity={disabled ? 1 : TOUCH_OPACITY}
              onPress={() => {
                if (!this.props.disabled) {
                  if (!isVisible) this._onFetchSuggestion();
                  toggleModal();
                }
              }}
            >
              <View pointerEvents="none">{triggerComponent}</View>
            </TouchableOpacity>
            {M}
          </View>
        )}
      </Modal>
    );
  }
}

SelectModalFilterV2.propTypes = propsType;

SelectModalFilterV2.defaultProps = propsDefault;

export default connect(mapStateToProps, {})(SelectModalFilterV2);
