import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import noop from 'lodash-es/noop'
import Modal from '../Modal'
import Image from '../Image'
import Text from '../Text'
import { HP1, HP3, WP4, WP40, WP5 } from '../../constants/Sizes'
import { GUN_METAL, REDDISH, SHIP_GREY_CALM, WHITE } from '../../constants/Colors'

const styles = {
  image: {
    width: WP40,
    aspectRatio: 1,
    marginBottom: WP5
  },
  contentWrapper: {
    padding: WP5
  },
  title: {
    marginBottom: WP5
  },
  content: {
    lineHeight: HP3
  }
}

class PopUpInfo extends Component {
  _renderContent = ({ toggleModal }) => {
    const { template, onPress } = this.props

    return (
      <View>
        <View style={styles.contentWrapper}>
          <Image
            imageStyle={styles.image}
            source={template.image}
          />
          <Text type='Circular' weight={500} size='large' color={GUN_METAL} centered style={styles.title}>
            {template.title}
          </Text>
          <Text type='Circular' weight={200} size='mini' color={SHIP_GREY_CALM} centered style={styles.content}>
            {template.content}
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            toggleModal()
            onPress()
          }}
          style={{ backgroundColor: REDDISH, padding: WP4 }}
        >
          <Text color={WHITE} type='Circular' weight={500} centered>{template.cta || 'Lanjutkan'}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    const {
      children, isVisible, closeOnBackdrop
    } = this.props

    return (
      <Modal
        renderModalContent={this._renderContent}
        position='center'
        style={{
          borderBottomRightRadius: HP1,
          borderBottomLeftRadius: HP1,
          overflow: 'hidden'
        }}
        swipeDirection='none'
        isVisible={isVisible}
        closeOnBackdrop={closeOnBackdrop}
      >
        {
          ({ toggleModal }, M) => (
            <>
              <TouchableOpacity
                onPress={toggleModal}
              >
                {children}
              </TouchableOpacity>
              {M}
            </>
          )
        }

      </Modal>
    )
  }
}

PopUpInfo.propTypes = {
  template: PropTypes.objectOf(PropTypes.any),
  onPress: PropTypes.func,
  isVisible: PropTypes.bool
}

PopUpInfo.defaultProps = {
  template: {},
  onPress: noop
}

export default PopUpInfo
