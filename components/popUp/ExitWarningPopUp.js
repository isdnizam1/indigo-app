import React from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import noop from 'lodash-es/noop'
import Text from '../Text'
import { GUN_METAL, PALE_GREY, REDDISH, SHIP_GREY_CALM, WHITE } from '../../constants/Colors'
import { HP1, WP3, WP4, WP6, WP8 } from '../../constants/Sizes'
import Modal from '../Modal'

const styles = {
  title: {
    marginBottom: WP4
  },
  content: {
    marginBottom: WP8
  },
  button: {
    borderRadius: 10,
    paddingVertical: WP4,
    marginBottom: WP3
  }
}

const ActionButton = ({ label, onPress, backgroundColor, textColor }) => (
  <TouchableOpacity
    onPress={onPress}
    style={{
      backgroundColor,
      ...styles.button
    }}
  >
    <Text type='Circular' color={textColor} centered weight={400} size='mini'>
      {label}
    </Text>
  </TouchableOpacity>
)

const ExitWarningPopUpContent = ({ onContinue, onCancel, template }) => {
  return (
    <View style={{
      padding: WP6,
      paddingBottom: WP3,
      borderRadius: 10,
    }}
    >
      <Text centered type='Circular' color={GUN_METAL} weight={600} style={styles.title} size='medium'>
        {template.title}</Text>
      <Text centered type='Circular' color={SHIP_GREY_CALM} weight={300} style={styles.content} size='mini'>
        {template.content}
      </Text>

      <ActionButton
        label='Lanjutkan'
        onPress={onContinue}
        backgroundColor={REDDISH}
        textColor={WHITE}
      />
      <ActionButton
        label='Batalkan'
        onPress={onCancel}
        backgroundColor={PALE_GREY}
        textColor={SHIP_GREY_CALM}
      />

    </View>
  )
}

const ExitWarningPopUp = (props) => {
  if (!props.isVisible) {
    return <View/>
  }

  return (
    <Modal
      renderModalContent={() => (
        <ExitWarningPopUpContent
          {...props}
        />
      )}
      position='center'
      style={{
        borderBottomRightRadius: HP1,
        borderBottomLeftRadius: HP1,
        overflow: 'hidden'
      }}
      swipeDirection='none'
      isVisible
      closeOnBackdrop={true}
      closeBackdropCallback={props.onContinue}
    >
      {
        ({ toggleModal }, M) => (
          M
        )
      }
    </Modal>
  )
}

ExitWarningPopUp.propTypes = {
  onContinue: PropTypes.func,
  onCancel: PropTypes.func,
  template: PropTypes.objectOf(PropTypes.any),
  isVisible: PropTypes.bool
}

ExitWarningPopUp.defaultProps = {
  onContinue: noop,
  onCancel: noop,
  template: {}
}

export default ExitWarningPopUp
