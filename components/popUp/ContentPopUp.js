import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, ScrollView, TouchableOpacity, View } from 'react-native'
import RNModal from 'react-native-modal'
import Text from '../Text'
import { GUN_METAL, NAVY_DARK, NO_COLOR, PALE_BLUE_TWO, SHIP_GREY, SHIP_GREY_CALM, WHITE } from '../../constants/Colors'
import { HP40, WP1, WP10, WP2, WP3, WP5 } from '../../constants/Sizes'
import { MODAL_STYLE } from '../../constants/Styles'
import Icon from '../Icon'

const STYLES = {
  container: {
    backgroundColor: WHITE,
  },
  section: {
    paddingVertical: WP5,
    paddingHorizontal: WP5,
    borderBottomColor: PALE_BLUE_TWO,
    borderBottomWidth: .5
  },
  contentWrapper: {
    paddingBottom: 0
  },
  contentItem: {
    flexDirection: 'row',
    marginBottom: WP3
  },
  closeWrapper: {
    flexDirection: 'row',
    alignItems: 'center'
  }
}

class ContentPopUp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isVisible: this.props.isVisible
    }
  }

  openModal = () => {
    this.setState({ isVisible: true })
  }

  closeModal = () => {
    this.setState({ isVisible: false })
  }

  _renderContent = () => {
    const { content, list, contentStyle } = this.props

    return (
      <ScrollView style={[STYLES.section, { maxHeight: HP40 }]}>
        <View style={[STYLES.contentWrapper, contentStyle]}>
          {
            content.map((item, index) => (
              <View style={STYLES.contentItem} key={index}>
                { list &&
                  <Icon size='large' style={{ marginTop: WP1, marginLeft: -WP2 }} color={PALE_BLUE_TWO} type='Entypo' name='dot-single'/>
                }
                <Text type='Circular' color={SHIP_GREY} size='mini'>{item}</Text>
              </View>
            ))
          }
        </View>
      </ScrollView>
    )
  }

  render() {
    const {
      isVisible
    } = this.state
    const {
      title
    } = this.props

    return (
      <RNModal
        style={[MODAL_STYLE.bottom]}
        isVisible={isVisible}
        onBackdropPress={this.closeModal}
        onHardwareBackPress={this.closeModal}
        propagateSwipe={Platform.OS === 'ios'}
        avoidKeyboard={Platform.OS === 'ios'}
      >
        <View style={STYLES.container}>
          <View style={STYLES.section}>
            <Text type='Circular' color={NAVY_DARK} weight={700} size='medium'>{title}</Text>
          </View>
          {this._renderContent()}
          <TouchableOpacity style={[STYLES.section, STYLES.closeWrapper]} onPress={this.closeModal}>
            <View style={{ width: WP10 }}>
              <Icon
                size={'huge'}
                color={SHIP_GREY_CALM}
                name={'close'}
                type={'MaterialCommunityIcons'}
                background={'none'}
                backgroundColor={NO_COLOR}
              />
            </View>
            <Text type='Circular' color={GUN_METAL} size='small'>Tutup</Text>
          </TouchableOpacity>
        </View>
      </RNModal>
    )
  }
}

ContentPopUp.propTypes = {
  isVisible: PropTypes.bool,
  content: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string,
  list: PropTypes.bool,
  contentStyle: PropTypes.objectOf(PropTypes.any)
}

ContentPopUp.defaultProps = {
  isVisible: false,
  content: [],
  title: '',
  list: false,
  contentStyle: {}
}

export default ContentPopUp
