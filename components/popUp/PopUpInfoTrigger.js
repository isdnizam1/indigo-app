import React, { Component } from 'react'
import { noop, isFunction } from 'lodash-es'
import { View, StyleSheet, Modal as ModalRN, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { GUN_METAL, REDDISH, SHIP_GREY, WHITE } from '../../constants/Colors'
import { WP1, WP2, WP4, WP40, WP502, WP6, WP606, WP85 } from '../../constants/Sizes'
import Icon from '../Icon'
import Image from '../Image'
import Text from '../Text'
import Touchable from '../Touchable'

const borderRadius = WP2
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentWrapper: {
    alignItems: 'center',
    width: WP85,
    justifyContent: 'center',
    backgroundColor: WHITE,
    borderRadius,
    overflow: 'hidden',
  }
})

class PopUpInfoTrigger extends Component {
  constructor(props) {
    super(props)
    this.state = {
      visibility: false
    }
  }

  _show = () => {
    this.setState({ visibility: true })
  }

  _hide = () => {
    this.setState({ visibility: false })
  }

  render() {
    const { visibility } = this.props.visibility ? this.props.visibility : this.state
    const { template } = this.props
    const isCancel = template?.isCancel || false,
      animation = template?.animation || 'fade',
      fullImage = template?.fullImage || true,
      aspectRatio = template?.aspectRatio || 1,
      closeColor = template?.closeColor || WHITE,
      multipleContent = template?.multipleText || true,
      buttonTitle = template?.cta || 'Tutup'
    return (
      <ModalRN
        transparent
        animationType={animation}
        onRequestClose={isCancel ? this._hide : noop}
        visible={visibility}
      >
        <TouchableOpacity
          activeOpacity={1}
          onPress={this._hide}
          disabled={!isCancel}
          style={styles.container}
        >
          <TouchableOpacity disabled={true}>
            <View style={styles.contentWrapper}>
              <View style={{ marginBottom: WP4 }}>
                <View style={{ padding: !fullImage ? WP6 : 0 }}>
                  <Image
                    source={template.image}
                    imageStyle={[{ width: fullImage ? WP85 : WP40 }]}
                    aspectRatio={aspectRatio}
                  />
                </View>
                <Touchable
                  style={{ position: 'absolute', top: WP1, right: WP1 }}
                  onPress={this._hide}
                >
                  <Icon
                    type='MaterialCommunityIcons'
                    name='close'
                    size='large'
                    background='dark-circle'
                    color={closeColor}
                  />
                </Touchable>
              </View>

              <View style={{ paddingHorizontal: WP6, paddingBottom: WP6 }}>
                <Text lineHeight={WP606} type='Circular' weight={600} size='small' color={GUN_METAL} centered style={{ marginBottom: WP4 }}>
                  {template.title}
                </Text>
                {
                  multipleContent ?
                    (
                      <Text lineHeight={WP502} type='Circular' weight={300} size='mini' color={SHIP_GREY} centered>
                        {
                          template?.content?.length > 0 ? (
                            template?.content.map((item, index) => {
                              const isBold = item.style == 'bold'
                              return (
                                <Text key={`${index + Math.random()}`} type='Circular' weight={isBold ? 500 : 300} size='mini' color={SHIP_GREY} centered>{item.text}</Text>
                              )
                            })
                          ) : ''
                        }
                      </Text>
                    ) : (
                      <Text lineHeight={WP502} type='Circular' weight={300} size='mini' color={SHIP_GREY} centered>
                        {template.content}
                      </Text>
                    )
                }
              </View>

              <TouchableOpacity
                onPress={() => {
                  if(isFunction(template.onPress)) template.onPress()
                  setTimeout(() => {
                    this._hide()
                  }, 500)
                }}
                style={{ width: '100%', backgroundColor: REDDISH, padding: WP4 }}
              >
                <Text color={WHITE} size='small' type='Circular' lineHeight={WP6} weight={500} centered>{buttonTitle}</Text>
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        </TouchableOpacity>
      </ModalRN >
    )
  }
}

PopUpInfoTrigger.propTypes = {
  template: PropTypes.objectOf(PropTypes.any),
}

PopUpInfoTrigger.defaultProps = {
  template: {},
}

export default PopUpInfoTrigger
