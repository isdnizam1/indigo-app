import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Animated, Easing, View } from 'react-native'
import { WP05, WP100 } from '../../constants/Sizes'
import { PALE_GREY, REDDISH } from '../../constants/Colors'

class ProgressBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isVisible: this.props.isVisible,
      progress: new Animated.Value(this.props.initialValue),
    }
  }

  _animatedProgress = (value) => {
    Animated.timing(this.state.progress, {
      toValue: value,
      duration: 350,
      delay: 500,
      easing: Easing.ease,
      useNativeDriver: false
    }).start(() => {

    })
  };

  render() {
    return (
      <View
        style={{ width: WP100, height: WP05, backgroundColor: PALE_GREY }}
      >
        <Animated.View
          style={{
            height: WP05,
            backgroundColor: REDDISH,
            width: this.state.progress,
          }}
        />
      </View>
    )
  }
}

ProgressBar.propTypes = {
  initialValue: PropTypes.number,
  isVisible: PropTypes.bool,
}

ProgressBar.defaultProps = {
  initialValue: 0,
  isVisible: false
}

export default ProgressBar
