import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Animated, View } from 'react-native'
import { SHADOW_STYLE } from 'sf-constants/Styles'
import { PALE_SALMON20, REDDISH, SHIP_GREY_CALM, WHITE } from '../../constants/Colors'
import { WP2, WP305, WP4 } from '../../constants/Sizes'
import Icon from '../Icon'
import Text from '../Text'

class SnackBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isVisible: false,
      fadeAnim: new Animated.Value(0),
    }
  }

  componentDidMount = async () => {
  }

  _fadeIn = () => {
    const { showDuration } = this.props
    this.setState({ isVisible: true })
    Animated.timing(this.state.fadeAnim, {
      toValue: 1,
      duration: 500,
      delay: 100,
      useNativeDriver: true
    }).start(() => {
      setTimeout(() => {
        if (showDuration) {
          this._fadeOut()
        }
      }, showDuration)
    })
  };

  _fadeOut = () => {
    Animated.timing(this.state.fadeAnim, {
      toValue: 0,
      duration: 500,
      delay: 100,
      useNativeDriver: true
    }).start(() => {
      this.setState({ isVisible: false })
    })
  };

  render() {
    const { label, iconName, rightComponent, style } = this.props
    const { fadeAnim, isVisible } = this.state
    if (!isVisible) return null
    return (
      <View style={{
        overflow: 'hidden', paddingBottom: 5,
        position: 'absolute',
        width: '100%',
        top: 0,
        left: 0,
        right: 0,
        zIndex: 99999,
      }}
      >
        <Animated.View
          style={[{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: WHITE,
            paddingVertical: WP305,
            paddingHorizontal: WP4,
            opacity: fadeAnim,
            ...SHADOW_STYLE.shadow
          }, style
          ]}
        >
          <View
            style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}
          >
            <Text
              weight={300}
              type='Circular'
              size={'slight'}
              color={SHIP_GREY_CALM}
            >
              {label}
            </Text>
            {iconName && (
              <Icon
                style={{ marginLeft: WP2 }}
                background='dark-circle'
                backgroundColor={PALE_SALMON20}
                size='xtiny'
                color={REDDISH}
                name={iconName}
                type='MaterialCommunityIcons'
              />
            )}
          </View>
          {rightComponent}
        </Animated.View>
      </View>
    )
  }
}

SnackBar.propTypes = {
  showDuration: PropTypes.number,
  label: PropTypes.string,
  iconName: PropTypes.string,
  rightComponent: PropTypes.objectOf(PropTypes.any),
  style: PropTypes.objectOf(PropTypes.any),
}

SnackBar.defaultProps = {
  label: '',
  showDuration: 5000,
  style: {}
}

export default SnackBar
