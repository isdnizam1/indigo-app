import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native-animatable'
import { WP100 } from '../constants/Sizes'
import { PALE_BLUE } from '../constants/Colors'

const HorizontalLine = ({ color, width, style }) => (
  <View style={[{ backgroundColor: color, width, height: 1 }, style]} />
)

HorizontalLine.propTypes = {
  color: PropTypes.string,
  width: PropTypes.number,
  style: PropTypes.any,
}

HorizontalLine.defaultProps = {
  color: PALE_BLUE,
  width: WP100,
}

export default HorizontalLine
