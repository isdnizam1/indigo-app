import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { connect } from 'react-redux'
import Modal from 'react-native-modal'
import {
  HP2, WP4, WP30, WP100, WP40
} from '../constants/Sizes'
import {
  GREY,
  WHITE,
  ORANGE,
  GREY_WARM
} from '../constants/Colors'
import Text from './Text'
import Button from './Button'
import Image from './Image'

const propsType = {
  dismiss: PropTypes.func,
  submit: PropTypes.func,
  modalVisible: PropTypes.bool,
}

const propsDefault = {
  submit: () => { },
  dismiss: () => { },
  modalVisible: false,
}

const mapStateToProps = ({ auth }) => ({
  userData: auth.user
})

class SubmitModal extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { modalVisible, dismiss, submit } = this.props

    return (
      <View style={{ flexGrow: 1 }}>
        <Modal
          onBackButtonPress={dismiss}
          onBackdropPress={dismiss}
          isVisible={modalVisible}
          animationType={'slide'}
          animationIn='slideInUp'
          animationOut='slideOutDown'
          style={{
            margin: 0,
            justifyContent: 'flex-end',
          }}
        >
          <View
            style={{
              backgroundColor: WHITE,
              width: WP100,
              padding: WP4,
              justifyContent: 'center',
            }}
          >
            <Image
              source={require('./../assets/images/areYouSure.png')}
              style={{ marginVertical: HP2 }}
              imageStyle={{ height: WP30 }}
              aspectRatio={150 / 74}
            />
            <Text
              centered
              type='NeoSans'
              color={GREY}
              size='large'
              weight={500}
              style={{ marginHorizontal: WP4 }}

            >
                            Are you sure about this post?
            </Text>
            <Text
              centered size='tiny' color={GREY_WARM}
              style={{ marginVertical: HP2, marginHorizontal: WP4 }}
            >
                          Please make sure again, everything on this post is already correct?
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginVertical: HP2,
                alignItems: 'center',
                justifyContent: 'space-around'
              }}
            >
              <Button
                onPress={() => {
                  dismiss()
                }}
                width={WP40}
                shadow='none'
                backgroundColor={WHITE}
                centered
                bottomButton
                radius={WP4}
                textType='NeoSans'
                textSize='small'
                textColor={ORANGE}
                textWeight={500}
                text='Check again'
                contentStyle={{
                  borderWidth: 1,
                  borderColor: ORANGE,
                }}
              />
              <Button
                onPress={() => {
                  dismiss()
                  submit()
                }}
                width={WP40}
                backgroundColor={ORANGE}
                centered
                bottomButton
                radius={WP4}
                shadow='none'
                textType='NeoSans'
                textSize='small'
                textColor={WHITE}
                textWeight={500}
                text='Yes'
              />
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

SubmitModal.propTypes = propsType

SubmitModal.defaultProps = propsDefault

export default connect(mapStateToProps, {})(SubmitModal)
