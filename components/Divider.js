import React from 'react'
import { View } from 'react-native'
import { PALE_BLUE } from 'sf-constants/Colors'

class Divider extends React.PureComponent {

  render() {
    const { color = PALE_BLUE, size = 1 } = this.props
    return (
      <View
        style={{
          height: size,
          backgroundColor: color
        }}
      />
    )
  }

}

export default React.memo(Divider)
