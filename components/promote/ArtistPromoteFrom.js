import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import { cloneDeep } from 'lodash-es'
import Text from '../Text'
import { HP1, HP105, HP2, WP1, WP2 } from '../../constants/Sizes'
import { BLACK, GREY, GREY_CALM } from '../../constants/Colors'
import Icon from '../Icon'
import InputModal from '../InputModal'
import { getMentionSuggestions } from '../../actions/api'
import { BORDER_COLOR, BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'
import RequiredMark from '../RequiredMark'

class ArtistPromoteFrom extends Component {
  _onAdd = (value) => {
    const members = cloneDeep(this.props.members)
    members.push(value)
    this.props.onChange(members)
  }

  _onDelete = (index) => {
    const members = cloneDeep(this.props.members)
    members.splice(index, 1)
    this.props.onChange(members)
  }

  _memberItem = (member, index) => {
    return (
      <View
        key={index}
        style={{
          backgroundColor: GREY_CALM, flexGrow: 0, marginRight: WP2, marginVertical: WP1,
          paddingHorizontal: WP2, paddingVertical: WP1, borderRadius: WP1
        }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text size='slight' color={BLACK}>{member.name}</Text>
          <TouchableOpacity activeOpacity={TOUCH_OPACITY} onPress={() => this._onDelete(index)}>
            <Icon size='slight' name='close' color={BLACK} style={{ marginLeft: 5 }}/>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    const {
      required, boxed, members
    } = this.props

    return (
      <View style={{ width: '100%' }}>
        <View
          style={[
            { marginBottom: boxed ? HP2 : HP1, marginTop: boxed ? 0 : HP105, borderColor: BORDER_COLOR },
            boxed ? { borderWidth: BORDER_WIDTH, padding: WP2, borderRadius: 5 } : {}
          ]}
        >
          <View style={{ flexDirection: 'row', marginBottom: WP2 }}>
            <Text size={boxed ? 'tiny' : 'mini'} color={GREY} weight={400}>Members</Text>
            {
              required && <RequiredMark/>
            }
          </View>
          <View style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            alignItems: 'center',
            borderColor: BORDER_COLOR,
            borderWidth: BORDER_WIDTH,
            borderRadius: 2,
            padding: WP2
          }}
          >
            {
              members.map((member, index) => (
                this._memberItem(member, index)
              ))
            }

            <InputModal
              triggerComponent={(
                <View style={{
                  borderColor: BORDER_COLOR,
                  borderWidth: BORDER_WIDTH,
                  paddingHorizontal: WP2 - 2,
                  paddingVertical: WP1 - 2,
                  borderRadius: WP1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexGrow: 0
                }}
                >
                  <Icon size='mini' color={GREY} name='plus' style={{ alignSelf: 'center', marginRight: 5 }}/>
                  <Text size='xmini' color={GREY}>ADD Member</Text>
                </View>
              )}
              header='Select Member'
              suggestion={getMentionSuggestions}
              suggestionKey='full_name'
              suggestionPathResult='name'
              suggestionPathValue='id'
              onChange={this._onAdd}
              suggestionCreateNewOnEmpty={false}
              selected=''
              asObject={true}
              createNew={false}
              refreshOnSelect={true}
              placeholder='Search member here...'
            />
          </View>
        </View>
      </View>
    )
  }
}

ArtistPromoteFrom.propTypes = {
  required: PropTypes.bool,
  boxed: PropTypes.bool,
  members: PropTypes.arrayOf(PropTypes.any),
  onChange: PropTypes.func
}

ArtistPromoteFrom.defaultProps = {
  required: false,
  boxed: false,
  members: [],
  onChange: () => {
  }
}

export default ArtistPromoteFrom
