import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Keyboard, TouchableOpacity, View } from 'react-native'
import { cloneDeep, isEmpty } from 'lodash-es'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { HP1, HP105, HP2, WP1, WP10, WP100, WP2, WP3, WP4, WP6 } from '../../constants/Sizes'
import { BLACK, GREY, NO_COLOR, ORANGE, WHITE } from '../../constants/Colors'
import Text from '../Text'
import Icon from '../Icon'
import Modal from '../Modal'
import InputTextLight from '../InputTextLight'
import Button from '../Button'
import { BORDER_COLOR, BORDER_STYLE, BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'
import RNPicker from '../RNPicker'
import RequiredMark from '../RequiredMark'

const socMedPlaceholder = {
  youtube: {
    label: 'Channel Name',
    username: 'Soundfren Official',
    link: 'youtube.com/user/soundfrenofficial'
  },
  instagram: {
    label: 'Username',
    username: '@soundfren',
    link: 'instagram.com/soundfren'
  },
  facebook: {
    label: 'Facebook Page',
    username: 'Soundfren Official',
    link: 'facebook.com/soundfren_official'
  },
  twitter: {
    label: 'Username',
    username: '@soundfren',
    link: 'twitter.com/soundfren'
  }
}

class SocmedPromoteForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: 'youtube',
      link: '',
      username: '',
      modalHeight: '90%',
      exists: false
    }
  }

  componentDidMount = () => {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        this.setState({ modalHeight: '50%' })
      }
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        this.setState({ modalHeight: '90%' })
      }
    )
    this._resetState()
  }

  _onChange = (key) => (value) => {
    this.setState({ [key]: value })
  }

  _onDelete = (index) => {
    const socialMedia = cloneDeep(this.props.socialMedia)
    socialMedia.splice(index, 1)
    this.props.onChange(socialMedia)
  }

  _onAdd = () => {
    const {
      socialMedia, onChange
    } = this.props

    const {
      name, link, username
    } = this.state

    const newSocialMedia = cloneDeep(socialMedia)
    newSocialMedia.push({ name, username, link })
    onChange(newSocialMedia)
    this._resetState()
  }

  _onEdit = () => {
    const {
      socialMedia, onChange
    } = this.props

    const {
      name, link, username
    } = this.state

    const newSocialMedia = cloneDeep(socialMedia)
    const filteredNewSocialMedia = newSocialMedia.map((item) => item.name == name ? { name, link, username } : item)
    onChange(filteredNewSocialMedia)
    this._resetState()
  }

  _resetState = () => {
    this.setState({ name: 'youtube', username: '', link: '' })
    this.props.socialMedia.map((item) => {
      const { username, link } = item
      item.name === 'youtube' && this.setState({ exists: true, username, link })
    })
  }

  _onSocialSelected = (name, index) => {
    const { socialMedia } = this.props
    const currentSocials = socialMedia.map((item) => item.name)
    const exists = currentSocials.indexOf(name) >= 0
    this.setState({ name, username: '', link: '', exists }, () => {
      exists && socialMedia.map((item) => {
        const { username, link } = item
        item.name === name && this.setState({ username, link })
      })
    })
  }

  render() {
    const {
      required, boxed, socialMedia
    } = this.props

    const {
      name, link, username, exists
    } = this.state

    return (
      <View>
        <View
          style={[
            { marginBottom: boxed ? HP2 : HP1, marginTop: boxed ? 0 : HP105, borderColor: BORDER_COLOR },
            boxed ? { borderWidth: BORDER_WIDTH, padding: WP2, borderRadius: 5 } : {}
          ]}
        >
          <Modal
            isVisible={false}
            position='center'
            renderModalContent={({ toggleModal, payload }) => (
              <View style={{
                backgroundColor: WHITE,
                padding: WP4
              }}
              >
                <View style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  ...BORDER_STYLE['bottom'],
                  marginBottom: 5,
                  paddingBottom: WP4
                }}
                >
                  <Icon
                    onPress={() => {
                      this._resetState()
                      toggleModal()
                    }}
                    size='slight' color={BLACK} name='left'
                  />
                  <Text weight={400}>Social Media</Text>
                  <Icon size='slight' color={NO_COLOR} name='left'/>
                </View>

                <KeyboardAwareScrollView>

                  <View style={{ paddingHorizontal: WP4, paddingTop: WP4 }}>
                    <View style={{ flexDirection: 'row', marginBottom: 3 }}>
                      <Text size={boxed ? 'tiny' : 'mini'} color={GREY} weight={400}>Platform</Text>
                      {
                        required && <RequiredMark/>
                      }
                    </View>

                    <RNPicker
                      onValueChange={this._onSocialSelected.bind(this)}
                      Icon={() => <Icon size='xtiny' color={GREY} name='caretdown' style={{ alignSelf: 'center', marginTop: 8, marginRight: 5 }}/>}
                      useNativeAndroidPickerStyle={false}
                      onValueChange={this._onSocialSelected.bind(this)}
                      items={[
                        { label: 'Youtube', value: 'youtube' },
                        { label: 'Instagram', value: 'instagram' },
                        { label: 'Twitter', value: 'twitter' },
                        { label: 'Facebook', value: 'facebook' }
                      ]}
                    />

                    <InputTextLight
                      style={{ width: '100%' }}
                      value={username}
                      label={socMedPlaceholder[name].label}
                      labelWeight={400}
                      labelColor={GREY}
                      placeholder={`ex: ${socMedPlaceholder[name].username}`}
                      onChangeText={this._onChange('username')}
                    />

                    <InputTextLight
                      style={{ width: '100%' }}
                      value={link}
                      label='URL Link'
                      labelWeight={400}
                      labelColor={GREY}
                      placeholder={`ex: ${socMedPlaceholder[name].link}`}
                      onChangeText={this._onChange('link')}
                    />

                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>

                      <Button
                        onPress={() => {
                          exists && this._onEdit()
                          !exists && this._onAdd()
                          toggleModal()
                        }}
                        contentStyle={{ paddingHorizontal: WP6, paddingVertical: WP3, flexGrow: 0 }}
                        soundfren
                        rounded
                        centered
                        text={exists ? 'UPDATE SOCIAL MEDIA' : 'ADD SOCIAL MEDIA'}
                        textColor={WHITE}
                        textSize='mini'
                        textWeight={500}
                        shadow='none'
                      />
                    </View>

                  </View>
                </KeyboardAwareScrollView>
              </View>
            )}
          >
            {({ toggleModal }, M) => (
              <Fragment>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: WP2 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Text size={boxed ? 'tiny' : 'mini'} color={GREY} weight={400}>Social Media</Text>
                    {
                      required && <RequiredMark/>
                    }
                  </View>
                  {
                    !isEmpty(socialMedia) && (
                      <TouchableOpacity
                        activeOpacity={TOUCH_OPACITY} onPress={() => {
                          this._resetState()
                          toggleModal()
                        }}
                      >
                        <Text color={ORANGE} size='mini'>Add more</Text>
                      </TouchableOpacity>
                    )
                  }
                </View>
                <View style={{ flexDirection: 'row' }}>
                  {
                    isEmpty(socialMedia) && (
                      <TouchableOpacity
                        activeOpacity={TOUCH_OPACITY} style={{
                          borderColor: ORANGE,
                          borderWidth: 2,
                          paddingHorizontal: WP2 - 2,
                          paddingVertical: WP1 - 2,
                          borderRadius: WP10,
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'center',
                          flexGrow: 0
                        }}
                        onPress={toggleModal}
                      >
                        <Icon size='mini' color={ORANGE} name='plus' style={{ alignSelf: 'center', marginRight: 5 }}/>
                        <Text size='xmini' color={ORANGE}>ADD</Text>
                      </TouchableOpacity>
                    )
                  }
                </View>
                {M}
              </Fragment>
            )}
          </Modal>
          <View>
            {
              socialMedia.map((item, index) => (
                <View
                  key={index}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    ...BORDER_STYLE['bottom'],
                    alignItems: 'center',
                    paddingVertical: WP2
                  }}
                >
                  <View style={{ flexDirection: 'row' }}>
                    <Text>{item.name}</Text>
                    <Text color={ORANGE} style={{ marginLeft: WP3 }}>{item.username}</Text>
                  </View>
                  <TouchableOpacity
                    activeOpacity={TOUCH_OPACITY}
                    style={{ backgroundColor: GREY, borderRadius: WP100, flexGrow: 0, padding: 4 }}
                    onPress={() => this._onDelete(index)}
                  >
                    <Icon size='mini' name='close' color={WHITE}/>
                  </TouchableOpacity>
                </View>
              ))
            }
          </View>
        </View>
      </View>
    )
  }
}

SocmedPromoteForm.propTypes = {
  required: PropTypes.bool,
  boxed: PropTypes.bool,
  onChange: PropTypes.func,
  socialMedia: PropTypes.arrayOf(PropTypes.any)
}

SocmedPromoteForm.defaultProps = {
  required: false,
  boxed: false,
  onChange: () => {
  },
  socialMedia: []
}

export default SocmedPromoteForm
