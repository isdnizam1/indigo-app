import { StyleSheet } from 'react-native'
import { GREY_DISABLE, ORANGE_BRIGHT, PINK_RED } from '../../constants/Colors'
import { WP2, WP3 } from '../../constants/Sizes'
import { BORDER_STYLE } from '../../constants/Styles'

export const invalidButtonStyle = {
  buttonColor: [GREY_DISABLE, GREY_DISABLE],
  disabled: true
}

export const validButtonStyle = {
  buttonColor: [PINK_RED, ORANGE_BRIGHT],
  disabled: false
}

export const promoteStyle = StyleSheet.create({
  sectionPreview: {
    marginBottom: WP3,
    ...BORDER_STYLE['bottom'],
    paddingBottom: WP3
  },
  sectionHeader: {
    marginBottom: WP2
  }
})
