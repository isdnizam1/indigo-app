import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import { cloneDeep } from 'lodash-es'
import Text from '../Text'
import { HP1, HP105, HP2, WP1, WP10, WP2 } from '../../constants/Sizes'
import { GREY, GREY20, ORANGE, ORANGE_BRIGHT, WHITE } from '../../constants/Colors'
import Icon from '../Icon'
import InputModal from '../InputModal'
import { getJob } from '../../actions/api'
import LinearGradient from '../LinearGradientCompact'

class ProfessionPromoteForm extends Component {
    _onAdd = (value) => {
      const profesions = cloneDeep(this.props.profesion)
      profesions.push(value)
      this.props.onChange(profesions)
    }

    _onDelete = (index) => {
      const profesions = cloneDeep(this.props.profesion)
      profesions.splice(index, 1)
      this.props.onChange(profesions)
    }

    _profesionItem = (profesion, index) => {
      return (
        <LinearGradient
          rounded
          key={index}
          style={{ flexGrow: 0, marginRight: WP2, marginVertical: WP1,
            paddingHorizontal: WP2, paddingVertical: WP1,
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
            <Text size='slight' color={WHITE}>{profesion}</Text>
            <TouchableOpacity onPress={() => this._onDelete(index)}>
              <Icon size='slight' name='close' color={WHITE} style={{ marginLeft: 5 }} />
            </TouchableOpacity>
          </View>
        </LinearGradient>
      )
    }

    render() {
      const {
        required, boxed, profesion
      } = this.props

      return (
        <View style={{ width: '100%' }}>
          <View
            style={[
              { marginBottom: boxed ? HP2 : HP1, marginTop: boxed ? 0 : HP105, borderColor: GREY20 },
              boxed ? { borderWidth: 0.5, padding: WP2, borderRadius: 5 } : {}
            ]}
          >
            <View style={{ flexDirection: 'row', marginBottom: WP2 }}>
              <Text size={boxed ? 'tiny' : 'mini'} color={GREY} weight={400}>What are you looking for?</Text>
              {
                required && <Icon size='tiny' color={ORANGE_BRIGHT} name='asterisk' type='MaterialCommunityIcons' />
              }
            </View>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center' }}>
              {
                profesion.map((item, index) => (
                  this._profesionItem(item, index)
                ))
              }
              <View>
                <InputModal
                  triggerComponent={(
                    <View style={{
                      borderColor: ORANGE, borderWidth: 2, paddingHorizontal: WP2 - 2, paddingVertical: WP1 - 2, borderRadius: WP10,
                      flexDirection: 'row', alignItems: 'center', justifyContent: 'center', flexGrow: 0
                    }}
                    >
                      <Icon size='mini' color={ORANGE} name='plus' style={{ alignSelf: 'center', marginRight: 5 }} />
                      <Text size='xmini' color={ORANGE}>ADD PROFESSION</Text>
                    </View>
                  )}
                  header='Select Profession'
                  suggestion={getJob}
                  suggestionKey='job_title'
                  suggestionPathResult='job_title'
                  onChange={this._onAdd}
                  selected=''
                  createNew={false}
                  placeholder='Search profession here...'
                  refreshOnSelect={true}
                />
              </View>
            </View>
          </View>
        </View>
      )
    }
}

ProfessionPromoteForm.propTypes = {
  required: PropTypes.bool,
  boxed: PropTypes.bool,
  onChange: PropTypes.func,
  profesion: PropTypes.arrayOf(PropTypes.any)
}

ProfessionPromoteForm.defaultProps = {
  required: false,
  boxed: false,
  onChange: () => { },
  profesion: [],
}

export default ProfessionPromoteForm
