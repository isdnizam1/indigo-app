import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View } from 'react-native'
import { cloneDeep } from 'lodash-es'
import Text from '../Text'
import { HP1, HP105, HP2, WP1, WP10, WP2 } from '../../constants/Sizes'
import { GREY, ORANGE, PURPLE, WHITE } from '../../constants/Colors'
import Icon from '../Icon'
import InputModal from '../InputModal'
import { getGenreInterest } from '../../actions/api'
import { BORDER_COLOR, BORDER_WIDTH, TOUCH_OPACITY } from '../../constants/Styles'
import RequiredMark from '../RequiredMark'

class GenrePromoteForm extends Component {
  _onAdd = (value) => {
    const genres = cloneDeep(this.props.genre)
    genres.push(value)
    this.props.onChange(genres)
  }

  _onDelete = (index) => {
    const genres = cloneDeep(this.props.genre)
    genres.splice(index, 1)
    this.props.onChange(genres)
  }

  _genreItem = (genre, index) => {
    return (
      <View
        key={index}
        style={{
          backgroundColor: PURPLE, flexGrow: 0, marginRight: WP2, marginVertical: WP1,
          paddingHorizontal: WP2, paddingVertical: WP1, borderRadius: WP10
        }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Text size='slight' color={WHITE}>{genre}</Text>
          <TouchableOpacity activeOpacity={TOUCH_OPACITY} onPress={() => this._onDelete(index)}>
            <Icon size='slight' name='close' color={WHITE} style={{ marginLeft: 5 }}/>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  render() {
    const {
      required, boxed, genre
    } = this.props

    return (
      <View>
        <View
          style={[
            { marginBottom: boxed ? HP2 : HP1, marginTop: boxed ? 0 : HP105, borderColor: BORDER_COLOR },
            boxed ? { borderWidth: BORDER_WIDTH, padding: WP2, borderRadius: 5 } : {}
          ]}
        >
          <View style={{ flexDirection: 'row', marginBottom: WP2 }}>
            <Text size={boxed ? 'tiny' : 'mini'} color={GREY} weight={400}>Genre</Text>
            {
              required && <RequiredMark/>
            }
          </View>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center' }}>
            {
              genre.map((item, index) => (
                this._genreItem(item, index)
              ))
            }
            <View>
              <InputModal
                triggerComponent={(
                  <View style={{
                    borderColor: ORANGE,
                    borderWidth: 2,
                    paddingHorizontal: WP2 - 2,
                    paddingVertical: WP1 - 2,
                    borderRadius: WP10,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                  >
                    <Icon size='mini' color={ORANGE} name='plus' style={{ alignSelf: 'center', marginRight: 5 }}/>
                    <Text size='xmini' color={ORANGE}>ADD GENRE</Text>
                  </View>
                )}
                header='Select Interest'
                suggestion={getGenreInterest}
                suggestionKey='interest_name'
                suggestionPathResult='interest_name'
                onChange={this._onAdd}
                selected=''
                createNew={true}
                placeholder='Search interest here...'
                refreshOnSelect={true}
              />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

GenrePromoteForm.propTypes = {
  required: PropTypes.bool,
  boxed: PropTypes.bool,
  onChange: PropTypes.func,
  genre: PropTypes.arrayOf(PropTypes.any)
}

GenrePromoteForm.defaultProps = {
  required: false,
  boxed: false,
  onChange: () => {
  },
  genre: []
}

export default GenrePromoteForm
