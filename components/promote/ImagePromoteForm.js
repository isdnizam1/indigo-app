import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Image, Platform, StatusBar, TouchableOpacity, View } from 'react-native'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'
import Text from '../Text'
import Modal from '../Modal'
import ListItem from '../ListItem'
import Icon from '../Icon'
import { WP100, WP2, WP30, WP8 } from '../../constants/Sizes'
import { GREY, GREY_CALM } from '../../constants/Colors'
import { TOUCH_OPACITY } from '../../constants/Styles'
import RequiredMark from '../RequiredMark'

class ImagePromoteForm extends Component {
  _selectPhoto = async () => {
    const {
      aspectRatio
    } = this.props

    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCameraRoll === 'granted') {
      return await ImagePicker.launchImageLibraryAsync({
        base64: true,
        allowsEditing: true,
        aspect: aspectRatio
      })
    }
  }

  _takePhoto = async () => {
    const {
      aspectRatio
    } = this.props

    const { status: statusCamera } = await Permissions.askAsync(Permissions.CAMERA)
    const { status: statusCameraRoll } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if (statusCamera === 'granted' && statusCameraRoll === 'granted') {
      return await ImagePicker.launchCameraAsync({
        base64: true,
        allowsEditing: true,
        aspect: aspectRatio
      })
    }
  }

  _handlePhoto = async (result) => {
    const {
      onChange
    } = this.props

    if (Platform.OS === 'ios') {
      StatusBar.setHidden(false)
    }

    if (!result) {
      return
    }

    if (!result.cancelled) onChange(result.base64)
  }

  render() {
    const {
      label, image, imageAspectRatio, rounded, base64, style, imageStyle, emptyImage, disabled, required
    } = this.props

    const imageUri = base64 ? `data:image/gif;base64,${image}` : image
    const defaultImage = emptyImage || require('../../assets/icons/iconCamera.png')

    return (
      <View>
        {
          label && (
            <View style={{ flexDirection: 'row' }}>
              <Text weight={500} color={GREY} size='mini' style={{ marginBottom: 5 }}>{label}</Text>
              {
                required && <RequiredMark/>
              }
            </View>
          )
        }
        <Modal
          renderModalContent={({ toggleModal }) => (
            <View>
              <ListItem
                inline onPress={() => {
                  this._selectPhoto().then((result) => {
                    toggleModal()
                    this._handlePhoto(result)
                  })
                }}
              >
                <Icon type='MaterialIcons' size='large' name='photo-library'/>
                <Text style={{ paddingLeft: WP2 }}>Select from library</Text>
              </ListItem>
              <ListItem
                inline onPress={() => {
                  this._takePhoto().then((result) => {
                    toggleModal()
                    this._handlePhoto(result)
                  })
                }}
              >
                <Icon size='large' name='camera'/>
                <Text style={{ paddingLeft: WP2 }}>Take a picture</Text>
              </ListItem>
            </View>
          )}
        >
          {({ toggleModal }, M) => (
            <View>
              <TouchableOpacity activeOpacity={TOUCH_OPACITY} onPress={toggleModal} disabled={disabled}>
                <View style={{
                  backgroundColor: GREY_CALM,
                  height: WP30,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: rounded ? WP100 : WP2,
                  ...style
                }}
                >
                  <Image
                    source={image ? { uri: imageUri } : defaultImage}
                    style={{
                      height: image ? WP30 : WP8,
                      aspectRatio: image ? imageAspectRatio : 1.2,
                      borderRadius: rounded ? WP100 : WP2,
                      ...imageStyle
                    }}
                  />
                </View>
              </TouchableOpacity>
              {M}
            </View>
          )}
        </Modal>
      </View>
    )
  }
}

ImagePromoteForm.propTypes = {
  label: PropTypes.string,
  onChange: PropTypes.func,
  image: PropTypes.string,
  aspectRatio: PropTypes.arrayOf(PropTypes.any),
  imageAspectRatio: PropTypes.number,
  rounded: PropTypes.bool,
  base64: PropTypes.bool,
  imageStyle: PropTypes.objectOf(PropTypes.any),
  style: PropTypes.objectOf(PropTypes.any),
  emptyImage: PropTypes.objectOf(PropTypes.any),
  disabled: PropTypes.bool,
  required: PropTypes.bool
}

ImagePromoteForm.defaultProps = {
  label: undefined,
  onChange: () => {
  },
  aspectRatio: [1, 1],
  imageAspectRatio: 1,
  rounded: false,
  base64: true,
  imageStyle: {},
  style: {},
  disabled: false,
  required: false
}

export default ImagePromoteForm
