import { View } from 'react-native'
import React from 'react'
import Text from '../Text'
import { GREY } from '../../constants/Colors'
import { WP1 } from '../../constants/Sizes'

export const DetailSection = ({ label, content, contentColor }) => {
  return (
    <View style={{
      flexDirection: 'row',
      flex: 1,
      marginBottom: WP1
    }}
    >
      <Text size='slight' color={GREY} weight={400}>
        {`${label}: `}
      </Text>
      <Text size='slight' color={contentColor || GREY}>
        {content}
      </Text>
    </View>
  )
}

export const formatMember = (members) => {
  const dataToDisplay = members.reduce((accum, item) => (
    `${accum}${item.name}, `
  ), '')

  return dataToDisplay.substring(0, dataToDisplay.length - 2)
}

export const workIcon = {
  song: require('../../assets/icons/explorePageAuditionSelect.png'),
  video: require('../../assets/icons/explorePageAuditionSelectVideo.png')
}
