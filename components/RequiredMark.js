import React from 'react'
import PropTypes from 'prop-types'
import { ORANGE_BRIGHT } from '../constants/Colors'
import Icon from './Icon'

const RequiredMark = ({ size, style }) => (
  <Icon
    size={size} color={ORANGE_BRIGHT} name='asterisk' type='MaterialCommunityIcons'
    style={{ marginLeft: 3, marginTop: 3, ...style }}
  />
)

RequiredMark.propTypes = {
  size: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.any)
}

RequiredMark.defaultProps = {
  size: 'xtiny'
}

export default RequiredMark
