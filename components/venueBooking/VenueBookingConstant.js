export const bookingConstant = (navigateBack, navigateTo) => ({
  SUCCESS: {
    IMAGE: require('../../assets/images/bookingSuccess.png'),
    TITLE: 'Booking Success!',
    MESSAGE: 'We have received your booking. Please check your email for details. We will e contacting you soon to follow up your order',
    BUTTON_LABEL: 'Back to Home',
    BUTTON_ACTION: () => {
      navigateTo('TimelineScreen')
    }
  },
  FAILED: {
    IMAGE: require('../../assets/images/bookingFailed.png'),
    TITLE: 'Booking Failed!',
    MESSAGE: 'We\'re sorry the date you choose s already booked by someone else',
    BUTTON_LABEL: 'Choose another date',
    BUTTON_ACTION: () => {
      navigateBack()
    }
  }
})
