import React from 'react'
import { ScrollView, TouchableOpacity, View } from 'react-native'
import { HP80, WP1, WP3, WP6, WP70 } from '../constants/Sizes'
import { GREY, TOMATO, WHITE } from '../constants/Colors'
import { BORDER_WIDTH, TOUCH_OPACITY } from '../constants/Styles'
import { isAfterFebruary2021 } from '../screens/InviteFriendsScreen'
import Text from './Text'
import Image from './Image'
import Modal from './Modal'

const defaultTnC = [
  { type: 'header', format: '1.', value: 'Program Soundfren Referral hanya berlaku bagi penerima kode referral melalui aplikasi Soundfren.' },
  { type: 'header', format: '2.', value: 'Kode referral berlaku untuk pengguna Soundfren dan dibagikan kepada yang belum pernah memakai layanan Soundfren sebelumnya.' },
  { type: 'child', format: 'a.', value: 'User yang dikategorikan belum pernah mencakupi alamat e-mail yang belum pernah di daftarkan di soundfren.' },
  { type: 'header', format: '3.', value: 'Pemberi referral berhak mendapat reward jika referral digunakan oleh penerima referral, reward yang diberikan yaitu:' },
  { type: 'child', format: 'a.', value: 'Untuk pengguna Soundfren non-premium mendapatkan reward selama 1 bulan menjadi premium user (basic) jika kode referral berhasil digunakan oleh 3 pengguna baru yang menyelesaikan seluruh proses pendaftaran.' },
  { type: 'child', format: 'b.', value: 'Untuk pengguna Soundfren premium (basic/pro) mendapatkan reward selama 1 bulan perpanjangan sesuai dengan paket yang sedang aktif jika kode referral berhasil digunakan oleh 3 pengguna baru yang menyelesaikan seluruh proses pendaftaran.' },
  { type: 'child', format: 'c.', value: 'Untuk pengguna Soundfren yang berhasil mendapatkan digunakan kode referralnya sebanyak 10 pengguna baru, akan mendapatkan Voucher Discount 50% Music Webinar.' },
  { type: 'child', format: 'd.', value: 'Untuk pengguna Soundfren yang berhasil mendapatkan digunakan kode referralnya sebanyak 25 pengguna baru, akan mendapatkan Voucher Gratis sesi Connect.' },
  { type: 'header', format: '4.', value: 'Reward hanya berlaku selama periode bulan Februari 2021.' },
  { type: 'header', format: '5.', value: 'Berlaku untuk semua pengguna iOS dan Android yang menggunakan aplikasi Soundfren.' }
]

const afterFebTnC = [
  { type: 'header', format: '1.', value: 'Program Soundfren Referral hanya berlaku bagi penerima kode referral melalui aplikasi Soundfren.' },
  { type: 'header', format: '2.', value: 'Kode referral berlaku untuk pengguna Soundfren dan dibagikan kepada yang belum pernah memakai layanan Soundfren sebelumnya.' },
  { type: 'child', format: 'a.', value: 'User yang dikategorikan belum pernah mencakupi alamat e-mail yang belum pernah di daftarkan di soundfren.' },
  { type: 'header', format: '3.', value: 'Pemberi referral berhak mendapat hadiah jika referral digunakan oleh penerima referral, reward yang diberikan yaitu:' },
  { type: 'child', format: 'a.', value: 'Untuk pengguna Soundfren non-premium mendapatkan reward selama 1 bulan menjadi premium user (basic) jika kode referral berhasil digunakan oleh 5 pengguna baru yang menyelesaikan seluruh proses pendaftaran.' },
  { type: 'child', format: 'b.', value: 'Untuk pengguna Soundfren premium (basic/pro) mendapatkan reward selama 1 bulan perpanjangan sesuai dengan paket yang sedang aktif jika kode referral berhasil digunakan oleh 5 pengguna baru yang menyelesaikan seluruh proses pendaftaran.' },
  { type: 'header', format: '4.', value: 'Reward berlaku kelipatan dengan batas maksimal sebanyak 3 bulan.' },
  { type: 'header', format: '5.', value: 'Berlaku untuk semua pengguna iOS dan Android yang menggunakan aplikasi Soundfren.' },
]

const getTnC = () => {
  if (isAfterFebruary2021()) {
    return afterFebTnC
  }

  return defaultTnC
}

const InviteFriendTnC = (props) => {
  const {
    children
  } = props

  const tnc = getTnC() || []

  return (
    <Modal
      swipeDirection={false}
      renderModalContent={({ toggleModal }) => (
        <View style={{ maxHeight: HP80 }}>
          <Image
            source={require('../assets/images/illustrationProfile.png')}
            style={{ position: 'absolute', bottom: 0, right: 0 }}
            imageStyle={{ width: WP70, height: undefined, aspectRatio: 206 / 165, opacity: 0.5 }}
          />
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: WP6 }}>
            <Text size='mini' type='NeoSans' weight={500} color={TOMATO}>{'Syarat dan Ketentuan'}</Text>
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              style={{
                paddingHorizontal: WP3,
                paddingVertical: WP1,
                borderRadius: 6,
                borderWidth: BORDER_WIDTH,
                borderColor: TOMATO,
                backgroundColor: TOMATO,
              }}
              onPress={() => toggleModal()}
            >
              <Text centered size='tiny' type='NeoSans' weight={500} color={WHITE}>Back</Text>
            </TouchableOpacity>
          </View>
          <ScrollView>
            <View style={{ paddingHorizontal: WP6, paddingBottom: WP6 }}>
              {tnc.map((item) => (
                // eslint-disable-next-line react/jsx-key
                <View style={{ flex: 1, flexDirection: 'row', paddingLeft: item.type == 'child' ? WP6 : 0, marginBottom: WP1, paddingRight: WP6 }}>
                  <Text color={GREY} size='xmini' type='NeoSans' weight={400} style={{ width: WP6 }}>{item.format}</Text>
                  <Text color={GREY} size='xmini' type='NeoSans' weight={400}>{item.value}</Text>
                </View>
              ))}
            </View>
          </ScrollView>
        </View>
      )}
    >
      {({ toggleModal }, M) => (
        <View>
          <TouchableOpacity onPress={toggleModal}>
            {children}
          </TouchableOpacity>
          {M}
        </View>
      )}
    </Modal>
  )
}

InviteFriendTnC.propTypes = {}

InviteFriendTnC.defaultProps = {}

export default InviteFriendTnC
