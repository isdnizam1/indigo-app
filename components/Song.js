import React from 'react'
import PropTypes from 'prop-types'
import { isObject, upperFirst, isNil, isUndefined } from 'lodash-es'
import {
  Image,
  TouchableWithoutFeedback,
  View,
  ActivityIndicator,
} from 'react-native'
import { connect } from 'react-redux'
import {
  GUN_METAL,
  WHITE,
  PALE_GREY,
  REDDISH,
  SHIP_GREY_CALM,
} from 'sf-constants/Colors'
import { WP1, WP10 } from 'sf-constants/Sizes'
import { setIdJourney, setPlayback } from 'sf-services/song/actionDispatcher'
import { getTime, NavigateToInternalBrowser } from 'sf-utils/helper'
import { postLogViewJourney } from 'sf-actions/api'
import styles from 'sf-styles/components/Song'
import Icon from 'sf-components/Icon'
import Text from 'sf-components/Text'
import SongOptions from 'sf-components/SongOptions'
import { SONG_SOURCES } from 'sf-constants/Icons'
import { restrictedAction } from '../utils/helper'

const mapStateToProps = ({
  song: { id_journey: nowPlaying, playback: pb, soundObject },
  auth,
}) => ({
  nowPlaying,
  pb,
  soundObject,
  userData: auth.user,
})

const mapDispatchToProps = {
  setNowPlaying: (response) => setIdJourney(response),
  setPlayback: (response) => setPlayback(response),
}

class Song extends React.Component {
  static propTypes = {
    idViewer: PropTypes.any.isRequired,
    isMine: PropTypes.bool,
    firstSong: PropTypes.bool,
    song: PropTypes.any,
    style: PropTypes.any,
    styleFirst: PropTypes.any,
    showArtist: PropTypes.bool,
    borderInner: PropTypes.bool,
    refreshData: PropTypes.func,
  };

  constructor(props) {
    super(props)
    this.state = {
      timerWidth: WP10,
      song: isObject(props.song)
        ? props.song
        : {
            additional_data: {},
          },
    }
    this._playThisSong = this._playThisSong.bind(this)
    this._onTimerLayout = this._onTimerLayout.bind(this)
  }

  shouldComponentUpdate(nextProps, nextState) {
    const shouldUpdate =
      nextProps.nowPlaying == this.props.song.id_journey ||
      this.props.pb.isPlaying != nextProps.pb.isPlaying ||
      this.state.timerWidth != nextState.timerWidth ||
      this.state.song.total_view != nextState.song.total_view ||
      nextProps.loading == !this.props.loading
    return shouldUpdate
  }

  _playThisSong = async () => {
    const {
      song,
      song: { id_journey },
      soundObject,
      nowPlaying,
      setNowPlaying,
      setPlayback,
      pb: { positionMillis, durationMillis, isPlaying, isLoaded },
      idViewer: id_viewer,
    } = this.props
    const isPlayThisSong = isPlaying && id_journey == nowPlaying
    if (isPlaying && id_journey == nowPlaying) {
      soundObject.pauseAsync()
    } else if (positionMillis / durationMillis >= 0.985) {
      await soundObject.setPositionAsync(0)
      soundObject.playAsync()
    } else {
      if (nowPlaying != id_journey) setNowPlaying(id_journey)
      try {
        if (!isPlayThisSong && nowPlaying != id_journey && isLoaded) {
          await soundObject.pauseAsync()
          await soundObject.unloadAsync()
        }
      } catch (err) {
        // silent is gold
      } finally {
        const add = isObject(song.additional_data)
          ? song.additional_data
          : JSON.parse(song.additional_data)
        const { in_app_player: isFileSong } = add
        const logBody = {
          id_user: id_viewer,
          id_journey,
        }
        if (nowPlaying != id_journey && isFileSong == 1) {
          const param = {
            uri: add.url_song,
            overrideFileExtensionAndroid: 'm3u8',
          }
          await soundObject.loadAsync(param)
          postLogViewJourney(logBody).then(({ data: responseFromServer }) => {
            song.total_view++
          })
        }
        if (isFileSong == 1) {
          soundObject.setOnPlaybackStatusUpdate(setPlayback)
          soundObject.playAsync()
        } else {
          NavigateToInternalBrowser({ url: add.url_song })
          postLogViewJourney(logBody).then(({ data: responseFromServer }) => {
            song.total_view++
          })
        }
      }
    }
  };

  _onTimerLayout = ({
    nativeEvent: {
      layout: { width: timerWidth },
    },
  }) => {
    this.setState({ timerWidth: timerWidth + WP1 })
  };

  render() {
    const {
      firstSong,
      style,
      styleFirst,
      showArtist,
      loading = false,
      borderInner = false,
      navigateTo,
      userData,
      isMine,
      isProfileScreen,
      nowPlaying,
      idViewer: id_viewer,
      pb: { isPlaying, positionMillis, durationMillis, isBuffering },
      refreshData,
    } = this.props
    const { timerWidth, song } = this.state
    let add = isObject(song.additional_data)
        ? song.additional_data
        : JSON.parse(song.additional_data),
      title = loading ? 'Song Title' : upperFirst(song.title),
      image =
        loading || isUndefined(add.cover_image) ? null : { uri: add.cover_image },
      subtitle = loading
        ? showArtist
          ? 'Artist Name'
          : '0'
        : showArtist
        ? add.artist_name
        : `  ${song.total_view}`,
      isPlayThisSong = isPlaying && song.id_journey == nowPlaying,
      isFileSong = add.in_app_player == 1
    if (image == null && !loading) {
      image =
        Object.keys(SONG_SOURCES).indexOf(add.source_from) >= 0
          ? SONG_SOURCES[add.source_from]
          : SONG_SOURCES['others']
    }
    return (
      <View
        style={
          firstSong
            ? [styles.firstSong, styleFirst]
            : [styles.song, { borderTopWidth: borderInner ? 0 : 1 }, style]
        }
      >
        <TouchableWithoutFeedback
          onPress={restrictedAction({
            action: this._playThisSong,
            navigateTo,
            userData: { id_user: id_viewer },
          })}
        >
          <View style={styles.songCoverWrapper}>
            <Image style={styles.songCover} source={image} />
            {isPlayThisSong && !isBuffering && (
              <View style={styles.songPause}>
                <Image
                  style={styles.songPauseIcon}
                  source={require('../assets/icons/spPauseCircle.png')}
                />
              </View>
            )}
            {isBuffering && song.id_journey == nowPlaying && (
              <View style={styles.songPause}>
                <ActivityIndicator color={WHITE} size={'small'} />
              </View>
            )}
          </View>
        </TouchableWithoutFeedback>
        <View
          style={[
            styles.songContent,
            {
              borderBottomWidth: borderInner ? 1 : 0,
              borderBottomColor: PALE_GREY,
            },
          ]}
        >
          <View style={{ flex: 1 }}>
            <Text
              numberOfLines={1}
              style={{ flex: 1 }}
              onPress={this._playThisSong}
              size={'slight'}
              type={'Circular'}
              color={GUN_METAL}
              weight={500}
            >
              {title}
            </Text>
            {isUndefined(song.id_journey) && (
              <Text type={'Circular'} color={SHIP_GREY_CALM}>
                ⋯
              </Text>
            )}
            {song.id_journey == nowPlaying && !isNil(nowPlaying) && isFileSong && (
              <View style={styles.songContentMeta}>
                <View>
                  <Image
                    style={styles.playingIllustration}
                    source={require('../assets/icons/v3/playingIllustration.png')}
                  />
                </View>
                <View style={{ width: timerWidth, alignItems: 'flex-start' }}>
                  {timerWidth > 0 && isFileSong && (
                    <Text
                      type={'Circular'}
                      weight={300}
                      color={REDDISH}
                      size={'tiny'}
                    >
                      {positionMillis ? getTime(positionMillis) : '00:00'}
                    </Text>
                  )}
                </View>
                <View style={styles.progressBar}>
                  <View
                    style={[
                      styles.progressBarActive,
                      {
                        width: `${(positionMillis / durationMillis) * 100}%`,
                      },
                    ]}
                  />
                </View>
                <View style={{ width: timerWidth, alignItems: 'flex-end' }}>
                  {timerWidth > 0 && isFileSong && (
                    <Text
                      type={'Circular'}
                      weight={300}
                      color={REDDISH}
                      size={'tiny'}
                    >
                      {durationMillis ? getTime(durationMillis) : '00:00'}
                    </Text>
                  )}
                </View>
              </View>
            )}
            {(song.id_journey != nowPlaying || !isFileSong) && (
              <View style={styles.songContentMeta}>
                <View>
                  {!showArtist && (
                    <Icon color={'#919eab'} size={'tiny'} name={'play'} />
                  )}
                </View>
                <Text
                  numberOfLines={1}
                  style={{ flex: 1 }}
                  type={'Circular'}
                  weight={300}
                  color={SHIP_GREY_CALM}
                  size={'xmini'}
                >
                  {subtitle}
                </Text>
              </View>
            )}
          </View>
          {!loading && (
            <View>
              {id_viewer ? (
                <SongOptions
                  refreshData={refreshData}
                  userData={userData}
                  isMine={isMine}
                  song={song}
                  navigateTo={navigateTo}
                  isProfileScreen={isProfileScreen}
                />
              ) : (
                <Icon
                  background='dark-circle'
                  size='mini'
                  color={SHIP_GREY_CALM}
                  name='dots-three-horizontal'
                  type='Entypo'
                  centered
                />
              )}
            </View>
          )}
        </View>
        {isPlayThisSong && (
          <View onLayout={this._onTimerLayout} style={styles.invisibleTimerHelper}>
            <Text type={'Circular'} weight={300} color={REDDISH} size={'tiny'}>
              00:00
            </Text>
          </View>
        )}
      </View>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Song)
