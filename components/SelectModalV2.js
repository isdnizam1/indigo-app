import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  TouchableOpacity,
  View,
  TextInput,
  ScrollView,
  SafeAreaView,
} from "react-native";
import {
  isFunction,
  isEqual,
  isEmpty,
  reduce,
  isArray,
  includes,
} from "lodash-es";
import { connect } from "react-redux";
import { WP1, WP2, WP4, HP100, WP3, WP105, WP308 } from "../constants/Sizes";
import {
  GREY,
  WHITE,
  TOMATO,
  SHIP_GREY_CALM,
  REDDISH,
  NAVY_DARK,
  PALE_GREY,
  GUN_METAL,
  PALE_GREY_TWO,
} from "../constants/Colors";
import { TOUCH_OPACITY } from "../constants/Styles";
import Modal from "./Modal";
import Text from "./Text";
import Icon from "./Icon";
import Button from "./Button";

const propsType = {
  triggerComponent: PropTypes.objectOf(PropTypes.any),
  keyword: PropTypes.string,
  onChange: PropTypes.func,
  suggestion: PropTypes.func,
  renderItem: PropTypes.func,
  reformatFromApi: PropTypes.func,
  suggestionKey: PropTypes.string,
  suggestionPathResult: PropTypes.string,
  suggestionPathValue: PropTypes.string,
  asObject: PropTypes.bool,
  placeholder: PropTypes.string,
  refreshOnSelect: PropTypes.bool,
  extraResult: PropTypes.bool,
  iconName: PropTypes.string,
  iconType: PropTypes.string,
  iconSize: PropTypes.string,
};

const propsDefault = {
  keyword: "",
  triggerComponent: {},
  onChange: () => {},
  suggestion: () => {},
  suggestionKey: "",
  suggestionPathResult: "",
  suggestionPathValue: "",
  asObject: false,
  createNew: true,
  placeholder: "",
  refreshOnSelect: false,
  extraResult: false,
  iconName: "",
  iconType: "AntDesign",
  iconSize: "small",
};

const mapStateToProps = ({ auth }) => ({
  userData: auth.user,
});

class SelectModalV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyword: "",
      inputKey: "",
      suggestions: [],
      suggestionsRecent: [],
      showSuggestion: false,
      modalHeight: HP100,
    };
  }

  componentDidMount = () => {
    this._onFetchSuggestion();
  };

  _onFocus = () => {
    this.setState({ showSuggestion: true }, this._onFetchSuggestion);
  };

  _onBlur = () => {
    this.setState({ showSuggestion: false });
  };

  _onChangeText = (text) => {
    this.setState({ keyword: text }, this._onFetchSuggestion);
  };

  _onSelectSuggestion = (value, closeModalCallback) => {
    const { onChange, suggestionPathResult, asObject, refreshOnSelect } =
      this.props;

    if (asObject) {
      if (value[suggestionPathResult] !== this.props.selected) {
        if (!refreshOnSelect)
          this.setState({ keyword: value[suggestionPathResult] });
        onChange(value);
      } else {
        this.setState({ keyword: "" });
        onChange({});
      }
    } else {
      if (value[suggestionPathResult] !== this.props.selected) {
        if (!refreshOnSelect)
          this.setState({ keyword: value[suggestionPathResult] });
        else this.setState({ keyword: "" });
        onChange(value[suggestionPathResult]);
      } else {
        this.setState({ keyword: "" });
        onChange("");
      }
    }
    closeModalCallback();
  };

  _onCreateNewOption = (closeModalCallback) => {
    const { keyword } = this.state;
    const { onChange } = this.props;

    onChange(keyword);
    closeModalCallback();
  };

  _onClearInput = () => {
    this._onFocus();
  };

  _onFetchSuggestion = () => {
    const {
      suggestion,
      suggestionKey,
      userData: { id_user },
      suggestionPathResult,
      extraResult,
      reformatFromApi,
      suggestionPathValue,
    } = this.props;
    const { keyword } = this.state;
    if (isFunction(suggestion)) {
      suggestion({
        [suggestionKey]: keyword,
        id_user,
        limit: 6,
        start: 0,
      })
        .then(({ data }) => {
          let suggestionData = data.result || [];
          if (reformatFromApi) {
            suggestionData = reduce(
              suggestionData,
              (result, item) => {
                result.push({
                  [suggestionPathResult]: reformatFromApi(
                    item[suggestionPathResult]
                  ),
                  [suggestionPathValue]: item[suggestionPathValue],
                });
                return result;
              },
              []
            );
          }
          let suggestionDataRecentlySearch = [];
          if (extraResult) {
            suggestionDataRecentlySearch = data.recentlySearch || [];
            if (!isEmpty(suggestionDataRecentlySearch)) {
              suggestionDataRecentlySearch = reduce(
                data.recentlySearch,
                (result, item) => {
                  result.push({
                    [suggestionPathResult]: reformatFromApi
                      ? reformatFromApi(item.value)
                      : item.value,
                    [suggestionPathValue]: item[suggestionPathValue],
                  });
                  return result;
                },
                []
              );
            }
          }
          this.setState({
            suggestions: suggestionData,
            suggestionsRecent: suggestionDataRecentlySearch,
            showSuggestion: true,
          });
        })
        .catch(() => {
          // Silent is gold
        });
    }
  };

  _isSelected = (item) => {
    const { selected, suggestionPathResult } = this.props;

    if (isArray(selected))
      return includes(selected, item[suggestionPathResult]);
    else return isEqual(selected, item[suggestionPathResult]);
  };

  // RENDER LIST
  _suggestionList(
    suggestions,
    toggleModal,
    suggestionPathResult,
    suggestionsRecent
  ) {
    const { extraResult } = this.props;
    const { keyword } = this.state;
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          paddingHorizontal: WP4,
          paddingTop: WP4,
        }}
      >
        {/* {!isEmpty(suggestionsRecent) && isEmpty(keyword) && extraResult && (
          <Text type='Circular' color={NAVY_DARK} size='mini' weight={500}>
            Pencarian Terakhir
          </Text>
        )}
        {!isEmpty(suggestionsRecent) && isEmpty(keyword) && extraResult && (
          <View style={{ marginTop: WP2, marginBottom: WP3 }}>
            {suggestionsRecent.map((item, index) =>
              this._renderItem(index, item, toggleModal, suggestionPathResult),
            )}
          </View>
        )} */}
        {isEmpty(keyword) && extraResult && (
          <View style={{ paddingBottom: WP2 }}>
            <Text type="Circular" color={NAVY_DARK} size="mini" weight={500}>
              Most Searched
            </Text>
          </View>
        )}
        {(suggestions || []).map((item, index) =>
          this._renderItem(index, item, toggleModal, suggestionPathResult)
        )}
        {!isEmpty(keyword) && this._createNewOption(toggleModal)}
      </ScrollView>
    );
  }

  _renderItem = (index, item, toggleModal, suggestionPathResult) => {
    const { iconName, iconType, iconSize } = this.props;
    return (
      <TouchableOpacity
        key={index}
        style={{
          paddingVertical: WP2,
          paddingHorizontal: WP1,
          borderBottomWidth: 1,
          borderBottomColor: PALE_GREY,
        }}
        onPress={() => {
          this._onSelectSuggestion(item, toggleModal);
        }}
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <View style={{ alignItems: "center", flexDirection: "row" }}>
            {!isEmpty(iconName) && (
              <Icon
                style={{ marginRight: WP3 }}
                centered
                color={this._isSelected(item) ? PALE_GREY : GUN_METAL}
                type={iconType}
                name={iconName}
                size={iconSize}
              />
            )}
            {this._renderItemText(item, suggestionPathResult)}
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  _renderItemText = (item, suggestionPathResult) => {
    const { renderItem } = this.props;
    if (renderItem) {
      const itemEl = renderItem(item[suggestionPathResult]);
      if (React.isValidElement(itemEl)) return itemEl;
      else
        return (
          <Text
            size="mini"
            type="Circular"
            color={this._isSelected(item) ? REDDISH : GUN_METAL}
            weight={this._isSelected(item) ? 400 : 300}
          >
            {itemEl}
          </Text>
        );
    } else
      return (
        <Text
          size="mini"
          type="Circular"
          color={this._isSelected(item) ? REDDISH : GUN_METAL}
          weight={this._isSelected(item) ? 400 : 300}
        >
          {item[suggestionPathResult]}
        </Text>
      );
  };

  _createNewOption(toggleModal) {
    const { createNew } = this.props;
    const { keyword, suggestions } = this.state;

    if (!createNew && isEmpty(suggestions)) {
      return (
        <View style={{ paddingTop: WP3, paddingHorizontal: WP2 }}>
          <Text
            centered
            size="slight"
            type="Circular"
            color={SHIP_GREY_CALM}
            weight={500}
          >{`Sorry, no results found for “${keyword}”`}</Text>
        </View>
      );
    }

    if (createNew && isEmpty(suggestions)) {
      return (
        <View style={{ paddingTop: WP3 }}>
          <Text size="mini" color={GREY}>
            {keyword}
          </Text>
          <Button
            onPress={() => {
              this._onCreateNewOption(toggleModal);
            }}
            backgroundColor={TOMATO}
            nativeEffect
            rounded
            centered
            compact="flex-end"
            shadow="none"
            textColor={WHITE}
            textSize="tiny"
            textType="NeoSans"
            textWeight={500}
            marginless
            style={{ alignSelf: "flex-end", marginTop: WP105 }}
            text="Create New"
          />
        </View>
      );
    }
  }
  // END RENDER LIST

  render() {
    const { triggerComponent, suggestionPathResult, placeholder } = this.props;

    const { keyword, suggestions, suggestionsRecent, showSuggestion } =
      this.state;
    return (
      <Modal
        style={{
          flex: 1,
          borderTopRightRadius: 0,
          borderTopLeftRadius: 0,
        }}
        renderModalContent={({ toggleModal }) => (
          <SafeAreaView style={{ flex: 1, backgroundColor: WHITE }}>
            <View
              style={{
                backgroundColor: PALE_GREY_TWO,
                height: "100%",
              }}
            >
              {/*search input*/}
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  paddingHorizontal: WP4,
                  paddingVertical: WP2,
                  backgroundColor: WHITE,
                }}
              >
                <Icon
                  centered
                  onPress={toggleModal}
                  background="dark-circle"
                  size="large"
                  color={SHIP_GREY_CALM}
                  name="chevron-left"
                  type="Entypo"
                />
                <View
                  style={{
                    flex: 1,
                    backgroundColor: "rgba(145, 158, 171, 0.1)",
                    borderRadius: 6,
                    flexDirection: "row",
                    alignItems: "center",
                    paddingHorizontal: WP2,
                    paddingVertical: WP1,
                    marginHorizontal: WP2,
                  }}
                >
                  <Icon
                    centered
                    background="dark-circle"
                    size="large"
                    color={SHIP_GREY_CALM}
                    name="magnify"
                    type="MaterialCommunityIcons"
                  />
                  <TextInput
                    returnKeyType="search"
                    value={keyword}
                    numberOfLines={1}
                    style={{
                      flex: 1,
                      fontFamily: "CircularBook",
                      color: SHIP_GREY_CALM,
                      fontSize: WP308,
                      marginRight: WP2,
                      paddingVertical: 0,
                    }}
                    onChangeText={this._onChangeText}
                    placeholderTextColor={SHIP_GREY_CALM}
                    onFocus={this._onFocus}
                    placeholder={placeholder}
                  />
                  {!isEmpty(keyword) && (
                    <TouchableOpacity
                      onPress={() =>
                        this.setState({ keyword: "" }, this._onClearInput)
                      }
                    >
                      <Icon
                        centered
                        color={SHIP_GREY_CALM}
                        type="MaterialCommunityIcons"
                        name="close"
                        size="slight"
                      />
                    </TouchableOpacity>
                  )}
                </View>
                {!isEmpty(keyword) && (
                  <Text
                    type="Circular"
                    size="mini"
                    weight={400}
                    color={REDDISH}
                    centered
                    onPress={toggleModal}
                  >
                    Cancel
                  </Text>
                )}
              </View>

              {/*Suggestion*/}
              {showSuggestion &&
                this._suggestionList(
                  suggestions,
                  toggleModal,
                  suggestionPathResult,
                  suggestionsRecent
                )}
            </View>
          </SafeAreaView>
        )}
      >
        {({ toggleModal, isVisible }, M) => (
          <View style={{ flexGrow: 1 }}>
            <TouchableOpacity
              activeOpacity={TOUCH_OPACITY}
              onPress={() => {
                if (!isVisible) this._onFetchSuggestion();
                toggleModal();
              }}
            >
              <View pointerEvents="none">{triggerComponent}</View>
            </TouchableOpacity>
            {M}
          </View>
        )}
      </Modal>
    );
  }
}

SelectModalV2.propTypes = propsType;

SelectModalV2.defaultProps = propsDefault;

export default connect(mapStateToProps, {})(SelectModalV2);
