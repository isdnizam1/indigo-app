import React from 'react'
import PropTypes from 'prop-types'
import { View, TouchableOpacity } from 'react-native'
import { noop } from 'lodash-es'
import { WHITE, GREY, BLUE_LIGHT, GREY_CALM_SEMI, ICE_BLUE } from '../constants/Colors'
import { HP2, WP3, WP4, WP40, WP105, WP6, HP4 } from '../constants/Sizes'
import { SHADOW_STYLE } from '../constants/Styles'
import Button from './Button'
import Text from './Text'
import Image from './Image'

const propsType = {
  isActive: PropTypes.bool,
  coachmark: PropTypes.object,
  backgroundStyle: PropTypes.any,
  action: PropTypes.func
}

const propsDefault = {
  isActive: true,
  action: noop
}

const CoachmarkV2 = (props) => {
  const {
    isActive,
    coachmark,
    children,
    backgroundStyle,
    action,
    actionSkip
  } = props
  return (
    isActive ? (
      <View style={[{ zIndex: 999 }, children ? {} : backgroundStyle]}>
        <View style={[
          {
            position: children ? 'absolute' : 'relative',
            borderRadius: children ? 0 : WP3,
            overflow: children ? 'visible' : 'hidden',
          },
          children ?
            [{ backgroundColor: WHITE }, backgroundStyle] :
            {
              backgroundColor: coachmark.isFirst ? WHITE : coachmark.type == 'white' ? ICE_BLUE : BLUE_LIGHT,
              borderBottomStartRadius: coachmark.arrowPosition == 'bottom-left' ? 0 : WP3,
              borderBottomEndRadius: coachmark.arrowPosition == 'bottom-right' ? 0 : WP3,
              borderTopEndRadius: coachmark.arrowPosition == 'top-right' ? 0 : WP3,
            }
        ]}
        >
          {children ? null : (
            <View style={{ alignItems: 'center', padding: WP6, justifyContent: 'center' }}>
              {
                coachmark.image ? (
                  <Image
                    source={coachmark.image} style={[coachmark.isFirst ? { marginVertical: HP2 } : coachmark.imageStyle]} imageStyle={{ height: coachmark.imageSize || WP40 }}
                    aspectRatio={coachmark.imageRatio || 1}
                  />
                ) : null
              }
              <View style={[{
                zIndex: 99,
                width: '100%',
                marginBottom: HP4,
                alignItems: coachmark.positionText
              }]}
              >
                <Text size='slight' weight={500} color={coachmark.type == 'white' ? GREY : WHITE} style={{ marginBottom: coachmark.positionText != 'center' ? WP105 : 0 }}>{coachmark.title}</Text>
                <Text size='xmini' color={coachmark.type == 'white' ? GREY : ICE_BLUE}>{coachmark.description}</Text>
              </View>

              <Button
                style={{ alignSelf: coachmark.actionPosition || 'center', ...SHADOW_STYLE['shadow'] }}
                onPress={action}
                textColor={coachmark.type == 'white' ? WHITE : BLUE_LIGHT}
                marginless
                textSize='mini'
                textWeight={500}
                textType='NeoSans'
                text={coachmark.actionText}
                centered
                radius={10}
                backgroundColor={coachmark.type == 'white' ? BLUE_LIGHT : WHITE}
                contentStyle={{ paddingVertical: WP3, paddingHorizontal: WP3, }}
                width={'100%'}
                shadow='none'
              />
              {
                coachmark.isFirst && (
                  <TouchableOpacity onPress={actionSkip}>
                    <Text
                      centered
                      style={{ marginTop: WP4 }}
                      color={GREY_CALM_SEMI}
                      type='NeoSans'
                      weight={500} size='xmini'
                    >
                      Skip, I already know
                    </Text>
                  </TouchableOpacity>
                )
              }
            </View>
          )}
        </View>
        {
          children
        }
      </View>
    ) : children
  )
}

CoachmarkV2.propTypes = propsType
CoachmarkV2.defaultProps = propsDefault
export default CoachmarkV2
