import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Image as RNImage } from 'react-native'
import Constants from 'expo-constants'
import { WHITE } from '../constants/Colors'
import { HP100, WP100 } from '../constants/Sizes'
import Modal from './Modal'
import Image from './Image'
import Icon from './Icon'
import Header from './Header'

class ImageWithPreview extends Component {
  render() {
    const {
      source,
      style,
      imageStyle,
      aspectRatio
    } = this.props

    return (
      <View>
        <Modal
          swipeDirection={'right'}
          renderModalContent={({ toggleModal, payload }) => (
            <View
              style={{
                backgroundColor: '#000',
                width: WP100,
                height: HP100
              }}
            >
              <Header style={{ marginTop: Constants.statusBarHeight }}>
                <Icon onPress={toggleModal} size='massive' color={WHITE} name='close'/>
              </Header>
              <View style={{
                flex: 1,
                flexGrow: 1,
                justifyContent: 'center',
                alignItems: 'center'
              }}
              >
                <RNImage
                  source={source} style={{
                    width: WP100,
                    aspectRatio: 2
                  }}
                />
              </View>
            </View>
          )}
        >
          {({ toggleModal }, M) => (
            <View>
              <Image
                source={source}
                onPress={toggleModal}
                style={style}
                imageStyle={imageStyle}
                aspectRatio={aspectRatio}
              />
              {M}
            </View>
          )}
        </Modal>
      </View>
    )
  }
}

ImageWithPreview.propTypes = {
  source: PropTypes.any.isRequired,
  style: PropTypes.objectOf(PropTypes.any),
  imageStyle: PropTypes.objectOf(PropTypes.any),
  aspectRatio: PropTypes.number
}

ImageWithPreview.defaultProps = {
  style: {},
  imageStyle: {},
  aspectRatio: 1
}

export default ImageWithPreview
