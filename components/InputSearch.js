import React from 'react'
import { TextInput, View, ActivityIndicator } from 'react-native'
import { FONT_SIZE, WP2, WP105 } from '../constants/Sizes'
import { WHITE, SHIP_GREY_CALM } from '../constants/Colors'
import { FONTS } from '../constants/Fonts'
import Icon from './Icon'

const propsType = {
}

const propsDefault = {
  size: 'small',
  placeholder: 'Search'
}

class InputSearch extends React.Component {
  state = {
    value: '',
    isLoading: false,
  }

  _onChangeText = (text) => {
    this.setState({
      value: text
    })
  }

  _onEndEditing = () => {
    this.props.onSearch(this.state.value)
  }

  _onFocus = () => {

  }

  _onBlur = () => {

  }

  render() {
    const {
      size,
      placeholder
    } = this.props
    const {
      value,
      isLoading
    } = this.state
    return (
      <View
        style={
          {
            backgroundColor: 'rgba(145, 158, 171, 0.1)',
            borderRadius: 6,
            paddingHorizontal: WP2,
            paddingVertical: WP105,
            flexDirection: 'row',
            alignItems: 'center',
          }
        }
      >
        <Icon
          centered
          background='dark-circle'
          size='large'
          color={SHIP_GREY_CALM}
          name='magnify'
          type='MaterialCommunityIcons'
          style={{ marginRight: WP105 }}
        />
        <TextInput
          returnKeyType='search'
          value={value}
          style={{
            flex: 1,
            color: SHIP_GREY_CALM,
            fontFamily: FONTS.Circular[300],
            fontSize: FONT_SIZE[size] || FONT_SIZE[propsDefault.size],
          }}
          onFocus={this._onFocus}
          onBlur={this._onBlur}
          onEndEditing={this._onEndEditing}
          onChangeText={this._onChangeText}
          placeholder={placeholder}
          placeholderTextColor={SHIP_GREY_CALM}
        />
        {
          isLoading && (
            <ActivityIndicator size='small' color={WHITE} />
          )
        }
      </View>
    )
  }
}

InputSearch.propTypes = propsType
InputSearch.defaultProps = propsDefault
export default InputSearch
