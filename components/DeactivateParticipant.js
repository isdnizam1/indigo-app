import React, { useEffect, useRef } from "react";
import {
  View,
  Animated,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Image,
} from "react-native";
import {
  WHITE,
  REDDISH,
  WHITE_MILK,
  GUN_METAL,
  SHIP_GREY_CALM,
} from "../constants/Colors";
import { WP05, WP1, WP10, WP12, WP2, WP3, WP5, WP6 } from "../constants/Sizes";
import Text from "./Text";

const { width, height } = Dimensions.get("window");

const style = StyleSheet.create({
  popup: {
    width: 300,
    backgroundColor: WHITE,
    position: "absolute",
    justifyContent: "center",
    borderRadius: 6,
    elevation: 5,
  },
  button: {
    backgroundColor: REDDISH,
    paddingVertical: WP05,
    borderRadius: WP2,
    height: 40,
    justifyContent: "center",
  },
});

const DeactivateParticipant = ({ onConfirm, containerStyle }) => {
  let fadeAnim = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    setTimeout(() => {
      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 200,
        useNativeDriver: true,
      }).start();
    }, 300);
  }, []);

  return (
    <Animated.View
      style={{ ...style.popup, opacity: fadeAnim, ...containerStyle }}
    >
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
          paddingHorizontal: WP3,
          paddingVertical: WP5,
        }}
      >
        <View
          style={{
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            marginBottom: WP5,
          }}
        >
          <Image source={require("../assets/icons/icBlocked.png")} />
        </View>

        <View style={{ marginBottom: WP3 }}>
          <Text
            color={GUN_METAL}
            size={"slight"}
            centered
            weight={600}
            type="Circular"
          >
            You've been blocked from this event for several reasons
          </Text>
        </View>
        <View style={{ marginBottom: WP5 }}>
          <Text color={SHIP_GREY_CALM} size={"tiny"} centered weight={400}>
            Check your email or contact your event organizer for re-activate
            your account to this event.
          </Text>
        </View>
        <View>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={onConfirm}
            style={{
              ...style.button,
              width: 300 - WP6,
            }}
          >
            <Text centered size={"mini"} weight={500} color={WHITE_MILK}>
              Yes, I understand
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Animated.View>
  );
};

DeactivateParticipant.defaultProps = {
  onConfirm: null,
  onCancel: null,
  containerStyle: {},
};

export default React.memo(DeactivateParticipant);
