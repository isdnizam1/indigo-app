import React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, View } from "react-native";
import { map, toLower } from "lodash-es";
import { WP2 } from "../constants/Sizes";
import { TOUCH_OPACITY } from "../constants/Styles";
import {
  PALE_BLUE_TWO,
  REDDISH_LIGHT,
  REDDISH,
  SHIP_GREY,
  WHITE,
  REDDISH_DISABLED,
} from "../constants/Colors";
import Text from "./Text";

const propsType = {
  options: PropTypes.array,
  onPress: PropTypes.func,
  selectedValue: PropTypes.any,
  style: PropTypes.any,
};

const propsDefault = {
  options: [],
};

const selectedOptionStyle = (selectedValue, value) =>
  toLower(selectedValue) === value
    ? {
        container: {
          backgroundColor: REDDISH_DISABLED,
          borderColor: REDDISH,
        },
        text: {
          color: REDDISH,
        },
      }
    : {
        container: {
          borderColor: REDDISH,
        },
        text: {
          color: SHIP_GREY,
        },
      };

const SelectionBar = (props) => {
  const { onPress, options, selectedValue, style } = props;
  const optionsLength = options.length;
  return (
    <View style={[{ flexDirection: "row" }, style]}>
      {map(options, (option, i) => (
        <TouchableOpacity
          activeOpacity={TOUCH_OPACITY}
          onPress={() => onPress(option.key)}
          style={{
            paddingVertical: WP2,
            flex: 1,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            borderColor: PALE_BLUE_TWO,
            borderWidth: 1,
            borderTopStartRadius: i === 0 ? 6 : undefined,
            borderBottomStartRadius: i === 0 ? 8 : undefined,
            borderTopEndRadius: i === optionsLength - 1 ? 8 : undefined,
            borderBottomEndRadius: i === optionsLength - 1 ? 8 : undefined,
            ...selectedOptionStyle(selectedValue, option.key).container,
          }}
        >
          <Text
            centered
            weight={400}
            size="xmini"
            type="Circular"
            {...selectedOptionStyle(selectedValue, option.key).text}
          >
            {option.text}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

SelectionBar.propTypes = propsType;
SelectionBar.defaultProps = propsDefault;
export default SelectionBar;
