import React, { memo, isValidElement } from 'react'
import { View, TouchableOpacity, Platform } from 'react-native'
import PropTypes from 'prop-types'
import { isUndefined, isArray, get } from 'lodash-es'
import { PALE_BLUE_TWO, PALE_GREY, SHIP_GREY_CALM, GUN_METAL, NO_COLOR } from '../constants/Colors'
import { WP4, WP100, WP2, WP12 } from '../constants/Sizes'
import { TOUCH_OPACITY } from '../constants/Styles'
import Icon from './Icon'
import Text from './Text'
import Modal from './Modal'
import Image from './Image'

const propsType = {
  options: PropTypes.any,
  triggerComponent: PropTypes.func.isRequired,
  closable: PropTypes.bool,
  closableAction: PropTypes.func,
  closableIconName: PropTypes.string,
  closableIconType: PropTypes.string,
  closableTitle: PropTypes.string,
  customHeader: PropTypes.any,
}

const MenuOptions = (props) => {
  const {
    options,
    triggerComponent,
    closable = true,
    closableAction,
    closableIconName = 'close',
    closableIconType = 'MaterialCommunityIcons',
    closableIconSize = 'large',
    closableIconColor = SHIP_GREY_CALM,
    closableTitle = 'Tutup',
    closableTitleType,
    closableTitleSize,
    closableTitleColor,
    closableTitleWeight,
    closableTitleStyle,
    customHeader,
  } = props

  const _renderItem = (menu, index, toggleModal) => {
    if(React.isValidElement(menu)) return menu
    if (menu.type === 'separator') {
      return (
        <View style={{ borderBottomColor: PALE_BLUE_TWO, borderBottomWidth: 1 }} />
      )
    }
    if (Platform.OS !== 'android' && menu.isPremiumMenu) return null

    return (
      <TouchableOpacity
        activeOpacity={TOUCH_OPACITY}
        key={`${Math.random()}`}
        onPress={async () => {
          await menu.onPress()
          const not_close = menu.not_close || false
          if (!not_close) await toggleModal()
        }}
        style={{
          flexDirection: 'row', paddingVertical: WP4, alignItems: 'center', marginHorizontal: WP4, borderBottomColor: PALE_GREY, borderBottomWidth: menu.withBorder ? 1 : 0
        }}
      >
        {
          menu.iconName && (
            <View style={{ width: WP12 }}>
              <Icon
                size={menu.iconSize || 'large'}
                color={menu.iconColor || SHIP_GREY_CALM}
                name={menu.iconName}
                type={menu.iconType || 'MaterialCommunityIcons'}
                background={menu.iconBackground ? 'dark-circle' : 'none'}
                backgroundColor={menu.iconBackground || NO_COLOR}
              />
            </View>
          )
        }
        {
          menu.image && (
            <View style={{ width: WP12, alignItems: 'flex-start' }}>
              <View>
                <Image
                  source={menu.image}
                  imageStyle={[{ width: menu.imageSize, height: menu.imageSize, aspectRatio: menu.aspectRatio || 1, alignSelf: 'flex-start', }, menu.imageStyle]}
                />
              </View>
            </View>
          )
        }
        <Text type={menu.titleType || 'Circular'} size={menu.titleSize || 'small'} color={menu.titleColor || GUN_METAL} weight={menu.titleWeight || 300} style={menu.titleStyle}>{menu?.title}</Text>
      </TouchableOpacity>
    )
  }
  return (
    <Modal
      style={{ borderTopRightRadius: 0, borderTopLeftRadius: 0, }}
      position='bottom'
      swipeDirection={null}
      onClose={!isUndefined(closableAction) ? closableAction : () => {}}
      renderModalContent={({ toggleModal }) => (
        <View style={{ width: WP100 }}>
          {isValidElement(customHeader) && customHeader}
          <View style={{ paddingVertical: WP2 }}>
            {isArray(options) && options.map((menu, index) => {
              const validation = get(menu, 'validation') == undefined ? true : get(menu, 'validation')
              return isValidElement(menu) ? menu : (!!menu && validation && _renderItem(menu, index, toggleModal))
            })}
            {!!options && !isArray(options) && isValidElement(options) && options}
          </View>
          {closable && (
            <View style={{ width: '100%', borderTopColor: PALE_BLUE_TWO, borderTopWidth: 1 }}>
              {
                _renderItem(
                  {
                    title: closableTitle,
                    titleType: closableTitleType,
                    titleSize: closableTitleSize,
                    titleColor: closableTitleColor,
                    titleWeight: closableTitleWeight,
                    titleStyle: closableTitleStyle,
                    iconName: closableIconName,
                    iconType: closableIconType,
                    iconSize: closableIconSize,
                    iconColor: closableIconColor,
                    onPress: !isUndefined(closableAction) ? closableAction : () => {}
                  },
                  `${Math.random()}`,
                  toggleModal
                )
              }
            </View>
          )}
        </View>
      )}
    >
      {({ toggleModal }, M) => (
        <>
          {
            triggerComponent(toggleModal)
          }
          {M}
        </>
      )}
    </Modal>
  )
}

MenuOptions.propTypes = propsType
export default memo(MenuOptions)
