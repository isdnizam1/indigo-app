import React from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet } from 'react-native'

const propsType = {
  direction: PropTypes.oneOf(['up', 'right', 'down', 'left', 'up-right', 'up-left', 'down-right', 'down-left']),
  width: PropTypes.number,
  height: PropTypes.number,
  color: PropTypes.string,
}

const propsDefault = {
  direction: 'up',
  width: 0,
  height: 0,
  color: 'white',
}

const Triangle = (props) => {

  const _borderStyles = () => {
    if (props.direction == 'up') {
      return {
        borderTopWidth: 0,
        borderRightWidth: props.width / 2.0,
        borderBottomWidth: props.height,
        borderLeftWidth: props.width / 2.0,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: props.color,
        borderLeftColor: 'transparent',
      }
    } else if (props.direction == 'right') {
      return {
        borderTopWidth: props.height / 2.0,
        borderRightWidth: 0,
        borderBottomWidth: props.height / 2.0,
        borderLeftWidth: props.width,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: props.color,
      }
    } else if (props.direction == 'down') {
      return {
        borderTopWidth: props.height,
        borderRightWidth: props.width / 2.0,
        borderBottomWidth: 0,
        borderLeftWidth: props.width / 2.0,
        borderTopColor: props.color,
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
      }
    } else if (props.direction == 'left') {
      return {
        borderTopWidth: props.height / 2.0,
        borderRightWidth: props.width,
        borderBottomWidth: props.height / 2.0,
        borderLeftWidth: 0,
        borderTopColor: 'transparent',
        borderRightColor: props.color,
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
      }
    } else if (props.direction == 'up-left') {
      return {
        borderTopWidth: props.height,
        borderRightWidth: props.width,
        borderBottomWidth: 0,
        borderLeftWidth: 0,
        borderTopColor: props.color,
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
      }
    } else if (props.direction == 'up-right') {
      return {
        borderTopWidth: 0,
        borderRightWidth: props.width,
        borderBottomWidth: props.height,
        borderLeftWidth: 0,
        borderTopColor: 'transparent',
        borderRightColor: props.color,
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
      }
    } else if (props.direction == 'down-left') {
      return {
        borderTopWidth: props.height,
        borderRightWidth: 0,
        borderBottomWidth: 0,
        borderLeftWidth: props.width,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: props.color,
      }
    } else if (props.direction == 'down-right') {
      return {
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: props.height,
        borderLeftWidth: props.width,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: props.color,
        borderLeftColor: 'transparent',
      }
    } else {
      return {}
    }
  }

  let borderStyles = _borderStyles()
  return (
    <View>
      <View style={[styles.triangle, borderStyles, props.style]}/>
      {props.children}
    </View>
  )
}

const styles = StyleSheet.create({
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
  },
})

Triangle.propTypes = propsType
Triangle.defaultProps = propsDefault
export default Triangle
