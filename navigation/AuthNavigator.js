import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import MessageScreen from "../screens/MessageScreen";
import RegisterPersonalScreen from "../screens/sign_up/RegisterPersonalScreen";
import RegisterLocationJobScreen from "../screens/sign_up/RegisterLocationJobScreen";
import RegisterJobScreen from "../screens/sign_up/RegisterJobScreen";
import RegisterLocationScreen from "../screens/sign_up/RegisterLocationScreen";
import RegisterPhotoScreen from "../screens/sign_up/RegisterPhotoScreen";
import RegisterVerifyEmailScreen from "../screens/RegisterVerifyEmailScreen";
import LoginScreen from "../screens/LoginScreen";
import LoginEmailScreen from "../screens/LoginEmailScreen";
import LoginSsoScreen from "../screens/LoginSsoScreen";
import LoginForgotPasswordScreen from "../screens/forgot_password/LoginForgotPasswordScreen";
import LoginForgotInputCodeScreen from "../screens/forgot_password/LoginForgotInputCodeScreen";
import LoginForgotNewPasswordScreen from "../screens/forgot_password/LoginForgotNewPasswordScreen";
import RegisterGenreScreen from "../screens/sign_up/RegisterGenreScreen";
import RegisterVerificationScreen from "../screens/sign_up/RegisterVerificationScreen";
import RegisterV3 from "../screens/register/v3";
import ForgotPasswordScreen from "../screens/ForgotPasswordScreen";
import RecoveryCodeScreen from "../screens/RecoveryCodeScreen";
import ResetPasswordScreen from "../screens/ResetPasswordScreen";

const LoginStack = {
  LoginScreen,
  LoginEmailScreen,
  LoginSsoScreen,
  LoginForgotPasswordScreen,
  LoginForgotInputCodeScreen,
  LoginForgotNewPasswordScreen,
  RegisterPersonalScreen,

  RegisterJobScreen,
  RegisterLocationScreen,

  RegisterLocationJobScreen,
  RegisterPhotoScreen,
  RegisterGenreScreen,
  RegisterVerificationScreen,

  RegisterVerifyEmailScreen,
  MessageScreen,
  RegisterV3,
  ForgotPasswordScreen,
  RecoveryCodeScreen,
  ResetPasswordScreen,
};

// const LoginStack = createStackNavigator({
//   LoginScreen,
//   LoginEmailScreen,
//   LoginForgotPasswordScreen,
//   LoginForgotInputCodeScreen,
//   LoginForgotNewPasswordScreen,
//   RegisterPersonalScreen,

//   RegisterJobScreen,
//   RegisterLocationScreen,

//   RegisterLocationJobScreen,
//   RegisterPhotoScreen,
//   RegisterGenreScreen,
//   RegisterVerificationScreen,

//   RegisterVerifyEmailScreen,
//   MessageScreen,
//   RegisterV3,
//   ForgotPasswordScreen,
//   RecoveryCodeScreen,
//   ResetPasswordScreen
// }, {
//   initialRouteName: 'LoginScreen',
//   headerMode: 'none',
//   animationEnabled: true,
//   defaultNavigationOptions: {
//     gesturesEnabled: false
//   },
//   transitionConfig: () => fromRight()
// })

const TransitionScreenOptions = {
  ...TransitionPresets.SlideFromRightIOS, // This is where the transition happens
};

const Stack = createStackNavigator();

function AuthNavigator() {
  const items = [];

  for (const [key, value] of Object.entries(LoginStack)) {
    items.push(
      <Stack.Screen
        name={key}
        component={value}
        options={TransitionScreenOptions}
      />
    );
  }

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      {items}
    </Stack.Navigator>
  );
}

export default AuthNavigator;
