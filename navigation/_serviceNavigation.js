import * as React from "react";
import { StackActions } from "@react-navigation/native";

const navigationRef = React.createRef();

function navigate(routeName, params = {}) {
  console.log(99991, routeName);
  // navigationRef.current?.navigate('AppNavigator', {
  //   screen: 'ExploreStack',
  //   initial: true,
  //   params: {
  //     screen: routeName,
  //     params,
  //     initial: false,
  //   },
  // })
  navigationRef.current?.navigate("AuthNavigator", {
    // screen: 'LoginScreen',
    screen: "LoginEmailScreen",
    initial: true,
    params: {
      // forceReload: true,
      screen: routeName,
      params,
      initial: false,
    },
  });
}

function push(routeName, params = {}) {
  navigationRef.current?.dispatch(StackActions.push(routeName, params));
}

// add other navigation functions that you need and export them

export default {
  navigationRef,
  navigate,
  push,
};
