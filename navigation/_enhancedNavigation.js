import React from "react";
import { InteractionManager } from "react-native";
import { NavigationActions } from "@react-navigation/compat";
import { CommonActions, StackActions } from "@react-navigation/native";
import { isFunction } from "lodash-es";
import { dispatcher } from "sf-utils/clientHttp";
import { isEqualObject } from "sf-utils/helper";
import { logAmplitudeEvent } from "sf-utils/clientAmplitude";
import bugsnagClient from "sf-utils/bugsnag";
import { setCurrentScreen } from "sf-services/helper/actionDispatcher";
import { connect } from "react-redux";

const mapDispatchToProps = {
  setCurrentScreen,
};

const _mapNavigationParams = (props) => (keyVal, defaultVal) => {
  if (props.route && props.route.params && props.route.params[keyVal]) {
    return props.route.params[keyVal];
  }
  return defaultVal;
};

const _enhancedNavigation = (
  Component,
  mapNavigationParamsToProps = null,
  initialLoading = false
) => {
  class Enhancer extends React.Component {
    constructor(props) {
      super(props);
      let derivedNavigationParams = {};
      if (mapNavigationParamsToProps)
        derivedNavigationParams = mapNavigationParamsToProps(
          _mapNavigationParams(props)
        );
      this.state = {
        isLoading: initialLoading,
        derivedNavigationParams,
      };
    }

    shouldComponentUpdate(nextProps, nextState) {
      const shouldUpdate =
        !isEqualObject(this.state, nextState) ||
        !isEqualObject(nextProps, this.props);
      return shouldUpdate;
    }

    _setState = (state, cb) => {
      this.setState(state, cb);
    };

    dispatch = (
      action,
      data = {},
      beforeLoaded = async (dataResponse) => {},
      allowError = false,
      isLoading = true
    ) => {
      return new Promise((resolve, reject) => {
        this._setState(
          {
            isLoading,
          },
          () => {
            dispatcher(action, data, this._setState, allowError)
              .then(async (dataResponse) => {
                if (isFunction(beforeLoaded)) await beforeLoaded(dataResponse);
                this.setState({ isLoading: false }, () =>
                  resolve(dataResponse)
                );
              })
              .catch((e) => {
                if (e.response) {
                  bugsnagClient.leaveBreadcrumb(e.response.data);
                  bugsnagClient.notify(e);
                } else {
                  bugsnagClient.notify(e);
                }
                resolve({ result: [] });
              });
          }
        );
      });
    };

    navigateBack = (params = null) => {
      this.props.navigation.goBack(null);
      bugsnagClient.leaveBreadcrumb("navigate back");
    };

    popToTop = () => this.props.navigation.popToTop();

    jumpTo = (toScreen, toScreenParams = {}) => {
      const { navigation } = this.props;
      navigation.jumpTo(toScreen, toScreenParams);
    };

    navigateTo = (toScreen, toScreenParams = {}, type = "navigate") => {
      const { navigation, setCurrentScreen } = this.props;
      setCurrentScreen(toScreen);
      if (type == "navigate") {
        navigation.dispatch(
          CommonActions.navigate({
            name: toScreen,
            params: toScreenParams,
          })
        );
      } else if (type == "push") {
        navigation.dispatch(StackActions.push(toScreen, toScreenParams));
      } else if (type == "replace") {
        navigation.dispatch(StackActions.replace(toScreen, toScreenParams));
      }
      this.setState(
        { isLoading: false },
        () => logAmplitudeEvent(toScreen),
        () => bugsnagClient.leaveBreadcrumb(`navigate to ${toScreen}`)
      );
    };

    nestedNavigateTo = (toNavigator, toScreen, toScreenParams = {}) => {
      const { navigation, setCurrentScreen } = this.props;
      setCurrentScreen(toScreen);
      navigation.navigate(toNavigator, {
        screen: toScreen,
        params: toScreenParams,
      });
      this.setState(
        { isLoading: false },
        () => logAmplitudeEvent(toScreen),
        () => bugsnagClient.leaveBreadcrumb(`navigate to ${toScreen}`)
      );
    };

    resetNavigation = (routeName) => {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName })],
      });
      this.props.navigation.dispatch(resetAction);
    };

    componentDidMount = async () => {
      InteractionManager.runAfterInteractions(() => {
        this.setState({
          isLoading: false,
        });
      });
    };

    render() {
      const { isLoading, derivedNavigationParams } = this.state;
      return (
        <Component
          {...this.props}
          isLoading={isLoading}
          setLoading={(isLoading) => this.setState({ isLoading })}
          dispatch={this.dispatch}
          navigateTo={this.navigateTo}
          navigateBack={this.navigateBack}
          resetNavigation={this.resetNavigation}
          popToTop={this.popToTop}
          {...derivedNavigationParams}
        />
      );
    }
  }

  Enhancer.navigationOptions = Component.navigationOptions || undefined;
  return connect(null, mapDispatchToProps)(Enhancer);
};

export default _enhancedNavigation;
