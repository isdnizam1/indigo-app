import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  CardStyleInterpolators,
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";
import { TabBarBottom } from "../components";
import TimelineScreen from "../screens/TimelineScreen";
import FeedPostScreen from "../screens/FeedPostScreen";
import FeedPostTopic from "../screens/FeedPostTopic";
import FeedDetailScreen from "../screens/FeedDetailScreen";
import ExploreScreen from "../screens/ExploreScreen";
import FriendSuggestionScreen from "../screens/FriendSuggestionScreen";
import ProfileFollowScreen from "../screens/ProfileFollowScreen";
import ProfileScreen from "../screens/profile/v2/ProfileScreen";
import ProfileScreenNoTab from "../screens/profile/v2/ProfileScreen";
import SettingScreen from "../screens/SettingScreen";
import StartupProfileSettingScreen from "../screens/StartupProfileSettingScreen";
import ProfileUpdateScreen from "../screens/_unsafe/ProfileUpdateScreen";
import ProfileUpdateAboutScreen from "../screens/ProfileUpdateAboutScreen";
import ProfileUpdateBasicScreen from "../screens/ProfileUpdateBasicScreen";
import ProfileUpdatePasswordScreen from "../screens/ProfileUpdatePasswordScreen";
import ProfileAddSongScreen from "../screens/_unsafe/ProfileAddSongScreen";
import SongLinkForm from "../screens/song/SongLinkForm";
import ProfileAddVideoScreen from "../screens/ProfileAddVideoScreen";
import ProfileShowArtScreen from "../screens/ProfileShowArtScreen";
import ProfileShowArtScreenNoTab from "../screens/ProfileShowArtScreen";
import ProfileAddArtScreen from "../screens/ProfileAddArtScreen";
import SoundfrenBlogScreen from "../screens/SoundfrenBlogScreen";
import AdsDetailScreen from "../components/ads/AdsDetailScreen";
import AdsDetailScreenNoTab from "../components/ads/AdsDetailScreenNoTab";
import NotificationScreen from "../screens/NotificationScreen";
import ChatScreen from "../screens/ChatScreen";
import ChatRoomScreen from "../screens/ChatRoomScreen";
import SearchScreen from "../screens/SearchScreen";
import StartupSearchScreen from "../screens/startup_search/StartupSearchScreen";
import AdsScreen from "../screens/AdsScreen";
import AdsScreenNoTab from "../screens/AdsScreen";
import SubmissionRegisterForm from "../screens/submission/SubmissionRegisterForm";
import StartupSubmissionForm from "../screens/submission/StartupSubmissionForm";
import SelectWorks from "../components/jobRegistration/SelectWorks";
import VenueBookingScreen from "../screens/VenueBookingScreen";
import VenueBookingResultScreen from "../screens/VenueBookingResultScreen";
import LikesScreen from "../screens/LikesScreen";
import ImagePreviewScreen from "../screens/ImagePreviewScreen";
import ImagePreviewScreenNoTab from "../screens/ImagePreviewScreen";
import PromoteBandScreen from "../screens/PromoteBandScreen";
import PackageScreen from "../screens/PackageScreen";
import CreateArtistPage1 from "../screens/promote_band/CreateArtistPage1";
import CreateArtistPage2 from "../screens/promote_band/CreateArtistPage2";
import CreateArtistPage3 from "../screens/promote_band/CreateArtistPage3";
import CreateArtistPage4 from "../screens/promote_band/CreateArtistPage4";
import CreateArtistPage5 from "../screens/promote_band/CreateArtistPage5";
import PromoteUser1 from "../screens/promote_user/PromoteUser1";
import PromoteUser2 from "../screens/promote_user/PromoteUser2";
import PromoteUser3 from "../screens/promote_user/PromoteUser3";
import PromoteUser4 from "../screens/promote_user/PromoteUser4";
import PromoteCollab from "../screens/_unsafe/promote_collab/PromoteCollab";
import PromoteCollab1 from "../screens/_unsafe/promote_collab/PromoteCollab1";
import PromoteCollab2 from "../screens/_unsafe/promote_collab/PromoteCollab2";
import BrowserScreen from "../screens/BrowserScreen";
import SubscriptionScreen from "../screens/SubscriptionScreen";
import RegisterGenreInterestScreen from "../screens/sign_up/RegisterGenreInterestScreen";
import CreateExperience from "../screens/experience/CreateExperience";
import CreateExperienceMusic from "../screens/experience/CreateExperienceMusic";
import CreateExperienceFinish from "../screens/experience/CreateExperienceFinish";
import CreateExperiencePerformance from "../screens/experience/CreateExperiencePerformance";
import ListExperienceScreen from "../screens/experience/ListExperienceScreen";
import EditExperienceMusic from "../screens/experience/EditExperienceMusic";
import EditExperiencePerformance from "../screens/experience/EditExperiencePerformance";
import Submission from "../screens/submission/v2/Submission";
import SubmissionForm from "../screens/submission/SubmissionForm";
import SubmissionPreview from "../screens/submission/SubmissionPreview";
import Collaboration from "../screens/collaboration/Collaboration";
import CollaborationForm from "../screens/collaboration/v2/CollaborationForm";
import CollaborationPreview from "../screens/collaboration/CollaborationPreview";
import ProfileForm from "../screens/profile/v2/ProfileForm";
import Song from "../screens/song/Song";
import SongFileForm from "../screens/song/SongFileForm";
import PortofolioForm from "../screens/ProfileAddPortofolioScree";
import FinishScreen from "../screens/FinishScreen";
import NewGroupChatScreen from "../screens/chat/NewGroupChatScreen";
import InviteFriendsScreen from "../screens/InviteFriendsScreen";
import BrowserScreenNoTab from "../screens/BrowserScreenNoTab";
import ProjectDashboard from "../screens/project/ProjectDashboard";
import AdRequests from "../screens/project/AdRequests";
import WorkListScreen from "../screens/works/WorkListScreen";
import SelectVisual from "../components/jobRegistration/SelectVisual";
import PremiumVideoTeaser from "../screens/premium_content/PremiumVideoTeaser";
import PremiumVideoFull from "../screens/premium_content/PremiumVideoFull";
import VideoListScreen from "../screens/VideoListScreen";
import GroupDetailScreen from "../screens/chat/GroupDetailScreen";
import SoundconnectScreen from "../screens/soundconnect/SoundconnectScreen";
import ScListScreen from "../screens/soundconnect/ScListScreen";
import SessionDetail from "../screens/soundconnect/SessionDetail";
import SessionRegisterForm from "../screens/soundconnect/SessionRegisterForm";
import SpeakerDetail from "../screens/soundconnect/SpeakerDetail";
import VoucherListScreen from "../screens/voucher/VoucherListScreen";
import VoucherDetailScreen from "../screens/voucher/VoucherDetailScreen";
import SoundplayDetail from "../screens/soundplay/SoundplayDetail";
import SoundplayScreen from "../screens/soundplay/SoundplayScreen";
import SoundplayListAlbum from "../screens/soundplay/SoundplayListAlbum";
import ShareScreen from "../screens/ShareScreen";
import DetailExperienceScreen from "../screens/experience/DetailExperienceScreen";
import MyRewardScreen from "../screens/gamification/MyRewardScreen";
import RewardScreen from "../screens/gamification/RewardScreen";
import ClaimAlbumList from "../screens/gamification/ClaimAlbumList";
import ClaimSessionList from "../screens/gamification/ClaimSessionList";
import ClaimRewardForm from "../screens/gamification/ClaimRewardForm";
import SoundfrenLearnScreen from "../screens/learn/SoundfrenLearnScreen";
import PodcastListScreen from "../screens/learn/PodcastListScreen";
import SoundfrenLearnAboutScreen from "../screens/learn/SoundfrenLearnAboutScreen";
import VideoCategoryScreen from "../screens/learn/VideoCategoryScreen";
import VideoSubCategoryScreen from "../screens/learn/VideoSubCategoryScreen";
import VideoMMBListScreen from "../screens/learn/VideoMMBListScreen";
import CollaborationScreen from "../screens/collaboration/v2/CollaborationScreen";
import CollaborationScreenV3 from "../screens/collaboration/v3/CollaborationScreenV3";
import MyCollaborationScreen from "../screens/collaboration/v3/MyCollaborationScreen";
import MyNetworkScreen from "../screens/collaboration/v3/MyNetworkScreen";
import ExploreCollaborationScreen from "../screens/collaboration/v3/ExploreCollaborationScreen";
import CollaborationGroupchatScreen from "../screens/collaboration/v3/CollaborationGroupchatScreen";
import CollaborationDetailScreen from "../screens/collaboration/v3/CollaborationDetailScreen";
import CollaboratorListScreen from "../screens/collaboration/v2/CollaboratorListScreen";
import CollaborationListScreen from "../screens/collaboration/v2/CollaborationListScreen";
import SearchCollaborationScreen from "../screens/collaboration/v2/SearchCollaborationScreen";
import FeedbackScreen from "../screens/FeedbackScreen";
import GalleryScreen from "../screens/profile/v2/GalleryScreen";
import ArtistSpotlightScreen from "../screens/artist_spotlight/ArtistSpotlightScreen";
import PopularArtistListScreen from "../screens/artist_spotlight/PopularArtistListScreen";
import NewMusicListScreen from "../screens/artist_spotlight/NewMusicListScreen";
import NewArtistListScreen from "../screens/artist_spotlight/NewArtistListScreen";
import ArtistSpotlightSearchScreen from "../screens/artist_spotlight/ArtistSpotlightSearchScreen";
import ArtworkGridScreen from "../screens/profile/v2/ProfileScreen/TabComponents/ArtworkGridScreen";
import ScheduleForm from "../screens/profile/v2/schedule/ScheduleForm";
import AboutFeatureScreen from "../screens/AboutFeatureScreen";
import CreateSessionContentForm from "../screens/create_session/CreateSessionContentForm";
import CreateSessionSpeakerForm from "../screens/create_session/CreateSessionSpeakerForm";
import CreateSubmissionDetailForm from "../screens/submission/CreateSubmissionDetailForm";
import CreateSubmissionInfoForm from "../screens/submission/CreateSubmissionInfoForm";
import NewsDetailScreen from "../screens/news/NewsDetailScreen";
import FindFriendScreen from "../screens/find_friend/FindFriendScreen";
import ContactScreen from "../screens/chat/ContactScreen";
import AddMemberGroupChatScreen from "../screens/chat/AddMemberGroupChatScreen";
import ChooseAdminGroup from "../screens/chat/ChooseAdminGroup";
import LuckyFrenScreen from "../screens/lucky_fren/LuckyFrenScreen";
import ListRankingScreen from "../screens/lucky_fren/ListRankingScreen";
import MissionScreen from "../screens/lucky_fren/MissionScreen";
import MissionCategoryDetailScreen from "../screens/lucky_fren/MissionCategoryDetailScreen";
import LuckyBoxScreen from "../screens/lucky_fren/LuckyBoxScreen";
import RewardDetailScreen from "../screens/lucky_fren/RewardDetailScreen";
import ClaimRewardScreen from "../screens/lucky_fren/ClaimRewardScreen";
import SuccessClaimRewardScreen from "../screens/lucky_fren/SuccessClaimRewardScreen";
import ProfileVisitScreen from "../screens/ProfileVisitScreen";
import MembershipScreen from "../screens/MembershipScreen";
import MembershipDurationScreen from "../screens/MembershipScreen/MembershipDurationScreen";
import PaymentMethodScreen from "../screens/MembershipScreen/PaymentMethodScreen";
import PaymentHistory from "../screens/profile/setting/PaymentHistory";
import PaymentWebView from "../screens/payment/PaymentWebView";
import YoutubePreviewScreen from "../screens/YoutubePreviewScreen";
import LoadingModalScreen from "../screens/LoadingModalScreen";
import HomeStartupScreen from "../screens/home_startup/HomeStartupScreen";
import StartupLearnScreen from "../screens/home_startup/StartupLearnScreen";
import StartupMissionHomeScreen from "../screens/home_startup/StartupMissionHomeScreen";
import DetailGeneralContentScreen from "../screens/home_startup/DetailGeneralContentScreen";
import ListArticleStartupScreen from "../screens/home_startup/ListArticleStartupScreen";
import Forum1000StartupScreen from "../screens/home_startup/Forum1000StartupScreen";
import Notification1000StartupScreen from "../screens/home_startup/Notification1000StartupScreen";
import StartUpProfileScreen from "../screens/startup_profile/v2/ProfileScreen";
import StartUpProfileForm from "../screens/startup_profile/v2/ProfileForm";
import PodcastCategoryScreen from "../screens/home_startup/podcast/PodcastCategoryScreen";
import PodcastModuleScreen from "../screens/home_startup/podcast/PodcastModuleScreen";
import PodcastByModuleScreen from "../screens/home_startup/podcast/PodcastByModuleScreen";
import TeamUpScreen from "../screens/team_up/TeamUpScreen";
import TeamUpInvitation from "../screens/team_up/TeamUpInvitation";
import TeamUpSearch from "../screens/team_up/TeamUpSearch";
import TeamDiscussion from "../screens/team_up/TeamDiscussion";
import TeamTask from "../screens/team_up/TeamTask";
import StartUpCommentScreen from "../screens/StartUpCommentScreen";
import AuthNavigator from "./AuthNavigator";
import EventSelectionScreen from "../screens/EventSelectionScreen";
import * as Analytics from "expo-firebase-analytics";

const screens = {
  FeedPostScreen,
  FeedPostTopic,
  FeedDetailScreen,
  AdsDetailScreen,
  AdsDetailScreenNoTab,
  ProfileScreen,
  ProfileFollowScreen,
  ChatScreen,
  ChatRoomScreen,
  ProfileUpdateScreen,
  ProfileUpdateBasicScreen,
  ProfileAddSongScreen,
  SongLinkForm,
  ProfileAddVideoScreen,
  ProfileAddArtScreen,
  ProfileShowArtScreen,
  ProfileUpdateAboutScreen,
  ProfileUpdatePasswordScreen,
  SettingScreen,
  StartupProfileSettingScreen,
  FriendSuggestionScreen,
  LikesScreen,
  AdsScreen,
  SelectWorks,
  VenueBookingScreen,
  VenueBookingResultScreen,
  ImagePreviewScreen,
  ImagePreviewScreenNoTab,
  PromoteBandScreen,
  PackageScreen,
  CreateArtistPage1,
  CreateArtistPage2,
  CreateArtistPage3,
  CreateArtistPage4,
  CreateArtistPage5,
  PromoteUser1,
  PromoteUser2,
  PromoteUser3,
  PromoteUser4,
  PromoteCollab,
  PromoteCollab1,
  PromoteCollab2,
  BrowserScreen,
  SubscriptionScreen,
  RegisterGenreInterestScreen,
  CreateExperience,
  CreateExperienceMusic,
  CreateExperiencePerformance,
  CreateExperienceFinish,
  SoundfrenBlogScreen,
  ListExperienceScreen,
  EditExperienceMusic,
  EditExperiencePerformance,
  Submission,
  SubmissionForm,
  SubmissionPreview,
  SubmissionRegisterForm,
  StartupSubmissionForm,
  Collaboration,
  MyCollaborationScreen,
  MyNetworkScreen,
  ExploreCollaborationScreen,
  CollaborationGroupchatScreen,
  CollaborationDetailScreen,
  CollaborationForm,
  CollaborationPreview,
  ProfileForm,
  Song,
  SongFileForm,
  PortofolioForm,
  FinishScreen,
  InviteFriendsScreen,
  BrowserScreenNoTab,
  ProjectDashboard,
  AdRequests,
  ProfileScreenNoTab,
  SelectVisual,
  PremiumVideoTeaser,
  PremiumVideoFull,
  VideoListScreen,
  WorkListScreen,
  GroupDetailScreen,
  NewGroupChatScreen,
  SoundconnectScreen,
  ScListScreen,
  SessionDetail,
  SpeakerDetail,
  SessionRegisterForm,
  VoucherListScreen,
  VoucherDetailScreen,
  SoundplayDetail,
  SoundplayScreen,
  SoundplayListAlbum,
  ShareScreen,
  ProfileShowArtScreenNoTab,
  DetailExperienceScreen,
  AdsScreenNoTab,
  MyRewardScreen,
  RewardScreen,
  ClaimAlbumList,
  ClaimSessionList,
  ClaimRewardForm,
  SoundfrenLearnScreen,
  PodcastListScreen,
  SoundfrenLearnAboutScreen,
  VideoCategoryScreen,
  VideoSubCategoryScreen,
  VideoMMBListScreen,
  CollaborationScreen,
  CollaborationScreenV3,
  CollaboratorListScreen,
  CollaborationListScreen,
  SearchCollaborationScreen,
  FeedbackScreen,
  GalleryScreen,
  ArtistSpotlightScreen,
  NewArtistListScreen,
  NewMusicListScreen,
  PopularArtistListScreen,
  ArtistSpotlightSearchScreen,
  ArtworkGridScreen,
  ScheduleForm,
  AboutFeatureScreen,
  CreateSessionContentForm,
  CreateSessionSpeakerForm,
  CreateSubmissionDetailForm,
  CreateSubmissionInfoForm,
  NewsDetailScreen,
  FindFriendScreen,
  SearchScreen,
  StartupSearchScreen,
  ContactScreen,
  AddMemberGroupChatScreen,
  ChooseAdminGroup,
  LuckyFrenScreen,
  MissionScreen,
  MissionCategoryDetailScreen,
  ListRankingScreen,
  LuckyBoxScreen,
  RewardDetailScreen,
  ClaimRewardScreen,
  SuccessClaimRewardScreen,
  ProfileVisitScreen,
  TimelineScreen,
  MembershipScreen,
  MembershipDurationScreen,
  PaymentMethodScreen,
  PaymentHistory,
  PaymentWebView,
  NotificationScreen,
  YoutubePreviewScreen,
  HomeStartupScreen,
  StartupLearnScreen,
  DetailGeneralContentScreen,
  StartUpProfileScreen,
  StartUpProfileForm,
  Forum1000StartupScreen,
  Notification1000StartupScreen,
  ListArticleStartupScreen,
  StartupMissionHomeScreen,
  PodcastCategoryScreen,
  PodcastModuleScreen,
  PodcastByModuleScreen,
  TeamUpScreen,
  TeamDiscussion,
  TeamTask,
  TeamUpInvitation,
  TeamUpSearch,
  StartUpCommentScreen,
  EventSelectionScreen,
};

const tabBarVisibleScreens = [
  "CreateArtistPage1",
  "CreateArtistPage2",
  "CreateArtistPage3",
  "CreateArtistPage4",
  "CreateArtistPage5",
  "PromoteUser1",
  "PromoteUser2",
  "PromoteUser3",
  "PromoteUser4",
  "PromoteCollab1",
  "PromoteCollab2",
  "FeedPostScreen",
  "FeedPostTopic",
  "ProfileUpdateScreen",
  "AdsDetailScreenNoTab",
  "SubmissionRegisterForm",
  "StartupSubmissionForm",
  "SoundfrenBlogScreen",
  "CreateExperience",
  "CreateExperienceMusic",
  "CreateExperiencePerformance",
  "CreateExperienceFinish",
  "EditExperienceMusic",
  "EditExperiencePerformance",
  "Submission",
  "SubmissionForm",
  "SubmissionPreview",
  "Collaboration",
  "MyCollaborationScreen",
  "MyNetworkScreen",
  "ExploreCollaborationScreen",
  "CollaborationGroupchatScreen",
  "CollaborationDetailScreen",
  "CollaborationForm",
  "CollaborationPreview",
  "Song",
  "SongFileForm",
  "ProfileAddVideoScreen",
  "SelectWorks",
  "FinishScreen",
  "SettingScreen",
  "ProfileForm",
  "InviteFriendsScreen",
  "ChatScreen",
  "ChatRoomScreen",
  "NewGroupChatScreen",
  "FeedDetailScreen",
  "ProfileAddVideoScreen",
  "ProfileAddArtScreen",
  "PromoteBandScreen",
  "BrowserScreenNoTab",
  "ProjectDashboard",
  "AdRequests",
  "ProfileScreenNoTab",
  "SelectVisual",
  "PremiumVideoTeaser",
  "PremiumVideoFull",
  "VideoListScreen",
  "GroupDetailScreen",
  "SoundconnectScreen",
  "ScListScreen",
  "SessionDetail",
  "SpeakerDetail",
  "SessionRegisterForm",
  "VoucherListScreen",
  "VoucherDetailScreen",
  "SoundplayDetail",
  "SoundplayScreen",
  "SoundplayListAlbum",
  "ShareScreen",
  "ProfileShowArtScreenNoTab",
  "ImagePreviewScreen",
  "ImagePreviewScreenNoTab",
  "AdsScreenNoTab",
  "DetailExperienceScreen",
  "FriendSuggestionScreen",
  "MyRewardScreen",
  "RewardScreen",
  "ClaimAlbumList",
  "ClaimSessionList",
  "ClaimRewardForm",
  "SoundfrenLearnScreen",
  "PodcastListScreen",
  "SoundfrenLearnAboutScreen",
  "VideoCategoryScreen",
  "VideoSubCategoryScreen",
  "VideoMMBListScreen",
  "CollaborationScreen",
  "CollaborationScreenV3",
  "CollaboratorListScreen",
  "CollaborationListScreen",
  "SearchCollaborationScreen",
  "FeedbackScreen",
  "GalleryScreen",
  "ArtistSpotlightScreen",
  "NewArtistListScreen",
  "NewMusicListScreen",
  "PopularArtistListScreen",
  "ArtistSpotlightSearchScreen",
  "ArtworkGridScreen",
  "ScheduleForm",
  "AboutFeatureScreen",
  "CreateSessionContentForm",
  "CreateSessionSpeakerForm",
  "CreateSubmissionDetailForm",
  "CreateSubmissionInfoForm",
  "NewsDetailScreen",
  "FindFriendScreen",
  "SearchScreen",
  "StartupSearchScreen",
  "ContactScreen",
  "AddMemberGroupChatScreen",
  "ChooseAdminGroup",
  "ProfileUpdatePasswordScreen",
  "LuckyFrenScreen",
  "MissionScreen",
  "MissionCategoryDetailScreen",
  "ListRankingScreen",
  "LuckyBoxScreen",
  "RewardDetailScreen",
  "ClaimRewardScreen",
  "SuccessClaimRewardScreen",
  "ProfileVisitScreen",
  "PackageScreen",
  "MembershipScreen",
  "MembershipDurationScreen",
  "PaymentMethodScreen",
  "PaymentHistory",
  "PaymentWebView",
  "YoutubePreviewScreen",
  "HomeStartupScreen",
  "StartupLearnScreen",
  "DetailGeneralContentScreen",
  "StartUpProfileScreen",
  "StartUpProfileForm",
  "Forum1000StartupScreen",
  "Notification1000StartupScreen",
  "ListArticleStartupScreen",
  "StartupMissionHomeScreen",
  "PodcastCategoryScreen",
  "PodcastModuleScreen",
  "PodcastByModuleScreen",
  "TeamUpScreen",
  "TeamDiscussion",
  "TeamTask",
  "TeamUpInvitation",
  "TeamUpSearch",
  "StartUpCommentScreen",
  "EventSelectionScreen",
];

const Stack = createStackNavigator();

// home screen
const ExploreStack = ({ navigation, route }) => {
  React.useLayoutEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    Analytics.setCurrentScreen(routeName);
    Analytics.logEvent(routeName, {
      screen_name: routeName,
    });
    if (tabBarVisibleScreens.includes(routeName)) {
      navigation.setOptions({ tabBarVisible: false });
    } else {
      navigation.setOptions({ tabBarVisible: true });
    }
  }, [navigation, route]);
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      {Object.entries({
        ExploreScreen,
        ...screens,
      }).map(([key, stack]) => {
        return <Stack.Screen key={key} name={key} component={stack} />;
      })}
    </Stack.Navigator>
  );
};

// feed screen
const HomeStack = ({ navigation, route }) => {
  React.useLayoutEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    Analytics.logEvent(routeName, {
      screen_name: routeName,
    });
    if (tabBarVisibleScreens.includes(routeName)) {
      navigation.setOptions({ tabBarVisible: false });
    } else {
      navigation.setOptions({ tabBarVisible: true });
    }
  }, [navigation, route]);
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      {Object.entries({
        TimelineScreen,
        ...screens,
      }).map(([key, stack]) => {
        return <Stack.Screen key={key} name={key} component={stack} />;
      })}
    </Stack.Navigator>
  );
};

// notification screen
const NotificationStack = ({ navigation, route }) => {
  React.useLayoutEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    Analytics.logEvent(routeName, {
      screen_name: routeName,
    });
    if (tabBarVisibleScreens.includes(routeName)) {
      navigation.setOptions({ tabBarVisible: false });
    } else {
      navigation.setOptions({ tabBarVisible: true });
    }
  }, [navigation, route]);
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      {Object.entries({
        NotificationScreen,
        ...screens,
      }).map(([key, stack]) => {
        return <Stack.Screen key={key} name={key} component={stack} />;
      })}
    </Stack.Navigator>
  );
};

// profile screen
const ProfileStack = ({ navigation, route }) => {
  React.useLayoutEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    Analytics.logEvent(routeName, {
      screen_name: routeName,
    });
    if (tabBarVisibleScreens.includes(routeName)) {
      navigation.setOptions({ tabBarVisible: false });
    } else {
      navigation.setOptions({ tabBarVisible: true });
    }
  }, [navigation, route]);
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      {Object.entries({
        ProfileScreen,
        ...screens,
      }).map(([key, stack]) => {
        return <Stack.Screen key={key} name={key} component={stack} />;
      })}
    </Stack.Navigator>
  );
};

const TransitionScreenOptions = {
  ...TransitionPresets.SlideFromRightIOS, // This is where the transition happens
};

const Tab = createBottomTabNavigator();

function AppNavigator() {
  return (
    <Tab.Navigator tabBar={(props) => <TabBarBottom {...props} />}>
      <Tab.Screen
        name="HomeStack"
        component={ExploreStack}
        options={TransitionScreenOptions}
      />
      <Tab.Screen
        name="FeedStack"
        component={HomeStack}
        options={TransitionScreenOptions}
      />
      <Tab.Screen
        name="NotificationStack"
        component={NotificationStack}
        options={TransitionScreenOptions}
      />
      <Tab.Screen
        name="ProfileStack"
        component={ProfileStack}
        options={TransitionScreenOptions}
      />
    </Tab.Navigator>
  );
}

function RootStackScreen() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: { backgroundColor: "transparent" },
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({ current: { progress } }) => ({
          cardStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 0.5, 0.9, 1],
              outputRange: [0, 0.25, 0.7, 1],
            }),
          },
          overlayStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0.5],
              extrapolate: "clamp",
            }),
          },
        }),
      }}
      mode="modal"
    >
      <Stack.Screen
        name="Main"
        component={AppNavigator}
        options={{ headerShown: false }}
      />
      <Stack.Screen name="AuthNavigator" component={AuthNavigator} />
      <Stack.Screen
        name="EventSelectionScreen"
        component={EventSelectionScreen}
      />
      <Stack.Screen name="LoadingModalScreen" component={LoadingModalScreen} />
    </Stack.Navigator>
  );
}

export default RootStackScreen;
