import React from "react";
import {
  Keyboard,
  Platform,
  TouchableOpacity,
  View,
  Image,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";
import { TabActions } from "@react-navigation/native";
import Text from "sf-components/Text";
import { logAmplitudeEvent } from "sf-utils/clientAmplitude";
import { STOP } from "sf-services/player/actionTypes";
import { WP205 } from "sf-constants/Sizes";
import { setCameFromBottomNavigation } from "sf-services/helper/actionDispatcher";
import style from "sf-styles/bottomNavigator";
import Draggable from "react-native-draggable";
import { isEmpty } from "lodash";
import { getBottomSpace } from "../utils/iphoneXHelper";
import { WHITE } from "../constants/Colors";
import { HP100 } from "../constants/Sizes";
import * as Analytics from "expo-firebase-analytics";

const windows = Dimensions.get("window");

class TabBarBottom extends React.Component {
  constructor(props) {
    super(props);
    this.keyboardWillShow = this.keyboardWillShow.bind(this);
    this.keyboardWillHide = this.keyboardWillHide.bind(this);
    this.state = {
      isVisible: true,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  UNSAFE_componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener(
      "keyboardDidShow",
      this.keyboardWillShow
    );
    this.keyboardWillHideSub = Keyboard.addListener(
      "keyboardDidHide",
      this.keyboardWillHide
    );
  }

  componentWillUnmount() {
    setTimeout(() => {
      this.keyboardWillShowSub.remove();
      this.keyboardWillHideSub.remove();
    }, 750);
  }

  keyboardWillShow = (event) => {
    setTimeout(() => {
      this.setState({
        isVisible: Platform.select({
          ios: true,
          android: false,
        }),
      });
    }, 750);
  };

  keyboardWillHide = (event) => {
    setTimeout(() => {
      this.setState({
        isVisible: true,
      });
    }, 750);
  };

  onTabPress = (index) => {
    const { state, navigation } = this.props;

    const { routes } = state;

    const event = navigation.emit({
      type: "tabPress",
      target: routes[index].key,
      canPreventDefault: true,
    });
    Analytics.logEvent(routes[index].name, {
      screen_name: routes[index].name,
    });
    if (!event.defaultPrevented) {
      navigation.dispatch({
        ...TabActions.jumpTo(routes[index].name),
        target: state.key,
      });
    }
  };

  render() {
    const { state, navigation, showNotificationFlag, descriptors, userData } =
      this.props;

    const { routes, index: activeRouteIndex } = state;

    const options = descriptors[routes[activeRouteIndex].key].options;

    if (options && options.tabBarVisible === false) {
      return null;
    }

    return this.state.isVisible ? (
      <View>
        {/* <View style={{
          bottom: HP100,
        }} >
          <Draggable
            x={windows.width - 90}
            y={windows.height / 3}
            minX={10} minY={10}
            maxX={windows.width}
            maxY={windows.height - 40}
            onShortPressRelease={() => {
              if (isEmpty(this.props.idUser)) {
                this.props.navigation.navigate('AuthNavigator', {
                  screen: 'LoginScreen',
                  params: {
                    forceReload: true,
                  },
                })
              } else {
                navigation.navigate('HomeStartupScreen')

              }
            }}
          >
            <Image
              style={{ ...style.buttonKanal }}
              source={require('sf-assets/images/kanal1000Startup.png')}
            />
          </Draggable>
        </View> */}

        <View style={style.wrapper}>
          <Image
            style={style.backgroundImage}
            resizeMode={"cover"}
            source={require("sf-assets/images/bottomNavbarCurved.png")}
          />
          <TouchableOpacity
            activeOpacity={0.75}
            style={style.wrapButton}
            onPress={() => navigation.navigate("FeedPostScreen")}
          >
            <Image
              style={style.addButton}
              source={require("sf-assets/icons/bottomNavbar/plusCircleGradient.png")}
            />
          </TouchableOpacity>
          <View style={style.innerWrapper}>
            <TouchableOpacity
              onPress={() => {
                logAmplitudeEvent(routes[0].name);
                this.props.playerStop();
                this.onTabPress(0);
              }}
              style={style.tabItem}
            >
              <View style={style.tabItemWrapper}>
                <Image
                  style={style.tabIcon}
                  source={
                    activeRouteIndex == 0
                      ? require("sf-assets/icons/bottomNavbar/homeActive.png")
                      : require("sf-assets/icons/bottomNavbar/homeInactive.png")
                  }
                />
                <Text
                  style={[
                    style.tabLabel,
                    activeRouteIndex == 0 ? style.tabLabelActive : null,
                  ]}
                  numberOfLines={1}
                  size={"tiny"}
                  center
                >
                  Home
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                logAmplitudeEvent(routes[1].name);
                this.props.playerStop();
                this.onTabPress(1);
              }}
              style={style.tabItem}
            >
              <View style={style.tabItemWrapper}>
                <Image
                  style={style.tabIcon}
                  source={
                    activeRouteIndex == 1
                      ? require("sf-assets/icons/bottomNavbar/timelineActive.png")
                      : require("sf-assets/icons/bottomNavbar/timelineInactive.png")
                  }
                />
                <Text
                  style={[
                    style.tabLabel,
                    activeRouteIndex == 1 ? style.tabLabelActive : null,
                  ]}
                  numberOfLines={1}
                  size={"tiny"}
                  center
                >
                  Feed
                </Text>
              </View>
            </TouchableOpacity>

            <View style={{ ...style.tabItem, flex: 1.28 }} />

            <TouchableOpacity
              onPress={() => {
                logAmplitudeEvent(routes[2].name);
                this.props.playerStop();
                this.onTabPress(2);
              }}
              style={style.tabItem}
            >
              <View style={style.tabItemWrapper}>
                <View>
                  <Image
                    style={style.tabIcon}
                    source={
                      activeRouteIndex == 2
                        ? require("sf-assets/icons/bottomNavbar/notificationActive.png")
                        : require("sf-assets/icons/bottomNavbar/notificationInactive.png")
                    }
                  />
                  {showNotificationFlag && (
                    <Image
                      style={{
                        position: "absolute",
                        width: WP205,
                        height: WP205,
                        top: 2,
                        right: 2,
                      }}
                      source={require("sf-assets/icons/v3/dotReddish.png")}
                    />
                  )}
                </View>
                <Text
                  style={[
                    style.tabLabel,
                    activeRouteIndex == 2 ? style.tabLabelActive : null,
                  ]}
                  numberOfLines={1}
                  size={"tiny"}
                  center
                >
                  Notifications
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                logAmplitudeEvent(routes[3].name);
                setCameFromBottomNavigation(true);
                this.props.playerStop();
                this.onTabPress(3);
              }}
              style={style.tabItem}
            >
              <View style={style.tabItemWrapper}>
                <Image
                  style={style.tabIcon}
                  source={
                    activeRouteIndex == 3
                      ? require("sf-assets/icons/bottomNavbar/profileActive.png")
                      : require("sf-assets/icons/bottomNavbar/profileInactive.png")
                  }
                />
                <Text
                  style={[
                    style.tabLabel,
                    activeRouteIndex == 3 ? style.tabLabelActive : null,
                  ]}
                  numberOfLines={1}
                  size={"tiny"}
                  center
                >
                  Profile
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            backgroundColor: WHITE,
            width: "100%",
            height: getBottomSpace() / 2,
          }}
        />
      </View>
    ) : (
      <View />
    );
  }
}

const mapStateToProps = ({
  notification: { showFlag: showNotificationFlag },
  auth,
}) => ({
  showNotificationFlag,
  idUser: auth.user.id_user,
});

const mapDispatchToProps = {
  playerStop: () => ({ type: STOP }),
  setCameFromBottomNavigation,
};

export default connect(mapStateToProps, mapDispatchToProps)(TabBarBottom);
