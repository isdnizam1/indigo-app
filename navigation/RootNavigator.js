import React from 'react'
import { NavigationContainer, DefaultTheme } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { connect } from 'react-redux'
import PreLoadingScreen from '../screens/PreLoadingScreen'
import LoadingScreen from '../screens/LoadingScreen'
import AppNavigator from './AppNavigator'
import NavigationService from './_serviceNavigation'
import { Text, View } from 'react-native'
import LoginScreen from '../screens/LoginScreen'
import AuthNavigator from './AuthNavigator'

const mapStateToProps = ({ auth, app }) => ({
  userData: auth.user,
  isLoading: app.isLoading,
  isLogin: app.isLogin,
})

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: 'transparent',
  },
}

function HomeScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
    </View>
  );
}

const Stack = createStackNavigator()
function App({ userData, isLoading, isLogin }) {
  return (
    <NavigationContainer theme={MyTheme} ref={NavigationService.navigationRef}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          cardStyle: { backgroundColor: "transparent" },
          cardOverlayEnabled: true,
          // cardStyleInterpolator: ({ current: { progress } }) => ({
          //   cardStyle: {
          //     opacity: progress.interpolate({
          //       inputRange: [0, 0.5, 0.9, 1],
          //       outputRange: [0, 0.25, 0.7, 1],
          //     }),
          //   },
          //   overlayStyle: {
          //     opacity: progress.interpolate({
          //       inputRange: [0, 1],
          //       outputRange: [0, 1],
          //       extrapolate: 'clamp',
          //     }),
          //   },
          // }),
        }}
      >
        <Stack.Screen name="PreLoadingScreen" component={PreLoadingScreen} />
        <Stack.Screen name="LoadingScreen" component={LoadingScreen} />
        <Stack.Screen name="AuthNavigator" component={AuthNavigator} />
        <Stack.Screen name='AppNavigator' component={AppNavigator} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default connect(mapStateToProps)(App)
